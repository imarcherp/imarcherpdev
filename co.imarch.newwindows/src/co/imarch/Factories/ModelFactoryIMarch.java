package co.imarch.Factories;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

import co.imarch.model.*;


public class ModelFactoryIMarch implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
		
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MIMACranecontrol.Table_Name))
			return MIMACranecontrol.class;
		
		if(tableName.equalsIgnoreCase(MIMAinventorytank.Table_Name))
			return MIMAinventorytank.class;

		if(tableName.equalsIgnoreCase(MIMAinventorytankline.Table_Name))
			return MIMAinventorytankline.class;
		
		return null;
		
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MIMACranecontrol.Table_Name))
			return new MIMACranecontrol(Env.getCtx(), Record_ID, trxName);
		
		if(tableName.equalsIgnoreCase(MIMAinventorytank.Table_Name))
			return new MIMAinventorytank(Env.getCtx(), Record_ID, trxName);
		
		if(tableName.equalsIgnoreCase(MIMAinventorytankline.Table_Name))
			return new MIMAinventorytankline(Env.getCtx(), Record_ID, trxName);
		
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MIMACranecontrol.Table_Name))
			return new MIMACranecontrol(Env.getCtx(), rs, trxName);
		
		if(tableName.equalsIgnoreCase(MIMAinventorytank.Table_Name))
			return new MIMAinventorytank(Env.getCtx(), rs, trxName);
		
		if(tableName.equalsIgnoreCase(MIMAinventorytankline.Table_Name))
			return new MIMAinventorytankline(Env.getCtx(), rs, trxName);
		
		return null;
	}

}
