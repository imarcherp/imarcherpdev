/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package co.imarch.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for IMARCH_CraneControl
 *  @author iDempiere (generated) 
 *  @version Release 4.1
 */
@SuppressWarnings("all")
public interface I_IMARCH_CraneControl 
{

    /** TableName=IMARCH_CraneControl */
    public static final String Table_Name = "IMARCH_CraneControl";

    /** AD_Table_ID=1000002 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name imarch_addressdelivery */
    public static final String COLUMNNAME_imarch_addressdelivery = "imarch_addressdelivery";

	/** Set imarch_addressdelivery	  */
	public void setimarch_addressdelivery (String imarch_addressdelivery);

	/** Get imarch_addressdelivery	  */
	public String getimarch_addressdelivery();

    /** Column name imarch_addressload */
    public static final String COLUMNNAME_imarch_addressload = "imarch_addressload";

	/** Set imarch_addressload	  */
	public void setimarch_addressload (String imarch_addressload);

	/** Get imarch_addressload	  */
	public String getimarch_addressload();

    /** Column name imarch_applicant */
    public static final String COLUMNNAME_imarch_applicant = "imarch_applicant";

	/** Set imarch_applicant	  */
	public void setimarch_applicant (String imarch_applicant);

	/** Get imarch_applicant	  */
	public String getimarch_applicant();

    /** Column name imarch_crane_plate */
    public static final String COLUMNNAME_imarch_crane_plate = "imarch_crane_plate";

	/** Set imarch_crane_plate	  */
	public void setimarch_crane_plate (String imarch_crane_plate);

	/** Get imarch_crane_plate	  */
	public String getimarch_crane_plate();

    /** Column name IMARCH_cranecontrol_ID */
    public static final String COLUMNNAME_IMARCH_cranecontrol_ID = "IMARCH_cranecontrol_ID";

	/** Set CraneControl	  */
	public void setIMARCH_cranecontrol_ID (int IMARCH_cranecontrol_ID);

	/** Get CraneControl	  */
	public int getIMARCH_cranecontrol_ID();

    /** Column name IMARCH_cranecontrol_UU */
    public static final String COLUMNNAME_IMARCH_cranecontrol_UU = "IMARCH_cranecontrol_UU";

	/** Set IMARCH_cranecontrol_UU	  */
	public void setIMARCH_cranecontrol_UU (String IMARCH_cranecontrol_UU);

	/** Get IMARCH_cranecontrol_UU	  */
	public String getIMARCH_cranecontrol_UU();

    /** Column name imarch_drivercrane */
    public static final String COLUMNNAME_imarch_drivercrane = "imarch_drivercrane";

	/** Set imarch_drivercrane	  */
	public void setimarch_drivercrane (String imarch_drivercrane);

	/** Get imarch_drivercrane	  */
	public String getimarch_drivercrane();

    /** Column name imarch_operator */
    public static final String COLUMNNAME_imarch_operator = "imarch_operator";

	/** Set imarch_operator	  */
	public void setimarch_operator (String imarch_operator);

	/** Get imarch_operator	  */
	public String getimarch_operator();

    /** Column name imarch_operator_telephone */
    public static final String COLUMNNAME_imarch_operator_telephone = "imarch_operator_telephone";

	/** Set imarch_operator_telephone	  */
	public void setimarch_operator_telephone (String imarch_operator_telephone);

	/** Get imarch_operator_telephone	  */
	public String getimarch_operator_telephone();

    /** Column name imarch_requestcenter */
    public static final String COLUMNNAME_imarch_requestcenter = "imarch_requestcenter";

	/** Set imarch_requestcenter	  */
	public void setimarch_requestcenter (String imarch_requestcenter);

	/** Get imarch_requestcenter	  */
	public String getimarch_requestcenter();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name level */
    public static final String COLUMNNAME_level = "level";

	/** Set level	  */
	public void setlevel (boolean level);

	/** Get level	  */
	public boolean islevel();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException;

    /** Column name smj_brand_id */
    public static final String COLUMNNAME_smj_brand_id = "smj_brand_id";

	/** Set smj_brand_id	  */
	public void setsmj_brand_id (BigDecimal smj_brand_id);

	/** Get smj_brand_id	  */
	public BigDecimal getsmj_brand_id();

    /** Column name smj_invoicevalue */
    public static final String COLUMNNAME_smj_invoicevalue = "smj_invoicevalue";

	/** Set smj_invoicevalue	  */
	public void setsmj_invoicevalue (BigDecimal smj_invoicevalue);

	/** Get smj_invoicevalue	  */
	public BigDecimal getsmj_invoicevalue();

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set smj_plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get smj_plate	  */
	public String getsmj_plate();

    /** Column name Summary */
    public static final String COLUMNNAME_Summary = "Summary";

	/** Set Summary.
	  * Textual summary of this request
	  */
	public void setSummary (String Summary);

	/** Get Summary.
	  * Textual summary of this request
	  */
	public String getSummary();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
