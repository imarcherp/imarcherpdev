/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package co.imarch.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for IMARCH_inventorytankline
 *  @author iDempiere (generated) 
 *  @version Release 4.1 - $Id$ */
public class X_IMARCH_inventorytankline extends PO implements I_IMARCH_inventorytankline, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170512L;

    /** Standard Constructor */
    public X_IMARCH_inventorytankline (Properties ctx, int IMARCH_inventorytankline_ID, String trxName)
    {
      super (ctx, IMARCH_inventorytankline_ID, trxName);
      /** if (IMARCH_inventorytankline_ID == 0)
        {
			setIMARCH_inventorytank_ID (0);
			setIMARCH_inventorytankline_ID (0);
			setM_Locator_ID (0);
			setM_Product_ID (0);
			setmeasureqty (Env.ZERO);
			setMovementDate (new Timestamp( System.currentTimeMillis() ));
        } */
    }

    /** Load Constructor */
    public X_IMARCH_inventorytankline (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_IMARCH_inventorytankline[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_IMARCH_inventorytank getIMARCH_inventorytank() throws RuntimeException
    {
		return (I_IMARCH_inventorytank)MTable.get(getCtx(), I_IMARCH_inventorytank.Table_Name)
			.getPO(getIMARCH_inventorytank_ID(), get_TrxName());	}

	/** Set InventorytTank.
		@param IMARCH_inventorytank_ID InventorytTank	  */
	public void setIMARCH_inventorytank_ID (int IMARCH_inventorytank_ID)
	{
		if (IMARCH_inventorytank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytank_ID, Integer.valueOf(IMARCH_inventorytank_ID));
	}

	/** Get InventorytTank.
		@return InventorytTank	  */
	public int getIMARCH_inventorytank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IMARCH_inventorytank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set InventorytTankLine.
		@param IMARCH_inventorytankline_ID InventorytTankLine	  */
	public void setIMARCH_inventorytankline_ID (int IMARCH_inventorytankline_ID)
	{
		if (IMARCH_inventorytankline_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytankline_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytankline_ID, Integer.valueOf(IMARCH_inventorytankline_ID));
	}

	/** Get InventorytTankLine.
		@return InventorytTankLine	  */
	public int getIMARCH_inventorytankline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IMARCH_inventorytankline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set IMARCH_inventorytankline_UU.
		@param IMARCH_inventorytankline_UU IMARCH_inventorytankline_UU	  */
	public void setIMARCH_inventorytankline_UU (String IMARCH_inventorytankline_UU)
	{
		set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytankline_UU, IMARCH_inventorytankline_UU);
	}

	/** Get IMARCH_inventorytankline_UU.
		@return IMARCH_inventorytankline_UU	  */
	public String getIMARCH_inventorytankline_UU () 
	{
		return (String)get_Value(COLUMNNAME_IMARCH_inventorytankline_UU);
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set measureqty.
		@param measureqty measureqty	  */
	public void setmeasureqty (BigDecimal measureqty)
	{
		set_Value (COLUMNNAME_measureqty, measureqty);
	}

	/** Get measureqty.
		@return measureqty	  */
	public BigDecimal getmeasureqty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_measureqty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Movement Date.
		@param MovementDate 
		Date a product was moved in or out of inventory
	  */
	public void setMovementDate (Timestamp MovementDate)
	{
		set_ValueNoCheck (COLUMNNAME_MovementDate, MovementDate);
	}

	/** Get Movement Date.
		@return Date a product was moved in or out of inventory
	  */
	public Timestamp getMovementDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_MovementDate);
	}
}