/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package co.imarch.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for IMARCH_CraneControl
 *  @author iDempiere (generated) 
 *  @version Release 4.1 - $Id$ */
public class X_IMARCH_CraneControl extends PO implements I_IMARCH_CraneControl, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170324L;

    /** Standard Constructor */
    public X_IMARCH_CraneControl (Properties ctx, int IMARCH_CraneControl_ID, String trxName)
    {
      super (ctx, IMARCH_CraneControl_ID, trxName);
      /** if (IMARCH_CraneControl_ID == 0)
        {
			setC_BPartner_ID (0);
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setIMARCH_cranecontrol_ID (0);
			setlevel (false);
			setName (null);
			setProcessed (false);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_IMARCH_CraneControl (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_IMARCH_CraneControl[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_ValueNoCheck (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set imarch_addressdelivery.
		@param imarch_addressdelivery imarch_addressdelivery	  */
	public void setimarch_addressdelivery (String imarch_addressdelivery)
	{
		set_Value (COLUMNNAME_imarch_addressdelivery, imarch_addressdelivery);
	}

	/** Get imarch_addressdelivery.
		@return imarch_addressdelivery	  */
	public String getimarch_addressdelivery () 
	{
		return (String)get_Value(COLUMNNAME_imarch_addressdelivery);
	}

	/** Set imarch_addressload.
		@param imarch_addressload imarch_addressload	  */
	public void setimarch_addressload (String imarch_addressload)
	{
		set_Value (COLUMNNAME_imarch_addressload, imarch_addressload);
	}

	/** Get imarch_addressload.
		@return imarch_addressload	  */
	public String getimarch_addressload () 
	{
		return (String)get_Value(COLUMNNAME_imarch_addressload);
	}

	/** Set imarch_applicant.
		@param imarch_applicant imarch_applicant	  */
	public void setimarch_applicant (String imarch_applicant)
	{
		set_Value (COLUMNNAME_imarch_applicant, imarch_applicant);
	}

	/** Get imarch_applicant.
		@return imarch_applicant	  */
	public String getimarch_applicant () 
	{
		return (String)get_Value(COLUMNNAME_imarch_applicant);
	}

	/** Set imarch_crane_plate.
		@param imarch_crane_plate imarch_crane_plate	  */
	public void setimarch_crane_plate (String imarch_crane_plate)
	{
		set_Value (COLUMNNAME_imarch_crane_plate, imarch_crane_plate);
	}

	/** Get imarch_crane_plate.
		@return imarch_crane_plate	  */
	public String getimarch_crane_plate () 
	{
		return (String)get_Value(COLUMNNAME_imarch_crane_plate);
	}

	/** Set CraneControl.
		@param IMARCH_cranecontrol_ID CraneControl	  */
	public void setIMARCH_cranecontrol_ID (int IMARCH_cranecontrol_ID)
	{
		if (IMARCH_cranecontrol_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_IMARCH_cranecontrol_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_IMARCH_cranecontrol_ID, Integer.valueOf(IMARCH_cranecontrol_ID));
	}

	/** Get CraneControl.
		@return CraneControl	  */
	public int getIMARCH_cranecontrol_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IMARCH_cranecontrol_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set IMARCH_cranecontrol_UU.
		@param IMARCH_cranecontrol_UU IMARCH_cranecontrol_UU	  */
	public void setIMARCH_cranecontrol_UU (String IMARCH_cranecontrol_UU)
	{
		set_Value (COLUMNNAME_IMARCH_cranecontrol_UU, IMARCH_cranecontrol_UU);
	}

	/** Get IMARCH_cranecontrol_UU.
		@return IMARCH_cranecontrol_UU	  */
	public String getIMARCH_cranecontrol_UU () 
	{
		return (String)get_Value(COLUMNNAME_IMARCH_cranecontrol_UU);
	}

	/** Set imarch_drivercrane.
		@param imarch_drivercrane imarch_drivercrane	  */
	public void setimarch_drivercrane (String imarch_drivercrane)
	{
		set_Value (COLUMNNAME_imarch_drivercrane, imarch_drivercrane);
	}

	/** Get imarch_drivercrane.
		@return imarch_drivercrane	  */
	public String getimarch_drivercrane () 
	{
		return (String)get_Value(COLUMNNAME_imarch_drivercrane);
	}

	/** Set imarch_operator.
		@param imarch_operator imarch_operator	  */
	public void setimarch_operator (String imarch_operator)
	{
		set_Value (COLUMNNAME_imarch_operator, imarch_operator);
	}

	/** Get imarch_operator.
		@return imarch_operator	  */
	public String getimarch_operator () 
	{
		return (String)get_Value(COLUMNNAME_imarch_operator);
	}

	/** Set imarch_operator_telephone.
		@param imarch_operator_telephone imarch_operator_telephone	  */
	public void setimarch_operator_telephone (String imarch_operator_telephone)
	{
		set_Value (COLUMNNAME_imarch_operator_telephone, imarch_operator_telephone);
	}

	/** Get imarch_operator_telephone.
		@return imarch_operator_telephone	  */
	public String getimarch_operator_telephone () 
	{
		return (String)get_Value(COLUMNNAME_imarch_operator_telephone);
	}

	/** Set imarch_requestcenter.
		@param imarch_requestcenter imarch_requestcenter	  */
	public void setimarch_requestcenter (String imarch_requestcenter)
	{
		set_Value (COLUMNNAME_imarch_requestcenter, imarch_requestcenter);
	}

	/** Get imarch_requestcenter.
		@return imarch_requestcenter	  */
	public String getimarch_requestcenter () 
	{
		return (String)get_Value(COLUMNNAME_imarch_requestcenter);
	}

	/** Set level.
		@param level level	  */
	public void setlevel (boolean level)
	{
		set_Value (COLUMNNAME_level, Boolean.valueOf(level));
	}

	/** Get level.
		@return level	  */
	public boolean islevel () 
	{
		Object oo = get_Value(COLUMNNAME_level);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException
    {
		return (org.compiere.model.I_R_Request)MTable.get(getCtx(), org.compiere.model.I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_R_Request_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_brand_id.
		@param smj_brand_id smj_brand_id	  */
	public void setsmj_brand_id (BigDecimal smj_brand_id)
	{
		set_Value (COLUMNNAME_smj_brand_id, smj_brand_id);
	}

	/** Get smj_brand_id.
		@return smj_brand_id	  */
	public BigDecimal getsmj_brand_id () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_brand_id);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set smj_invoicevalue.
		@param smj_invoicevalue smj_invoicevalue	  */
	public void setsmj_invoicevalue (BigDecimal smj_invoicevalue)
	{
		set_Value (COLUMNNAME_smj_invoicevalue, smj_invoicevalue);
	}

	/** Get smj_invoicevalue.
		@return smj_invoicevalue	  */
	public BigDecimal getsmj_invoicevalue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_invoicevalue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set smj_plate.
		@param smj_plate smj_plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get smj_plate.
		@return smj_plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	/** Set Summary.
		@param Summary 
		Textual summary of this request
	  */
	public void setSummary (String Summary)
	{
		set_Value (COLUMNNAME_Summary, Summary);
	}

	/** Get Summary.
		@return Textual summary of this request
	  */
	public String getSummary () 
	{
		return (String)get_Value(COLUMNNAME_Summary);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}