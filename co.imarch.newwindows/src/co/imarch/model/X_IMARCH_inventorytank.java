/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package co.imarch.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for IMARCH_inventorytank
 *  @author iDempiere (generated) 
 *  @version Release 4.1 - $Id$ */
public class X_IMARCH_inventorytank extends PO implements I_IMARCH_inventorytank, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170512L;

    /** Standard Constructor */
    public X_IMARCH_inventorytank (Properties ctx, int IMARCH_inventorytank_ID, String trxName)
    {
      super (ctx, IMARCH_inventorytank_ID, trxName);
      /** if (IMARCH_inventorytank_ID == 0)
        {
			setC_Period_ID (0);
			setIMARCH_inventorytank_ID (0);
			setProcessed (false);
        } */
    }

    /** Load Constructor */
    public X_IMARCH_inventorytank (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_IMARCH_inventorytank[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Period_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set InventorytTank.
		@param IMARCH_inventorytank_ID InventorytTank	  */
	public void setIMARCH_inventorytank_ID (int IMARCH_inventorytank_ID)
	{
		if (IMARCH_inventorytank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytank_ID, Integer.valueOf(IMARCH_inventorytank_ID));
	}

	/** Get InventorytTank.
		@return InventorytTank	  */
	public int getIMARCH_inventorytank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IMARCH_inventorytank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set IMARCH_inventorytank_UU.
		@param IMARCH_inventorytank_UU IMARCH_inventorytank_UU	  */
	public void setIMARCH_inventorytank_UU (String IMARCH_inventorytank_UU)
	{
		set_ValueNoCheck (COLUMNNAME_IMARCH_inventorytank_UU, IMARCH_inventorytank_UU);
	}

	/** Get IMARCH_inventorytank_UU.
		@return IMARCH_inventorytank_UU	  */
	public String getIMARCH_inventorytank_UU () 
	{
		return (String)get_Value(COLUMNNAME_IMARCH_inventorytank_UU);
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}