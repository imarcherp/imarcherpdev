package org.adempiere.webui.dashboard;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.DatetimeBox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WTableDirEditor;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MRequest;
import org.compiere.model.MRequestType;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Center;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.South;

public class EventWindowiMarch extends Window implements EventListener<Event>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4509922286644357438L;
	private DatetimeBox dtBeginDate, dtEndDate;
	private Textbox txtContent, txtHeaderColor, txtContentColor;
	private ConfirmPanel confirmPanel;
	
	//---iMarch
	private Button btndelete;
	private Window parent;
	private static final CLogger log = CLogger.getCLogger(EventWindowiMarch.class);
	private int AdminRol = MSysConfig.getIntValue("iMarch_AdminRolEvent", 0);
	private WTableDirEditor txtConfideltial;
	//---
	
	private int R_Request_ID = 0;
	
	public EventWindowiMarch(Window parent) {
		
		super();
		this.parent = parent;
		
		Properties ctx = Env.getCtx();
		setTitle(Msg.getMsg(ctx,"Event"));
		setAttribute(Window.MODE_KEY, Window.MODE_POPUP);
		setWidth("400px");
		setHeight("380px");
		this.setBorder("normal");
		this.setClosable(true);
		
		Label lblHeaderColor  = new Label(Msg.getElement(ctx,MRequestType.COLUMNNAME_HeaderColor));
		Label lblContentColor = new Label(Msg.getElement(ctx,MRequestType.COLUMNNAME_ContentColor));
		Label lblBeginDate    = new Label(Msg.getElement(ctx,"StartDate"));
		Label lblEndDate      = new Label(Msg.getElement(ctx,"EndDate"));
		Label lblContent      = new Label(Msg.getElement(ctx,MRequest.COLUMNNAME_Summary));
		//---iMarch
		Label lblConfidential = new Label(Msg.getElement(ctx,MRequest.COLUMNNAME_ConfidentialType));
	    //---
		dtBeginDate = new DatetimeBox();
		dtBeginDate.setEnabled(false);
		
		dtEndDate = new DatetimeBox();
		dtEndDate.setEnabled(false);
		
		txtContent = new Textbox();
		txtContent.setRows(5);
		txtContent.setWidth("95%");
		txtContent.setHeight("100%");
		txtContent.setReadonly(true);
		
		//---iMarch
		int columnID = MColumn.getColumn_ID(MRequest.Table_Name, MRequest.COLUMNNAME_ConfidentialType);
		MLookup lookup = MLookupFactory.get(Env.getCtx(), 0, 0, columnID, DisplayType.List);
		txtConfideltial =  new WTableDirEditor("ConfidentialType", true, true, true, lookup);
		txtConfideltial.getComponent().setWidth("95%");
		//---
		
		txtHeaderColor = new Textbox();
		txtHeaderColor.setWidth("50px");
		txtHeaderColor.setReadonly(true);
		
		txtContentColor = new Textbox();
		txtContentColor.setWidth("50px");
		txtContentColor.setReadonly(true);
		
		confirmPanel = new ConfirmPanel(false, false, false, false, false, false);
		confirmPanel.addActionListener(this);
		
		//---iMarch
		btndelete = new Button();
		btndelete.setLabel("Delete");
		btndelete.addEventListener(Events.ON_CLICK, this);
		btndelete.setHeight("70%");
		btndelete.setWidth("100%");
		//---
		
		Grid grid = GridFactory.newGridLayout();
		
		Columns columns = new Columns();
		grid.appendChild(columns);
		
		Column column = new Column();
		columns.appendChild(column);
		
		column = new Column();
		columns.appendChild(column);
		column.setWidth("250px");
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(lblBeginDate.rightAlign());
		row.appendChild(dtBeginDate);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblEndDate.rightAlign());
		row.appendChild(dtEndDate);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblContent.rightAlign());
		row.appendChild(txtContent);
		
		//---iMarch
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblConfidential.rightAlign());
		row.appendChild(txtConfideltial.getComponent());
		//---
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblHeaderColor.rightAlign());
		row.appendChild(txtHeaderColor);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblContentColor.rightAlign());
		row.appendChild(txtContentColor);
		
		//---iMarch
		row = new Row();
		rows.appendChild(row);
		row.appendChild(new Label());
		row.appendChild(btndelete);
		//---
		
		Borderlayout borderlayout = new Borderlayout();
		appendChild(borderlayout);
		
		Center center = new Center();
		borderlayout.appendChild(center);
		center.appendChild(grid);
		grid.setVflex("1");
		grid.setHflex("1");
		
		South south = new South();
		borderlayout.appendChild(south);
		south.appendChild(confirmPanel);
	}
	
	public void setData(ADCalendarEvent event) {
		txtHeaderColor.setStyle("background-color: " + event.getHeaderColor());
		txtContentColor.setStyle("background-color: " + event.getContentColor());
		
		dtBeginDate.setValue(event.getBeginDate());
		dtEndDate.setValue(event.getEndDate());
		txtContent.setText(event.getContent());
		
		//---iMarch
		txtConfideltial.setValue(getConfidential(event.getR_Request_ID()));
		
		R_Request_ID = event.getR_Request_ID();
		//---iMarch
		if(Env.getAD_User_ID(Env.getCtx()) == getCreateRequest(R_Request_ID) || 
				Env.getAD_Role_ID(Env.getCtx()) == AdminRol)
			btndelete.setDisabled(false);
		else btndelete.setDisabled(true);
		//---
		//confirmPanel.getButton(ConfirmPanel.A_ZOOM).setEnabled(R_Request_ID > 0);
	}
	
	public void onEvent(Event e) throws Exception {
		if (e.getTarget() == confirmPanel.getButton(ConfirmPanel.A_OK))
			setVisible(false);
		else if (e.getTarget() == confirmPanel.getButton(ConfirmPanel.A_ZOOM)) {
			if (R_Request_ID > 0)
				AEnv.zoom(417, R_Request_ID);
		}
		else if(e.getName().equals(Events.ON_CLICK)){
			if(e.getTarget() == btndelete){
				if(DeleteRequest(R_Request_ID)){
					setVisible(false);
					Events.postEvent("onRefresh", parent, null);
				}
				else
					Messagebox.show("No se elimino el evento. Contacte al administrador");
			}
		}
	}//onEvent
	
	public Object getConfidential(int RequestID){
		Object result="";
		StringBuffer sql = new StringBuffer("SELECT ConfidentialType FROM R_Request WHERE R_Request_ID ="+RequestID);
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next())
				result = rs.getString("ConfidentialType");
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs, st);
			st = null;
			rs = null;
		}
		return result;
	}//getConfidential
	
	public int getCreateRequest (int RequestID){
		int result = 0;
		StringBuffer sql = new StringBuffer("SELECT CreatedBy FROM R_Request WHERE R_Request_ID ="+RequestID);
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next())
				result = rs.getInt("CreatedBy");
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs, st);
			st = null;
			rs = null;
		}
		return result;
	}//getCreateRequest
	
	public Boolean DeleteRequest (int RequestID){
		int nodel = DB.executeUpdateEx(
				"DELETE FROM R_Request WHERE R_Request_ID = ?",
				new Object[] { RequestID },
				null);
		if(nodel > 0)
			return true;
		
		return false;
	}//DeleteRequest

}
