package org.adempiere.webui.dashboard;


import org.adempiere.webui.component.ToolBarButton;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Box;
import org.zkoss.zul.Vbox;

import co.imarch.webui.acct.WAcctVieweriMarch;


public class DPViewsiMarch extends DashboardPanel implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1619139095629180972L;
	
	private CLogger log = CLogger.getCLogger(DPViewsiMarch.class);


	public DPViewsiMarch(){
		super();
		this.setSclass("views-box");
		//this.setHeight("100%");
		ZKUpdateUtil.setHeight(this, "100%");
		this.appendChild(createViewPaneliMarch());
		
	}
	
	private Box createViewPaneliMarch(){
		Vbox vbox = new Vbox();
				
		if(MSysConfig.getBooleanValue(MSysConfig.DPViews_ShowInfoAccount, true, Env.getAD_Client_ID(Env.getCtx()))
				&& MRole.getDefault().isAllow_Info_Account() && MRole.getDefault().isShowAcct()){
	
			ToolBarButton btnViewLink = new ToolBarButton("InfoAccountiMarch");//Informacion Contable	
			btnViewLink.setSclass("link");
			btnViewLink.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "InfoAccount")));
			btnViewLink.setImage(ThemeManager.getThemeResource("images/InfoAccount16.png"));
			btnViewLink.addEventListener(Events.ON_CLICK, this);
			
			ToolBarButton btnPayAsigLink = new ToolBarButton("PaymentAsignament");//Asignacion Pagos
			btnPayAsigLink.setSclass("link");
			btnPayAsigLink.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "payment.allocation")));
			btnPayAsigLink.setImage(ThemeManager.getThemeResource("images/mForm.png"));
			btnPayAsigLink.addEventListener(Events.ON_CLICK, this);
						
			vbox.appendChild(btnViewLink);
			vbox.appendChild(btnPayAsigLink);  
		}
		else
			log.warning("Not access");
		return vbox;
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		
		if(arg0.getName().equals(Events.ON_CLICK)){
			if(arg0.getTarget() instanceof ToolBarButton){
				ToolBarButton link = (ToolBarButton)arg0.getTarget();
				if(link.getName().equals("InfoAccountiMarch")){
					new WAcctVieweriMarch();
				}
				else if(link.getName().equals("PaymentAsignament")){
					doOnClick(getMenuIDForm("co.imarch.plugins.forms.WAllocationiMarch"));	
				}
			}// if ToolbarButton
		}//if ON_CLIC
		
	}
	
	private int getMenuIDForm(String classnameform){
		String sql = "SELECT MAX(ad_menu_id) FROM ad_menu WHERE ad_form_id="
				+"(SELECT ad_form_id FROM ad_form WHERE classname=?)";
		int noquery = DB.getSQLValue(null, sql, new Object[] { classnameform });
		if (noquery > 0)
			return noquery;
		
		return 0;
	}
	
	private void doOnClick(int menuid){
		if(menuid>0)
			SessionManager.getAppDesktop().onMenuSelected(menuid);
	}
	
	
}
