package org.adempiere.webui.dashboard;



import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.MenuItem;
import org.adempiere.webui.apps.DocumentSearchController.SearchResult;
import org.adempiere.webui.component.Bandbox;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Box;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Vbox;

import co.imarch.webui.acct.WAcctVieweriMarch;


public class DPViewsiMarchWOrder extends DashboardPanel implements EventListener<Event>, 
			ValueChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1619139095629180972L;
	
	private CLogger log = CLogger.getCLogger(DPViewsiMarchWOrder.class);
	
	private static final String ON_ENTER_KEY = "onEnterKey";
	private static final String ON_CREATE_ECHO = "onCreateEcho";
	private static final String ON_SEARCH_DOCUMENTS = "onSearchDocuments";
	
	private Textbox txtworkorder = new Textbox();

	private Button btnFind = new Button("Buscar");
	
	private Checkbox orgInfotrans = new Checkbox();
	private Checkbox orgUcolbus = new Checkbox();
	
	private Bandbox bandbox;
	
	private int orgSelect = 0;
	
	
	

	public DPViewsiMarchWOrder(){
		super();
		this.setSclass("views-box");
		//this.setHeight("100%");
		ZKUpdateUtil.setHeight(this, "100%");
		this.appendChild(createViewPaneliMarch());
		
	}
	
	private Box createViewPaneliMarch(){
		Vbox vbox = new Vbox();
				
		if(MSysConfig.getBooleanValue(MSysConfig.DPViews_ShowInfoAccount, true, Env.getAD_Client_ID(Env.getCtx()))
				&& MRole.getDefault().isAllow_Info_Account() && MRole.getDefault().isShowAcct()){
	
					
			txtworkorder.addEventListener(Events.ON_CHANGE, this);
			txtworkorder.addEventListener(Events.ON_CHANGING, this);
			txtworkorder.addEventListener(ON_ENTER_KEY, this);
			txtworkorder.addEventListener(Events.ON_OK, this);
			txtworkorder.addEventListener(ON_SEARCH_DOCUMENTS, this);
			
							
			btnFind.addEventListener(Events.ON_CLICK, this);
			btnFind.setEnabled(false);
			
			orgInfotrans.setLabel("Infotrans");
			orgInfotrans.setChecked(true);
			orgSelect=1000004;
			orgInfotrans.addActionListener(this);
			orgUcolbus.setLabel("Ucolbus");
			orgUcolbus.setChecked(false);
			orgUcolbus.addActionListener(this);
						
			bandbox = new Bandbox();
			bandbox.setSclass("global-search-box");
			bandbox.setAutodrop(true);
			bandbox.addEventListener(Events.ON_CHANGING, this);
			bandbox.addEventListener(Events.ON_CHANGE, this);
			bandbox.setCtrlKeys("#up#down");
			bandbox.addEventListener(Events.ON_CTRL_KEY, this);
			bandbox.addEventListener(ON_ENTER_KEY, this);
						
			vbox.appendChild(bandbox);
			vbox.appendChild(txtworkorder);
			vbox.appendChild(btnFind);
			vbox.appendChild(orgInfotrans);
			vbox.appendChild(orgUcolbus); 
			 
			
		}
		else
			log.warning("Not access");
		return vbox;
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		
		//InputEvent inputEvent = (InputEvent) arg0;
		
		if(arg0.getName().equals(Events.ON_CLICK))
		{
			if(arg0.getTarget() instanceof Button)
			{
				valideWOrder();
			}// if Button
		}//Event  ON_CLIC
		
		if(Events.ON_CHANGING.equals(arg0.getName()) || Events.ON_CHANGE.equals(arg0.getName())) 
		{
			InputEvent inputEvent = (InputEvent) arg0;
			if(arg0.getTarget() instanceof Textbox) 
			{
				 inputEvent = (InputEvent) arg0;
				if(inputEvent.getValue().isEmpty())
					btnFind.setEnabled(false);
				else
					btnFind.setEnabled(true);
			}// TextBox
		}// Event ON_CHANGE
		 else if (Events.ON_CTRL_KEY.equals(arg0.getName())) {
	        	KeyEvent ke = (KeyEvent) arg0;
	        	if (ke.getKeyCode() == KeyEvent.UP) {
	        		Messagebox.show("Presiono UP");
	        	} else if (ke.getKeyCode() == KeyEvent.DOWN) {

	        		Messagebox.show("Presiono DOWN");
	        	}
	        }
		
		if (arg0.getName().equals(ON_CREATE_ECHO)) {
	    		StringBuilder script = new StringBuilder("jq('#")
	    			.append(txtworkorder.getUuid())
	    			.append("').bind('keydown', function(e) {var code=e.keyCode||e.which;console.log(code);if(code==13){")
	    			.append("var widget=zk.Widget.$(this);")
	    			.append("var event=new zk.Event(widget,'")
	    			.append(ON_ENTER_KEY)
	    			.append("',{},{toServer:true});")
	    			.append("zAu.send(event);")
	    			.append("}});");
	    		Clients.evalJavaScript(script.toString());
		}
		else if (arg0.getName().equals(ON_ENTER_KEY)) {
			/*
			 inputEvent = (InputEvent) arg0;
			if(!inputEvent.getValue().isEmpty())
				valideWOrder();
			*/
			Messagebox.show("Presiono ENTER");
		}
		
		if(arg0.getTarget().equals(orgInfotrans)) {
			if (orgInfotrans.isSelected()) {
				orgUcolbus.setChecked(false);
				orgSelect=1000004;
			}
			else {
				orgUcolbus.setChecked(true);
				orgSelect=1000002;
			}
		}
		else if(arg0.getTarget().equals(orgUcolbus)) {
			if (orgUcolbus.isSelected()) {
				orgInfotrans.setChecked(false);
				orgSelect=1000002;
			}
			else {
				orgInfotrans.setChecked(true);
				orgSelect=1000004;
			}
		}
	}
	
	private void valideWOrder() {
		
		String sql = "SELECT r_request_id FROM r_request WHERE documentno=?"
				+" AND AD_Org_ID=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] { txtworkorder.getValue().trim(), orgSelect });
		
		if(noquery<0)
			Messagebox.show("No se encuentra la Solicitud "+txtworkorder.getValue()+
					" para la organizacion "+(orgInfotrans.isChecked() ? "Infotrans":"Ucolbus"));
		else {
			
			MQuery query = new MQuery(417);
			query.addRestriction(" R_Request_ID='"+noquery+"' ");
			AEnv.zoom((orgInfotrans.isChecked() ?  1000045 : 1000085), query);
			txtworkorder.setValue("");
		}
	}

	@Override
	public void valueChange(ValueChangeEvent e) {

		 String name = e.getPropertyName();
		 
		 if(name.equals("textbo"))
			 name="";
	}
	
	
}
