package com.smj.webui.component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.util.DataQueries;
import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013-04-25
 *          <ul TYPE ="circle">
 *          <li>Clase ventana emergente para mostrar el listado de ordenes de trabajo y seleccionar una
 *          <li>Class float window to show work order list and select one
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoWorkOrder extends InfoPanel implements EventListener<Event>, WTableModelListener{
	
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	private Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
	private String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 935516751282073726L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();
	private Label stateLabel = new Label();
	private Listbox stateListbox = new Listbox();
	private Boolean isInvoice = false;
	private Boolean isGarranty = false;
	private String plate = "";
	private String states = "";
	private String noStates = "";
	private Label plateLabel = new Label();
	private Textbox plateTextBox = new Textbox();
	private Label requestTypeLabel = new Label();
	private Listbox requestTypeListbox = new Listbox();
	private Integer requestTypeId = 0;
	private Integer orgId = 0; 
	
	private Integer stateComplete = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOCOMPLETE",Env.getAD_Client_ID(Env.getCtx())).trim());
//	private Integer stateDelivery = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWODELIVERY",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateInvoiced = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOINVOICED",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static Integer rTypeRmnFac = Integer.parseInt(MSysConfig.getValue("SMJ-REQTYPEREMANUFAC",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static Integer rTypeWO = Integer.parseInt(MSysConfig.getValue("SMJ-REQUESTTYPE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	private LinkedList<HashMap<String, Object>> list = null;
	private Callback<Integer> _callback;

	/**
	 * SMJInfoWorkOrder constructor.
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 */
	public SMJInfoWorkOrder(int WindowNo, String tableName, String keyColumn,
			boolean multipleSelection, String whereClause, Boolean is_Invoice, 
			String selected_plate, Boolean is_Garranty, String no_states, Callback<Integer> callback) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		isInvoice = is_Invoice;
		plate = selected_plate;
		plateTextBox.setValue(plate);
		states = stateComplete.toString();//+","+stateDelivery.toString();
		orgId = Env.getAD_Org_ID(Env.getCtx());
		//--Adicion iMarch / Organizacion Transportadora--
		orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
		if(orgfreighttrans.equals("true")) orgInfotrans=orgId;
		//------------------------------------------------
		//si es organizacion infotrans
		if (orgId.equals(orgInfotrans)){
			states = MSysConfig.getValue("SMJ-INFOTRAVELEND",Env.getAD_Client_ID(Env.getCtx())).trim();
		}
		
		noStates = no_states;
		isGarranty = is_Garranty;
		if(isGarranty){
			states = stateInvoiced.toString();
		}
		_callback = callback;
		init();
		listRequestType();
		getDataList(null, null,0);
		loadTable();
	}//SMJInfoWorkOrder

	/**
	 * ubica elementos en la ventana - locates elements in info window
	 */
	private void init()
	{
		//Adicion iMarch / Organizacion Transportadora
		orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
		if(orgfreighttrans.equals("true")) orgInfotrans=orgId;
		//---------------------------------------------
		fieldValue.setWidth("50%");
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
		stateLabel.setText("     " + Msg.translate(Env.getCtx(), "R_Status_ID")+"     ");
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		plateTextBox.addEventListener(Events.ON_BLUR, this);
		requestTypeLabel.setText("" + Msg.translate(Env.getCtx(), "R_RequestType_ID"));
		
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		fieldLabel.setWidth("100px");
		row.appendChild(fieldLabel.rightAlign());
		fieldValue.setWidth("150px");
		row.appendChild(fieldValue);
		
		if (plate!= null && plate.length()>0){
			row = new Row();
			rows.appendChild(row);
			plateLabel.setWidth("100px");
			row.appendChild(plateLabel.rightAlign());
			plateTextBox.setWidth("150px");
			row.appendChild(plateTextBox);
		}
		
		if(!isInvoice && !orgId.equals(orgInfotrans)){
			row = new Row();
			rows.appendChild(row);
			stateLabel.setWidth("100px");
			row.appendChild(stateLabel.rightAlign());
			stateListbox.setWidth("150px");
			row.appendChild(stateListbox);

			Integer woListStates = Integer.parseInt(MSysConfig.getValue("SMJ-STATESWOLIST",Env.getAD_Client_ID(Env.getCtx())).trim());
			listStates(woListStates);
		}
		if(!isInvoice || orgId.equals(orgInfotrans)){
			row = new Row();
			rows.appendChild(row);
			requestTypeLabel.setWidth("100px");
			row.appendChild(requestTypeLabel.rightAlign());
			requestTypeListbox.setWidth("150px");
			row.appendChild(requestTypeListbox);
		}//if(!isInvoice)
		else{
			requestTypeId = rTypeWO;
		}
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        contentPanel.setMold("paging");
        contentPanel.setPageSize(MSysConfig.getIntValue("ZK_PAGING_SIZE", 100));
        contentPanel.setPagingPosition("bottom");
        contentPanel.setAutopaging(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		setWidth("700px");
//		setHeight("350px");
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	}//init
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		try {
			Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
			Iterator<HashMap<String, Object>> itlist = list.iterator();
			while (itlist.hasNext()){
				HashMap<String, Object> obj = itlist.next();
				Vector<Object> v = new Vector<Object>();
				v.add(obj.get("DOC"));
				v.add(obj.get("SUMMARY"));
				v.add(obj.get("REQTYPE"));
				v.add(obj.get("STATUS"));
				v.add(obj.get("PLATE"));
				v.add(obj.get("DATE"));
				datat.add(v);
			}
			Vector<String> columnNames = getTableColumnNames();
			contentPanel.getModel().clear();
			contentPanel.getModel().removeTableModelListener(this);
			//  Set Model
			ListModelTable modelTable = new ListModelTable(datat);
			modelTable.removeTableModelListener(this);
//			model.addTableModelListener(this);
			contentPanel.setData(modelTable, columnNames);
//			contentPanel.addActionListener(this);
			contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
			setTableColumnClass(contentPanel);
			contentPanel.setFocus(true);
			
			for (Component component : contentPanel.getListHead().getChildren()) {
				ListHeader header = (ListHeader) component;
				header.setHflex("max");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".loadTable - ERROR: " + e.getMessage(), e);
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
		columnNames.add(Msg.translate(Env.getCtx(), "summary"));
		columnNames.add(Msg.translate(Env.getCtx(), "R_RequestType_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "R_Status_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelDate"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);  
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true); 
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event ev) {
//		super.onEvent(event);
		try {
			Integer state = 0;
			if(stateListbox.getSelectedItem()!= null )
				//state = (Integer) stateListbox.getSelectedItem().getValue(); //modificacion iMarch
				state = Integer.parseInt(stateListbox.getSelectedItem().getValue().toString());
			if(state == null){
				state = 0;
			}
			String field = "";
			if(fieldValue.getValue() != null){
				field = fieldValue.getValue();
			}
			if (field == null)
				field = "";
			if (ev.getTarget().equals(fieldValue)){
				if (field == null || field.length()<= 0 || field.equals("") || field.equals(" "))
					return;
				getDataList(null, field, state);
				loadTable();
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Integer index = contentPanel.getSelectedIndex();
				if (index == null || index < 0){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectItemList"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				Iterator<Object> iteratorObject = contentPanel.getModel().getSelection().iterator();
				Vector<Object> vector = (Vector<Object>) iteratorObject.next();

				_callback.onCallback(DataQueries.getWOByDocumentNo(vector.get(0).toString(), Env.getAD_Client_ID(Env.getCtx())));
				dispose(false);
				onClose();
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				_callback.onCallback(null);
	            dispose(false);
	            onClose();
	        }
			else if(ev.getTarget().equals(stateListbox)){
				getDataList(null, field, state);
				loadTable();
			}
			else if (ev.getTarget().equals(plateTextBox)){
				plate = plateTextBox.getValue();
				getDataList(null, field, state);
				loadTable();
			}
			else if(ev.getTarget().equals(requestTypeListbox)){
				requestTypeId = (Integer) requestTypeListbox.getSelectedItem().getValue();
				getDataList(null, field, state);
				loadTable();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".onEvent ERROR:: "+e.getMessage(), e);
		}
	}//onEvent
	
	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
		
	}

	/**
	 * regresa la lista de solicitudes - return request list
	 * @param trxName
	 * @param value
	 * @return LinkedList<HashMap<String, Object>>
	 */ 
	private void getDataList(String trxName, String value, Integer state) {
		StringBuffer sql = new StringBuffer();
		String[] plates = plate.split(";");
		String qryPlates = "";
		for(int i=0;i<plates.length;i++){
			qryPlates = qryPlates+"'"+plates[i]+"',";
			
		}
		if (qryPlates.endsWith(",")){
			qryPlates = qryPlates.substring(0, qryPlates.length()-1);
		}
		
		sql.append(" SELECT r.R_Request_ID, r.documentNo, r.summary, s.name, r.smj_plate, r.datenextaction, t.name AS REQTYPE ");
		sql.append(" FROM R_Request r, R_Status s, R_RequestType t ");
		sql.append(" WHERE r.R_Status_ID = s.R_Status_ID AND r.ad_client_ID = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		sql.append(" AND r.ad_org_id = "+Env.getAD_Org_ID(Env.getCtx())+" ");
		sql.append(" AND r.R_RequestType_ID = t.R_RequestType_ID");
		if (value != null){
			sql.append(" AND r.documentNo LIKE '"+value.trim()+"%' ");
		}
		//Adicion iMarch / Organizacion Transportadora
		orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
		if(orgfreighttrans.equals("true")) orgInfotrans=orgId;
		//---------------------------------------------
		if (isInvoice || orgId.equals(orgInfotrans)){
			sql.append(" AND r.R_Status_ID in("+states+") ");
		}
		if(orgId.equals(orgInfotrans)){
			sql.append(" AND r.C_Invoice_ID IS NULL AND smj_isinvoiceapproved = 'Y' ");
		}
		if( plate.length() > 0){
			sql.append(" AND r.smj_plate in("+qryPlates+") ");
		}
		if(isGarranty){
			sql.append(" AND r.R_Request_ID in (SELECT distinct(R_Request_ID) FROM smj_workOrderLine ");
			sql.append(" WHERE smj_isWarrantyApplied = 'N') ");
		}
		if( noStates.length() > 0){
			sql.append(" AND r.R_Status_ID NOT IN ("+noStates+") ");
		}
		if(state >0){
			sql.append(" AND r.R_Status_ID = "+state+" ");
		}
		if(requestTypeId >0){
			sql.append(" AND r.R_RequestType_ID = "+requestTypeId+" ");
		}
		sql.append(" ORDER BY documentNo ASC ");
		
		list = new LinkedList<HashMap<String, Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("CODE", rs.getInt("R_Request_ID"));
				obj.put("DOC", rs.getString("documentNo"));
				obj.put("SUMMARY", rs.getString("summary"));
				obj.put("STATUS", rs.getString("name"));
				obj.put("PLATE", rs.getString("smj_plate"));
				obj.put("REQTYPE", rs.getString("REQTYPE"));
				Date dt = rs.getDate("datenextaction");
				if (dt == null){
					obj.put("DATE", " ");
				}else{
					obj.put("DATE", sdf.format(dt));
				}
				list.add(obj);
			}// if
			
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getDataList - SQL: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
	}// getDataList

	/**
	 * Llena el combo de estados
	 * fill state list
	 */
	private void listStates(Integer woListStates){
		stateListbox.removeAllItems();
		Vector<Vector<Object>> data = DataQueriesTrans.getStateList(woListStates,0);
		stateListbox.appendItem("", "-1");
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			stateListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		stateListbox.setMold("select");
		stateListbox.addActionListener(this);
//		stateListbox.setValue(stateDefault);
	}//listStates	
	
	/**
	 * Llena el combo de estados
	 * fill state list
	 * @param state
	 */
	private void listRequestType(){
		requestTypeListbox.removeAllItems();
		String value = rTypeRmnFac+","+rTypeWO;
		Vector<Vector<Object>> data = DataQueriesTrans.getRequestTypeList(value, orgId);
		requestTypeListbox.appendItem("", 0);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			requestTypeListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		requestTypeListbox.setMold("select");
		requestTypeListbox.addActionListener(this);
//		requestTypeListbox.setValue(rTypeWO);
	}//listStates
	
}//SMJInfoWorkOrder
