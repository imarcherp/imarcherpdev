package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.model.MSMJTires;
import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;
import com.smj.util.Message;
import com.smj.util.SaveHistory;
import com.smj.util.UtilTrans;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013-04-30
 *          <ul TYPE ="circle">
 *          <li>Clase ventana emergente para actualizar la informacion de llantas
 *          <li>Class float window to update tires information
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoTires extends InfoPanel implements EventListener<Event>, WTableModelListener{

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 935516751282073726L;
	private Borderlayout layout;
	private boolean isRotateTires = false; 
	private Vbox southBody;
	//	private Textbox fieldValue = new Textbox();
	//	private Label fieldLabel = new Label();
	private Label plateLabel = new Label();
	private Textbox plateTextBox = new Textbox();
	private Label positionLabel = new Label();
	private Textbox positionTextBox = new Textbox();
	private Label kmsLabel = new Label();
	private Textbox kmsTextBox = new Textbox();
	private Label depthTireLabel = new Label();
	private Decimalbox depthTireTextBox = new Decimalbox();
	private Label depthBrakeLabel = new Label();
	private Decimalbox depthBrakeTextBox = new Decimalbox();
	private Label serialLabel = new Label();
	private Textbox serialTextBox = new Textbox();
	private Label positionNewLabel = new Label();
	private Intbox positionNewTextBox = new Intbox();
	private Checkbox rotateTires = new Checkbox();
	private Button editButton = new Button ();
	private Button editTireButton = new Button (); 
	private ValueChangeListener listener = null;
	private Integer vehicleId = 0;
	private Integer qtyTires = 0;
	private Integer mechanicId = 0;
	private Integer woLineId = 0;
	private Boolean isTrailer = false; 
	private Label trailerLabel = new Label();
	private Checkbox trailerCheckbox = new Checkbox();
	private MSMJVehicle vh = null;
	private Boolean isCompleted = false;
	private List<EventListener<Event>> listeners = new ArrayList<EventListener<Event>>();

	private LinkedList<MSMJTires> list = null;
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();

	public SMJInfoTires(int WindowNo, String tableName, String keyColumn, boolean multipleSelection, 
			String whereClause, Integer vehicle, ValueChangeListener m_listener, Integer mechanic_Id
			,Integer lineId, Boolean completed) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		vehicleId = vehicle;
		listener = m_listener;
		mechanicId = mechanic_Id;
		woLineId = lineId;
		isCompleted = completed;
		if (vehicleId > 0){
			vh = new MSMJVehicle(Env.getCtx(), vehicleId, null);
			qtyTires = vh.getqtytires();
			if(vh.istrailer() || vh.istrucktractor()){
				isTrailer = true;
			}
			plateTextBox.setValue(vh.getsmj_plate());
			//			System.out.println(this.getClass().getName()+".SMJInfoPlate vehicle......."+vehicleId+"...tires...."+qtyTires);
		}
		//		else{
		//			System.out.println("fue nulo......"+vehicleId);
		//		}

		init();
		if (vh!=null){
			list = DataQueriesTrans.getTiresList(null, vh);
		}
		loadTable();
		lockFields();
	}//SMJInfoPlate

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		//		fieldValue.setWidth("50%");
		//		fieldValue.addEventListener(Events.ON_BLUR, this);
		//		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		positionLabel.setText("" + Msg.translate(Env.getCtx(), "position"));
		kmsLabel.setText("" + Msg.translate(Env.getCtx(), "kms"));
		depthTireLabel.setText("" + Msg.translate(Env.getCtx(), "valueTire"));
		depthBrakeLabel.setText("" + Msg.translate(Env.getCtx(), "valueBrake"));
		serialLabel.setText("" + Msg.translate(Env.getCtx(), "serial"));
		positionNewLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelNewPosition"));
		rotateTires.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelRotateTires"));
		rotateTires.addActionListener(this);

		editButton.setLabel("" + Msg.translate(Env.getCtx(), "save"));
		editButton.addActionListener(this);
		editTireButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelNewEditTire"));
		editTireButton.addActionListener(this);
		trailerLabel.setText("" + Msg.translate(Env.getCtx(), "istrailer"));

		Grid grid = GridFactory.newGridLayout();

		Rows rows = new Rows();
		grid.appendChild(rows);

		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(plateLabel);
		plateTextBox.setReadonly(true);
		row.appendChild(plateTextBox);
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());

		row = new Row();
		rows.appendChild(row);
		row.appendChild(positionLabel);
		positionTextBox.setReadonly(true);
		row.appendChild(positionTextBox);
		row.appendChild(positionNewLabel);

		Div dpos = new Div();
		positionNewTextBox.setReadonly(true);  // deja en readonly

		dpos.appendChild(positionNewTextBox);
		if(isTrailer){
			dpos.appendChild(trailerCheckbox);
			dpos.appendChild(trailerLabel);
		}
		dpos.appendChild(rotateTires);   // check de rotacion



		row.appendChild(dpos);
		row.appendChild(serialLabel);
		serialTextBox.setReadonly(true);
		row.appendChild(serialTextBox);

		row = new Row();
		rows.appendChild(row);
		row.appendChild(kmsLabel);
		kmsTextBox.setReadonly(true);
		row.appendChild(kmsTextBox);
		row.appendChild(depthTireLabel);
		row.appendChild(depthTireTextBox);
		row.appendChild(depthBrakeLabel);
		row.appendChild(depthBrakeTextBox);

		//button row
		row = rows.newRow();
		row.setAlign("right");
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		Div divButton = new Div();
		divButton.appendChild(editButton);
		divButton.appendChild(editTireButton);
		row.appendChild(divButton);


		layout = new Borderlayout();
		layout.setWidth("100%");
		layout.setHeight("100%");
		if (!isLookup())
		{
			layout.setStyle("position: absolute");
		}
		this.appendChild(layout);

		North north = new North();
		layout.appendChild(north);
		north.appendChild(grid);

		Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		center.setHflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
		else
			contentPanel.setStyle("width: 99%; margin: 0px auto;");
		contentPanel.setVflex(true);

		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);

		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_CANCEL).setVisible(false);
		//		setWidth("600px");
		//		setHeight("350px");
		//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	}//init

	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
		if (list == null)
			return;
		Iterator<MSMJTires> itlist = list.iterator();
		while (itlist.hasNext()){
			MSMJTires obj = itlist.next();
			Vector<Object> v = new Vector<Object>();
			v.add(obj.getPlate());
			v.add(obj.getPosition());
			v.add(obj.getName());
			v.add(obj.getserial());
			v.add(obj.getvaluetire());
			v.add(obj.getvaluebrake());
			v.add(obj.getStatus());
			datat.add(v);
		}
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(datat);
		modelTable.removeTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
		//		contentPanel.addEventListener(Events.ON_CLICK, this);
		contentPanel.addActionListener(this);
		setTableColumnClass(contentPanel);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		columnNames.add(Msg.translate(Env.getCtx(), "Position"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_PRoduct_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "serial"));
		columnNames.add(Msg.translate(Env.getCtx(), "valuetire"));
		columnNames.add(Msg.translate(Env.getCtx(), "valuebrake"));
		columnNames.add(Msg.translate(Env.getCtx(), "status"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass

	@Override
	public void onEvent(Event ev) {
		try {
			if (ev.getTarget().equals(editButton)){
				int index = contentPanel.getSelectedIndex();
				
				if (index < 0) {
					Message.showWarning("SMJMSGSelectItemList", true);
					return;
				}
				
				Trx trx = Trx.get(Trx.createTrxName(), true);
				try {
					saveData(trx.getTrxName());
					trx.commit();
					list = DataQueriesTrans.getTiresList(null, vh);
					loadTable();
				} catch (Exception ex) {
					Messagebox.showDialog(ex.getMessage(), labelInfo, Messagebox.OK, Messagebox.ERROR);
					trx.rollback();
				} finally {
					trx.close();
				}
			}
			else if (ev.getTarget().equals(editTireButton)){
				String value = "llanta";
				showInfoProduct(value);
				//				loadTable();
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
			{
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				dispose(false);
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				fireEventListener(ev);
				dispose(false);
			}
			else if(ev.getTarget().equals(contentPanel)){
				Integer index = contentPanel.getSelectedIndex();

				if (index < 0) {
					return;
				}

				MSMJTires tire = list.get(index);
				positionTextBox.setValue(Integer.toString(tire.getPosition()));
				positionNewTextBox.setValue(tire.getPosition());
				serialTextBox.setValue(Integer.toString(tire.getserial()));
				kmsTextBox.setValue(tire.getkms().toString());
				depthTireTextBox.setValue(tire.getvaluetire());
				depthBrakeTextBox.setValue(tire.getvaluebrake());
			}
			else if (ev.getTarget().equals(rotateTires)){
				Integer index = contentPanel.getSelectedIndex();
				if (index < 0) {
					rotateTires.setChecked(false);
					Message.showWarning("SMJMSGSelectItemList", true);
					return;
				}
				
				isRotateTires = rotateTires.isChecked();

				if (isRotateTires)
				{
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantRotateTires"), labelInfo, Messagebox.OK | Messagebox.CANCEL, 
							Messagebox.QUESTION, new Callback<Integer>() {

						@Override
						public void onCallback(Integer result) {
							if (result == Messagebox.OK) {
								positionNewTextBox.setReadonly(false);
								isRotateTires = true;
								rotateTires.setChecked(true);
							} else {
								isRotateTires = false;
								rotateTires.setChecked(false);
								positionNewTextBox.setReadonly(true);
							}
						}
					});
				}
				else
				{
					isRotateTires = false;
					rotateTires.setChecked(false);
					positionNewTextBox.setReadonly(true);
				}

			}//rotateTires
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".onEvent :: "	, e);
		}
	}//onEvent

	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {

	}



	/**
	 * muestra la ventana de informacion de productos y trae el producto seleccionado - 
	 * show info product window and returns selected product data  
	 * @param value
	 */
	private void showInfoProduct(String value){
		try {
			Integer index = contentPanel.getSelectedIndex();
			Integer pos = 0;
			if(index != null && index >0){
				pos = list.get(index).getPosition();
			}

			int rdm = UtilTrans.getRdmInteger();
			SMJAddDelTires ip = new SMJAddDelTires(rdm, "M_Product", "M_Product_ID", false, "", qtyTires, 
					vehicleId, mechanicId, pos, woLineId); 
			ip.setVisible(true);
			ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelSearchProducts"));
			ip.setStyle("border: 2px");
			ip.setClosable(true);
			ip.setParent(this);
			ip.addValueChangeListener(listener);
			ip.addEventListener(new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					list = DataQueriesTrans.getTiresList(null, vh);
					loadTable();
				}
			});
			AEnv.showCenterScreen(ip);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".showInfoProduct - ERROR: "+e.getMessage(), e);
		}
	}//showInfoProduct  

	/**
	 * guarda la informacion de la llanta selecionada - 
	 * save data selected tire
	 * @return Boolean
	 */
	private void saveData(String trxName) throws Exception {
		Boolean onlyCheck = false;
		Integer pos2 = positionNewTextBox.getValue();
		Integer index = contentPanel.getSelectedIndex();
		// aqui revisa si huno cambio de posicion (rotacion) si es asi toma la razon de rotacion
		// si no registra que solo es una revision
		Integer reason = 0;
		if (isRotateTires) {
			reason = Integer.parseInt(MSysConfig.getValue("SMJ-REASONCHANGETIRE", Env.getAD_Client_ID(Env.getCtx())).trim());
		} else {
			reason = Integer.parseInt(MSysConfig.getValue("SMJ-REASONTIRECHECK", Env.getAD_Client_ID(Env.getCtx())).trim());
			onlyCheck = true;
		}

		MSMJTires tire = list.get(index);

		tire.set_TrxName(trxName);
		Integer pos1 = tire.getPosition();
		BigDecimal brakeT2 = Env.ZERO;
		BigDecimal tireT1 = tire.getvaluetire();
		tire.setserial(tire.getserial()*-1);
		tire.setPosition(tire.getPosition()*-1);
		tire.saveEx();
		if (pos2 != null && pos2>0 && isRotateTires){
			MSMJTires p2 = null;

			if(isTrailer && trailerCheckbox.isChecked() && vh.getasociatedtrucktrailer_ID()>0){
				p2 = DataQueriesTrans.getTireData(vh.getasociatedtrucktrailer_ID(), pos2);
				p2.setsmj_vehicle_ID(vehicleId);
				tire.setsmj_vehicle_ID(vh.getasociatedtrucktrailer_ID());
			}else{
				p2 = DataQueriesTrans.getTireData(vehicleId, pos2);
			}

			if (p2 == null){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGNoTireInPosition"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}

			SaveHistory.saveHistorytire(trxName, tire, reason);   // historico de llanta anterior
			SaveHistory.saveHistorytire(trxName, p2, reason);   // historico de llanta nueva
			p2.setC_BPartner_ID(mechanicId);

			if(woLineId>0){
				p2.setsmj_workOrderLine_ID(woLineId);
			}

			brakeT2 = p2.getvaluebrake();
			p2.set_TrxName(trxName);
			p2.setPosition(pos1);
			p2.setvaluebrake(depthBrakeTextBox.getValue());
			p2.setvaluetire(depthTireTextBox.getValue());
			p2.saveEx();
			tire.setPosition(pos2);
			tire.setvaluebrake(brakeT2);
			tire.setvaluetire(tireT1);
		}//if newPos
		else{
			tire.setvaluetire(depthTireTextBox.getValue());
			tire.setvaluebrake(depthBrakeTextBox.getValue());
		}

		tire.setC_BPartner_ID(mechanicId);

		if(woLineId>0){
			tire.setsmj_workOrderLine_ID(woLineId);
		}

		if(tire.getserial() < 0){
			tire.setserial(tire.getserial()*-1);
		}

		if (tire.getPosition()<0){
			tire.setPosition(tire.getPosition()*-1);
		}

		tire.saveEx();

		if (onlyCheck) {
			SaveHistory.saveHistorytire(trxName, tire, reason);   // historico de llanta si solo es un checkeo
		}

		depthTireTextBox.setValue(Env.ZERO);
		depthBrakeTextBox.setValue(Env.ZERO);
		serialTextBox.setValue("");
		positionNewTextBox.setValue(0);
		isRotateTires = false;
		rotateTires.setChecked(false);
		positionNewTextBox.setReadonly(true);
		positionTextBox.setValue("");
		trailerCheckbox.setChecked(false);
	}//saveData

	/**
	 * bloquea o desbloquea campos - 
	 * lock or unlock fields.
	 */
	private void lockFields(){
		editButton.setEnabled(!isCompleted);
		editTireButton.setEnabled(!isCompleted);
		//		kmsTextBox.setReadonly(isCompleted);
		positionNewTextBox.setReadonly(true);
		trailerCheckbox.setDisabled(isCompleted);
		depthBrakeTextBox.setReadonly(isCompleted);
		depthTireTextBox.setReadonly(isCompleted);
	}//lockFields


	public void addEventListener(EventListener<Event> eventListener) {

		if (eventListener == null) {
			return;
		}

		listeners.add(eventListener);
	}

	private void fireEventListener(Event event) throws Exception {
		for (EventListener<Event> listener : listeners)
		{
			listener.onEvent(event);
		}
	}
}//SMJInfoTires
