package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.model.MRequest;
import org.compiere.model.MRequestType;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.smj.model.InfoProductList;
import com.smj.model.MSMJScheduleRules;
import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJVehicle;
import com.smj.model.MSMJWorkOrderLine;
import com.smj.util.DataQueriesTrans;

/**
 * @version
 *          <li>SmartJSP: WSimplifiedSales, 2013/05/14
 *          <ul TYPE ="circle">
 *          <li>Clase crea la orden de trabajo para una regla
 *          <li>Class create work order by rule
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class CreateWOProgram {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	private Integer stateProgram = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOPROGRAM", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer requestType = Integer.parseInt(MSysConfig.getValue("SMJ-REQUESTTYPE", Env.getAD_Client_ID(Env.getCtx())).trim());

	/**
	 * crea la la orden de trabajo de programacion - create programming work
	 * order
	 * 
	 * @param trxName
	 * @param vehicleId
	 * @param acts
	 * @return
	 */
	public String saveRequest(String trxName, MSMJScheduleRules rule, Integer vehicleId, LinkedList<Integer> acts) throws Exception {
		String flag = null;
		// inico programacion...");
		MRequest tmpRequest = new MRequest(Env.getCtx(), 0, trxName);
		MSMJVehicle vh = new MSMJVehicle(Env.getCtx(), vehicleId, trxName);
		MSMJServiceRateGroup sg = new MSMJServiceRateGroup(Env.getCtx(), vh.getsmj_servicerategroup_ID(), trxName);
		String temparioName = sg.getName();
		tmpRequest.set_ValueOfColumn("smj_plate", vh.getsmj_plate());
		tmpRequest.set_ValueOfColumn("smj_vehicle_ID", vh.getSMJ_Vehicle_ID());
		Integer bpartnerId = DataQueriesTrans.getVehicleOwnerId(vh.getsmj_plate());
		tmpRequest.setC_BPartner_ID(bpartnerId);
		tmpRequest.set_ValueOfColumn("c_bpartnercustomer_ID", bpartnerId);
		tmpRequest.setR_Status_ID(stateProgram);
		tmpRequest.setR_RequestType_ID(requestType);
		tmpRequest.setDueType("7");
		tmpRequest.setPriorityUser("5");
		tmpRequest.setPriority("5");
		tmpRequest.setConfidentialTypeEntry("I");
		tmpRequest.setConfidentialType("I");
		tmpRequest.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));

		/// datos programacion
		tmpRequest.set_ValueOfColumn("smj_isprogrammingnext", true);
		tmpRequest.set_ValueOfColumn("periodicity", rule.getperiodicity());
		tmpRequest.set_ValueOfColumn("valueperiodicity", rule.getvalueperiodicity());
		tmpRequest.set_ValueOfColumn("smj_ScheduleRules_ID", rule.getsmj_ScheduleRules_ID());

		if (rule.getperiodicity().equals("K")) {
			BigDecimal kms = vh.getkms().add(rule.getvalueperiodicity());
			tmpRequest.set_ValueOfColumn("kmActual", kms);
			tmpRequest.setSummary(rule.getName() + " - Kms: " + kms);
		} else if (rule.getperiodicity().equals("T")) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR, rule.getvalueperiodicity().intValue());
			Date nextDate = cal.getTime();
			tmpRequest.setDateNextAction(new Timestamp(nextDate.getTime()));
			tmpRequest.set_ValueOfColumn("kmActual", vh.getkms());
			tmpRequest.setSummary(rule.getName() + " - Tiempo: " + nextDate);
		}

		MRequestType rt = new MRequestType(Env.getCtx(), requestType, trxName);
		Integer seq = rt.get_ValueAsInt("Sequence") + 1;
		tmpRequest.setDocumentNo(seq.toString());
		tmpRequest.set_ValueOfColumn("smj_sequence", seq.toString());
		rt.set_ValueOfColumn("Sequence", seq);
		rt.saveEx();
		
		tmpRequest.saveEx();
		flag = tmpRequest.getDocumentNo();
		
		Iterator<Integer> ita = acts.iterator();
		while (ita.hasNext()) {
			Integer lcode = ita.next();
			Boolean lok = createWoLines(trxName, tmpRequest.getR_Request_ID(), lcode, temparioName);
			if (!lok)
				return null;
		} // while
		
		return flag;
	}// saveRequest

	/**
	 * crea una linea a la orden de trabajo de programacion - create programming
	 * work order line
	 * 
	 * @param trxName
	 * @param requestId
	 * @param code
	 * @param temparioName
	 * @return
	 */
	private Boolean createWoLines(String trxName, Integer requestId, Integer code, String temparioName) throws Exception {
		Boolean flag = true;
		
		// inicio de lineas..");
		MProduct p = MProduct.get(Env.getCtx(), code);
		InfoProductList dato = null;
		SMJSearchProducts search = new SMJSearchProducts();
		
		String locatorBranch = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"))).getName();
		LinkedList<InfoProductList> listProd = search.searchProducts_Locator(p.getValue(), locatorBranch, true, temparioName);
		
		if (listProd.size() == 1) {
			Iterator<InfoProductList> itl = listProd.iterator();
			dato = itl.next();
		} else {
			return true;
		}
		
		MSMJWorkOrderLine ipl = new MSMJWorkOrderLine(Env.getCtx(), 0, trxName);
		ipl.setM_Product_ID(dato.getProductID());
		ipl.setPrice(dato.getPrice());
		ipl.setlocator_ID(dato.getLocatorId());
		ipl.setprovider_ID(dato.getPartnerId());
		ipl.setQty(dato.getQty());
		ipl.setpurchasePrice(dato.getPurchasePrice());
		ipl.setsmj_estimatedtime(dato.getEstimedTime());
		ipl.setsmj_isallowchanges(dato.getIsAllowChange());
		ipl.setProductValue(p.getValue());
		ipl.setProductName(p.getName());

		BigDecimal tax = DataQueriesTrans.getTaxValue(p.getC_TaxCategory_ID());
		if (tax != null) {
			ipl.settaxvalue(tax);
		}

		ipl.setQty(Env.ONE);
		Integer attInstance = DataQueriesTrans.getAttInstanceId(ipl.getlocator_ID(), ipl.getM_Product_ID());
		ipl.setM_AttributeSetInstance_ID(attInstance);
		ipl.settotal(ipl.getQty().multiply(ipl.getPrice()));

		Boolean isService = p.getProductType().equals("S") ? true : false;
		ipl.setsmj_isservice(isService);
		if (isService) {
			ipl.setwolstate("S");
		}

		ipl.setsmj_iscommission(p.get_ValueAsBoolean("smj_iscommission"));
		ipl.setdateopen(Env.getContextAsDate(Env.getCtx(), "#Date"));
		MLocator loc = MLocator.get(Env.getCtx(), ipl.getlocator_ID());
		MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
		ipl.setWarehouseName(wh.getName());
		ipl.setR_Request_ID(requestId);
		ipl.saveEx();

		return flag;
	}// createWoLines

	/**
	 * regresa el listado de actividades de la regla - return activity list for
	 * rule
	 * 
	 * @param value
	 * @return
	 */
	public LinkedList<Integer> getActivity(Integer value) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT M_Product_ID FROM smj_activityRules ");
		sql.append(" WHERE isactive = 'Y' AND smj_ScheduleRules_ID =" + value + " ");
		LinkedList<Integer> codes = new LinkedList<Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {

			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Integer code = rs.getInt("M_Product_ID");
				// M_Product_ID "+code);
				codes.add(code);
			} // while
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return codes;
	}// getActivity

}// CreateWOProgram
