package com.smj.webui.component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MProduct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013/05/06
 *          <ul TYPE ="circle">
 *          <li>Clase ventana emergente para mostrar el listado lineas para garantia y seleccionar una
 *          <li>Class float window to show item garranty list list and select one
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoGarrantyLine extends InfoPanel implements EventListener<Event>, WTableModelListener{
	
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 935516751282073726L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();
	private Integer woCode = 0;
	private Callback<String> _callback; //iMarch
	
	LinkedList<HashMap<String, Object>> list = null;

	/**
	 * SMJInfoWorkOrder constructor.
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param callback //modificate by iMarch
	 */
	public SMJInfoGarrantyLine(int WindowNo, String tableName, String keyColumn,
			boolean multipleSelection, String whereClause, Integer wo_Code, String docNo, Callback<String> callback) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
//		System.out.println(this.getClass().getCanonicalName()+".SMJInfoWorkOrder plate.."+wo_Code);
		woCode = wo_Code;
		_callback = callback;
		init();
		fieldValue.setValue(docNo);
		getDataList(null, woCode);
		loadTable();
	}//SMJInfoPlate

	/**
	 * ubica elementos en la ventana - locates elements in info window
	 */
	private void init()
	{
		fieldValue.setWidth("50%");
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
	
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(fieldLabel);
		fieldValue.setReadonly(true);
		row.appendChild(fieldValue);
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
        
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setFlex(true);
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		setWidth("500px");
//		setHeight("350px");
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	}//init
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
		Iterator<HashMap<String, Object>> itlist = list.iterator();
		while (itlist.hasNext()){
			HashMap<String, Object> obj = itlist.next();
			Vector<Object> v = new Vector<Object>();
			v.add(obj.get("VALUE"));
			v.add(obj.get("NAME"));
			v.add(obj.get("SUMMARY"));
			datat.add(v);
		}
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(datat);
		modelTable.removeTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
		contentPanel.setMultiSelection(true);
//		contentPanel.addActionListener(this);
//		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		setTableColumnClass(contentPanel);
		contentPanel.setFocus(true);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "value"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProductService"));
		columnNames.add(Msg.translate(Env.getCtx(), "summary"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true); 
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	@Override
	public void onEvent(Event ev) {
//		super.onEvent(event);
		try {
			if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				int[] indexs = contentPanel.getSelectedIndices();
				if(indexs == null || indexs.length <= 0){ //
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectLeastOneItem"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				String codes = "";
				for (int i=0; i<indexs.length;i++){
					HashMap<String, Object> obj = list.get(indexs[i]);
					Integer code = (Integer) obj.get("CODE");
					codes = codes + code.toString()+",";
				}

				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "WOLCODES", codes);
				_callback.onCallback(codes);
				dispose(false);
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				Env.setContext(Env.getCtx(), p_WindowNo, "WOLCODES", "");
	            dispose(false);
	        }
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".onEvent ERROR:: "+e.getMessage(), e);
		}
	}//onEvent
	
	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
		
	}

	/**
	 * regresa la lista de solicitudes - return request list
	 * @param trxName
	 * @param value
	 * @return LinkedList<HashMap<String, Object>>
	 */ 
	private void getDataList(String trxName, Integer value) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT l.smj_workOrderLine_ID, l.M_Product_ID, l.observations ");
		sql.append(" FROM smj_workOrderLine l, M_Product p WHERE smj_isWarrantyApplied ='N' ");
		sql.append(" AND l.M_Product_ID = p.M_Product_ID AND p.producttype = 'S' ");
		sql.append("AND R_Request_ID = "+value+" ");
//		System.out.println(this.getClass().getCanonicalName()+".getDataList SQL::"+sql.toString());
		
		list = new LinkedList<HashMap<String, Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Integer prodID = rs.getInt("M_Product_ID");
				MProduct p = MProduct.get(Env.getCtx(), prodID);
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("CODE", rs.getInt("smj_workOrderLine_ID"));
				obj.put("VALUE", p.getValue());
				obj.put("NAME", p.getName());
				obj.put("SUMMARY", rs.getString("observations"));
				list.add(obj);
			}// if
			
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getDataList - SQL: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return ;
	}// getDataList
	

}//SMJInfoGarrantyLine
