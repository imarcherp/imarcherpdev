package com.smj.webui.component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListCell;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: SMJInfoProduct, 2013/04/06
 *          <ul TYPE ="circle">
 *          <li>Ventana modal de informacion de placas
 *          <li>Modal Window for info plate
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoPlate  extends InfoPanel implements EventListener<Event>, WTableModelListener{
	
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 935516751282073726L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();

	public SMJInfoPlate(int WindowNo, String tableName, String keyColumn,
			boolean multipleSelection, String whereClause, Integer bPartnerID) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		init();
		LinkedList<String> list = DataQueriesTrans.getPlateList(null, bPartnerID);
		loadTable(list);
	}//SMJInfoPlate

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		fieldValue.setWidth("50%");
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
	
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());

        
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		setWidth("200px");
		setHeight("350px");
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	}//init
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(LinkedList<String> list){
		Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
		Iterator<String> itlist = list.iterator();
		while (itlist.hasNext()){
			String plate = itlist.next();
			Vector<Object> v = new Vector<Object>();
			v.add(plate);
			datat.add(v);
		}
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(datat);
		modelTable.removeTableModelListener(this);
//		model.addTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
		contentPanel.removeEventListener(Events.ON_DOUBLE_CLICK, this);
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		setTableColumnClass(contentPanel);
		contentPanel.setFocus(true);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);   
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	@Override
	public void onEvent(Event ev) {
//		super.onEvent(event);
		try {
			if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Integer index = contentPanel.getSelectedIndex();
				if (index == null || index < 0){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectItemList"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				List<Component> items = contentPanel.getSelectedItem().getChildren();
				ListCell dato0 = (ListCell) items.get(0);
				String plate = (String) dato0.getValue();
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "PLATE", plate);
				dispose(false);
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				Env.setContext(Env.getCtx(), p_WindowNo, "PLATE", "");
	            dispose(false);
	        }
			else if (ev.getTarget().equals(contentPanel)){
				List<Component> items = contentPanel.getSelectedItem().getChildren();
				ListCell dato0 = (ListCell) items.get(0);
				String plate = (String) dato0.getValue();
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "PLATE", plate);
				dispose(false);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".onEvent :: "	, e);
		}
	}//onEvent
	
	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
		
	}

}//SMJInfoPlate
