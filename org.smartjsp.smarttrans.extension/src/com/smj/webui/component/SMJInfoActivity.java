package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;
import com.smj.util.DataQueriesTrans;
import com.smj.util.UtilTrans;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013-04-25
 *          <ul TYPE ="circle">
 *          <li>Clase ventana emergente para actualizar la informacion de actividades
 *          <li>Class float window to update activity information
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoActivity extends InfoPanel implements EventListener<Event>, WTableModelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3729208262465567749L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox activityTextbox = new Textbox();
	private Label activityLabel = new Label();
	private Textbox descTextbox = new Textbox();
	private Label descLabel = new Label();
	private Textbox mechanicTextbox = new Textbox();
	private Listbox mechanicListbox = new Listbox();
	private Label mechanicLabel = new Label();
	private Textbox invoiceTextbox = new Textbox();
	private Label invoiceLabel = new Label();
	private LinkedList<MSMJWoMechanicAssigned> listMec = new LinkedList<MSMJWoMechanicAssigned>();
	private LinkedList<MInvoiceLine> listInv = new LinkedList<MInvoiceLine>();
	private Boolean isExternal = false;
	private Button deleteButton = new Button (); 
	private Button editButton = new Button (); 
	private ValueChangeListener listener = null;
	private MProduct product = null;
	private MSMJWorkOrderLine woLine = null; 
	private Integer invoiceId = 0;
	private Integer index = -1;
	private Integer invoiceLineId = 0;
	private Boolean isComplete = false;
	private Label isExternalLabel = new Label();
	private Checkbox isExternalChk = new Checkbox();
	private Boolean isTires = false;
	private Button tiresButton = new Button ();
	private Integer vehicleId = 0;
	private Boolean tiresOk = false; 
	private String selectedPlate  = "";
	private List<EventListener<Event>> listeners = new ArrayList<EventListener<Event>>();

	//	private WTableDirEditor mechanicPick = null;

	private BigDecimal maxFee = new BigDecimal(MSysConfig.getValue("SMJ-MAXMECHANICFEE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();

	/**
	 * SMJInfoProduct constructor
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param listVal
	 * @param searchService
	 */
	public SMJInfoActivity(int WindowNo, String tableName, String keyColumn, Boolean multipleSelection, 
			String whereClause, ValueChangeListener m_listener,Integer productId, MSMJWorkOrderLine wo_Line,
			Boolean complete, Integer vehicle, String plate) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		listener = m_listener;
		product = MProduct.get(Env.getCtx(), productId);
		woLine = wo_Line;
		isExternal = woLine.issmj_isexternallservice();
		isComplete = complete;
		vehicleId = vehicle;
		selectedPlate = plate;
		MProduct p = MProduct.get(Env.getCtx(), woLine.getM_Product_ID());
		isTires = p.get_ValueAsBoolean("smj_istires");
		listMechanic();
		init();
		loadData();
		lockFields();
		showLogic();		
	}//constructor

	/**
	 * carga los datos de la actividad - 
	 * load activity data
	 */
	private void loadData(){
		try {
			invoiceId = woLine.getC_Invoice_ID();
			descTextbox.setValue(woLine.getobservations());
			activityTextbox.setValue(product.getName());
			isExternalChk.setChecked(isExternal);
			if(isExternal){
				MInvoice inv = MInvoice.get(Env.getCtx(), invoiceId);
				invoiceTextbox.setValue(inv.getDocumentNo());
				//				System.out.println(this.getClass().getName()+".LoadData --invLine--"+woLine.getC_InvoiceLine_ID());
				if (woLine.getC_InvoiceLine() != null && woLine.getC_InvoiceLine_ID() > 0){
					invoiceLineId = woLine.getC_InvoiceLine_ID();
				}
				getInvoiceLines(invoiceId);
				loadTableInv();
				if (index >= 0){
					contentPanel.setSelectedIndex(index);
				}
			}else{
				listMec = DataQueriesTrans.getMechanicList(woLine.getsmj_workOrderLine_ID());
				loadTable(null);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".loadData - ERROR:: "+e.getMessage(), e);
		}
	}//loadData

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		activityLabel.setText("" + Msg.translate(Env.getCtx(), "smj_ActivityRequest_ID"));
		isExternalLabel.setText("" + Msg.translate(Env.getCtx(), "smj_isexternallService"));
		//		isExternalChk.addEventListener(Events.ON_CHECK, this);
		isExternalChk.setText("" + Msg.translate(Env.getCtx(), "smj_isexternallService"));
		isExternalChk.addActionListener(this);
		descLabel.setText("" + Msg.translate(Env.getCtx(), "summary"));
		mechanicLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelMechanic"));
		mechanicTextbox.addEventListener(Events.ON_BLUR, this);
		invoiceLabel.setText("" + Msg.translate(Env.getCtx(), "C_Invoice_ID"));
		invoiceTextbox.addEventListener(Events.ON_BLUR, this);
		deleteButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelDelete"));
		deleteButton.addActionListener(this);
		editButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelActivityOk"));
		editButton.addActionListener(this);
		tiresButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelChangingTires"));
		tiresButton.addActionListener(this);

		Grid grid = GridFactory.newGridLayout();

		Rows rows = new Rows();
		grid.appendChild(rows);

		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(activityLabel);
		activityTextbox.setReadonly(true);
		activityTextbox.setWidth("200px");
		Div divChk = new Div();
		divChk.appendChild(activityTextbox);
		//			divChk.appendChild(isExternalLabel);
		divChk.appendChild(isExternalChk);
		row.appendChild(divChk);

		row = new Row();
		rows.appendChild(row);
		row.appendChild(descLabel);
		descTextbox.setCols(40);
		descTextbox.setRows(3);
		row.appendChild(descTextbox);

		//		if(isExternal){
		row = new Row();
		rows.appendChild(row);
		row.appendChild(invoiceLabel);
		invoiceTextbox.setWidth("200px");
		row.appendChild(invoiceTextbox);
		//		}else{
		row = new Row();
		rows.appendChild(row);
		row.appendChild(mechanicLabel);
		//			mechanicTextbox.setWidth("200px");
		row.appendChild(mechanicListbox);
		//		}

		//			row = rows.newRow();
		//			row.appendChild(mechanicPick.getComponent());

		row = rows.newRow();
		row.setAlign("right");
		row.appendChild(new Label());
		Div divButton = new Div();
		divButton.appendChild(deleteButton);
		divButton.appendChild(editButton);
		if(isTires){
			divButton.appendChild(tiresButton);
		}
		row.appendChild(divButton);

		layout = new Borderlayout();
		layout.setWidth("100%");
		layout.setHeight("100%");
		if (!isLookup())
		{
			layout.setStyle("position: absolute");
		}
		this.appendChild(layout);

		North north = new North();
		layout.appendChild(north);
		north.appendChild(grid);

		Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
		else
			contentPanel.setStyle("width: 99%; margin: 0px auto;");
		contentPanel.setVflex(true);

		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);

		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		setWidth("600px");
	}//init

	@Override
	protected String getSQLWhere() {
		return null;
	}//getSQLWhere

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
	}//setParameters

	/**
	 * Carga la informacion en la tabla de mecanicos
	 * load table with machanic info 
	 */
	private void loadTable(MSMJWoMechanicAssigned ww){
		if (listMec == null){
			listMec = new LinkedList<MSMJWoMechanicAssigned>();
		}
		try {
			BigDecimal accFee = Env.ZERO;
			Iterator<MSMJWoMechanicAssigned> it = listMec.iterator();
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			String com = "-";
			while (it.hasNext()){
				MSMJWoMechanicAssigned ipl = it.next();
				if(ipl != null){
					if (ww!=null && ipl.getC_BPartner_ID()==ww.getC_BPartner_ID()){
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGItemListExist"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}
					com = "-";
					if(ipl.issmj_iscommission()){
						accFee = accFee.add(ipl.getappliedfee());
						com = "X";
					}
					if (accFee.compareTo(maxFee)>0){
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGMechanicFeeExceeds"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						listMec.remove(ipl);
						return;
					}
					Vector<Object> rc = getTableElement(ipl, com);
					data.add(rc);
				}//(ipl != null)

			}//while

			if(ww != null){
				com = "-";
				if(ww.issmj_iscommission()){
					accFee = accFee.add(ww.getappliedfee());
					com = "X";
				}
				if (accFee.compareTo(maxFee) > 0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGMechanicFeeExceeds"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					//					list.remove(ww);
					return;
				}
				listMec.add(ww);
				Vector<Object> rc = getTableElement(ww, com);
				data.add(rc);
			}

			Vector<String> columnNames = getTableColumnNames();
			contentPanel.clear();
			//  Set Model
			ListModelTable modelTable = new ListModelTable(data);
			contentPanel.setData(modelTable, columnNames);
			setTableColumnClass(contentPanel);
			
			for (Component component : contentPanel.getListHead().getChildren()) {
				ListHeader header = (ListHeader) component;
				header.setHflex("max");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".loadTable - ERROR:: "+e.getMessage(), e);
		}
	}//loadTable

	/**
	 * crea un elemento de tipo tabla - create table element
	 * @param ipl
	 * @param com
	 * @return
	 */
	private Vector<Object> getTableElement(MSMJWoMechanicAssigned ipl, String com){
		MBPartner par = MBPartner.get(Env.getCtx(), ipl.getC_BPartner_ID());
		Vector<Object> rc = new Vector<Object>();
		rc.add(par.getTaxID());
		rc.add(par.getName());
		rc.add(com);
		rc.add(ipl.getappliedfee());

		return rc;
	}//getTableElement

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		if(isExternal){
			columnNames.add(Msg.translate(Env.getCtx(), "M_Product_Id"));
			columnNames.add(Msg.translate(Env.getCtx(), "Description"));
			columnNames.add(Msg.translate(Env.getCtx(), "QtyEntered"));
			columnNames.add(Msg.translate(Env.getCtx(), "PriceEntered"));
			columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		}else{
			columnNames.add(Msg.translate(Env.getCtx(), "taxId"));
			columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelMechanic"));
			columnNames.add(Msg.translate(Env.getCtx(), "smj_iscommission"));
			columnNames.add(Msg.translate(Env.getCtx(), "smj_commissionValue"));
		}
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		if(isExternal){
			table.setColumnClass(i++, BigDecimal.class, true);
			table.setColumnClass(i++, String.class, true);
		}else{
			table.setColumnClass(i++, String.class, true);
		}
		//  Table UI
		table.autoSize();
	}//setTableColumnClass

	@Override
	public void onEvent(Event ev) {
		//		super.onEvent(event);
		//		System.out.println(this.getClass().getCanonicalName()+".evento...."+ev.getName());
		try {
			if(ev.getTarget().equals(isExternalChk)){
				//				System.out.println("por check.....");
				isExternal = isExternalChk.isChecked();
				//				System.out.println("chk...... "+isExternal);
				showLogic();
			}			
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Boolean ok = saveData("A");
				if(ok){
					Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
					Env.setContext(Env.getCtx(), p_WindowNo, "TIRESOK", tiresOk.toString());
					fireEventListener(ev);
					dispose(false);					
				}
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
			{
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				Env.setContext(Env.getCtx(), p_WindowNo, "TIRESOK", tiresOk.toString());
				fireEventListener(ev);
				dispose(false);
			}
			else if (ev.getTarget().equals(editButton))
			{
				if (product!=null && product.get_ValueAsBoolean("smj_istires") && !tiresOk){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGDoTiresActivity"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				
				final Event finalEvent = ev;
				org.adempiere.webui.component.Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSureActivityComplete"), labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, 
						new Callback<Integer>() {

					@Override
					public void onCallback(Integer result) {
						if (result == Messagebox.OK){
							Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
							Env.setContext(Env.getCtx(), p_WindowNo, "TIRESOK", tiresOk.toString());
							Boolean ok = saveData("C");
							if(ok){
								try {
									fireEventListener(finalEvent);
								} catch (Exception e) {
									e.printStackTrace();
								}
								dispose(false);
							}
						}//response Ok								
					}
				});
				return;
			}
			else if (ev.getTarget().equals(invoiceTextbox)){
				String field = invoiceTextbox.getValue();
				if (field.length()<= 0 || field.equals("") || field.equals(" "))
					return;
				showInfoInvoice(field);
				mechanicTextbox.setText("");
			}
			else if(ev.getTarget().equals(tiresButton)){
				//				System.out.println("Boton de llantas,....."+vehicleId);
				showInfoTires();
			}
			else if(ev.getTarget().equals(deleteButton)){
				//				System.out.println(this.getClass().getCanonicalName()+"delete button");
				deleteData();
			}
			else if (ev.getTarget().equals(mechanicListbox)){
				Integer mec =  (Integer) mechanicListbox.getSelectedItem().getValue();
				//				System.out.println(this.getClass().getName()+".onEvent lista mecanico.."+mec);
				if(mec == null || mec <=0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelecMechanicList"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				asignateMechanic(mec);
				mechanicListbox.setValue(0);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".onEvent :: "	, e);
		}
	}//onEvent

	/**
	 * Borra el dato de mecanico o factura - 
	 * delete invoice or mechanic data
	 */
	private void deleteData(){
		try {
			Integer index = contentPanel.getSelectedIndex();
			if (index != null && index >= 0){
				//				s
				if(isExternal){
					invoiceTextbox.setValue("");
					//					System.out.println(this.getClass().getName()+".LoadData --invLine--"+woLine.getC_InvoiceLine_ID());
					invoiceLineId = 0;
					invoiceId = 0 ;
					woLine.setC_InvoiceLine_ID(0);
					woLine.setC_Invoice_ID(0);
					woLine.save();
					getInvoiceLines(0);
					loadTableInv();
				}else{
					MSMJWoMechanicAssigned mec = listMec.get(index);
					listMec.remove(mec);
					mec.delete(true);
					loadTable(null);
				}
			}else{
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectItemList"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".onEvent ERROR:: ", e);
		}
	}//deleteData

	/**
	 * asigna el mecanico seleccionado a la lista - 
	 * asignate selected mechanic to list
	 * @param iPartnerId
	 */
	private void asignateMechanic(Integer iPartnerId){
		//		System.out.println(this.getClass().getName()+".asignateMechanic ..mec ..."+iPartnerId);
		try {
			if(iPartnerId >0){
				MBPartner partner = MBPartner.get(Env.getCtx(), iPartnerId);
				MSMJWoMechanicAssigned wma = new MSMJWoMechanicAssigned(Env.getCtx(), 0, null);
				wma.setC_BPartner_ID(iPartnerId);
				wma.setsmj_workOrderLine_ID(woLine.getsmj_workOrderLine_ID());
				String fee = partner.get_ValueAsString("smj_commissionvalue");
				try {
					wma.setappliedfee(new BigDecimal(fee));
				} catch (Exception e) {
					wma.setappliedfee(Env.ZERO);
				}
				Boolean isFee = partner.get_ValueAsBoolean("smj_iscommission");
				wma.setsmj_iscommission(isFee);
				//				list.add(wma);
				//				wma.save();
				loadTable(wma);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName() + ".asignateMechanic - ERROR: " + e.getMessage(), e);
		}
	}//asignateMechanic

	/**
	 * muestra la ventana de informacion de terceros y trae el tercero seleccionado - 
	 * show info partner window and returns selected partner data  
	 * @param value
	 */
	@SuppressWarnings("unused")
	private void showInfoPartner(String value){
		try {
			Integer iPartnerId = DataQueriesTrans.getMechanicByTaxId(Env.getAD_Client_ID(Env.getCtx()), value);
			if (iPartnerId!=null && iPartnerId>0){ //primer if
				//				bpartnerId = iPartnerId;
			}else{
				iPartnerId = DataQueriesTrans.getMechanicByName(Env.getAD_Client_ID(Env.getCtx()), value.trim());
				if (iPartnerId!=null && iPartnerId>0){ //segundo if
					//					bpartnerId = iPartnerId;
				}else{
					int rdm = UtilTrans.getRdmInteger();
					Env.setContext(Env.getCtx(), rdm, Env.TAB_INFO, "C_BPartner_ID", "");
					SMJInfoBPartnerPanel ip = new SMJInfoBPartnerPanel(value.toUpperCase().trim(), rdm, true, false, "", "true");
					ip.setVisible(true);
					ip.setTitle(Msg.getMsg(Env.getCtx(), "Info Tercero"));
					ip.setStyle("border: 2px");
					ip.setClosable(true);
					ip.setAttribute("mode", "modal");
					ip.addValueChangeListener(listener);
					AEnv.showWindow(ip);
					String spartner  = Env.getContext(Env.getCtx(), rdm, Env.TAB_INFO, "C_BPartner_ID").trim();

					try {
						if(spartner==null || spartner.equals("")){
							iPartnerId = 0;
						}else{
							iPartnerId = Integer.parseInt(spartner);
						}
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName()
								+ ".showInfoPartner - ERROR: " + e.getMessage(), e);
					}
				}//segundo if 
			}//primer if
			asignateMechanic(iPartnerId);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".showInfoPartner ERROR:: ", e);
		}

	}//showInfoPartner

	/**
	 * guarda los datos de la forma.
	 * Save data form
	 * @return
	 */
	private Boolean saveData(String state){
		Boolean flag = true;
		try {
			if (isExternal){
				if (invoiceId>0){
					woLine.setC_Invoice_ID(invoiceId);
					Integer index = contentPanel.getSelectedIndex();
					if (index != null && index >= 0){
						MInvoiceLine inv = listInv.get(index);
						woLine.setC_InvoiceLine_ID(inv.getC_InvoiceLine_ID());
						//TODO VALOR 
						MProduct p = MProduct.get(Env.getCtx(), inv.getM_Product_ID());
						BigDecimal util = new BigDecimal(p.get_ValueAsString("smj_utility"));
						BigDecimal price = inv.getPriceEntered();
						BigDecimal qtyinv = inv.getQtyInvoiced();
						BigDecimal tax = DataQueriesTrans.getTaxValue(p.getC_TaxCategory_ID());
						BigDecimal salesPrice = UtilTrans.calculateSalesPrice(price, util, tax);
						woLine.setPrice(salesPrice);
						woLine.setQty(qtyinv);
						woLine.settotal(salesPrice.multiply(qtyinv));
					}else{
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectInvoiceLine"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
				}else{
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectInvoice"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
			}else{
				if (contentPanel.getItemCount()<=0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelecMechanic"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
				Iterator<MSMJWoMechanicAssigned> it = listMec.iterator();
				while (it.hasNext()){
					try {
						MSMJWoMechanicAssigned ipl = it.next();
						ipl.save();
					} catch (Exception e) {
						log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".saveData - ERROR :: "+e.getMessage(), e);
					}
				}//while
				woLine.setC_InvoiceLine_ID(0);
				woLine.setC_Invoice_ID(0);
			}
			woLine.setsmj_isexternallservice(isExternal);
			woLine.setwolstate(state);
			woLine.setobservations(descTextbox.getValue());
			woLine.saveEx();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".saveData - ERROR:: "+e.getMessage(), e);
			flag = false;
		}
		return flag;
	}//saveData

	/**
	 * muestra la ventana para seleccionar la factura - 
	 * show invoice selectwindow
	 */
	private void showInfoInvoice(String value){
		final int rdm = UtilTrans.getRdmInteger();
		SMJInfoInvoice ip = new SMJInfoInvoice(rdm, "R_Request", "R_Request", false, "", value , woLine.getM_Product_ID(), selectedPlate);
		ip.setVisible(true);
		ip.setTitle(Msg.getMsg(Env.getCtx(), "C_Invoice_ID"));
		ip.setStyle("border: 2px");
		ip.setClosable(true);
		ip.setParent(this);
		ip.addValueChangeListener(listener);

		ip.addEventListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				try {
					String invCode = Env.getContext(Env.getCtx(), rdm, "INVCODE");
					invoiceId = Integer.parseInt(invCode);
					MInvoice inv = MInvoice.get(Env.getCtx(), invoiceId);
					invoiceTextbox.setValue(inv.getDocumentNo());
					getInvoiceLines(invoiceId);
					loadTableInv();
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()+".showInfoInvoice ERROR:: ", e);
				}
			}
		});

		AEnv.showCenterScreen(ip);

		return ;
	}//showInfoInvoice

	/**
	 * carga la lista lineas de la factura - 
	 * load invoice lines list
	 * @param code
	 */
	private void getInvoiceLines(Integer code){
		listInv = new LinkedList<MInvoiceLine>();
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID = "+code+" ");
		sql.append(" AND M_Product_ID = "+woLine.getM_Product_ID()+" ");
		sql.append(" AND (smj_plate = '"+selectedPlate.toUpperCase().trim()+"'  OR smj_plate IS NULL)");
		sql.append(" AND C_InvoiceLine_ID NOT IN (SELECT C_InvoiceLine_ID FROM smj_workorderline ");
		sql.append("WHERE C_InvoiceLine_ID IS NOT NULL AND C_InvoiceLine_ID <> "+invoiceLineId+")");
		//		System.out.println(this.getClass().getCanonicalName()+".getInvoiceLines SQL::"+sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			MInvoiceLine line = null;
			while (rs.next ())	{
				Integer id = rs.getInt("C_InvoiceLine_ID");
				line = new MInvoiceLine(Env.getCtx(),id,null);
				listInv.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, this.getClass().getCanonicalName()+".getInvoiceLines - SQL: "+ sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getInvoiceLines

	/**
	 * carga informacion de lineas de facturas en la tabla - 
	 * load invoice line info in table
	 */
	private void loadTableInv(){
		if (listInv == null){
			listInv = new LinkedList<MInvoiceLine>();
		}
		try {
			int i = 0;
			Iterator<MInvoiceLine> it = listInv.iterator();
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			//			System.out.println("INV lista ...."+listInv.size());
			while (it.hasNext()){
				MInvoiceLine dt = it.next();
				MProduct p = MProduct.get(Env.getCtx(), dt.getM_Product_ID());
				Vector<Object> rc = new Vector<Object>();
				rc.add(p.getName());
				rc.add(dt.getDescription());
				rc.add(dt.getQtyEntered());
				rc.add(dt.getPriceEntered());
				rc.add(dt.get_Value("smj_plate"));
				data.add(rc);
				//				System.out.println(this.getClass().getName()+".loadTableInv ..lineId.."+invoiceLineId+"...get.."+dt.getC_InvoiceLine_ID());
				if (invoiceLineId > 0 && invoiceLineId.equals(dt.getC_InvoiceLine_ID())){
					//					System.out.println(this.getClass().getName()+".LoadTableinv...por if..."+i);
					index = i;
				}
				i++;
			}//while

			Vector<String> columnNames = getTableColumnNames();
			contentPanel.clear();
			//  Set Model
			ListModelTable modelTable = new ListModelTable(data);
			contentPanel.setData(modelTable, columnNames);
			setTableColumnClass(contentPanel);
			
			for (Component component : contentPanel.getListHead().getChildren()) {
				ListHeader header = (ListHeader) component;
				header.setHflex("max");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".loadTableInv :: "	, e);
		}
	}//loadTableInv

	/**
	 * bloquea o desbloquea los campos si la actividad esta completa.
	 * lock and unlock fields if activity is complete
	 */
	private void lockFields(){
		descTextbox.setReadonly(isComplete);
		mechanicTextbox.setReadonly(isComplete);
		invoiceTextbox.setReadonly(isComplete);
		deleteButton.setEnabled(!isComplete);
		editButton.setEnabled(!isComplete);
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(!isComplete);
		isExternalChk.setDisabled(isComplete);
	}//readOnly

	/**
	 * muestra y oculta campos segun servicio externo - 
	 * show or hide fields by external service
	 */
	private void showLogic(){
		mechanicLabel.setVisible(!isExternal);
		mechanicTextbox.setVisible(!isExternal);
		invoiceLabel.setVisible(isExternal);
		invoiceTextbox.setVisible(isExternal);
		mechanicListbox.setVisible(!isExternal);
	}//showLogic

	/**
	 * muestra ventana info de llantas - 
	 * Show tires info window 
	 */
	private void showInfoTires(){
		try {
			if (contentPanel.getItemCount()<=0){
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelecMechanic"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			Integer mechanicId = listMec.get(0).getC_BPartner_ID();
			//			System.out.println(this.getClass().getCanonicalName()+".showInfoTires - mecani......"+mechanicId);
			final int rdm = UtilTrans.getRdmInteger();
			SMJInfoTires ip = new SMJInfoTires(rdm, "smj_tiresActivity", "smj_tiresActivity_ID", false, "", 
					vehicleId, listener, mechanicId, woLine.getsmj_workOrderLine_ID(), isComplete);
			ip.setVisible(true);
			ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelChangingTires"));
			ip.setStyle("border: 2px");
			ip.setClosable(true);
			ip.addValueChangeListener(listener);
			ip.setParent(this);

			ip.addEventListener(new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					tiresOk = true;
				}
			});

			AEnv.showCenterScreen(ip);

		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".showInfoTires - ERROR:: ", e);
		}
	}//showInfoTires

	/**
	 * Llena el combo de estados
	 * fill state list
	 */
	private void listMechanic(){
		mechanicListbox.removeAllItems();
		Vector<Vector<Object>> data = DataQueriesTrans.getMechanicList(Env.getAD_Client_ID(Env.getCtx()),Env.getAD_Org_ID(Env.getCtx()));
		mechanicListbox.appendItem("", 0);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			mechanicListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		mechanicListbox.setMold("select");
		mechanicListbox.addActionListener(this);
		mechanicListbox.setValue(0);
	}//listStates	

	public void addEventListener(EventListener<Event> eventListener) {

		if (eventListener == null) {
			return;
		}

		listeners.add(eventListener);
	}

	private void fireEventListener(Event event) throws Exception {
		for (EventListener<Event> listener : listeners)
		{
			listener.onEvent(event);
		}
	}

}//SMJInfoActivity
