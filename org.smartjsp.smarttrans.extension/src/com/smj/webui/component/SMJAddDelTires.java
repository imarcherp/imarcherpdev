package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MRefList;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.model.InfoProductList;
import com.smj.model.MSMJHistoryTire;
import com.smj.model.MSMJTires;
import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;
import com.smj.util.SaveHistory;

/**
 * @version <li>SmartJSP: SMJInfoProduct, 2013/05/06
 *          <ul TYPE ="circle">
 *          <li>Ventana modal para adicionar o remover llantas
 *          <li>Modal Window for add or remove tires
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJAddDelTires extends InfoPanel implements EventListener<Event>, WTableModelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3729208262465567749L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();
	private Label stateLabel = new Label();
	private Listbox stateListbox = new Listbox();
	
	private Label statusLabel = new Label();
	private Listbox statusListbox = new Listbox();
	
	private Label statusLabelOld = new Label();
	private Listbox statusListboxOld = new Listbox();
	
	private Intbox serialValue = new Intbox();
	private Label serialLabel = new Label();
	private Label positionLabel = new Label();
	private Listbox positionListbox = new Listbox();
	
	private Label depthTireLabel = new Label();
	private Decimalbox depthTireTextBox = new Decimalbox();
	private Label depthBrakeLabel = new Label();
	private Decimalbox depthBrakeTextBox = new Decimalbox();

	
	private Integer qtyTires = 0;
	private Integer vehicleId = 0;
	private Integer mechanicId = 0;
	private Integer position = 0; 
	private Integer wolineId = 0;
	private MSMJVehicle vehicle = null;
	private LinkedList<InfoProductList> list = new LinkedList<InfoProductList>();
	private Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private List<EventListener<Event>> listeners = new ArrayList<EventListener<Event>>();
	
	
	/**
	 * SMJInfoProduct constructor
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param listVal
	 * @param searchService
	 */
	public SMJAddDelTires(int WindowNo, String tableName, String keyColumn, Boolean multipleSelection, 
			String whereClause, Integer qty_Tires, Integer vehicle_Id, Integer mechanic_Id, Integer position_Id,
			Integer lineId) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		try {
			qtyTires = qty_Tires;
			vehicleId = vehicle_Id;
			mechanicId = mechanic_Id;
			wolineId = lineId;
			if(position_Id!= null && position_Id >0){
				position = position_Id;
			}
			vehicle = new MSMJVehicle(Env.getCtx(), vehicleId, null);
			init();
			list = getProductList("Llanta");
			loadTable();
			listStates();
			listTireStatus();
			listPosition();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".SMJAddDelTires - ERROR: " + e.getMessage(), e);
		}
	}//constructor

	/**
	 * ubica elementos en la ventana - locates elements in info window
	 */
	private void init()
	{
		
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "M_Product_ID"));
		stateLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelReasonChangeTires"));
		statusLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelTireStatus"));
		statusLabelOld.setText("" + Msg.translate(Env.getCtx(), "SMJLabelTireStatusOld"));
		serialLabel.setText("" + Msg.translate(Env.getCtx(), "serial"));
		positionLabel.setText("" + Msg.translate(Env.getCtx(), "position"));
		depthTireLabel.setText("" + Msg.translate(Env.getCtx(), "valueTire"));
		depthBrakeLabel.setText("" + Msg.translate(Env.getCtx(), "valueBrake"));

		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(fieldLabel);
		fieldValue.setWidth("150px");
		row.appendChild(fieldValue);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(positionLabel);
		positionListbox.setWidth("150px");
		row.appendChild(positionListbox);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(stateLabel);
		stateListbox.setWidth("150px");
		row.appendChild(stateListbox);
        
		/*   Aqui esta el combo de estados para la llanta actual
		 *   que se ha dejado siempre como en vehiculo al cambiar llantas
		row = new Row();
		rows.appendChild(row);
		row.appendChild(statusLabel);
		statusListbox.setWidth("150px");
		row.appendChild(statusListbox);
		*/
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(statusLabelOld);
		statusListboxOld.setWidth("150px");
		row.appendChild(statusListboxOld);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(serialLabel);
		serialValue.setWidth("150px");
		row.appendChild(serialValue);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(depthTireLabel);
		row.appendChild(depthTireTextBox);
		row.appendChild(depthBrakeLabel);
		row.appendChild(depthBrakeTextBox);
		
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		center.setHflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		setWidth("600px");
//		setHeight("350px");
	}//init
	
	@Override
	protected String getSQLWhere() {
		return null;
	}//getSQLWhere

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
	}//setParameters

	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		if (list == null || list.size()<=0){
			return;
		}
		Iterator<InfoProductList> it = list.iterator();
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (it.hasNext()){
			try {
				InfoProductList ipl = it.next();
				Vector<Object> rc = new Vector<Object>();
				rc.add(ipl.getProductValue());
				rc.add(ipl.getProductName());
				data.add(rc);
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()+".loadTable :: "	, e);
			}
		}//while
		
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(data);
		modelTable.removeTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		setTableColumnClass(contentPanel);
		contentPanel.setFocus(true);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "value"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Product_ID"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass

	@Override
	public void onEvent(Event ev) {
//		super.onEvent(event);
		try {
			if (ev.getTarget().equals(fieldValue)){
				String field = fieldValue.getValue();
				if (field.length()<= 0 || field.equals("") || field.equals(" "))
					return;
				list = getProductList(field);
				loadTable();
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Integer index = contentPanel.getSelectedIndex();
				if(positionListbox.getSelectedItem() == null || positionListbox.getSelectedItem().getValue() == null
						|| (Integer)positionListbox.getSelectedItem().getValue()<=0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectTirePosition"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				if(stateListbox.getSelectedItem() == null || stateListbox.getSelectedItem().getValue() == null
						|| (Integer)stateListbox.getSelectedItem().getValue()<=0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectReazon"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				Integer serial = serialValue.getValue();
				if(serial == null || serial<=0){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGFillSerial"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				else {  // serial valido, se valida si ya existe fuera de la posicion 0 (no seria valido)
					Trx trx = Trx.get(Trx.createTrxName("AL"), true);
					MSMJTires oldTire = getTireBySerial(serial, trx.getTrxName());
					
					if (oldTire != null) {  
						if (oldTire.getPosition() != 0)   {  // serial existe en orto vehuculo con posicion valida, no se puede usar
							Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSerialAssigned"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
							return;
						}
					}	
					
				}
				
				if (index == null || index < 0){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectItemList"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				
				InfoProductList ipl = list.get(index);
				String inVehicleStatus = MSysConfig.getValue("SMJ-STATUSTIREINVEHICLE",Env.getAD_Client_ID(Env.getCtx())).trim();
				Boolean ok = saveData(serial, (Integer)stateListbox.getSelectedItem().getValue(), 
						(Integer)positionListbox.getSelectedItem().getValue(), ipl.getProductID(),
                        inVehicleStatus,      						// (String)statusListbox.getSelectedItem().getValue(),
						(String)statusListboxOld.getSelectedItem().getValue(),
						depthBrakeTextBox.getValue(),depthTireTextBox.getValue());
				if(ok){
					Env.setContext(Env.getCtx(), p_WindowNo, "OK", ok.toString().toLowerCase());
					fireEventListener(ev);
					dispose(false);
				}
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
	            dispose(false);
	        }
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".onEvent :: "+e.getMessage(), e);
		}
	}//onEvent

	/**
	 * Llena el combo de razones para cambio
	 * fill reasons for change tires
	 */
	private void listStates(){
		stateListbox.removeAllItems();
		Vector<Vector<Object>> data = getState();
		stateListbox.appendItem("", "-1");
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			stateListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		stateListbox.setMold("select");
		stateListbox.addActionListener(this);
	}//listStates	


	/**
	 * Llena los combos de estados de llanta
	 * fill tire status list
	 */
	private void listTireStatus(){
		statusListbox.removeAllItems();
		statusListboxOld.removeAllItems();
		Vector<String> data = getTireStatus();
		//statusListbox.appendItem("", "");
		for (int i=0; i<data.size();i++){
			String line = data.get(i);
			statusListbox.appendItem(line,line);
			statusListboxOld.appendItem(line,line);
		}//for
		statusListbox.setMold("select");
		statusListboxOld.setMold("select");
		statusListbox.addActionListener(this);
		statusListboxOld.addActionListener(this);
	}//listTireStatus	

	
	
	
	/**
	 * regresa la lista de estados de la llanta
	 * returns list of tire stuses
	 * @param valueId
	 * @return Vector<String> 
	 */
	private Vector<String> getTireStatus(){
		
		
		ValueNamePair[] mf = MRefList.getList (Env.getCtx(),1000051,false);
		Vector<String> data = new Vector<String>();
		
		for (int j = 0;j < mf.length; j++)
		{
			String refDesc = mf[j].getName();
				data.add(refDesc);
		}		
		return data;
	}//getTireStatus
	
	
	
	
	/**
	 * regresa la lista de razones para cambio
	 * returns list of reasons for change
	 * @param valueId
	 * @return Vector<Vector<Object>> 
	 */
	private Vector<Vector<Object>> getState(){
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT smj_changeTire_ID, name FROM smj_changeTire ");
			sql.append(" WHERE isactive = 'Y' ");
			sql.append(" ORDER BY name ASC ");
//		System.out.println(this.getClass().getName()+".getState SQL::"+sql.toString());
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("smj_changeTire_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getState - SQL: " + sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getState
	
	/**
	 * lista de productos llantas - 
	 * llanta product list
	 * @param value
	 * @param newCode
	 * @param oldCode
	 * @param desc
	 * @return
	 */
	private LinkedList<InfoProductList> getProductList(String value) {
		LinkedList<InfoProductList> data = new LinkedList<InfoProductList>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT distinct M_Product_ID, name, value FROM M_Product f WHERE isactive = 'Y'");
		sql.append(" AND M_Product_Category_ID = "+tiresCategory+" ");
		sql.append(" AND productType <> 'S'");
			sql.append(" AND (f.value like '%"+value.trim()+"%' ");
			sql.append(" OR f.sku like '%"+value.trim()+"%' ");
			sql.append(" OR UPPER(f.name) like '%"+value.toUpperCase().trim()+"%') ");
		sql.append(" ORDER BY name ASC ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				InfoProductList ipl = new InfoProductList();
				ipl.setProductID(rs.getInt("M_Product_ID"));
				ipl.setProductValue(rs.getString("value"));
				ipl.setProductName(rs.getString("name"));
				data.add(ipl);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".getProductList - SQL : "+sql.toString()	, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;

	}// getProductList

	/**
	 * guarda los datos de la llanta - 
	 * save tire data
	 * @param serial
	 * @param reason
	 * @param position
	 * @param productId
	 */
	private Boolean saveData(Integer serial, Integer reason, Integer position, 
			                 Integer productId, String status,String statusOld,
			                 BigDecimal depthBrake,BigDecimal depthTire){
		Boolean ok = true;
		try {
			//String inVehicleStatus = MSysConfig.getValue("SMJ-STATUSTIREINVEHICLE",Env.getAD_Client_ID(Env.getCtx())).trim();
			// aqui se deja el estado actual (nuevo) de la llanta siempre en Vehiculo, la anterior es la que cambia
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			MSMJTires tire = DataQueriesTrans.getTireData(vehicleId, position);   // llanta actual
			MSMJTires tireOld = DataQueriesTrans.getTireData(vehicleId, position);  // copia de datos de llanta actual
			MSMJTires tire2 = new MSMJTires(Env.getCtx(), 0, trx.getTrxName()); // nueva llanta si existe la previa
			
			Boolean hok = true;
			Boolean tireExist = false;
			if (tire != null){   // ya existia la llanta (cambio)
				tireExist = true;
				MSMJVehicle v = new MSMJVehicle(Env.getCtx(), tire.getsmj_vehicle_ID(),trx.getTrxName() );
				MSMJHistoryTire h = new MSMJHistoryTire(Env.getCtx(), 0, trx.getTrxName());
				h.setM_Product_ID(tire.getM_Product_ID());
				h.setsmj_vehicle_ID(tire.getsmj_vehicle_ID());
				h.setC_BPartner_ID(tire.getC_BPartner_ID());
				h.setPosition(tire.getPosition());
				h.setserial(tire.getserial());
				h.setdatemount(tire.getdatemount());
				h.setdateumount(Env.getContextAsDate(Env.getCtx(), "#Date"));
				h.setsmj_changeTire_ID(reason);
				h.setkms(tire.getkms());
				h.setvaluebrake(tire.getvaluebrake());
				h.setvaluetire(tire.getvaluetire());
				h.settirestate(statusOld);
				// en este bloque quita la placa y id del vehiculo si el estado de 
				// la llanta anterior no es VEHICULO (cuando se envia a bodega, reencauche o dada de baja)

				//if (statusOld != inVehicleStatus) {
				//	h.setsmj_plate("");
					// asigna el historico de la llanta 
				//	h.setsmj_vehicle_ID(Integer.parseInt(MSysConfig.getValue("SMJ-VEHICLEIDFORTIRESNOTASSIGNED",Env.getAD_Client_ID(Env.getCtx())).trim()));   //asigna el vehiculo 0 - llantas no en vehiculo
				//}
				if(wolineId > 0){
					h.setsmj_workOrderLine_ID(wolineId);
				}
				else {
					h.setsmj_workOrderLine_ID(tire.getsmj_workOrderLine_ID());
				}
				h.setsmj_plate(v.get_ValueAsString("smj_plate"));
				if (tire.getkmsMount() == null || tire.getkmsMount().compareTo(Env.ZERO)<=0){
					tire.setkmsMount(vehicle.getkms());
				}
				hok = h.save(); // salva el registro para el historico
				
				// Aqui crea una nueva llanta con la informacion anterior, pero el vehiculo 0000 (segun configuracion) y posicion 0
				tire2.set_TrxName(trx.getTrxName());
				tire2.setserial(tireOld.getserial());
				tire2.setPosition(0);  // esta fuera del vehiculo
				tire2.setM_Product_ID(tireOld.getM_Product_ID());
				tire2.setC_BPartner_ID(tireOld.getC_BPartner_ID());
				tire2.setdatemount(Env.getContextAsDate(Env.getCtx(), "#Date"));
				tire2.setkmsMount(tireOld.getkmsMount());
				tire2.setvaluetire(tireOld.getvaluetire());
				tire2.setvaluebrake(tireOld.getvaluebrake());
				if(wolineId > 0){
					tire2.setsmj_workOrderLine_ID(wolineId);
				}
				tire2.setkms(tireOld.getkms());
				tire2.setsmj_vehicle_ID(Integer.parseInt(MSysConfig.getValue("SMJ-VEHICLEIDFORTIRESNOTASSIGNED",Env.getAD_Client_ID(Env.getCtx())).trim()));   //asigna el vehiculo 0 - llantas no en vehiculo
				tire2.setStatus(statusOld);
				
				// asigna el historico de la llanta 
				
				
			}  else{ // era una llanta nueva
				
				// aqui valida si el serial existia fuera del vehiculo (posicion0), 
				// 
				MSMJTires oldTire = getTireBySerial(serial, trx.getTrxName());
				if (oldTire != null) {  
					if (oldTire.getPosition() == 0)   {  // serial existe pero esta fuera de vehiculo se puede reutilizar
                    tire = oldTire;
                    tire.setkms(vehicle.getkms());
					}
				}
				else {   // la llanta no existia con ese serial, se crea una nueva normal
					tire = new MSMJTires(Env.getCtx(), 0, trx.getTrxName());
					// tire.setkms(Env.ZERO);
					tire.setkms(vehicle.getkms());	
				}
				
				
			} 
			tire.set_TrxName(trx.getTrxName());
			tire.setserial(serial);
			tire.setPosition(position);
			tire.setM_Product_ID(productId);
			tire.setsmj_vehicle_ID(vehicleId);
			tire.setC_BPartner_ID(mechanicId);
			tire.setdatemount(Env.getContextAsDate(Env.getCtx(), "#Date"));
			tire.setkmsMount(vehicle.getkms());
			tire.setvaluetire(depthTire);
			tire.setvaluebrake(depthBrake);
			
			if(wolineId > 0){
				tire.setsmj_workOrderLine_ID(wolineId);
			}
			BigDecimal kmsTire = getKmsMax(serial);
			tire.setkms(kmsTire);
			tire.setStatus(status);   // estado al montar la llanta
			tire.save();
			ok = tire.save();
			if (tireExist) // crea la llanta en posicion 0, estado anterior y vehiculo 000 / si es llanta nueva no la crea
			{
				ok = tire2.save();
			}
			     
			if(ok && hok ){
				SaveHistory.saveHistorytire(trx.getTrxName(), tire, reason);  // crea el registro historico para la llanta nueva
				trx.commit();
			}else{
				trx.rollback();
				ok = false;
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorSavingData"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
			trx.close();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+". - ERROR: "+e.getMessage(), e);
		}
		return ok;
	}//

	/**
	 * Llena el combo de estados
	 * fill state list
	 */
	private void listPosition(){
		positionListbox.removeAllItems();
//		positionListbox.appendItem("", "0");
		for (Integer i=1; i<=qtyTires;i++){
			positionListbox.appendItem(i.toString(), i);
		}//for
		positionListbox.setMold("select");
		positionListbox.addActionListener(this);
		if(position >0){
			positionListbox.setValue(position);
			positionListbox.setDisabled(true);
		}else{
			positionListbox.setValue(1);
			positionListbox.setDisabled(false);
		}
	}//listStates	
	
	/**
	 * regresa el kms maximo de la llanta por serial - 
	 * return max kms tire by serial
	 * @param serial
	 * @return
	 */
	private BigDecimal getKmsMax(Integer serial){
		try {
			BigDecimal kmsa = getKms("smj_tires", serial);
			BigDecimal kmsh = getKms("smj_HistoryTire", serial);
			if (kmsh.compareTo(kmsa)> 0){
				return kmsh;
			}else{
				return kmsa;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ "getKmsMax - ERROR: " + e.getMessage(), e);
			return Env.ZERO;
		}
	}//getKmsMax

	/**
	 * returna el kms de la llanta por serial -
	 * return kms tire by serial
	 * @param value
	 * @param serial
	 * @return
	 */
	private BigDecimal getKms(String value, Integer serial) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT MAX(kms) AS kms FROM "+value+" ");
		sql.append(" WHERE serial = "+serial+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BigDecimal kms = Env.ZERO;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()){
				kms = rs.getBigDecimal("kms");
			}
			if (kms == null){
				kms = Env.ZERO;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName()+".getKms - SQL : "+sql.toString()	, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return kms;

	}// getKms

	
	/**
	 * returna la llanta por serial -
	 * return tire by serial
	 * @param serial
	 * @return  MSMJTires
	 */
	private MSMJTires getTireBySerial(Integer serial, String trxName) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_tires_id FROM smj_tires");
		sql.append(" WHERE serial = "+serial+" ");
		PreparedStatement pstmt = null;
		MSMJTires tire = null;
		ResultSet rs = null;
		Integer tireId = 0; 
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()){
				tireId = rs.getInt("smj_tires_ID");
				tire = new MSMJTires(Env.getCtx(), tireId, trxName);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJAddDelTires.class.getName()+".getTireBySerial - SQL : "+sql.toString()	, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return tire;

	}// getTireBySerial

	public void addEventListener(EventListener<Event> eventListener) {
		if (eventListener == null) {
			return;
		}

		listeners.add(eventListener);
	}

	public void removeEventListener(EventListener<Event> eventListener) {
		if (eventListener == null) {
			return;
		}

		listeners.remove(eventListener);
	}
	
	private void fireEventListener(Event event) throws Exception {
		for (EventListener<Event> listener : listeners)
		{
			listener.onEvent(event);
		}
	}
}//SMJAddDelTires
