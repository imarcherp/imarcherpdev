package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

/**
 * @version <li>SmartJSP: SMJInfoProduct, 2013/04/30
 *          <ul TYPE ="circle">
 *          <li>Ventana modal de informacion de facturas para orden de trabajo
 *          <li>Modal Window for info invoice by work order
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoInvoice extends InfoPanel implements EventListener<Event>, WTableModelListener{
	
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 935516751282073726L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();
	private Integer productId = 0;  
	private String selectedPlate  = "";
	private Label plateLabel = new Label();
	private Textbox plateTextBox = new Textbox();
	
	private LinkedList<HashMap<String, Object>> list = null;
	private List<EventListener<Event>> listeners = new ArrayList<EventListener<Event>>();

	public SMJInfoInvoice(int WindowNo, String tableName, String keyColumn,
			boolean multipleSelection, String whereClause, String value, 
			Integer product_Id, String plate) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		productId = product_Id;
		selectedPlate = plate;
		init();
		plateTextBox.setValue(selectedPlate);
		getDataList(null, value);
		loadTable();
	}//SMJInfoPlate

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		fieldValue.setWidth("50%");
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "C_Invoice_ID"));
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		plateTextBox.addEventListener(Events.ON_BLUR, this);
		
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(fieldLabel.rightAlign());
		fieldValue.setWidth("200px");
		row.appendChild(fieldValue);
		row.appendChild(new Label());
		
		row = new Row();
		rows.appendChild(row);
		plateLabel.setWidth("100px");
		row.appendChild(plateLabel.rightAlign());
		plateTextBox.setWidth("200px");
		plateTextBox.setMaxlength(10);
		row.appendChild(plateTextBox);
        
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		setWidth("700px");
//		setHeight("350px");
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	}//init
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
		Iterator<HashMap<String, Object>> itlist = list.iterator();
		while (itlist.hasNext()){
			HashMap<String, Object> obj = itlist.next();
			Vector<Object> v = new Vector<Object>();
			v.add(obj.get("DOC"));
			v.add(obj.get("DATE"));
			v.add(obj.get("NAME"));
			v.add(obj.get("TOTAL"));
			datat.add(v);
		}
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(datat);
		modelTable.removeTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		setTableColumnClass(contentPanel);
		contentPanel.setFocus(true);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "documentNo"));
		columnNames.add(Msg.translate(Env.getCtx(), "dateinvoiced"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "grandTotal"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, BigDecimal.class, true); 
		table.setColumnClass(i++, String.class, true);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	@Override
	public void onEvent(Event ev) {
//		super.onEvent(event);
		try {
			if (ev.getTarget().equals(fieldValue)){
				String field = fieldValue.getValue();
				if (field == null || field.length()<= 0 || field.equals("") || field.equals(" "))
					return;
				getDataList(null, field);
				loadTable();
				return;
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Integer index = contentPanel.getSelectedIndex();
				if (index == null || index < 0){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectItemList"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				HashMap<String, Object> obj = list.get(index);
				Integer code = (Integer) obj.get("CODE");
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "INVCODE", code.toString());
				fireEventListener(ev);
				dispose(false);
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				Env.setContext(Env.getCtx(), p_WindowNo, "INVCODE", "");
	            dispose(false);
	        }
			else if (ev.getTarget().equals(contentPanel)){
				Integer index = contentPanel.getSelectedIndex();
				if (index == null || index < 0){
					return;
				}
				HashMap<String, Object> obj = list.get(index);
				Integer code = (Integer) obj.get("CODE");
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "INVCODE", code.toString());
				fireEventListener(ev);
				dispose(false);
			}
			else if (ev.getTarget().equals(plateTextBox)){
				selectedPlate = plateTextBox.getValue();
				String field = fieldValue.getValue();
				getDataList(null, field);
				loadTable();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".onEvent :: "	, e);
		}
	}//onEvent
	
	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
		
	}

	/**
	 * regresa la lista de solicitudes - return request list
	 * @param trxName
	 * @param value
	 * @return LinkedList<HashMap<String, Object>>
	 */ 
	private void getDataList(String trxName, String value) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT i.C_Invoice_ID, i.documentNo, i.dateinvoiced, p.name, i.grandTotal ");
		sql.append(" , l.C_InvoiceLine_ID, l.M_Product_ID, l.smj_plate ");
		sql.append(" FROM C_Invoice i, C_BPartner p, C_InvoiceLine l ");
		sql.append(" WHERE i.isactive='Y' AND i.issotrx = 'N' AND i.docstatus = 'CO' ");
		sql.append(" AND i.ad_client_ID= "+Env.getAD_Client_ID(Env.getCtx())+" ");
		sql.append(" AND p.C_BPartner_ID = i.C_BPartner_ID ");
		sql.append(" AND i.C_Invoice_ID = l.C_Invoice_ID ");
		if (value != null){
			sql.append(" AND i.documentNo LIKE '"+value.trim()+"%' ");
		}
		if (productId != null && productId >0){
			sql.append(" AND l.M_Product_ID = "+productId+" ");
		}
		if (selectedPlate != null && selectedPlate.length() >0){
			sql.append(" AND (l.smj_plate = '"+selectedPlate.toUpperCase().trim()+"'  OR l.smj_plate IS NULL)");
		}
		sql.append(" ORDER BY documentNo ASC ");
//		System.out.println(this.getClass().getName()+".getDataList SQL::"+sql.toString());
		
		list = new LinkedList<HashMap<String, Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("CODE", rs.getInt("C_Invoice_ID"));
				obj.put("DOC", rs.getString("documentNo"));
				obj.put("DATE", rs.getDate("dateinvoiced"));
				obj.put("NAME", rs.getString("name"));
				obj.put("TOTAL", rs.getBigDecimal("grandTotal"));
				obj.put("PLATE", rs.getString("smj_plate"));
				list.add(obj);
			}// if
			
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".getDataList - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return ;
	}// getDataList
	
	public void addEventListener(EventListener<Event> eventListener) {
		
		if (eventListener == null) {
			return;
		}
		
		listeners.add(eventListener);
	}
	
	public void removeEventListener(EventListener<Event> eventListener) {
		if (eventListener == null) {
			return;
		}
		
		listeners.remove(eventListener);
	}
	
	public void fireEventListener(Event event) throws Exception {
		for (EventListener<Event> listener : listeners)
        {
           listener.onEvent(event);
        }
	}
}//SMJInfoInvoice
