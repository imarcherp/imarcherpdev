package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Center;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.smj.model.MSMJTires;
import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;
import com.smj.util.SaveHistory;
import com.smj.util.UtilTrans;
import com.smj.webui.component.SMJAddDelTires;

public class WUpdateTires extends UpdateTires implements IFormController, EventListener, 
WTableModelListener, ValueChangeListener{

	public WUpdateTires() {
		try	{
			dynInit();
			zkInit(); 
			form.addEventListener(Events.ON_OPEN, this);
			AuFocus auf = new AuFocus(plateTextBox);
			Clients.response(auf);
		}catch(Exception e){
			log.log(Level.SEVERE, "", e);
		}//try/catch
	}//constructor ...
	
	private CustomForm form = new CustomForm();
//  generales
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private final static Integer reason = Integer.parseInt(MSysConfig.getValue("SMJ-REASONCHANGETIRE", Env.getAD_Client_ID(Env.getCtx())).trim());
	
	private Label kmLabel = new Label();
	private Decimalbox kmIntbox = new Decimalbox();
	private Label kmOldLabel = new Label();
	private Decimalbox kmOldIntbox = new Decimalbox();
	private Label plateLabel = new Label();
	private Textbox plateTextBox = new Textbox();
	private Label entryDateLabel = new Label();
	private WDateEditor entryDate = new WDateEditor();
	
	//variables de la forma web (paneles)
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
//	private Panel buttonPanel = new Panel();
	//---Panel Sur ------
//	private Panel southPanel = new Panel();
	//---Panel de informacion ------
	private Trx trx = null;
	private String trxName = null;
	private Borderlayout infoPanel = new Borderlayout();
	private MSMJVehicle vehicle = null;
	private Panel tablePanel = new Panel();
	private Borderlayout tableLayout = new Borderlayout();
	private WListbox dataTable = ListboxFactory.newDataTable();
	
	private Label positionLabel = new Label();
	private Textbox positionTextBox = new Textbox();
	private Label kmsLabel = new Label();
//	private Decimalbox kmsTextBox = new Decimalbox();
	private Label depthTireLabel = new Label();
	private Decimalbox depthTireTextBox = new Decimalbox();
	private Label depthBrakeLabel = new Label();
	private Decimalbox depthBrakeTextBox = new Decimalbox();
	private Label serialLabel = new Label();
	private Textbox serialTextBox = new Textbox();
	private Label positionNewLabel = new Label();
	private Intbox positionNewTextBox = new Intbox();
	private Button editButton = new Button ();
	private Button editTireButton = new Button (); 
	private Integer vehicleId = 0;
	private Integer qtyTires = 0;
	private Integer mechanicId = 0;
	private Integer woLineId = 0;
	private Boolean isTrailer = false; 
	private Label trailerLabel = new Label();
	private Checkbox trailerCheckbox = new Checkbox();
	
	private LinkedList<MSMJTires> list = null;
	
	/**
	 *  Prepara los campos dinamicos
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	private void dynInit() throws Exception 	{
		try {
			entryDate.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".dynInit - ERROR: " + e.getMessage(), e);
		}
	}//dynInit
	
	/**
	 *  Inicializa los componentes estaticos
	 *  Static Init (inicializa los componentes estaticos)
	 *  @throws Exception
	 */
	private void zkInit() throws Exception	{
		form.appendChild(mainLayout);
		mainLayout.setWidth("99%");
		mainLayout.setHeight("100%");
		parameterPanel.setHeight("100%");
		parameterLayout.setWidth("100%");
		parameterLayout.setHeight("100%");
		parameterPanel.appendChild(parameterLayout);
		
		//establecer etiquetas - fill label
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		plateTextBox.addEventListener(Events.ON_BLUR, this);
		kmLabel.setText("" + Msg.translate(Env.getCtx(), "kms"));
		kmOldLabel.setText("" + Msg.translate(Env.getCtx(), "kmsActual"));
		entryDateLabel.setText("" + Msg.translate(Env.getCtx(), "entryDate"));
		positionLabel.setText("" + Msg.translate(Env.getCtx(), "position"));
		kmsLabel.setText("" + Msg.translate(Env.getCtx(), "kms"));
		depthTireLabel.setText("" + Msg.translate(Env.getCtx(), "valueTire"));
		depthBrakeLabel.setText("" + Msg.translate(Env.getCtx(), "valueBrake"));
		serialLabel.setText("" + Msg.translate(Env.getCtx(), "serial"));
		positionNewLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelNewPosition"));
		editButton.setLabel("" + Msg.translate(Env.getCtx(), "save"));
		editButton.addActionListener(this);
		editTireButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelNewEditTire"));
		editTireButton.addActionListener(this);
		trailerLabel.setText("" + Msg.translate(Env.getCtx(), "istrailer"));
		
		//definicion de paneles y ubicacion en la forma.
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();
		
		//row
		row = new Row();
		rows.appendChild(row);
		row.appendChild(plateLabel.rightAlign());
//		plateTextBox.setReadonly(true);
		row.appendChild(plateTextBox);
		kmOldLabel.setWidth("100px");
		row.appendChild(kmOldLabel.rightAlign());
		kmOldIntbox.setWidth("150px");
		kmOldIntbox.setReadonly(true);
		row.appendChild(kmOldIntbox);
		rows.appendChild(row);
		row.appendChild(kmsLabel.rightAlign());
//		kmsTextBox.setReadonly(true);
		row.appendChild(kmIntbox);
		

		row = new Row();
		rows.appendChild(row);
		row.appendChild(positionLabel.rightAlign());
		positionTextBox.setReadonly(true);
		row.appendChild(positionTextBox);
		//row.appendChild(positionNewLabel.rightAlign());   // se remueve la nueva posicion de esta ventana
		Div dpos = new Div();
		//dpos.appendChild(positionNewTextBox);
		if(isTrailer){
			dpos.appendChild(trailerCheckbox);
			dpos.appendChild(trailerLabel);
		}
		//row.appendChild(dpos);
		row.appendChild(serialLabel.rightAlign());
		serialTextBox.setReadonly(true);
		row.appendChild(serialTextBox);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(depthTireLabel.rightAlign());
		row.appendChild(depthTireTextBox);
		row.appendChild(depthBrakeLabel.rightAlign());
		row.appendChild(depthBrakeTextBox);
		row.appendChild(new Label());
		row.appendChild(new Label());
		
		//button row
		row = new Row();
		rows.appendChild(row);
		row.setAlign("right");
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		Div divButton = new Div();
			divButton.appendChild(editButton);
			//divButton.appendChild(editTireButton);
		row.appendChild(divButton);   // se desahbilita la modificacion de llantas sin orden de trabajo
		
		//---------PANEL SUR---------------------------------------------
		Panel southPanel = new Panel();
		South south = new South();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		confirmPanel.addActionListener(this);
		southPanel.appendChild(confirmPanel);
		//----Configura table panel-------------------------------------------------
		tablePanel.appendChild(tableLayout);
		tablePanel.setWidth("100%");
		tablePanel.setHeight("100%");
		tableLayout.setWidth("100%");
		tableLayout.setHeight("100%");
		tableLayout.setStyle("border: none");
		north = new North();
		north.setStyle("border: none");
		tableLayout.appendChild(north);
		south = new South();
		south.setStyle("border: none");
		tableLayout.appendChild(south);
		Center center = new Center();
		tableLayout.appendChild(center);
		center.appendChild(dataTable);
		
		dataTable.setWidth("100%");
		dataTable.setHeight("99%");
		center.setStyle("border: none");
		//---Coloca el Tabla panel en la pantalla----------------------
		center = new Center();
		center.setFlex(true);
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		
		infoPanel.setStyle("border: none");
		infoPanel.setWidth("100%");
		infoPanel.setHeight("100%");
		
		north = new North();
		north.setStyle("border: none");
		north.setHeight("100%");
		infoPanel.appendChild(north);
		north.appendChild(tablePanel);
		north.setSplittable(true);
	}//zkInit
	
	@Override 
	public ADForm getForm() {
		return form;
	}//getForm

	@Override
	public void valueChange(ValueChangeEvent evt) {
		
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		
	}

	@Override
	public void onEvent(Event ev) throws Exception {
		if(ev.getTarget().equals(plateTextBox)){
			String plate = plateTextBox.getValue().toUpperCase().trim();
			if (plate==null || plate.length() <= 0 || plate.trim().equals("")){
				return;
			}
			plateTextBox.setValue(plate);
//			System.out.println(this.getClass().getCanonicalName()+".onEvent plate.."+plate);
			vehicleId = DataQueriesTrans.getVehicleId(plate);
			if (vehicleId <=0){
				return;
			}
			vehicle = new MSMJVehicle(Env.getCtx(), vehicleId, null);
			if (vehicle != null){
				kmOldIntbox.setValue(vehicle.getkms());
				qtyTires = vehicle.getqtytires();
				if(vehicle.istrailer() || vehicle.istrucktractor()){
					isTrailer = true;
				}
				try {
					if (vehicle.getodometer() == null || !vehicle.getodometer().equals("F")){
						kmIntbox.setReadonly(true);
						kmIntbox.setValue(vehicle.getkms());
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					}else{
						kmIntbox.setReadonly(false);
					}
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()+".setVehicle ERROR:: ", e);
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}else{
				trx.close();
			}
			list = DataQueriesTrans.getTiresList(trxName, vehicle);
			loadTable();
			if (trx != null){
				trx.rollback();
				trx.close();
				trx = null;
				trxName = null;
			}
		}//if plateTextBox
		else if(ev.getTarget().equals(dataTable)){
//			System.out.println(this.getClass().getName()+".onEvent ... dataTable..");
			Integer index = dataTable.getSelectedIndex();
			MSMJTires tire = list.get(index);
//			System.out.println(this.getClass().getName()+".onEvent indx.."+index);
			positionTextBox.setValue(Integer.toString(tire.getPosition()));
			serialTextBox.setValue(Integer.toString(tire.getserial()));
//			kmsTextBox.setValue(tire.getkms());
			depthTireTextBox.setValue(tire.getvaluetire());
			depthBrakeTextBox.setValue(tire.getvaluebrake());
		}
		else if(ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
			try {
//				System.out.println(this.getClass().getCanonicalName()+".onEvent OK");
				BigDecimal kms = kmIntbox.getValue();
				//TODO KMS
				if(!kmIntbox.isReadonly()){
					if (kms == null || kms.compareTo(Env.ZERO)<=0 ){
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGValidKms"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}
					if(kms.compareTo(vehicle.getkms())<=0){ 
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGGreaterKms"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}
				}else{
					kms=vehicle.getkms();
				}
				
//				vehicle.setkms(kms);
				Boolean ok = saveKmsTire(trxName);
//				Boolean ok = vehicle.save();
				if(ok){
					clean();
					if(trx != null){
						trx.commit();
						trx.close();
						trx = null;
						trxName = null;
					}
				}else{
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorSavingData"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".onEvent - ERROR ConfirmPanel.A_OK: " + e.getMessage(), e);
			}
		}//if OK
		else if(ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))){
			int response = Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGWantCancel"), labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
			if (response == Messagebox.OK){
				if(trx != null){
					trx.rollback();
					trx.close();
					trx = null;
					trxName = null;
				}
				clean();
				SessionManager.getAppDesktop().closeActiveWindow();
			}//response OK
			return;
		}//if CANCEL
		else if (ev.getTarget().equals(editButton)){
//			System.out.println(this.getClass().getName()+".onEvent edit button....");
			Boolean ok = saveData();
			if(ok){
				list = DataQueriesTrans.getTiresList(trxName, vehicle);
				loadTable();
			}
		}
		else if (ev.getTarget().equals(editTireButton)){
			String value = "llanta";
			showInfoProduct(value);
//			loadTable();
		}
		
	}//onEvent

	/**
	 * limpia los campos de la forma - 
	 * clean form fields
	 */
	private void clean(){
		vehicle = null;
		plateTextBox.setValue("");
		kmIntbox.setValue(Env.ZERO);
		kmOldIntbox.setValue(Env.ZERO);
		positionTextBox.setValue(null);
		serialTextBox.setValue(null);
//		kmsTextBox.setValue(null);
		kmIntbox.setValue(Env.ZERO);
		depthTireTextBox.setValue(Env.ZERO);
		depthBrakeTextBox.setValue(Env.ZERO);
		list = new LinkedList<MSMJTires>();
		loadTable();
		if (trx!= null){
			trx.close();
			trx = null;
			trxName = null;
		}
	}//clean
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		Vector<Vector<Object>> datat = new Vector<Vector<Object>>();
		if (list == null)
			return;
		Iterator<MSMJTires> itlist = list.iterator();
		while (itlist.hasNext()){
			MSMJTires obj = itlist.next();
			Vector<Object> v = new Vector<Object>();
			v.add(obj.getPlate());
			v.add(obj.getPosition());
			v.add(obj.getName());
			v.add(obj.getserial());
			v.add(obj.getvaluetire());
			v.add(obj.getvaluebrake());
			v.add(obj.getStatus());
			datat.add(v);
		}
		Vector<String> columnNames = getTableColumnNames();
		dataTable.clear();
		dataTable.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(datat);
		modelTable.removeTableModelListener(this);
		dataTable.setData(modelTable, columnNames);
		dataTable.addEventListener(Events.ON_CLICK, this);
		dataTable.addActionListener(this);
		setTableColumnClass(dataTable);
		
		for (Component component : dataTable.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * muestra la ventana de informacion de productos y trae el producto seleccionado - 
	 * show info product window and returns selected product data  
	 * @param value
	 */
	private void showInfoProduct(String value){
		try {
			Integer index = dataTable.getSelectedIndex();
			Integer pos = 0;
			if(index != null && index >0){
				pos = list.get(index).getPosition();
			}
			int rdm = UtilTrans.getRdmInteger();
			SMJAddDelTires ip = new SMJAddDelTires(rdm, "M_Product", "M_Product_ID", false, "", qtyTires, 
					vehicleId, mechanicId, pos, woLineId); 
			ip.setVisible(true);
			ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelSearchProducts"));
			ip.setStyle("border: 2px");
			ip.setClosable(true);
			ip.setAttribute("mode", "modal");
			ip.addValueChangeListener(this);
			AEnv.showWindow(ip);
			String ok = Env.getContext(Env.getCtx(), rdm, "OK");
//			System.out.println( this.getClass().getCanonicalName()+".showInfoProduct - esOK????? "+ok);
			if (ok.equals("true")){
				list = DataQueriesTrans.getTiresList(trxName, vehicle);
				loadTable();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".showInfoProduct - ERROR: "+e.getMessage(), e);
		}
	}//showInfoProduct  

	/**
	 * guarda la informacion de la llanta selecionada - 
	 * save data selected tire
	 * @return Boolean
	 */
	private Boolean saveData(){
		Boolean ok = true;
		try {
//			System.out.println("cehicle change ..."+vehicleId);
			Boolean pok = true;
//			Boolean chVehicle = false;
			if (trx == null){
				trx = Trx.get(Trx.createTrxName("AL"), true);	
				trxName = trx.getTrxName();
			}
			Integer pos2 = positionNewTextBox.getValue();
			Integer index = dataTable.getSelectedIndex();
			if (index == null || index< 0){
				return false;
			}
			MSMJTires tire = list.get(index);
			SaveHistory.saveHistorytire(trx.getTrxName(), tire, reason);
			tire.set_TrxName(trx.getTrxName());
			Integer pos1 = tire.getPosition();
			BigDecimal brakeT2 = Env.ZERO;
			BigDecimal tireT1 = tire.getvaluetire();
			tire.setserial(tire.getserial()*-1);
			tire.setPosition(tire.getPosition()*-1);
			tire.save();
			if (pos2 != null && pos2>0){
				MSMJTires p2 = null;
				if(isTrailer && trailerCheckbox.isChecked() && vehicle.getasociatedtrucktrailer_ID()>0){
					p2 = DataQueriesTrans.getTireData(vehicle.getasociatedtrucktrailer_ID(), pos2);
					p2.setsmj_vehicle_ID(vehicleId);
					tire.setsmj_vehicle_ID(vehicle.getasociatedtrucktrailer_ID());
				}else{
					p2 = DataQueriesTrans.getTireData(vehicleId, pos2);
				}
				if (p2 == null){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoTireInPosition"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
				SaveHistory.saveHistorytire(trx.getTrxName(), p2, reason);
				p2.setC_BPartner_ID(mechanicId);
				if(woLineId>0){
					p2.setsmj_workOrderLine_ID(woLineId);
				}
				brakeT2 = p2.getvaluebrake();
				p2.set_TrxName(trx.getTrxName());
				p2.setPosition(pos1);
				p2.setvaluebrake(depthBrakeTextBox.getValue());
				p2.setvaluetire(depthTireTextBox.getValue());
				pok = p2.save();
				tire.setPosition(pos2);
				tire.setvaluebrake(brakeT2);
				tire.setvaluetire(tireT1);
			}//if newPos
			else{
				
				tire.setvaluetire(depthTireTextBox.getValue());
				tire.setvaluebrake(depthBrakeTextBox.getValue());
			}
			tire.setC_BPartner_ID(mechanicId);
			if(woLineId>0){
				tire.setsmj_workOrderLine_ID(woLineId);
			}
			if(tire.getserial() < 0){
				tire.setserial(tire.getserial()*-1);
			}
			if (tire.getPosition()<0){
				tire.setPosition(tire.getPosition()*-1);
			}
			ok = tire.save();
			if(pok && ok){
				depthTireTextBox.setValue(Env.ZERO);
				depthBrakeTextBox.setValue(Env.ZERO);
				serialTextBox.setValue("");
				positionNewTextBox.setValue(0);
				positionTextBox.setValue("");
				trailerCheckbox.setChecked(false);
			}else{
//				trx.rollback();
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorSavingData"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				ok = false;
			}
//			trx.close();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".saveData - ERROR: "+e.getMessage(), e);
		}//saveData
		return ok;
	}//saveData

	
	/**
	 * guarda el kms del vehiculo y las llantas - 
	 * save kms for vehicle and tires
	 * @param trxName
	 * @return Boolean
	 */
	private Boolean saveKmsTire(String trxName){
		Boolean flag = true;
		try {
//			LinkedList<MSMJTires> list = DataQueriesTrans.getTiresList(trxName, vehicle);
			BigDecimal kmsOld =  kmOldIntbox.getValue();
			Iterator<MSMJTires> it = list.iterator();
			BigDecimal kmActual = kmIntbox.getValue();
			vehicle.set_TrxName(trxName);
			vehicle.setdatelastmileage(Env.getContextAsDate(Env.getCtx(), "#Date"));
			vehicle.setkms(kmActual);
			Boolean ok = vehicle.save();
			if (!ok){
				return false;
			}
			while(it.hasNext()){
				MSMJTires tire = it.next();
				BigDecimal kmsTotal = UtilTrans.calculateKms(tire, kmsOld, kmActual);
				if (kmsTotal == null){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGValidKms"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
				if (tire.getkmsMount() == null || tire.getkmsMount().compareTo(Env.ZERO)<=0){
					tire.setkmsMount(kmActual);
				}
				tire.setkms(kmsTotal);
//				System.out.println(this.getClass().getCanonicalName()+".saveKmsTire lmsTotal.."+kmsTotal);
				
				ok = tire.save();
				if (!ok){
					return false;
				}
			}//while 
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".saveKmsTire - ERROR: " + e.getMessage(), e);
			flag = false;
		}
		return flag;
	}//saveKmsTire
	
}//WUpdateTires
