package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;

public class WUpdateKms extends UpdateTires implements IFormController, EventListener, 
WTableModelListener, ValueChangeListener{

	public WUpdateKms() {
		try	{
			dynInit();
			zkInit(); 
			form.addEventListener(Events.ON_OPEN, this);
			AuFocus auf = new AuFocus(plateTextBox);
			Clients.response(auf);
		}catch(Exception e){
			log.log(Level.SEVERE, "", e);
		}//try/catch
	}//constructor ...
	
	private CustomForm form = new CustomForm();
//  generales
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	
	private Label kmLabel = new Label();
	private Decimalbox kmIntbox = new Decimalbox();
	private Label kmOldLabel = new Label();
	private Decimalbox kmOldIntbox = new Decimalbox();
	private Label plateLabel = new Label();
	private Textbox plateTextBox = new Textbox();
	private Label entryDateLabel = new Label();
	private WDateEditor entryDate = new WDateEditor();
	
	//variables de la forma web (paneles)
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
//	private Panel buttonPanel = new Panel();
	//---Panel Sur ------
//	private Panel southPanel = new Panel();
	//---Panel de informacion ------
	private Borderlayout infoPanel = new Borderlayout();
	private MSMJVehicle vehicle = null;
	private Boolean odometerOK = true;
	
	/**
	 *  Prepara los campos dinamicos
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	private void dynInit() throws Exception 	{
		try {
			entryDate.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".dynInit - ERROR: " + e.getMessage(), e);
		}
	}//dynInit
	
	/**
	 *  Inicializa los componentes estaticos
	 *  Static Init (inicializa los componentes estaticos)
	 *  @throws Exception
	 */
	private void zkInit() throws Exception	{
		form.appendChild(mainLayout);
		mainLayout.setWidth("99%");
		mainLayout.setHeight("100%");
		parameterPanel.setHeight("100%");
		parameterLayout.setWidth("100%");
		parameterLayout.setHeight("100%");
		parameterPanel.appendChild(parameterLayout);
		
		//establecer etiquetas - fill label
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		plateTextBox.addEventListener(Events.ON_BLUR, this);
		kmLabel.setText("" + Msg.translate(Env.getCtx(), "kms"));
		kmOldLabel.setText("" + Msg.translate(Env.getCtx(), "kmsActual"));
		entryDateLabel.setText("" + Msg.translate(Env.getCtx(), "entryDate"));
		
		//definicion de paneles y ubicacion en la forma.
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();
		
		//row
		row = rows.newRow();
		plateLabel.setWidth("100px");
		row.appendChild(plateLabel.rightAlign());
		plateTextBox.setWidth("150px");
		row.appendChild(plateTextBox);
		
		//row
		row = rows.newRow();
		kmOldLabel.setWidth("100px");
		row.appendChild(kmOldLabel.rightAlign());
		kmOldIntbox.setWidth("150px");
		kmOldIntbox.setReadonly(true);
		row.appendChild(kmOldIntbox);
		
//		//row
//		row = rows.newRow();
//		entryDateLabel.setWidth("100px");
//		row.appendChild(entryDateLabel.rightAlign());
////		entryDate.setWidth("150px");
//		row.appendChild(entryDate.getComponent());
		
		row = rows.newRow();
		kmLabel.setWidth("100px");
		row.appendChild(kmLabel.rightAlign());
		kmIntbox.setWidth("150px");
		row.appendChild(kmIntbox);

		//---------PANEL SUR---------------------------------------------
		Panel southPanel = new Panel();
		South south = new South();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		confirmPanel.addActionListener(this);
		southPanel.appendChild(confirmPanel);
		////---------PANEL CENTRO---------------------------------------------
		north = new North();
		north.setStyle("border: none");
		south = new South();
		south.setStyle("border: none");
		Center center = new Center();
		center.setStyle("border: none");
		//---Coloca el Tabla panel en la pantalla----------------------
		center.setFlex(true);
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		infoPanel.setStyle("border: none");
		infoPanel.setWidth("100%");
		infoPanel.setHeight("100%");
		
		north = new North();
		north.setStyle("border: none");
		north.setHeight("100%");
		infoPanel.appendChild(north);
//		north.appendChild(tablePanel);
//		north.setSplittable(true);
	}//zkInit
	
	@Override 
	public ADForm getForm() {
		return form;
	}//getForm

	@Override
	public void valueChange(ValueChangeEvent evt) {
		
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		
	}

	@Override
	public void onEvent(Event ev) throws Exception {
		if(ev.getTarget().equals(plateTextBox)){
			String plate = plateTextBox.getValue().toUpperCase().trim();
			if (plate==null || plate.length() <= 0 || plate.trim().equals("")){
				return;
			}
			odometerOK = true;
			plateTextBox.setValue(plate);
//			System.out.println(this.getClass().getCanonicalName()+".onEvent plate.."+plate);
			Integer vehicleId = DataQueriesTrans.getVehicleId(plate);
			vehicle = new MSMJVehicle(Env.getCtx(), vehicleId, null);
			if (vehicle != null){
				try {
					if (vehicle.getodometer() == null || !vehicle.getodometer().equals("F")){
						kmIntbox.setReadonly(true);
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}else{
						kmIntbox.setReadonly(false);
					}
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()+".setVehicle ERROR:: ", e);
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				kmOldIntbox.setValue(vehicle.getkms());
			}
		}//if plateTextBox
		else if(ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
			try {
				if(!odometerOK){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
//				System.out.println(this.getClass().getCanonicalName()+".onEvent OK");
				String plate = plateTextBox.getValue();
				if(plate == null || plate.trim().equals("") ||vehicle == null){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGValidPlate"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				BigDecimal kms = kmIntbox.getValue();
				if (kms == null || kms.compareTo(Env.ZERO)<=0 ){
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGValidKms"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				if(kms.compareTo(vehicle.getkms())<=0){ 
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGGreaterKms"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				vehicle.setdatelastmileage(Env.getContextAsDate(Env.getCtx(), "#Date"));
				vehicle.setkms(kms);
				Boolean ok = vehicle.save();
				if(ok){
					clean();
				}else{
					Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorSavingData"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".onEvent - ERROR ConfirmPanel.A_OK: " + e.getMessage(), e);
			}
		}//if OK
		else if(ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))){
			int response = Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGWantCancel"), labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
			if (response == Messagebox.OK){
				SessionManager.getAppDesktop().closeActiveWindow();
			}//response OK
			return;
		}//if CANCEL
		
	}//onEvent

	/**
	 * limpia los campos de la forma - 
	 * clean form fields
	 */
	private void clean(){
		vehicle = null;
		plateTextBox.setValue("");
		kmIntbox.setValue(Env.ZERO);
		kmOldIntbox.setValue(Env.ZERO);
	}//clean
	
}//WUpdateTires
