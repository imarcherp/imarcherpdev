package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.I_AD_Reference;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MCost;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocator;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MOrder;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRequest;
import org.compiere.model.MRequestType;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.smj.model.InfoProductList;
import com.smj.model.MSMJScheduleRules;
import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJTires;
import com.smj.model.MSMJVehicle;
import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;
import com.smj.reactive.Consumer;
import com.smj.util.CompleteSaleWebTrans;
import com.smj.util.DataQueries;
import com.smj.util.DataQueriesTrans;
import com.smj.util.Documents;
import com.smj.util.DocumentsTrans;
import com.smj.util.ShowWindowTrans;
import com.smj.util.UtilTrans;
import com.smj.webui.component.CreateWOProgram;
import com.smj.webui.component.SMJInfoActivity;
import com.smj.webui.component.SMJInfoGarrantyLine;
import com.smj.webui.component.SMJInfoProduct;
import com.smj.webui.component.SMJInfoWorkOrder;
import com.smj.webui.component.SMJSearchProducts;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013-04-24
 *          <ul TYPE ="circle">
 *          <li>Clase para maneja la forma de venta simplificada
 *          <li>Class to form simplified sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class WWorkOrder extends WorkOrder implements IFormController, EventListener<Event>, WTableModelListener, ValueChangeListener {

	private CustomForm form = new CustomForm();
	
	private static final String CONSIGNMENT_DATA = "ConsignmentData";
	private static final String CONSIGNMENT_SHIPMENTS = "ConsignmentShipments";

	@Override
	public ADForm getForm() {
		return form;
	}// getForm

	/**
	 * WSimplifiedSales constructor
	 */
	public WWorkOrder() {
		try {
			dynInit();
			zkInit();
			focusSeq();
			form.addEventListener(Events.ON_OPEN, this);
			form.addEventListener(Events.ON_OPEN, this);
			AuFocus auf = new AuFocus(searchPlate.getComponent());
			Clients.response(auf);
		} catch (Exception e) {
			log.log(Level.SEVERE, "", e);
		}// try/catch
	}// WSimplifiedSales

	// variables de la forma web (paneles)
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	private Grid buttonLayout = GridFactory.newGridLayout();
	// ---Panel de informacion ------
	private Borderlayout infoPanel = new Borderlayout();
	// panel Table
	private Panel tablePanel = new Panel();
	private Borderlayout tableLayout = new Borderlayout();
	private WListbox dataTable = ListboxFactory.newDataTable();
	// campos a ser mostrados
	private Label productLabel = new Label();
	private Textbox productTextBox = new Textbox();
	private Label bPartnerLabel = new Label();
	private WSearchEditor bpartnerSearch = null;
	private WSearchEditor searchPlate;
	private Button deleteButton = new Button();
	private Button editButton = new Button();
	private Button cleanButton = new Button();
	private Button requestButton = new Button();
	private Label totalLabel = new Label();
	private Label totalDecimalbox = new Label();
	private Label subTotalLabel = new Label();
	private Label subTotalDecimalbox = new Label();
	private Label taxLabel = new Label();
	private Label taxDecimalbox = new Label();
	private Label stateLabel = new Label();
	private Listbox stateListbox = new Listbox();
	private Label plateLabel = new Label();
	private Boolean showPlate = false;
	private String selectedPlate = "";
	private Integer vehicleID = 0;
	private Button findButton = new Button();
	private Label woLabel = new Label();
	private Textbox woTextBox = new Textbox();
	private Label kmLabel = new Label();
	private Decimalbox kmIntbox = new Decimalbox();
	private Label descLabel = new Label();
	private Textbox descTextBox = new Textbox();
	private Label garrantyLabel = new Label();
	private Textbox garrantyTextbox = new Textbox();
	private Button addlinegarrantyBtn = new Button();
	private Label nextProgLabel = new Label();
	private Checkbox nextProgCheckbox = new Checkbox();
	private Button deleteWOButton = new Button();
	private Label requestTypeLabel = new Label();
	private Listbox requestTypeListbox = new Listbox();
	private Boolean isRmnFac = false;
	// /
	private Label numReqCustomerLabel = new Label();
	private Textbox numReqCustomerTextBox = new Textbox();
	private Label diagnosisLabel = new Label();
	private Textbox diagnosisTextBox = new Textbox();
	private Button createButton = new Button();
	private Boolean tiresOk = false;
	private Boolean tiresDoit = true;
	private Integer countTiresAct = 0;
	// camps
	private Integer bpartnerId = 0;
	private String bpartnerName = "";
	private Integer locationID = 0;
	private LinkedList<MSMJWorkOrderLine> list = new LinkedList<MSMJWorkOrderLine>();
	private BigDecimal grandTotal = Env.ZERO;
	private BigDecimal taxTotal = Env.ZERO;
	private BigDecimal subTotal = Env.ZERO;
	private MRequest tmpRequest = null;
	private String temparioName = "";
	private Boolean isEditable = true;
	private MSMJVehicle vehicle = null;
	private Boolean isChangeActivity = false;
	private Boolean isReOpen = false;
	private BigDecimal kmOld = Env.ZERO;
	private Integer invId = 0;
	private String descRmn = null;
	private String descProduct = "";

	// generales
	private static final String labelInfo = Msg.translate(Env.getCtx(),
			"SMJLabelInfo").trim();
	private Integer woListStates = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATESWOLIST", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateDefault = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATESWODEFAULT", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateWO = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATEWORKORDER", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateGarranty = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATEWOWARRANTY", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateComplete = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOCOMPLETE", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateInvoiced = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOINVOICED", Env.getAD_Client_ID(Env.getCtx())).trim());
	// private Integer stateClose =
	private Integer stateProgram = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATEWOPROGRAM", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer stateReOpen = Integer.parseInt(MSysConfig.getValue(
			"SMJ-STATEWOREOPEN", Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer tiresCategory = Integer.parseInt(MSysConfig
			.getValue("SMJ-TIRESCATEGORY", Env.getAD_Client_ID(Env.getCtx()))
			.trim());
	private static DecimalFormat df = new DecimalFormat("$ ###,###,###,##0.00");
	private String roles = MSysConfig.getValue("SMJ-ROLEACTIVITY",
			Env.getAD_Client_ID(Env.getCtx())).trim();
	private String rolReOpen = MSysConfig.getValue("SMJ-ROLEREOPEN",
			Env.getAD_Client_ID(Env.getCtx())).trim();
	private Integer serviceRateGroupDefault = Integer.parseInt(MSysConfig
			.getValue("SMJ-SRATEGROUPDEF", Env.getAD_Client_ID(Env.getCtx()))
			.trim());
	private static final Integer codeServiceMtto = Integer.parseInt(MSysConfig
			.getValue("SMJ-SERVICEMTTO", Env.getAD_Client_ID(Env.getCtx()))
			.trim());
	private static final Integer codeAdministrationTires = Integer
			.parseInt(MSysConfig.getValue("SMJ-ADMINTIRES",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	private BigDecimal maxFee = new BigDecimal(MSysConfig.getValue(
			"SMJ-MAXMECHANICFEE", Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer rTypeRmnFac = Integer
			.parseInt(MSysConfig.getValue("SMJ-REQTYPEREMANUFAC",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer rTypeWO = Integer.parseInt(MSysConfig
			.getValue("SMJ-REQUESTTYPE", Env.getAD_Client_ID(Env.getCtx()))
			.trim());
	private static final Integer userSeveter = Integer
			.parseInt(MSysConfig.getValue("SMJ-USRSEVETERRMNFAC",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer rmnFacCategory = Integer.parseInt(MSysConfig
			.getValue("SMJ-REMANUFACTUREDCAT",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer purchaseId = Integer.parseInt(MSysConfig
			.getValue("SMJ_PURCHASELIST", Env.getAD_Client_ID(Env.getCtx()))
			.trim());
	private static final Integer principalWarehouse = Integer
			.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	private static final Integer paytermDef = Integer.parseInt(MSysConfig
			.getValue("SMJ-PAYMENTTERMDEFAULT",
					Env.getAD_Client_ID(Env.getCtx())).trim());
	
	private static final Integer idPartnerOwnConsumer = Integer.parseInt(MSysConfig.getValue("SMJ-USERINTERNALPAY",
			Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()))); //iMarch
	private Integer m_R_Request_ID = 0;//iMarch
	private Integer m_C_BPartner_ID = 0;//iMarch
	private String m_DocumentNo = "";//iMarch
	private String m_Summary = "";//iMarch
	private Boolean ipformOpen = false;

	/**
	 * Prepara los campos dinamicos Dynamic Init (prepare dynamic fields)
	 * 
	 * @throws Exception
	 *             if Lookups cannot be initialized
	 */
	private void dynInit() throws Exception {
		try {
			int AD_Column_ID = 2893; // C_Order.C_BPartner_ID

			MLookup lookupBP = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), 0, AD_Column_ID, DisplayType.Search);
			bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
			bpartnerSearch.addValueChangeListener(new ValueChangeListener() {

				@Override
				public void valueChange(ValueChangeEvent evt) {
					searchBPartnerValueChange(evt);
				}
			});

			//Search Plate
			final int v_ColumnID_SMJ_Vehicle = DataQueries.getColumnID("SMJ_Vehicle_ID", "SMJ_Vehicle");
			String whereClause = I_AD_Reference.COLUMNNAME_Name + " LIKE 'Search Vehicle (SmartPOS)'";			
			int AD_Reference_ID = new Query(Env.getCtx(), I_AD_Reference.Table_Name, whereClause, null).firstIdOnly();
			MLookup lookupPlate = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), v_ColumnID_SMJ_Vehicle, DisplayType.Search, Env.getLanguage(Env.getCtx()), "SMJ_Vehicle_ID", AD_Reference_ID, true, null);
			searchPlate = new WSearchEditor("SMJ_Vehicle_ID", true, false, true, lookupPlate);

			searchPlate.getComponent().getTextbox().addEventListener(Events.ON_CHANGE, new EventListener<Event>() {

				@Override
				public void onEvent(Event evt) throws Exception {
					if (bpartnerId < 1) {
						selectedPlate = searchPlate.getComponent().getText().toUpperCase();
					}
				}
			});

			searchPlate.addValueChangeListener(new ValueChangeListener() {			
				@Override
				public void valueChange(ValueChangeEvent evt) {
					searchPlateValueChange(evt);
				}
			});

			list = new LinkedList<MSMJWorkOrderLine>();
			String shPlate = MSysConfig.getValue("SMJ_SHOWPLATE",
					Env.getAD_Client_ID(Env.getCtx())).trim();
			showPlate = shPlate.equals("true") ? true : false;
			listStates(stateReOpen);
			listRequestType();
			canChangeActivity();
			validateLoginOrg();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".dynInit - ERROR: " + e.getMessage(), e);
		}
	}// dynInit

	/**
	 * Inicializa los componentes estaticos Static Init (inicializa los
	 * componentes estaticos)
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private void zkInit() throws Exception {
		form.appendChild(mainLayout);
		//mainLayout.setWidth("99%");
		//mainLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		//parameterPanel.setHeight("100%");
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		//parameterLayout.setWidth("100%");
		//parameterLayout.setHeight("235px");
		//parameterLayout.setVflex("1");
		ZKUpdateUtil.setWidth(parameterLayout, "100%");
		ZKUpdateUtil.setHeight(parameterLayout, "235px");
		ZKUpdateUtil.setVflex(parameterLayout, "1");
		parameterPanel.appendChild(parameterLayout);

		// establecer etiquetas - fill label
		bPartnerLabel
		.setText("" + Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		productLabel.setText(""
				+ Msg.translate(Env.getCtx(), "SMJLabelProductService"));
		totalLabel.setText("" + Msg.translate(Env.getCtx(), "Total"));
		subTotalLabel.setText("" + Msg.translate(Env.getCtx(), "SubTotal"));
		taxLabel.setText("" + Msg.translate(Env.getCtx(), "C_Tax_ID"));
		productTextBox.addEventListener(Events.ON_BLUR, this);
		stateLabel.setText("" + Msg.translate(Env.getCtx(), "R_Status_ID"));
		plateLabel.setText("" + Msg.translate(Env.getCtx(), "smj_plate"));
		woLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
		kmLabel.setText("" + Msg.translate(Env.getCtx(), "kms"));
		// kmIntbox.addEventListener(Events.ON_BLUR, this);
		descLabel.setText("" + Msg.translate(Env.getCtx(), "summary"));
		// descTextBox.addEventListener(Events.ON_BLUR, this);
		garrantyLabel.setText(""
				+ Msg.translate(Env.getCtx(), "SMJLabelGaranty"));
		nextProgLabel.setText(""
				+ Msg.translate(Env.getCtx(), "SMJLabelScheduleNext"));
		nextProgCheckbox.addActionListener(this);
		requestTypeLabel.setText(""
				+ Msg.translate(Env.getCtx(), "R_RequestType_ID"));
		diagnosisLabel.setText(""
				+ Msg.translate(Env.getCtx(), "smj_diagnosis"));
		numReqCustomerLabel.setText(""
				+ Msg.translate(Env.getCtx(), "smj_numreqcustomer"));

		// Tooltip
		plateLabel.setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTPlate"));
		productLabel.setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTProduct"));
		bPartnerLabel.setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTPartner"));
		searchPlate.getComponent().setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTPlate"));
		productTextBox.setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTProduct"));
		bpartnerSearch.getComponent().setTooltiptext(""
				+ Msg.translate(Env.getCtx(), "SMJTTPartner"));

		deleteButton.setLabel(""
				+ Msg.translate(Env.getCtx(), "SMJLabelDelete"));
		deleteButton.addActionListener(this);
		editButton.setLabel(""
				+ Msg.translate(Env.getCtx(), "SMJLabelEditActivity"));
		editButton.addActionListener(this);
		cleanButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelClean"));
		cleanButton.addActionListener(this);
		// solicitud 1000430 buton para solicitudes
		requestButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelRequest"));
		requestButton.addActionListener(this);

		deleteWOButton.setLabel(""
				+ Msg.translate(Env.getCtx(), "SMJLabelDelete") + " "
				+ Msg.translate(Env.getCtx(), "SMJLabelWorkOrder"));
		deleteWOButton.addActionListener(this);
		//
		createButton.setLabel(""
				+ Msg.translate(Env.getCtx(), "SMJLabelCreate"));
		createButton.addActionListener(this);

		findButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelFind"));
		findButton.addActionListener(this);
		//modificacion iMarch-------
		addlinegarrantyBtn.setLabel("AgregaLinea");
		addlinegarrantyBtn.addActionListener(this);
		//fin modificacion iMarch---
		// definicion de paneles y ubicacion en la forma.
		South south = new South();
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);

		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();

		// row
		row = rows.newRow();
		//woLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(woLabel, "100%");
		row.appendChild(woLabel.rightAlign());
		Div divOt = new Div();
		woTextBox.setWidth("90px");
		ZKUpdateUtil.setWidth(woTextBox, "90px");
		woTextBox.setReadonly(true);
		divOt.appendChild(woTextBox);
		//findButton.setHeight("10px");//modificacion iMarch
		//findButton.setWidth("65px");//modificacion iMarch
		findButton.setStyle("padding:2px;");//modificacion iMarch
		ZKUpdateUtil.setHeight(findButton, "10px");
		ZKUpdateUtil.setWidth(findButton, "65px");
		divOt.appendChild(findButton);
		//row.appendChild(divOt);
		row.appendCellChild(divOt,2);//modificacion iMArch
		//requestTypeLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(requestTypeLabel, "100px");
		row.appendChild(requestTypeLabel.rightAlign());
		//requestTypeListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(requestTypeListbox, "150px");
		row.appendChild(requestTypeListbox);
		if (showPlate) {
			//plateLabel.setWidth("100px");
			ZKUpdateUtil.setWidth(plateLabel, "100px");
			row.appendChild(plateLabel.rightAlign());
			//searchPlate.getComponent().setWidth("150px");
			ZKUpdateUtil.setWidth(searchPlate.getComponent(), "100px");
			row.appendChild(searchPlate.getComponent());
		}

		// row
		row = rows.newRow();
		//bPartnerLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(bPartnerLabel, "100px");
		row.appendChild(bPartnerLabel.rightAlign());
		//bpartnerSearch.getComponent().setWidth("150px");
		ZKUpdateUtil.setWidth(bpartnerSearch.getComponent(), "150px");
		row.appendChild(bpartnerSearch.getComponent());
		// row.appendChild(bpartnerSearch.getComponent());
		//numReqCustomerLabel.setWidth("150px");
		ZKUpdateUtil.setWidth(numReqCustomerLabel, "150px");
		row.appendCellChild(numReqCustomerLabel.rightAlign(),2);
		//numReqCustomerTextBox.setWidth("150px");
		ZKUpdateUtil.setWidth(numReqCustomerTextBox, "150px");
		row.appendChild(numReqCustomerTextBox);
		//kmLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(kmLabel, "100px");
		row.appendChild(kmLabel.rightAlign());
		//kmIntbox.setWidth("150px");
		ZKUpdateUtil.setWidth(kmIntbox, "150px");
		row.appendChild(kmIntbox);

		// /description row summary
		row = rows.newRow();
		row.setSpans("1, 6");
		//descLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(descLabel, "100px");
		row.appendChild(descLabel.rightAlign());
		descTextBox.setCols(158);
		descTextBox.setRows(2);
		// descTextBox.setValue("" + Msg.translate(Env.getCtx(),
		// "SMJLabelWorkOrder")+":");
		// descTextBox.setWidth("90%");
		// descTextBox.setHeight("40px");
		row.appendChild(descTextBox);

		// fila para programar siguiente
		row = rows.newRow();
		//stateLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(stateLabel, "100px");
		row.appendChild(stateLabel.rightAlign());
		//stateListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(stateListbox, "150px");
		row.appendChild(stateListbox);
		//productLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(productLabel, "100px");
		row.appendChild(productLabel.rightAlign());
		//productTextBox.setWidth("150px");
		ZKUpdateUtil.setWidth(productTextBox, "150px");
		row.appendChild(productTextBox);
		row.appendChild(nextProgLabel);
		row.appendChild(nextProgCheckbox);
		nextProgLabel.setVisible(false);
		nextProgCheckbox.setVisible(false);

		// /summary row
		row = rows.newRow();
		row.setSpans("1, 6");
		//diagnosisLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(diagnosisLabel, "100px");
		row.appendChild(diagnosisLabel.rightAlign());
		diagnosisTextBox.setCols(150);
		diagnosisTextBox.setRows(1);
		row.appendChild(diagnosisTextBox);

		// button row
		row = rows.newRow();
		row.appendChild(garrantyLabel.rightAlign());
		/**
		garrantyTextbox.setReadonly(true);
		garrantyTextbox.setWidth("80px");
		row.appendChild(garrantyTextbox);
		garrantyLabel.setVisible(false);
		garrantyTextbox.setVisible(false);
		addlinegarrantyBtn.setStyle("float: left");**/
		
		//modificacion iMarch---------
		Div divim = new Div();
		divim.setStyle("align: left;");
		garrantyTextbox.setReadonly(true);
		garrantyTextbox.setStyle("float: left");
		//garrantyTextbox.setWidth("100px");
		ZKUpdateUtil.setWidth(garrantyTextbox, "100px");
		addlinegarrantyBtn.setStyle("float: left; padding:2px;");
		//addlinegarrantyBtn.setWidth("100px");
		//addlinegarrantyBtn.setHeight("10px");
		ZKUpdateUtil.setWidth(addlinegarrantyBtn, "100px");
		ZKUpdateUtil.setHeight(addlinegarrantyBtn, "10px");
		addlinegarrantyBtn.setVisible(false);
		addlinegarrantyBtn.setDisabled(false);
		garrantyLabel.setVisible(false);
		garrantyTextbox.setVisible(false);
		divim.appendChild(garrantyTextbox);
		divim.appendChild(addlinegarrantyBtn);
		row.appendCellChild(divim, 2);
		//fin modificacion iMarch---------
		
		Div div = new Div();
		div.setStyle("align: right");
		
		//addlinegarrantyBtn.setStyle("float: left;  left: -20px;");
		deleteButton.setStyle("float: right");
		createButton.setStyle("float: right");
		cleanButton.setStyle("float: right");
		deleteButton.setStyle("float: right");
		editButton.setStyle("float: right");
		requestButton.setStyle("float: right");
		//div.appendChild(addlinegarrantyBtn);//modificacion iMarch
		div.appendChild(requestButton);
		div.appendChild(editButton);
		div.appendChild(deleteButton);
		div.appendChild(cleanButton);
		div.appendChild(createButton);		
		div.appendChild(deleteWOButton);

		row.appendCellChild(div, 5);
		deleteWOButton.setVisible(false);
		// ---------PANEL SUR---------------------------------------------
		Panel southPanel = new Panel();
		south = new South();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		confirmPanel.addActionListener(this);

		southPanel.appendChild(confirmPanel);
		Rows rowsb = buttonLayout.newRows();
		Row rowb = rowsb.newRow();

		// row.......
		rowb = rowsb.newRow();
		rowb.setAlign("right");
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		//subTotalLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(subTotalLabel, "100px");
		subTotalLabel.rightAlign();
		rowb.appendChild(subTotalLabel);
		//subTotalDecimalbox.setWidth("50px");
		ZKUpdateUtil.setWidth(subTotalDecimalbox, "50px");
		subTotalDecimalbox.rightAlign();
		subTotalDecimalbox.setText("$0,00");
		rowb.appendChild(subTotalDecimalbox);

		// row.......
		rowb = rowsb.newRow();
		rowb.setAlign("right");
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		//taxLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(taxLabel, "100px");
		taxLabel.rightAlign();
		taxLabel.setStyle("bold");
		rowb.appendChild(taxLabel);
		//taxDecimalbox.setWidth("50px");
		ZKUpdateUtil.setWidth(taxDecimalbox, "50px");
		taxDecimalbox.rightAlign();
		taxDecimalbox.setText("$0,00");
		rowb.appendChild(taxDecimalbox);

		// row.......
		rowb = rowsb.newRow();
		rowb.setAlign("right");
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		//totalLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(totalLabel, "100px");
		totalLabel.rightAlign();
		rowb.appendChild(totalLabel);
		//totalDecimalbox.setWidth("50px");
		ZKUpdateUtil.setWidth(totalDecimalbox, "50px");
		totalDecimalbox.rightAlign();
		totalDecimalbox.setText("$0,00");
		rowb.appendChild(totalDecimalbox);

		// ----Configura table
		// panel-------------------------------------------------
		tablePanel.appendChild(tableLayout);
		//tablePanel.setWidth("100%");
		//tablePanel.setHeight("100%");
		ZKUpdateUtil.setWidth(tablePanel, "100%");
		ZKUpdateUtil.setHeight(tablePanel, "100%");
		//tableLayout.setWidth("100%");
		//tableLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(tableLayout, "100%");
		ZKUpdateUtil.setHeight(tableLayout, "100%");
		tableLayout.setStyle("border: none");
		//buttonLayout.setWidth("100%");
		//buttonLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(buttonLayout, "100%");
		ZKUpdateUtil.setHeight(buttonLayout, "100%");
		buttonLayout.setStyle("border: none");
		north = new North();
		north.setStyle("border: none");
		tableLayout.appendChild(north);
		south = new South();
		south.setStyle("border: none");
		tableLayout.appendChild(south);
		south.appendChild(buttonLayout);
		Center center = new Center();
		center.setStyle("border: none");
		tableLayout.appendChild(center);
		center.appendChild(dataTable);

		//dataTable.setWidth("100%");
		//dataTable.setHeight("99%");
		ZKUpdateUtil.setWidth(dataTable, "100%");
		//ZKUpdateUtil.setHeight(dataTable, "99%");
		center.setStyle("border: none");
		// ---Coloca el Tabla panel en la pantalla----------------------
		center = new Center();
		//center.setVflex("1");
		ZKUpdateUtil.setVflex(center, "1");
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);

		infoPanel.setStyle("border: none");
		//infoPanel.setWidth("100%");
		//infoPanel.setHeight("100%");
		ZKUpdateUtil.setWidth(infoPanel, "100%");
		ZKUpdateUtil.setHeight(infoPanel, "100%");

		north = new North();
		north.setStyle("border: none");
		//north.setHeight("100%");
		ZKUpdateUtil.setHeight(north, "100%");
		infoPanel.appendChild(north);
		north.appendChild(tablePanel);
		//north.setSplittable(true);
		//north.setVflex("1");
		//ZKUpdateUtil.setVflex(north, "1");
	}// zkInit

	private void focusSeq() {
		searchPlate.getComponent().getTextbox().setTabindex(1);
		bpartnerSearch.getComponent().getTextbox().setTabindex(2);
		descTextBox.setTabindex(3);
		productTextBox.setTabindex(4);
		createButton.setTabindex(5);
	}

	/**
	 * se ejecuta cuando cambia el valor de bpartnerSearch - it's excecuted when
	 * bpartnerSearch changes
	 */
	@Override
	public void valueChange(ValueChangeEvent evt) {
		try {
			String name = evt.getPropertyName();
			Object value = evt.getNewValue();
			if (value == null) {
				if (bpartnerId > 0) {
					bpartnerSearch.setValue(bpartnerId);
				} else {
					try {
						org.adempiere.webui.component.Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantCreatePartner"),labelInfo, Messagebox.OK | Messagebox.CANCEL,
								Messagebox.QUESTION, new Callback<Integer>() {

							@Override
							public void onCallback(Integer result) {
								if (result == Messagebox.OK) {
									ShowWindowTrans.openPartnerWebPOS(0);
								}
							}
						});
					} catch (Exception ex) {
						log.log(Level.SEVERE, this.getClass()
								.getCanonicalName()
								+ ".valueChange ERROR: Show Message : ", ex);
					}
				}
				return;
			}// if Value
			// BPartner
			if (name.equals("C_BPartner_ID")) {
				bpartnerSearch.setValue(value);
				bpartnerId = ((Integer) value).intValue();
				setPartner();
			}// C_BPartner_ID
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".valueChange - ERROR: " + e.getMessage(), e);
		}
		descTextBox.setFocus(true);
	}// valueChange

	/**
	 * Carga la informacion en la tabla - load table with info generate
	 */
	private void loadTable() {
		try {
			Iterator<MSMJWorkOrderLine> it = list.iterator();
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			grandTotal = Env.ZERO;
			subTotal = Env.ZERO;
			taxTotal = Env.ZERO;
			while (it.hasNext()) {
				MSMJWorkOrderLine ipx = it.next();
				Vector<Object> v = new Vector<Object>();
				// MProduct prod = MProduct.get(Env.getCtx(),
				// ipx.getM_Product_ID());
				// v.add(prod.getValue());
				v.add(ipx.getProductValue());
				v.add(ipx.getWarehouseName());
				// v.add(prod.getName());
				v.add(ipx.getProductName());
				v.add(ipx.getPrice());
				v.add(ipx.getQty());
				v.add(ipx.gettotal());
				v.add(ipx.getobservations());
				//ESTADOS PARAMETRICOS
				if (ipx.getwolstate() == null)
					v.add(" ");
				else if (ipx.getwolstate().equals("S"))
					v.add("Solicitud");
				else if (ipx.getwolstate().equals("A"))
					v.add("Asignado");
				else if (ipx.getwolstate().equals("C"))
					v.add("Completo");
				else
					v.add(" ");
				if (ipx.getsmj_estimatedtime() != null) {
					v.add(ipx.getsmj_estimatedtime());
				} else {
					v.add("");
				}
				v.add("");
				data.add(v);
				grandTotal = grandTotal.add(ipx.gettotal());
				BigDecimal btax = Env.ONE.add((ipx.gettaxvalue().divide(
						Env.ONEHUNDRED, 2, RoundingMode.HALF_UP)));
				BigDecimal base = ipx.gettotal().divide(btax, 2,
						RoundingMode.HALF_UP);
				BigDecimal tax = ipx.gettotal().subtract(base);
				subTotal = subTotal.add(base);
				taxTotal = taxTotal.add(tax);
			}
			subTotalDecimalbox.setValue(df.format(subTotal));
			taxDecimalbox.setValue(df.format(taxTotal));
			totalDecimalbox.setValue(df.format(grandTotal));

			Vector<String> columnNames = getTableColumnNames();
			dataTable.clear();
			dataTable.getModel().removeTableModelListener(this);
			// Set Model
			ListModelTable model = new ListModelTable(data);
			model.removeTableModelListener(this);
			model.addTableModelListener(this);
			dataTable.setData(model, columnNames);
			dataTable.removeEventListener(Events.ON_DOUBLE_CLICK, this);
			dataTable.addEventListener(Events.ON_DOUBLE_CLICK, this);
			setTableColumnClass(dataTable, isEditable);
			dataTable.setFocus(true);

			for (Component component : dataTable.getListHead().getChildren()) {
				ListHeader header = (ListHeader) component;
				header.setHflex("max");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".loadTable - ERROR: " + e.getMessage(), e);
		}
	}// loadTable

	/**
	 * se ejecuta cuando cambia algun valor en la tabla - It's executed when
	 * something value in table changes
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void tableChanged(WTableModelEvent evx) {
		try {
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			if (evx != null) {
				int index = evx.getIndex0();
				// System.out.println("tbel index.."+index);
				Vector<Object> v = (Vector<Object>) dataTable.getModel().get(
						index);
				MSMJWorkOrderLine ipl = list.get(index);
				BigDecimal qty = (BigDecimal) v.get(4);
				BigDecimal price = (BigDecimal) v.get(3);
				String mec = (String) v.get(9);
				// price
				if (evx.getColumn() == 3) {
					MProduct p = MProduct.get(Env.getCtx(),
							ipl.getM_Product_ID());
					MLocator locator = MLocator.get(Env.getCtx(),
							ipl.getlocator_ID());
					MWarehouse warehouse = MWarehouse.get(Env.getCtx(),
							locator.getM_Warehouse_ID());
					Integer warehouseOwnerId = 0;
					Boolean isConsigment = warehouse
							.get_ValueAsBoolean("smj_isconsignment");
					if (isConsigment) {
						warehouseOwnerId = warehouse
								.get_ValueAsInt("C_BPartner_ID");
					}
					String tempario = temparioName;
					if (ipl.getM_Product_ID() == codeServiceMtto
							|| tempario == null || tempario.length() <= 0) {
						tempario = "Mostrador";
					}
					HashMap<String, Integer> codes = DataQueriesTrans
							.getPListVersion(null, p, ipl.getprovider_ID(),
									tiresCategory, ipl.getlocator_ID(),
									warehouseOwnerId, tempario);
					// System.out.println(this.getClass().getName()+".tableChanged codes..."+codes);
					MProductPrice sp = null;
					Boolean allowChanges = false;
					if (codes != null) {
						sp = MProductPrice
								.get(Env.getCtx(), codes.get("SALES"),
										ipl.getM_Product_ID(), null);
						try {
							if (sp != null) {
								// System.out.println(this.getClass().getName()+".tableChanged  sp.."+sp);
								allowChanges = (Boolean) sp
										.get_Value("smj_isallowchanges");
							} else {
								Messagebox.show(Msg.translate(Env.getCtx(),
										"SMJMSGCantFindPriceList"), labelInfo,
										Messagebox.OK, Messagebox.EXCLAMATION);
								loadTable();
								return;
							}
						} catch (Exception e) {
							log.log(Level.SEVERE,
									this.getClass().getName()
									+ ".tableChanged - ERROR: "
									+ e.getMessage(), e);
						}
					} else {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGCantFindPriceList"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}
					// System.out.println(this.getClass().getName()+".tableChanged   allowchanges: "+sp+"..limit..."+sp.getPriceLimit());
					Integer state = (Integer) stateListbox.getSelectedItem()
							.getValue();
					if (state.equals(stateGarranty)) {
						if (price.compareTo(Env.ZERO) < 0) {
							Messagebox.show(Msg.translate(Env.getCtx(),
									"SMJMSGNOValueLowerZero"), labelInfo,
									Messagebox.OK, Messagebox.EXCLAMATION);
							loadTable();
							return;
						} else {
							ipl.setPrice(price);
						}
					} else if (allowChanges) {
						if (price.compareTo(sp.getPriceLimit()) < 0) {
							// System.out.println(this.getClass().getName()+"tableChanged....limite mayor o igual..."+price.compareTo(sp.getPriceLimit()));
							Messagebox.show(Msg.translate(Env.getCtx(),
									"SMJMSGBelowLimitPrice"), labelInfo,
									Messagebox.OK, Messagebox.EXCLAMATION);
							loadTable();
							return;
						} else {
							ipl.setPrice(price);
							// ipl.setQtyEntered(qty);
							// ipl.settotal(ipl.getQtyEntered().multiply(ipl.getPrice()));
							// ipl.save();
							// list.remove(index);
							// list.add(index, ipl);
						}
					} else {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGNoPriceChange"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}
				}// columna precio
				if (evx.getColumn() == 4) {
					
					Boolean isExternalService = ipl.get_ValueAsBoolean("smj_isexternallservice");
					if(isExternalService) 
					{
						Messagebox.show("No puede modificar cantidad para Servicio Externo", labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}
					
				}// columna cantidad
				if (!ipl.issmj_isservice()) {

					BigDecimal qtyWh = DataQueriesTrans.getTotalProductByLocator(
							ipl.getM_Product_ID(), ipl.getlocator_ID(), null);
					// System.out.println(this.getClass().getCanonicalName()+"...qty: "+qty+"..qtyWh: "+qtyWh);
					if (qty.compareTo(qtyWh) > 0) {
						Messagebox
						.show(Msg.translate(Env.getCtx(),
								"SMJMSGQtyEntered"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}
				}// if a not services
				if (evx.getColumn() == 9) {
					try {
						// System.out.println(this.getClass().getName()+".tableChanged por mechanico..."+mec);
						Integer mecId = DataQueriesTrans.getBpartnerId(
								Env.getAD_Client_ID(Env.getCtx()), mec);
						MBPartner bMechanic = MBPartner
								.get(Env.getCtx(), mecId);
						if (bMechanic != null) {
							// System.out.println(this.getClass().getName()+".tableChanged ...mec..."+bMechanic.getName());
							Boolean isMec = bMechanic
									.get_ValueAsBoolean("smj_ismechanic");
							// System.out.println(this.getClass().getName()+".tableChanged isMec..."+isMec);
							if (isMec) {
								// System.out.println(this.getClass().getName()+".tableChanged doSomething...");
								if (ipl.getwolstate().equals("S")) {
									asignMechanic(bMechanic, ipl);
								} else {
									Messagebox.show(Msg.translate(Env.getCtx(),
											"SMJMSGNoAsignateMecSimple"),
											labelInfo, Messagebox.OK,
											Messagebox.EXCLAMATION);
								}
								loadTable(); // refresh the status of the
								// activites
								productTextBox.setFocus(true); // set the focus
								// to thsi field

							} else {
								showEditActivity(ipl.getM_Product_ID(),
										ipl.getsmj_workOrderLine_ID(), index);
							}
						} else {
							showEditActivity(ipl.getM_Product_ID(),
									ipl.getsmj_workOrderLine_ID(), index);
						}
						return; // fix - it was showing duplicated activities
						// when a mechanic was modified in the first
						// line
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName()
								+ ".tableChanged - ERROR: " + e.getMessage(), e);
					}
				}// if a mechanic
				ipl.setQty(qty);

				ipl.settotal(ipl.getQty().multiply(ipl.getPrice()));
				ipl.save();
				list.remove(index);
				list.add(index, ipl);
				loadTable();
			}
			// dataTable.setFocus(true);
			productTextBox.setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".tableChanged:: " + e.getMessage(), e);
		}
	}// tableChanged

	/**
	 * se ejecuta cuando sucede un evento - it's executes an event occurs
	 */
	@Override
	public void onEvent(Event ev) throws Exception {

		if (ev.getTarget().equals(productTextBox)) {
			String value = productTextBox.getValue();
			if (value.length() <= 0 || value.trim().equals("")) {
				return;
			}
			if (!isEditable) {
				// System.out.println(this.getClass().getCanonicalName()+" - productTextBox");
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			showInfoProduct(value.trim().toUpperCase());
			// }//if / else
			productTextBox.setValue("");
			productTextBox.setFocus(true);
			return;
		}// productTextBox
		else if (ev.getTarget().equals(deleteButton)) {
			if (!isEditable) {
				// System.out.println(this.getClass().getCanonicalName()+" - deletebutton");
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			try {
				Integer index = dataTable.getSelectedIndex();
				if (index != null && index >= 0) {
					MSMJWorkOrderLine woLine = list.get(index);
					if (woLine.issmj_isservice()
							&& !woLine.getwolstate().equals("S")) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGNoDeleteActivity"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}

					list.remove(woLine);
					woLine.delete(true);
					loadTable();
				} else {
					Messagebox
					.show(Msg.translate(Env.getCtx(),
							"SMJMSGSelectItemList"), labelInfo,
							Messagebox.OK, Messagebox.EXCLAMATION);
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".onEvent ERROR:: ", e);
			}
			return;
		} // delete button
		else if (ev.getTarget().equals(
				confirmPanel.getButton(ConfirmPanel.A_OK))) {
			if (!isEditable) {
				// System.out.println(this.getClass().getCanonicalName()+" - OK");
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			if (bpartnerId <= 0) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGSelectBpartner"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if (list.size() <= 0) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGAddItemList"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			// if(vehicleID <=0){
			// Messagebox.show(Msg.translate(Env.getCtx(),
			// "SMJMSGOnlyVehicleRegistred"), labelInfo, Messagebox.OK,
			// Messagebox.EXCLAMATION);
			// return;
			// }
			// System.out.println(this.getClass().getCanonicalName()+".onEvent OK antes processs");
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			Boolean ok = finishWorkOrder(trx);
			Integer whOrderId = 0;
			if (ok) {
				if (isRmnFac) {
					whOrderId = completeRemanufactured(trx);
				
					//---modificacion iMarch 06/03/2020  agrega Ov y FactCxP a OM remanu
					if (whOrderId > 0) 
						tmpRequest.setC_Order_ID(whOrderId.intValue());
					if (invId > 0) 
						tmpRequest.setC_Invoice_ID(invId.intValue());
		
					ok = tmpRequest.save();	
					//---fin 06/03/2020
				}
			}
			if (ok && whOrderId >= 0) {
				
				if (whOrderId < 0) { // (whOrderId <= 0) 
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGRequestComplete"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
				}

				if (whOrderId > 0 && isRmnFac) {
					ShowWindowTrans.openSalesOrder(whOrderId);
				}
				if (invId > 0) {
					ShowWindowTrans.openInvoiceVendor(invId);
				}

				trx.commit();
				clean();
			} else {
				trx.rollback();
			}
			trx.close();
			return;
		}// OK
		else if (ev.getTarget().equals(
				confirmPanel.getButton(ConfirmPanel.A_CANCEL))) {
			org.adempiere.webui.component.Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantCancel"), labelInfo,
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new Callback<Integer>() {

				@Override
				public void onCallback(Integer result) {
					SessionManager.getAppDesktop().closeActiveWindow();							
				}
			});
			return;
		}// CANCEL
		else if (ev.getTarget().equals(searchPlate.getComponent())) {
			String plate = searchPlate.getComponent().getText().toUpperCase();
			if (plate == null || plate.length() <= 0 || plate.trim().equals("")) {
				return;
			}
			if (!isEditable) {
				// System.out.println(this.getClass().getCanonicalName()+" - plate");
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			// aqui se valida si la placa existe, si no se busca como numero de
			// vehiculo
			int vehicleIdT = DataQueriesTrans.getVehicleId(plate);
			if (vehicleIdT <= 0) // no tiene placa valida, se busca como numero
				// de vehiculo,
			{
				String aPlate = plate;
				plate = DataQueriesTrans.getVehicleByInternalNumber(aPlate);
				if (plate.equalsIgnoreCase(""))  // la placa no tien ni numero interno ni placa valida, se deja la digitada
				{
					plate = aPlate;
				}
				selectedPlate = plate;
			}
			else {
				plate = searchPlate.getComponent().getText().toUpperCase();
				selectedPlate = plate;
			}
			String states = stateComplete + "," + stateInvoiced;
			String rCodes = getWOOpen(plate, states);
			// Boolean enter = true;
			if (rCodes.length() > 0) {
				showInfoWO(plate, states);
				// enter = false;
			}// if (rCodes.length()>0)
			else {
				setVehicle(plate);
			}
			bpartnerSearch.getComponent().setFocus(true);
			// if (enter){
			// //
			// System.out.println(this.getClass().getCanonicalName()+".onEvent plate por enter..."+enter);
			// setVehicle(plate.toUpperCase().trim());
			// }//if (enter)
			return;
		}// modify activity
		else if (ev.getTarget().equals(editButton)) {
			if (bpartnerId > 0) {
				try {
					Integer index = dataTable.getSelectedIndex();
					if (index != null && index >= 0) {
						MSMJWorkOrderLine ipl = list.get(index);
						showEditActivity(ipl.getM_Product_ID(),
								ipl.getsmj_workOrderLine_ID(), index);
					} else {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGSelectItemList"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
					}
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()
							+ ".onEvent ERROR:: ", e);
				}
			} else {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGSelectBpartner"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
			return;
		}// editButton
		else if (ev.getTarget().equals(findButton)) {
			clean();
			showInfoWO("", "");
			return;
		} // findButton
		else if (ev.getTarget().equals(dataTable)) {
			if (bpartnerId > 0) {
				try {
					Integer index = dataTable.getSelectedIndex();
					// System.out.println(this.getClass().getCanonicalName()+".index: "+index+"..isEditable.."+isEditable);
					if (index != null && index >= 0) {// && isEditable
						MSMJWorkOrderLine ipl = list.get(index);
						showEditActivity(ipl.getM_Product_ID(),
								ipl.getsmj_workOrderLine_ID(), index);
					} else {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGSelectItemList"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
					}
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()
							+ ".onEvent ERROR:: ", e);
				}
			} else {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGSelectBpartner"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
			return;
		}// dataTable
		else if (ev.getTarget().equals(stateListbox)) {
			Integer state = (Integer) stateListbox.getSelectedItem().getValue();
			// System.out.println(this.getClass().getName()+".onEvent --- lista."+isReOpen+". state..."+state);
			if (state.equals(stateReOpen) && isReOpen) {
				// System.out.println(this.getClass().getName()+"onEvent .. reabrir orden ...");
				tmpRequest.setR_Status_ID(stateReOpen);
				Trx trx = Trx.get(Trx.createTrxName("AL"), true);
				tmpRequest.set_TrxName(trx.getTrxName());
				Boolean ok = tmpRequest.save();
				isEditable = ok;
				if (ok) {
					trx.commit();
				} else {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGErrorSavingData"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					trx.rollback();
				}
				trx.close();
				lockFields(true, true);
				return;
			}
			if (!isEditable) {
				// System.out.println(this.getClass().getCanonicalName()+" - stateListbox");
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			// System.out.println(this.getClass().getCanonicalName()+" Cambio lista de estados....");

			if (state.equals(stateComplete) || state.equals(stateInvoiced)) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGStateNoSelectable"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				Integer upState = stateDefault;
				if (tmpRequest != null && tmpRequest.getR_Status_ID() > 0) {
					upState = tmpRequest.getR_Status_ID();
				}
				stateListbox.setValue(upState);
			}
			if (state.equals(stateGarranty)) {
				if (tmpRequest != null) {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGNoChWarrantyWO"), labelInfo,
							Messagebox.OK, Messagebox.EXCLAMATION);
					stateListbox.setValue(tmpRequest.getR_Status_ID());
					return;
				}

				String garrantyDoc = garrantyTextbox.getText().trim();

				if (garrantyDoc == null || garrantyDoc.length() <= 0 || garrantyDoc.equals("")) {
					showInfoWOGarranty();
				}

				garrantyLabel.setVisible(true);
				garrantyTextbox.setVisible(true);
				addlinegarrantyBtn.setVisible(true);//modificacion iMarch
			}// if (state.equals(stateGarranty))
			else {
				garrantyLabel.setVisible(false);
				garrantyTextbox.setVisible(false);
				addlinegarrantyBtn.setVisible(false);//modificacion iMarch
			}
		}// stateListbox
		else if (ev.getTarget().equals(cleanButton)) {
			org.adempiere.webui.component.Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSureCleanWindow"), labelInfo, Messagebox.OK | Messagebox.CANCEL,
					Messagebox.QUESTION, new Callback<Integer>() {

				@Override
				public void onCallback(Integer result) {
					if (result == Messagebox.OK) {
						clean();
					}
				}
			});
		}// if cleanButton
		else if (ev.getTarget().equals(requestButton)) { // solicitud 1000430
			if (tmpRequest.getR_Status_ID() == stateDefault) {
				ShowWindowTrans.openRequestOrder(tmpRequest.get_ID());
			} else {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGNonValidOrderStatus"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
			}
		}// if requestButton

		else if (ev.getTarget().equals(createButton)) {
			// System.out.println(this.getClass().getName()+".onEvent Create button");
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			Boolean ok = saveRequest(trx.getTrxName());
			if (ok) {
				trx.commit();
				lockFields(true, true);
			} else {
				trx.rollback();
			}
			trx.close();
			return;
		}// createButton
		else if (ev.getTarget().equals(nextProgCheckbox)) {
			Boolean isNext = nextProgCheckbox.isChecked();
			// System.out.println(this.getClass().getName()+".onEvent nextProg..."+isNext);
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				nextProgCheckbox.setChecked(!isNext);
				return;
			}
			if (tmpRequest == null) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGCreateRequest"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			// System.out.println(this.getClass().getName()+".onEvent do Something");
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			Boolean ok = saveRequest(trx.getTrxName());
			if (ok) {
				trx.commit();
			} else {
				trx.rollback();
			}
			trx.close();
			return;
		}// nextProgCheckBox
		else if (ev.getTarget().equals(deleteWOButton)) {
			if (tmpRequest != null
					&& (tmpRequest.getR_Status_ID() == stateDefault || tmpRequest
					.getR_Status_ID() == stateProgram)) {
				org.adempiere.webui.component.Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWOSureDelete"),
						labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new Callback<Integer>() {

					@Override
					public void onCallback(Integer result) {
						deleteWorkOrden();
					}
				});
			}
			return;
		}// deleteWOButton
		else if (ev.getTarget().equals(requestTypeListbox)) {
			//request Type
			Integer value = (Integer) requestTypeListbox.getSelectedItem()
					.getValue();
			// System.out.println(this.getClass().getName()+".onEvent requestTypeListbox.."+value);
			if (value.equals(rTypeRmnFac)) {
				isRmnFac = true;
				setRemanufactured();
			} else {
				isRmnFac = false;
			}
			// System.out.println(this.getClass().getName()+".onEvent ...isRmnFac.."+isRmnFac);
		}//requestTypeList
		else if (ev.getTarget().equals(addlinegarrantyBtn)) {
			
			String docNo = garrantyTextbox.getValue();
			Integer code = DataQueries.getWOByDocumentNo(docNo, Env.getAD_Client_ID(Env.getCtx()));
			m_Summary=tmpRequest.getSummary();
			m_R_Request_ID = tmpRequest.getR_RequestRelated_ID();
			m_C_BPartner_ID = tmpRequest.getC_BPartner_ID();
			m_DocumentNo = garrantyTextbox.getValue();
			showInfoWOGarrantyLines(code, docNo);
			
		}//addlinegarrantyBtn
		// dataTable.setFocus(true);
	}// onEvent

	/**
	 * muestra la ventana de informacion de productos y trae el producto
	 * seleccionado - show info product window and returns selected product data
	 * 
	 * @param value
	 */
	private void showInfoProduct(String value) {
		try {
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			if (tmpRequest == null) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGCreateRequest"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
		}
		final int rdm = UtilTrans.getRdmInteger();
		SMJSearchProducts search = new SMJSearchProducts();
		
		if (temparioName == null || temparioName.length() <= 0) {
			MSMJServiceRateGroup srg = new MSMJServiceRateGroup(Env.getCtx(),
					serviceRateGroupDefault, null);
			if (srg != null) {
				temparioName = srg.getName();
			}
		}

		String locatorBranch = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"))).getName();
		LinkedList<InfoProductList> listProd = search.searchProducts_Locator(value, locatorBranch, true, temparioName);

		if (listProd.size() == 1) {
			MSMJWorkOrderLine ipl = new MSMJWorkOrderLine(Env.getCtx(), 0, null);
			Iterator<InfoProductList> itl = listProd.iterator();
			InfoProductList dato = itl.next();

			ipl.setM_Product_ID(dato.getProductID());
			ipl.setPrice(dato.getPrice());
			ipl.setlocator_ID((dato.getLocatorId() == null) ? 0 : dato.getLocatorId());
			ipl.setprovider_ID(dato.getPartnerId());
			ipl.setQty(dato.getQty());
			ipl.setpurchasePrice(dato.getPurchasePrice());
			ipl.setsmj_estimatedtime(dato.getEstimedTime());
			ipl.setsmj_isallowchanges(dato.getIsAllowChange());

			Integer state = (Integer) stateListbox.getSelectedItem().getValue();
			MProduct p = MProduct.get(Env.getCtx(), ipl.getM_Product_ID());
			Boolean isService = p.getProductType().equals("S") ? true : false;
			ipl.setsmj_isservice(isService);
			if (isService) {
				ipl.setwolstate("S");
			}
			try {
				if (state.equals(stateGarranty)) {
					if (!isService) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGOnlyAllowServGarranty"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}// if (!isService)
				}// estado garantia..
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()
						+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
			}
			ipl.setProductValue(p.getValue());
			ipl.setProductName(p.getName());
			try {
				BigDecimal tax = DataQueriesTrans.getTaxValue(p
						.getC_TaxCategory_ID());
				if (tax != null) {
					ipl.settaxvalue(tax);
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
			}
			// ipl.setpartnername(DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()),
			// ipl.getprovider_id()));
			ipl.setQty(Env.ONE);
			Integer attInstance = DataQueriesTrans.getAttInstanceId(
					ipl.getlocator_ID(), ipl.getM_Product_ID());
			ipl.setM_AttributeSetInstance_ID(attInstance);
			ipl.settotal(ipl.getQty().multiply(ipl.getPrice()));
			Boolean isExternal = p.get_ValueAsBoolean("smj_isexternallservice");
			// System.out.println(this.getClass().getCanonicalName()+"externallllllll "+isExternal);
			ipl.setsmj_isexternallservice(isExternal);
			ipl.setsmj_iscommission(p.get_ValueAsBoolean("smj_iscommission"));
			ipl.setdateopen(Env.getContextAsDate(Env.getCtx(), "#Date"));
			MLocator loc = MLocator.get(Env.getCtx(), ipl.getlocator_ID());
			MWarehouse wh = MWarehouse.get(Env.getCtx(),
					loc.getM_Warehouse_ID());
			ipl.setWarehouseName(wh.getName());
			try {
				// if (exist){
				ipl.setR_Request_ID(tmpRequest.getR_Request_ID());
				ipl.saveEx();
				list.add(ipl);
				loadTable();
				// }
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".showInfoProduct - ERROR: " + e.getMessage(), e);
			}
		} else {
			SMJInfoProduct ip = new SMJInfoProduct(rdm, "M_Product",
					"M_Product_ID", false, "", listProd, true, temparioName);
			ip.setVisible(true);
			ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelSearchProducts"));
			ip.setStyle("border: 2px");
			ip.setClosable(true);
			ip.addEventListener(new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					MSMJWorkOrderLine ipl = new MSMJWorkOrderLine(Env.getCtx(), 0, null);
					String product = Env.getContext(Env.getCtx(), rdm, "PRODUCT");
					ipl.setM_Product_ID(Integer.parseInt(product.trim()));
					String price = Env.getContext(Env.getCtx(), rdm, "PRICE");
					ipl.setPrice(new BigDecimal(price));
					String locator = Env.getContext(Env.getCtx(), rdm, "LOCATOR");
					try {
						ipl.setlocator_ID(Integer.parseInt(locator.trim()));
					} catch (Exception e) {
					}
					String partner = Env.getContext(Env.getCtx(), rdm, "PARTNER");
					try {
						ipl.setprovider_ID(Integer.parseInt(partner.trim()));
					} catch (Exception e) {
					}
					String qty = Env.getContext(Env.getCtx(), rdm, "QTY");
					try {
						ipl.setQty(new BigDecimal(qty));
					} catch (Exception e) {
					}
					String pprice = Env.getContext(Env.getCtx(), rdm, "PURCHASE");
					try {
						ipl.setpurchasePrice(new BigDecimal(pprice));
					} catch (Exception e) {
					}
					String hour = Env.getContext(Env.getCtx(), rdm, "HOUR");
					try {
						ipl.setsmj_estimatedtime(new BigDecimal(hour));
					} catch (Exception e) {
					}
					String allow = Env.getContext(Env.getCtx(), rdm, "ALLOWCH");
					// System.out.println("Allow....."+allow);
					Boolean isAllow = allow.equals("TRUE") ? true : false;
					ipl.setsmj_isallowchanges(isAllow);

					Integer state = (Integer) stateListbox.getSelectedItem().getValue();
					MProduct p = MProduct.get(Env.getCtx(), ipl.getM_Product_ID());
					Boolean isService = p.getProductType().equals("S") ? true : false;
					ipl.setsmj_isservice(isService);
					if (isService) {
						ipl.setwolstate("S");
					}
					try {
						if (state.equals(stateGarranty)) {
							if (!isService) {
								Messagebox.show(Msg.translate(Env.getCtx(),
										"SMJMSGOnlyAllowServGarranty"), labelInfo,
										Messagebox.OK, Messagebox.EXCLAMATION);
								return;
							}// if (!isService)
						}// estado garantia..
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName()
								+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
					}
					ipl.setProductValue(p.getValue());
					ipl.setProductName(p.getName());
					try {
						BigDecimal tax = DataQueriesTrans.getTaxValue(p
								.getC_TaxCategory_ID());
						if (tax != null) {
							ipl.settaxvalue(tax);
						}
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()
								+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
					}
					// ipl.setpartnername(DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()),
					// ipl.getprovider_id()));
					ipl.setQty(Env.ONE);
					Integer attInstance = DataQueriesTrans.getAttInstanceId(
							ipl.getlocator_ID(), ipl.getM_Product_ID());
					ipl.setM_AttributeSetInstance_ID(attInstance);
					ipl.settotal(ipl.getQty().multiply(ipl.getPrice()));
					Boolean isExternal = p.get_ValueAsBoolean("smj_isexternallservice");
					// System.out.println(this.getClass().getCanonicalName()+"externallllllll "+isExternal);
					ipl.setsmj_isexternallservice(isExternal);
					ipl.setsmj_iscommission(p.get_ValueAsBoolean("smj_iscommission"));
					ipl.setdateopen(Env.getContextAsDate(Env.getCtx(), "#Date"));
					MLocator loc = MLocator.get(Env.getCtx(), ipl.getlocator_ID());
					MWarehouse wh = MWarehouse.get(Env.getCtx(),
							loc.getM_Warehouse_ID());
					ipl.setWarehouseName(wh.getName());

					try {
						// if (exist){
						ipl.setR_Request_ID(tmpRequest.getR_Request_ID());
						ipl.save();
						list.add(ipl);
						loadTable();
						// }
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()
								+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
					}
				}
			});
			ip.setParent(form);
			AEnv.showCenterScreen(ip);
		}
	}// showInfoProduct

	/**
	 * valida existencias ingresadas no superen las existencias en almacen -
	 * validate qty entered no exced qty in warehouse
	 * 
	 * @return
	 */
	private Boolean validateData() {
		Boolean flag = true;
		StringBuffer activities = new StringBuffer();
		Boolean inComplete = false;
		int countRmnFac = 0;
		tiresDoit = false;
		countTiresAct = 0;
		try {
			if ((temparioName.length() <= 0 || temparioName.equals(""))
					&& !isRmnFac) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGOnlyVehicleTempario"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return false;
			}
			try {
				HashMap<String, Object> vd = getVehicledata(selectedPlate);
				if (vd != null) {
					String tName = (String) vd.get("TEMPA");
					if (tName != null) {
						temparioName = tName.trim();
					} else {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGOnlyVehicleTempario"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
					if ((temparioName.length() <= 0 || temparioName.equals(""))
							&& !isRmnFac) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGOnlyVehicleTempario"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()
						+ ".validateData - ERROR: " + e.getMessage(), e);
			}
			if ((tmpRequest.getR_RequestType_ID() == rTypeRmnFac)
					&& !(tmpRequest.getC_BPartner_ID() == userSeveter)) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGUserMustBeSeveter"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			Iterator<MSMJWorkOrderLine> itl = list.iterator();
			while (itl.hasNext()) {
				MSMJWorkOrderLine xp = itl.next();
				MProduct p = MProduct.get(Env.getCtx(), xp.getM_Product_ID());
				if (xp.issmj_isservice()) {
					if (xp.issmj_isexternallservice()) {
						if (xp.gettotal().compareTo(Env.ZERO) <= 0
								|| xp.getC_InvoiceLine_ID() <= 0) {
							Messagebox.show(Msg.translate(Env.getCtx(),
									"SMJMSGServExtNoInvoice"), labelInfo,
									Messagebox.OK, Messagebox.EXCLAMATION);
							return false;
						}
					}// if external service
					if (xp.getwolstate().equals("S")) {
						activities.append(xp.getProductName() + "; ");
						inComplete = true;
					}
					if (p != null && p.get_ValueAsBoolean("smj_istires")) {
						countTiresAct++;
						// System.out.println(this.getClass().getName()+".validateData ..por tirs..."+xp.getwolstate()+"......"+tmpRequest.get_ValueAsBoolean("smj_istiresok"));
						if (!xp.getwolstate().equals("C")) {
							if (!tmpRequest.get_ValueAsBoolean("smj_istiresok")) {

								// isTires=false;
							} else {
								tiresDoit = true;
							}
						} else {
							tiresDoit = true;
						}
					}// if valida actividad llantas
				}// if service
				else {
					BigDecimal qty = DataQueriesTrans.getTotalProductByLocator(
							xp.getM_Product_ID(), xp.getlocator_ID(), null);
					// System.out.println("new..."+xp.getQtyEntered()+"...exis..."+qty);
					if (xp.getQty().compareTo(qty) > 0) {
						Messagebox
						.show(Msg.translate(Env.getCtx(),
								"SMJMSGQtyEntered"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}// if qty
				}// if/else Service

				Integer pCat = p.getM_Product_Category_ID();
				if (p != null && pCat.equals(rmnFacCategory)) {
					countRmnFac++;
				}
			}// while
			if (inComplete) {
				Messagebox.show(
						Msg.translate(Env.getCtx(),
								"SMJMSGActivitiesNoComplete")
						+ " : "
						+ activities.toString(), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			if (isRmnFac && countRmnFac > 1) {
				Messagebox
				.show(Msg.translate(Env.getCtx(),
						"SMJMSGOnlyAllowOneRmnProd")
						+ " : "
						+ activities.toString(), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			// System.out.println(this.getClass().getName()+".validateData ..coubt.."+countTiresAct+"..doit..."+tiresDoit);
			if (countTiresAct > 0 && !tiresDoit) {
				tiresOk = false;
			} else {
				tiresOk = true;
			}
			// System.out.println(this.getClass().getName()+".tiresOk..."+tiresOk);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".validateData ERROR:: " + e.getMessage(), e);
		}
		return flag;
	}// validateData

	/***
	 * limpia los datos de la ventana - Clean data window
	 */
	private void clean() {
		bpartnerId = 0;
		bpartnerName = "";
		bpartnerSearch.setValue("");
		vehicle = null;
		vehicleID = 0;
		kmIntbox.setValue(Env.ZERO);
		kmOld = Env.ZERO;
		searchPlate.setValue(null);
		searchPlate.setReadWrite(false);
		// solicitud 1000432
		vehicleID = 0;
		selectedPlate = "";
		vehicle = null;
		//
		bpartnerSearch.getComponent().setText("");
		woTextBox.setValue("");
		locationID = 0;
		grandTotal = Env.ZERO;
		taxTotal = Env.ZERO;
		subTotal = Env.ZERO;
		tmpRequest = null;
		temparioName = "";
		stateListbox.setValue(0);
		descTextBox.setValue("");
		descTextBox.setReadonly(false);
		productTextBox.setFocus(true);
		isEditable = true;
		list = new LinkedList<MSMJWorkOrderLine>();
		nextProgLabel.setVisible(false);
		nextProgCheckbox.setVisible(false);
		nextProgCheckbox.setChecked(false);
		numReqCustomerTextBox.setValue("");
		diagnosisTextBox.setValue("");
		lockFields(true, true);
		createButton.setDisabled(false);
		tiresOk = false;
		loadTable();
		deleteWOButton.setVisible(false);
		requestTypeListbox.setValue(rTypeWO);
		isRmnFac = false;
		garrantyTextbox.setValue("");
		addlinegarrantyBtn.setVisible(false);//modificacion iMarch
		kmIntbox.setReadonly(false);
		tiresDoit = true;
		countTiresAct = 0;
		searchPlate.setReadWrite(true);
		bpartnerSearch.setReadWrite(true);
		searchPlate.getComponent().setFocus(true);
		//modificacion iMarch---
		stateListbox.setEnabled(true);
		requestTypeListbox.setEnabled(true);
		m_C_BPartner_ID=0;
		m_DocumentNo="";
		m_R_Request_ID=0;
		m_Summary="";
		invId=0;
		//----------------------
	}// clean

	/**
	 * valida que no exista el elemento en la lista, si no existe regresa true -
	 * validate no exist element in list, if no exist return true
	 * 
	 * @param ipl
	 * @return
	 */
	@SuppressWarnings("unused")
	private Boolean validateElementExist(MSMJWorkOrderLine ipl) {
		Boolean flag = true;
		try {
			Iterator<MSMJWorkOrderLine> it = list.iterator();
			while (it.hasNext()) {
				MSMJWorkOrderLine ipx = it.next();
				if ((ipl.getM_Product_ID() == ipx.getM_Product_ID())
						&& (ipl.getlocator_ID() == ipx.getlocator_ID())) {
					Messagebox.show(
							Msg.getMsg(Env.getCtx(), "SMJMSGItemListExist"),
							labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
			}// while
		} catch (Exception e) {
			flag = false;
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".validateElementExist ERROR:: ", e);
		}
		return flag;
	}// validateElementExist

	/**
	 * valida la informacion de tercero. - validate bpartner information
	 */
	private void setPartner() {
		try {
			bpartnerName = DataQueriesTrans.getBpartnerName(
					Env.getAD_Client_ID(Env.getCtx()), bpartnerId);
			// System.out.println(this.getClass().getName()+".setPartner - name...."+bpartnerName+"..ID.."+bpartnerId);
			locationID = DataQueriesTrans.getLocationPartner(bpartnerId);
			if (bpartnerId <= 0) {
				bpartnerName = "";
				locationID = 0;
			}
			if (locationID <= 0) {
				try {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGPartnerValidLocation"), labelInfo,
							Messagebox.OK, Messagebox.EXCLAMATION);
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()
							+ ".setPartner ERROR:: ", e);
				}
				bpartnerId = 0;
				bpartnerName = "";
				bpartnerSearch.getComponent().setText(bpartnerName);
				bpartnerSearch.setValue(bpartnerId);
				return;
			}// if location
			LinkedList<String> plList = DataQueriesTrans.getPlateList(null,
					bpartnerId);

			bpartnerSearch.getComponent().setText(bpartnerName);
			descTextBox.setFocus(true);
			if (isRmnFac) {
				return;
			}
			try {
				String plate = searchPlate.getComponent().getText().toUpperCase().trim();
				if (plate == null || plate.equals("")) {
					if (!isRmnFac) {
						if (plList != null && plList.size() > 0) {
							showInfoPlate(MBPartner.get(Env.getCtx(), bpartnerId).getName());							
						}
					}
				} else {
					if (selectedPlate == null) {
						setVehicle(plate);
					}
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".setPartner ERROR:: ", e);
			}
			// listTmpSales();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".setPartner ERROR:: ", e);
		}
	}// setPartner

	/**
	 * llena los datos de vehiculo - set vehicle data
	 * 
	 * @param plate
	 */
	private void setVehicle(String plate) {
		try {
			// System.out.println(this.getClass().getCanonicalName()+".setVehicle plate.."+plate);
			if (plate == null && !isRmnFac) {
				return;
			}
			selectedPlate = plate.toUpperCase();
			HashMap<String, Object> vd = getVehicledata(plate.toUpperCase());
			if (vd != null) {
				searchPlate.getComponent().setText((String) vd.get("PLATE"));
				selectedPlate = (String) vd.get("PLATE");
				vehicleID = (Integer) vd.get("VEHICLE");
				vehicle = new MSMJVehicle(Env.getCtx(), vehicleID, null);
				BigDecimal km = vehicle.getkms();
				// if (tmpRequest != null ){
				// km = new
				// BigDecimal(tmpRequest.get_ValueAsString("kmActual"));
				// if (km != null && km.compareTo(vehicle.getkms()) > 0){
				//
				// }
				// }
				kmIntbox.setValue(km);
				kmOld = kmIntbox.getValue();
				bpartnerId = (Integer) vd.get("OWNER");
				bpartnerSearch.setValue(bpartnerId);
				bpartnerName = DataQueriesTrans.getBpartnerName(
						Env.getAD_Client_ID(Env.getCtx()), bpartnerId);
				try {
					String tName = (String) vd.get("TEMPA");
					if (tName != null) {
						temparioName = tName.trim();
					}
				} catch (Exception e) {
					log.log(Level.SEVERE,
							this.getClass().getName()
							+ ".setVehicle - ERROR: temparioName ::"
							+ e.getMessage(), e);
				}
				// System.out.println(this.getClass().getName()+".setVehicle ABC temp...."+temparioName+"...vehicle..."+vehicleID);
				if ((temparioName == null || temparioName.trim().equals("") || temparioName
						.length() <= 0) && !isRmnFac) {
					if (vehicleID > 0) {
						vehicleID = 0;
						selectedPlate = "";
						bpartnerId = 0;
						vehicle = null;
						searchPlate.getComponent().setText("");
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGOnlyVehicleTempario"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}
				}
				if (bpartnerId > 0) {
					setPartner();
				}
				try {
					if (vehicle.getodometer() == null
							|| !vehicle.getodometer().equals("F")) {
						kmIntbox.setReadonly(true);
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGNoOdometerOK"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					} else {
						kmIntbox.setReadonly(false);
					}
				} catch (Exception e) {
					kmIntbox.setReadonly(true);
					log.log(Level.SEVERE, this.getClass().getCanonicalName()
							+ ".setVehicle ERROR:: ", e);
					Messagebox.show(
							Msg.translate(Env.getCtx(), "SMJMSGNoOdometerOK"),
							labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
			}// if (vd != null){
			if (temparioName == null || temparioName.length() <= 0) {
				MSMJServiceRateGroup srg = new MSMJServiceRateGroup(
						Env.getCtx(), serviceRateGroupDefault, null);
				if (srg != null) {
					temparioName = srg.getName();
				}
			}
			// System.out.println(this.getClass().getName()+".setVehicle temp...."+temparioName+"...vhId..."+vehicleID);
		} catch (Exception ex) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".setVehicle ERROR:: ", ex);
		}
	}// setVehicle

	/**
	 * Llena el combo de estados fill state list
	 * 
	 * @param state
	 */
	private void listStates(Integer state) {
		stateListbox.removeAllItems();
		Vector<Vector<Object>> data = DataQueriesTrans.getStateList(woListStates,
				state);
		stateListbox.appendItem("", 0);
		for (int i = 0; i < data.size(); i++) {
			Vector<Object> line = data.get(i);
			stateListbox.appendItem((String) line.get(1), line.get(0));
		}// for
		stateListbox.setMold("select");
		stateListbox.addActionListener(this);
		stateListbox.setValue(0);
	}// listStates

	/**
	 * muestra la ventana de informacion de ordenes de trabajo y trae la
	 * seleccionada - show info work Order window and returns selected data
	 */
	private void showInfoWO(final String plate, String noStates) {

		Callback<Integer> callback = new Callback<Integer>() {

			@Override
			public void onCallback(Integer code) {
				if (code != null) {
					try {
						tmpRequest = new MRequest(Env.getCtx(), code, null);
						woTextBox.setValue(tmpRequest.getDocumentNo());
						descTextBox.setValue(tmpRequest.getSummary());
						Boolean lockReopen = false;
						if (isReOpen
								&& (tmpRequest.getR_Status_ID() == stateComplete || tmpRequest
								.getR_Status_ID() == stateReOpen)) {
							// System.out.println(this.getClass().getName()+".showInfoWO por reop y complete...");
							listStates(0);
							lockReopen = true;
						} else {
							if (tmpRequest.getR_Status_ID() == stateReOpen) {
								listStates(0);
							} else {
								listStates(stateReOpen);
							}
						}
						// System.out.println(this.getClass().getName()+".showInfoWO reOpen.."+isReOpen+"..lockre.."+lockReopen);
						stateListbox.setValue(tmpRequest.getR_Status_ID());
						if (tmpRequest.getR_RequestType_ID() == rTypeRmnFac) {
							isRmnFac = true;
						} else {
							isRmnFac = false;
						}
						vehicleID = tmpRequest.get_ValueAsInt("smj_vehicle_ID");
						if (vehicle == null && vehicleID > 0) {
							vehicle = new MSMJVehicle(Env.getCtx(), vehicleID, null);
						}
						try {
							String skmActual = tmpRequest.get_ValueAsString("kmActual");
							// System.out.println(this.getClass().getName()+".showInfoWO -  kmActual.."+skmActual);
							if (skmActual != null && skmActual.length() > 0) {
								kmIntbox.setValue(new BigDecimal(skmActual));
								kmOld = kmIntbox.getValue();
							}
						} catch (Exception e) {
							log.log(Level.SEVERE,
									this.getClass().getName()
									+ ".showInfoWO - ERROR: kmActual - "
									+ e.getMessage(), e);
						}// try/catch kmActual
						try {
							selectedPlate = tmpRequest.get_ValueAsString("smj_plate");
							if (selectedPlate != null && selectedPlate.length() > 0) {
								HashMap<String, Object> vd = getVehicledata(selectedPlate);
								if (vd != null) {
									temparioName = (String) vd.get("TEMPA");
								}
								if (temparioName == null || temparioName.length() <= 0) {
									MSMJServiceRateGroup srg = new MSMJServiceRateGroup(
											Env.getCtx(), serviceRateGroupDefault, null);
									if (srg != null) {
										temparioName = srg.getName();
									}
								}
								// System.out.println("TEMPARIO...."+temparioName);
								searchPlate.getComponent().setText(selectedPlate);
							}// if (selectedPlate != null && selectedPlate.length()>0)
						} catch (Exception e) {
							log.log(Level.SEVERE,
									this.getClass().getName()
									+ ".showInfoWO - ERROR: smj_plate -"
									+ e.getMessage(), e);
						}// try / catch selected plate
						bpartnerId = tmpRequest.get_ValueAsInt("c_bpartnercustomer_ID");
						if (bpartnerId <= 0) {
							bpartnerId = tmpRequest.getC_BPartner_ID();
						}
						setPartner();
						list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
						Integer woState = tmpRequest.getR_Status_ID();
						diagnosisTextBox.setValue(tmpRequest
								.get_ValueAsString("smj_diagnosis"));
						numReqCustomerTextBox.setValue(tmpRequest
								.get_ValueAsString("smj_numreqcustomer"));
						tiresOk = tmpRequest.get_ValueAsBoolean("smj_istiresok");
						if (woState.equals(stateComplete)
								|| woState.equals(stateInvoiced)) {
							isEditable = false;
							lockFields(isEditable, lockReopen);
						} else {
							isEditable = true;
							lockFields(isEditable, lockReopen);
						}
						descTextBox.setReadonly(true);
						//modificacion iMarch---
						String docNoWarranty="";
						Boolean iswarranty = tmpRequest.get_ValueAsBoolean("smj_iswarranty");
						if(iswarranty){
							garrantyLabel.setVisible(true);
							garrantyTextbox.setVisible(true);
							MRequest rr = new MRequest(Env.getCtx(),
									tmpRequest.getR_RequestRelated_ID(), null);
							docNoWarranty = rr.getDocumentNo();
							garrantyTextbox.setValue(docNoWarranty);
						}
						//fin modificacion iMarch---
						if (woState.equals(stateGarranty)) {
							//MRequest rr = new MRequest(Env.getCtx(), tmpRequest.getR_RequestRelated_ID(), null);
							//garrantyTextbox.setValue(rr.getDocumentNo());
							garrantyTextbox.setValue(docNoWarranty);//modificacion iMarch
							garrantyLabel.setVisible(true);
							garrantyTextbox.setVisible(true);
							addlinegarrantyBtn.setVisible(true);//modificacion iMarch
						}// if (state.equals(stateGarranty))
						else if(!iswarranty){
							garrantyTextbox.setValue("");
							garrantyLabel.setVisible(false);
							garrantyTextbox.setVisible(false);
							addlinegarrantyBtn.setVisible(false);//modificacion iMarch
							stateListbox.setEnabled(false);
							requestTypeListbox.setEnabled(false);
						}
						String periodicity = tmpRequest
								.get_ValueAsString("periodicity");
						requestTypeListbox.setValue(tmpRequest.getR_RequestType_ID());
						// System.out.println(this.getClass().getCanonicalName()+".showInfoWO per,,,"+periodicity+"..");
						if (periodicity == null || periodicity.trim().equals("")) {
							// System.out.println(this.getClass().getCanonicalName()+".showInfoWO visible false");
							nextProgLabel.setVisible(false);
							nextProgCheckbox.setVisible(false);
							nextProgCheckbox.setChecked(false);
						} else {
							// System.out.println(this.getClass().getCanonicalName()+".showInfoWO visible true");
							nextProgLabel.setVisible(true);
							nextProgCheckbox.setVisible(true);
							Boolean nextProg = tmpRequest
									.get_ValueAsBoolean("smj_isprogrammingnext");
							nextProgCheckbox.setChecked(nextProg);
							if (tmpRequest != null) {
								BigDecimal km = new BigDecimal(
										tmpRequest.get_ValueAsString("kmActual"));
								if (vehicle.getkms() == null) {
									vehicle.setkms(Env.ZERO);
								}
								// System.out.println(this.getClass().getCanonicalName()+".showInfoWO km.."+km+"..veh.km.."+vehicle.getkms());
								if (km != null && km.compareTo(vehicle.getkms()) > 0) {
									kmIntbox.setValue(km);
								} else {
									kmIntbox.setValue(vehicle.getkms());
								}
								kmOld = kmIntbox.getValue();
							}// if (tmpRequest != null )
						}// if/else ("periodicity") == null
						if (tmpRequest != null
								&& (tmpRequest.getR_Status_ID() == stateDefault || tmpRequest
								.getR_Status_ID() == stateProgram)) {
							deleteWOButton.setVisible(true);
						} else {
							deleteWOButton.setVisible(false);
						}
						if (vehicle != null && vehicleID > 0) {
							try {
								if (vehicle.getodometer() == null
										|| !vehicle.getodometer().equals("F")) {
									kmIntbox.setReadonly(true);
									Messagebox.show(Msg.translate(Env.getCtx(),
											"SMJMSGNoOdometerOK"), labelInfo,
											Messagebox.OK, Messagebox.EXCLAMATION);
								} else {
									kmIntbox.setReadonly(false);
								}
							} catch (Exception e) {
								kmIntbox.setReadonly(true);
								log.log(Level.SEVERE, this.getClass()
										.getCanonicalName() + ".setVehicle ERROR:: ", e);
								Messagebox.show(Msg.translate(Env.getCtx(),
										"SMJMSGNoOdometerOK"), labelInfo,
										Messagebox.OK, Messagebox.EXCLAMATION);
							}// try/catch
						}// if(vehicle!=null && vehicleID > 0)
						loadTable();
						searchPlate.setReadWrite(false);
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()
								+ ".showInfoWO ERROR:: ", e);
					}// try / catch
				}// if OK
				else {					
					setVehicle(plate);
				}
				
				bpartnerSearch.getComponent().getTextbox().setFocus(true);
			}
		};

		int rdm = UtilTrans.getRdmInteger();
		SMJInfoWorkOrder ip = new SMJInfoWorkOrder(rdm, "R_Request", "R_Request", false, "", false, plate, false, noStates, callback);
		ip.setVisible(true);
		ip.setParent(form);
		ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelWorkOrder"));
		ip.setStyle("border: 2px");
		ip.setClosable(false);
		ip.addValueChangeListener(this);
		AEnv.showCenterScreen(ip);		
		return;
	}// showInfoWO

	/**
	 * muestra la ventana de editar actividad - show edir activity
	 */
	private void showEditActivity(Integer productId, final Integer woLineId, final Integer index) {
		// System.out.println(this.getClass().getCanonicalName()+".showEditActivity entro...."+index);
		try {
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				return;
			}
			MSMJWorkOrderLine woLine = list.get(index);
			Boolean isComplete = false;
			try {
				if (woLineId <= 0 || woLine == null
						|| !woLine.issmj_isservice()) {
					Messagebox
					.show(Msg.translate(Env.getCtx(),
							"SMJMSGElementService"), labelInfo,
							Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				// System.out.println(this.getClass().getCanonicalName()+".showEditActivity editable.."+isEditable+"id...."+woLineId+"..estate.."+woLine.getwolstate());
				if (woLine.getwolstate().equals("C") || !isEditable) {
					isComplete = true;
					// Messagebox.show(Msg.translate(Env.getCtx(),
					// "SMJMSGActivityComplete"), labelInfo, Messagebox.OK,
					// Messagebox.EXCLAMATION);
					// return;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()
						+ ".showEditActivity - ERROR: ", e);
			}
			SMJInfoActivity ip = new SMJInfoActivity(Env.TAB_INFO,
					"smj_ActivityRequest", "smj_ActivityRequest_ID", false, "",
					this, productId, woLine, isComplete, vehicleID,
					selectedPlate);
			ip.setVisible(true);
			ip.setTitle(""
					+ Msg.translate(Env.getCtx(), "smj_ActivityRequest_ID"));
			ip.setStyle("border: 2px");
			ip.setClosable(false);
			ip.setParent(form);
			ip.addValueChangeListener(this);
			ip.addEventListener(new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					MSMJWorkOrderLine woLine = list.get(index);
					try {
						String istiresok = Env.getContext(Env.getCtx(), Env.TAB_INFO, "TIRESOK");
						if (istiresok.equals("true")) {
							tiresOk = true;
							tmpRequest.set_ValueOfColumn("smj_istiresok", tiresOk);
							tmpRequest.save();							
						}

						String ok = Env.getContext(Env.getCtx(), Env.TAB_INFO, "OK");
						Integer totalMech = getCountMechanic(woLine
								.getsmj_workOrderLine_ID());
						if (woLine != null && !woLine.issmj_isexternallservice()
								&& totalMech <= 0) {
							list.remove(woLine);
							woLine.setwolstate("S");
							woLine.save();
							list.add(index, woLine);
							list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
							loadTable();
						}
						if (woLine != null && woLine.issmj_isexternallservice()
								&& woLine.getC_InvoiceLine_ID() <= 0) {
							list.remove(woLine);
							woLine.setwolstate("S");
							woLine.save();
							list.add(index, woLine);
							list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
							loadTable();
						}
						if (ok.equals("true")) {
							list.remove(woLine);
							woLine = new MSMJWorkOrderLine(Env.getCtx(), woLineId, null);
							MProduct p = MProduct.get(Env.getCtx(),
									woLine.getM_Product_ID());
							woLine.setProductValue(p.getValue());
							woLine.setProductName(p.getName());
							list.add(index, woLine);
							list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
							loadTable();
							return;
						}// if OK
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName()
								+ ".showEditActivity - ERROR: " + e.getMessage(), e);
					}
				}
			});
			AEnv.showCenterScreen(ip);

		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".showEditActivity - ERROR: " + e.getMessage(), e);
		}

		return;
	}// showEditActivity

	/**
	 * guarda una solicitud - save request
	 */
	private Boolean saveRequest(String trxName) {
		Boolean flag = true;
		Boolean newRequest = false;
		try {
			if ((selectedPlate == null || selectedPlate.trim().length() <= 0)
					&& !isRmnFac) {
				Messagebox.show(Msg.translate(Env.getCtx(), "smj_plate") + " "
						+ Msg.translate(Env.getCtx(), "SMJMSGMandatoryField"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			if (bpartnerId == null || bpartnerId <= 0) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGPartnerValidLocation"), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			String desc = descTextBox.getValue().trim();
			if (desc == null || desc.length() <= 0) {
				Messagebox.show(Msg.translate(Env.getCtx(), "summary") + " "
						+ Msg.translate(Env.getCtx(), "SMJMSGMandatoryField"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			Integer requestType = rTypeWO;
			try {
				requestType = (Integer) requestTypeListbox.getSelectedItem()
						.getValue();
				if (requestType == null || requestType <= 0) {
					requestType = rTypeWO;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()
						+ ".saveRequest - ERROR: " + e.getMessage(), e);
			}
			// System.out.println(this.getClass().getName()+".saveRequest --- **** 1");
			if (tmpRequest == null) {
				newRequest = true;
				tmpRequest = new MRequest(Env.getCtx(), 0, trxName);
				MRequestType rt = new MRequestType(Env.getCtx(), requestType, trxName);
				Integer seq = rt.get_ValueAsInt("Sequence") + 1;
				// String rseq =
				// po.get_ValueAsString(MRequest.COLUMNNAME_DocumentNo);
				tmpRequest.setDocumentNo(seq.toString());
				tmpRequest.set_ValueOfColumn("smj_sequence", seq.toString());
				rt.set_ValueOfColumn("Sequence", seq);
				tmpRequest.setTaskStatus("0");//imarch 12/03/2020
				if(!rt.save()) {
					tmpRequest=null;
					return false;
				}
			} else {
				Integer reqId = tmpRequest.getR_Request_ID();
				tmpRequest = new MRequest(Env.getCtx(), reqId, trxName);
			}

			tmpRequest.setC_BPartner_ID(bpartnerId);
			tmpRequest.set_ValueOfColumn("smj_plate", selectedPlate);
			String vId = DataQueriesTrans
					.getVehicleInternalNumberByPlate(selectedPlate);
			tmpRequest.set_ValueOfColumn("smj_internalnumber", vId);
			tmpRequest.set_ValueOfColumn("smj_vehicle_ID", vehicleID);
			tmpRequest.set_ValueOfColumn("c_bpartnercustomer_ID", bpartnerId);
			// System.out.println("Par..."+bpartnerId
			// +".******"+tmpRequest.get_ValueAsInt("c_bpartnercustomer_ID"));
			BigDecimal km = Env.ZERO;
			if (kmIntbox.getValue() != null) {
				km = kmIntbox.getValue();
			}
			tmpRequest.set_ValueOfColumn("kmactual", km);
			Integer state = stateWO;
			// System.out.println(this.getClass().getName()+".saveRequest ..."+stateListbox.getSelectedItem());
			try {
				state = (Integer) stateListbox.getSelectedItem().getValue();
				if (state <= 0) {
					stateListbox.setValue(stateDefault);
					state = stateDefault;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getCanonicalName()
						+ ".saveRequest ERROR:: ", e);
				stateListbox.setValue(stateDefault);
			}
			tmpRequest.setR_Status_ID(state);
			tmpRequest.setDueType("7");
			tmpRequest.setPriorityUser("5");
			tmpRequest.setPriority("5");
			tmpRequest.setConfidentialTypeEntry("I");
			tmpRequest.setConfidentialType("I");

			tmpRequest.set_ValueNoCheck(MRequest.COLUMNNAME_R_RequestType_ID, requestType);
			String desTB = descTextBox.getValue();
			if (requestType.equals(rTypeRmnFac)) {
				MRequestType rt = MRequestType.get(Env.getCtx(), requestType);
				if (desTB.contains(rt.getName())) {
					desc = desTB;
				} else {
					desc = rt.getName() + " - " + desTB;
				}
			}
			tmpRequest.setSummary(desc);
			tmpRequest.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
			tmpRequest.set_ValueOfColumn("smj_diagnosis", diagnosisTextBox.getValue());
			tmpRequest.set_ValueOfColumn("smj_numreqcustomer", numReqCustomerTextBox.getValue());
			
			//modificacion imarch
			//tmpRequest.set_ValueOfColumn("ad_client_id", Env.getAD_Client_ID(Env.getCtx()));
			//tmpRequest.set_ValueOfColumn("ad_org_id", Env.getAD_Org_ID(Env.getCtx()));
			//fin modificacion imarch--
			//tmpRequest.saveEx();
			flag = tmpRequest.save();
			if (flag) {
				searchPlate.setReadWrite(false);
				woTextBox.setValue(tmpRequest.getDocumentNo());
				descTextBox.setReadonly(true);
			} else {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGErrorSavingData"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				if(newRequest) {
					tmpRequest = null;
				}
			}
			if (tmpRequest != null
					&& (tmpRequest.getR_Status_ID() == stateDefault || tmpRequest
					.getR_Status_ID() == stateProgram)) {
				deleteWOButton.setVisible(true);
			} else {
				deleteWOButton.setVisible(false);
			}
			tmpRequest.set_TrxName(null);
			productTextBox.setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".saveRequest ERROR:: " + e.getMessage(), e);
		}
		return flag;
	}// saveRequest

	/***
	 * finaliza la orden de trabajo - finalize work order
	 */
	private Boolean finishWorkOrder(Trx trx) {
		Boolean ok = false;
		try {
			if ((temparioName == null || temparioName.trim().equals("") || temparioName
					.length() <= 0) && !isRmnFac) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGOnlyVehicleTempario"), labelInfo, Messagebox.OK,
						Messagebox.EXCLAMATION);
				if (vehicleID > 0) {
					selectedPlate = "";
					bpartnerId = 0;
					vehicle = null;
					searchPlate.getComponent().setText("");
					return false;
				}// if(vehicleID
			}// if tempario
			if (isRmnFac && !bpartnerId.equals(userSeveter)) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGUserMustBeSeveter"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}
			ok = validateData();
			if ((vehicle != null && vehicleID > 0) && !isRmnFac) {
				Boolean isAdmTires = DataQueriesTrans.isAdministrationTypeByPlate(
						codeAdministrationTires, selectedPlate);
				// System.out.println(this.getClass().getName()+".finishWorkOrder isAdmTire.."+isAdmTires+"..tiresOK.."+tiresOk);
				if (isAdmTires && !tiresOk) {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGDoTiresActivity"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					return false;
				}
			}// if ((vehicle!= null && vehicleID > 0) && !isRmnFac)
			// System.out.println(this.getClass().getName()+".finishWorkOrder --- here");
			if (ok) {
				Integer reqId = tmpRequest.getR_Request_ID();
				tmpRequest = new MRequest(Env.getCtx(), reqId, trx.getTrxName());
				// xp.getwolstate().equals("C")
				Integer woState = (Integer) stateListbox.getSelectedItem()
						.getValue();
				if (woState.equals(stateGarranty)) {
					tmpRequest.set_ValueOfColumn("smj_iswarranty", true);
				} else {
					tmpRequest.set_ValueOfColumn("smj_iswarranty", false);
				}
				// ok = saveRequest(trx.getTrxName());
				Boolean kmt = true;
				// System.out.println(this.getClass().getName()+".finishWorkOrder ..vehicle.."+vehicle+"..ID.."+vehicleID);
				if (vehicle != null && vehicleID > 0) {
					kmt = saveKmsTire(trx.getTrxName());
				}
				if (!kmt) {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGErrorSavingData"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					return false;
				}
				// System.out.println(this.getClass().getCanonicalName()+".finishWorkOrder KMS save");
				Iterator<MSMJWorkOrderLine> itl = list.iterator();
				while (itl.hasNext()) {
					MSMJWorkOrderLine xp = itl.next();
					if (xp.getwolstate().equals("A")) {
						try {
							xp.setwolstate("C");
							xp.save();
						} catch (Exception e) {
							log.log(Level.SEVERE,
									this.getClass().getName()
									+ "validateData - ERROR: "
									+ e.getMessage(), e);
						}
					}// if(xp.getwolstate().equals("A"))
				}// while....
				if (isRmnFac) {
					tmpRequest.setR_Status_ID(stateInvoiced);
				} else {
					tmpRequest.setR_Status_ID(stateComplete);
				}
				Boolean rok = tmpRequest.save();
				if (!rok) {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGErrorSavingData"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					return false;
				}
				// System.out.println(this.getClass().getCanonicalName()+".finishWorkOrder tmprequest save");
				Boolean pok = true;
				Integer ruleCode = tmpRequest
						.get_ValueAsInt("smj_ScheduleRules_ID");
				if (ruleCode >= 0
						&& tmpRequest.get_ValueAsString("periodicity") != null
						&& nextProgCheckbox.isChecked()) {
					pok = createProgram(trx.getTrxName(), ruleCode);
					// System.out.println(this.getClass().getCanonicalName()+".finishWorkOrder crea program..."+pok);
				}
				if (!pok) {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGErrorSavingData"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					return false;
				}
				// System.out.println(this.getClass().getName()+".finishWorkOrder ..fin ...");
			}// if OK
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".finishWorkOrder ERROR:: " + e.getMessage(), e);
			return false;
		}
		return ok;
	}// finishWorkOrder

	/**
	 * guarda el kms del vehiculo y las llantas - save kms for vehicle and tires
	 * 
	 * @param trxName
	 * @return Boolean
	 */
	private Boolean saveKmsTire(String trxName) {
		Boolean flag = true;
		try {
			LinkedList<MSMJTires> list = DataQueriesTrans.getTiresList(trxName,
					vehicle);
			Iterator<MSMJTires> it = list.iterator();
			BigDecimal kmActual = kmIntbox.getValue();
			vehicle.set_TrxName(trxName);
			vehicle.setdatelastmileage(Env.getContextAsDate(Env.getCtx(),
					"#Date"));
			vehicle.setkms(kmActual);
			Boolean ok = vehicle.save();
			if (!ok) {
				return false;
			}
			while (it.hasNext()) {
				MSMJTires tire = it.next();
				BigDecimal kmsTotal = UtilTrans.calculateKms(tire, kmOld, kmActual);
				if (kmsTotal == null) {
					Messagebox.show(
							Msg.translate(Env.getCtx(), "SMJMSGValidKms"),
							labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
				if (tire.getkmsMount() == null
						|| tire.getkmsMount().compareTo(Env.ZERO) <= 0) {
					tire.setkmsMount(kmActual);
				}
				// System.out.println(this.getClass().getCanonicalName()+".saveKmsTire lmsTotal.."+kmsTotal);
				tire.setkms(kmsTotal);
				ok = tire.save();
				if (!ok) {
					return false;
				}
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".saveKmsTire - ERROR: " + e.getMessage(), e);
		}
		return flag;
	}// saveKmsTire

	/**
	 * bloquea o desbloquea campos - lock or unlock fields.
	 * 
	 * @param isLock
	 */
	private void lockFields(Boolean isLock, Boolean lockReopen) {
		if (!lockReopen) {
			lockReopen = isLock;
		}
		productTextBox.setReadonly(!isLock);
		searchPlate.setReadWrite(!isLock);
		bpartnerSearch.setReadWrite(isLock);
		descTextBox.setReadonly(!isLock);
		kmIntbox.setReadonly(!isLock);
		deleteButton.setDisabled(!isLock);
		numReqCustomerTextBox.setReadonly(!isLock);
		diagnosisTextBox.setReadonly(!isLock);
		createButton.setDisabled(!isLock);
		confirmPanel.getButton(ConfirmPanel.A_OK).setDisabled(!isLock);
		stateListbox.setEnabled(!lockReopen);
		nextProgCheckbox.setEnabled(isLock);
		//modificacion iMarch------------
		addlinegarrantyBtn.setDisabled(!isLock);
		requestTypeListbox.setEnabled(!lockReopen);
		//fin modificacion iMarch------------
	}// lockFields

	/**
	 * seleccionar orden para garantia - select garranty order
	 */
	private void showInfoWOGarranty() {
		try {
			if (!isChangeActivity) {
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGRolNoAllowOperation"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName() + ".showInfoWOGarranty - ERROR: " + e.getMessage(), e);
		}
		if(ipformOpen) return;//modificacion iMArch, valida que no se vuelva a abrir otra ventana si aun esta abierta
		try {
			Integer code = 0;
			if (code <= 0) {
				int rdm = UtilTrans.getRdmInteger();
				String plate = "";
				if (selectedPlate != null && selectedPlate.length() > 0) {
					plate = selectedPlate;
				}

				Callback<Integer> callback = new Callback<Integer>() {

					@Override
					public void onCallback(Integer code) {
						ipformOpen=false;//modificacion iMArch
						if (code == null || code <= 0) {
							stateListbox.setSelectedIndex(0);
							return;
						}// if (code == null || code <= 0)						
						//Trx trx = Trx.get(Trx.createTrxName("AL"), true);
						MRequest rr = new MRequest(Env.getCtx(), code, null);
						searchPlate.getComponent().setText(rr.get_ValueAsString("smj_plate"));
						selectedPlate = rr.get_ValueAsString("smj_plate");
						vehicleID = rr.get_ValueAsInt("smj_vehicle_ID");
						bpartnerId = rr.get_ValueAsInt("c_bpartnercustomer_ID");
						setVehicle(selectedPlate);
						bpartnerId = rr.get_ValueAsInt("c_bpartnercustomer_ID");
						setPartner();
						m_R_Request_ID = rr.getR_Request_ID();//iMarch
						m_C_BPartner_ID = rr.getC_BPartner_ID();//iMarch
						m_DocumentNo = rr.getDocumentNo();//iMarch
						m_Summary = rr.getSummary() + " - " + rr.get_ValueAsString("smj_diagnosis");

						try {
							kmIntbox.setValue(new BigDecimal(rr
									.get_ValueAsString("kmActual")));
						} catch (Exception e) {
							log.log(Level.SEVERE, this.getClass().getName()
									+ ".showInfoWOGarranty - ERROR: " + e.getMessage(),
									e);
						}

						kmOld = kmIntbox.getValue();
						Integer requestType = rTypeWO;

						try {
							requestType = (Integer) requestTypeListbox.getSelectedItem().getValue();
							if (requestType == null || requestType <= 0) {
								requestType = rTypeWO;
							}
						} catch (Exception e) {
							log.log(Level.SEVERE, this.getClass().getName() + ".showInfoWOGarranty - ERROR: " + e.getMessage(), e);
						}
						showInfoWOGarrantyLines(code, rr.getDocumentNo());
						/**Modificacion iMarch
						if (tmpRequest == null) {
							tmpRequest = new MRequest(Env.getCtx(), 0, trx.getTrxName());
							MRequestType rt = new MRequestType(Env.getCtx(), requestType, trx.getTrxName());
							Integer seq = rt.get_ValueAsInt("Sequence") + 1;
							// String rseq =
							// po.get_ValueAsString(MRequest.COLUMNNAME_DocumentNo);
							tmpRequest.setDocumentNo(seq.toString());
							tmpRequest.set_ValueOfColumn("smj_sequence", seq.toString());
							rt.set_ValueOfColumn("Sequence", seq);
							rt.save();
						}

						tmpRequest.setSummary(rr.getSummary() + " - " + rr.get_ValueAsString("smj_diagnosis"));
						descTextBox.setValue(tmpRequest.getSummary());
						tmpRequest.setR_Status_ID(stateGarranty);
						tmpRequest.setR_RequestType_ID(requestType);
						tmpRequest.setDueType("7");
						tmpRequest.setPriorityUser("5");
						tmpRequest.setPriority("5");
						tmpRequest.setConfidentialTypeEntry("I");
						tmpRequest.setConfidentialType("I");
						tmpRequest.set_ValueOfColumn("smj_iswarranty", true);
						tmpRequest.setR_RequestRelated_ID(rr.getR_Request_ID());
						tmpRequest.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
						tmpRequest.setC_BPartner_ID(rr.getC_BPartner_ID());
						tmpRequest.set_ValueOfColumn("smj_plate", selectedPlate);
						tmpRequest.set_ValueOfColumn("smj_vehicle_ID", vehicleID);
						tmpRequest.set_ValueOfColumn("c_bpartnercustomer_ID", bpartnerId);
						tmpRequest.set_ValueOfColumn("kmactual", kmIntbox.getValue());

						try {
							tmpRequest.setR_RequestType_ID((Integer) requestTypeListbox
									.getSelectedItem().getValue());
						} catch (Exception e) {
							log.log(Level.SEVERE, this.getClass().getName() + ".showInfoWOGarranty - ERROR: " + e.getMessage(), e);
						}

						Boolean okg = tmpRequest.save();
						
						if(!okg) {
							// System.out.println(this.getClass().getName()+".showInfoWOGarranty ... Crear Garantia ");
							trx.rollback();
							trx.close();
							return;
						}
					
						garrantyTextbox.setValue(rr.getDocumentNo());

						try {
							String[] codes = showInfoWOGarrantyLines(code, rr.getDocumentNo());
							if (codes == null) {
								//trx.rollback();
								//trx.close();
								return;
							}// if(codes == null)

							for (int i = 0; i < codes.length; i++) {
								Integer codex = Integer.parseInt(codes[i]);
								Boolean lok = createLine(trx.getTrxName(), codex, tmpRequest.getR_Request_ID());

								if (!lok) {
									trx.rollback();
									trx.close();
									return;
								}
							}// for codes
							trx.commit();
							woTextBox.setValue(tmpRequest.getDocumentNo());
							trx.close();
						} catch (Exception e) {
							trx.rollback();
							log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".showInfoWOGarranty ERROR:: ", e);
							trx.close();
							return;
						} finally {
							trx.close();
						}// try/catch/finally

						list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
						loadTable();						
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectGarrantyElements"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);

						if (list.size() > 0) {
							stateListbox.setValue(stateWO);
						} else {
							stateListbox.setValue(stateDefault);
						}**/
					}// if OK

				};

				SMJInfoWorkOrder ip = new SMJInfoWorkOrder(rdm, "R_Request", "R_Request", false, "", true, plate, true, "", callback);
				ip.setVisible(true);
				ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelWorkOrder"));
				ip.setStyle("border: 2px");
				ip.setClosable(false);
				ip.addValueChangeListener(this);
				ip.setParent(form);
				AEnv.showCenterScreen(ip);
				ipformOpen=true;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".showInfoWOGarranty ERROR:: ", e);
		}
	}// showInfoWOGarranty

	/***
	 * muestra la ventana con las actividades de la orden de trabajo- shows
	 * window to work order activities
	 * 
	 * @return
	 */
	//private String[] showInfoWOGarrantyLines(Integer code, String docNo) {
	private void showInfoWOGarrantyLines(Integer code, String docNo) {
		// System.out.println(this.getClass().getCanonicalName()+".showInfoWOGarrantyLines por lineas...");
		//String[] codes = null;
		try {
			int rdm = UtilTrans.getRdmInteger();
		//Modificacion iMarch-----------------
			Callback<String> callbackl = new Callback<String>() {

				@Override
				public void onCallback(String mcodes) {
					
					Trx trx = Trx.get(Trx.createTrxName("AL"), true);
					Integer requestType = rTypeWO;
					if (tmpRequest == null) {
						tmpRequest = new MRequest(Env.getCtx(), 0, trx.getTrxName());
						MRequestType rt = new MRequestType(Env.getCtx(), requestType, trx.getTrxName());
						Integer seq = rt.get_ValueAsInt("Sequence") + 1;
						// String rseq =
						// po.get_ValueAsString(MRequest.COLUMNNAME_DocumentNo);
						tmpRequest.setDocumentNo(seq.toString());
						tmpRequest.set_ValueOfColumn("smj_sequence", seq.toString());
						rt.set_ValueOfColumn("Sequence", seq);
						rt.save();
					}
					else {
						Integer reqId = tmpRequest.getR_Request_ID();
						tmpRequest.set_TrxName(null);
						tmpRequest = new MRequest(Env.getCtx(), reqId, trx.getTrxName());
						
					}
					
					tmpRequest.setSummary(m_Summary);
					descTextBox.setValue(tmpRequest.getSummary());
					tmpRequest.setR_Status_ID(stateGarranty);
					
					tmpRequest.setDueType("7");
					tmpRequest.setPriorityUser("5");
					tmpRequest.setPriority("5");
					tmpRequest.setConfidentialTypeEntry("I");
					tmpRequest.setConfidentialType("I");
					tmpRequest.set_ValueOfColumn("smj_iswarranty", true);
					tmpRequest.setR_RequestRelated_ID(m_R_Request_ID);
					tmpRequest.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
					tmpRequest.setC_BPartner_ID(m_C_BPartner_ID);
					tmpRequest.set_ValueOfColumn("smj_plate", selectedPlate);
					tmpRequest.set_ValueOfColumn("smj_vehicle_ID", vehicleID);
					tmpRequest.set_ValueOfColumn("c_bpartnercustomer_ID", bpartnerId);
					tmpRequest.set_ValueOfColumn("kmactual", kmIntbox.getValue());

					try {
						//tmpRequest.setR_RequestType_ID((Integer) requestTypeListbox.getSelectedItem().getValue());
						//modificacion iMarch--
						tmpRequest.set_ValueNoCheck(MRequest.COLUMNNAME_R_RequestType_ID, requestType);
						//fin modificacion iMarch--
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName() + ".showInfoWOGarranty - ERROR: " + e.getMessage(), e);
					}
					tmpRequest.saveEx();//iMarch
					Boolean okg = tmpRequest.save();
					
					if(!okg) {
						// System.out.println(this.getClass().getName()+".showInfoWOGarranty ... Crear Garantia ");
						trx.rollback();
						trx.close();
						return;
					}
				
					garrantyTextbox.setValue(m_DocumentNo);

					try {
						String[] codes = mcodes.split(",");
						if (codes == null) {
							trx.rollback();
							trx.close();
							return;
						}// if(codes == null)

						for (int i = 0; i < codes.length; i++) {
							Integer codex = Integer.parseInt(codes[i]);
							Boolean lok = createLine(trx.getTrxName(), codex, tmpRequest.getR_Request_ID());

							if (!lok) {
								trx.rollback();
								trx.close();
								return;
							}
						}// for codes
						trx.commit();
						woTextBox.setValue(tmpRequest.getDocumentNo());
						trx.close();
					} catch (Exception e) {
						trx.rollback();
						log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".showInfoWOGarranty ERROR:: ", e);
						trx.close();
						return;
					} finally {
						trx.close();
						//modificacion iMarch-------
						m_C_BPartner_ID=0;
						m_DocumentNo="";
						m_R_Request_ID=0;
						m_Summary="";
						//fin modificacion iMarch---
					}// try/catch/finally

					list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
					loadTable();
					if(list.isEmpty())
						Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGSelectGarrantyElements"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);

					if (list.size() > 0) {
						//stateListbox.setValue(stateWO);
						stateListbox.setValue(stateGarranty);
					} else {
						stateListbox.setValue(stateDefault);
					}
				}
			};
		//fin Modificacion iMarch------------------
			
			SMJInfoGarrantyLine ip = new SMJInfoGarrantyLine(rdm,
					"smj_workOrderLine", "smj_workOrderLine_ID", false, "",
					code, docNo, callbackl);
			ip.setVisible(true);
			ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelWorkOrder"));
			ip.setStyle("border: 2px");
			ip.setClosable(false);
			ip.addValueChangeListener(this);
			ip.setParent(form);
			AEnv.showCenterScreen(ip);
			/**modificacion iMarch
			String ok = Env.getContext(Env.getCtx(), m_rdm, "OK");
			if (ok.equals("true")) {
				String wocodes = Env.getContext(Env.getCtx(), rdm, "WOLCODES");
				codes = wocodes.split(",");
				// System.out.println(this.getClass().getCanonicalName()+".showInfoWOGarrantyLines codes..."+wocodes+"... codes....."+codes);
				if (codes == null || codes.length <= 0) {
					//return null;
				}
			}
			**/
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".showInfoWOGarrantyLines ERROR:: ", e);
		}
		//return codes;
	}// showInfoWOGarrantyLines

	/**
	 * crea linea de garantia copia de la linea de la orden correspondiente -
	 * Create garranty line copy from work order line corresponding
	 * 
	 * @param trxName
	 * @param code
	 * @param requestId
	 * @return Boolean
	 */
	private Boolean createLine(String trxName, Integer code, Integer requestId) {
		// System.out.println(this.getClass().getName()+".createLine... code."+code+".RR.."+requestId);
		MSMJWorkOrderLine dato = new MSMJWorkOrderLine(Env.getCtx(), code,
				trxName);
		MSMJWorkOrderLine ipl = new MSMJWorkOrderLine(Env.getCtx(), 0, trxName);
		ipl.setM_Product_ID(dato.getM_Product_ID());
		ipl.setPrice(dato.getPrice());
		ipl.setlocator_ID(dato.getlocator_ID());
		ipl.setprovider_ID(dato.getprovider_ID());
		//ipl.setQty(dato.getQty());
		ipl.setQty(Env.ZERO);//modificado iMarch
		ipl.setpurchasePrice(dato.getpurchasePrice());
		ipl.setsmj_estimatedtime(dato.getsmj_estimatedtime());
		ipl.setsmj_isallowchanges(dato.issmj_isallowchanges());
		ipl.setProductValue(dato.getProductValue());
		ipl.setProductName(dato.getProductName());
		ipl.settaxvalue(dato.gettaxvalue());
		ipl.setM_AttributeSetInstance_ID(dato.getM_AttributeSetInstance_ID());
		//ipl.settotal(dato.gettotal());
		ipl.settotal(Env.ZERO);	//modificacion iMarch
		ipl.setsmj_isservice(dato.issmj_isservice());
		ipl.setsmj_isexternallservice(dato.issmj_isexternallservice());
		ipl.setsmj_iscommission(dato.issmj_iscommission());
		ipl.setdateopen(Env.getContextAsDate(Env.getCtx(), "#Date"));
		ipl.setWarehouseName(dato.getWarehouseName());
		ipl.setR_Request_ID(requestId);
		MProduct p = MProduct.get(Env.getCtx(), ipl.getM_Product_ID());
		ipl.setProductValue(p.getValue());
		ipl.setProductName(p.getName());
		Boolean isService = p.getProductType().equals("S") ? true : false;
		ipl.setsmj_isservice(isService);
		ipl.setsmj_iswarranty(true);
		ipl.setsmj_workorderline_parent_ID(dato.getsmj_workOrderLine_ID());
		MLocator loc = MLocator.get(Env.getCtx(), ipl.getlocator_ID());
		MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
		ipl.setWarehouseName(wh.getName());
		dato.setsmj_iswarrantyapplied(true);
		try {
			Boolean dok = dato.save();
			Boolean iok = ipl.save();
			if (!iok || !dok) {
				return false;
			}
			list.add(ipl);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".createLine - ERROR: " + e.getMessage(), e);
		}
		return true;
	}// createLine

	/**
	 * crea nueva regla de programacion - create new program rule
	 * 
	 * @param trxName
	 * @param ruleCode
	 * @return
	 */
	private Boolean createProgram(String trxName, Integer ruleCode) {
		try {
			CreateWOProgram wop = new CreateWOProgram();
			// System.out.println(this.getClass().getCanonicalName()+".createProgram rule.."+ruleCode);
			MSMJScheduleRules rule = new MSMJScheduleRules(Env.getCtx(),
					ruleCode, null);
			LinkedList<Integer> acts = wop.getActivity(rule
					.getsmj_ScheduleRules_ID());
			String ok = wop.saveRequest(trxName, rule, vehicleID, acts);
			// System.out.println(this.getClass().getCanonicalName()+".createProgram code.."+ok);
			if (ok == null) {
				return false;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".createProgram - ERROR: " + e.getMessage(), e);
		}
		return true;
	}// createProgram

	/**
	 * evalua si el rol de usuario puede cambiar las actividades - evaluates if
	 * user role can change activities
	 */
	private void canChangeActivity() {
		Integer rolId = Env.getAD_Role_ID(Env.getCtx());
		// System.out.println(this.getClass().getName()+".showInfoProduct rol..."+rolId);
		String[] lRol = roles.split(";");
		for (int c = 0; c < lRol.length; c++) {
			Integer rol = Integer.parseInt(lRol[c]);
			if (rol.equals(rolId)) {
				isChangeActivity = true;
			}
		} // for
		String[] lReOpen = rolReOpen.split(";");
		for (int f = 0; f < lReOpen.length; f++) {
			Integer rol = Integer.parseInt(lReOpen[f]);
			if (rol.equals(rolId)) {
				isReOpen = true;
			}
		} // for
		// System.out.println(this.getClass().getName()+".canChangeActivity ..chage.."+isChangeActivity+"..reOpen.."+isReOpen);
	}// canChangeActivity

	/**
	 * asigna un mecanica a la actividad . asignate mechanic to activity
	 * 
	 * @param bMechanic
	 * @param woLineID
	 */
	private void asignMechanic(MBPartner bMechanic, MSMJWorkOrderLine ipl) {
		try {
			String fee = bMechanic.get_ValueAsString("smj_commissionvalue");
			BigDecimal mecFee = new BigDecimal(fee);
			if (mecFee.compareTo(maxFee) > 0) {
				Messagebox
				.show(Msg.translate(Env.getCtx(),
						"SMJMSGMechanicFeeExceeds"), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
				// list.remove(ww);
				return;
			}
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);

			if (bMechanic != null) {
				MSMJWoMechanicAssigned wma = new MSMJWoMechanicAssigned(
						Env.getCtx(), 0, null);
				wma.setC_BPartner_ID(bMechanic.getC_BPartner_ID());
				wma.setsmj_workOrderLine_ID(ipl.getsmj_workOrderLine_ID());

				try {
					wma.setappliedfee(mecFee);
				} catch (Exception e) {
					wma.setappliedfee(Env.ZERO);
				}
				Boolean isFee = bMechanic
						.get_ValueAsBoolean("smj_iscommission");
				wma.setsmj_iscommission(isFee);
				// list.add(wma);
				Boolean ok = wma.save();
				ipl.setwolstate("A");
				Boolean wok = ipl.save();
				if (ok && wok) {
					trx.commit();
				} else {
					Messagebox.show(Msg.translate(Env.getCtx(),
							"SMJMSGErrorSavingData"), labelInfo, Messagebox.OK,
							Messagebox.EXCLAMATION);
					trx.rollback();
				}
				trx.close();
			}

		} catch (NumberFormatException e) {
			try {
				Messagebox
				.show(Msg.translate(Env.getCtx(),
						"SMJMSGMechanicFeeExceeds"), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".asignMechanic - ERROR: " + e.getMessage(), e);
		}
	}// asignMechanic

	/**
	 * borra una orden de trabajo - delete work Order
	 */
	private void deleteWorkOrden() {
		// System.out.println(this.getClass().getName()+".deleteWorkOrden ...borrar ..");
		Boolean ok = false;
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		try {
			if (tmpRequest != null) {
				list = DataQueriesTrans.getWOLines(tmpRequest.getR_Request_ID());
				Iterator<MSMJWorkOrderLine> it = list.iterator();
				while (it.hasNext()) {
					MSMJWorkOrderLine ipx = it.next();
					if (ipx.issmj_isservice() && !ipx.getwolstate().equals("S")) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGWONoDeleteService"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						trx.rollback();
						trx.close();
						return;
					}// if(ipx.issmj_isservice() &&
					// !ipx.getwolstate().equals("S"))
					ipx.set_TrxName(trx.getTrxName());
					Boolean lok = ipx.delete(true);
					if (!lok) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGWONoDeleteLine"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						trx.rollback();
						trx.close();
						return;
					}// if(!lok){
				}// while
			}// if (tmpRequest != null)
			tmpRequest.set_TrxName(trx.getTrxName());
			ok = tmpRequest.delete(true);
			if (ok) {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGRecordDeleted"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				trx.commit();
				clean();
			} else {
				Messagebox.show(
						Msg.translate(Env.getCtx(), "SMJMSGWOErrorDelete"),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				trx.rollback();
			}
			trx.close();

		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + " - ERROR: " + e.getMessage(),
					e);
			trx.rollback();
			trx.close();
		}
		return;
	}// deleteWorkOrden

	/**
	 * Llena el combo de estados fill state list
	 * 
	 * @param state
	 */
	private void listRequestType() {
		requestTypeListbox.removeAllItems();
		String value = rTypeRmnFac + "," + rTypeWO;
		Vector<Vector<Object>> data = DataQueriesTrans.getRequestTypeList(value,
				Env.getAD_Org_ID(Env.getCtx()));
		// requestTypeListbox.appendItem("", 0);
		for (int i = 0; i < data.size(); i++) {
			Vector<Object> line = data.get(i);
			requestTypeListbox.appendItem((String) line.get(1), line.get(0));
		}// for
		requestTypeListbox.setMold("select");
		requestTypeListbox.addActionListener(this);
		requestTypeListbox.setValue(rTypeWO);
		isRmnFac = false;
	}// listStates

	/**
	 * establece los datos de remanofacturados - set remanufactured data
	 */
	private void setRemanufactured() {
		bpartnerId = userSeveter;
		setPartner();
	}// setRemanufactured

	/**
	 * crea orden warehouse de remanofacturados - create warehouse order
	 * remanufacturated
	 * 
	 * @param trx
	 * @return
	 */
	@SuppressWarnings("unused")
	private Integer completeRemanufactured(Trx trx) {
        int lclientId = Env.getAD_Client_ID(Env.getCtx());
        Boolean flag = true;
        MOrder whOrder = null;
        StringBuilder descProd = new StringBuilder();        
        String trxName = trx.getTrxName();
        Boolean onlyservice = true;   // iMarch 05/03/2020

        // Keep orderLine ID for looking for the MInOutLine and to assign instance
     	Map<Integer, List<MInOutLine>> inOutLinesByOLineId = new HashMap<Integer, List<MInOutLine>>();
        
        try {
            Integer warehouseId = Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE", Env.getAD_Client_ID(Env.getCtx())).trim());
            String diagnosis = this.diagnosisTextBox.getValue();
            String desc = this.tmpRequest.getSummary();
            if (diagnosis != null && diagnosis.length() > 0) {
                desc = desc + " - " + diagnosis;
            }

            Integer salesRepId = Env.getAD_User_ID(Env.getCtx());
            Integer priceListSalesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST", Env.getAD_Client_ID(Env.getCtx())).trim());
            Integer docPurchaseOrder = Integer.parseInt(MSysConfig.getValue("SMJ-DOCPURCHASEORDER", Env.getAD_Client_ID(Env.getCtx())).trim());
            
            //Actualizado iMarch 27/02/2020
            Integer payterm = paytermDef;
            if(this.tmpRequest.getC_BPartner_ID() == idPartnerOwnConsumer)
            	payterm = Integer.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMINTERNAL", Env.getAD_Client_ID(Env.getCtx())).trim());
            Integer typeSO = Integer.parseInt(MSysConfig.getValue("iM_TypeSORemanu","0", Env.getAD_Client_ID(Env.getCtx())).trim());
            if(typeSO==0)
            	Messagebox.show("La variable iM_TypeSORemanu no esta configurada (Type document Sales Order)", "Error", Messagebox.OK, Messagebox.ERROR);
            //----27/02/2020
           
            //Se reemplaza whOrder para generar OV - iMarch 04/03/2020
            Integer locationID = DataQueries.getLocationPartner(this.tmpRequest.getC_BPartner_ID());
            whOrder = DocumentsTrans.createSalesOrder(trx.getTrxName(), desc, this.tmpRequest.getC_BPartner_ID(), locationID, Env.getContextAsDate(Env.getCtx(), "#Date"), salesRepId, payterm, typeSO, warehouseId, priceListSalesId, null, null, this.woTextBox.getValue());
            //----04/03/2020
            //whOrder = DocumentsTrans.createWarehouseOrder(trx, desc, this.tmpRequest.getC_BPartner_ID(), new Date(), warehouseId, priceListSalesId, salesRepId, payterm, this.woTextBox.getValue());
            MSMJWorkOrderLine rxp = null;
            MOrder purchaseOrder = null;
            Map<Integer, MInOut> listInOutConsignmentPositive = null;
            Map<Integer, MInOut> listInOutConsignmentNegative = null;
            Map<Integer, Map<Integer, BigDecimal>> mapProductAttributeQty = new HashMap<Integer, Map<Integer,BigDecimal>>();
            String descPurchaseOrder = MSysConfig.getValue("SMJ-MSGDEFAULTDESCORDER", Env.getAD_Client_ID(Env.getCtx())).trim();
            Integer locatorID = DataQueries.getLocatorId(whOrder.getM_Warehouse_ID());
            if (whOrder == null) {
                org.zkoss.zul.Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorCreateWhOrder"), labelInfo, 1, "z-messagebox-icon z-messagebox-exclamation");
                return Integer.valueOf(-1);
            }

            Iterator<MSMJWorkOrderLine> itl = this.list.iterator();
            BigDecimal total = Env.ZERO;
            int seq = 10;

            while(true) {
                while(itl.hasNext()) {
                    MSMJWorkOrderLine xp = (MSMJWorkOrderLine)itl.next();
                    MProduct p = MProduct.get(Env.getCtx(), xp.getM_Product_ID());
                    Integer pCat = p.getM_Product_Category_ID();
                    Boolean isService = p.getProductType().equals("S") ? true : false;
                    if (p != null && pCat.equals(rmnFacCategory)) {
                        rxp = xp;
                    } 
                    //--Modificacion iMarch 05/03/2020
                    else if(isService) {
                    	descProd.append(Msg.translate(Env.getCtx(), "M_Product_ID") + " : " + p.getName() + ", " + Msg.translate(Env.getCtx(), "qty") + " : " + xp.getQty() + " - ");
                    	total = total.add(xp.getPrice().multiply(xp.getQty()));
                    }
                    //--fin 05/03/2020
                    else {
                    	onlyservice = false; //iMarch 05/03/2020
                        Boolean pok = true;
                        Integer code = DocumentsTrans.createOrderLine(whOrder, xp.getM_Product_ID(), 
                        		xp.getQty(), xp.getPrice(), xp.getobservations(), Integer.valueOf(0), 
                        		xp.getlocator_ID(), this.bpartnerId, false, xp.getprovider_ID(), 
                        		whOrder.getDateOrdered(), "", xp.getsmj_workOrderLine_ID(), seq);
                        
                        if (code.intValue() <= 0) {
                            pok = false;
                        }

                        seq += 10;
                        total = total.add(xp.getPrice().multiply(xp.getQty()));
                        if (!pok.booleanValue()) {
                            org.zkoss.zul.Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorCreateWhOrderLin"), labelInfo, 1, "z-messagebox-icon z-messagebox-exclamation");
                            return Integer.valueOf(-1);
                        }

                        Integer lineLocator = xp.getlocator_ID();        				
        				        				
                        
                        if (!locatorID.equals(lineLocator) && !isService) {

        					if (listInOutConsignmentNegative == null) {
        						listInOutConsignmentNegative = new HashMap<Integer, MInOut>();
        					}

        					if (listInOutConsignmentPositive == null) {
        						listInOutConsignmentPositive = new HashMap<Integer, MInOut>();
        					}

        					StringBuilder generalDesc = new StringBuilder();
        					generalDesc.append("Orden de Venta Warehouse: " + whOrder.getDocumentNo());
        					generalDesc.append(" - " + tmpRequest.getSummary());
        					
        					Integer providerId = xp.getprovider_ID();
        					if (providerId == null || providerId <= 0) {
        						MLocator locator = MLocator.get(Env.getCtx(), lineLocator);
        						providerId = DataQueries.getwarehouseOwner(locator.getM_Warehouse_ID());
        					}

        					String listName = DataQueries.getBpartnerName(lclientId, providerId);

        					int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
        					int M_PriceList_Version_ID = DataQueries.getPriceListVersion(trxName, lclientId, "Compra_" + listName.trim());

        					// Positive (Consignment)
        					MInOut inOutConsignmentPositive;
        					if (listInOutConsignmentPositive.containsKey(providerId)) {
        						inOutConsignmentPositive = listInOutConsignmentPositive.get(providerId);
        						listInOutConsignmentPositive.remove(providerId);
        					} else {
        						inOutConsignmentPositive = Documents.createConsignmentDocuments(providerId, M_PriceList_Version_ID, M_Warehouse_ID, generalDesc.toString(), true, trxName);
        					}

        					// Negative (Consignment)
        					MInOut inOutConsignmentNegative;
        					if (listInOutConsignmentNegative.containsKey(lineLocator)) {
        						inOutConsignmentNegative = listInOutConsignmentNegative.get(lineLocator);
        						listInOutConsignmentNegative.remove(lineLocator);
        					} else {
        						int M_Warehouse_Provider_ID = MLocator.get(Env.getCtx(), lineLocator).getM_Warehouse_ID();
        						inOutConsignmentNegative = Documents.createConsignmentDocuments(providerId, M_PriceList_Version_ID, M_Warehouse_Provider_ID,
        								generalDesc.toString(), false, trxName);
        					}

        					Consumer<MInOutLine> storeInOutLinesByOLineId = storeInOutLinesByOLineId(inOutLinesByOLineId, code);
        					
        					Map<Integer, BigDecimal> mapAttributeQty = Documents.createConsignmentLinesNegative(inOutConsignmentNegative, inOutConsignmentPositive,
        							xp.getM_Product_ID(), M_PriceList_Version_ID, xp.getQty().negate(),
        							mapProductAttributeQty.get(xp.getM_Product_ID()), storeInOutLinesByOLineId, trxName);

        					listInOutConsignmentNegative.put(lineLocator, inOutConsignmentNegative);
        					listInOutConsignmentPositive.put(providerId, inOutConsignmentPositive);

        					// Updates the attributes used by product
        					if (mapProductAttributeQty.containsKey(xp.getM_Product_ID())) {
        						mapProductAttributeQty.remove(xp.getM_Product_ID());
        						mapProductAttributeQty.put(xp.getM_Product_ID(), mapAttributeQty);
        					} else {
        						mapProductAttributeQty.put(xp.getM_Product_ID(), mapAttributeQty);
        					}        					

        					// --------iMARCH-----------busca si es bodega de consignacion--------------------
        					String locatorConsignament = DataQueries.getLocatorConsignment(lineLocator);
        					if (locatorConsignament.equals("Y")) {
        						Integer existentPO = DataQueries.getCurrentPurchaseOrderPOS(trxName, descPurchaseOrder, providerId, docPurchaseOrder,
        								Env.getAD_Client_ID(Env.getCtx()));
        						if (existentPO > 0) {
        							purchaseOrder = new MOrder(Env.getCtx(), existentPO, trxName);
        						} else {
        							purchaseOrder = Documents.createPurchaseOrder(trxName, descPurchaseOrder, providerId, whOrder.getDateOrdered(),
        									Env.getAD_User_ID(Env.getCtx()), whOrder.getC_PaymentTerm_ID(), docPurchaseOrder, principalWarehouse, purchaseId, true);
        						}
        						BigDecimal purchasePrice = DataQueries.getPurchasePriceTrans(trxName, p, providerId, lineLocator);
        						String descPurchaseLine = generalDesc + " - " + descPurchaseOrder;

        						Timestamp time = Env.getContextAsDate(Env.getCtx(), "#Date");
        						Integer orderLineTransCode = Documents.createOrderLineTrans(purchaseOrder, xp.getM_Product_ID(), xp.getQty(), purchasePrice,
        								descPurchaseLine, xp.getM_AttributeSetInstance_ID(), locatorID, providerId, true, providerId, time, null, null, null);
        						if (orderLineTransCode <= 0) {
        							StringBuilder stringBuilder = new StringBuilder();
									stringBuilder.append(WWorkOrder.class.getCanonicalName());
									stringBuilder.append(".completeRemanufactured - Error Create Purchase order Line :: ");
									stringBuilder.append(descPurchaseOrder);
									log.log(Level.SEVERE, stringBuilder.toString());
        							return null;
        						}
        					} // orden de compra si es bodega consignacion				
        				} else if (isService.booleanValue()) {
                            Boolean wok = CompleteSaleWebTrans.calculateCommisionProcess(trx.getTrxName(), xp, Integer.valueOf(0));
                            if (!wok.booleanValue()) {
                                org.zkoss.zul.Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorCreateMecComm"), labelInfo, 1, "z-messagebox-icon z-messagebox-exclamation");
                                return Integer.valueOf(-1);
                            }
                        }

                        descProd.append(Msg.translate(Env.getCtx(), "M_Product_ID") + " : " + p.getName() + ", " + Msg.translate(Env.getCtx(), "qty") + " : " + xp.getQty() + " - ");                        
                    }                  
                }

                this.descProduct = descProd.toString();
                
                // Validate if has consignment documents to complete
    			if (listInOutConsignmentPositive != null && listInOutConsignmentNegative != null && !listInOutConsignmentNegative.isEmpty()) 
    			{
    				List<Integer> consignmentShipmentsId = new ArrayList<Integer>();
    				
    				// Positive
    				Iterator<Integer> listPositive = listInOutConsignmentPositive.keySet().iterator();    				
    				while (listPositive.hasNext()) {
    					MInOut inOutConsignmentPositive = listInOutConsignmentPositive.get(listPositive.next());
    					MOrder poConsignmentPositive = new MOrder(whOrder.getCtx(), inOutConsignmentPositive.getC_Order_ID(), trxName);
    					
    					poConsignmentPositive.setDocAction(DocAction.ACTION_Complete);
    					if (!poConsignmentPositive.processIt(DocAction.ACTION_Complete)) {
    						throw new AdempiereException(poConsignmentPositive.getProcessMsg());
    					}

    					poConsignmentPositive.saveEx();

    					inOutConsignmentPositive.setDocAction(DocAction.ACTION_Complete);
    					if (!inOutConsignmentPositive.processIt(DocAction.ACTION_Complete)) {
    						throw new AdempiereException(inOutConsignmentPositive.getProcessMsg());
    					}

    					inOutConsignmentPositive.saveEx();	
    					consignmentShipmentsId.add(inOutConsignmentPositive.get_ID());
    				}

    				// Negative
    				Iterator<Integer> listNegative = listInOutConsignmentNegative.keySet().iterator();
    				while (listNegative.hasNext()) {
    					MInOut inOutConsignmentNegative = listInOutConsignmentNegative.get(listNegative.next());
    					MOrder poConsignmentNegative = new MOrder(whOrder.getCtx(), inOutConsignmentNegative.getC_Order_ID(), trxName);

    					poConsignmentNegative.setDocAction(DocAction.ACTION_Complete);
    					if (!poConsignmentNegative.processIt(DocAction.ACTION_Complete)) {
    						throw new AdempiereException(poConsignmentNegative.getProcessMsg());
    					}

    					poConsignmentNegative.saveEx();

    					inOutConsignmentNegative.setDocAction(DocAction.ACTION_Complete);
    					if (!inOutConsignmentNegative.processIt(DocAction.ACTION_Complete)) {
    						throw new AdempiereException(inOutConsignmentNegative.getProcessMsg());
    					}

    					inOutConsignmentNegative.saveEx();
    					consignmentShipmentsId.add(inOutConsignmentNegative.get_ID());
    				}
    				
    				// Updates description when the Warehouse Order generates and completes the MInOut
    				if (!consignmentShipmentsId.isEmpty()) {
    					whOrder.getCtx().put(CONSIGNMENT_SHIPMENTS, consignmentShipmentsId);    					
    				}
    			}

    			if (!inOutLinesByOLineId.isEmpty()) {
					whOrder.getCtx().put(CONSIGNMENT_DATA, inOutLinesByOLineId);
    			}
    			//Modificacion iMarch 05/03/2020  cuando no se generan lineas en OV
    			if(onlyservice) {
    				flag=true;
    			}//----fin 05/03/2020 con else
    			else {
                whOrder.completeIt(); // crear la entrega
                whOrder.setDocStatus("CO");
                whOrder.setDocAction("CL");
                whOrder.setProcessed(true);
                flag = whOrder.save();
    			}
                if (!flag.booleanValue()) {
                    org.zkoss.zul.Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorCompleteWhOrder"), labelInfo, 1, "z-messagebox-icon z-messagebox-exclamation");
                    return Integer.valueOf(-1);
                }

                if (rxp != null && flag.booleanValue()) {
                    Boolean ucOk = this.updateCosts(trx, rxp, total, whOrder, onlyservice);
                    if (!ucOk.booleanValue()) {
                        return Integer.valueOf(-1);
                    }
            		if(!onlyservice)
            			this.tmpRequest.setC_Order_ID(whOrder.getC_Order_ID());
                    whOrder.delete(true, trx.getTrxName());
                }

                if (rxp != null) {
                	MRequestType rt = MRequestType.get(Env.getCtx(), rTypeRmnFac);
                    MProduct p = MProduct.get(Env.getCtx(), rxp.getM_Product_ID());
                    if (p != null) {
                        StringBuilder productDescription = new StringBuilder((p.getHelp() != null) ? p.getHelp() : "");
						productDescription.append(rt.getName());
						productDescription.append(" : ");
						productDescription.append(this.tmpRequest.getDocumentNo());
						productDescription.append(" - ");
						
                        if (this.descRmn != null) {
                            productDescription.append(this.descRmn);
                        }
                        
                        productDescription.append(" - ");
                        productDescription.append(descProduct);
                        
                        p.set_TrxName(trx.getTrxName());
                        p.setHelp(productDescription.toString());
                        p.saveEx();
                    }
                }
                break;
            }
        } catch (Exception var40) {
            log.log(Level.SEVERE, this.getClass().getName() + ".completeRemanufactured - ERROR: " + var40.getMessage(), var40);
            return Integer.valueOf(-1);
        }

        return whOrder != null ? whOrder.getC_Order_ID() : Integer.valueOf(-1);
    }

	/**
	 * actualiza el costo del producto o crea la factura de compra segun
	 * parametro "SMJ-UPDATEAVCOST" - update product cost or create invoice
	 * depends parameter "SMJ-UPDATEAVCOST"
	 * 
	 * @param trx
	 * @param rxp
	 * @param total
	 * @return
	 */
	private Boolean updateCosts(Trx trx, MSMJWorkOrderLine rxp,
			BigDecimal total, MOrder whOrder, Boolean isOnlyService) {
		Boolean flag = true;
		try {
			MProduct p = MProduct.get(Env.getCtx(), rxp.getM_Product_ID());
			if (p != null) {
				
				HashMap<String, Integer> codes = DataQueries.getPListVersion(
						trx.getTrxName(), p, bpartnerId, tiresCategory,
						rxp.getlocator_ID(), 0, null);
				
				MProductPrice pp = null;
				
				if (codes != null) {
					pp = MProductPrice.get(Env.getCtx(), codes.get("PURCHASE"),
							rxp.getM_Product_ID(), trx.getTrxName());
				}
				
				if (pp == null) {
					Messagebox.show(
							Msg.translate(Env.getCtx(),
									"SMJMSGCantFindPriceList")
							+ " - "
							+ rxp.getProductName(), labelInfo,
							Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
				
				BigDecimal tot = pp.getPriceStd().add(total);
				pp.setPrices(tot, tot, tot);
				flag = pp.save();
				
				if (!flag) {
					return flag;
				}
				
				String isUpdate = MSysConfig.getValue("SMJ-UPDATEAVCOST",
						Env.getAD_Client_ID(Env.getCtx())).trim();
				
				if (isUpdate.equals("true")) {
					Integer costId = Integer.parseInt(MSysConfig.getValue(
							"SMJ_COSTELEMENT",
							Env.getAD_Client_ID(Env.getCtx())).trim());
					Integer costTypeId = Integer.parseInt(MSysConfig.getValue(
							"SMJ-MCOSTTYPEDEFAULT",
							Env.getAD_Client_ID(Env.getCtx())).trim());
					Integer acctSchemaId = Integer.parseInt(MSysConfig
							.getValue("SMJ-CACCTSCHEMADEFAULT",
									Env.getAD_Client_ID(Env.getCtx())).trim());
					MCost mc = MCost.get(Env.getCtx(),
							Env.getAD_Client_ID(Env.getCtx()), 0,
							p.getM_Product_ID(), costTypeId, acctSchemaId,
							costId, 0, trx.getTrxName());
					if (mc == null) {
						MAcctSchema as = MAcctSchema.get(Env.getCtx(),
								acctSchemaId, trx.getTrxName());
						mc = new MCost(p, 0, as, 0, costId);
					}
					mc.setCurrentCostPrice(tot);
					Boolean okmc = mc.save();
					if (!okmc) {
						Messagebox.show(
								Msg.translate(Env.getCtx(),
										"SMJMSGErrorUpdateCostProd")
								+ " - "
								+ rxp.getProductName(), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
				} else {
					Integer orgId = Env.getAD_Org_ID(Env.getCtx());
					int clientCode = Env.getAD_Client_ID(Env.getCtx());
					Integer documentInv = Integer.parseInt(MSysConfig.getValue(
							"SMJ-DOCUMENTAPINVOICE", clientCode, orgId).trim()); // obtiene
					// el
					// tipo
					// por
					// organizacion
					Integer ioId = 0;
					if(!isOnlyService) {
						ioId = DataQueries.getIoByOrder(trx.getTrxName(),
							whOrder.getC_Order_ID());
					
						if (ioId <= 0) {
							return false;
						}
					}
					
					MInOut io = new MInOut(Env.getCtx(), ioId, trx.getTrxName());
					
					String workOrder = whOrder.get_ValueAsString("smj_workorder");

					String sql = "SELECT M_PriceList_ID FROM M_PriceList WHERE AD_Client_ID=? AND AD_Org_ID = ? AND IsSOPriceList=? AND IsActive='Y' ORDER BY IsDefault DESC";
					int M_PriceList_ID = DB.getSQLValue (null, sql, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()), false);

					MInvoice inv = null;
					if(isOnlyService) {
						inv = DocumentsTrans.createInvoiceNoIo(trx.getTrxName(), "", 
								whOrder.getC_BPartner_ID(), whOrder.getDateOrdered(), 
								M_PriceList_ID, documentInv, whOrder.getC_PaymentTerm_ID(), 
								"", 0, workOrder, false, whOrder.getSalesRep_ID(), null, false);
					}
					else {
						inv = DocumentsTrans.createInvoice(trx.getTrxName(),
							io, "", whOrder.getDateOrdered(),
							M_PriceList_ID, documentInv,
							paytermDef, "", 0, workOrder, false);
					}
					
					if (inv == null) {
						Messagebox.show(Msg.translate(Env.getCtx(),
								"SMJMSGErrorCreateInvoice"), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
					// System.out.println(this.getClass().getName()+".updateCosts  invoice.."+inv.getDocumentNo());
					// MOrderLine lines[] = whOrder.getLines();
					// for (int i = 0; i < lines.length; i++) {
					// MOrderLine oLine = lines[i];
					//
					// Integer ioLineId =
					// DataQueries.getInOutLineByOrderLine(oLine.getC_OrderLine_ID(),
					// trx.getTrxName());
					// Integer invLineId = Documents.createInvoiceLine(inv,
					// oLine.getDescription(), oLine.getM_Product_ID(),
					// oLine.getPriceEntered(), oLine.getC_Tax_ID(),
					// oLine.getLineNetAmt(), oLine.getQtyEntered(), 0,
					// oLine.getC_OrderLine_ID(), "", oLine.getLine(),
					// ioLineId,0);
					// if(invLineId <= 0){
					// Messagebox.show(Msg.translate(Env.getCtx(),
					// "SMJMSGErrorCreateInvoice")+" - "+rxp.getProductName(),
					// labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					// return false;
					// }
					//
					// }//for
					// ACA REMANU CCHANGE
					//----------------------------------------------iMarch
					//Integer taxId = DataQueries.getProductTax(rxp
					//	.getM_Product_ID());
					Integer taxId = DataQueries.getProductTaxbyTrx(rxp
							.getM_Product_ID(), false); 				// false para compras
					//-----------------------------------------------------
					Integer invLineId = DocumentsTrans.createInvoiceLine(inv,
							rxp.getobservations(), rxp.getM_Product_ID(),
							total, taxId, total, rxp.getQty(), 0, 0, "", 10, 0,
							0);
					if (invLineId <= 0) {
						Messagebox.show(
								Msg.translate(Env.getCtx(),
										"SMJMSGErrorCreateInvoice")
								+ " - "
								+ rxp.getProductName(), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
					MRequestType rt = MRequestType.get(Env.getCtx(), rTypeRmnFac);
					if(!isOnlyService) {
						descRmn = Msg.translate(Env.getCtx(), "C_Order_ID") + " : "
								+ whOrder.getDocumentNo();
						descRmn = descRmn + " - "
								+ Msg.translate(Env.getCtx(), "M_InOut_ID") + " : "
								+ io.getDocumentNo();
						inv.setC_Order_ID(whOrder.getC_Order_ID());
					}
					else descRmn = "";
					inv.setDescription(rt.getName() +": "+ this.tmpRequest.getDocumentNo() + " - " + descRmn + " - "
							+ descProduct.toString());
					inv.completeIt();
					inv.setDocStatus(DocAction.STATUS_Completed);
					inv.setDocAction(DocAction.STATUS_Closed);
					inv.setProcessed(true);
					Boolean inok = inv.save();
					
					if (!inok) {
						Messagebox.show(
								Msg.translate(Env.getCtx(),
										"SMJMSGErrorCreateInvoice")
								+ " - "
								+ rxp.getProductName(), labelInfo,
								Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}
					invId = inv.getC_Invoice_ID();					
				}// if / else (isUpdate.equals("true"))
			}// if (p!= null )
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".updateCosts - ERROR: " + e.getMessage(), e);
			flag = false;
		}

		
		return flag;
	}// updateCosts

	/**
	 * valida que se ingresa con una organizacion - validate log in with
	 * organization
	 */
	private void validateLoginOrg() {
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (orgId <= 0) {
				Messagebox.show(Msg.translate(Env.getCtx(),
						"SMJMSGEnterWithOrganization"), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".validateLoginOrg - ERROR: " + e.getMessage(), e);
		}
		return;
	}// validateLoginOrg

	private void searchPlateValueChange(ValueChangeEvent evt) {
		if (evt.getNewValue() == null) {

			if (bpartnerId < 1 && !selectedPlate.isEmpty()) {

				String whereClause = "UPPER(SMJ_Plate) = UPPER(?)";
				MSMJVehicle vehicle = new Query(Env.getCtx(), MSMJVehicle.Table_Name, whereClause, null)
						.setParameters(selectedPlate)
						.first();

				if (vehicle != null) {
					temparioName = vehicle.getsmj_servicerategroup().getName();
				}

				searchPlate.getComponent().setText(selectedPlate);
				String states = stateComplete + "," + stateInvoiced;
				String rCodes = getWOOpen(selectedPlate, states);
				if (rCodes.length() > 0) {
					showInfoWO(selectedPlate, states);
				}
			} else {
				searchPlate.getComponent().setText("");
				searchPlate.setValue(null);
			}			

			return;
		}

		int smj_vehicle_id = (Integer) evt.getNewValue();

		if (smj_vehicle_id < 1) {
			return;
		}

		MSMJVehicle vehicle = new MSMJVehicle(Env.getCtx(), smj_vehicle_id, null);

		bpartnerId = DataQueries.getVehicleTender(vehicle.getsmj_plate(), "1");
		bpartnerSearch.setValue(bpartnerId);

		String states = stateComplete + "," + stateInvoiced;
		String rCodes = getWOOpen(vehicle.getsmj_plate(), states);
		// Boolean enter = true;
		if (rCodes.length() > 0) {
			showInfoWO(vehicle.getsmj_plate(), states);
			// enter = false;
		}// if (rCodes.length()>0)
		else {
			setVehicle(vehicle.getsmj_plate());
		}

		selectedPlate = vehicle.getsmj_plate();
		bpartnerSearch.getComponent().setFocus(true);
	}

	private void searchBPartnerValueChange(ValueChangeEvent evt) {
		if (!isEditable) {
			// System.out.println(this.getClass().getCanonicalName()+" - bpartnerTextBox");
			Messagebox.show(
					Msg.translate(Env.getCtx(), "SMJMSGWONoModificable"),
					labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			bpartnerSearch.getComponent().setText("");
			descTextBox.setFocus(true);
			return;
		}

		Object value = evt.getNewValue();
		if (value != null) {
			bpartnerId = ((Integer) value).intValue();
			setPartner();
			//showInfoPlate(bpartnerId.toString());
		} else {
			bpartnerId = 0;
		}
	}

	/**
	 * muestra la ventana de informacion de placas por tercero y trae la placa seleccionada - 
	 * show info plate window and returns selected plate data 
	 */
	private void showInfoPlate(String value){			
		searchPlate.getComponent().setText(value);
		searchPlate.onEvent(new Event(Events.ON_CHANGE));
	}//showInfoPlate
	
	private Consumer<MInOutLine> storeInOutLinesByOLineId(final Map<Integer, List<MInOutLine>> inOutLinesByOLineId, final Integer code) {
		return new Consumer<MInOutLine>() {
			@Override
			public void accept(MInOutLine ioLine) throws Exception {
				// Registers the "InOutLine" to update the InOutLine created in the "completeIt" process of warehouse order
				List<MInOutLine> inOutLines;
				
				if (inOutLinesByOLineId.containsKey(code)) {
					inOutLines = inOutLinesByOLineId.get(code);
				} else {
					inOutLines = new ArrayList<MInOutLine>();
				}
				
				inOutLines.add(ioLine);
				
				inOutLinesByOLineId.put(code, inOutLines);				
			}
		};
	}
}// WWorkOrder
