package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListCell;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.adempiere.webui.component.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.smj.model.MSMJFuelLine;
import com.smj.model.MSMJFuelSales;
import com.smj.util.DataQueriesTrans;
import com.smj.util.ShowWindowTrans;

/**
 * @version <li>SmartJSP: WFuelSales, 2013/02/19
 *          <ul TYPE ="circle">
 *          <li>Clase para maneja la forma de venta de combustible
 *          <li>Class to form fuel sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class WFuelSales extends FuelSales  implements IFormController, EventListener<Event>, 
WTableModelListener, ValueChangeListener{

	private CustomForm form = new CustomForm();
	@Override 
	public ADForm getForm() {
		return form;
	}
	
	/**
	 * WFuelSales constructor
	 */
	public WFuelSales() {
		try	{
			dynInit();
			zkInit(); 
			form.addEventListener(Events.ON_OPEN, this);
			AuFocus auf = new AuFocus(bpartnerSearch.getComponent());
			Clients.response(auf);
		}catch(Exception e){
			log.log(Level.SEVERE, "", e);
		}//try/catch
	}//WFuelSales
	
	//variables de la forma web (paneles)
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	//---Panel Sur ------
//	private Panel southPanel = new Panel();
	//---Panel de informacion ------
	private Borderlayout infoPanel = new Borderlayout();
	//panel Table
	private Panel tablePanel = new Panel();
	private Borderlayout tableLayout = new Borderlayout();
	private WListbox dataTable = ListboxFactory.newDataTable();
	//campos a ser mostrados
	private Label productLabel = new Label();
	private Listbox productListbox = new Listbox();
	private Label bPartnerLabel = new Label();
	private WSearchEditor bpartnerSearch = null;
	private Label entryDateLabel = new Label();
	private WDateEditor entryDate = new WDateEditor();
	private Label warehouseLabel = new Label();
	private Listbox warehouseListbox = new Listbox();
	private Label islandLabel = new Label();
	private Listbox islandListbox = new Listbox();
	private Label fuelLineLabel = new Label();
	private Listbox fuelLineListbox = new Listbox();
	private Label hundredEndLabel = new Label();
	private Decimalbox hundredEndDecimalbox = new Decimalbox();
	
	private Label hundredStartLabel = new Label();
	private Decimalbox hundredStartDecimalbox = new Decimalbox();
	
	private Button addButton = new Button ();
	private Integer fuelIslandID = -1;
	private Integer fuelTankID = -1;
	
	
	private Vector<Vector<Object>> data = null;
	private Vector<Vector<Object>> dataSales = new Vector<Vector<Object>>();
	private Date date = null;
	
	private Integer bpartnerId = 0;
	private String bpartnerName = "";
	private Integer locationID = 0;
	private HashMap<String, LinkedList<MSMJFuelSales>> fuelList = null;
	private HashMap<String,BigDecimal> fuelAgg = null;
	private HashMap<String, Integer> warehouseKey = null;
	
	// codigos generales
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer payTermId = Integer.parseInt(MSysConfig.getValue("SMJ_PAYTERM",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer docOrderId = Integer.parseInt(MSysConfig.getValue("SMJ-DOCWAREHOUSEORDER",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer currencyId = Integer.parseInt(MSysConfig.getValue("SMJ_CURRENCY",Env.getAD_Client_ID(Env.getCtx())).trim());
	
	/**
	 *  Prepara los campos dinamicos
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	private void dynInit() throws Exception 	{
		//  BPartner
		int AD_Column_ID = 2762;        //  C_Order.C_BPartner_ID 
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, AD_Column_ID, DisplayType.Search);
		bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
		bpartnerSearch.addValueChangeListener(this);
		listWarehouse();
		listFuelIsland(-1);
		listFuelLine(-1);
		listProduct();
		confirmPanel.setEnabledAll(true);
		dataSales = new Vector<Vector<Object>>();
		fuelList = new HashMap<String, LinkedList<MSMJFuelSales>>();
		fuelAgg = new HashMap<String, BigDecimal>();
		hundredEndDecimalbox.setValue(Env.ZERO);
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
		//Adicion iMarch
		entryDate.addValueChangeListener(this);
		//----
	}//dynInit
	
	/**
	 *  Inicializa los componentes estaticos
	 *  Static Init (inicializa los componentes estaticos)
	 *  @throws Exception
	 */
	private void zkInit() throws Exception	{
		form.appendChild(mainLayout);
		//mainLayout.setWidth("99%");
		//mainLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		//parameterPanel.setHeight("100%");
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		//parameterLayout.setWidth("800px");
		//parameterLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(parameterLayout, "800px");
		ZKUpdateUtil.setHeight(parameterLayout, "100%");
		parameterPanel.appendChild(parameterLayout);
		
		//establecer etiquetas - fill label
		bPartnerLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelServiceVendors"));
		productLabel.setText("" + Msg.translate(Env.getCtx(), "M_Product_ID"));
		entryDateLabel.setText("" + Msg.translate(Env.getCtx(), "entryDate"));
		warehouseLabel.setText("" + Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		islandLabel.setText("" + Msg.translate(Env.getCtx(), "smj_FuelIsland_ID"));
		fuelLineLabel.setText("" + Msg.translate(Env.getCtx(), "smj_FuelLine_ID"));
		hundredEndLabel.setText("" + Msg.translate(Env.getCtx(), "hundredEnd"));
		hundredStartLabel.setText("" + Msg.translate(Env.getCtx(), "actualHundred"));
		
		//botones
		addButton.setLabel("Adicionar");
		addButton.addActionListener(this);
		
		//definicion de paneles y ubicacion en la forma.
		South south = new South();
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();
		
		//row
		row = rows.newRow();
		//bPartnerLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(bPartnerLabel, "100px");
		row.appendChild(bPartnerLabel.rightAlign());
		row.appendChild(bpartnerSearch.getComponent());
		//entryDateLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(entryDateLabel, "100px");
		row.appendChild(entryDateLabel.rightAlign());
		row.appendChild(entryDate.getComponent());
		
		//row
		row = rows.newRow();
		
		//warehouseLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(warehouseLabel, "100px");
		row.appendChild(warehouseLabel.rightAlign());
		//warehouseListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(warehouseListbox, "150px");
		row.appendChild(warehouseListbox);
		//productLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(productLabel, "100px");
		row.appendChild(productLabel.rightAlign());
		//productListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(productListbox, "150px");
		row.appendChild(productListbox);
		
		//row
		row = rows.newRow();
		//islandLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(islandLabel, "100px");
		row.appendChild(islandLabel.rightAlign());
		//islandListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(islandListbox, "150px");
		row.appendChild(islandListbox);
		//fuelLineLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(fuelLineLabel, "100px");
		row.appendChild(fuelLineLabel.rightAlign());
		//fuelLineListbox.setWidth("150px");
		ZKUpdateUtil.setWidth(fuelLineListbox, "150px");
		row.appendChild(fuelLineListbox);
		
		//row
		row = rows.newRow();
		//hundredEndLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(hundredEndLabel, "100px");
		row.appendChild(hundredEndLabel.rightAlign());
		//hundredEndDecimalbox.setWidth("150px");
		ZKUpdateUtil.setWidth(hundredEndDecimalbox, "150px");
		row.appendChild(hundredEndDecimalbox);
		//hundredStartLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(hundredStartLabel, "100px");
		row.appendChild(hundredStartLabel.rightAlign());
		//hundredStartDecimalbox.setWidth("150px");
		ZKUpdateUtil.setWidth(hundredStartDecimalbox, "150px");
		row.appendChild(hundredStartDecimalbox);
		hundredStartDecimalbox.setReadonly(true);
		
		//row
		row = rows.newRow();
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(addButton);
		
		//---------PANEL SUR---------------------------------------------
		Panel southPanel = new Panel();
		south = new South();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		confirmPanel.addActionListener(this);
		
		southPanel.appendChild(confirmPanel);
		
		//----Configura calendar panel-------------------------------------------------
		tablePanel.appendChild(tableLayout);
		//tablePanel.setWidth("100%");
		//tablePanel.setHeight("100%");
		ZKUpdateUtil.setWidth(tablePanel, "100%");
		ZKUpdateUtil.setHeight(tablePanel, "100%");
		//tableLayout.setWidth("100%");
		//tableLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(tableLayout, "100%");
		ZKUpdateUtil.setHeight(tableLayout, "100%");
		tableLayout.setStyle("border: none");
		north = new North();
		north.setStyle("border: none");
		tableLayout.appendChild(north);
		south = new South();
		south.setStyle("border: none");
		tableLayout.appendChild(south);
		Center center = new Center();
		tableLayout.appendChild(center);
		center.appendChild(dataTable);
		//dataTable.setWidth("99%");
		//dataTable.setHeight("99%");
		ZKUpdateUtil.setWidth(dataTable, "99%");
		//ZKUpdateUtil.setHeight(dataTable, "99%");
		center.setStyle("border: none");
		//---Coloca el Tabla panel en la pantalla----------------------
		center = new Center();
		center.setVflex("1");
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		
		infoPanel.setStyle("border: none");
		//infoPanel.setWidth("100%");
		//infoPanel.setHeight("100%");
		ZKUpdateUtil.setWidth(infoPanel, "100%");
		ZKUpdateUtil.setHeight(infoPanel, "100%");
		
		north = new North();
		north.setStyle("border: none");
		//north.setHeight("100%");
		ZKUpdateUtil.setHeight(north, "100%");
		infoPanel.appendChild(north);
		north.appendChild(tablePanel);
		north.setSplittable(true);
	}//zkInit
	
	@Override
	public void valueChange(ValueChangeEvent evt) {
		String name = evt.getPropertyName();
		Object value = evt.getNewValue();
		if (value==null){
			return;
		}//if Value
		//  BPartner
		if (name.equals("C_BPartner_ID")){
			bpartnerSearch.setValue(value);
			bpartnerId = ((Integer)value).intValue();
			bpartnerName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bpartnerId);
			locationID = DataQueriesTrans.getLocationPartner(bpartnerId);
			if	(locationID<= 0){
				try {
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGPartnerValidLocation"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				} catch (Exception e) {
					e.printStackTrace();
				}
				bpartnerId = -1;
				bpartnerName = "";
				bpartnerSearch.setValue("");
			}
		}//C_BPartner_ID
		//Adicion iMarch  -- Valida Fecha que sea consecutiva al ultimo ingreso
		if (name.equals("Date")){
			if(entryDate.getValue()!=null){
				date = (Date) entryDate.getValue();
				if(!dateEnable(date)){
					Messagebox.showDialog("La fecha no es correcta con respecto a las ventas actuales","Error", Messagebox.OK, Messagebox.EXCLAMATION);
					entryDate.setValue(null);
				}
			}
		}//Date
	}//valueChange

	@Override
	public void tableChanged(WTableModelEvent event) {
	}

	@Override
	public void onEvent(Event e) throws Exception {
		if (Env.getAD_Org_ID(Env.getCtx())<=0){
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGEnterWithOrganization"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		else if (e.getTarget().equals(warehouseListbox)){
			fuelTankID = (Integer) warehouseListbox.getSelectedItem().getValue();
			listFuelIsland(fuelTankID);
			return;
		}//warehouseListbox
		else if (e.getTarget().equals(islandListbox)){
			fuelIslandID = (Integer) islandListbox.getSelectedItem().getValue();
			listFuelLine(fuelIslandID);
			return;
		}//islandListbox
		else if (e.getTarget().equals(fuelLineListbox)){
			if(fuelLineListbox.getSelectedItem() == null || fuelLineListbox.getSelectedItem().getValue() == null
					|| (Integer)fuelLineListbox.getSelectedItem().getValue()<=0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectFuelLine"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			MSMJFuelLine line = new MSMJFuelLine(Env.getCtx(), (Integer) fuelLineListbox.getSelectedItem().getValue(), null);
			hundredStartDecimalbox.setValue(line.getactualhundred());
		}//fuelLineListbox
		else if (e.getTarget().equals(addButton)){
			if(bpartnerId<=0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGValidPartner"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if(entryDate == null || entryDate.getValue() == null){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGValidDate"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}else{
				date = (Date) entryDate.getValue();
			}
			if(warehouseListbox.getSelectedItem() == null || warehouseListbox.getSelectedItem().getValue() == null
					|| (Integer)warehouseListbox.getSelectedItem().getValue()<=0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGValidWarehouse"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if(productListbox.getSelectedItem() == null || productListbox.getSelectedItem().getValue() == null
					|| (Integer)productListbox.getSelectedItem().getValue()<=0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGValidProduct"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if(fuelLineListbox.getSelectedItem() == null || fuelLineListbox.getSelectedItem().getValue() == null
					|| (Integer)fuelLineListbox.getSelectedItem().getValue()<=0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectFuelLine"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if(hundredEndDecimalbox.getValue() == null || hundredEndDecimalbox.getValue().compareTo(Env.ZERO)==0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGEnterHundred"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			bpartnerSearch.setReadWrite(false);
			entryDate.setReadWrite(false);
			addElement();
		}//addButton
		else if(e.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
			if (!DataQueriesTrans.isPeriodOpen(date, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()))){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGClosedPeriod"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
			process();
			bpartnerSearch.setReadWrite(true);
			entryDate.setReadWrite(true);
			clearFields();
		}//OK
		else if (e.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))){
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantCancel"), labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, 
					new Callback<Integer>() {

						@Override
						public void onCallback(Integer result) {
							if (result.equals(Messagebox.OK)) {
								SessionManager.getAppDesktop().closeActiveWindow();
							}
						}
					});				
			return;
		}//CANCEL
		else if (e.getTarget().equals(dataTable)){
			
			Messagebox.showDialog(Msg.translate(Env.getCtx(),
					"SMJMSGDeleteRecord"), labelInfo, Messagebox.OK
					| Messagebox.CANCEL, Messagebox.QUESTION,
					new Callback<Integer>() {
				@Override
				public void onCallback(Integer result) {
					if (result.equals(Messagebox.OK)) {
						List<Component> items = dataTable.getSelectedItem().getChildren();
						int index = dataTable.getSelectedIndex();
						ListCell dato = (ListCell) items.get(1);
						String island = (String) dato.getValue();
						ListCell dato2 = (ListCell) items.get(2);
						String line = (String) dato2.getValue();
						ListCell dato0 = (ListCell) items.get(0);
						String warehouse = (String) dato0.getValue();
						BigDecimal agg = fuelAgg.get(warehouse);
						LinkedList<MSMJFuelSales> list = fuelList.get(warehouse);
						if (list == null){
							list = new LinkedList<MSMJFuelSales>();
						}
						MSMJFuelSales fuel = getElementList(island, line, list);
						agg = agg.add(fuel.getqtysold());
						fuelAgg.put(warehouse, agg);
						if	(fuel != null){
							list.remove(fuel);
						}
						fuelList.put(warehouse, list);
						dataTable.removeItemAt(index);
						dataSales.remove(index);
					}
				}
			});	
		}//if dataTable
	}//onEvent

	/***
	 * regresa el elemento de la lista buscado por isla y manguera - 
	 * returns list element by island and fuel line 
	 * @param islandName
	 * @param lineName
	 * @param list
	 * @return
	 */
	private MSMJFuelSales getElementList(String islandName, String lineName, LinkedList<MSMJFuelSales> list){
		MSMJFuelSales fuel = null;
		Boolean flag = true;
		Iterator<MSMJFuelSales> it = list.iterator();
		while (it.hasNext() && flag){
			MSMJFuelSales e = it.next();
			if (e.getIslandName().equals(islandName) && e.getLineName().equals(lineName)){
				flag = false;
				fuel = e;
			}
		}
		return fuel;
	}//getElementList
	
	/**
	 * addiciona un elemento a la lista correspondiente -
	 * add element to corespond list
	 * @throws InterruptedException
	 */
	private void addElement() throws InterruptedException{
		String warehouseName = warehouseListbox.getSelectedItem().getLabel();
		String islandName = islandListbox.getSelectedItem().getLabel();
		String lineName = fuelLineListbox.getSelectedItem().getLabel();
		LinkedList<MSMJFuelSales> list = fuelList.get(warehouseName);
		if (list == null){
			list = new LinkedList<MSMJFuelSales>();
		}
		MSMJFuelSales fuel = getElementList(islandName,	lineName, list);
		if	(fuel != null){
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGFuelLineExist"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		MSMJFuelSales f = new MSMJFuelSales(Env.getCtx(), 0, null);
		f.setC_BPartner_ID(bpartnerId);
		f.setentrydate(new Timestamp(date.getTime()));
		f.setM_Warehouse_ID((Integer) warehouseListbox.getSelectedItem().getValue());
		f.setM_Product_ID((Integer) productListbox.getSelectedItem().getValue());
		f.setsmj_FuelIsland_ID((Integer) islandListbox.getSelectedItem().getValue());
		f.setsmj_FuelLine_ID((Integer) fuelLineListbox.getSelectedItem().getValue());
		f.setIslandName(islandName);
		f.setLineName(lineName);
		BigDecimal hundred = hundredEndDecimalbox.getValue();
		f.sethundredend(hundred);
		MSMJFuelLine fline = new MSMJFuelLine(Env.getCtx(), f.getsmj_FuelLine_ID(), null);
		BigDecimal lastHunderd = fline.getactualhundred();
		f.setactualhundred(lastHunderd);
		
		if (f.gethundredend().compareTo(lastHunderd) < 1) {
			Messagebox.showDialog(Msg.parseTranslation(Env.getCtx(), "@hundredEnd@ @SMJMSG_MustBeGreaterThan@ @Actualhundred@"), labelInfo, Messagebox.OK, Messagebox.ERROR);
			return;
		}
		
		BigDecimal qty = f.gethundredend().subtract(lastHunderd);
		if (qty.compareTo(Env.ZERO)<0){
			BigDecimal tmp = fline.getlimithundred().subtract(lastHunderd);
			qty = tmp.add(f.gethundredend());
		}
		// SMJMSGNoQtyWarehouse
		BigDecimal agg = fuelAgg.get(warehouseName);
		if (agg == null){
			agg = DataQueriesTrans.getQtyWarehouse(null, (Integer) productListbox.getSelectedItem().getValue(), (Integer) warehouseListbox.getSelectedItem().getValue());
		}
		BigDecimal totalWarehouse = agg.subtract(qty);
		if (totalWarehouse.compareTo(Env.ZERO) < 0){
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGNoQtyWarehouse"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		fuelAgg.put(warehouseName, totalWarehouse);
		MProduct p = new MProduct(Env.getCtx(), f.getM_Product_ID(), null);
		HashMap<String, Integer> codes = DataQueriesTrans.getPListVersion(null, p, -1, -1, f.getM_Warehouse_ID(), 0, null);
		MProductPrice sp = null;
		if (codes != null){
			sp = MProductPrice.get(Env.getCtx(), codes.get("SALES"), f.getM_Product_ID(), null);
		}

		try {
			f.setgalonprice(sp.getPriceStd());
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".addElement - ERROR:: ", e);
		}
		BigDecimal total = f.getgalonprice().multiply(qty);
		f.setqtysold(qty);
		f.settotalprice(total);
		f.setisprocessed(false);
		list.add(f);
		fuelList.put(warehouseName, list);
		Vector<Object> rc = new Vector<Object>();
		rc.add(warehouseName);
		rc.add(islandName);
		rc.add(lineName);
		rc.add(NumberFormat.getInstance().format(hundred));
		rc.add(NumberFormat.getInstance().format(qty));
		rc.add(productListbox.getSelectedItem().getLabel());
		rc.add(NumberFormat.getInstance().format(sp.getPriceStd()));
		rc.add(NumberFormat.getInstance().format(total));
		dataSales.add(rc);
		loadTable(dataSales);
		clearFields();
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
	}//addElement

	/**
	 * limipia campos - clear fields
	 */
	private void clearFields(){
		warehouseListbox.setValue(-1);
		productListbox.setValue(-1);
		islandListbox.setValue(-1);
		listFuelLine(-1);
		hundredEndDecimalbox.setValue(Env.ZERO);
		hundredStartDecimalbox.setValue(Env.ZERO);;
	}//clearFields
	
	/**
	 * Llena el combo de almacenes
	 * fill warehouse list
	 */
	private void listWarehouse(){
		warehouseListbox.removeAllItems();
		warehouseKey = new HashMap<String, Integer>();
		data = getWarehouseList(Env.getAD_Client_ID(Env.getCtx()));
		warehouseListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			warehouseListbox.appendItem((String) line.get(1), line.get(0));
			warehouseKey.put((String) line.get(1), (Integer) line.get(0));
		}//for
		warehouseListbox.setMold("select");
		warehouseListbox.addActionListener(this);
	}//listWarehouse
	
	/**
	 * Llena el combo de islas
	 * fill fuel islans list
	 */
	private void listFuelIsland(Integer tankId){
		islandListbox.removeAllItems();
		data = getIslandList(Env.getAD_Client_ID(Env.getCtx()), tankId);
		islandListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			islandListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		islandListbox.setMold("select");
		islandListbox.addActionListener(this);
	}//listWarehouse
	
	/**
	 * Llena el combo de mangeras
	 * fill fuel line list
	 */
	private void listFuelLine(Integer islandId){
		fuelLineListbox.removeAllItems();
		data = getFuelLineList(Env.getAD_Client_ID(Env.getCtx()), islandId);
		fuelLineListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			fuelLineListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		fuelLineListbox.setMold("select");
		fuelLineListbox.removeActionListener(this);
		fuelLineListbox.addActionListener(this);
	}//listWarehouse

	/**
	 * Llena el combo  de producto
	 * fill product list
	 */
	private void listProduct(){
		productListbox.removeAllItems();
		data = getProductList(Env.getAD_Client_ID(Env.getCtx()), 1000024);
		productListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			productListbox.appendItem((String) line.get(1),line.get(0));
		}//for
		productListbox.setMold("select");
		productListbox.addActionListener(this);
	}//listProduct
	
	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(Vector<Vector<Object>> datat){
		Vector<String> columnNames = getTableColumnNames();
		dataTable.clear();
		dataTable.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelP = new ListModelTable(datat);
		dataTable.setData(modelP, columnNames);
		dataTable.removeActionListener(this);
		dataTable.addActionListener(this);
		setTableColumnClass(dataTable);
		
		for (Component component : dataTable.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable
	
	/**
	 * crea las ordenes de venta y muestra las facturas - 
	 * create sales order and show them
	 * @throws InterruptedException
	 */
	@SuppressWarnings("rawtypes")
	private void process() throws InterruptedException{
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		
		String desc = "";
		Boolean flag = true;
		LinkedList<Integer> oList = new LinkedList<Integer>();
		String orderNos = "";
//		String invoices = "";
		try{
			Iterator it = fuelList.entrySet().iterator();
			while (it.hasNext()) {
				flag = true;
				Map.Entry hml = (Map.Entry)it.next();
				String wkey = (String) hml.getKey();
				desc = bpartnerName+" - "+wkey;
				LinkedList<MSMJFuelSales> list = fuelList.get(wkey);
				if(list.size()>0){
					Integer warehouseId = warehouseKey.get(wkey);
//					System.out.println(this.getClass().getName()+".process ...date ..."+date);
					MOrder order = createOrder(trx, desc, bpartnerId, date, warehouseId, salesId, Env.getAD_User_ID(Env.getCtx()));
					Iterator<MSMJFuelSales> itl = list.iterator();
					while (itl.hasNext()){
						MSMJFuelSales e = itl.next();
						e.set_TrxName(trx.getTrxName());
						Boolean lok = createOrderLine(trx, order, e.getM_Product_ID(), e.getqtysold(), e.getgalonprice(), 
								e.gettotalprice(), desc+" - "+e.getIslandName()+" - "+e.getLineName());
						e.setC_Order_ID(order.getC_Order_ID());
						Boolean sok = e.save();
//						MSMJFuelLine line = new MSMJFuelLine(Env.getCtx(), e.getsmj_FuelLine_ID(), trx.getTrxName());
//						line.setactualhundred(e.gethundredend());
//						Boolean fok = line.save();
						Boolean fok = DataQueriesTrans.updateFuelLine(trx.getTrxName(), e.getsmj_FuelLine_ID(), e.gethundredend());
						if (!sok || !lok || !fok){
							flag = false;
						}
					}//while lista
					order.completeIt();
					order.setDocStatus(DocAction.STATUS_Completed);
					order.setDocAction(DocAction.STATUS_Closed);
					order.setProcessed(true);
					Boolean pok = order.save();
					if (!pok || !flag) {
						trx.rollback();
						trx.close();
						confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
						log.log(Level.SEVERE, "Error Create Order :: "+ desc);
						return ;
					}else{
						oList.add(order.getC_Order_ID());
						orderNos = orderNos+order.getDocumentNo()+", ";
					}
				}//if(list.size()>0)

			}//while hashMap
		}//try
		catch (Exception e) {
			trx.rollback();
			trx.close();
			confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
			log.log(Level.SEVERE, "Error Create Order Sales :: "+ desc, e);
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGProcessError")+":: "+desc+"- "+ e.getMessage(), labelInfo, Messagebox.OK, Messagebox.ERROR);
			return;
		}finally{
			trx.commit();
			trx.close();
		}
		Iterator<Integer> ito = oList.iterator();
		
		while (ito.hasNext()){
			Integer ooId = ito.next();
			if (ooId >0){
				ShowWindowTrans.openSalesOrder(ooId);
			}// (ooId >0)
		}//ito.hasNext()
		if (orderNos.endsWith(", ")){
			orderNos = orderNos.substring(0, orderNos.length()-2);
		}
		Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGOrdersCreated")+orderNos, labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
		dataSales = new Vector<Vector<Object>>();
		loadTable(dataSales);
		fuelList = new HashMap<String, LinkedList<MSMJFuelSales>>();
		bpartnerId = -1;
		bpartnerName = "";
		bpartnerSearch.setValue("");
		date = null;
		entryDate.setValue(null);
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
	}//process
	
	/**
	 * crea la orden de venta - 
	 * create sales order 
	 * @param trx
	 * @param desc
	 * @param partnerId
	 * @param orderDate
	 * @param warehouseId
	 * @param priceListId
	 * @param SalesRepId
	 * @return MOrder
	 */
	private MOrder createOrder(Trx trx, String desc, Integer partnerId, Date orderDate, 
			Integer warehouseId, Integer priceListId, Integer SalesRepId){

		MOrder order = null;
		try {
			order = new MOrder(Env.getCtx(), 0, trx.getTrxName());
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setDateAcct(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationID);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(currencyId);
			order.setCopyFrom("N"); 
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);
			//todo termino de pago
			order.setC_PaymentTerm_ID(payTermId); 
			order.setM_Warehouse_ID(warehouseId);

				//para orden de venta - For order Sales
				order.setIsSOTrx(true); 
				order.setC_DocTypeTarget_ID(docOrderId);//SMJ-DOCWAREHOUSEORDER
				order.setC_DocType_ID(docOrderId);//0
				order.setPaymentRule("B"); //B para venta
				order.setM_PriceList_ID(salesId);
		
			Boolean pok = order.save();
//			System.out.println("order creada............."+order.getC_Order_ID());
			if (!pok) {
				trx.rollback();
				log.log(Level.SEVERE, "Error save Create Order :: "	+ desc);
				return null;
			} 

		} catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, "Error Create Order :: "	+ desc, e);
		} 
		return order;
	}// createOrder

	/**
	 * crea las lineas de la orden - 
	 * create line order
	 * @param trx
	 * @param order
	 * @param animal
	 * @return Boolean
	 */
	private Boolean createOrderLine(Trx trx, MOrder order, Integer productId, BigDecimal qty,
			BigDecimal price,BigDecimal total, String description) {
		Boolean flag = false;
		
		Integer uomId = DataQueriesTrans.getProductUom(productId);
		Integer taxId = DataQueriesTrans.getProductTax(productId);
		try {
			MOrderLine line = new MOrderLine(Env.getCtx(), 0, trx.getTrxName());
			line.setC_Order_ID(order.getC_Order_ID());
			line.setIsActive(true);
			line.setDescription(description);
			line.setC_BPartner_ID(order.getC_BPartner_ID());
			line.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
			line.setM_Warehouse_ID(order.getM_Warehouse_ID());
			line.setC_Currency_ID(currencyId);
			line.setDateOrdered(order.getDateOrdered());
			line.setDatePromised(order.getDatePromised());
			line.setM_Product_ID(productId);
			line.setC_UOM_ID(uomId);
			line.setDiscount(Env.ZERO);
			line.setFreightAmt(Env.ZERO); 
			line.setC_Tax_ID(taxId);
			line.setIsDescription(false);
			line.setQtyEntered(qty);
			line.setQtyOrdered(qty);
			line.setQtyDelivered(Env.ZERO);
			line.setQtyInvoiced(Env.ZERO);
			line.setQtyReserved(Env.ZERO);
			line.setRRAmt(Env.ZERO);
			line.setPriceList(price);
			line.setPriceActual(price);
			line.setPriceLimit(Env.ZERO);
			line.setPriceEntered(price);
			line.setPriceCost(Env.ZERO);
			line.setProcessed(false);
			line.setQtyLostSales(Env.ZERO);
			flag = line.save();
		} catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, "Error Create purchase OrderLine :: "	+ description, e);
		} 
		return flag;
	}// createOrderLine
	
}//WFuelSales
