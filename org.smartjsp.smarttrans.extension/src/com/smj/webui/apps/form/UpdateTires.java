package com.smj.webui.apps.form;

import java.util.Vector;

import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class UpdateTires {
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	public Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		columnNames.add(Msg.translate(Env.getCtx(), "Position"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_PRoduct_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "serial"));
		columnNames.add(Msg.translate(Env.getCtx(), "valuetire"));
		columnNames.add(Msg.translate(Env.getCtx(), "valuebrake"));
		columnNames.add(Msg.translate(Env.getCtx(), "status"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	public void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
}//UpdateTires
