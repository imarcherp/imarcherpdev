/**
 * 
 */
package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Vector;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;
import com.smj.util.DocumentsTrans;
import com.smj.util.ShowWindow;
import com.smj.util.UtilTrans;

/**
 * @author Dany Diaz - SmartJSP (http://www.smartjsp.com/)
 *
 */
public class ServiceRequestInvoice {

	/**
	 * Layout for BPartner Table
	 * 
	 * @return
	 */
	private String starDate = MSysConfig.getValue("iMarch_SRInvoiceLimitDate", "2013/01/01", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
	
	protected ColumnInfo[] getBPartnerLayout() {
		ColumnInfo[] bpartnerLayout = new ColumnInfo[] { new ColumnInfo("", "C_BPartnerOwner_ID", IDColumn.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name", String.class),
				new ColumnInfo(Msg.parseTranslation(Env.getCtx(), "# @SMJ_LblServiceRequests@"), "ServiceRequests", Integer.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "GrandTotal"), "Total", BigDecimal.class) };

		return bpartnerLayout;
	}

	/**
	 * Layout for BPartner Table
	 * 
	 * @return
	 */
	protected ColumnInfo[] getServiceRequestLayout() {
		ColumnInfo[] serviceRequestLayout = new ColumnInfo[] { 
				new ColumnInfo("", "R_Request_ID", BigDecimal.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Select"), "Select", Boolean.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "R_Request_ID"), "DocumentNo", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Created"), "Created", Timestamp.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"), "Name", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "SMJ_Plate"), "SMJ_Plate", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_Invoice_ID"), "Invoice", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "SMJ_VehicleCharge"), "SMJ_VehicleCharge", BigDecimal.class) };

		return serviceRequestLayout;
	}
	
	public void setServiceRequestColumnClass(IMiniTable SRequestTable) {
		
		int i=0;
		SRequestTable.setColumnClass(i++, Boolean.class, false);
		SRequestTable.setColumnClass(i++, String.class, true);
		SRequestTable.setColumnClass(i++, Timestamp.class, true);
		SRequestTable.setColumnClass(i++, String.class, true);
		SRequestTable.setColumnClass(i++, String.class, true);
		SRequestTable.setColumnClass(i++, String.class, true);
		SRequestTable.setColumnClass(i++, BigDecimal.class, true);
	}
	
	public Vector<String> getServiceRequestColumnNames() 	{
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "Select"));
		columnNames.add(Msg.translate(Env.getCtx(), "R_Request_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Created"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJ_Plate"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_Invoice_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJ_VehicleCharge"));
		return columnNames;
	}

	/**
	 * Fill BPartner Table
	 * 
	 * @param table
	 *            (BPartner Table)
	 * @throws Exception
	 */
	protected void bPartnerTableLoad(IMiniTable table) throws Exception {
		StringBuilder sql = new StringBuilder("SELECT C_BPartnerOwner_ID, bp.Name, COUNT(C_BPartnerOwner_ID) AS ServiceRequests, ")
				.append("SUM(COALESCE(SMJ_VehicleCharge, 0)) AS Total FROM R_Request hdr ")
				.append("JOIN C_BPartner bp ON (bp.C_BPartner_ID = hdr.C_BPartnerOwner_ID) ")
				.append("WHERE R_RequestType_ID = ? AND SMJ_InvoiceProvider_ID IS NULL AND SMJ_IsInvoiceCharge = 'Y' AND R_Status_ID = ? ")
				.append(" AND hdr.Created > '").append(starDate).append("' ")
				.append(" AND C_BPartnerOwner_ID <> ?")
				.append("GROUP BY C_BPartnerOwner_ID, bp.Name")
				.append(" ORDER BY bp.Name");
				

		PreparedStatement pstm = null;
		ResultSet rs = null;
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());

		try {
			pstm = DB.prepareStatement(sql.toString(), null);
			pstm.setInt(1, MSysConfig.getIntValue("SMJ_SSRequestType", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(2, MSysConfig.getIntValue("SMJ_SSRequestStatus", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(3, MSysConfig.getIntValue("iMarch_bpOrg", 0, AD_Client_ID, AD_Org_ID));
			
			rs = pstm.executeQuery();
			table.loadTable(rs);
		} finally {
			DB.close(rs, pstm);
		}
	}

	/**
	 * Fill Service Request Table
	 * 
	 * @param table
	 * @throws Exception
	 */
	protected void serviceRequestTableLoad(IMiniTable table, int C_BPartner_ID) throws Exception {
		StringBuilder sql = new StringBuilder("SELECT R_Request_ID, hdr.DocumentNo, hdr.Created, bp.Name, hdr.SMJ_Plate, i.DocumentNo AS Invoice, ")
				.append("SMJ_VehicleCharge  FROM R_Request hdr ")
				.append("JOIN C_BPartner bp ON (bp.C_BPartner_ID = hdr.C_BPartnerOwner_ID) ")
				.append("LEFT JOIN C_Invoice i ON (i.C_Invoice_ID = hdr.C_Invoice_ID)")
				.append("WHERE R_RequestType_ID = ? AND SMJ_InvoiceProvider_ID IS NULL AND SMJ_IsInvoiceCharge = 'Y' AND R_Status_ID = ? ")
				.append(" AND hdr.Created > '").append(starDate).append("' ")
				.append("AND C_BPartnerOwner_ID = ? ").append("ORDER BY hdr.DocumentNo, bp.Name");

		PreparedStatement pstm = null;
		ResultSet rs = null;
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());

		try {
			pstm = DB.prepareStatement(sql.toString(), null);
			pstm.setInt(1, MSysConfig.getIntValue("SMJ_SSRequestType", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(2, MSysConfig.getIntValue("SMJ_SSRequestStatus", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(3, C_BPartner_ID);

			rs = pstm.executeQuery();
			table.loadTable(rs);

		} finally {
			DB.close(rs, pstm);
		}
	}
	
	
	public Vector<Vector<Object>> getserviceRequest(boolean SelectTodo, IMiniTable table, int C_BPartner_ID) throws Exception
	{
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		StringBuilder sql = new StringBuilder("SELECT R_Request_ID, hdr.DocumentNo, hdr.Created, bp.Name, hdr.SMJ_Plate, i.DocumentNo AS Invoice, ")
				.append("SMJ_VehicleCharge  FROM R_Request hdr ")
				.append("JOIN C_BPartner bp ON (bp.C_BPartner_ID = hdr.C_BPartnerOwner_ID) ")
				.append("LEFT JOIN C_Invoice i ON (i.C_Invoice_ID = hdr.C_Invoice_ID)")
				.append("WHERE R_RequestType_ID = ? AND SMJ_InvoiceProvider_ID IS NULL AND SMJ_IsInvoiceCharge = 'Y' AND R_Status_ID = ? ")
				.append(" AND hdr.Created > '").append(starDate).append("' ")
				.append("AND C_BPartnerOwner_ID = ? ").append("ORDER BY hdr.DocumentNo, bp.Name");

		PreparedStatement pstm = null;
		ResultSet rs = null;
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());

		try {
			pstm = DB.prepareStatement(sql.toString(), null);
			pstm.setInt(1, MSysConfig.getIntValue("SMJ_SSRequestType", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(2, MSysConfig.getIntValue("SMJ_SSRequestStatus", 0, AD_Client_ID, AD_Org_ID));
			pstm.setInt(3, C_BPartner_ID);

			rs = pstm.executeQuery();
			while (rs.next()) {
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));
				KeyNamePair pp = new KeyNamePair(rs.getInt(1), rs.getString(2));
				line.add(pp);	//R_Request_ID , DocumentNo
				line.add(rs.getString(3));	//Created
				line.add(rs.getString(4));	//Name 
				line.add(rs.getString(5));	//SMJ_Plate
				line.add(rs.getString(6));	//Invoice
				line.add(rs.getBigDecimal(7));	// SMJ_VehicleCharge
				data.add(line);
			}
		} finally {
			DB.close(rs, pstm);
		}
		
		return data;
	}

	/**
	 * 
	 * @param C_BPartner_ID
	 * @param requestIds
	 * @throws Exception
	 */
	protected void generateInvoice(int C_BPartner_ID, int[] requestIds, String documentNo) throws Exception {
		Trx trx = Trx.get(Trx.createTrxName(), false);
		Timestamp currentDate = Env.getContextAsDate(Env.getCtx(), "#Date");
		
		if (!DataQueriesTrans.isPeriodOpenAPI(new Date(currentDate.getTime()), Env.getAD_Client_ID(Env.getCtx()),
				Env.getAD_Org_ID(Env.getCtx()))) {
			throw new AdempiereException("@SMJMSGClosedPeriod@");
		}
		
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		int C_PaymentTerm_ID = MSysConfig.getIntValue("SMJ-PAYMENTTERMDEFAULT", 0, AD_Client_ID);
		int C_DocType_ID = MSysConfig.getIntValue("SMJ-DOCUMENTAPINVOICE", 0, AD_Client_ID, AD_Org_ID);
		int M_PriceList_ID = MSysConfig.getIntValue("SMJ_INFPURCHASELIST", 0, AD_Client_ID, AD_Org_ID);
		
		int salesRepId = Env.getAD_User_ID(Env.getCtx());

		MBPartner bpartner = new MBPartner(Env.getCtx(), C_BPartner_ID, null);

		if (bpartner.getC_PaymentTerm_ID() > 0) {
			C_PaymentTerm_ID = bpartner.getC_PaymentTerm_ID();
		}
		
		if (documentNo == null || documentNo.isEmpty()) {
			C_DocType_ID = MSysConfig.getIntValue("SMJ-DOCUMENTAPINVOICE-IT3", 0, AD_Client_ID, AD_Org_ID);
		}

		String workOrders = "";
		
		try {
			MInvoice inv = DocumentsTrans.createInvoiceNoIo(trx.getTrxName(), "", C_BPartner_ID, currentDate, M_PriceList_ID, C_DocType_ID, C_PaymentTerm_ID, null, 0,
					null, false, salesRepId, documentNo, false);
			
			if (inv == null) {
				trx.rollback();
				throw new AdempiereException(Msg.parseTranslation(Env.getCtx(), "@SMJMSGErrorCreateInvoice@"));
			}
			
			for (int R_Request_ID : requestIds) {
				MRequest request = new MRequest(Env.getCtx(), R_Request_ID, trx.getTrxName());
				
				BigDecimal price = new BigDecimal(request.get_ValueAsString("SMJ_VehicleCharge"));
				Integer taxId = DataQueriesTrans.getProductTax(request.getM_Product_ID());
				String customerPlate = request.get_ValueAsString("SMJ_Plate");
				
				Integer iLineId = DocumentsTrans.createInvoiceLine(inv, "", request.getM_Product_ID(), price, taxId, price,
						Env.ONE, 0, 0, customerPlate, null, 0, 0);
				
				if (iLineId <= 0) {
					trx.rollback();
					throw new AdempiereException("@SMJMSGErrorCreateInvoiceLine@");
				}
				

				request.set_ValueNoCheck("SMJ_InvoiceProvider_ID", inv.getC_Invoice_ID());
				request.saveEx();
				
				if (!workOrders.isEmpty())
					workOrders += ";";
				
				workOrders += request.getDocumentNo();
			}
				
			inv.set_ValueOfColumn("SMJ_WorkOrder", workOrders);
			inv.saveEx();
			
			trx.commit(true);
			
			ShowWindow.openInvoiceVendor(inv.getC_Invoice_ID());
		} finally {
			trx.close();
		}		
	}
	
	protected boolean requiredDocumentNo(int C_BPartner_ID) {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		
		MBPartner bpartner = new MBPartner(Env.getCtx(), C_BPartner_ID, null);
		
		int idTaxTypeBP = bpartner.get_ValueAsInt("LCO_TaxPayerType_ID");
		String taxTypesRequiredToInvoice = MSysConfig.getValue("SMJ-TAXPAYERTYPES-INVOICEREQUIRED", AD_Client_ID, AD_Org_ID).trim();
		
		return UtilTrans.validateRol(idTaxTypeBP, taxTypesRequiredToInvoice);
	}
}
