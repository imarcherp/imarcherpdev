package com.smj.webui.apps.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @version <li>SmartJSP: FuelSales, 2013/02/19
 *          <ul TYPE ="circle">
 *          <li>logica de la forma de venta de combustible
 *          <li>logic to form fuel sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class FuelSales {
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	public Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
//		columnNames.add(Msg.translate(Env.getCtx(), "entrydate"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_FuelIsland_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_FuelLine_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "hundredEnd"));
		columnNames.add(Msg.translate(Env.getCtx(), "qtySold"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Product_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "galonPrice"));
		columnNames.add(Msg.translate(Env.getCtx(), "totalPrice"));
		return columnNames;
	}//getTableColumnNames
	
	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	public void setTableColumnClass(IMiniTable table)	{
		int i = 0;
//		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	/**
	 * regresa la lista almacenes 
	 * returns warehouse list
	 * @param clientId
	 * @return Vector<Vector<Object>> 
	 */
	protected Vector<Vector<Object>> getWarehouseList(Integer clientId){
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT M_Warehouse_ID, name FROM  M_Warehouse WHERE isactive='Y' AND AD_Client_ID = "+clientId);
			sql.append(" AND smj_isFuelTank ='Y' ORDER BY name ASC ");
//		System.out.println("getWarehouseList SQL::"+sql.toString());
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("M_Warehouse_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getWarehouseList
	
	/**
	 * regresa la lista de islas de combustible 
	 * returns fuel island list
	 * @param clientId
	 * @return Vector<Vector<Object>> 
	 */
	protected Vector<Vector<Object>> getIslandList(Integer clientId, Integer warehouseId){
		StringBuffer sql = new StringBuffer();
		
			sql.append(" SELECT smj_FuelIsland_ID, name FROM  smj_FuelIsland WHERE isactive='Y' AND AD_Client_ID = "+clientId);
			sql.append(" AND smj_FuelIsland_ID in (SELECT smj_FuelIsland_ID FROM adempiere.smj_TankIsland "); 
			sql.append(" WHERE M_Warehouse_ID="+warehouseId+") ORDER BY name ASC ");
			
//		System.out.println("getIslandList SQL::"+sql.toString());
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("smj_FuelIsland_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getIslandList
	
	/**
	 * regresa la lista de mangeras de combustible por isla 
	 * returns fuel line list by fuel island
	 * @param clientId
	 * @param fuelIslandID
	 * @return Vector<Vector<Object>> 
	 */
	protected Vector<Vector<Object>> getFuelLineList(Integer clientId, Integer fuelIslandID){
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT smj_FuelLine_ID, name FROM  smj_FuelLine WHERE isactive='Y' AND AD_Client_ID = "+clientId);
			sql.append(" AND smj_FuelIsland_ID = "+fuelIslandID+" ORDER BY name ASC ");
//		System.out.println("getFuelLineList SQL::"+sql.toString());
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("smj_FuelLine_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getfuelLineList
	
	/**
	 * regresa la lista de mangeras de combustible por isla 
	 * returns fuel line list by fuel island
	 * @param clientId
	 * @param fuelIslandID
	 * @return Vector<Vector<Object>> 
	 */
	protected Vector<Vector<Object>> getProductList(Integer clientId, Integer categoryId){
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT M_Product_ID, name FROM  M_Product WHERE isactive='Y' AND AD_Client_ID = "+clientId);
			sql.append(" AND M_Product_Category_ID = "+categoryId+" ORDER BY name ASC ");
//		System.out.println("getProductList SQL::"+sql.toString());
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("M_Product_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getfuelLineList
	
	/**
	 * iMarch
	 * True si la Fecha digitada es consecutiva a la ultima venta ingresada 
	 */
	public Boolean dateEnable(Date fch){
		Boolean ok = false;
		Date result = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COALESCE(MAX(entrydate), current_date) FROM adempiere.smj_FuelSales WHERE c_order_id IS NOT NULL");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try{
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getTimestamp(1);
			}
		}
		catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, st);
			rs = null; 
			st = null;
		}
		
		Calendar fchlastin = Calendar.getInstance();
		fchlastin.setTime(result);
		fchlastin.add(Calendar.DAY_OF_YEAR, 1);
		if(fch.compareTo(fchlastin.getTime())==0) ok = true;
		
		return ok;
	}
	
}//FuelSales
