package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.factory.ButtonFactory;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MBPartner;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;

import com.smj.util.CSSUtils;

/**
 * 
 * @author Dany Diaz - SmartJSP (http://www.smartjsp.com/)
 * Modificado por Ing.Andres Quevedo - iMarch (http://imarch.co)
 *
 */
public class WServiceRequestInvoice extends ServiceRequestInvoice 
implements IFormController, EventListener<Event>, WTableModelListener, ValueChangeListener {

	public static CLogger log = CLogger.getCLogger(WServiceRequestInvoice.class);
	private CustomForm form = new CustomForm();
	
	@Override
	public ADForm getForm() {
		return form;
	}

	public  WServiceRequestInvoice() {
		
		try {
			dynInit();
			zkInit();
			resetFields();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "init", ex);
		}
	}

	private Borderlayout container;
	private Panel northPanel;
	private Panel centerPanel;
	private Panel southPanel;

	private Label lblTitle;
	private Label lblBPartners;
	private Label lblServiceRequests;
	private Label lblTotalLines;
	private Label txtTotalLines;
	private Label lblTotalAmt;
	private Label txtTotalAmt;
	private Label lblDocumentNo;
	
	private Textbox txtDocumentNo;
	
	private WListbox tblBPartners;
	private WListbox tblServiceRequests;
	
	private Button btnOk;
	
	private Checkbox allRequestServices = new Checkbox();
	
	Boolean onCalculate = false;
	
	int qtySelect = 0;
	
	
	private void dynInit() throws Exception{
		
		tblBPartners = ListboxFactory.newDataTable();
		tblBPartners.prepareTable(getBPartnerLayout(), MBPartner.Table_Name, "1=1",false, MBPartner.Table_Name);
		tblBPartners.setMultiSelection(false);
		ZKUpdateUtil.setHflex(tblBPartners, "1");
		ZKUpdateUtil.setVflex(tblBPartners, "1");
		tblBPartners.setStyle("height: 100%; width: 100%;");
		bPartnerTableLoad(tblBPartners);
		fixWidthColumnsBP(tblBPartners);
		tblBPartners.addEventListener(Events.ON_CLICK, this);

		tblServiceRequests = ListboxFactory.newDataTable();

		loadTable(tblServiceRequests, 0);
		ZKUpdateUtil.setHflex(tblServiceRequests, "1");
		ZKUpdateUtil.setVflex(tblServiceRequests, "1");
		ZKUpdateUtil.setWidth(tblServiceRequests, "100%");

		fixWidthColumnsSR(tblServiceRequests);
		//tblServiceRequests.addEventListener(Events.ON_SELECT, this);
		tblServiceRequests.addEventListener(Events.ON_CLICK, this);
		
		btnOk = ButtonFactory.createNamedButton(ConfirmPanel.A_OK);
		btnOk.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event evt) throws Exception {
				int C_BPartner_ID = ((IDColumn) tblBPartners.getValueAt(tblBPartners.getSelectedIndex(), 0)).getRecord_ID(); 
				int[] requestIds = new int[qtySelect];
				requestIds = getSelectedIndices();

				generateInvoice(C_BPartner_ID, requestIds, txtDocumentNo.getValue());
				
				resetFields();
				bPartnerTableLoad(tblBPartners);
				loadTable(tblServiceRequests, 0);
			}
		});
		
		
		allRequestServices.addActionListener(this);
		allRequestServices.setVisible(false);
		
	}

	private void zkInit() {
		
		
		form.setStyle("");
		ZKUpdateUtil.setWidth(form, "100%");
		ZKUpdateUtil.setHeight(form, "100%");
		ZKUpdateUtil.setHflex(form, "1");
		ZKUpdateUtil.setVflex(form, "1");
		
		// Layouts

		container = new Borderlayout();
		container.setParent(form);
		container.setStyle(container.getStyle());
		
		northPanel = new Panel();
		ZKUpdateUtil.setWidth(northPanel, "100%");
		ZKUpdateUtil.setHeight(northPanel, "100%");
		ZKUpdateUtil.setHflex(northPanel, "1");
		ZKUpdateUtil.setVflex(northPanel, "1");
		northPanel.setStyle(CSSUtils.textCenter("margin-top: 10px; margin-bottom: 25px;"));

		centerPanel = new Panel();
		ZKUpdateUtil.setWidth(centerPanel, "100%");
		ZKUpdateUtil.setHeight(centerPanel, "100%");
		ZKUpdateUtil.setHflex(centerPanel, "1");
		ZKUpdateUtil.setVflex(centerPanel, "1");
		
		southPanel = new Panel();
		ZKUpdateUtil.setWidth(southPanel, "100%");
		ZKUpdateUtil.setHeight(southPanel, "100%");
		ZKUpdateUtil.setHflex(southPanel, "1");
		ZKUpdateUtil.setVflex(southPanel, "1");
		southPanel.setStyle(CSSUtils.textRight(""));
		
		container.appendNorth(northPanel);
		container.appendCenter(centerPanel);
		container.appendSouth(southPanel);
		
		// Center
		
		Borderlayout centerLayout = new Borderlayout();
		ZKUpdateUtil.setWidth(centerLayout, "100%");
		ZKUpdateUtil.setHeight(centerLayout, "100%");
		ZKUpdateUtil.setHflex(centerLayout, "1");
		ZKUpdateUtil.setVflex(centerLayout, "1");
		centerLayout.setParent(centerPanel);
		
		North north = new North();
		ZKUpdateUtil.setHeight(north, "40%");
		north.setParent(centerLayout);
		
		South south = new South();
		ZKUpdateUtil.setHeight(south, "60%");
		south.setParent(centerLayout);
		south.setStyle("padding-bottom: 10px; padding-top: 15px;");
		
		// Components
		
		
		lblTitle = new Label(Msg.translate(Env.getCtx(), "SMJ_LblServiceRequestsInvoice"));
		lblTitle.setStyle(CSSUtils.textH1(""));
		lblTitle.setParent(northPanel);
		
		lblBPartners = new Label(Msg.translate(Env.getCtx(), "smenu.bpartners"));
		lblBPartners.setStyle(CSSUtils.textH2(""));
		
		Borderlayout bl = new Borderlayout();
		bl.setStyle("height: 100%; width: 100%");
		north.appendChild(bl);
		
		Hbox boxtitleBP = new Hbox();
		boxtitleBP = new Hbox();
	 	ZKUpdateUtil.setWidth(boxtitleBP, "100%");
	 	boxtitleBP.setPack("center");
	 	boxtitleBP.appendChild(lblBPartners);
	 	
		bl.appendNorth(boxtitleBP);
		
		Panel p = new Panel();
		p.appendChild(tblBPartners);
		p.setStyle("width: 100%; height: 100%; padding: 0; margin: 0;");
		bl.appendCenter(p);
		
		lblServiceRequests = new Label(Msg.translate(Env.getCtx(), "SMJ_LblServiceRequests"));
		lblServiceRequests.setStyle(CSSUtils.textH2(""));
		
		lblTotalLines = new Label(Msg.parseTranslation(Env.getCtx(), "@NoOfLines@:"));
		lblTotalAmt = new Label(Msg.parseTranslation(Env.getCtx(), "@GrandTotal@:"));
		txtTotalLines = new Label();
		txtTotalAmt = new Label();
		
		bl = new Borderlayout();
		//bl.setStyle("height: 200px; width: 100%");
		ZKUpdateUtil.setHeight(bl, "100%");
		ZKUpdateUtil.setWidth(bl, "100%");
		
		south.appendChild(bl);
	
		Div div = new Div();
		div.setStyle("border: none; width: 100%; height: 100%; float:left;");
		Hbox boxtitle = new Hbox();
	 	boxtitle = new Hbox();
	 	ZKUpdateUtil.setWidth(boxtitle, "70%");
	 	boxtitle.setPack("center");
	 	boxtitle.appendChild(lblServiceRequests);
	 	Div subdiv1 = new Div();
	 	subdiv1.setStyle("border: none;  width: 20%; height: 100%; "
	 			+ "float: left; display:table-cell; vertical-align:middle; "
	 			+ "padding: 10px 0 0 25px; ");
	 	allRequestServices.setText("Seleccionar Todo");
	 	allRequestServices.setStyle("color: #636376; font-weight: bold; text-decoration-line: underline; ");
	 	subdiv1.appendChild(allRequestServices);
	 	Div subdiv2 = new Div();
	 	subdiv2.setStyle("border: none ;  width: 80%; height: 100%; float: right;");
	 	subdiv2.appendChild(boxtitle);
		div.appendChild(subdiv1);
		div.appendChild(subdiv2);
		bl.appendNorth(div);
		
		p = new Panel();
		p.appendChild(tblServiceRequests);
		p.setStyle("width: 100%; height: 100%; padding: 0; margin: 0;");
		bl.appendCenter(p);
		
		Grid grid = GridFactory.newGridLayout();
		Rows rows = grid.newRows();
		Row row = rows.newRow();
		
		row.appendChild(lblTotalLines.rightAlign());
		row.appendChild(txtTotalLines);
		row.appendChild(lblTotalAmt.rightAlign());
		row.appendChild(txtTotalAmt);
		row.appendChild(new Space());

		
		lblDocumentNo = new Label(Msg.parseTranslation(Env.getCtx(), "@DocumentNo@ (@C_Invoice_ID@): "));
		txtDocumentNo = new Textbox();
		lblDocumentNo.setStyle("color: blue");
		lblDocumentNo.setVisible(false);
		txtDocumentNo.setVisible(false);
		
		//row = rows.newRow();
		row.appendCellChild(lblDocumentNo.rightAlign());
		row.appendCellChild(txtDocumentNo);
		row.appendChild(new Space());
		
		bl.appendSouth(grid);
		
		//South
		btnOk.setParent(southPanel);
				
	}
	
	/**
	 * Fix width of columns
	 * @param table
	 */
	private void fixWidthColumnsBP(WListbox table) {
		Boolean firstcol= true;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(firstcol) {
				//header.setHflex("min");
				ZKUpdateUtil.setHflex(header, "min");
				firstcol=false;
			}
			else //header.setHflex("max");
				ZKUpdateUtil.setHflex(header, "max");
		}
	}
	private void fixWidthColumnsSR(WListbox table) {
		int col= 0;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(col==0) {
				ZKUpdateUtil.setWidth(header, "1px");
				ZKUpdateUtil.setHflex(header, "min");
			}
			if(col<=2 || col==4) {
				ZKUpdateUtil.setHflex(header, "min");
			}
			else 
				ZKUpdateUtil.setHflex(header, "max");
			col++;
		}
	}
	
	/**
	 * Reset Fields (Total Labels & Button)
	 */
	private void resetFields() {
		txtDocumentNo.setValue("");
		NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
		txtTotalAmt.setValue(defaultFormat.format(0));
		txtTotalLines.setValue("0");
		btnOk.setEnabled(false);
	}
	
	private void loadTable(WListbox table, int C_BPartner_ID) throws Exception  {
		
		Vector<Vector<Object>> data;
		data = getserviceRequest(true, table, C_BPartner_ID);
		Vector<String> columnNames = getServiceRequestColumnNames();
		table.clear();
		table.getModel().removeTableModelListener(this);
		ListModelTable modelP = new ListModelTable(data);
		modelP.addTableModelListener(this);
		
		tblServiceRequests.setData(modelP, columnNames);
		setServiceRequestColumnClass(tblServiceRequests);
		fixWidthColumnsSR(tblServiceRequests);
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
	}

	@Override
	public void tableChanged(WTableModelEvent event) {

		boolean isUpdate = (event.getType() == WTableModelEvent.CONTENTS_CHANGED);
		//  Not a table update
		if (!isUpdate || onCalculate)	return;
		
		int row = event.getFirstRow();
		allRequestServices.setChecked(false);
		tblServiceRequests.setSelectedIndex(row);
		Calculate(0);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
		Object acction = event.getTarget();
		if( Events.ON_CLICK.equals(event.getName()) && acction instanceof WListbox)
		{
			if (acction == tblServiceRequests){
				allRequestServices.setChecked(false);
				int rowselect = tblServiceRequests.getSelectedRow();
				if(((Boolean)tblServiceRequests.getValueAt(rowselect, 0)).booleanValue()) {
					tblServiceRequests.setValueAt(false, rowselect, 0);
					qtySelect--;
				}
				else {
					tblServiceRequests.setValueAt(true, rowselect, 0);
					qtySelect++;
				}
				//Calculate(0);
				
			}
	
			if(acction == tblBPartners) {
				allRequestServices.setVisible(true);
				allRequestServices.setChecked(false);
				qtySelect = 0;
				if (tblBPartners.getSelectedItem() == null)
					return;
				
				int C_BPartner_ID = ((IDColumn) tblBPartners.getValueAt(tblBPartners.getSelectedIndex(), 0)).getRecord_ID();
				//serviceRequestTableLoad(tblServiceRequests, C_BPartner_ID);
				loadTable(tblServiceRequests, C_BPartner_ID);
				
				refreshtblBP();
				
				boolean requiredDocumentNo = requiredDocumentNo(C_BPartner_ID);
				txtDocumentNo.setVisible(requiredDocumentNo);
				lblDocumentNo.setVisible(requiredDocumentNo);
				resetFields();
			}
		}
		else if(acction.equals(allRequestServices))
		{
			if(allRequestServices.isSelected())
				Calculate(1);
			else
				Calculate(2);
		}
		
	}
	
	private void Calculate(int SelectAll) {
		
		if(onCalculate) return;
		onCalculate=true;
		BigDecimal totalAmt = Env.ZERO;
		int rows = tblServiceRequests.getRowCount();
		int totalLines = 0;
		qtySelect=0;
		//if(SelectAll==1) qtySelect=rows;
		//if(SelectAll==2) qtySelect=0;
		//else qtySelect=0;
		for(int i=0; i < rows; i++) {
			if(SelectAll==1) tblServiceRequests.setValueAt(true, i, 0);
			if(SelectAll==2) tblServiceRequests.setValueAt(false, i, 0);
			if(((Boolean)tblServiceRequests.getValueAt(i, 0)).booleanValue()) {
				totalAmt = totalAmt.add((BigDecimal) tblServiceRequests.getValueAt(i, 6));
				totalLines++;
				qtySelect++;
			}
		}
		if(qtySelect==rows) allRequestServices.setChecked(true);
		NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
		txtTotalAmt.setValue(defaultFormat.format(totalAmt));
		txtTotalLines.setValue(Integer.toString(totalLines));
		
		btnOk.setEnabled(true);
		onCalculate = false;
		
	}
	
	private int[] getSelectedIndices() {
		
		int[] indices = new int[qtySelect];
		int r = 0;
		int rows = tblServiceRequests.getRowCount();
		
		for(int i=0; i < rows; i++) {
			if(((Boolean)tblServiceRequests.getValueAt(i, 0)).booleanValue()) {
				KeyNamePair RequestID = (KeyNamePair)tblServiceRequests.getValueAt(i, 1);
				indices[r++]= RequestID.getKey();
			}
		}
		
		return indices;
	}
	
	private void refreshtblBP() {
		
		int rows = tblServiceRequests.getRowCount();
		BigDecimal total = Env.ZERO;
		for(int x=0;  x<rows ; x++) {
			total = total.add((BigDecimal) tblServiceRequests.getValueAt(x, 6));
		}
		
		tblBPartners.setValueAt(rows, tblBPartners.getSelectedIndex(), 2);
		tblBPartners.setValueAt(total, tblBPartners.getSelectedIndex(), 3);
		tblBPartners.repaint();
		
	}
	
}
