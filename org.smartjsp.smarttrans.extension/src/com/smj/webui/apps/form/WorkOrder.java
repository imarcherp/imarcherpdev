package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013-04-24
 *          <ul TYPE ="circle">
 *          <li>Clase para la logica de la forma de venta simplificada
 *          <li>Class to logic for form simplified sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class WorkOrder {
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	protected Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "value"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProductService"));
		columnNames.add(Msg.translate(Env.getCtx(), "price"));
		columnNames.add(Msg.translate(Env.getCtx(), "qty"));	
		columnNames.add(Msg.translate(Env.getCtx(), "totalPrice"));
		columnNames.add(Msg.translate(Env.getCtx(), "summary"));
		columnNames.add(Msg.translate(Env.getCtx(), "R_Status_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_estimatedTime"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelMechanic"));
		return columnNames;
	}//getTableColumnNames
	
	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	protected void setTableColumnClass(IMiniTable table, Boolean isEditable)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);//0
		table.setColumnClass(i++, String.class, true); 
		table.setColumnClass(i++, String.class, true);//2
		table.setColumnClass(i++, BigDecimal.class, !isEditable);
		table.setColumnClass(i++, BigDecimal.class, !isEditable); //4   	
		table.setColumnClass(i++, BigDecimal.class, true);   
		table.setColumnClass(i++, String.class, true); //6
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true); //8
		table.setColumnClass(i++, String.class, !isEditable);
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	/**
	 * regresa los datos de vehiculo, placa y prietario - 
	 * returns vehicle Id , plate and owner
	 * @param plate
	 * @return
	 */
	protected HashMap<String, Object> getVehicledata(String plate) {
		if(plate == null){
			return null;
		}
		HashMap<String, Object> value = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT v.smj_vehicle_ID, v.smj_plate, v.C_PaymentTerm_ID, s.name, qtyTires ");
		sql.append(" FROM smj_vehicle v LEFT JOIN smj_servicerategroup s  ");
		sql.append(" ON (v.smj_servicerategroup_ID = s.smj_servicerategroup_ID  ) WHERE v.isactive = 'Y' ");
		sql.append(" AND (TRIM(UPPER(smj_plate)) = '" + plate.trim().toUpperCase() + "' ");
		sql.append(" OR TRIM(UPPER(smj_internalNumber)) = '" + plate.trim().toUpperCase() + "') ");
//		 System.out.println(this.getClass().getCanonicalName()+".getVehicledata SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()){
				value = new HashMap<String, Object>();
				value.put("VEHICLE", rs.getInt("smj_vehicle_ID"));
				String data = rs.getString("smj_plate");
				value.put("PLATE", data);
				Integer owner = DataQueriesTrans.getVehicleTender(data, "1");
				if (owner <= 0){
					owner = DataQueriesTrans.getVehicleOwnerId(data);
				}
				value.put("OWNER", owner);
				value.put("PAY", rs.getInt("C_PaymentTerm_ID"));
				value.put("TEMPA", rs.getString("name"));
				value.put("TIRES", rs.getInt("qtyTires"));
			}
			
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getVehicledata - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getVehicledata
	
	/**
	 * regresa el listado de solicitudes abiertas por placa - 
	 * return open request by plate
	 * @param plate
	 * @param states
	 * @return LinkedList<Integer>
	 */
	protected String getWOOpen(String plate, String states) {
		if(plate == null){
			return null;
		}
//		LinkedList<Integer> value = new LinkedList<Integer>();
		String value = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT r.R_Request_ID, r.documentNo FROM R_Request r ");
		sql.append(" WHERE r.isactive='Y' AND R_Status_ID NOT IN ("+states+") ");
		sql.append(" AND r.smj_plate = '" + plate.trim().toUpperCase() + "' ");
//		System.out.println(this.getClass().getName()+".getWOOpen SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				String code = rs.getString("documentNo");
				value = value+code+",";
			}//while
			if (value.endsWith(",")){
				value = value.substring(0, value.length()-1);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getWOOpen - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getWOOpen
	
	/**
	 * conteo de mecanicos asignados en la actividad - 
	 * total mechanic by activity
	 * @param woLineId
	 * @return
	 */
	protected Integer getCountMechanic(Integer woLineId) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT count(1) AS total FROM SMJ_WoMechanicAssigned ");
		sql.append(" WHERE smj_workOrderLine_id = "+woLineId);
		
//		System.out.println(this.getClass().getName()+".getCountMechanic SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()){
				value = rs.getInt("total");
			}//if
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getCountMechanic - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getCountMechanic
	
}//WorkOrder
