package com.smj.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;
import com.smj.util.UtilTrans;

public class InventoryCsvDAO {
private final static String fileSeparator = ";";
	
/** Logger */
public static CLogger log = CLogger.getCLogger(Allocation.class);
private Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
private Integer principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	/**
	 * carga archivo - load file
	 * @param csvFile
	 * @param separator
	 * @param processedDirectory
	 * @return
	 * @throws Exception
	 */
	public String load(File csvFile, String separator, String processedDirectory) throws Exception{
		System.out.println("file...."+csvFile.getName());
		BufferedReader bufRdr;
		BufferedWriter writer;
		String code = "";
		String msg = "";
		String msgPrice = "";
		Integer warehouseId = 0;
		Integer locatorId = 0;
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		MInventory inv = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			bufRdr = new BufferedReader(new FileReader(csvFile));
			writer = new BufferedWriter(new FileWriter(processedDirectory + fileSeparator + csvFile.getName() + ".bad"));
			String line = null;
			int i = 0;
//			int c = 0;
			// read each line of text file
			
			while ((line = bufRdr.readLine()) != null) {
				try {
					if (inv == null){
//						System.out.println(this.getClass().getName()+".load create Document ....");
						String fields[] = line.split(separator, 15);
						String desc = fields[2];
						String sDate = fields[3];
						String swh  = fields[8];
						String sloc = fields[10];
//						System.out.println(this.getClass().getName()+".load swh: "+swh+"..sDate.."+sDate+"...sloc ..."+sloc);
						warehouseId = DataQueriesTrans.getWarehouseIdbyCode(swh, Env.getAD_Client_ID(Env.getCtx()));
						locatorId = DataQueriesTrans.getLocatorIdbyCode(warehouseId,sloc);
//						System.out.println(this.getClass().getName()+".load wh ID"+warehouseId+"..loc..."+locatorId);
						
						Date dDate = sdf.parse(sDate); 
//						System.out.println(this.getClass().getName()+".load date...."+dDate);
//						if (!DataQueriesTrans.isPeriodOpen(dDate, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()))){
//							trx.rollback();
//							trx.close();
//							bufRdr.close();
//							writer.close();
//							return Msg.translate(Env.getCtx(), "SMJMSGClosedPeriod");
//						}
						MWarehouse wh = MWarehouse.get(Env.getCtx(), warehouseId);
						inv = new MInventory(wh, trx.getTrxName());
						inv.setAD_Org_ID(1000000);
						inv.setDescription(desc);
						inv.setMovementDate(new Timestamp(dDate.getTime()));
						inv.setC_DocType_ID(1000023);
						inv.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
						inv.setDocAction(DocAction.ACTION_Complete);
						Boolean iok = inv.save();
						if (!iok){
//							System.out.println("\n\n ERROR:: "+"error al crear el documento de inventario");
							log.log(Level.SEVERE, this.getClass().getName()+ " - ERROR: " +"error al crear el documento de inventario");
							return "error al crear el documento de inventario";
						}
					}
//					c=0;
					i++;
					line = line.replace("\"", ""); // it removes the "
					line = line.replace("\'", ""); // it removes the '
					String fields[] = line.split(separator, 15);
					code = fields[6];
//					System.out.println(code+"*********************************************************fields..."+i+"..");
					Integer productId = DataQueriesTrans.getProductByValue(Env.getAD_Client_ID(Env.getCtx()), code);
//					System.out.println(this.getClass().getName()+".load - code: "+code+"..ProdId.."+productId);
					if (productId >0){
						MInventoryLine l = new MInventoryLine(Env.getCtx(), 0, trx.getTrxName());
						l.setM_Inventory_ID(inv.getM_Inventory_ID());
						l.setAD_Org_ID(inv.getAD_Org_ID());
						l.setM_Locator_ID(locatorId);
						l.setM_Product_ID(productId);
						String count = fields[11];
						l.setQtyCount(new BigDecimal(count));
						l.setInventoryType("D");	
						
						Boolean ok = l.save();
						if (!ok){
							log.log(Level.SEVERE, this.getClass().getName()+ " - ERROR: inventario producto: "+code);
						}else{
							String sPrice = fields[12];
//							System.out.println(this.getClass().getName()+".load --price...."+sPrice);
							Boolean uok = updatePrice(trx.getTrxName(), productId, new BigDecimal(sPrice), warehouseId, locatorId);
							if(!uok){
								msgPrice = msgPrice+code+"; ";
//								System.out.println(this.getClass().getName()+".load no se pudo actalizar el precio para: "+code+" Price: "+sPrice);
								log.log(Level.SEVERE, this.getClass().getName()
										+ " - ERROR: no se pudo actalizar el precio para: "+code+" Price: "+sPrice);
							}
						}
					}else{
						msg = msg +code+"; ";
//						System.out.println(this.getClass().getName()+".load - No se encontro el producto: "+code);
						log.log(Level.SEVERE, this.getClass().getName()
								+ " - ERROR: " + "No se encontro el producto: "+code);
					}
					
				} catch (Exception ex) {
//					System.out.println("ERROR::: "+ex);
//					flag = false;
					writer.write(line + "\n");
					StackTraceElement[] trace = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : trace) {
						System.out.println(" - 	:: "+stackTraceElement.toString());
					}
//					log.debug();
					System.out.println(ex.getStackTrace());
					log.log(Level.SEVERE,this.getClass().getName() + " - ERROR: " + ex.getMessage(),ex);
					trx.rollback();
					trx.close();
					return "ERROR: "+ex;
				}// try / catch dentro del while
			}// while
				// close the file
			bufRdr.close();
			writer.close();
			
//			System.out.println(this.getClass().getName()+".load ... despues de guardar ....");
		} catch (Exception e) {
//			System.out.println("--------------- CODE -----"+code);
//			System.out.println("\n\n ERROR:: "+e);
			log.log(Level.SEVERE,this.getClass().getName() + " - ERROR: " + e.getMessage(),e);
			trx.rollback();
			trx.close();
			return "ERROR: "+e;
		}// try/catch
		String msgRet = "";
		if(msg != null && msg.length() >0){
			msgRet = msgRet+" No se encontraron los Productos: "+msg;
		}
		if (msgPrice != null && msgPrice.length()>0){
			msgRet = msgRet+"\n ***** No se pudo actualiza el precio de los siguientes productos: "+msgPrice;
		}
		if (msgRet!=null && msgRet.length() > 0 ){
			trx.rollback();
		}else{
			try {
				msgRet = "Inventario Fisico No.: "+inv.getDocumentNo()+" \n ****** ";
				inv.completeIt();
				inv.setDocStatus(DocAction.STATUS_Completed);
				inv.setDocAction(DocAction.STATUS_Closed);
				inv.setProcessed(true);
				inv.save();
				trx.commit();
			} catch (Exception e) {
//				System.out.println("--------------- CODE -----"+code);
//				System.out.println("\n\n ERROR:: "+e);
				log.log(Level.SEVERE,this.getClass().getName() + " - ERROR: " + e.getMessage(),e);
				trx.rollback();
				trx.close();
				return "ERROR: "+e;
			}
		}
		
		trx.close();
		return msgRet;
	}//load
	
	/**
	 * Actualiza los precios del producto - 
	 * Update product prices
	 * @param trxName
	 * @param productId
	 * @param price
	 * @param warehouseId
	 * @param locatorId
	 * @return Boolean
	 */
	private Boolean updatePrice(String trxName, Integer productId, BigDecimal price, Integer warehouseId, Integer locatorId){
		MProduct p = MProduct.get(Env.getCtx(), productId);
		BigDecimal util = new BigDecimal(p.get_ValueAsString("smj_utility"));
		BigDecimal tax = DataQueriesTrans.getTaxValue(p.getC_TaxCategory_ID());
		BigDecimal salesPrice = UtilTrans.calculateSalesPrice(price, util, tax);
		Integer bparnerId = 0;
		Integer warehouseOwnerId = DataQueriesTrans.getwarehouseOwner(warehouseId);
		if (!locatorId.equals(principalWarehouse)){
			bparnerId = warehouseOwnerId;
		}
		HashMap<String, Integer> codes = DataQueriesTrans.getPListVersion(trxName, p, bparnerId, tiresCategory, locatorId, warehouseOwnerId, null);
		MProductPrice sp = null;
		MProductPrice pp = null;
		Boolean sok = true;
		Boolean pok = true;
		if(codes != null){
			sp = MProductPrice.get(Env.getCtx(), codes.get("SALES"), productId, trxName);
			if (sp == null){
				sp = new MProductPrice(Env.getCtx(), codes.get("SALES"), productId, trxName);
			}
				sp.setPrices(salesPrice, salesPrice, salesPrice);
				sok = sp.save();
				
			pp = MProductPrice.get(Env.getCtx(), codes.get("PURCHASE"), productId, trxName);
			if(pp==null){
				pp = new MProductPrice(Env.getCtx(), codes.get("PURCHASE"), productId, trxName);
			}
			pp.setPrices(price, price, price);
			pok = pp.save();
		}
		if (!sok || !pok){
			return false;
		}
		return true;
	}//updatePrice
	
}//InventoryCsvDAO
