package com.smj.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.smj.model.MSMJBodyType;
import com.smj.model.MSMJBrand;
import com.smj.model.MSMJProgrammingGroup;
import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: VehicleCsvDAO, 2013/02/28 <ul TYPE ="circle">
 *          <li>Carga archivos CSV y los inserta en la tabla vehiculo y sus relacionadas
 *          <li>Loads CSV files and insert rows table vehicle and relateed
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class VehicleCsvDAO {
	
	private final static String fileSeparator = ";";
	private HashMap<String, Integer> brand = new HashMap<String, Integer>();
	private HashMap<String, String> odometer = new HashMap<String, String>();
	private HashMap<String, String> company = new HashMap<String, String>();
	private HashMap<String, String> gRate = new HashMap<String, String>();
	private HashMap<String, String> serviceType = new HashMap<String, String>();
	private HashMap<String, String> typeAdm = new HashMap<String, String>();
	private HashMap<String, String> vhType = new HashMap<String, String>();
	
	/**
	 * carga archivo - load file
	 * @param csvFile
	 * @param separator
	 * @param processedDirectory
	 * @return
	 * @throws Exception
	 */
	public Boolean load(File csvFile, String separator, String processedDirectory) throws Exception{
		System.out.println("file...."+csvFile.getName());
		getBrand();
		getList();
		BufferedReader bufRdr;
		BufferedWriter writer;
		Boolean flag = true;
		String plate = "";
		try {
			
			bufRdr = new BufferedReader(new FileReader(csvFile));
			writer = new BufferedWriter(new FileWriter(processedDirectory + fileSeparator + csvFile.getName() + ".bad"));
			String line = null;
			int i = 0;
			int c = 0;
			// read each line of text file
			while ((line = bufRdr.readLine()) != null ) {
				try {
					c=0;
					i++;
					line = line.replace("\"", ""); // it removes the "
					line = line.replace("\'", ""); // it removes the '
					String fields[] = line.split(separator, 60);
					plate = fields[c++];
//					System.out.println(plate+"*********************************************************fields..."+i+"..");
					Integer vehicleId = DataQueriesTrans.getVehicleId(plate);
					MSMJVehicle v = new MSMJVehicle(Env.getCtx(), vehicleId, null);
					v.setsmj_plate(plate);
					String bodyBrand = fields[c++];
					Integer bodyBrandId = brand.get(bodyBrand);
					if ((bodyBrandId == null || bodyBrandId <= 0) && (bodyBrand != null && bodyBrand.length() > 0 && !bodyBrand.equals(""))){
						MSMJBrand br = new MSMJBrand(Env.getCtx(), 0, null);
						br.setName(bodyBrand);
						br.save();
						bodyBrandId = br.getsmj_brand_ID();
						brand.put(bodyBrand,bodyBrandId);
					}
					if (bodyBrandId!= null && bodyBrand.length() > 0)
						v.setsmj_bodyworkbrand_ID(bodyBrandId);
					String motorBrand = fields[c++];
					Integer motorBrandId = brand.get(motorBrand);
					if ((motorBrandId == null || motorBrandId <= 0) && (motorBrand != null && motorBrand.length() > 0 && !motorBrand.equals(""))){
						MSMJBrand br = new MSMJBrand(Env.getCtx(), 0, null);
						br.setName(motorBrand);
						br.save();
						motorBrandId = br.getsmj_brand_ID();
						brand.put(motorBrand,motorBrandId);
					}
					if(motorBrandId!=null)
						v.setsmj_motorbrand_ID(motorBrandId);
					String gearboxBrand = fields[c++];
					Integer gearboxBrandId = brand.get(gearboxBrand);
					if ((gearboxBrandId == null || gearboxBrandId <= 0) && (gearboxBrand != null && gearboxBrand.length() > 0 && !gearboxBrand.equals(""))){
						MSMJBrand br = new MSMJBrand(Env.getCtx(), 0, null);
						br.setName(gearboxBrand);
						br.save();
						gearboxBrandId = br.getsmj_brand_ID();
						brand.put(gearboxBrand,gearboxBrandId);
					}
					if(gearboxBrandId!=null)
						v.setsmj_gearboxbrand_ID(gearboxBrandId);
					String rateGroup = fields[c++];
					Integer rateGId = DataQueriesTrans.getServiceRateGroupId(rateGroup);
					if ((rateGId == null || rateGId <= 0) && (rateGroup != null && rateGroup.length() > 0)){
						MSMJServiceRateGroup sr = new MSMJServiceRateGroup(Env.getCtx(), 0, null);
						sr.setName(rateGroup); 
						sr.save();
						rateGId = sr.getsmj_servicerategroup_ID();
						//brand.put(rateGroup,gearboxBrandId);
					}
					v.setsmj_servicerategroup_ID(rateGId);
					v.setodometer(odometer.get(fields[c++]));
					String lm = odometer.get(fields[c++]);
					try{
						v.setlastmileage(Integer.parseInt(lm));
					}catch (Exception e) {
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					try{
						Date dlm = sdf.parse(fields[c++]);
						v.setdatelastmileage(new Timestamp(dlm.getTime()));
					}catch (Exception e) {
					}
					String oil = fields[c++];				
					String oilBrand = fields[c++];
					try {
						v.setoilproduct_ID(Integer.parseInt(oil));
					} catch (Exception e) {
					}
					String compan = company.get(fields[c++].trim());
//					v.setcompany(compan);
					v.settypeadministration(typeAdm.get(fields[c++].trim()));
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setdateaffiliation(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setdateexpirationaffiliation(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setdatedisaffiliation(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					v.setsmj_internalnumber(fields[c++].trim());
					v.settransitlicense(fields[c++].trim());
					try{
						Date d = sdf.parse(fields[c++]);
						v.setdatetransitlicense(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					String vehicleBrand = fields[c++];
					Integer vehicleBrandId = brand.get(vehicleBrand);
					if ((vehicleBrandId == null || vehicleBrandId <= 0) && (vehicleBrand != null && vehicleBrand.length() > 0 && !vehicleBrand.equals(""))){
						MSMJBrand br = new MSMJBrand(Env.getCtx(), 0, null);
						br.setName(vehicleBrand);
						br.save();
						vehicleBrandId = br.getsmj_brand_ID();
						brand.put(vehicleBrand,vehicleBrandId);
					}
					if(vehicleBrandId!=null)
						v.setsmj_vehiclebrand_ID(vehicleBrandId);
					try{
						v.setmodel(Integer.parseInt(fields[c++].trim()));
					}catch (Exception e) {
					}
					v.setrepoweredde(fields[c++].trim());
					v.setLine(fields[c++].trim());
					v.setchassisnumber(fields[c++].trim());
					v.setmotornumber(fields[c++].trim());
					try {
						v.setcylindercapacity(new BigDecimal(fields[c++].trim()));
					} catch (Exception e) {
					}
					v.setcolor(fields[c++]);
					String bodyType = fields[c++];
					Integer bodyTypeId = DataQueriesTrans.getBodyTypeId(bodyType);
					if ((bodyTypeId == null || bodyTypeId <= 0) && (bodyType != null && bodyType.length() > 0 && !bodyType.equals(""))){
						MSMJBodyType bt = new MSMJBodyType(Env.getCtx(), 0, null);
						bt.setName(bodyType);
						bt.save();
						bodyTypeId = bt.getsmj_bodytype_id();
					}
					if(bodyTypeId!=null)
						v.setsmj_bodytype_ID(bodyTypeId);
					String sType = fields[c++].trim();
					v.setservicetype(serviceType.get(sType));
					
					v.setvehicletype(vhType.get(fields[c++].trim()));
					v.setaxles(fields[c++].trim());
					try {
						v.setbodylength(new BigDecimal(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setcapacitytons(new BigDecimal(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setcapacitygallons(new BigDecimal(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setcapacitypassenger(Integer.parseInt(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setchairsnumber(Integer.parseInt(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setpassengersseated(Integer.parseInt(fields[c++].trim()));
					} catch (Exception e) {
					}
					try {
						v.setpassengersstanding(Integer.parseInt(fields[c++].trim()));
					} catch (Exception e) {
					}
					String trucTRactor = fields[c++].trim();
					if (trucTRactor!=null && trucTRactor.equals("SI")){
						v.setistrucktractor(true);
					}
					String trailer = fields[c++].trim();
					if (trailer!=null && trailer.equals("SI")){
						v.setistrailer(true);
					}
					try {
						Integer trailerId = DataQueriesTrans.getVehicleId(fields[c++].trim());
						if (trailerId != null && trailerId >0 ){
							v.setasociatedtrucktrailer_ID(trailerId);
						}
					} catch (Exception e) {
					}
					
					v.setsoatnumber(fields[c++].trim());
					try{
						Date d = sdf.parse(fields[c++]);
						v.setsoatexpiration(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					try {
						Integer soatInsurerId = DataQueriesTrans.getBpartnerId(1000000, fields[c++].trim());
						if (soatInsurerId != null && soatInsurerId > 0){
							v.setsoatinsurer_ID(soatInsurerId);
						}
					} catch (Exception e) {
					}
					v.setcardoperation(fields[c++].trim());
					try{
						Date d = sdf.parse(fields[c++]);
						v.setcardoperationexpiration(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setextinguisherexpiration(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					v.setliabilitypolicy(fields[c++].trim());
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setliabilitypolicyexpiration(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					v.setliabilitypolicyinsurer(fields[c++].trim());
					v.settechnicalrevisionnumber(fields[c++].trim());
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setdatetechnicalrevision(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.settechnicalrevisionnumberexpira(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					Integer diagnosticcenterId = DataQueriesTrans.getPartnerByName(1000000, fields[c++].trim());
					if (diagnosticcenterId != null && diagnosticcenterId > 0){
						v.setdiagnosticcenter_ID(diagnosticcenterId);
					}
					String rate = "";
					try {
						rate = fields[c++].trim();
						v.setgrouprate(gRate.get(rate));
					} catch (Exception e) {
						System.out.println(plate+" - ERROR RATE: "+rate);
					}
					String favoriteRoute = fields[c++].trim();
					String lastRoute = fields[c++].trim();
					String despacho = fields[c++].trim();
					try{
						Date d = sdf.parse(fields[c++].trim());
						v.setdatelastprogramming(new Timestamp(d.getTime()));
					}catch (Exception e) {
					}
					String programingGroup = fields[c++].trim();
					Integer pGroupId = DataQueriesTrans.getProgramingGroupId(programingGroup);
					if ((pGroupId == null || pGroupId <= 0) && (programingGroup != null && programingGroup.length() > 0)){
						MSMJProgrammingGroup pr = new MSMJProgrammingGroup(Env.getCtx(), 0, null);
						pr.setName(programingGroup);
						pr.save();
						pGroupId = pr.getsmj_programmingGroup_ID();
						//brand.put(rateGroup,gearboxBrandId);
					}
					System.out.println(c+"__Pgroup..."+programingGroup);
					v.setsmj_programmingGroup_ID(pGroupId);
					try {
						v.settourniquet(Integer.parseInt(fields[c++].trim()));
					} catch (Exception e) {
					}
					Boolean ok = v.save();
					if (!ok){
						flag = false;
					}
//					System.out.println(v.getsmj_vehicle_ID()+"--guardado...."+ok);
				} catch (Exception ex) {
					flag = false;
					writer.write(line + "\n");
					StackTraceElement[] trace = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : trace) {
						System.out.println("\n\n error:: "+stackTraceElement.toString());
					}
//					log.debug();
					System.out.println(ex.getStackTrace());

				}// try / catch dentro del while
			}// while
				// close the file
			bufRdr.close();
			writer.close();

		} catch (Exception e) {
			System.out.println("\n\n ERROR:: "+e.getStackTrace());
			System.out.println("---------------Placa-----"+plate);
		}// try/catch
		
		return flag;
	}//load
	
	/**
	 * carga las marcas en memoria - load brands in memory
	 */
	private void getBrand(){
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_brand_id, name FROM smj_brand WHERE isactive = 'Y' ");
		 System.out.println("getBrand SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name = "";
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				value = rs.getInt("smj_brand_id");
				name = rs.getString("name");
				brand.put(name, value);
			}
		} catch (Exception e) {
//			log.log(Level.SEVERE, sql.toString(), e);
			System.out.println("ERROR BRAND::"+e.getMessage());
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}//getBrand
	
	/**
	 * carga las listas en memoria - load lists in memory
	 */
	private void getList(){
		odometer.put("Sin establecer", "S");
		odometer.put("funcionando", "F");
		odometer.put("Averiado", "A");
		
		company.put("INFOTRANS S.A.", "I");
		company.put("UCOLBUS S.A.", "U");
		
		gRate.put("2001 Y POSTERIOR", "A");
		gRate.put("MODELO 1998,1999Y2000", "B");
		gRate.put("TARIFA MODELOS 2001", "C");

		
		serviceType.put("CARGA", "C");
		serviceType.put("ESPECIAL PASAJEROS", "E");
		serviceType.put("PASAJEROS MASIVO", "M");
		serviceType.put("PASAJEROS COLECTIVO", "P");
		
		typeAdm.put("CONTROL DOCUMENTOS", "C");
		typeAdm.put("RECAUDO DIARIO", "D");
		typeAdm.put("RECAUDO DIARIO Y PORGRAMACION MANTENIMIENTO", "R");
		typeAdm.put("PROGRAMACION VIAJES", "V");
		
		vhType.put("CAMION", "C");
		vhType.put("AUTOMOVIL", "A");
		vhType.put("DOBLETROQUE", "D");
		vhType.put("TURBO", "T");
		vhType.put("CAMIONETA", "M");
		vhType.put("BUS", "B");
		vhType.put("TRACTOCAMION", "X");
		vhType.put("MINIMULA", "L");
		vhType.put("CARGA OTRO", "O");
		vhType.put("REMOLQUE", "R");
		vhType.put("BUSETA", "S");
		vhType.put("MICROBUS", "U");
		
	}//getList
	
}//VehicleCsvDAO
