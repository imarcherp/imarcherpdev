package com.smj.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;
import com.smj.util.UtilTrans;

public class PriceCsvDAO {
private final static String fileSeparator = ";";
	
private Integer purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_PURCHASELIST",Env.getAD_Client_ID(Env.getCtx())).trim());
private Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",Env.getAD_Client_ID(Env.getCtx())).trim());

	/**
	 * carga archivo - load file
	 * @param csvFile
	 * @param separator
	 * @param processedDirectory
	 * @return
	 * @throws Exception
	 */
	public Boolean load(File csvFile, String separator, String processedDirectory) throws Exception{
		System.out.println("file...."+csvFile.getName());
		BufferedReader bufRdr;
		BufferedWriter writer;
		Boolean flag = true;
		String code = "";
		try {
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			bufRdr = new BufferedReader(new FileReader(csvFile));
			writer = new BufferedWriter(new FileWriter(processedDirectory + fileSeparator + csvFile.getName() + ".bad"));
			String line = null;
			int i = 0;
			int c = 0;
			// read each line of text file
			while ((line = bufRdr.readLine()) != null) {
				try {
					c=0;
					i++;
					line = line.replace("\"", ""); // it removes the "
					line = line.replace("\'", ""); // it removes the '
					String fields[] = line.split(separator, 16);
					code = fields[c++];
//					System.out.println(code+"*********************************************************fields..."+i+"..");
					Integer productId = getProductId(code);
					if(productId== null || productId<=0){
						System.out.println("por cero......."+code);
					}else{
						String name = fields[c++];
						String hour = fields[c++];
						String sprice = fields[c++];
//						System.out.println("sprtice....."+sprice);
						String cost = fields[c++];
						Boolean allow = false;
						if (cost.equals("VERDADERO")){
							allow = true;
							
						}
						BigDecimal price = Env.ZERO;
						if(sprice != null){
							price =  new BigDecimal(sprice.trim());
						}
						hour = hour.replace(",", ".");
//						Boolean pok = 
							createPriceList(trx.getTrxName(), price, productId, 
								name, new BigDecimal(hour), allow);
						
//						System.out.println("lista Creada...."+pok);
//						Boolean ok = p.save();
//						if (!ok){
//							flag = false;
//						}
//						System.out.println(v.getsmj_vehicle_ID()+"--guardado...."+ok);
					}
						
				} catch (Exception ex) {
					System.out.println("ERROR::: "+ex);
					flag = false;
					writer.write(line + "\n");
					StackTraceElement[] trace = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : trace) {
						System.out.println(" - error:: "+stackTraceElement.toString());
					}
//					log.debug();
					System.out.println(ex.getStackTrace());

				}// try / catch dentro del while
			}// while
				// close the file
			bufRdr.close();
			writer.close();
			trx.commit();
			trx.close();
		} catch (Exception e) {
			System.out.println("--------------- CODE -----"+code);
			System.out.println("\n\n ERROR:: "+e);
			
		}// try/catch
		
		return flag;
	}//load

	private Integer getProductId(String code) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT m_product_ID FROM m_product ");
		sql.append(" WHERE value = '" + code + "' ");
//		 System.out.println("getProductId SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("m_product_ID");
			if (value == null)
				value = 0;
		} catch (Exception e) {
			System.out.println("ERROR::"+e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getProductId

	/**
	 * crea lista de precios - create pricelist 
	 * @param trxName
	 * @param price
	 * @param bparnerId
	 * @param productId
	 * @param util
	 * @param listName
	 * @return
	 */
	private Boolean createPriceList(String trxName, BigDecimal price, Integer productId, 
					String listName, BigDecimal hour, Boolean allow){
		Boolean flag = true;
		Boolean pru = true;
		Boolean su = true;
		// /// lista de precios de compra
		
		String namePrice = "";
		try{
			Integer pId = 0;
			if (listName.length()>0){
				namePrice = "Compra_"+listName.trim();
				pId = DataQueriesTrans.getPriceListVersion(trxName,Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (pId <=0){
					pId = UtilTrans.createPriceListVersion(namePrice, purchaseId);
				}
				if (pId <=0){
					pId =  purchaseId;
				}
			}else{
				pId =  purchaseId;
			}
			if(!DataQueriesTrans.validatePriceListExists(productId, pId)){
				MProductPrice pur = new MProductPrice(Env.getCtx(), pId, productId,
						Env.ZERO, Env.ZERO, Env.ZERO, trxName);
//				pur.setAD_Org_ID(orgId);
				pur.set_ValueOfColumn("smj_estimatedTime", hour);
				pur.set_ValueOfColumn("smj_isallowchanges", allow);
				if (pur != null) {
					pru = pur.save();
				}// if(pur != null){
			}
				// ///LIstas de precios de venta
			Integer sId = 0;
			if (listName.length()>0){
				namePrice = "Venta_"+listName.trim();
				sId = DataQueriesTrans.getPriceListVersion(trxName,Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (sId <=0){
					sId = UtilTrans.createPriceListVersion(namePrice, salesId);
				}
				if (sId <=0){
					sId =  salesId;
				}
			}else{
				sId =  salesId;
			}
	
			if(!DataQueriesTrans.validatePriceListExists(productId, sId)){
//				BigDecimal salesPrice = UtilTrans.calculateSalesPrice(price, util, tax);
				MProductPrice sal = new MProductPrice(Env.getCtx(), sId, productId,
						price, price, price, trxName);
//				sal.setAD_Org_ID(orgId);
				sal.set_ValueOfColumn("smj_estimatedTime", hour);
				sal.set_ValueOfColumn("smj_isallowchanges", allow);
				if (sal != null) {
					su = sal.save();
				}// if(sal != null){
			}
			
			if (!pru || !su) {
				flag = false;
				System.out.println("Error en listas de precios - compra:"+pru+" - Venta: "+su);
				System.out.println("namePrice "+namePrice);
			}// if(!pru || !su){
		}catch (Exception e) {
			System.out.println("Error en listas de precios");
			System.out.println("ERROR:: "+e.getMessage()+ "e::"+e);
		}
		return flag;
	}//crearPriceList
	
}
