package com.smj.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;

public class ServiceCsvDAO {
private final static String fileSeparator = ";";
	
private Integer purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_PURCHASELIST",Env.getAD_Client_ID(Env.getCtx())).trim());
private Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",Env.getAD_Client_ID(Env.getCtx())).trim());
private Trx trx = Trx.get(Trx.createTrxName("AL"), true);
	/**
	 * carga archivo - load file
	 * @param csvFile
	 * @param separator
	 * @param processedDirectory
	 * @return
	 * @throws Exception
	 */
	public Boolean load(File csvFile, String separator, String processedDirectory) throws Exception{
		System.out.println("file...."+csvFile.getName());
		BufferedReader bufRdr;
		BufferedWriter writer;
		Boolean flag = true;
		Boolean allow = false;
		String code = "";
		try {
			
			//bufRdr = new BufferedReader(new FileReader(csvFile));
			bufRdr = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile.getAbsolutePath()), "UTF-8"));
			writer = new BufferedWriter(new FileWriter(processedDirectory + fileSeparator + csvFile.getName() + ".bad"));
			String line = null;
			int i = 0;
			int c = 0;
			// read each line of text file
			while ((line = bufRdr.readLine()) != null) {
				try {
					c=0;
					i++;
					line = line.replace("\"", ""); // it removes the "
					line = line.replace("\'", ""); // it removes the '
					String fields[] = line.split(separator, 15);
					code = fields[c++];
					String name = fields[c++].toUpperCase().trim();
					String system = fields[c++];    // system
					String subSystem = fields[c++];  //subsystem
					String cost = fields[c++];
					String iva = fields[c++];
					String iscommission = fields[c++];
					String isfixed = fields[c++];
					//valor precio
					String value = fields[c++];
//					System.out.println("value......"+val);
						
					String istires = fields[c++];
					String smj_util = fields[c++];
					//lista de precios por nombre 
					String serviceRate = fields[c++];
					BigDecimal price = Env.ZERO;
					try {
						price = new BigDecimal(value);
					} catch (Exception e) {
						System.out.println("ERROR EN PRECIO: "+value);
					}
					System.out.println(i+".*****...->"+code.trim());
					
					if (cost.equals("VERDADERO")){
						allow = true;
						
					}else{
						allow = false;
					}
					
					Integer productId = getProductId(trx.getTrxName(), code);
					if (productId > 0 )
					{
						System.out.println(i+". XXXXXXXX Producto repetido ->"+code+" - se crea nuevo precio solo si es lista de precios diferente "+price+"->"+serviceRate);
						MProduct p = new MProduct(Env.getCtx(), productId, trx.getTrxName());
						createPriceList(trx.getTrxName(), price, p.getM_Product_ID(), serviceRate, Env.ZERO, allow);
					}
					else
					{	
					MProduct p = new MProduct(Env.getCtx(), productId, trx.getTrxName());
					p.setValue(code.trim().toUpperCase());  // codigo
					p.setName(name); //nombre de producto
					
					
					Integer parent = getCategory(0, system);
					
					
					Integer cat = getCategory(parent, subSystem);
//					System.out.println("subs..."+subSystem+"...."+cat);
					p.setM_Product_Category_ID(cat);
					p.setProductType("S");
					p.setC_UOM_ID(100);
					
					if (cost.equals("VERDADERO")){
						p.set_ValueOfColumn("smj_isallowchanges", true);
						
					}else{
						p.set_ValueOfColumn("smj_isallowchanges", false);
					}
					
					//categoria de impuesto, si es iva o excento
					if (iva.equals("VERDADERO")){
						p.setC_TaxCategory_ID(1000005);
						
					}else{
						p.setC_TaxCategory_ID(1000004);
					}
					
					if (iscommission.equals("VERDADERO")){
						p.set_ValueOfColumn("smj_iscommission", true);
						
					}else{
						p.set_ValueOfColumn("smj_iscommission", false);
					}
					
					if (isfixed.equals("VERDADERO")){
						p.set_ValueOfColumn("smj_isfixed", true);
						
					}else{
						p.set_ValueOfColumn("smj_isfixed", false);
					}
					
					if (istires.equals("VERDADERO")){
						p.set_ValueOfColumn("smj_istires", true);
						
					}else{
						p.set_ValueOfColumn("smj_istires", false);
					}
					
					p.set_ValueOfColumn("smj_utility", new BigDecimal(smj_util));
					
					Boolean ok = p.save();
					if (!ok){
						flag = false;
					}
					
					
					createPriceList(trx.getTrxName(), price, p.getM_Product_ID(), serviceRate, Env.ZERO, allow);  // si no existia lo crea en 0
				}
//					System.out.println(p.getM_Product_ID()+"--guardado...."+ok);
				} catch (Exception ex) {
					System.out.println("ERROR::: "+ex);
					flag = false;
					writer.write(line + "\n");
					//StackTraceElement[] trace = ex.getStackTrace();
					//for (StackTraceElement stackTraceElement : trace) {
					//	System.out.println(" - error:: "+stackTraceElement.toString());
					//}
//					log.debug();
					System.out.println(ex.getStackTrace());

				}// try / catch dentro del while
			}// while
				// close the file
			bufRdr.close();
			writer.close();
			trx.commit();
			trx.close();
		} catch (Exception e) {
			System.out.println("--------------- CODE -----"+code);
			System.out.println("\n\n ERROR:: "+e);
			
		}// try/catch
		return flag;
	}//load

	/**
	 * regresa la categoria del producto - 
	 * returs product category
	 * @param parent
	 * @param category
	 * @return
	 */
	private Integer getCategory(Integer parent, String category){
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT M_Product_Category_ID FROM M_Product_Category ");
		sql.append(" WHERE UPPER(name) = '" + category.toUpperCase() + "' ");
//		 System.out.println("getCategory SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null); //trx.getTrxName()
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("M_Product_Category_ID");
			if (value == null || value==0 || value.equals(0)){
				System.out.println("Categoria:"+category.toUpperCase()+" con Padre ->"+ parent+" No encontrada - se crea una nueva");
				MProductCategory mp = new MProductCategory(Env.getCtx(), 0, null); //trx.getTrxName()  
				mp.setName(category.toUpperCase());
				mp.set_ValueOfColumn("smj_isservice", true);
				if(parent >0){
					mp.setM_Product_Category_Parent_ID(parent);
				}
				mp.save();
				value = mp.getM_Product_Category_ID();
			}
		} catch (Exception e) {
			System.out.println("ERROR::"+e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}

	/**
	 * crea lista de precios - create pricelist 
	 * @param trxName
	 * @param price
	 * @param bparnerId
	 * @param productId
	 * @param util
	 * @param listName
	 * @return
	 */
	private Boolean createPriceList(String trxName, BigDecimal price, Integer productId, 
					String listName, BigDecimal hour, Boolean allow){
		Boolean flag = true;
		Boolean pru = true;
		Boolean su = true;
		// lista de precios de compra
		
		String namePrice = "";
		try{
			Integer pId = 0;
			if (listName.length()>0){
				namePrice = "Compra_"+listName.trim();
				pId = DataQueriesTrans.getPriceListVersion(trxName,Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (pId <=0){
					pId = createPriceListVersion(trxName, namePrice, purchaseId);
				}
				if (pId <=0){
					pId =  purchaseId;
				}
			}else{
				pId =  purchaseId;
			} // lista de precios de compra en 0
			if(!DataQueriesTrans.validatePriceListExistsTrx(productId, pId,trxName)){
				MProductPrice pur = new MProductPrice(Env.getCtx(), pId, productId,
						Env.ZERO, Env.ZERO, Env.ZERO, trxName);
//				pur.setAD_Org_ID(orgId);
				pur.set_ValueOfColumn("smj_estimatedTime", hour);
				pur.set_ValueOfColumn("smj_isallowchanges", allow);
				if (pur != null) {
					pru = pur.save();
				}// if(pur != null){
			}	
				else {   // precio repetivo para lista existente
					System.out.println(" Precios - compra repetido:"+price+" - "+listName);
				}
			
				// ///LIstas de precios de venta
			Integer sId = 0;
			if (listName.length()>0){
				namePrice = "Venta_"+listName.trim();
				sId = DataQueriesTrans.getPriceListVersion(trxName, Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (sId <=0){
					sId = createPriceListVersion(trxName, namePrice, salesId);
				}
				if (sId <=0){
					sId =  salesId;
				}
				
			}else{
				sId =  salesId;
			}
	
			if(!DataQueriesTrans.validatePriceListExistsTrx(productId, sId,trxName)){
//				BigDecimal salesPrice = UtilTrans.calculateSalesPrice(price, util, tax);
				MProductPrice sal = new MProductPrice(Env.getCtx(), sId, productId,
						price, price, price, trxName);
//				sal.setAD_Org_ID(orgId);
				sal.set_ValueOfColumn("smj_estimatedTime", hour);
				sal.set_ValueOfColumn("smj_isallowchanges", allow);
				if (sal != null) {
					su = sal.save();
				}// if(sal != null){
			}	
				else {
						MProductPrice sal = MProductPrice.get(Env.getCtx(), sId, productId,	trxName);
						sal.setPriceLimit(price);
						sal.setPriceList(price);
						sal.setPriceStd(price);
						sal.set_ValueOfColumn("smj_estimatedTime", hour);
						sal.set_ValueOfColumn("smj_isallowchanges", allow);
						if (sal != null) {
							su = sal.save();
						}// if(sal != null){
						
					System.out.println(" Precios - venta repetido:"+price+" - "+listName);
				}
			
			
			if (!pru || !su) {
				flag = false;
				System.out.println("Error en listas de precios - compra:"+pru+" - Venta: "+su);
				System.out.println("namePrice "+namePrice);
			}// if(!pru || !su){
		}catch (Exception e) {
			System.out.println("Error en listas de precios");
			System.out.println("ERROR:: "+e.getMessage());
		}
		return flag;
	}//crearPriceList

	/**
	 * consulta el codigo del producto si existe - 
	 * get product code if exits
	 * @param code
	 * @return
	 */
	private Integer getProductId(String trxName, String code) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT m_product_ID FROM m_product ");
		sql.append(" WHERE UPPER(value) = '" + code.trim().toUpperCase() + "' ");
		 System.out.println("getProductId SQL::"+sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("m_product_ID");
			if (value == null)
				value = 0;
		} catch (Exception e) {
			System.out.println("ERROR::"+e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getProductId
	
	/**
	 * crea una version de lista de precios - 
	 * create pricelist version
	 * @param name
	 * @param priceList
	 * @return
	 */
	Integer createPriceListVersion(String trxName, String name, Integer priceList){
		Integer code = 0;
		MPriceListVersion mplv = new MPriceListVersion(Env.getCtx(), 0, trxName);
		mplv.setM_PriceList_ID(priceList);
		mplv.setName(name);
		mplv.setDescription(name);
		mplv.setM_DiscountSchema_ID(1000000);
		mplv.setValidFrom(Env.getContextAsDate(Env.getCtx(), "#Date"));
		Boolean ok = mplv.save();
		if(ok){
			code = mplv.getM_PriceList_Version_ID();
		}
		return code;
	}//createPriceListVersion
	
}//ServiceCsvDAO
