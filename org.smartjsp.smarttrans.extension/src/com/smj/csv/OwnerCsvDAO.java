package com.smj.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.compiere.util.Env;

import com.smj.model.MSMJVehicleOwner;
import com.smj.util.DataQueriesTrans;

public class OwnerCsvDAO {

	private final static String fileSeparator = ";";
	
	/**
	 * carga archivo - load file
	 * @param csvFile
	 * @param separator
	 * @param processedDirectory
	 * @return
	 * @throws Exception
	 */
	public Boolean load(File csvFile, String separator, String processedDirectory) throws Exception{
		System.out.println("file...."+csvFile.getName());
		BufferedReader bufRdr;
		BufferedWriter writer;
		Boolean flag = true;
		String plate = "";
		try {
			
			bufRdr = new BufferedReader(new FileReader(csvFile));
			writer = new BufferedWriter(new FileWriter(processedDirectory + fileSeparator + csvFile.getName() + ".bad"));
			String line = null;
			int i = 0;
			int c = 0;
			// read each line of text file
			while ((line = bufRdr.readLine()) != null) {
				try {
					c=0;
					i++;
					line = line.replace("\"", ""); // it removes the "
					line = line.replace("\'", ""); // it removes the '
					String fields[] = line.split(separator, 6);
					plate = fields[c++];
//					System.out.println(plate+"*********************************************************fields..."+i+"..");
					Integer vehicleId = DataQueriesTrans.getVehicleId(plate);
					Integer ownerId = DataQueriesTrans.getVehicleOwnerId(plate);
					MSMJVehicleOwner v = new MSMJVehicleOwner(Env.getCtx(), ownerId, null);
					v.setsmj_vehicle_ID(vehicleId);
					String holderNit = fields[c++];
					try {
						Integer holder = DataQueriesTrans.getBpartnerId(Env.getAD_Client_ID(Env.getCtx()), holderNit);
						v.setC_BPartner_ID(holder);
					} catch (Exception e) {
						System.out.println("ERROR holder NIT:: "+holderNit+", Placa: "+plate);
					}
					String holderNit2 = fields[c++];
//					try {
//						Integer holder2 = DataQueriesTrans.getBpartnerId(Env.getAD_Client_ID(Env.getCtx()), holderNit2);
//						v.setc_bpartnerholder2_ID(holder2);
//					} catch (Exception e) {
//						System.out.println("ERROR holder2 NIT:: "+holderNit2+", Placa: "+plate);
//					}
//					String holderNit3 = fields[c++];
//					try {
//						Integer holder3 = DataQueriesTrans.getBpartnerId(Env.getAD_Client_ID(Env.getCtx()), holderNit3);
//						v.setc_bpartnerholder3_ID(holder3);
//					} catch (Exception e) {
//						System.out.println("ERROR holder3 NIT:: "+holderNit3+", Placa: "+plate);
//					}
//					try {
//						String isholder = fields[c++];
//						if (isholder!=null && isholder.equals("MISMO TENEDOR")){
//							v.setisownerholder(true);
//						}else{
//							v.setisownerholder(false);
//						}
//					} catch (Exception e) {
//					}
//					String ownerNit = fields[c++];
//					try {
//						Integer owner = DataQueriesTrans.getBpartnerId(Env.getAD_Client_ID(Env.getCtx()), ownerNit);
//						v.setc_bpartnerowner_ID(owner);
//					} catch (Exception e) {
//						System.out.println("ERROR Owner NIT:: "+ownerNit+", Placa: "+plate);
//					}
					Boolean ok = v.save();
					if (!ok){
						flag = false;
					}
//					System.out.println(v.getsmj_vehicle_ID()+"--guardado...."+ok);
				} catch (Exception ex) {
					flag = false;
					writer.write(line + "\n");
					StackTraceElement[] trace = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : trace) {
						System.out.println("\n\n error:: "+stackTraceElement.toString());
					}
//					log.debug();
					System.out.println(ex.getStackTrace());

				}// try / catch dentro del while
			}// while
				// close the file
			bufRdr.close();
			writer.close();

		} catch (Exception e) {
			System.out.println("\n\n ERROR:: "+e.getStackTrace());
			System.out.println("---------------Placa-----"+plate);
		}// try/catch
		
		return flag;
	}//load
	
	
}//DriverCsvDAO
