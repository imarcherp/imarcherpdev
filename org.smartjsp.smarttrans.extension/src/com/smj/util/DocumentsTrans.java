package com.smj.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPayment;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Trx;

/**
 * @version <li>SmartJSP: Documents, 2013/03/08
 *          <ul TYPE ="circle">
 *          <li>Clase que contienen los metodos de creacion de documentos
 *          <li>Class to create documents
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class DocumentsTrans {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
//	private final static Integer warrantyPriceList = Integer.parseInt(MSysConfig
//			.getValue("SMJ_WARRANTYPRICELIST", Env.getAD_Client_ID(Env.getCtx()))
//			.trim(),Env.getAD_Org_ID(Env.getCtx()));
//	private final static Integer warrantyRStatus = Integer.parseInt(MSysConfig
//			.getValue("SMJ-STATEWOWARRANTY", Env.getAD_Client_ID(Env.getCtx()))
//			.trim());

	/**
	 * crea la orden de venta - 
	 * create sales order
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param locationId
	 * @param orderDate
	 * @param SalesRepId
	 * @param payTermId
	 * @param standardOrder
	 * @param warehouseId
	 * @param salesListId
	 * @return MOrder
	 */
	public static MOrder createSalesOrder(String trxName, String desc, Integer partnerId, Integer locationId,
			Date orderDate, Integer SalesRepId, Integer payTermId, Integer standardOrder,
			Integer warehouseId, Integer salesListId, String plate, Integer vehicleId, String workOrder){

		MOrder order = null;
		try {
			order = new MOrder(Env.getCtx(), 0, trxName);
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationId);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N");
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);

			order.setC_PaymentTerm_ID(payTermId); 
			order.setM_Warehouse_ID(warehouseId);

				//para orden de venta - For order Sales
				order.setIsSOTrx(true); 
				order.setC_DocTypeTarget_ID(standardOrder);
				order.setC_DocType_ID(standardOrder);
				order.setPaymentRule("P"); 
				order.setM_PriceList_ID(salesListId);
				
			
			if(plate!=null){
				order.set_ValueOfColumn("smj_plate", plate);
			}
			if (vehicleId!= null && vehicleId > 0){
				order.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
			}
			if (workOrder!= null){
				order.set_ValueOfColumn("smj_workorder", workOrder);
			}
			Boolean pok = order.save();
			if (!pok) {
				log.log(Level.SEVERE, "Error save Create Order :: "	+ desc);
				return null;
			} 

		} catch (Exception e) {
			order = null;
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createSalesOrder - ERROR: Create Order :: "+desc +" - "+ e.getMessage(), e);
		} 
		return order;
	}// createSalesOrder

	/**
	 * crea la orden de compra - 
	 * create purchase order
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param orderDate
	 * @param SalesRepId
	 * @param payTermId
	 * @param standardOrder
	 * @param warehouseId
	 * @param purchaseListId
	 * @param isSystem
	 * @return
	 */
	public static MOrder createPurchaseOrder(String trxName, String desc, Integer partnerId, 
			Date orderDate, Integer SalesRepId, Integer payTermId, Integer standardOrder,
			Integer warehouseId, Integer purchaseListId, Boolean isSystem){

		MOrder order = null;
		Integer locationId = DataQueries.getLocationPartner(partnerId);
		try {
			order = new MOrder(Env.getCtx(), 0, trxName);
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationId);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N");
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);
			

			order.setC_PaymentTerm_ID(payTermId); 
			order.setM_Warehouse_ID(warehouseId);

				//para orden de compra - For purchase order
				order.setIsSOTrx(false); 
				order.setC_DocTypeTarget_ID(standardOrder); 
				order.setC_DocType_ID(standardOrder);
				order.setPaymentRule("P"); 
				order.setM_PriceList_ID(purchaseListId);
			
			order.set_ValueOfColumn("smj_isSystem", isSystem);
			Boolean pok = order.save();
			if (!pok) {
				log.log(Level.SEVERE,Documents.class.getName() + 
						".createPurchaseOrder - ERROR: Create Purchase Order :: "+desc );
				return null;
			} 

		} catch (Exception e) {
			order = null;
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPurchaseOrder - ERROR: Create Purchase Order :: "+desc +" - "+ e.getMessage(), e);
		} 
		return order;
	}// createPurchaseOrder
	
	/**
	 * crea las lineas de la orden - 
	 * create line order
	 * @param order
	 * @param productId
	 * @param qty
	 * @param price
	 * @param total
	 * @param description
	 * @return Boolean
	 */
	public static Integer createOrderLine(MOrder order, Integer productId, BigDecimal qty,
			BigDecimal price, String description, Integer attInstanceId, Integer locatorId,
			Integer bPartnerId, Boolean isSystem, Integer provider_ID, Date orderDate
			, String plate, Integer workOrderLineId, Integer sequence) {
		Boolean flag = false;
		Integer code = 0;
		Integer uomId = DataQueries.getProductUom(productId);
//---------------------------iMarch-----------------------------------------------------
		// Integer taxId = DataQueries.getProductTax(productId);    original
		Integer taxId = DataQueries.getProductTaxbyTrx(productId ,
				order.get_Value("IsSOTrx").equals(true));  // true es Ventas
//-----------------------------------------------------------------------

		Integer bPlocation = DataQueries.getLocationPartner(bPartnerId);
		try {
			MOrderLine line = new MOrderLine(order);
			line.setC_Order_ID(order.getC_Order_ID());
			line.setIsActive(true);
			line.setDescription(description);
			line.setC_BPartner_ID(bPartnerId);
			line.setC_BPartner_Location_ID(bPlocation);
			line.setM_Warehouse_ID(order.getM_Warehouse_ID());
			line.setC_Currency_ID(order.getC_Currency_ID());
			line.setDateOrdered(new Timestamp(orderDate.getTime()));
			line.setDatePromised(new Timestamp(orderDate.getTime()));
			line.setM_Product_ID(productId);
			line.setC_UOM_ID(uomId);
			line.setDiscount(Env.ZERO);
			line.setFreightAmt(Env.ZERO); 
			line.setC_Tax_ID(taxId);
			line.setIsDescription(false);
			line.setQtyEntered(qty);
			line.setQtyOrdered(qty);
			line.setQtyDelivered(Env.ZERO);
			line.setQtyInvoiced(Env.ZERO);
			line.setQtyReserved(Env.ZERO);
			line.setRRAmt(Env.ZERO);
			line.setPriceLimit(Env.ZERO);
			line.setPriceList(price);
			line.setPriceActual(price);
			line.setPriceEntered(price);
			line.setPrice(price);
			line.setPriceCost(Env.ZERO);
			line.setProcessed(false);
			line.setQtyLostSales(Env.ZERO);
//			if (attInstanceId >0){
//				line.setM_AttributeSetInstance_ID(attInstanceId);
//			}
//			line.setM_AttributeSetInstance_ID(0);
			if(locatorId >0 ){
				line.set_ValueOfColumn("smj_locator_ID", locatorId);
			}
			if (provider_ID!= null && provider_ID > 0){
				line.set_ValueOfColumn("smj_Provider_ID", provider_ID);
			}
			if(plate!=null){
				line.set_ValueOfColumn("smj_plate", plate);
			}
			if(workOrderLineId!=null){
				line.set_ValueOfColumn("smj_workOrderLine_ID", workOrderLineId);
			}
			line.set_ValueOfColumn("smj_isSystem", isSystem);
			if(sequence != null && sequence >0){
				line.setLine(sequence);
			}
			flag = line.save();
			line.setPriceList(price);
			line.setPriceActual(price);
			line.setPriceEntered(price);
			line.setPrice(price);
			flag = line.save();
			if (flag){
				code = line.getC_OrderLine_ID();
			}
		} catch (Exception e) {
			flag = false;
			description = description+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createOrderLine - ERROR: Create OrderLine :: "+description +" - "+ e.getMessage(), e);
		} 
		return code;
	}// createOrderLine

	/**
	 * Documento para el manejo de inventarios - 
	 * Document to manage inventory
	 * @param trxName
	 * @param desc
	 * @param order
	 * @param date
	 * @param documentId
	 * @param sales
	 * @return MInOut
	 */
	public static MInOut createMInOut(String trxName, String desc, MOrder order, Date date, 
			Integer documentId, Boolean sales) {
		Boolean lok = false;
		String instanceName = "";
		MInOut io = null;
		try {
			io = new MInOut(Env.getCtx(), 0, trxName);
			io.setDescription(desc);
			io.setIsSOTrx(sales); 
			io.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			io.setC_DocType_ID(documentId);// doctype 1000014-MM Receipt -- Warehouse Order 1000032
			io.setDescription(instanceName.trim());
			io.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
			io.setC_BPartner_ID(order.getC_BPartner_ID());
			io.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
			io.setM_Warehouse_ID(order.getM_Warehouse_ID());
			io.setC_Order_ID(order.getC_Order_ID());
			io.setFreightAmt(Env.ZERO);
			io.setChargeAmt(Env.ZERO);
			io.setCreateFrom("N");
			io.setGenerateTo("N");
			io.setCreateConfirm("N");
			io.setCreatePackage("N");
			if (sales){
				io.setMovementType("C-");
			}else{
				io.setMovementType("V+");
			}
			io.setSalesRep_ID(order.getSalesRep_ID());
			io.setMovementDate(new Timestamp(date.getTime()));
			io.setDateAcct(new Timestamp(date.getTime()));
			lok = io.save();
			if (!lok) {
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createMInOut - ERROR: Create InOut :: "+desc +" - "+ e.getMessage(), e);
			return null;
		}
		return io;
	}// createMInOut

	/**
	 * crea la linea de la entrega - 
	 * create in/out line
	 * @param io
	 * @param desc
	 * @param productId
	 * @param locatorId
	 * @param qty
	 * @param instanceId
	 * @return
	 */
	public static MInOutLine createIOLine(MInOut io, String desc, Integer productId, Integer locatorId, 
			BigDecimal qty, Integer orderLine, Integer sequence){
		MInOutLine line = null;
		Boolean flag = true;
		Integer uomId = DataQueries.getProductUom(productId);
		try {
			line = new MInOutLine(io);
			line.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			line.setM_InOut_ID(io.getM_InOut_ID());
			line.setC_OrderLine_ID(orderLine);
			line.setM_Product_ID(productId);
			line.setM_Locator_ID(locatorId);// 1000000
			line.setC_UOM_ID(uomId);
				line.setQtyEntered(qty);
				line.setMovementQty(qty);
//			if (instanceId != null && instanceId >0){
//				line.setM_AttributeSetInstance_ID(instanceId);
//			}	
			if (sequence != null && sequence >0){
				line.setLine(sequence);
			}
			line.setDescription(desc);
			flag = line.save();
			if (!flag){
				return null;
			}
		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createIOLine - ERROR: Create IO Line :: "+desc +" - "+ e.getMessage(), e);
		}
		return line;
	}//createIOLine
	
	/**
	 * crea la factura de compra apartir de la entrega - create purchace invoice from io
	 * @param trxName
	 * @param io
	 * @param desc
	 * @param date
	 * @param priceListId
	 * @param documentType
	 * @param paymentTermId
	 * @param plate
	 * @param vehicleId
	 * @param workOrder
	 * @return
	 */
	public static MInvoice createInvoice (String trxName, MInOut io, String desc, 
			Date date, Integer priceListId, Integer documentType, Integer paymentTermId
			, String plate, Integer vehicleId, String workOrder, Boolean isSOTrx){

		if (io==null){
			return null;
		}
		MInvoice invoice = null;
		try{
			invoice = new MInvoice(io, new Timestamp(date.getTime()));
			invoice.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			invoice.setIsActive(true);
			invoice.setIsSOTrx(isSOTrx); 
			invoice.setDateInvoiced(new Timestamp(date.getTime()));
			invoice.setDateAcct(new Timestamp(date.getTime()));
			invoice.setC_BPartner_ID(io.getC_BPartner_ID());
			invoice.setC_BPartner_Location_ID(io.getC_BPartner_Location_ID());
			invoice.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			invoice.setM_PriceList_ID(priceListId);
			invoice.setSalesRep_ID(io.getSalesRep_ID());
			invoice.setDocStatus(DocAction.STATUS_Drafted);
			invoice.setDocAction(DocAction.ACTION_Complete);
			invoice.setIsApproved(true);
			invoice.setDateOrdered(new Timestamp(date.getTime()));
			invoice.setC_ConversionType_ID(114);
			invoice.setDescription(desc);
			invoice.setC_PaymentTerm_ID(paymentTermId);
			invoice.setProcessed(true);
			invoice.setPosted(false);
			invoice.setC_DocTypeTarget_ID(documentType);
			invoice.setC_DocType_ID(documentType);
			invoice.setPaymentRule("P");
			invoice.setIsTaxIncluded(isSOTrx);//invoice.setIsTaxIncluded(true);
			if(plate!=null){
				invoice.set_ValueOfColumn("smj_plate", plate);
			}
			if (vehicleId!= null && vehicleId > 0){
				invoice.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
			}
			if (workOrder!= null){
				invoice.set_ValueOfColumn("smj_workorder", workOrder);
			}
			Boolean ok = invoice.save();
			if (!ok){
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoice - ERROR: Create Invoice :: "+desc +" - "+ e.getMessage(), e);
		} 
		return invoice;
	}//createInvoice
	
	/**
	 * crea la linea de la factura - create invoice line
	 * @param data
	 * @param desc
	 * @param productId
	 * @param price
	 * @param taxId
	 * @param total
	 * @param qty
	 * @param attibuteId
	 * @return Integer
	 */
	public static Integer createInvoiceLine(MInvoice data, String desc, Integer productId, BigDecimal price, Integer taxId,
			BigDecimal total, BigDecimal qty, Integer attibuteId, Integer orderLine, String plate
			, Integer sequence, Integer ioLine, Integer chargeId){
		Integer invLineId = 0; 
		MInvoiceLine line = null;
		Integer uomId = DataQueries.getProductUom(productId);
		try{
			line = new MInvoiceLine(data);
			line.setIsActive(true);
			line.setQtyEntered(new BigDecimal(1));
			line.setM_Product_ID(productId);
			line.setC_OrderLine_ID(orderLine);
			line.setPrice(price);
			line.setPriceActual(price);
			line.setPriceList(price);
			line.setC_Tax_ID(taxId);
			line.setLineTotalAmt(total);
			line.setLineNetAmt(total);
			line.setDescription(desc);
			line.setPriceEntered(price);
			line.setPriceLimit(price);
			line.setQty(qty);
			line.setQtyInvoiced(qty);
			line.setC_UOM_ID(uomId);
			line.setC_Charge_ID(chargeId);
			
			if(plate!=null){
				line.set_ValueOfColumn("smj_plate", plate);
			}
			if(sequence!= null && sequence>0){
				line.setLine(sequence);
			}
			if (ioLine != null && ioLine>0){
				line.setM_InOutLine_ID(ioLine);
			}
			Boolean flag = line.save();
			if (flag){
				invLineId = line.getC_InvoiceLine_ID();
			}
		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoiceLine - ERROR: Create Invoice Line:: "+desc +" - "+ e.getMessage(), e);
		}
		return invLineId;
	}//createInvoiceLine
	
	/**
	 * crea el pago para la factura - 
	 * create payment by invoice.
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param bankAccountId
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param aName
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @param writeOffAmt
	 * @param discountAmt
	 * @return MPayment
	 */
	public static MPayment createPayment(String trxName, MInvoice inv, BigDecimal value
			, Integer bankAccountId, String tenderType
			,String CreditCardType, String CreditCardNumber, String newCreditCardVV
			,Integer CreditCardExpMM, Integer newCreditCardExpYY, String aName
			,String routingNo, String accountNo, String checkNo, String micr
			, BigDecimal writeOffAmt, BigDecimal discountAmt, Integer orgId, Boolean complete){
		Boolean flag = true;
		MPayment pay = null;
		String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",Env.getAD_Client_ID(Env.getCtx())).trim();
		String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",Env.getAD_Client_ID(Env.getCtx())).trim();
		try {
			pay = new MPayment(Env.getCtx(), 0, trxName);
			pay.setAD_Org_ID(orgId);
			pay.setIsReceipt(true);
			pay.setC_DocType_ID(true);
			pay.setDescription(inv.getDescription());
			pay.setTrxType("S");
			pay.setC_BankAccount_ID(bankAccountId);
			pay.setC_BPartner_ID(inv.getC_BPartner_ID());
			pay.setC_Invoice_ID(inv.getC_Invoice_ID());
			pay.setC_Currency_ID(inv.getC_Currency_ID());
			pay.setPayAmt(value);
			pay.setWriteOffAmt(writeOffAmt);
			pay.setDiscountAmt(discountAmt);
			pay.setPosted(false);
			pay.setTenderType(tenderType);
			pay.setDateTrx(inv.getDateInvoiced());
			pay.setDateAcct(inv.getDateInvoiced());
//			pay.setIsAllocated(true);
//			pay.setIsApproved(true);
			if (tenderType.equals(tenderCreditCard)){ //credit card
				pay.setCreditCardType(CreditCardType);
				pay.setCreditCardNumber(CreditCardNumber);
				pay.setCreditCardVV(newCreditCardVV);
				pay.setCreditCardExpMM(CreditCardExpMM);
				pay.setCreditCardExpYY(newCreditCardExpYY);
				pay.setA_Name(aName);
			}else if(tenderType.equals(tenderCheck)){ //cheque - check
				pay.setRoutingNo(routingNo);
				pay.setAccountNo(accountNo);
				pay.setCheckNo(checkNo);
				pay.setMicr(micr);
				pay.setA_Name(aName);
//				pay.setDocumentNo(routingNo+": "+checkNo+" #"+micr);
			}
			Boolean ok = pay.save();
			if (ok){
				
				if (complete){
					pay.completeIt();
					pay.setDocStatus(DocAction.STATUS_Completed);
					pay.setDocAction(DocAction.STATUS_Closed);
					pay.setProcessed(true);
					flag = pay.save();
					if (!flag){
						return null;
					}
				}
				Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
				if (orgId.equals(orgInfotrans)){
					return pay;
				}
				//Modificacion iMarch, organizacion de trasporte de carga
				String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
				if (orgfreighttrans.equals("true")){
					return pay;
				}
				//--------------
			}else{
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPayment - ERROR: Create Payment:: "+ e.getMessage(), e);
		}
		return pay;
	}//createPayment
	
	/**
	 * crea un movimiento de materiales - 
	 * create a material movement
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param locationId
	 * @param movementDocId
	 * @param date
	 * @param salesRepId
	 * @return MMovement
	 */
	public static MMovement createMovement(String trxName, String desc, Integer partnerId, Integer locationId,
			Integer movementDocId, Date date, Integer salesRepId){
		MMovement mv =  null;
		try {
			mv =  new MMovement(Env.getCtx(), 0, trxName);
			mv.setDescription(desc);
			mv.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			mv.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			mv.setDocAction(DocAction.ACTION_Complete);
			mv.setC_DocType_ID(movementDocId); 
			mv.setMovementDate(new Timestamp(date.getTime()));
			mv.setDateReceived(new Timestamp(date.getTime()));
			mv.setSalesRep_ID(salesRepId);
			mv.setAD_User_ID(salesRepId);
			mv.setC_BPartner_ID(partnerId);
			mv.setC_BPartner_Location_ID(locationId);
			
			Boolean ok = mv.save();
			if (!ok){
				mv = null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createMovement - ERROR: Create Movement:: "+desc+" - " + e.getMessage(), e);
			return null;
		}
		return mv;
	}//createMovement
	
	/**
	 * crea la linea del movimiento a partir del movimiento - 
	 * create movement line from movement
	 * @param mv
	 * @param desc
	 * @param productId
	 * @param startLocatorId
	 * @param endLocatorId
	 * @param qty
	 * @return Boolean
	 */
	public static Boolean createMovementLine(MMovement mv, String desc, Integer productId, Integer startLocatorId,
			Integer endLocatorId, BigDecimal qty){
		Boolean flag = true;
		MMovementLine line = null;
		try {
			line = new MMovementLine(mv);
			line.setM_Product_ID(productId);
			line.setDescription(desc);
			line.setM_Locator_ID(startLocatorId);
			line.setM_LocatorTo_ID(endLocatorId);
			line.setMovementQty(qty);
			flag = line.save();
			
		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createMovementLine - Create Movement Line:: "+desc+" - " + e.getMessage(), e);
			flag = false;
		}
		return flag;
	}//createMovementLine
	
	/**
	 * crea la orden de venta warehouse - 
	 * create warehouse sales order 
	 * @param trx
	 * @param desc
	 * @param partnerId
	 * @param orderDate
	 * @param warehouseId
	 * @param priceListId
	 * @param SalesRepId
	 * @param paymentTermId
	 * @return
	 */
	public static MOrder createWarehouseOrder(Trx trx, String desc, Integer partnerId, Date orderDate, 
			Integer warehouseId, Integer priceListId, Integer SalesRepId, Integer paymentTermId, String workOrder){
		Integer docOrderWarehouseId = Integer.parseInt(MSysConfig.getValue("SMJ-DOCWAREHOUSEORDER",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer locationID = DataQueries.getLocationPartner(partnerId);
		MOrder order = null;
		try {
			order = new MOrder(Env.getCtx(), 0, trx.getTrxName());
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setDateAcct(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationID);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N"); 
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);
			//todo termino de pago
			order.setC_PaymentTerm_ID(paymentTermId); 
			order.setM_Warehouse_ID(warehouseId);

				//para orden de venta - For order Sales
				order.setIsSOTrx(true); 
				order.setC_DocTypeTarget_ID(docOrderWarehouseId);//SMJ-DOCWAREHOUSEORDER
				order.setC_DocType_ID(docOrderWarehouseId);//0
				order.setPaymentRule("P"); //P - Con termino de Pago 
				order.setM_PriceList_ID(priceListId);
		
			if (workOrder!= null){
				order.set_ValueOfColumn("smj_workorder", workOrder);
			}
				
			Boolean pok = order.save();
			if (!pok) {
				trx.rollback();
				log.log(Level.SEVERE, Documents.class.getName() + ".createWarehouseOrder - Error save Create Warehouse Order :: "	+ desc);
				return null;
			} 

		} catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, Documents.class.getName() + 
					".createWarehouseOrder - Error save Create Warehouse Order :: "+desc +" - " + e.getMessage(),e);
		} 
		return order;
	}// createWarehouseOrder
	
	/**
	 * crea el pago sin factura - 
	 * create payment without invoice.
	 * @param trxName
	 * @param description
	 * @param bpartnerId
	 * @param value
	 * @param bankAccountId
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param aName
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @param writeOffAmt
	 * @param discountAmt
	 * @return MPayment
	 */
	public static MPayment createPaymentNoinvoice(String trxName, String description, Integer bpartnerId
			, BigDecimal value , Integer bankAccountId, String tenderType
			,String CreditCardType, String CreditCardNumber, String newCreditCardVV
			,Integer CreditCardExpMM, Integer newCreditCardExpYY, String aName
			,String routingNo, String accountNo, String checkNo, String micr
			, BigDecimal writeOffAmt, BigDecimal discountAmt, Integer docTypeID, Boolean isPrepayment){
		MPayment pay = null;
		String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",Env.getAD_Client_ID(Env.getCtx())).trim();
		String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",Env.getAD_Client_ID(Env.getCtx())).trim();
		try {
			pay = new MPayment(Env.getCtx(), 0, trxName);
			pay.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			pay.setIsReceipt(true);
			pay.setC_DocType_ID(docTypeID);
			pay.setDescription(description);
			pay.setTrxType("S");
			pay.setC_BankAccount_ID(bankAccountId);
			pay.setC_BPartner_ID(bpartnerId);
			pay.setC_Invoice_ID(0);
			pay.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			pay.setPayAmt(value);
			pay.setWriteOffAmt(writeOffAmt);
			pay.setDiscountAmt(discountAmt);
			pay.setPosted(false);
			pay.setTenderType(tenderType);
			pay.setAccountNo(accountNo);  // solicitud 1000403
			pay.setDateTrx(Env.getContextAsDate(Env.getCtx(), "#Date"));
			pay.setDateAcct(Env.getContextAsDate(Env.getCtx(), "#Date"));
			pay.setIsPrepayment(isPrepayment);
//			pay.setIsAllocated(true);
//			pay.setIsApproved(true);
			if (tenderType.equals(tenderCreditCard)){ //credit card
				pay.setCreditCardType(CreditCardType);
				pay.setCreditCardNumber(CreditCardNumber);
				pay.setCreditCardVV(newCreditCardVV);
				pay.setCreditCardExpMM(CreditCardExpMM);
				pay.setCreditCardExpYY(newCreditCardExpYY);
				pay.setA_Name(aName);
			}else if(tenderType.equals(tenderCheck)){ //cheque - check
				pay.setRoutingNo(routingNo);
				pay.setAccountNo(accountNo);
				pay.setCheckNo(checkNo);
				pay.setMicr(micr);
				pay.setA_Name(aName);
//				pay.setDocumentNo(routingNo+": "+checkNo+" #"+micr);
			}
			Boolean ok = pay.save();
			if (!ok){
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPaymentNoinvoice - ERROR: Create Payment:: "+ e.getMessage(), e);
		}
		return pay;
	}//createPaymentNoinvoice
	
	/**
	 * crea la factura de compra apartir sin entrega - 
	 * create purchace invoice without io
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param date
	 * @param priceListId
	 * @param documentType
	 * @param paymentTermId
	 * @param plate
	 * @param vehicleId
	 * @param workOrder
	 * @param isSOTrx
	 * @param salesRepId
	 * @return MInvoice
	 */
	public static MInvoice createInvoiceNoIo (String trxName, String desc, Integer partnerId,
			Timestamp date, Integer priceListId, Integer documentType, Integer paymentTermId
			, String plate, Integer vehicleId, String workOrder, Boolean isSOTrx, Integer salesRepId,String sequence,boolean taxIncluded) {

		MInvoice invoice = null;
		Integer locationId = DataQueries.getLocationPartner(partnerId);
		try{
			invoice = new MInvoice(Env.getCtx(), 0, trxName);
			
			// solicitud 1000387
			if (sequence != null && !sequence.isEmpty() && !sequence.equalsIgnoreCase("0"))   // es necesario asignar secuenca manual
			{
				invoice.setDocumentNo(sequence);
			}
			
			invoice.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			invoice.setIsActive(true);
			invoice.setIsSOTrx(isSOTrx); 
			invoice.setDateInvoiced(date);
			invoice.setDateAcct(date);
			invoice.setC_BPartner_ID(partnerId);
			invoice.setC_BPartner_Location_ID(locationId);
			invoice.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			invoice.setM_PriceList_ID(priceListId);
			invoice.setSalesRep_ID(salesRepId);
			invoice.setDocStatus(DocAction.STATUS_Drafted);
			invoice.setDocAction(DocAction.ACTION_Complete);
			invoice.setIsApproved(true);
			invoice.setDateOrdered(date);
			invoice.setC_ConversionType_ID(114);
			invoice.setDescription(desc);
			invoice.setC_PaymentTerm_ID(paymentTermId);
			invoice.setPosted(false);
			invoice.setC_DocTypeTarget_ID(documentType);
			invoice.setC_DocType_ID(documentType);
			invoice.setPaymentRule("P");
			invoice.setIsTaxIncluded(taxIncluded);
			
			if(plate!=null) {
				invoice.set_ValueOfColumn("smj_plate", plate);
			}
			
			if (vehicleId!= null && vehicleId > 0) {
				invoice.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
			}
			
			if (workOrder!= null) {
				invoice.set_ValueOfColumn("smj_workorder", workOrder);
			}
			
			invoice.saveEx();
			
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoice - ERROR: Create Invoice :: "+desc +" - "+ e.getMessage(), e);
			return null;
		} 
		return invoice;
	}//createInvoice
	
	/**
	 * crea la linea del movimiento a partir del movimiento - 
	 * create movement line from movement
	 * @param mv
	 * @param desc
	 * @param productId
	 * @param startLocatorId
	 * @param endLocatorId
	 * @param qty
	 * @return MMovementLine
	 */
	public static MMovementLine newMovementLine(MMovement mv, String desc, Integer productId, Integer startLocatorId,
			Integer endLocatorId, BigDecimal qty) {
		MMovementLine line = null;
		
		line = new MMovementLine(mv);
		line.setM_Product_ID(productId);
		line.setDescription(desc);
		line.setM_Locator_ID(startLocatorId);
		line.setM_LocatorTo_ID(endLocatorId);
		line.setMovementQty(qty);
		line.saveEx();		
		
		return line;
	}//newMovementLine
	
	/**
	 * Create Purchase Order and Material Receipt
	 * @throws Exception
	 */
	public static MInOut createConsignmentDocuments(int C_BPartner_ID, int M_PriceList_Version_ID, int M_Warehouse_ID,
			String description, Boolean isPositive, String trxName) throws Exception {

		int C_DocType_ID;
		int M_PriceList_ID = new MPriceListVersion(Env.getCtx(), M_PriceList_Version_ID, trxName).getM_PriceList_ID();
		Timestamp date = Env.getContextAsDate(Env.getCtx(), "#Date");

		if (isPositive) {
			C_DocType_ID= MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_Order_ID");			
		} else {
			C_DocType_ID= MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_ConsignOrder_ID");
		}

		MOrder order = new MOrder(Env.getCtx(), 0, trxName);
		order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		order.setPOReference("CONSIGNACION");
		order.setBPartner(MBPartner.get(Env.getCtx(), C_BPartner_ID));
		order.setDateOrdered(date);
		order.setDateAcct(date);
		order.setDatePromised(date);
		order.setM_PriceList_ID(M_PriceList_ID);
		order.setM_Warehouse_ID(M_Warehouse_ID);
		order.setDescription(description);
		order.setC_DocTypeTarget_ID(C_DocType_ID);
		order.setC_DocType_ID(C_DocType_ID);
		order.setIsSOTrx(false);

		order.saveEx();

		// Create MInOut (Consignment)
		int C_DocTypeShipment_ID;

		if (isPositive) {
			C_DocTypeShipment_ID = MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_Receipt_ID");			
		} else {
			C_DocTypeShipment_ID = MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_ConsignReceipt_ID");
		}

		MInOut inOut = new MInOut(order, C_DocTypeShipment_ID, order.getDateOrdered());
		inOut.saveEx();

		return inOut;
	}
	
	/**
	 * Create Order Line (Consignment)
	 * @param order
	 * @param M_Product_ID
	 * @param M_PriceList_Version_ID
	 * @param qty
	 * @param trxName
	 * @throws Exception
	 */
	public static void createConsignmentLinesPositive(MInOut inOut, int M_Product_ID, int M_PriceList_Version_ID, 
			BigDecimal qty, int M_AttributeSetInstance_ID, String trxName) throws Exception {

		MOrder order = new MOrder(Env.getCtx(), inOut.getC_Order_ID(), trxName);

		MProductPrice pp = MProductPrice.get(Env.getCtx(), M_PriceList_Version_ID, M_Product_ID, trxName);

		MOrderLine oLine = new MOrderLine(order);
		oLine.setProduct(MProduct.get(Env.getCtx(), M_Product_ID));
		oLine.setPrice(pp.getPriceList());
		oLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
		oLine.setQty(qty);

		oLine.saveEx();

		createMInOutLineConsignmentPositive(inOut, oLine, M_AttributeSetInstance_ID);
	}
	
	/**
	 * Create InOut Line (Consignment)
	 * @param inOut
	 * @param oLine
	 */
	public static void createMInOutLineConsignmentPositive(MInOut inOut, MOrderLine oLine, int M_AttributeSetInstance_ID) {
		MInOutLine inOutLine = new MInOutLine(inOut);
		inOutLine.setOrderLine(oLine, 0, oLine.getQtyOrdered());
		inOutLine.setQty(oLine.getQtyOrdered());
		inOutLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
		
		inOutLine.saveEx();
	}
	
	/**
	 * Create Order Line (Consignment)
	 * @param order
	 * @param M_Product_ID
	 * @param M_PriceList_Version_ID
	 * @param qty
	 * @param trxName
	 * @throws Exception
	 */
	public static  Map<Integer, BigDecimal> createConsignmentLinesNegative(MInOut inOutNegative, MInOut inOutPositive, int M_Product_ID, int M_PriceList_Version_ID, 
			BigDecimal qty, Map<Integer, BigDecimal> mapAttributeQty, String trxName) throws Exception {

		MOrder order = new MOrder(Env.getCtx(), inOutNegative.getC_Order_ID(), trxName);

		MProductPrice pp = MProductPrice.get(Env.getCtx(), M_PriceList_Version_ID, M_Product_ID, trxName);

		MOrderLine oLine = new MOrderLine(order);
		oLine.setProduct(MProduct.get(Env.getCtx(), M_Product_ID));
		oLine.setPrice(pp.getPriceList());
		oLine.setQty(qty);

		oLine.saveEx();

		return createMInOutLineConsignmentNegative(inOutNegative, inOutPositive, oLine, mapAttributeQty, M_PriceList_Version_ID);
	}
	
	/**
	 * Create InOut Line (Consignment)
	 * @param inOutNegative
	 * @param oLine
	 * @throws Exception 
	 */
	public static Map<Integer, BigDecimal> createMInOutLineConsignmentNegative(MInOut inOutNegative, MInOut inOutPositive, MOrderLine oLine, Map<Integer, BigDecimal> mapAttributeQty, 
			int M_PriceList_Version_ID) throws Exception {
		
		MInOutLine inOutLine = new MInOutLine(inOutNegative);
		inOutLine.setOrderLine(oLine, 0, oLine.getQtyOrdered());

		//Get Attribute Set Instance, only when the quantity is negative
		//This is because you need to subtract the quantity at provider location

		String whereClause = "M_Product_ID = ? AND M_Locator_ID = ? AND QtyOnHand > 0";
		Query query = new Query(Env.getCtx(), MStorageOnHand.Table_Name, whereClause, oLine.get_TrxName());
		query.setParameters(inOutLine.getM_Product_ID(), inOutLine.getM_Locator_ID());
		query.setOrderBy("M_StorageOnHand.DateMaterialPolicy");
		List<MStorageOnHand> storages = query.list();

		BigDecimal positiveQty = oLine.getQtyOrdered().negate();
		BigDecimal usedQty = Env.ZERO;
		boolean first = true;
		
		if (mapAttributeQty == null) {
			mapAttributeQty = new HashMap<Integer, BigDecimal>();
		}

		for (MStorageOnHand storage: storages) {
			BigDecimal reqQty = positiveQty.subtract(usedQty);				
			BigDecimal attributeQty = Env.ZERO;
			
			if (reqQty.signum() != 1) {
				break;
			}
			
			//Validates if the attribute was used in other line ((true) = get qty used)
			if (mapAttributeQty.containsKey(storage.getM_AttributeSetInstance_ID())) {
				attributeQty = mapAttributeQty.get(storage.getM_AttributeSetInstance_ID());
			}
			
			BigDecimal qtyOnHand = storage.getQtyOnHand().subtract(attributeQty); //subtract previous qty (Other Lines)
			
			if (qtyOnHand.signum() == 0) {
				continue;
			}

			BigDecimal qtyToUse;

			if (qtyOnHand.compareTo(reqQty) >= 0) {
				qtyToUse = reqQty;
			} else {
				qtyToUse = qtyOnHand;
			}

			if (mapAttributeQty.containsKey(storage.getM_AttributeSetInstance_ID())) {
				mapAttributeQty.remove(storage.getM_AttributeSetInstance_ID());
			}
			
			//Registers the quantity used for the attribute in this line
			mapAttributeQty.put(storage.getM_AttributeSetInstance_ID(), qtyToUse.add(attributeQty));
			
			// First line, not need create a new line
			if (first) {
				inOutLine.setQty(qtyToUse.negate());
				inOutLine.setM_AttributeSetInstance_ID(storage.getM_AttributeSetInstance_ID());
				inOutLine.saveEx();

				first = false;
			} else {
				MInOutLine ioLine = new MInOutLine(inOutNegative);
				ioLine.setOrderLine(oLine, inOutLine.getM_Locator_ID(), qtyToUse);
				ioLine.setQty(qtyToUse.negate());
				ioLine.setM_AttributeSetInstance_ID(storage.getM_AttributeSetInstance_ID());
				ioLine.saveEx();
			}

			// Create the positive line, it's necessary for keep the same Attribute
			createConsignmentLinesPositive(inOutPositive, oLine.getM_Product_ID(), M_PriceList_Version_ID, qtyToUse, storage.getM_AttributeSetInstance_ID(), oLine.get_TrxName());
			
			usedQty = usedQty.add(qtyToUse);
		}
		
		
		return  mapAttributeQty;
	}
	
}//Documents