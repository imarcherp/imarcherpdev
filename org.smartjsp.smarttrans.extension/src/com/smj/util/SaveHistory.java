package com.smj.util;

import org.compiere.util.Env;

import com.smj.model.MSMJHistoryTire;
import com.smj.model.MSMJTires;
import com.smj.util.DataQueriesTrans;

public class SaveHistory {

	public static void saveHistorytire(String trxName, MSMJTires tire, Integer reason) throws Exception {
		MSMJHistoryTire h = new MSMJHistoryTire(Env.getCtx(), 0, trxName);
		h.setM_Product_ID(tire.getM_Product_ID());
		h.setsmj_vehicle_ID(tire.getsmj_vehicle_ID());
		h.setsmj_plate(DataQueriesTrans.getVehiclePlate(tire.getsmj_vehicle_ID()));
		h.setC_BPartner_ID(tire.getC_BPartner_ID());
		h.setPosition(tire.getPosition());
		h.setserial(tire.getserial());
		h.setdatemount(tire.getdatemount());
		h.setdateumount(Env.getContextAsDate(Env.getCtx(), "#Date"));
		h.setsmj_changeTire_ID(reason);
		h.setsmj_workOrderLine_ID(tire.getsmj_workOrderLine_ID());
		h.setkms(tire.getkms());
		h.setvaluebrake(tire.getvaluebrake());
		h.setvaluetire(tire.getvaluetire());
		h.settirestate(tire.getStatus());

		h.saveEx();
	}//saveHistorytire
	
}//SaveHistory
