package com.smj.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPayment;
import org.compiere.model.MProduct;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.model.MSMJMechanicCommission;
import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;

public class CompleteSaleWebTrans {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	private static Integer purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_PURCHASELIST",Env.getAD_Client_ID(Env.getCtx())).trim());
	private static Integer principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",Env.getAD_Client_ID(Env.getCtx())).trim();
	private final static String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",Env.getAD_Client_ID(Env.getCtx())).trim();
	private final static Integer stateInvoiced = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOINVOICED",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
	
//---iMARCH
	private static String LocatorConsignament = "";
	private static String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
//----------------------------
	
	/**
	 * crea los documentos de factura, entrega y movimiento -
	 * create DocumentsTrans invoice, in/out and movement 
	 * @param trxName
	 * @param sales
	 * @param order
	 * @return
	 */
	public static MInvoice completeDocumentsSales(String trxName, Boolean sales, MOrder order,boolean generatedOnlyShipment, boolean hasTransportRequest){
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		String description = "";
		String descPurchaseOrder = MSysConfig.getValue("SMJ-MSGDEFAULTDESCORDER",Env.getAD_Client_ID(Env.getCtx())).trim();
		Integer docPurchaseOrder = Integer.parseInt(MSysConfig.getValue("SMJ-DOCPURCHASEORDER",Env.getAD_Client_ID(Env.getCtx())).trim()); 
		MInvoice inv = null;
		try {
			String desc = order.getDescription();
			Integer orderId = order.getC_Order_ID();
			Integer payTermId = order.getC_PaymentTerm_ID(); 
				Integer documentMM= Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTMMSHIPMENT",Env.getAD_Client_ID(Env.getCtx())).trim());
				Integer locatorID = DataQueries.getLocatorId(order.getM_Warehouse_ID());
				description = "Create In/Out";
				MInOut io = DocumentsTrans.createMInOut(trxName, desc, order, order.getDateOrdered(),documentMM, sales);
				LinkedList<MOrderLine> listOrders = DataQueriesTrans.getOrderLines(orderId, trxName);
				if(listOrders == null || listOrders.size() <= 0){
					log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+".completeDocumentsTransales - Error Create Order Lines Pruchase Automatic :: "	+ description);
					return null;
				}
				description = "Create Invoice";
				int clientCode = Env.getAD_Client_ID(  Env.getCtx());
				Integer documentARInv= Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTARINVOICE",clientCode,orgId).trim()); // obtiene el tipo por organizacion
				
				
				//Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",clientCode).trim());
				Integer salesId = order.getM_PriceList_ID();  // toma la lista de precios que viene en la orden
				if (orgId.equals(orgInfotrans)){
					salesId = Integer.parseInt(MSysConfig.getValue("SMJ_INFSALESLIST",clientCode).trim());
					principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-INFWHPRINCIPAL",clientCode).trim());
					
					purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_INFPURCHASELIST", clientCode, orgId).trim());
					// si la primera linea era una orden de transporte, cambia el tipo de factura
					if (hasTransportRequest)  // fix solicitud 384/385
					{
					documentARInv= Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTARINVOICE-IT2",clientCode,orgId).trim()); // obtiene el tipo por organizacion
					}
				}
				String plate = order.get_ValueAsString("smj_plate");
				Integer vehicleId = order.get_ValueAsInt("smj_vehicle_ID");
				String workOrder = order.get_ValueAsString("smj_workorder");
				if (!generatedOnlyShipment) {
				inv = DocumentsTrans.createInvoice(trxName, io, desc, order.getDateOrdered(), salesId, 
						documentARInv, payTermId, plate, vehicleId, workOrder, io.isSOTrx());
				}

				if(io != null && listOrders != null && listOrders.size() > 0){
					MMovement mv = null;
					MOrder purchaseOrder = null;
					Iterator<MOrderLine> ito = listOrders.iterator();
					while (ito.hasNext()){
						MOrderLine oLine = ito.next();
						
						Integer lineLocator = oLine.get_ValueAsInt("smj_locator_ID");
						MProduct p = oLine.getProduct();
						Boolean isService = p.getProductType().equals("S")?true:false;
						//logica de crear orden de compra

						if(!locatorID.equals(lineLocator) && !isService){
						//if(!locatorID.equals(lineLocator) && !isService && LocatorConsignament=="Y"){
							String movDesc = "";
							if (generatedOnlyShipment) {
							movDesc = "Orden de Venta: "+order.getDocumentNo()+" - Entrega: "+io.getDocumentNo()+ " - "+desc;
							}
							else {
							movDesc = "Orden de Venta: "+order.getDocumentNo()+"- Factura: "+inv.getDocumentNo() +" - Entrega: "+io.getDocumentNo()+ " - "+desc;
							}
									
							if (mv == null){
								Integer documentMV = Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTMOVEMENT",Env.getAD_Client_ID(Env.getCtx())).trim());
								description = "Create Movement";
								mv = DocumentsTrans.createMovement(trxName, movDesc, order.getC_BPartner_ID(), order.getC_BPartner_Location_ID(), documentMV, order.getDateOrdered(), order.getSalesRep_ID());
//								System.out.println("movimiento...."+mv.getDocumentNo());
							}
							description = "Create Movement Line";
							Boolean mov = DocumentsTrans.createMovementLine(mv, movDesc, oLine.getM_Product_ID(), lineLocator, locatorID, oLine.getQtyEntered());
							if (!mov){
								log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+".completeDocumentsTransales - Error Create Movement Line :: "	+ desc);
								return null;
							}
							
							Integer providerId =  oLine.get_ValueAsInt("smj_Provider_ID");
							if (providerId == null || providerId <= 0){
								MLocator locator = MLocator.get(Env.getCtx(), lineLocator);
								providerId = DataQueries.getwarehouseOwner(locator.getM_Warehouse_ID());
							}
//--------iMARCH-----------busca si es bodega de consignacion--------------------
							LocatorConsignament = DataQueries.getLocatorConsignment(lineLocator);
							if (LocatorConsignament.equals("Y") ){
								Integer existentPO = DataQueries.getCurrentPurchaseOrderPOS(trxName, descPurchaseOrder, providerId,docPurchaseOrder, Env.getAD_Client_ID(Env.getCtx()));
								if (existentPO > 0){
									purchaseOrder = new MOrder(Env.getCtx(), existentPO, trxName); 
								}else{
									purchaseOrder = DocumentsTrans.createPurchaseOrder(trxName, descPurchaseOrder, providerId, 
											order.getDateOrdered(), Env.getAD_User_ID(Env.getCtx()), payTermId, docPurchaseOrder, 
											principalWarehouse, purchaseId, true);
								}
								BigDecimal purchasePrice = DataQueries.getPurchasePrice(trxName, p, providerId, lineLocator);
								String descPurchaseLine = "";
								if (generatedOnlyShipment) {
									descPurchaseLine= "Orden de Venta: "+order.getDocumentNo()+" - Entrega: "+io.getDocumentNo()+" - "+descPurchaseOrder;
								}
								else {
									descPurchaseLine= "Orden de Venta: "+order.getDocumentNo()+"- Factura: "+inv.getDocumentNo() +" - Entrega: "+io.getDocumentNo()+" - "+descPurchaseOrder;
								}
								
								Integer code = DocumentsTrans.createOrderLine(purchaseOrder, oLine.getM_Product_ID(), oLine.getQtyEntered(), purchasePrice
										, descPurchaseLine, oLine.getM_AttributeSetInstance_ID(), locatorID, providerId,true,providerId,new Date(), null, null, null);
								if(code <= 0){
									log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+".completeDocumentsTransales - Error Create Purchase order Line :: "	+ descPurchaseOrder);
									return null;
								}
						   }//orden de compra si es bodega consignacion
						}//if locators
						description = "Create In/Out Line";
						String descLine = oLine.getDescription();
						MInOutLine ioLine = DocumentsTrans.createIOLine(io, descLine, oLine.getM_Product_ID(), locatorID, oLine.getQtyEntered()
								, oLine.getC_OrderLine_ID(), oLine.getLine());
						if (ioLine == null){
							return null;
						}
						description = "Create Invoice Line";
						String plateLine = oLine.get_ValueAsString("smj_plate");
						//se elimino por problema de inventarios oLine.getM_AttributeSetInstance_ID()
						if (!generatedOnlyShipment) { // no ejecuta si es solo entrega
						Integer invLineId = DocumentsTrans.createInvoiceLine(inv, descLine, oLine.getM_Product_ID(), oLine.getPriceEntered(), oLine.getC_Tax_ID(), 
								oLine.getLineNetAmt(), oLine.getQtyEntered(), 0, oLine.getC_OrderLine_ID(), plateLine, oLine.getLine(), ioLine.getM_InOutLine_ID(), 0);
						if(invLineId <= 0){
							return null;
						}
						
						Integer woLineId = oLine.get_ValueAsInt("smj_workOrderLine_ID");
						if(woLineId != null && woLineId > 0){
							MSMJWorkOrderLine woLine = new MSMJWorkOrderLine(Env.getCtx(), woLineId, trxName);
							MRequest req = new MRequest(Env.getCtx(), woLine.getR_Request_ID(), trxName);
							req.setR_Status_ID(stateInvoiced);
							req.setC_Invoice_ID(inv.getC_Invoice_ID());
							req.save();
							Boolean wok = calculateCommisionProcess(trxName, woLine, invLineId);
							if(!wok){
								return null;
							}
						}
						} // if (!generatedOnlyShipment)
					}//while ito
					Boolean mvok = true;
					if (mv != null){
						description = "Complete In/Out";
						mv.completeIt();
						mv.setDocStatus(DocAction.STATUS_Completed);
						mv.setDocAction(DocAction.STATUS_Closed);
						mv.setProcessed(true);
						mvok = mv.save();
					}
					description = "Complete In/Out";
					io.completeIt();
					io.setDocStatus(DocAction.STATUS_Completed);
					io.setDocAction(DocAction.STATUS_Closed);
					io.setProcessed(true);
					Boolean iook = io.save();
					description = "Complete Invoice";
					
					if (!generatedOnlyShipment) { // no ejecuta si es solo entrega
					inv.completeIt();
					inv.setDocStatus(DocAction.STATUS_Completed);
					inv.setDocAction(DocAction.STATUS_Closed);
					inv.setProcessed(true);					
					if (orgId.equals(orgInfotrans)){
						updateInvoiceWO(trxName, workOrder, inv.getC_Invoice_ID());	
					}
					//Adicion iMarch / Organizacion Transportadora -----
					orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
					if(orgfreighttrans.equals("true")){
						updateInvoiceWO(trxName, workOrder, inv.getC_Invoice_ID());	
					}
					//--------------------------------------------------
					Boolean inok = inv.save();
					if (!iook || !inok || !mvok){
						return null;
					}
					} //  (!generatedOnlyShipment)
				}else{
					return null;
				}
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+".completeDocumentsTransales - Error Create DocumentsTrans Sales :: "	+ description, e);
			return null;
		}
		return inv;
	}//completeDocumentsTransSales
	
	/**
	 * crea el pago correspondiente - create payment
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param A_Name
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @return Integer
	 */
	public static Integer createPayment(String trxName, MInvoice inv, BigDecimal value, String tenderType, String CreditCardType, 
			String CreditCardNumber, String newCreditCardVV, Integer CreditCardExpMM, Integer newCreditCardExpYY, 
			String A_Name, String routingNo, String accountNo ,String checkNo, String micr, 
			Integer payTermId, BigDecimal writeOffAmt, BigDecimal discountAmt){
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		Integer paymentId = -1;
		Integer bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-ACCOUNTDEFAULT",Env.getAD_Client_ID(Env.getCtx()),orgId).trim());
		Integer payInternal = Integer.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMINTERNAL",Env.getAD_Client_ID(Env.getCtx())).trim());
		if(payTermId.equals(payInternal)){
			bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-ACCOUNTINTERNAL",Env.getAD_Client_ID(Env.getCtx())).trim());
		}
		if (orgId.equals(orgInfotrans)){
			bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-INFBANKACCOUNT",Env.getAD_Client_ID(Env.getCtx())).trim());
		}
		MPayment pay = null;
		if (tenderType.equals(tenderCreditCard)){
			
		pay = DocumentsTrans.createPayment(trxName, inv, value, bankAccountId, tenderType
				,CreditCardType, CreditCardNumber,newCreditCardVV, CreditCardExpMM, newCreditCardExpYY, A_Name
				,null,null,null,null, writeOffAmt, discountAmt, orgId, true );
		paymentId = pay.getC_Payment_ID();
		}
		else if(tenderType.equals(tenderCheck)){
			pay = DocumentsTrans.createPayment(trxName, inv, value, bankAccountId, tenderType
					,null,null,null,0,0,A_Name, routingNo, accountNo, checkNo, micr, writeOffAmt, discountAmt
					, orgId, true);
			paymentId = pay.getC_Payment_ID();
		}
		else{
			pay = DocumentsTrans.createPayment(trxName, inv, value, bankAccountId, tenderType
					,null,null,null,0,0,null,null,null,null,null, writeOffAmt, discountAmt
					, orgId, true);
			paymentId = pay.getC_Payment_ID();
			
		}
		if (pay == null || paymentId <= 0){
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+".createPayment - Error No Create Payment :: Invoice:"+inv.getC_Invoice_ID()+" - " + inv.getDescription());
			return -1;
		}
		return paymentId;
	}//createPayment

	/**
	 * crea las comisiones de los mecanicos - 
	 * create mechanic commission line
	 * @param trxName
	 * @param woLineId
	 * @return
	 */
	public static Boolean calculateCommisionProcess(String trxName, MSMJWorkOrderLine woLine, Integer invLineId){
		try {
			LinkedList<MSMJWoMechanicAssigned> listMec = DataQueries.getMechanicList(woLine.getsmj_workOrderLine_ID());
			Iterator<MSMJWoMechanicAssigned> it = listMec.iterator();
			while (it.hasNext()){
				MSMJWoMechanicAssigned wm = it.next();
				MSMJMechanicCommission mc = new MSMJMechanicCommission(Env.getCtx(), 0, trxName);
				BigDecimal appliedfee = calculateFee(woLine.getM_Product_ID(), wm.getC_BPartner_ID());
				mc.setC_BPartner_ID(wm.getC_BPartner_ID());
				mc.setM_Product_ID(woLine.getM_Product_ID());
				mc.setR_Request_ID(woLine.getR_Request_ID());
				mc.setsmj_workOrderLine_ID(woLine.getsmj_workOrderLine_ID());
				mc.setQty(woLine.getQty());
				mc.setappliedfee(appliedfee);
				BigDecimal total = (woLine.gettotal().multiply(appliedfee)).divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_DOWN);
				mc.settotal(total);
				mc.setC_InvoiceLine_ID(invLineId);
				mc.settotalinvoiced(woLine.gettotal());
				mc.setdatecreated(Env.getContextAsDate(Env.getCtx(), "#Date"));
				Boolean ok = mc.save();
				if(!ok){
					return false;
				}
			}//while (it.hasNext())
			if(woLine.issmj_iswarranty() || woLine.getsmj_workorderline_parent_ID() > 0){
				LinkedList<MSMJMechanicCommission> listGar = DataQueries.getMechanicCommisionList(woLine.getsmj_workorderline_parent_ID());
				Iterator<MSMJMechanicCommission> itg = listGar.iterator();
				while (itg.hasNext()){
					MSMJMechanicCommission gm = itg.next();
					MSMJMechanicCommission mc = new MSMJMechanicCommission(Env.getCtx(), 0, trxName); 
					mc.setC_BPartner_ID(gm.getC_BPartner_ID());
					mc.setM_Product_ID(gm.getM_Product_ID());
					mc.setR_Request_ID(gm.getR_Request_ID());
					mc.setsmj_workOrderLine_ID(gm.getsmj_workOrderLine_ID());
					mc.setQty(gm.getQty());
					mc.setappliedfee(gm.getappliedfee().multiply(new BigDecimal(-1)));
					mc.settotal(gm.gettotal().multiply(new BigDecimal(-1)));
					mc.setC_InvoiceLine_ID(invLineId);
					mc.settotalinvoiced(woLine.gettotal());
					mc.setdatecreated(Env.getContextAsDate(Env.getCtx(), "#Date"));
					Boolean ok = mc.save();
					if(!ok){
						return false;
					}
				}//while (itg.hasNext())
			}//if(woLine.issmj_iswarranty())
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+ ".workOrderProcess - ERROR: " + e.getMessage(), e);
			return false;
		}
		return true;
	}//workOrderProcess
	
	/**
	 * calcula la comision del mecanico - 
	 * calculate mechanic commission
	 * @param productID
	 * @param partnerID
	 * @return
	 */
	private static BigDecimal calculateFee(Integer productID, Integer partnerID){
		BigDecimal appliedfee = Env.ZERO;
		try {
			MProduct p = MProduct.get(Env.getCtx(), productID);
			MBPartner b = MBPartner.get(Env.getCtx(), partnerID);  
			Boolean isCommission = p.get_ValueAsBoolean("smj_iscommission");
			Boolean isFixed = p.get_ValueAsBoolean("smj_isfixed");
			Boolean isPartnerCom = b.get_ValueAsBoolean("smj_iscommission");
			// si servicio no comisiona
			if(!isCommission){
				return Env.ZERO;
			}
			//si tercero no comision
			else if(!isPartnerCom){
				return Env.ZERO;
			}
			//si servicio comision fija
			else if (isFixed){
				appliedfee = new BigDecimal(p.get_ValueAsString("smj_valuemeccommision"));
				return appliedfee;
			}
			//si tercero comision variable
			else{
				appliedfee = new BigDecimal(b.get_ValueAsString("smj_commissionvalue"));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName()+ ".calculateFee - ERROR: " + e.getMessage(), e);
		}//try/catch
		return appliedfee;
	}//calculateFee

	/**
	 * actualiza la factura creada en las solicitudes
	 * @param trxName
	 * @param workOrder
	 * @param invId
	 * @return Boolean
	 */
	private static Boolean updateInvoiceWO(String trxName, String workOrder, Integer invId){
		Boolean flag = true;
		try {
			String[] wos = workOrder.split(";");
			for(int i=0; i<wos.length;i++){
				String woDoc = wos[i];
				Integer reqId = DataQueries.getRequestIdByDocument(trxName, woDoc);
				if (reqId > 0){
					flag = DataQueries.updateRequestInvoice(trxName, reqId, invId);
				}
			}
		} catch (Exception e) {
			flag = false;
			log.log(Level.SEVERE, CompleteSaleWeb.class.getName() + ".updateInvoiceWO - ERROR: " + e.getMessage(),e);
		}
		return flag;
	}//updateInvoiceWO
	
}//CompleteSaleWeb
