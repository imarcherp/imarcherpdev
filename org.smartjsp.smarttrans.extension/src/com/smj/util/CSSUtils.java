package com.smj.util;

/**
 * 
 * @author Dany Diaz - SmartJSP (http://www.smartjsp.com/)
 *
 */
public final class CSSUtils {

	public static String containerFluid(String currentStyle) {
		StringBuilder style = new StringBuilder(validateStyle(currentStyle))
				.append("margin-right: auto;")
				.append("margin-left: auto;")
				.append("padding-left: 15px;")
				.append("padding-right: 15px;")
				.append("height: 100%;")
				.append("width: 100%;");
		
		return style.toString();
	}
	
	public static String row(String currentStyle) {
		StringBuilder style = new StringBuilder(validateStyle(currentStyle))
				.append("margin-left: -15px;")
				.append("margin-right: -15px;");
		
		return style.toString();
	}
	
	public static String col(String currentStyle, int colspan) {	
		if (colspan < 1) colspan = 1;			
		if (colspan > 12) colspan = 12;
		
		Double por = colspan * 8.33333333;
		
		StringBuilder style = new StringBuilder(validateStyle(currentStyle))
				.append("position: relative;")
				.append("min-height: 1px;")
				.append("padding-left: 15px;")
				.append("padding-right: 15px;")
				.append("float: left;")
				.append("height: 100%;")
				.append("width: " + por + "%;");
		
		return style.toString();
	}
	
	public static String textCenter(String currentStyle) {
		String style = validateStyle(currentStyle);		
		style += "text-align: center;";
		
		return style.toString();
	}
	
	public static String textRight(String currentStyle) {
		String style = validateStyle(currentStyle);		
		style += "text-align: right;";
		
		return style.toString();
	}
	
	public static String textH1(String currentStyle) {
		StringBuilder style = new StringBuilder(validateStyle(currentStyle))
				.append("font-family: inherit;")
				.append("font-weight: 500;")
				.append("line-height: 1.1;")
				.append("color: inherit;")
				.append("margin-top: 20px;")
				.append("margin-bottom: 10px;")
				.append("font-size: 36px;");
		
		return style.toString();
	}
	
	public static String textH2(String currentStyle) {
		StringBuilder style = new StringBuilder(validateStyle(currentStyle))
				.append("font-family: inherit;")
				.append("font-weight: 500;")
				.append("line-height: 1.1;")
				.append("color: inherit;")
				.append("margin-top: 20px;")
				.append("margin-bottom: 10px;")
				.append("font-size: 25px;");
		
		return style.toString();
	}
	
	private static String validateStyle(String style) {
		if (style == null)
			return "";
		
		return style;
	}
}
