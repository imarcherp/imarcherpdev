package com.smj.util;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.util.DataQueriesTrans;
import com.smj.util.ERPPosInterceptorTrans;

public class validateInOut {
	
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	/**
	 * valida la cantidad recepcionada no sea mayor a la cantidad ordenada - 
	 * validates receptioned amount not greater than the amount ordered
	 * @param trxName
	 * @param ioId
	 * @return String
	 */
	public static String validateQtyEntered(String trxName, Integer ioId){
		try {
			LinkedList<MInOutLine> lines = DataQueriesTrans.getInOutLines(ioId, trxName);
			Iterator<MInOutLine> itl = lines.iterator();
			while (itl.hasNext()){
				MInOutLine line = itl.next();
				Integer productId = line.getM_Product_ID();
				Integer orderLineId = line.getC_OrderLine_ID();
				BigDecimal totalInOuts = DataQueriesTrans.getQtyTotalInOuts(trxName, productId, ioId,0);
				if (orderLineId == null || orderLineId <= 0){
					return "";
				}
				MOrderLine oLine = new MOrderLine(Env.getCtx(), orderLineId, null);
				BigDecimal qtyOrdered = oLine.getQtyEntered();
				if (totalInOuts.compareTo(qtyOrdered) > 0){
					//TODO mensaje....
//					return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
					log.log(Level.SEVERE, "La cantidad Ingresada Excede la cantidad Ordenada:: "+qtyOrdered	);
					return "1.La cantidad Ingresada Excede la cantidad Ordenada: "+qtyOrdered;
				}
			}
		} catch (Exception e) {
			log.severe("ERROR: "+ERPPosInterceptorTrans.class.getName()+".validateQtyEntered :: "+e);
		}//try / Catch

		return "";
	}//validateQtyEntered
	
	/**
	 * valida la cantidad recepcionada no sea mayor a la capacidad del tanque
	 * validates receptioned amount not greater than the tank capacity
	 * @param trxName
	 * @param ioId
	 * @return
	 */
	public static String validateFuelTank(String trxName, Integer ioId){
//		System.out.println(".......................... INT validateFuelTank ....................");
		try {
//			MOrder order = new MOrder(Env.getCtx(), orderId, null);
			MInOut io = new MInOut(Env.getCtx(), ioId, trxName);
			Integer warehouseId = io.getM_Warehouse_ID();
			MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
			Boolean isFuel = warehouse.get_ValueAsBoolean("smj_isfueltank");
//			System.out.println("wareh.."+warehouseId+"..istank...."+isFuel);
			if (isFuel){
				BigDecimal capacity = new BigDecimal(warehouse.get_ValueAsString("smj_capacitytank")); 
//				if (capacity == null)
//					capacity = Env.ZERO;
				LinkedList<MInOutLine> lines = DataQueriesTrans.getInOutLines(ioId, trxName);
				Iterator<MInOutLine> itl = lines.iterator();
				
				while (itl.hasNext()){
					MInOutLine line = itl.next();
					Integer productId = line.getM_Product_ID();
					Integer orderLineId = line.getC_OrderLine_ID();
					BigDecimal qty = line.getQtyEntered();
//					System.out.println("prod:"+productId+"..oline.."+orderLineId+"..qty.."+qty);
//					Integer orderId = DataQueriesTrans.getOrderId(trxName, orderLineId);
					BigDecimal tankTotal = DataQueriesTrans.getQtyWarehouse(trxName, productId, warehouseId);
					BigDecimal total = Env.ZERO;
//					BigDecimal total = tankTotal.add(qty);
//					if (total.compareTo(capacity) > 0){
////						return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
//						//TODO mensaje....
//						return "1.La cantidad Ingresada Excede la Capacidad del Tanque, actual: "+tankTotal;
//					}
					BigDecimal totalLines = DataQueriesTrans.getQtyTotalInOuts(trxName, productId, ioId,0);
//					System.out.println("total lines..."+totalLines+"..tank..."+tankTotal);
					total = tankTotal.add(totalLines);
//					System.out.println("cap..."+capacity+"...tankTot.."+tankTotal+"...tot.."+total);
					if (total.compareTo(capacity) > 0){
//						return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
						//TODO mensaje....
						return "La cantidad Ingresada Excede la Capacidad del Tanque, actual:  "+tankTotal;
					}
				}//while
				
			}//if isFuel
		} catch (Exception e) {
			log.severe("ERROR: "+ERPPosInterceptorTrans.class.getName()+".validateFuelTank :: "+e);
		}//try / Catch

		return "";
	}//validateFuelTank
}//validateInOut
