package com.smj.util;

import org.adempiere.webui.apps.AEnv;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.model.MQuery;
import org.smj.util.OpenWindow;

public class ShowWindowTrans {

	/**
	 * abre la factura creada - open created invoice
	 * @param invoiceId
	 * @return boolean
	 */
	public static boolean openInvoiceCustomer(Integer invoiceId){
		  ///abre una factura
//		  System.out.println("abrir factura cliente ...."+invoiceId);
		  String whereString = " C_Invoice_ID= "+invoiceId+" "; 
		  MQuery query = new MQuery("C_Invoice"); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = 1000031; // invoice customer window is 167
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openInvoiceCustomer
	
	/**
	 * abre la factura creada - open created invoice
	 * @param invoiceId
	 * @return boolean
	 */
	public static boolean openInvoiceVendor(Integer Record_ID){
		  int AD_Window_ID = 1000019; // The id invoice-vendor 183.		  
		  return openWindowAsync(AD_Window_ID, MInvoice.Table_Name, Record_ID); 
	}//openInvoiceCustomer
	
	/**
	 * abre la ventana con el pago creado - open created payment window
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openPayment(Integer elementId){
		  ///abre una factura
//		  System.out.println("abrir fago ...."+elementId);
		  String whereString = " C_Payment_ID= "+elementId+" "; 
		  MQuery query = new MQuery("C_Payment"); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = 195; //"Payment";"Process Payments and Receipts" 195
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openPayment
	
	/**
	 * Abre venta de tercero punto de venta Web - 
	 * open Partner Web POS window
	 * @param elementId
	 * @return
	 */
	public static boolean openPartnerWebPOS(Integer elementId){
		  ///abre una factura
//		  System.out.println("abrir Partner Web POS ...."+elementId);
		  String whereString = " C_BPartner_ID= "+elementId+" "; 
		  MQuery query = new MQuery("C_BPartner"); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = 1000028;
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openPartnerWebPOS
	
	/**
	 * abre la orden de venta creada - open created sales order
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openSalesOrder(Integer Record_ID){ 
		  int AD_Window_ID = 1000029; // sales order 143
		  return openWindowAsync(AD_Window_ID, MOrder.Table_Name, Record_ID);
	}//openSalesOrder
	
	/**
	 * abre la orden de compra creada - open created purchase order
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openPurchaseOrder(Integer elementId){
		  ///abre una factura
//		  System.out.println("abrir orden compra ...."+elementId);
		  String whereString = " C_Order_ID= "+elementId+" "; 
		  MQuery query = new MQuery("C_Order"); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = 1000016; // pruchase order 181 
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openPurchaseOrder


	/**
	 * abre la orden de trabajo - open request 
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openRequestOrder(Integer elementId){
		  
		  String whereString = " R_Request_ID= "+elementId+" "; 
		  MQuery query = new MQuery("R_Request"); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = 1000044; //
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openPurchaseOrder
	
	
	
	
	/**
	 * metodo generico para abrir ventanas - 
	 * generic method to open windows
	 * @param windowId
	 * @param table
	 * @param record
	 * @return
	 */
	public static boolean openWindow(Integer windowId, String table, Integer record){
		  ///abre una factura
//		  System.out.println("abrir orden compra ...."+elementId);
		  String whereString = " "+table+"_ID= "+record+" "; 
		  MQuery query = new MQuery(table); 
		  query.addRestriction(whereString); 
		  int AD_Window_ID = windowId; 
		  AEnv.zoom(AD_Window_ID, query);
		  return true; 
	}//openWindow

	/**
	 * metodo generico para abrir ventanas de forma asincronica en idempiere - 
	 * generic method to open windows
	 * @param AD_Window_ID
	 * @param tableName
	 * @param records
	 * @return
	 */
	public static boolean openWindowAsync(Integer AD_Window_ID, String tableName, Integer... records) {
		///abre una close cash de forma asyncronica
		OpenWindow ow = new OpenWindow();
		ow.AD_Window_ID = AD_Window_ID;
		ow.tableName = tableName;
		ow.records = records;
		AEnv.executeAsyncDesktopTask(ow);
		return true; 
	}//openWindow
}//ShowWindow
