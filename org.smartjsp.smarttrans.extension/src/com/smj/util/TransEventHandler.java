package com.smj.util;

import java.util.logging.Level;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventManager;
import org.adempiere.base.event.IEventTopics;
import org.adempiere.base.event.LoginEventData;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.osgi.service.event.Event;

public class TransEventHandler extends AbstractEventHandler {

	protected CLogger log = CLogger.getCLogger(getClass());
	
	@Override
	protected void initialize() {
		registerEvent(IEventTopics.AFTER_LOGIN);
	}

	@Override
	protected void doHandleEvent(Event event) {
		String type = event.getTopic();

		if (type.equals(IEventTopics.AFTER_LOGIN)) {
			LoginEventData loginData = (LoginEventData) event.getProperty(IEventManager.EVENT_DATA);
			boolean useTransport = MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, loginData.getAD_Client_ID(), loginData.getAD_Org_ID());
			Env.setContext(Env.getCtx(), "#SMJ_USE_TRANSPORT", useTransport);
			log.log(Level.INFO,"SMJ_USE_TRANSPORT -> " + useTransport);
			return;
		}	
	}

}
