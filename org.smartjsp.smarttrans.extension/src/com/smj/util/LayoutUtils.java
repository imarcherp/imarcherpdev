/**
 * 
 */
package com.smj.util;

import org.adempiere.webui.component.Window;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Vbox;

/**
 * @author Dany Diaz - SmartJSP (http://www.smartjsp.com/)
 *
 */
public final class LayoutUtils {

	public static Window newRow(Component parent) {
		Window Window = new Window();
		
		if (parent != null)
			Window.setParent(parent);
		
		Window.setStyle(CSSUtils.row(""));
		return Window;
	}
	
	public static Window newCol(Component parent, int colspan) {
		Window Window = new Window();
		
		if (parent != null)
			Window.setParent(parent);
		
		Window.setStyle(CSSUtils.col("", colspan));
		return Window;
	}
	
	public static Vbox newContainer() {
		Vbox Window = new Vbox();
		Window.setStyle(CSSUtils.containerFluid(""));
		return Window;
	}
}
