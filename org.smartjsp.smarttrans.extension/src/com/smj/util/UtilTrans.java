package com.smj.util;

import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MPriceListVersion;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.model.MSMJTires;

/**
 * @version <li>SmartJSP: UtilTrans, 2013/02/11
 *          <ul TYPE ="circle">
 *          <li>Clase para utilidades generales 
 *          <li>Class to general utilities 
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class UtilTrans {

	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	/**
	 * redondea un BigDeciamal a centena - 
	 *  round BigDeciamal a hundred
	 * @param val
	 * @return
	 */
	public static BigDecimal roundingHundreds(BigDecimal val){
		if (val == null)
			return Env.ZERO;
		BigDecimal rounded = Env.ZERO;
		try	{
			//elimina decimales
			BigDecimal xval = val.setScale(0,BigDecimal.ROUND_DOWN);
			String dato = xval.toString();
			//valida valor mayor a 100
			if (xval.compareTo(new BigDecimal(100))<=0){
				return new BigDecimal(100);
			}
			//obtiene los dos ultimos digitos
			String two = dato.substring(dato.length()-2);
			BigDecimal cent = Env.ZERO;
			//obtinene los digitos para completar la centena
			if (!two.equals("00")){
				cent = new BigDecimal(100).subtract(new BigDecimal(two));
			}
			//valor redondeado a centena
			rounded = xval.add(cent);
		}catch (Exception e) {
			log.log(Level.SEVERE, UtilTrans.class.getClass().getName() + ".roundingHundreds - ERROR: " + e.getMessage(), e);
		}
		return rounded;
	}//roundingHundreds
	

	/**
	 * calcula el precio de venta - calculate sales price
	 * @param price
	 * @param util
	 * @param tax
	 * @return BigDecimal
	 */
	public static BigDecimal calculateSalesPrice(BigDecimal price, BigDecimal util, BigDecimal tax){
		BigDecimal sales = Env.ZERO;
		try{
			if(util == null)
				util = Env.ZERO;
			BigDecimal valUtil = price.multiply((util.divide(new BigDecimal(100))));
			BigDecimal newPrice = price.add(valUtil);
			BigDecimal valTax = newPrice.multiply((tax.divide(new BigDecimal(100))));
			sales = newPrice.add(valTax);
		}catch (Exception e) {
			log.log(Level.SEVERE,UtilTrans.class.getClass().getName() + ".calculateSalesPrice - ERROR: " + e.getMessage(),e);
		}
		
		BigDecimal salesPrice = roundingHundreds(sales);
		return salesPrice;
	}//calculateSalesPrice
	
	/**
	 * crea una version de lista de precios - 
	 * create pricelist version
	 * @param name
	 * @param priceList
	 * @return
	 */
	public static Integer createPriceListVersion(String name, Integer priceList){
		Integer code = 0;
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		MPriceListVersion mplv = new MPriceListVersion(Env.getCtx(), 0, trx.getTrxName());
		mplv.setM_PriceList_ID(priceList);
		mplv.setName(name);
		mplv.setDescription(name);
		mplv.setM_DiscountSchema_ID(1000000);
		mplv.setValidFrom(Env.getContextAsDate(Env.getCtx(), "#Date"));
		Boolean ok = mplv.save();
		if(ok){
			trx.commit();
			code = mplv.getM_PriceList_Version_ID();
		}else{
			trx.rollback();
			log.log(Level.SEVERE,UtilTrans.class.getName() + ".createPriceListVersion - ERROR: " + name);
		}
		trx.close();
		return code;
	}//createPriceListVersion
	
	/**
	 * regresa un numero aleatorio entre 10000 y 99999 - 
	 * return randon number between 10000 and 99999
	 * @return
	 */
	public static int getRdmInteger(){
		Random r = new Random();
		int rdm = r.nextInt(99999); 
		if (rdm < 10000)
			rdm = rdm + 10000;
		return rdm; 
	}//getRdmInteger
	
	/**
	 * regresa el calculo de Kms para la llanta - 
	 * returns calculate kms for tire
	 * @param vehicle
	 * @param tire
	 * @return BigDecimal
	 */
	public static BigDecimal calculateKms(MSMJTires tire, BigDecimal oldVhKm, BigDecimal newVhKm){
		BigDecimal kmsTotal = Env.ZERO;
		try {
			if (newVhKm == null){
				return null;
			}
			if (oldVhKm == null){
				oldVhKm = newVhKm;
			}
			BigDecimal kmsDif = newVhKm.subtract(oldVhKm);
			BigDecimal kmTire = tire.getkms();
			if (kmTire == null){
				kmsTotal = kmsDif;
			}else{
				kmsTotal = kmTire.add(kmsDif);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, UtilTrans.class.getCanonicalName()+ ".calculateKms - ERROR: " + e.getMessage(), e);
		}
		return kmsTotal;
	}//calculateKms
	
	/**
	 * valida si el rol esta en la lista de los permitidos - 
	 * validate if role is in permitted role list 
	 * @param logRol
	 * @param rols
	 * @return
	 */
	public static Boolean validateRol(Integer logRol, String rols){
		Boolean ok = false;
		try {
			if (rols == null || rols.length()<=0){
				return true;
			}
			String[] lRol = rols.split(";");
			for (int c=0;c<lRol.length;c++){
				Integer rol = Integer.parseInt(lRol[c]);
				if (rol.equals(logRol)){
					ok = true;
				}
			} // for
		} catch (Exception e) {
			log.log(Level.SEVERE,UtilTrans.class.getCanonicalName() + ".validateRol - ERROR: " + e.getMessage(),e);
		}
//		System.out.println(UtilTrans.class.getCanonicalName()+".validateRol ...ok.."+ok);
		return ok;
	}//validateRol

	/**
	 * calcula el valor el porcentaje de comision - 
	 * Calculate Brokerage Percent
	 * @param invoiceValue
	 * @param chargeValue
	 * @return
	 */
	public static BigDecimal calculateBrokeragePercent (BigDecimal invoiceValue, BigDecimal chargeValue){
		BigDecimal percent = Env.ZERO;
		try {
			BigDecimal div = invoiceValue.divide(chargeValue, 4, BigDecimal.ROUND_DOWN);
			percent = (div.subtract(Env.ONE)).multiply(Env.ONEHUNDRED);
		} catch (Exception e) {
			log.log(Level.SEVERE,
					UtilTrans.class.getName() + ".calculateBrokeragePercent - ERROR: " + e.getMessage(),
					e);
		}
		return percent;
	}//calculateBrokeragePercent
	
}//UtilTrans
