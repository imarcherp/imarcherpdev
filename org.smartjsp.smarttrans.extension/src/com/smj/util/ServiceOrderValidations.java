package com.smj.util;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.util.UtilTrans;

public class ServiceOrderValidations {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	
	/**
	 * valida si el rol puede realizar la accion - 
	 * validates if role can do the action
	 * @param po
	 * @return
	 */
	public static String validateServiceOrder(Integer logRol, Integer status){
		//validar estado rol
//		String trxName = po.get_TrxName();
//		System.out.println(ServiceOrderValidations.class.getName()+".validateServiceOrder ...."+logRol);
		Integer stateReq = Integer.parseInt(MSysConfig.getValue("SMJ-INFOREQUEST",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer stateAp = Integer.parseInt(MSysConfig.getValue("SMJ-INFOAPROVED",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer statevhs = Integer.parseInt(MSysConfig.getValue("SMJ-INFOVHSELECTED",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer statevha = Integer.parseInt(MSysConfig.getValue("SMJ-INFOVHAPROVED",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer statetrs = Integer.parseInt(MSysConfig.getValue("SMJ-INFOTRAVELSTART",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer statetre = Integer.parseInt(MSysConfig.getValue("SMJ-INFOTRAVELEND",Env.getAD_Client_ID(Env.getCtx())).trim());
		String rols = "";
//		System.out.println(ServiceOrderValidations.class.getName()+".validateServiceOrder -staus.."+status+"...req.."+stateReq);
		if(status == null || status <= 0){
			return "";
		}
		if(status.equals(stateReq)){
			return "";
		}else if(status.equals(stateAp)){
			rols = MSysConfig.getValue("SMJ-INFAPAPROVED",Env.getAD_Client_ID(Env.getCtx())).trim();
		}else if(status.equals(statevhs)){
			rols = MSysConfig.getValue("SMJ-INFAPVHSELECTED",Env.getAD_Client_ID(Env.getCtx())).trim();
		}else if(status.equals(statevha)){
			rols = MSysConfig.getValue("SMJ-INFAPVHAPROVED",Env.getAD_Client_ID(Env.getCtx())).trim();
		}else if(status.equals(statetrs)){
			rols = MSysConfig.getValue("SMJ-INFAPTRAVELSTART",Env.getAD_Client_ID(Env.getCtx())).trim();
		}else if(status.equals(statetre)){
			rols = MSysConfig.getValue("SMJ-INFAPTRAVELEND",Env.getAD_Client_ID(Env.getCtx())).trim();
		}
		if (!UtilTrans.validateRol(logRol, rols)){
			return Msg.translate(Env.getCtx(), "SMJMSGActionNoRole");
		}else{
			return "";
		}//if / else
	}//validateServiceOrder

	
	/**
	 * valida si se pude crear adelantos or charges
	 * validate if can create  the advance or charges
	 * @param po
	 * @return
	 */
	public static String validateCreateAdvancesCharges(Integer logRol){
		String msg ="";
		String rols = MSysConfig.getValue("SMJ-INFVHADVANCE",Env.getAD_Client_ID(Env.getCtx())).trim();
		// valida si el rol actual puede crear
		Boolean rolPer = UtilTrans.validateRol(logRol, rols); 
		
		if (!rolPer){   // no puede crear 
			return Msg.translate(Env.getCtx(), "SMJMSGActionNoRole");
		}//if
		
		return msg;
	}//validateCreateAdvancesCharges
	
	
	/**
	 * valida si el rol puede aprobar adelantos or charges
	 * validate if the role can approve  the advance or charges
	 * @param po
	 * @return
	 */
	public static String validateApproveAdvancesCharges(Integer logRol,boolean oldAp, boolean newAp){
		String msg ="";
		String rols = MSysConfig.getValue("SMJ-ITROLAPROVATE",Env.getAD_Client_ID(Env.getCtx())).trim();
		// valida si el rol actual puede aprobar
		Boolean rolAP = UtilTrans.validateRol(logRol, rols);
		
		if (!rolAP  ){   // no puede aprobar
			if ((oldAp != newAp ))  {
				return Msg.translate(Env.getCtx(), "SMJMSGActionNoRole");	
			}
			
		}//if
		
		return msg;
	}//validateApproveAdvancesCharges
	
	
	
	
	/**
	 * regresa la suma de los valores de los adelantos - 
	 * returns the sum of the values of the advances
	 * @param requestID
	 * @return
	 */
	public static BigDecimal getTotalAdvancesByRequest(String trxName, Integer requestID) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(smj_value) AS total, r_request_ID FROM SMJ_ControlAdvances ");
		sql.append(" WHERE isActive = 'Y' AND isApproved = 'Y' AND smj_charges_ID IS NULL ");
		sql.append(" AND r_request_ID = "+requestID+" GROUP by r_request_ID ");
		// System.out.println(ServiceOrderValidations.class.getName()+".getTotalAdvancesByRequest SQL::"+sql.toString());

		BigDecimal value = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				value = rs.getBigDecimal("total");
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, ServiceOrderValidations.class.getName()+".getTotalAdvancesByRequest - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getTotalAdvancesByRequest
	
	/**
	 * regresa la suma de los valores de los adelantos - 
	 * returns the sum of the values of the advances
	 * @param requestID
	 * @return
	 */
	public static BigDecimal getTotalChagesByRequest(String trxName, Integer requestID) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(smj_value) AS total, r_request_ID FROM smj_charges ");
		sql.append(" WHERE isActive = 'Y' AND isApproved = 'Y' ");
		sql.append(" AND r_request_ID = "+requestID+" GROUP by r_request_ID ");
//		System.out.println(ServiceOrderValidations.class.getName()+".getTotalChagesByRequest SQL::"+sql.toString());

		BigDecimal value = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				value = rs.getBigDecimal("total");
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, ServiceOrderValidations.class.getName()+".getTotalChagesByRequest - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getTotalChagesByRequest
	
	/**
	 * actualiza el porcentaje de comision - 
	 * update smj_brokeragepercent
	 * @param trxName
	 * @param requestId
	 * @param total
	 * @return
	 */
	public static Boolean updateBrokeragePercent(String trxName,Integer requestId, BigDecimal total) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE R_Request SET smj_brokeragepercent = "+total+" ");
		sql.append(" WHERE R_Request_ID= "+requestId+" ");
//		System.out.println(ServiceOrderValidations.class.getName()+".updateBrokeragePercent SQL.."+sql.toString());
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs>0?true:false;
		}catch (Exception e)	{
			log.log (Level.SEVERE, ServiceOrderValidations.class.getName()+"updateBrokeragePercent - Error update Request" + requestId, e);
		}
		finally	{
			DB.close(pstmt);
			rs = null; pstmt = null;
		}
		
		return flag;
	}//updateBrokeragePercent
	
	
	/**
	 * regresa si todos los cargos para la solicitud estan aprobados 
	 * @param requestID
	 * @return
	 */
	public static boolean areApprovedChargesByRequest(String trxName, Integer requestID) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT isapproved FROM smj_charges ");
		sql.append(" WHERE isActive = 'Y' AND isApproved = 'N' ");
		sql.append(" AND r_request_ID = "+requestID+" ");

		boolean value = true;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				value = rs.getBoolean("isapproved");
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, ServiceOrderValidations.class.getName()+".areApprovedChargesByRequest - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// areApprovedChargesByRequest
	

	/**
	 * regresa si todos los anticipos para la solicitud estan aprobados 
	 * @param requestID
	 * @return
	 */
	public static boolean areApprovedAdvancesByRequest(String trxName, Integer requestID) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT isapproved FROM smj_controladvances ");
		sql.append(" WHERE isActive = 'Y' AND isApproved = 'N' ");
		sql.append(" AND r_request_ID = "+requestID+" ");

		boolean value = true;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				value = rs.getBoolean("isapproved");
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, ServiceOrderValidations.class.getName()+".areApprovedAdvancesByRequest - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// areApprovedAdvancesByRequest
	

	
	
	
}//ServiceOrderValidations
