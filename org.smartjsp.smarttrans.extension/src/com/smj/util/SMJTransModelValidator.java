package com.smj.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.callout.InOutCallout;

public class SMJTransModelValidator implements ModelValidator {
	
	private static final String CONSIGNMENT_DATA = "ConsignmentData";
	
	private int p_AD_Client_ID = 0;
	protected CLogger log = CLogger.getCLogger(getClass());

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		if (client != null)
			p_AD_Client_ID = client.getAD_Client_ID();

		// Model
		engine.addModelChange(MInOutLine.Table_Name, this);
		engine.addModelChange(MInventoryLine.Table_Name, this);

		// Document
		engine.addDocValidate(MOrder.Table_Name, this);
		engine.addDocValidate(MInOut.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		return p_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.log(Level.INFO, "SMJTransModelValidator Login!!");
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {

		if (po.get_TableName().equals(MInOutLine.Table_Name) && type == ModelValidator.TYPE_AFTER_NEW) {
			MInOutLine ioLine = (MInOutLine) po;
			
			// Update AttributeSetInstance
			updateAttributeSetInstanceFromCtx(po.getCtx(), ioLine, po.get_TrxName());

			// Validate it has M_AttributeSetInstance
			if (ioLine.getM_AttributeSetInstance_ID() < 1 && ioLine.getMovementQty().signum() == 1 && ioLine
					.getM_InOut().getC_DocType().getDocBaseType().equals(MDocType.DOCBASETYPE_MaterialReceipt)) {
				// Create new MAttributeSetInstance and it is assigned to IO
				// Line
				MAttributeSetInstance attribute = new MAttributeSetInstance(Env.getCtx(), 0, po.get_TrxName());
				attribute.setAD_Org_ID(ioLine.getAD_Org_ID());
				attribute.setM_AttributeSet_ID(ioLine.getM_Product().getM_AttributeSet_ID());
				attribute.saveEx();

				ioLine.setM_AttributeSetInstance_ID(attribute.get_ID());
				ioLine.saveEx();
			}
		} else if (po.get_TableName().equals(MInventoryLine.Table_Name) && type == ModelValidator.TYPE_AFTER_NEW) {
			MInventoryLine invLine = (MInventoryLine) po;

			// Create new MAttributeSetInstance and it is assigned to Inventory
			// Line
			MAttributeSetInstance attribute = new MAttributeSetInstance(Env.getCtx(), 0, po.get_TrxName());
			attribute.setAD_Org_ID(invLine.getAD_Org_ID());
			attribute.setM_AttributeSet_ID(invLine.getM_Product().getM_AttributeSet_ID());
			attribute.saveEx();

			invLine.setM_AttributeSetInstance_ID(attribute.get_ID());
			invLine.saveEx();
		}

		/*
		 * 2016-11-09 Seveter Request 1001172: The option to generate the
		 * attribute instance for the movement line by seveter request is
		 * eliminated.
		 */

		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {

		// When a order is voided and it has products in consignment
		if (po.get_TableName().equals(MOrder.Table_Name) && ((MOrder) po).isSOTrx()) {

			MOrder order = (MOrder) po;

			if (timing == TIMING_BEFORE_VOID) {

				String generalDesc = "Reversado";

				// Validate lines (If the product is from consignment)
				for (MOrderLine oLine : order.getLines()) {
					int M_Locator_ID = oLine.get_ValueAsInt("SMJ_Locator_ID");
					int SMJ_Provider_ID = oLine.get_ValueAsInt("SMJ_Provider_ID");

					if (SMJ_Provider_ID < 1 || M_Locator_ID < 1) {
						continue;
					}

					MLocator locator = MLocator.get(Env.getCtx(), M_Locator_ID);
					MWarehouse warehouse = MWarehouse.get(Env.getCtx(), locator.getM_Warehouse_ID(), po.get_TrxName());

					if (warehouse.get_ValueAsBoolean("SMJ_IsConsignment")) {

						String descPurchaseOrder = MSysConfig
								.getValue("SMJ-MSGDEFAULTDESCORDER", Env.getAD_Client_ID(Env.getCtx())).trim();
						int docPurchaseOrder = Integer.parseInt(
								MSysConfig.getValue("SMJ-DOCPURCHASEORDER", Env.getAD_Client_ID(Env.getCtx())).trim());
						int existentPO = DataQueriesTrans.getCurrentPurchaseOrderPOS(po.get_TrxName(),
								descPurchaseOrder, SMJ_Provider_ID, docPurchaseOrder,
								Env.getAD_Client_ID(Env.getCtx()));

						if (existentPO > 0) {
							int autoOrderLineId = DataQueriesTrans.getAutoOrderLine(existentPO, oLine.getM_Product_ID(),
									null, order.getDocumentNo());

							if (autoOrderLineId <= 0) {
								autoOrderLineId = DataQueriesTrans.getAutoOrderLine(existentPO, oLine.getM_Product_ID(),
										null, null);
							}

							if (autoOrderLineId > 0) {
								MOrderLine autoLine = new MOrderLine(Env.getCtx(), autoOrderLineId, po.get_TrxName());
								MOrder autoOrder = new MOrder(Env.getCtx(), autoLine.getC_Order_ID(), po.get_TrxName());
								if (autoOrder.getDocStatus().equals(DocAction.STATUS_Drafted)) {
									BigDecimal xQty = oLine.getQtyEntered();

									if (xQty.compareTo(Env.ZERO) < 0) {
										xQty = xQty.multiply(new BigDecimal(-1));
									}

									// Reverse Purchase Order Line
									BigDecimal newQty = autoLine.getQtyOrdered().subtract(xQty);
									autoLine.setQtyOrdered(newQty);
									autoLine.setQtyEntered(newQty);
									String autoDesc = autoLine.getDescription() + " - " + generalDesc;
									autoLine.setDescription(autoDesc);
									autoLine.saveEx();
								}
							}
						}
					}
				}
			} else if (timing == TIMING_AFTER_VOID) {

				// Recalculate Total
				order.calculateTaxTotal();

				List<MInOut> inOutList = DataQueriesTrans.getMaterialReceiptByDesc(po.getCtx(), order.getDocumentNo(),
						po.get_TrxName());

				// Revert documents generated in the sales process (Material
				// Receipt & Purchase Order)
				for (MInOut mInOut : inOutList) {
					mInOut.setDocAction(DocAction.ACTION_Reverse_Correct);
					if (!mInOut.processIt(DocAction.ACTION_Reverse_Correct)) {
						throw new AdempiereException(mInOut.getProcessMsg());
					}

					mInOut.saveEx();

					MOrder pOrder = new MOrder(Env.getCtx(), mInOut.getC_Order_ID(), po.get_TrxName());
					pOrder.setDocAction(DocAction.ACTION_Void);
					if (!pOrder.processIt(DocAction.ACTION_Void)) {
						throw new AdempiereException(pOrder.getProcessMsg());
					}

					pOrder.saveEx();
				}
			}

			return null;
		}
		
		if (po.get_Table_ID() == MInOut.Table_ID &&  
				(timing == TIMING_BEFORE_REVERSECORRECT || timing == TIMING_BEFORE_VOID)  && 
				DataQueriesTrans.validateInOutLinesLineNo(po.getCtx(), po.get_ID(), po.get_TrxName())) {
				
			MInOut inOut = (MInOut) po;
			MInOutLine[] ioLines = inOut.getLines(true);
			int lineNo = 0;
			for (MInOutLine mInOutLine : ioLines) {
				lineNo += 10;
				mInOutLine.setLine(lineNo);
				mInOutLine.saveEx();
			}
		}
		
		return null;
	}

	/**
	 * Keeps the same "AttributeSetInstance" sended from the WorkOrder
	 */
	private void updateAttributeSetInstanceFromCtx(Properties ctx, MInOutLine newIoLine, String trxName) throws AdempiereException {
		@SuppressWarnings("unchecked")		
		Map<Integer, List<MInOutLine>> inOutLinesByOLine = (Map<Integer, List<MInOutLine>>) ctx.get(CONSIGNMENT_DATA);
		
		if (inOutLinesByOLine != null && !inOutLinesByOLine.isEmpty() && inOutLinesByOLine.containsKey(newIoLine.getC_OrderLine_ID())) {						
			
			log.info("Updating AttributeSetInstance");
			
			List<MInOutLine> ioLines = inOutLinesByOLine.get(newIoLine.getC_OrderLine_ID());
			inOutLinesByOLine.remove(newIoLine.getC_OrderLine_ID());
			boolean first = true;
			
			for (MInOutLine ioLine: ioLines) {
				log.info("InOutLine ID found > " + ioLine.get_ID());
				
				MAttributeSetInstance attribute = new MAttributeSetInstance(ctx, ioLine.getM_AttributeSetInstance_ID(), trxName);
				
				log.info("AttributeSetInstance updated to > " + attribute.getDescription());
				
				if (first) {
					newIoLine.setM_AttributeSetInstance_ID(attribute.get_ID());
					newIoLine.setQtyEntered(ioLine.getQtyEntered().negate());
					newIoLine.setMovementQty(ioLine.getMovementQty().negate());
					newIoLine.saveEx();
				} else {
					MInOutLine inOutLine = new MInOutLine(newIoLine.getParent());
					inOutLine.setM_AttributeSetInstance_ID(attribute.get_ID());
					inOutLine.setM_Product_ID(newIoLine.getM_Product_ID());
					inOutLine.setQtyEntered(ioLine.getQtyEntered().negate());
					inOutLine.setMovementQty(ioLine.getMovementQty().negate());
					inOutLine.setM_Locator_ID(newIoLine.getM_Locator_ID());
					inOutLine.setC_OrderLine_ID(newIoLine.getC_OrderLine_ID());
					inOutLine.saveEx();
				}
				
				first = false;
			}

			// Remove to be sure.
			if (inOutLinesByOLine.isEmpty()) {
				ctx.remove(CONSIGNMENT_DATA);
			}
		}
	}
}
