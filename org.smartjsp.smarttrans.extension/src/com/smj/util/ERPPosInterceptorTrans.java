package com.smj.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.form.Allocation;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerProduct;
import org.compiere.model.MCost;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocation;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPayment;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRMALine;
import org.compiere.model.MRequest;
import org.compiere.model.MRequestType;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.osgi.service.event.Event;

import com.smj.model.MSMJCharges;
import com.smj.model.MSMJControlAdvances;
import com.smj.model.MSMJFuelIsland;
import com.smj.model.MSMJFuelLine;
import com.smj.model.MSMJFuelLineHistory;
import com.smj.model.MSMJFuelSales;
import com.smj.model.MSMJHistoryTire;
import com.smj.model.MSMJLoadOrder;
import com.smj.model.MSMJMechanicCommission;
import com.smj.model.MSMJRemittances;
import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJTires;
import com.smj.model.MSMJTmpPayment;
import com.smj.model.MSMJVehicle;
import com.smj.model.MSMJVehicleOwner;

/**
 * @version <li>SmartJSP: ERPPosInterceptorTrans, 2013/02/13
 *          <ul TYPE ="circle">
 *          <li>Clase interceptor para automatizacion de procesos
 *          <li>interceptor class for process automation
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ERPPosInterceptorTrans extends AbstractEventHandler {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	@Override
	protected void initialize() {
		// register for model change on C_Order
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MOrder.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MOrder.Table_Name);	
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE, MOrder.Table_Name);	
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MInOut.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MInOut.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MBPartner.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJServiceRateGroup.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJRemittances.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJRemittances.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJCharges.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJCharges.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_DELETE, MSMJCharges.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJControlAdvances.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJControlAdvances.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MRequest.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE, MRequest.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJTires.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJTires.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJFuelLine.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJFuelLine.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJVehicleOwner.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJVehicleOwner.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MSMJLoadOrder.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MSMJLoadOrder.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_DELETE, MInvoice.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MPayment.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_DELETE, MPayment.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_REVERSECORRECT, MPayment.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_VOID, MPayment.Table_Name);
		
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MProductPrice.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MProductPrice.Table_Name);
	}//initialize

	/**
	 * Para eventos del modelo (escritura a tablas) For Events model (write to
	 * tables)
	 */
	
	@Override
	protected void doHandleEvent(Event event) {
		int orgPos = Integer.parseInt(MSysConfig.getValue("SMJ_POSORGANIZATION",Env.getAD_Client_ID(Env.getCtx())).trim());
		String topic = event.getTopic();
		PO po = getPO(event);
		
		// if MSMJCharges.Table_Name
		if (po.get_TableName().equals(MSMJCharges.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW) || topic.equals(IEventTopics.PO_AFTER_DELETE)) ){			

			// actualiza el % de intermediacion cuando hay cambios en los cargos

			int reqId = po.get_ValueOldAsInt(MSMJCharges.COLUMNNAME_R_Request_ID);
			BigDecimal percent ;

			MRequest tRequest = new MRequest(po.getCtx(),reqId,po.get_TrxName() );
			BigDecimal totCharges = DataQueriesTrans.getTotalChargesByRequest(Integer.toString(reqId),po.get_TrxName());		
			BigDecimal invoiceValue = (BigDecimal)tRequest.get_Value("smj_invoicevalue");
			BigDecimal chargeValue = (BigDecimal) tRequest.get_Value("smj_vehiclecharge");
			if (invoiceValue == null || invoiceValue.compareTo(Env.ZERO)<=0){
				invoiceValue = new BigDecimal( 0);
			}

			if (chargeValue == null || chargeValue.compareTo(Env.ZERO)<=0){
				chargeValue = new BigDecimal( 0);
			}
			// actualiza % de interm ediacion luego de las validaciones ..

			percent = UtilTrans.calculateBrokeragePercent(invoiceValue, chargeValue.add(totCharges));
			tRequest.set_CustomColumn("smj_brokeragepercent" , percent);
			tRequest.saveEx();

			//

			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			//Boolean ap = po.get_ValueAsBoolean(MSMJCharges.COLUMNNAME_IsApproved);
			String msg = ServiceOrderValidations.validateCreateAdvancesCharges(logRol);
			if (msg.length() > 0 ){
				throw new AdempiereException(msg);
			}
			
			return;
		}//if MSMJTires.Table_Name

		if (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW)
				|| topic.equals(IEventTopics.PO_BEFORE_CHANGE)   ) 
		{
			if ((po.get_TableName()
					.substring(po.get_TableName().indexOf('_') + 1)
					.toUpperCase().trim().equalsIgnoreCase("ORDER")) 
					&& (po.getAD_Org_ID() == orgPos))   // just process orders for the POS ORGANIZATION (SMJ_POSORGANIZATION) Solicitud: 1000293 
			{
				String docStatus = po.get_ValueAsString("DocStatus");
				String docStatusOld = (String)po.get_ValueOld("DocStatus");
				Boolean isSales = po.get_ValueAsBoolean(MOrder.COLUMNNAME_IsSOTrx);
				Integer orderId = po.get_ValueAsInt("C_Order_ID");
				// validar si bodega consignacion
				Integer partnerId = po.get_ValueAsInt(MOrder.COLUMNNAME_C_BPartner_ID);
				Integer warehouseId = po.get_ValueAsInt(MOrder.COLUMNNAME_M_Warehouse_ID);
				MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
				Integer warehouseOwnerId = 0;
				Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
				String trxName = po.get_TrxName();
				if (isSales && docStatus.equals(DocAction.STATUS_Voided)){
					String docNo = DataQueriesTrans.getPaymentByOrder(trxName, orderId);
					if (docNo!= null && docNo.length()>0){	
						String desc = po.get_ValueAsString(MOrder.COLUMNNAME_Description)+" - Pago: "+docNo;
						DataQueriesTrans.updateOrderDescription(trxName, orderId, desc);
					}
				}//if (isSales && docStatus.equals(DocAction.STATUS_Voided))
				if (isConsigment && !isSales && warehouseId > 0 && partnerId > 0) {
					warehouseOwnerId = warehouse.get_ValueAsInt("C_BPartner_ID");
					if (!partnerId.equals(warehouseOwnerId)) {
						po.set_ValueOfColumn(MOrder.COLUMNNAME_C_BPartner_ID, warehouseOwnerId);
						throw new AdempiereUserError(Msg.getMsg(Env.getCtx(),"SMJMSGPartnerNOWarehouse"));
					}
				}//if (isConsigment && !isSales && warehouseId > 0 && partnerId > 0)
				if (docStatus.equals(DocAction.STATUS_Completed) && !isSales && (topic.equals(IEventTopics.PO_AFTER_CHANGE))
						&& !docStatusOld.equals(DocAction.STATUS_Completed)) 
				{
					//SE DESACTIVA ACTUALIZACION DE PRECIOS DESDE OC
					Integer ioId = DataQueriesTrans.getIoByOrder(trxName, orderId);
					if (ioId > 0) 
					{
						// aqui se incluye le fix de la solicitud 293, se desactiva creacion de precios para organizacion difentes a seveter
						Boolean flag = tableInOut(trxName, orderId, ioId, true);
						if (!flag) {
							throw new AdempiereException(Msg.translate(Env.getCtx(),"SMJMSGProcessError") + " - ERPPosInterceptorTrans: " + MOrder.Table_Name);
						}
					}
				}// if (docStatus.equals(DocAction.STATUS_Completed) && !isSales&& (topic.equals( IEventTopics.PO_AFTER_CHANGE))
				Boolean ap = po.get_ValueAsBoolean(MOrder.COLUMNNAME_IsApproved);
				String orderType = po.get_ValueAsString("OrderType");
				if (isSales && (topic.equals(IEventTopics.PO_AFTER_CHANGE)) && ap && orderType.contains("CHEQUE")) {

					if ((docStatus.equals(DocAction.STATUS_InProgress))) { // docStatus.equals(DocAction.STATUS_Completed)
						// ||
						Boolean com = completeOrderSales(trxName, po);
						if (!com) {
							throw new AdempiereException("Error al completar orden con cheque ...");
						}
					}
				}// if aprobar cheque....
				if (isSales && topic.equals(IEventTopics.PO_AFTER_CHANGE)
						&& docStatus.equals(DocAction.STATUS_NotApproved)) {
					MOrder order = new MOrder(Env.getCtx(), orderId,
							po.get_TrxName());
					String desc = order.getDescription();
					if (desc.contains("APROBADO")) {
						order.setDocStatus(DocAction.STATUS_Completed);
						order.setDocAction(DocAction.STATUS_Closed);
						order.setIsApproved(true);
						order.save();
					}
				}// cambia el estado de la orden aprobada
			}// IF order
			if (po.get_TableName().equals(MInOut.Table_Name)
					&& (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW)) && (po.getAD_Org_ID() == orgPos) )// 
			{
				String docStatus = po.get_ValueAsString("DocStatus");
				String docStatusOld = (String)po.get_ValueOld("DocStatus");
				Boolean isSales = po
						.get_ValueAsBoolean(MInOut.COLUMNNAME_IsSOTrx);
				Integer orderId = po.get_ValueAsInt("C_ORDER_ID");
				String trxName = po.get_TrxName();
				Integer ioId = po.get_ValueAsInt(MInOut.COLUMNNAME_M_InOut_ID);
				Integer docType = po.get_ValueAsInt(MInOut.COLUMNNAME_C_DocType_ID);
				if (orderId > 0 && !isSales && (topic.equals(IEventTopics.PO_AFTER_CHANGE)) && docStatus.equals(DocAction.STATUS_Completed)
						&& !docStatusOld.equals(DocAction.STATUS_Completed)) 
				{ // && 
					Boolean flag = tableInOut(trxName, orderId, ioId, false);
					if (!flag) {
						throw new AdempiereException(Msg.translate(Env.getCtx(), "SMJMSGProcessError")+ " - ERPPosInterceptorTrans: " + MInOut.Table_Name);
					}
				}// (docStatus.equals(DocAction.STATUS_Completed) && orderId > 0)
				Integer payTypeReturn = Integer.parseInt(MSysConfig.getValue("SMJ-PAYTYPERETURN",Env.getAD_Client_ID(Env.getCtx())).trim());
				if ((topic.equals(IEventTopics.PO_AFTER_CHANGE)) && docStatus.equals(DocAction.STATUS_Completed) 
						&& isSales && docType.equals(payTypeReturn)) 
				{

					/*  Solicitud 1000328 - funcionalidad removida 	
				  Boolean flag = tableInOutReturn(trxName, ioId, payTypeReturn);   
					if (!flag) {
						return Msg.translate(Env.getCtx(), "SMJMSGProcessError")+ " - ERPPosInterceptorTrans: " + MInOut.Table_Name;
					}*/
				}//IF && isSales && docType.equals(payTypeReturn)) {

				//Se elimina el proceso de reversar documentos de Consignaci�n de este Validador, y se traslada a SMJTransModelValidator
				/*if ((topic.equals( IEventTopics.PO_AFTER_CHANGE) && docStatus.equals(DocAction.STATUS_Reversed) 
						&& isSales) {
					Boolean rev = DataQueriesTrans.validateReversalIO(trxName, ioId);
					if (rev){
						MInOut io = new MInOut(Env.getCtx(), ioId, trxName);
						Boolean doit = io.get_ValueAsBoolean("smj_isreversal");
						if (!doit){
							Boolean flag = tableInOutRevelsal(trxName, io);
							if (!flag) {
								return Msg.translate(Env.getCtx(), "SMJMSGProcessError")+ " - ERPPosInterceptorTrans: " + MInOut.Table_Name;
							}
						}//if (!doit)

					}//if (rev)
				}//docStatus.equals(DocAction.STATUS_Reversed)*/
				if ((docStatus.equals(DocAction.STATUS_Completed) || docStatus.equals(DocAction.STATUS_Reversed))
						&& (topic.equals(IEventTopics.PO_AFTER_CHANGE))) {
					updateQty(trxName, ioId);
				}
			}// if MInOut.Table_Name
			if (po.get_TableName().equals(MBPartner.Table_Name)&& (topic.equals(IEventTopics.PO_AFTER_CHANGE)) ){
				String name = po.get_ValueAsString(MBPartner.COLUMNNAME_Name);
				Integer bparnerId = po.get_ValueAsInt(MBPartner.COLUMNNAME_C_BPartner_ID);
				Integer clientId = Env.getAD_Client_ID(Env.getCtx());
				String nameOld = DataQueriesTrans.getBpartnerName(clientId, bparnerId);
				if (!name.equals(nameOld)){
					String trxName = po.get_TrxName();
					chagePriceListName(trxName, name, nameOld, bparnerId, false);
				}
			}//if MBPartner.Table_Name
			if (po.get_TableName().equals(MSMJServiceRateGroup.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) )){
				String name = po.get_ValueAsString(MSMJServiceRateGroup.COLUMNNAME_Name);
				Integer code = po.get_ValueAsInt(MSMJServiceRateGroup.COLUMNNAME_smj_servicerategroup_ID);
				Integer clientId = Env.getAD_Client_ID(Env.getCtx());
				String nameOld = DataQueriesTrans.getServiceRateGroupName(clientId, code);
				if (!name.equals(nameOld)){
					String trxName = po.get_TrxName();
					chagePriceListName(trxName, name, nameOld, 0, true);
				}
			}
			Integer orgId = po.get_ValueAsInt(MRequest.COLUMNNAME_AD_Org_ID);
			//			Integer status = po.get_ValueAsInt(MRequest.COLUMNNAME_R_Status_ID);
			Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
			//--Adicion iMarch / Organizacion Transportadora--
			String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true")) orgInfotrans=orgId;
			//------------------------------------------------
			//TODO Valida rol en solicitud
			if (po.get_TableName().equals(MRequest.Table_Name)){
				if ((topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_BEFORE_CHANGE)) && orgId.equals(orgInfotrans)) {
					Integer logRol = Env.getAD_Role_ID(Env.getCtx());
					Integer status = po.get_ValueAsInt(MRequest.COLUMNNAME_R_Status_ID);
					String trxName = po.get_TrxName();
					Integer reqID = po.get_ValueAsInt(MRequest.COLUMNNAME_R_Request_ID);
					MRequest req = new MRequest(Env.getCtx(), reqID, trxName);
					String estadoTerminado = MSysConfig.getValue("SMJ-INFOTRAVELEND",Env.getAD_Client_ID(Env.getCtx())).trim();
					//
					if(status == null || status <=0){
						return;
					}

					else if (status.intValue() == Integer.parseInt(estadoTerminado)){  // validaciones para cuando se cambia estado a viaje terminado previamente 1000015 (estado 6)
						//  
						String msg1 = "";
						int tercero = req.get_ValueAsInt(MRequest.COLUMNNAME_C_BPartner_ID);
						if (tercero <= 0)
							msg1+="Tercero inv�lido - ";
						String placa = req.get_ValueAsString("smj_plate");
						if (placa.equalsIgnoreCase("") || placa.equalsIgnoreCase("null"))
							msg1+="Placa de Veh�culo inv�lida - ";
						int tenedor = req.get_ValueAsInt("c_bpartnerowner_id");
						if (tenedor <= 0)
							msg1+="Tenedor inv�lido - ";
						int conductor = req.get_ValueAsInt("c_bpartnerdriver_id");
						if (conductor <= 0)
							msg1+="Conductor inv�lido - ";
						boolean facturaAprobada = req.get_ValueAsBoolean("smj_isinvoiceapproved");
						if (!facturaAprobada)
							msg1+="Factura no ha sido aprobada - ";
						boolean cargoAprobado = req.get_ValueAsBoolean("smj_isinvoicecharge");
						if (!cargoAprobado)
							msg1+="Cargo no ha sido aprobado - ";
						boolean cargosAprobados = ServiceOrderValidations.areApprovedChargesByRequest(trxName,reqID);
						if (!cargosAprobados)
							msg1+="Cargos no han sido aprobados - ";
						boolean adelantosAprobados = ServiceOrderValidations.areApprovedAdvancesByRequest(trxName,reqID);
						if (!adelantosAprobados)
							msg1+="Adelantos no han sido aprobados - ";
						if (! msg1.equalsIgnoreCase(""))
							throw new AdempiereUserError(msg1 + " - No es posible cambiar el estado de una orden de servicio a completo sin los datos aprobados y completos ");
					}
					else {
						String msg = ServiceOrderValidations.validateServiceOrder(logRol, status);
						if(msg != null && msg.length() >0){
							throw new AdempiereException(msg);
						}

						req.setSummary(req.getSummary());
						req.save();
					}
				}else if((topic.equals(IEventTopics.PO_AFTER_NEW))&& orgId.equals(orgInfotrans)){
					String trxName = po.get_TrxName();
					Integer seqId = po.get_ValueAsInt(MRequest.COLUMNNAME_R_RequestType_ID);
					MRequestType rt = new MRequestType(Env.getCtx(), seqId, trxName);
					Integer seq = rt.get_ValueAsInt("Sequence")+1;
					//					String rseq = po.get_ValueAsString(MRequest.COLUMNNAME_DocumentNo);
					Integer reqId = po.get_ValueAsInt(MRequest.COLUMNNAME_R_Request_ID);
					po.set_ValueOfColumn(MRequest.COLUMNNAME_DocumentNo, seq.toString());
					MRequest req = new MRequest(Env.getCtx(), reqId, trxName);
					req.setDocumentNo(seq.toString());
					req.set_ValueOfColumn("smj_sequence", seq.toString());
					req.save();
					rt.set_ValueOfColumn("Sequence", seq);
					rt.save();
				}
			}//MRequest.Table_Name
			//TODO info-trans adelantos
			if (po.get_TableName().equals(MSMJControlAdvances.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))){
				Integer logRol = Env.getAD_Role_ID(Env.getCtx());
				//Boolean ap = po.get_ValueAsBoolean(MSMJControlAdvances.COLUMNNAME_IsApproved);
				String msg = ServiceOrderValidations.validateCreateAdvancesCharges(logRol);
				if (msg.length() > 0 ){
					throw new AdempiereException(msg);
				}
				msg = validateCreateAdvance(po);
				if (msg.length() > 0 ){
					throw new AdempiereException(msg);
				}
				return;
			}

			if (po.get_TableName().equals(MSMJTires.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))){
				Boolean pos = validateTires(po);
				if(!pos){
					throw new AdempiereException(Msg.translate(Env.getCtx(), "SMJMSGPosTiresGreater"));
				}
			}//if MSMJTires.Table_Name
			// crea historico de manguera de combustible Solicitud 1000294
			if (po.get_TableName().equals(MSMJFuelLine.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))) {
				try {
					createFuelLineHistory(po);
				} catch (Exception e) {
					throw new AdempiereException(e);
				}
				return;
			}//if MSMJFuelLine.Table_Name 
			//vehicleOwner Solicitud 1000273 valicacion de propietario y actualizacion de termino de pago del vehiculo
			if (po.get_TableName().equals(MSMJVehicleOwner.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))){

				String msg = validateVehicleOwner(po);
				if (msg != null && msg.length() > 0){
					throw new AdempiereException(msg);
				}
			}//MSMJVehicleOwner
			//TODO loadOrder
			if (po.get_TableName().equals(MSMJLoadOrder.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))){
				fillLoadOrder(po);
			}//MSMJLoadOrder
			if (po.get_TableName().equals(MSMJRemittances.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW))){

				String msg = fillRemittance(po);
				if (msg.length()>0){
					throw new AdempiereException(msg);
				}
			}//MSMJLoadOrder
			//   Product prices  - it  creates sales prices (new version) from purchase values for seveter or only - solciitud 1000312  			
			if (po.get_TableName().equals(MProductPrice.Table_Name) &&        
					(topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_NEW)) &&
					(po.getAD_Org_ID() == orgPos)){
				Integer pLVersionID = po.get_ValueAsInt(MProductPrice.COLUMNNAME_M_PriceList_Version_ID);
				Integer productID = po.get_ValueAsInt(MProductPrice.COLUMNNAME_M_Product_ID);
				MPriceListVersion pLVersionPurchase = new MPriceListVersion(Env.getCtx(), pLVersionID, po.get_TrxName());

				MPriceList pList = (MPriceList)pLVersionPurchase.getM_PriceList();
				if (!pList.isSOPriceList()) {   // solamete para precios de compra - genera version de precio de venta si no existia

					//MProduct prod = new MProduct(Env.getCtx(), productID, po.get_TrxName());

					String salesString = pLVersionPurchase.getDescription().replace("Compra","Venta");
					Integer plVersionSales = DataQueriesTrans.getPriceListVersion(po.get_TrxName(), po.getAD_Client_ID(), salesString);

					if (plVersionSales != 0) //  Existe una version de ventas  
					{
						if (!DataQueriesTrans.validatePriceListExists(productID, plVersionSales )) // if the sales version doesn�t exist creare a new one with the purchase prices
						{
							MProductPrice pp =  MProductPrice.get(Env.getCtx(),pLVersionID ,productID, po.get_TrxName()); // obtiene precio de compra para esa version

							MProductPrice sp =  new MProductPrice(Env.getCtx(),plVersionSales ,productID, po.get_TrxName()); //crear una nuevo precio de venta para esa version
							sp.setPriceList(pp.getPriceList());
							sp.setPriceLimit(pp.getPriceLimit());
							sp.setPriceStd(pp.getPriceStd());
							sp.save();
						}
					}  //  plVersionSales == 0
				} // !pList.isSOPriceList()
			}//if MSMJFuelLine.Table_Name


		}// if table change
		if (po.get_TableName().equals(MPayment.Table_Name) ){
			if ((topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_DELETE))){
				String docStatus = po.get_ValueAsString("DocStatus");
				String trxName = po.get_TrxName();
				Integer codeId = po.get_ValueAsInt(MPayment.COLUMNNAME_C_Payment_ID);
				if (docStatus.equals(DocAction.STATUS_Voided) || docStatus.equals(DocAction.ACTION_None) || topic.equals(IEventTopics.PO_AFTER_DELETE)){
					Integer code = DataQueriesTrans.getControlAdByPayment(trxName, codeId);
					if(code>0){
						DataQueriesTrans.updateAdvance(trxName, code, null);
					}//if(code>0)
				}//if docStatus
			} else if ((topic == IEventTopics.DOC_AFTER_VOID || topic == IEventTopics.DOC_AFTER_REVERSECORRECT)) {
				Integer codeId = po.get_ValueAsInt(MPayment.COLUMNNAME_C_Payment_ID);
				Integer code = DataQueriesTrans.getControlAdByPayment(po.get_TrxName(), codeId);
				if (code > 0) {
					DataQueriesTrans.updateAdvance(po.get_TrxName(), code, null);
				}
				
				return;
			}			 
		}//MPayment.Table_Name
		if (po.get_TableName().equals(MInvoice.Table_Name) && (topic.equals(IEventTopics.PO_AFTER_CHANGE) || topic.equals(IEventTopics.PO_AFTER_DELETE))){
			String docStatus = po.get_ValueAsString("DocStatus");
			String trxName = po.get_TrxName();
			Integer code = po.get_ValueAsInt(MInvoice.COLUMNNAME_C_Invoice_ID);
			if (docStatus.equals(DocAction.STATUS_Voided) || docStatus.equals(DocAction.ACTION_None) || docStatus.equals(DocAction.STATUS_Reversed) || topic.equals(IEventTopics.PO_AFTER_DELETE)){
				LinkedList<Integer> data = DataQueriesTrans.getRequestInvoice(trxName, code);
				if (data.size() > 0){
					Iterator<Integer> it = data.iterator();
					while (it.hasNext()){
						Integer cod = (Integer) it.next();
						DataQueriesTrans.updateRequestInvoice(trxName, cod, null);
					}//while iterator
					return;
				}//if (data.size() >= 0)
				Integer ccode = DataQueriesTrans.getChargeByInvoice(trxName, code);
				if (ccode > 0){
					DataQueriesTrans.updateCharge(trxName, ccode, null);
					return;
				}
				
				DataQueriesTrans.deleteInvoiceProvider(code, trxName);
				return;
			}// if docStatus

			// aqui se detecta si es una nota credito, y se dispara el proceso
			// de reversion, solo para la organizacion infotrans de ordenes de servicio
			// donde se quita el id de factura de la solicitud (r_request) si el total
			// de lineas de la factura original es igual al del rma. solicitud 1000317

			Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
			Integer orgId = po.get_ValueAsInt(MRequest.COLUMNNAME_AD_Org_ID); // org de la factura
			Integer targetDocId = po.get_ValueAsInt(MInvoice.COLUMNNAME_C_DocTypeTarget_ID);   // el de la factura
			Integer idDocCreditNote = Integer.parseInt(MSysConfig.getValue("SMJ_DOCCREDITNOTEINVOICE","0",Env.getAD_Client_ID(Env.getCtx()), orgId).trim());
			//--Adicion iMarch / Organizacion Transportadora--
			String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true")) orgInfotrans=orgId;
			//------------------------------------------------
			if (targetDocId.equals(idDocCreditNote)) {
				Integer rmaId = 	po.get_ValueAsInt(MInvoice.COLUMNNAME_M_RMA_ID);  // codigo de la rma
				if (orgId.equals(orgInfotrans) ) // es nota credito de infotrans
				{

					BigDecimal totalLines =  new BigDecimal (po.get_ValueAsString(MInvoice.COLUMNNAME_TotalLines)); // total de la nota credito
					Integer totalLinesSourceInvoice = DataQueriesTrans.getTotalLinesSourceInvoiceByRMAId(trxName, rmaId); // total de la factura origen
					if (totalLines.intValue() == (totalLinesSourceInvoice.intValue()))
					{
						//  Trx trx1 = Trx.get(Trx.createTrxName("AL1"), true);
						// obtiene factura origen:
						int sourceInvoiceId  = DataQueriesTrans.getInvoiceIdByRMAId(trxName, rmaId);
						LinkedList<Integer> data = DataQueriesTrans.getRequestInvoice(trxName, sourceInvoiceId);
						if (data.size() > 0){
							Iterator<Integer> it = data.iterator();
							while (it.hasNext()){
								Integer cod = (Integer) it.next();
								DataQueriesTrans.updateRequestInvoice(trxName, cod, null);
							}//while iterator
							return;
						}//if (data.size() >= 0)
						//int rId = DataQueries1.getRequestByInvoice(po.get_TrxName(), sourceInvoiceId);
						//DataQueries1.cleanRequestInvoice(po.get_TrxName(), rId);
						//  trx1.commit();
					}//  totalLines.intValue() 
				} // if infotrans
				else {    // is seveter & docCreditnote
					LinkedList<MRMALine> rmaLines = DataQueriesTrans.getRMALines(rmaId, trxName);

					if (rmaLines == null) {
						return;
					}

					Iterator<MRMALine> itl = rmaLines.iterator();
					while (itl.hasNext()){
						MRMALine line = itl.next();
						// aqui valida si el producto es un servicio, para efectuar la devolcuion de comisiones
						MProduct prodT = MProduct.get(Env.getCtx(), line.getM_Product_ID());
						Boolean isService = prodT.getProductType().equals("S")?true:false;
						if (isService) {
							//obtiene el numero de linea de factura correspondiente a la linea de la RMA
							int numInvoiceLine = DataQueriesTrans.getInvoiceLineIdByRMALineId(trxName,line.get_ID()  );
							// aqui reversara las comisiones que esten asociadas esta linea de factura
							LinkedList<MSMJMechanicCommission> listGar = 
									DataQueriesTrans.getMechanicCommisionListByInvoiceLine(trxName, numInvoiceLine);
							Iterator<MSMJMechanicCommission> itg = listGar.iterator();
							while (itg.hasNext()){
								MSMJMechanicCommission gm = itg.next();
								if (!DataQueriesTrans.isMechanicCommisionAlreadyReturned(trxName, numInvoiceLine,gm.gettotal().multiply(new BigDecimal(-1))) )
								{  // solo reversa la comision, si no hay una negativa ya con ese valor para esa linea de factura.
									MSMJMechanicCommission mc = new MSMJMechanicCommission(Env.getCtx(), 0, trxName); 
									mc.setC_BPartner_ID(gm.getC_BPartner_ID());
									mc.setM_Product_ID(gm.getM_Product_ID());
									mc.setR_Request_ID(gm.getR_Request_ID());
									mc.setsmj_workOrderLine_ID(gm.getsmj_workOrderLine_ID());
									mc.setQty(gm.getQty());
									mc.setappliedfee(gm.getappliedfee().multiply(new BigDecimal(-1)));
									mc.settotal(gm.gettotal().multiply(new BigDecimal(-1)));
									mc.setC_InvoiceLine_ID(numInvoiceLine);
									mc.settotalinvoiced(line.getTotalAmt());  // total de la linea del rma
									mc.setdatecreated(Env.getContextAsDate(Env.getCtx(), "#Date"));
									Boolean ok = mc.save();

									if(!ok){
										return;
									}
								} // isMechanicCommisionAlreadyReturned
							}//while (itg.hasNext())
						} // if (isService)
					}	// while				  

				}
			}	//targetDocId.equals(idDocCreditNote);		
		}//MInvoice.Table_Name

		// aqui se reversan las comisiones asociadas a devoluciones de servicios
		// cambiar el codigo para detectar aqui facturas que sean notas credito 
		// que incluyan servicios ...

		//		if (po.get_TableName().equals(MRMA.Table_Name) ){
		//			if (topic.equals( IEventTopics.PO_AFTER_CHANGE ){
		//				String docStatus = po.get_ValueAsString("DocStatus");
		//				String trxName = po.get_TrxName();
		//				Integer codeId = po.get_ValueAsInt(MRMA.COLUMNNAME_M_RMA_ID);
		//				if (docStatus.equals(DocAction.STATUS_Completed)){
		//					// obtiene las lineas de la RMA, y para cada linea efectua el reverso de las comisiones.
		//					// si ellas son servicios

		//					Integer code = DataQueriesTrans.getControlAdByPayment(trxName, codeId);
		//					if(code>0){
		//						DataQueriesTrans.updateAdvance(trxName, code, null);
		//					}//if(code>0)
		//				}//if docStatus
		//			}
		//		}//MPayment.Table_Name



		return;
	}// modelChange

	/**
	 * accion a realizar cuando se modifica tabla C_Order asociada a recepcion o
	 * asociada en estado completo to do when table C_Order associated receipt
	 * changes or receipt state is complete
	 * 
	 * @param trxName
	 * @param orderId
	 * @param ioId
	 * @return
	 */
	@SuppressWarnings("unused")
	private Boolean tableInOut(String trxName, Integer orderId, Integer ioId, Boolean fromOC) {
		Boolean flag = true;
		try {
			MOrder order = new MOrder(Env.getCtx(), orderId, trxName);
			if (order == null) {
				return false;
			}
			//MInOut inout = new MInOut(Env.getCtx(), ioId, trxName);
			//if (inout == null || !inout.getDocStatus().equals("CO")) {
				//return false;
			//}
			Integer bparnerId = order.getC_BPartner_ID();
			Integer warehouseId = order.getM_Warehouse_ID();
			MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
			Integer warehouseOwnerId = 0;
			Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
			String status = order.getDocStatus();
			
			if (isConsigment) {
				warehouseOwnerId = warehouse.get_ValueAsInt("C_BPartner_ID");
			}
			
			Integer locatorOrderId = DataQueriesTrans.getLocatorId(order.getM_Warehouse_ID());
			MOrderLine lines[] = order.getLines();
			
			if (lines == null) {
				return false;
			}
			
			Boolean purFlag = true;
			Boolean salFlag = true;
			Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY", Env.getAD_Client_ID(Env.getCtx())).trim());
			Integer rmnFacCategory = Integer.parseInt(MSysConfig.getValue("SMJ-REMANUFACTUREDCAT", Env.getAD_Client_ID(Env.getCtx())).trim());
			for (int i = 0; i < lines.length; i++) {
				MOrderLine lin = lines[i];
				MProduct p = new MProduct(Env.getCtx(), lin.getM_Product_ID(),trxName);
				BigDecimal qty = DataQueriesTrans.getTotalProductByPartner(lin.getM_Product_ID(), bparnerId, trxName);
				
				if (qty == null) {
					qty = Env.ZERO;
				}

				if (order.getPOReference() == null || !order.getPOReference().equals("CONSIGNACION")) {					
					if (p == null) {
						return false;
					}
					Boolean isRmnFac = false;
					if (p.getM_Product_Category_ID() == rmnFacCategory){
						isRmnFac = true;
					}
					HashMap<String, Integer> codes = DataQueriesTrans.getPListVersion(trxName, p, bparnerId, tiresCategory, locatorOrderId, warehouseOwnerId, null);
					MProductPrice sp = null;
					MProductPrice pp = null;

					if (codes != null) {
						sp = MProductPrice.get(Env.getCtx(), codes.get("SALES"),lin.getM_Product_ID(), trxName);
						pp = MProductPrice.get(Env.getCtx(), codes.get("PURCHASE"),lin.getM_Product_ID(), trxName);
					}

					if (pp == null) {
						pp = new MProductPrice(Env.getCtx(), codes.get("PURCHASE"),lin.getM_Product_ID(), trxName);
					}

					if (sp == null) {
						sp = new MProductPrice(Env.getCtx(), codes.get("SALES"),lin.getM_Product_ID(), trxName);
					}

					// Boolean isConsigment =
					BigDecimal util = new BigDecimal(p.get_ValueAsString("smj_utility"));
					BigDecimal tax = DataQueriesTrans.getTaxValue(p.getC_TaxCategory_ID());
					BigDecimal salesPrice = UtilTrans.calculateSalesPrice(lin.getPriceEntered(), util, tax);
					//iMarch
					BigDecimal listPriceOld = lin.getPriceList();
					//
					boolean allowChangesPur = pp.get_ValueAsBoolean("smj_isallowchanges");
					boolean allowChangesSal = sp.get_ValueAsBoolean("smj_isallowchanges");
					
					
					String noUpdatePrices = MSysConfig.getValue("SMJ-FUELCATEGORY", Env.getAD_Client_ID(Env.getCtx())).trim();
					String[] cates = noUpdatePrices.split(";");
					Boolean updateSales = true;
					try {
						for (int k=0;k<cates.length;k++){
							Integer fuelCategory = Integer.parseInt(cates[k].trim());
							if(p.getM_Product_Category_ID() == fuelCategory){
								updateSales = false;
							}
						}//for
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getName()+ ".tableInOut - ERROR: " + e.getMessage(), e);
					}
					if(isRmnFac){
						updateSales = false;
					}

					if (pp != null) {
						BigDecimal price = pp.getPriceStd();
						if (p.getM_Product_Category_ID() == tiresCategory) {
							if (price.compareTo(lin.getPriceEntered()) < 0) { // si  llantas  y mayor
								pp.setPriceList(lin.getPriceEntered());
								pp.setPriceStd(lin.getPriceEntered());
								if (!allowChangesPur){
									pp.setPriceLimit(lin.getPriceEntered());
								}
								purFlag = pp.save();
								if (sp != null && updateSales) {
									sp.setPriceList(salesPrice);
									sp.setPriceStd(salesPrice);
									if(!allowChangesSal){
										sp.setPriceLimit(salesPrice);
									}
									salFlag = sp.save();
								}
							}
						} // if llantas
						else if (isConsigment && (bparnerId.equals(warehouseOwnerId))) {
							pp.setPriceList(lin.getPriceEntered());
							pp.setPriceStd(lin.getPriceEntered());
							if (!allowChangesPur){
								pp.setPriceLimit(lin.getPriceEntered());
							}
							purFlag = pp.save();
							if (sp != null && updateSales) {
								//sp.setPrices(salesPrice, salesPrice, salesPrice);
								sp.setPriceList(salesPrice);
								sp.setPriceStd(salesPrice);
								if(!allowChangesSal){
									sp.setPriceLimit(salesPrice);
								}
								salFlag = sp.save();
							}
						} // else if consignacion
						else {

							BigDecimal cCost = lin.getPriceEntered();
							BigDecimal oldCost = pp.getPriceStd();
							BigDecimal totalWarehouse = DataQueriesTrans.getTotalProductByLocator(p.getM_Product_ID(),locatorOrderId, trxName, isConsigment);
							BigDecimal qtyNew = lin.getQtyDelivered();
							//iMarch
							if(fromOC) {
								oldCost = listPriceOld;
								if(totalWarehouse.compareTo(qtyNew)<=0)
									totalWarehouse = qtyNew;
							}
							//---
							if (totalWarehouse == null) {
								totalWarehouse = qtyNew;
							}

							BigDecimal qtyOld = totalWarehouse.subtract(qtyNew);
							BigDecimal totalAct = oldCost.multiply(qtyOld);
							BigDecimal totalNew = cCost.multiply(qtyNew);
							if (totalWarehouse.compareTo(Env.ZERO) > 0) {
								BigDecimal avp = ((totalAct.add(totalNew)).divide(totalWarehouse, 2, RoundingMode.HALF_UP));
								//iMarch
								if(!fromOC && qtyNew.compareTo(Env.ZERO)==0) {
									avp = listPriceOld;
									cCost = listPriceOld;
								}
								//---
								
								salesPrice = UtilTrans.calculateSalesPrice(avp, util,tax);
								
								//actualiza precios de compra pero no precios de venta
								if( !updateSales){
									pp.setPriceList(cCost);
									pp.setPriceStd(cCost);
									if (!allowChangesPur){
										pp.setPriceLimit(cCost);
									}
									purFlag = pp.save();
								}else{
									pp.setPriceList(avp);
									pp.setPriceStd(avp);
									if (!allowChangesPur){
										pp.setPriceLimit(avp);
									}
									purFlag = pp.save();
									if (sp != null) {
										//Validate if exists again (Bug)
										if (sp.is_new()) {
											MProductPrice spTmp = MProductPrice.get(Env.getCtx(), sp.getM_PriceList_Version_ID(), sp.getM_Product_ID(), trxName);

											if (spTmp != null) {
												sp = spTmp;
											}
										}

										sp.setPriceList(salesPrice);
										sp.setPriceStd(salesPrice);
										if(!allowChangesSal){
											sp.setPriceLimit(salesPrice);
										}

										sp.saveEx();
										salFlag = true;
									}
								}
								String isUpdate = MSysConfig.getValue("SMJ-UPDATEAVCOST",Env.getAD_Client_ID(Env.getCtx())).trim();
								if (isUpdate.equals("true")) {
									Integer costId = Integer.parseInt(MSysConfig.getValue("SMJ_COSTELEMENT",Env.getAD_Client_ID(Env.getCtx())).trim());
									Integer costTypeId = Integer.parseInt(MSysConfig.getValue("SMJ-MCOSTTYPEDEFAULT",Env.getAD_Client_ID(Env.getCtx())).trim());
									Integer acctSchemaId = Integer.parseInt(MSysConfig.getValue("SMJ-CACCTSCHEMADEFAULT",Env.getAD_Client_ID(Env.getCtx())).trim());
									MCost mc = MCost.get(Env.getCtx(),Env.getAD_Client_ID(Env.getCtx()), 0, p.getM_Product_ID(), costTypeId, acctSchemaId,costId, 0, trxName);
									if (mc == null) {
										MAcctSchema as = MAcctSchema.get(Env.getCtx(), acctSchemaId, trxName);
										mc = new MCost(p, 0, as, 0, costId);
									}
									mc.setCurrentCostPrice(avp);
									mc.saveEx();
								}// if (isUpdate.equals("true"))
							}// if(totalWarehouse.compareTo(Env.ZERO)>0)
						}// else
					}// pp != null
					if (!purFlag || !salFlag) {
						flag = false;
					}
				}
				// actualizar totales

				Boolean exist = DataQueriesTrans.validateProductParnerExist(trxName,
						p.getM_Product_ID(), bparnerId);
				if (exist) {
					DataQueriesTrans.updateProductParner(trxName, p.getM_Product_ID(), bparnerId, qty);
				} else {
					MBPartnerProduct mpro = new MBPartnerProduct(Env.getCtx(), 0, trxName);
					mpro.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
					mpro.setM_Product_ID(lin.getM_Product_ID());
					mpro.setC_BPartner_ID(bparnerId);
					mpro.setShelfLifeMinPct(0);
					mpro.setShelfLifeMinDays(0);
					mpro.setQualityRating(Env.ZERO);
					mpro.setVendorProductNo("");
					mpro.set_ValueOfColumn("smj_total", qty);
					mpro.saveEx();
				}

			}// for
		} catch (Exception e) {
			log.log(Level.SEVERE, ERPPosInterceptorTrans.class.getName()
					+ ".tableInOut - Error process :: ", e);
			flag = false;
		}
		return flag;
	}// tableInOut

	/***
	 * compeltar ordenes de venta
	 * 
	 * @param trxName
	 * @param po
	 * @param sales
	 * @return Boolean
	 */
	private Boolean completeOrderSales(String trxName, PO po) {
		Boolean flag = true;
		String description = "";
		try {
			Integer orderId = po.get_ValueAsInt(MOrder.COLUMNNAME_C_Order_ID);
			MOrder order = new MOrder(Env.getCtx(), orderId, trxName);
			MInvoice inv = CompleteSaleWebTrans.completeDocumentsSales(trxName,true, order,false,false);

			if (inv == null) {
				return false;
			} else {
				// invoiceId = inv.getC_Invoice_ID();
				Integer paytermDef = Integer.parseInt(MSysConfig.getValue(
						"SMJ-PAYMENTTERMDEFAULT",
						Env.getAD_Client_ID(Env.getCtx())).trim());
				Integer payInternal = Integer.parseInt(MSysConfig.getValue(
						"SMJ-PAYMENTTERMINTERNAL",
						Env.getAD_Client_ID(Env.getCtx())).trim());
				LinkedList<MSMJTmpPayment> payList = DataQueriesTrans.getTmpPayment(
						trxName, order.getC_Order_ID());
				Iterator<MSMJTmpPayment> itp = payList.iterator();
				while (itp.hasNext()) {
					MSMJTmpPayment pm = itp.next();
					if (pm.getC_PaymentTerm_ID() == paytermDef
							|| pm.getC_PaymentTerm_ID() == payInternal) {
						String tender = pm.getTenderType();
						if (pm.getC_PaymentTerm_ID() == payInternal) {
							tender = MSysConfig.getValue("SMJ-TENDERTYPECASH",
									Env.getAD_Client_ID(Env.getCtx())).trim();
						}
						Integer payId = CompleteSaleWebTrans.createPayment(trxName,
								inv, pm.gettotal(), tender, pm.getcctype(),
								pm.getccnumber(), pm.getccverfication(),
								pm.getccexpmm(), pm.getccexpyy(), pm.getName(),
								pm.getRoutingNo(), pm.getAccountNo(),
								pm.getCheckNo(), pm.getMicr(),
								pm.getC_PaymentTerm_ID(), Env.ZERO, Env.ZERO);
						if (payId <= 0) {
							return false;
						}
						pm.set_TrxName(trxName);
						pm.save();
						Boolean bdel = pm.delete(true);
						if (!bdel) {
							return false;
						}
					}
				}// while
			}// if/else inv == null
			order.setDescription("APROBADO **** " + order.getDescription());
			order.setDocStatus(DocAction.STATUS_Completed);
			order.setDocAction(DocAction.STATUS_Closed);
			order.setProcessed(true);
			order.save();
		} catch (Exception e) {
			log.log(Level.SEVERE, ERPPosInterceptorTrans.class.getName()
					+ ".completeOrderSales - Error process :: " + description,
					e);
		}
		return flag;
	}// completeOrderSales

	/**
	 * actualiza el total del inventario por tercero y producto - update
	 * inventory total by partner and product
	 * 
	 * @param trxName
	 * @param ioId
	 */
	private void updateQty(String trxName, Integer ioId) {
		try {
			LinkedList<HashMap<String, Integer>> data = DataQueriesTrans
					.getPartnerProductList(trxName, ioId);
			Iterator<HashMap<String, Integer>> it = data.iterator();
			while (it.hasNext()) {
				HashMap<String, Integer> codes = it.next();
				Integer bparnerId = codes.get("PARTNER");
				Integer productId = codes.get("PRODUCT");
				BigDecimal qty = DataQueriesTrans.getTotalProductByPartner(
						productId, bparnerId, trxName);
				if (qty != null) {
					DataQueriesTrans.updateProductParner(trxName, productId, bparnerId, qty);
				}
			}// while.....

		} catch (Exception e) {
			log.log(Level.SEVERE, ERPPosInterceptorTrans.class.getName()
					+ ".updateQty - Error process :: " + e.getMessage(), e);
		}
		return;
	}// updateQty

	/**
	 * actualiza el nombre de la lista de precios si cambia el nombre del tercero - 
	 * changes price list name if changes partner name
	 * @param trxName
	 * @param name
	 * @param oldName
	 * @param bpartnerId
	 * @return
	 */
	private Boolean chagePriceListName(String trxName, String name, String oldName, Integer bpartnerId, Boolean service){
		Boolean flag = true;
		Integer whId = DataQueriesTrans.getWarehouseByOwner(bpartnerId);
		if (whId > 0 || service){
			String namePrice = "Compra_"+oldName.trim();
			String nameSales = "Venta_"+oldName.trim();
			Integer pId = DataQueriesTrans.getPriceListVersion(trxName, Env.getAD_Client_ID(Env.getCtx()), namePrice);
			if (pId > 0 || service){
				String newName = "Compra_"+name.trim();
				Integer purchaceId = Integer.parseInt(MSysConfig.getValue("SMJ_PURCHASELIST",Env.getAD_Client_ID(Env.getCtx())).trim());
				if(pId.equals(0)){
					pId = UtilTrans.createPriceListVersion(newName, purchaceId);
				}
				MPriceListVersion mplv = new MPriceListVersion(Env.getCtx(), pId, trxName);
				mplv.setName(newName);
				mplv.setDescription(newName);
				mplv.save();
			}
			Integer sId  = DataQueriesTrans.getPriceListVersion(trxName,Env.getAD_Client_ID(Env.getCtx()), nameSales);
			if (sId > 0 || service){
				String newName = "Venta_"+name.trim();
				Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",Env.getAD_Client_ID(Env.getCtx())).trim());
				if (sId.equals(0)){
					sId = UtilTrans.createPriceListVersion(newName, salesId);
				}
				MPriceListVersion mplv = new MPriceListVersion(Env.getCtx(), sId, trxName);
				mplv.setName(newName);
				mplv.setDescription(newName);
				mplv.save();
			}
		}
		return flag;
	}

	/**
	 * valida si la posicion es mayor al numero de llantas del vehiculo - 
	 * validate if position is greater than tires number
	 * @param po
	 * @return Boolean
	 */
	private Boolean validateTires(PO po){
		Boolean flag = true;
		String trxName = po.get_TrxName();
		Integer vhId = po.get_ValueAsInt(MSMJTires.COLUMNNAME_smj_vehicle_ID);
		MSMJVehicle vh = new MSMJVehicle(Env.getCtx(), vhId, trxName);
		Integer qty = vh.getqtytires();
		Integer pos = po.get_ValueAsInt(MSMJTires.COLUMNNAME_Position);

		// por cada cambio inserta un registro en el historico`si el cambio es generaod por una orden de trabajo
		if (!(po.get_ValueAsInt("smj_workOrderLine_ID") > 0)) // genera historico para cambios que no sean generados por orden de trabajo
		{
			//MSMJTires tire = (MSMJTires)po; po.get_ValueAsString("ad_org_id");
			MSMJHistoryTire h = new MSMJHistoryTire(Env.getCtx(), 0, po.get_TrxName());
			h.setM_Product_ID(po.get_ValueOldAsInt("M_Product_ID"));
			h.setsmj_vehicle_ID(po.get_ValueOldAsInt("smj_vehicle_ID"));
			h.setsmj_plate(DataQueriesTrans.getVehiclePlate(po.get_ValueOldAsInt("smj_vehicle_ID")));
			h.setC_BPartner_ID(po.get_ValueOldAsInt("C_BPartner_ID"));;
			h.setPosition(po.get_ValueOldAsInt("Position"));
			h.setserial(po.get_ValueOldAsInt("Serial"));
			h.setdatemount( (Timestamp) po.get_ValueOld("datemount"));
			h.setdateumount(Env.getContextAsDate(Env.getCtx(), "#Date"));
			h.setsmj_changeTire_ID(1000000);  // by default first reason to change tires
			h.setsmj_workOrderLine_ID(po.get_ValueOldAsInt("smj_workOrderLine_ID"));
			h.setkms(new BigDecimal(po.get_ValueOldAsInt("kms")));
			h.setvaluebrake(new BigDecimal(po.get_ValueOldAsInt("valuebrake")));
			h.setvaluetire(new BigDecimal(po.get_ValueOldAsInt("valuetire")));
			h.settirestate((String)po.get_ValueOld("Status") );

			h.saveEx();
		}

		if(pos > qty){
			flag = false;
		}

		return flag;
	}//validateTires

	/**
	 * valida que los datos del vehiculo existan para crear el anticipo - 
	 * validate than vehicle data exist to create advance
	 * @param po	
	 * @return
	 */
	private String validateCreateAdvance(PO po){
		String msg = "";
		String trxName = po.get_TrxName();
		Integer advanceId = po.get_ValueAsInt(MSMJControlAdvances.COLUMNNAME_smj_controlAdvances_ID);
		MSMJControlAdvances ad =  new MSMJControlAdvances(Env.getCtx(), advanceId, trxName);
		Integer reqId = ad.getR_Request_ID();
		//		Integer chId = ad.getsmj_charges_ID();
		//		if (chId == null || chId <= 0){
		//			return "";
		//		}
		MRequest req = new MRequest(Env.getCtx(), reqId, trxName);
		String plate = req.get_ValueAsString("smj_plate");
		if (plate == null || plate.length()<= 0){
			return Msg.translate(Env.getCtx(), "SMJMSGErrorAdvanceNOPlate");

		}
		Integer ownerId = req.get_ValueAsInt("c_bpartnerowner_ID");
		if (ownerId == null || ownerId <= 0){
			return Msg.translate(Env.getCtx(), "SMJMSGErrorAdvanceNOOwner");

		}
		Integer driverId = req.get_ValueAsInt("c_bpartnerdriver_ID");
		if (driverId == null || driverId <= 0){
			return Msg.translate(Env.getCtx(), "SMJMSGErrorAdvanceNODriver");

		}
		Integer chargeId = ad.getsmj_charges_ID();
		if (chargeId != null && chargeId > 0){
			MSMJCharges ca = new MSMJCharges(Env.getCtx(), chargeId, trxName);
			Integer invId =  ca.getC_Invoice_ID();
			if (invId != null && invId > 0){
				return msg = Msg.translate(Env.getCtx(), "SMJMSGChargeHasInvoice");
			}//if (invId != null && invId > 0)
			if (!ca.isApproved()){
				return msg = Msg.translate(Env.getCtx(), "SMJMSGNoCreateAdvChargeNoAp");
			}
		}//if (chargeId != null && chargeId > 0)
		Boolean isAproved = ad.isApproved();
		if(isAproved){
			DataQueriesTrans.updateRequestUpdVh(trxName, reqId);
		}
		return msg;
	}//validateCreateAdvance
	/**
	 * crea	el historico de linea de combustible - 
	 * create history fuel line
	 * @param po
	 * @return
	 */
	private void createFuelLineHistory(PO po) throws Exception {
		String trxName = po.get_TrxName();
		Integer lineId = po.get_ValueAsInt(MSMJFuelLine.COLUMNNAME_smj_FuelLine_ID);
		MSMJFuelLine fl = new MSMJFuelLine(Env.getCtx(), lineId, trxName);
		MSMJFuelLine f2 = new MSMJFuelLine(Env.getCtx(), lineId, trxName);
		if(fl != null){
			MSMJFuelLineHistory h = new MSMJFuelLineHistory(Env.getCtx(), 0, trxName);
			h.setsmj_FuelIsland_ID(fl.getsmj_FuelIsland_ID());
			h.setsmj_FuelLine_ID(fl.getsmj_FuelLine_ID());
			h.setsmj_rchangefuelline_ID(fl.getsmj_rchangefuelline_ID());
			h.setactualhundred(fl.getactualhundred());
			h.setlimithundred(fl.getlimithundred());
			h.setName(fl.getName());
			h.saveEx();
			
			DataQueriesTrans.updateFuelLine(trxName, lineId, fl.getactualhundred());
			MUser us =  MUser.get(Env.getCtx());

			MSMJFuelIsland i = new MSMJFuelIsland(Env.getCtx(), fl.getsmj_FuelIsland_ID(), trxName);
			MSMJFuelSales f = new MSMJFuelSales(Env.getCtx(), 0, trxName);
			f.setC_BPartner_ID(us.getC_BPartner_ID());
			f.setentrydate(Env.getContextAsDate(Env.getCtx(), "#Date"));
			f.setM_Warehouse_ID(i.getM_Warehouse_ID());
			f.setsmj_FuelIsland_ID(fl.getsmj_FuelIsland_ID());
			f.setsmj_FuelLine_ID(lineId);
			f.setIslandName(i.getName());
			f.setLineName(fl.getName());
			f.setsmj_rchangefuelline_ID(fl.getsmj_rchangefuelline_ID());
			f.setactualhundred(f2.getactualhundred());
			f.sethundredend(fl.getactualhundred());
			f.setgalonprice(Env.ZERO);
			f.settotalprice(Env.ZERO);
			f.setqtysold(Env.ZERO);
			f.setisprocessed(false);
			f.saveEx();			
		}
	}//createFuelLineHistory

	/**
	 * validaciones a realizar cuando se cambia el proietario o tenedor del vehiculo - 
	 * to do when vehicle owner or tender changes 
	 * @param po
	 * @return
	 */
	private String validateVehicleOwner(PO po){
		String trxName = po.get_TrxName();
		Integer vehicleId = po.get_ValueAsInt(MSMJVehicleOwner.COLUMNNAME_smj_vehicle_ID);
		Date startDate = (Date) po.get_Value(MSMJVehicleOwner.COLUMNNAME_StartDate);
		if (startDate == null){
			return Msg.translate(Env.getCtx(), "SMJMSGErrorStartDate"); 
		}
		String relation = po.get_ValueAsString(MSMJVehicleOwner.COLUMNNAME_relationship);
		if(relation.equals("1")){
			Integer bpartnerId = po.get_ValueAsInt(MSMJVehicleOwner.COLUMNNAME_C_BPartner_ID);
			MBPartner bp = MBPartner.get(Env.getCtx(), bpartnerId);
			Integer payTermId = bp.getC_PaymentTerm_ID();
			MSMJVehicle vh = new MSMJVehicle(Env.getCtx(), vehicleId, trxName);
			if(payTermId == null || payTermId <= 0){
				vh.setC_PaymentTerm_ID(0);
			}else{
				vh.setC_PaymentTerm_ID(payTermId);
			}
			vh.save();
		}
		Integer vehicleOwnerID = po.get_ValueAsInt(MSMJVehicleOwner.COLUMNNAME_smj_vehicleOwner_ID);

		if(vehicleOwnerID < 1) {
			vehicleOwnerID = 0;
		}

		LinkedList<Integer> codes = DataQueriesTrans.getVehicleOwnerRelation(trxName, vehicleId, relation, vehicleOwnerID);
		Iterator<Integer> itCodes = codes.iterator();
		Integer noEnd = 0;
		while (itCodes.hasNext()){
			Boolean save = false;
			Integer code = itCodes.next();
			MSMJVehicleOwner vo = new MSMJVehicleOwner(Env.getCtx(), code, trxName);
			Date sd = vo.getStartDate();
			Date ed = vo.getEndDate();
			if (ed == null){
				ed = startDate;
				noEnd++;
				save = true;
			}
			String voIds = vehicleOwnerID+","+code;
			Integer count =  DataQueriesTrans.getValidateDatesOwner(vehicleId, relation, voIds, sd, ed);
			if (count>0){
				return Msg.translate(Env.getCtx(), "SMJMSGMoreThanOneRecordDate");
			}
			if (save){
				DataQueriesTrans.updateVehicleOwnerEnd(trxName, code, ed, null);
			}
		}
		if (noEnd>0){
			return Msg.translate(Env.getCtx(), "SMJMSGRecordWhitoutEndDate");
		}
		//		if (ownerId > 0 ){
		//			Boolean ok = DataQueries1.updateVehicleOwnerEnd(trxName, vehicleOwnerID, endDate, null);
		//		}
		return "";
	}//validateVehicleOwner

	/**
	 * llena los campos de naombre y direccion si estan vacios -
	 * fill name and location fields if is empty 
	 * @param po
	 * @return
	 */
	private Boolean fillLoadOrder(PO po){
		Boolean flag = true;
		String trxName = po.get_TrxName();
		Integer code = po.get_ValueAsInt(MSMJLoadOrder.COLUMNNAME_smj_loadOrder_ID);
		String name = po.get_ValueAsString(MSMJLoadOrder.COLUMNNAME_sendername);
		Integer locId = po.get_ValueAsInt(MSMJLoadOrder.COLUMNNAME_smj_mainstartloc_ID);
		Integer reqID = po.get_ValueAsInt(MSMJLoadOrder.COLUMNNAME_R_Request_ID);
		MRequest req = new MRequest(Env.getCtx(), reqID, trxName);

		MSMJLoadOrder lo = new MSMJLoadOrder(Env.getCtx(), code, trxName);

		if (name == null || name.length()<=0){
			lo.setsendername(req.get_ValueAsString("smj_mainstartname"));
			lo.save();
		}
		if (locId == null || locId<=0){
			lo.setsmj_mainstartloc_ID(req.get_ValueAsInt("smj_mainstartloc_ID"));
			lo.save();
		}
		return flag;
	}//fillLoadOrder

	/**
	 * llena los campos de naombre y direccion si estan vacios y 
	 * valida que la suma de las remisiones no sea mayor a la solicitud -
	 * fill name and location fields if is empty and validatte remittance summary 
	 * is not greater than request 
	 * @param po
	 * @return String
	 */
	private String fillRemittance(PO po) {
		String msg = "";
		String trxName = po.get_TrxName();
		Integer code = po.get_ValueAsInt(MSMJRemittances.COLUMNNAME_smj_remittances_ID);
		String sname = po.get_ValueAsString(MSMJRemittances.COLUMNNAME_sendername);
		Integer slocId = po.get_ValueAsInt(MSMJRemittances.COLUMNNAME_senderstartloc_ID);
		String ename = po.get_ValueAsString(MSMJRemittances.COLUMNNAME_receivername);
		Integer elocId = po.get_ValueAsInt(MSMJRemittances.COLUMNNAME_receiverendloc_ID);
		Integer reqId = po.get_ValueAsInt(MSMJLoadOrder.COLUMNNAME_R_Request_ID);
		String merchandise = po.get_ValueAsString(MSMJRemittances.COLUMNNAME_smj_merchandise);
		BigDecimal volume = Env.ZERO;
		try {
			volume = new BigDecimal(po.get_ValueAsString(MSMJRemittances.COLUMNNAME_smj_volume));
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".fillRemittance - ERROR: " + e.getMessage(),
					e);
		}
		BigDecimal weight = Env.ZERO;
		try {
			weight = new BigDecimal(po.get_ValueAsString(MSMJRemittances.COLUMNNAME_smj_weight));
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".fillRemittance - ERROR: " + e.getMessage(),
					e);
		}
		MRequest req = new MRequest(Env.getCtx(), reqId, trxName);

		MSMJRemittances lo = new MSMJRemittances(Env.getCtx(), code, trxName);
		BigDecimal rVol = Env.ZERO;
		BigDecimal rWe = Env.ZERO;
		try {
			rVol = new BigDecimal(req.get_ValueAsString("smj_volume"));
			rWe = new BigDecimal(req.get_ValueAsString("smj_weight"));
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".fillRemittance - ERROR: " + e.getMessage(),
					e);
			return Msg.translate(Env.getCtx(), "SMJMSGVolWeightAreMandatory");
		}
		BigDecimal nVol = Env.ZERO;
		BigDecimal nWe = Env.ZERO;
		if (volume == null || volume.compareTo(Env.ZERO)<=0){
			try {
				nVol = rVol;
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName() + ".fillRemittance - ERROR: "
						+ e.getMessage(), e);
			}
		}else{
			nVol = volume;
		}
		if (weight == null || weight.compareTo(Env.ZERO)<=0){
			try {
				nWe = rWe;
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName() + ".fillRemittance - ERROR: "
						+ e.getMessage(), e);
			}
		}else{
			nWe = weight;
		}

		try {
			HashMap<String, BigDecimal> sum = DataQueriesTrans.validateRemmitanceValues(trxName, reqId, code);
			BigDecimal sVol = sum.get("VOL");
			BigDecimal sWe = sum.get("WE");
			BigDecimal totVol = sVol.add(nVol);
			BigDecimal totWe = sWe.add(weight);
			if (totVol.compareTo(rVol) > 0 || totWe.compareTo(rWe) > 0){
				return Msg.translate(Env.getCtx(), "SMJMSGVolWeightExceedReqiest");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + "fillRemittance - ERROR: " + e.getMessage(),
					e);
		}
		lo.setsmj_volume(nVol);
		lo.setsmj_weight(nWe);
		lo.save();
		if (sname == null || sname.length()<=0){
			lo.setsendername(req.get_ValueAsString("smj_mainstartname"));
			lo.save();
		}


		if (slocId == null || slocId<=0){
			// 1000335 fix de localizaciones, se crean replciadas en contenido pero independientes
			// de la orden de servicio 

			int newLocation =  copyToNewLocation(req.get_ValueAsInt("smj_mainstartloc_ID"),trxName);
			lo.setsenderstartloc_ID(newLocation);
			lo.save();
		}
		if (ename == null || ename.length()<=0){
			lo.setreceivername(req.get_ValueAsString("smj_mainendname"));
			lo.save();
		}
		if (elocId == null || elocId<=0){
			// 1000335 fix de localizaciones, se crean replciadas en contenido pero independientes
			// de la orden de servicio 

			int newLocation =  copyToNewLocation(req.get_ValueAsInt("smj_mainendloc_ID"),trxName);
			lo.setreceiverendloc_ID(newLocation);

			lo.save();
		}
		if(merchandise == null || merchandise.length()<= 0){
			lo.setsmj_merchandise(req.getSummary());
			lo.save();

		}
		return msg;
	}//fillRemittance

	/**
	 * Creates a new location (copy) with the data of a previous location
	 * @param oldLocationId
	 * @return
	 */
	public int copyToNewLocation (int oldLocationId,String trxName) {
		MLocation lc =  new MLocation(Env.getCtx(), oldLocationId, trxName);
		MLocation newLc =  new MLocation(Env.getCtx(), lc.getC_Country_ID(),lc.getRegion().get_ID(),lc.getCity(),trxName);
		newLc.setAddress1(lc.getAddress1());
		newLc.setAddress2(lc.getAddress2());
		newLc.setAddress3(lc.getAddress3());
		newLc.setAddress4(lc.getAddress4());
		newLc.setC_City_ID(lc.getC_City_ID());
		newLc.setC_Region_ID(lc.getRegion().get_ID());
		newLc.setC_Country_ID(lc.getC_Country_ID());
		newLc.setCity(lc.getCity());
		newLc.setPostal(lc.getPostal());
		newLc.setAD_Org_ID(lc.getAD_Org_ID());
		newLc.saveEx();
		return newLc.get_ID();
	}



}// ERPPosInterceptorTrans
