package com.smj.model;

import java.util.Properties;

public class MSMJBrand extends X_smj_brand{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1271084100916263720L;

	/**
	 * MSMJBrand constructor
	 * @param ctx
	 * @param smj_brand_ID
	 * @param trxName
	 */
	public MSMJBrand(Properties ctx, int smj_brand_ID, String trxName) {
		super(ctx, smj_brand_ID, trxName);
	}

}//MSMJBrand
