/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_workOrderLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_smj_workOrderLine extends PO implements I_smj_workOrderLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160328L;

    /** Standard Constructor */
    public X_smj_workOrderLine (Properties ctx, int smj_workOrderLine_ID, String trxName)
    {
      super (ctx, smj_workOrderLine_ID, trxName);
      /** if (smj_workOrderLine_ID == 0)
        {
			setM_Product_ID (0);
			setPrice (Env.ZERO);
			setQty (Env.ZERO);
			setR_Request_ID (0);
			setsmj_isallowchanges (false);
			setsmj_iscommission (false);
			setsmj_isexternallservice (false);
			setsmj_isservice (false);
			setsmj_iswarranty (false);
			setsmj_iswarrantyapplied (false);
			setsmj_workOrderLine_ID (0);
			settaxvalue (Env.ZERO);
			settotal (Env.ZERO);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_smj_workOrderLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_workOrderLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_Invoice)MTable.get(getCtx(), org.compiere.model.I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_Value (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_Value (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Close.
		@param dateclose Date Close	  */
	public void setdateclose (Timestamp dateclose)
	{
		set_Value (COLUMNNAME_dateclose, dateclose);
	}

	/** Get Date Close.
		@return Date Close	  */
	public Timestamp getdateclose () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateclose);
	}

	/** Set Date Open.
		@param dateopen Date Open	  */
	public void setdateopen (Timestamp dateopen)
	{
		set_Value (COLUMNNAME_dateopen, dateopen);
	}

	/** Get Date Open.
		@return Date Open	  */
	public Timestamp getdateopen () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateopen);
	}

	/** Set Duration.
		@param Duration 
		Normal Duration in Duration Unit
	  */
	public void setDuration (BigDecimal Duration)
	{
		set_Value (COLUMNNAME_Duration, Duration);
	}

	/** Get Duration.
		@return Normal Duration in Duration Unit
	  */
	public BigDecimal getDuration () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Duration);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kms.
		@param kms Kms	  */
	public void setkms (BigDecimal kms)
	{
		set_Value (COLUMNNAME_kms, kms);
	}

	/** Get Kms.
		@return Kms	  */
	public BigDecimal getkms () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_kms);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Locator.
		@param locator_ID Locator	  */
	public void setlocator_ID (int locator_ID)
	{
		if (locator_ID < 1) 
			set_Value (COLUMNNAME_locator_ID, null);
		else 
			set_Value (COLUMNNAME_locator_ID, Integer.valueOf(locator_ID));
	}

	/** Get Locator.
		@return Locator	  */
	public int getlocator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Observations.
		@param observations Observations	  */
	public void setobservations (String observations)
	{
		set_Value (COLUMNNAME_observations, observations);
	}

	/** Get Observations.
		@return Observations	  */
	public String getobservations () 
	{
		return (String)get_Value(COLUMNNAME_observations);
	}

	/** Set Price.
		@param Price 
		Price
	  */
	public void setPrice (BigDecimal Price)
	{
		set_Value (COLUMNNAME_Price, Price);
	}

	/** Get Price.
		@return Price
	  */
	public BigDecimal getPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Price);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Provider.
		@param provider_ID Provider	  */
	public void setprovider_ID (int provider_ID)
	{
		if (provider_ID < 1) 
			set_Value (COLUMNNAME_provider_ID, null);
		else 
			set_Value (COLUMNNAME_provider_ID, Integer.valueOf(provider_ID));
	}

	/** Get Provider.
		@return Provider	  */
	public int getprovider_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_provider_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Purchase Price.
		@param purchasePrice Purchase Price	  */
	public void setpurchasePrice (BigDecimal purchasePrice)
	{
		set_Value (COLUMNNAME_purchasePrice, purchasePrice);
	}

	/** Get Purchase Price.
		@return Purchase Price	  */
	public BigDecimal getpurchasePrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_purchasePrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException
    {
		return (org.compiere.model.I_R_Request)MTable.get(getCtx(), org.compiere.model.I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Estimated Time.
		@param smj_estimatedtime Estimated Time	  */
	public void setsmj_estimatedtime (BigDecimal smj_estimatedtime)
	{
		set_Value (COLUMNNAME_smj_estimatedtime, smj_estimatedtime);
	}

	/** Get Estimated Time.
		@return Estimated Time	  */
	public BigDecimal getsmj_estimatedtime () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_estimatedtime);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Allow Changes.
		@param smj_isallowchanges Allow Changes	  */
	public void setsmj_isallowchanges (boolean smj_isallowchanges)
	{
		set_Value (COLUMNNAME_smj_isallowchanges, Boolean.valueOf(smj_isallowchanges));
	}

	/** Get Allow Changes.
		@return Allow Changes	  */
	public boolean issmj_isallowchanges () 
	{
		Object oo = get_Value(COLUMNNAME_smj_isallowchanges);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Commission.
		@param smj_iscommission Commission	  */
	public void setsmj_iscommission (boolean smj_iscommission)
	{
		set_Value (COLUMNNAME_smj_iscommission, Boolean.valueOf(smj_iscommission));
	}

	/** Get Commission.
		@return Commission	  */
	public boolean issmj_iscommission () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iscommission);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set External Service.
		@param smj_isexternallservice External Service	  */
	public void setsmj_isexternallservice (boolean smj_isexternallservice)
	{
		set_Value (COLUMNNAME_smj_isexternallservice, Boolean.valueOf(smj_isexternallservice));
	}

	/** Get External Service.
		@return External Service	  */
	public boolean issmj_isexternallservice () 
	{
		Object oo = get_Value(COLUMNNAME_smj_isexternallservice);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Service.
		@param smj_isservice Is Service	  */
	public void setsmj_isservice (boolean smj_isservice)
	{
		set_Value (COLUMNNAME_smj_isservice, Boolean.valueOf(smj_isservice));
	}

	/** Get Is Service.
		@return Is Service	  */
	public boolean issmj_isservice () 
	{
		Object oo = get_Value(COLUMNNAME_smj_isservice);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Warranty.
		@param smj_iswarranty Is Warranty	  */
	public void setsmj_iswarranty (boolean smj_iswarranty)
	{
		set_Value (COLUMNNAME_smj_iswarranty, Boolean.valueOf(smj_iswarranty));
	}

	/** Get Is Warranty.
		@return Is Warranty	  */
	public boolean issmj_iswarranty () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iswarranty);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Warranty Applied.
		@param smj_iswarrantyapplied Is Warranty Applied	  */
	public void setsmj_iswarrantyapplied (boolean smj_iswarrantyapplied)
	{
		set_Value (COLUMNNAME_smj_iswarrantyapplied, Boolean.valueOf(smj_iswarrantyapplied));
	}

	/** Get Is Warranty Applied.
		@return Is Warranty Applied	  */
	public boolean issmj_iswarrantyapplied () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iswarrantyapplied);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Work Order Line Parent.
		@param smj_workorderline_parent_ID Work Order Line Parent	  */
	public void setsmj_workorderline_parent_ID (int smj_workorderline_parent_ID)
	{
		if (smj_workorderline_parent_ID < 1) 
			set_Value (COLUMNNAME_smj_workorderline_parent_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workorderline_parent_ID, Integer.valueOf(smj_workorderline_parent_ID));
	}

	/** Get Work Order Line Parent.
		@return Work Order Line Parent	  */
	public int getsmj_workorderline_parent_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workorderline_parent_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tax Value.
		@param taxvalue Tax Value	  */
	public void settaxvalue (BigDecimal taxvalue)
	{
		set_Value (COLUMNNAME_taxvalue, taxvalue);
	}

	/** Get Tax Value.
		@return Tax Value	  */
	public BigDecimal gettaxvalue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_taxvalue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total.
		@param total Total	  */
	public void settotal (BigDecimal total)
	{
		set_Value (COLUMNNAME_total, total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal gettotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set State Work Order Line.
		@param wolstate State Work Order Line	  */
	public void setwolstate (String wolstate)
	{
		set_Value (COLUMNNAME_wolstate, wolstate);
	}

	/** Get State Work Order Line.
		@return State Work Order Line	  */
	public String getwolstate () 
	{
		return (String)get_Value(COLUMNNAME_wolstate);
	}
}