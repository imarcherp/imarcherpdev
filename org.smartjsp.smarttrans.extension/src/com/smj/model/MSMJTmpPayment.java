package com.smj.model;

import java.util.Properties;

public class MSMJTmpPayment extends X_smj_tmpPayment{

	/**
	 * MSMJTmpPayment constructir
	 */
	private static final long serialVersionUID = 2686959859141040590L;

	public MSMJTmpPayment(Properties ctx, int smj_tmpPayment_ID, String trxName) {
		super(ctx, smj_tmpPayment_ID, trxName);
	}

}//MSMJTmpPayment
