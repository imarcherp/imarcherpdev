/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_remittances
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_remittances extends PO implements I_smj_remittances, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20140403L;

    /** Standard Constructor */
    public X_smj_remittances (Properties ctx, int smj_remittances_ID, String trxName)
    {
      super (ctx, smj_remittances_ID, trxName);
      /** if (smj_remittances_ID == 0)
        {
			setR_Request_ID (0);
			setsmj_remittances_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_remittances (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_remittances[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getc_bpartnerdriver() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getc_bpartnerdriver_ID(), get_TrxName());	}

	/** Set Partner Driver.
		@param c_bpartnerdriver_ID Partner Driver	  */
	public void setc_bpartnerdriver_ID (int c_bpartnerdriver_ID)
	{
		throw new IllegalArgumentException ("c_bpartnerdriver_ID is virtual column");	}

	/** Get Partner Driver.
		@return Partner Driver	  */
	public int getc_bpartnerdriver_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_c_bpartnerdriver_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Address 1.
		@param receiveraddress 
		Address line 1 for this location
	  */
	public void setreceiveraddress (String receiveraddress)
	{
		throw new IllegalArgumentException ("receiveraddress is virtual column");	}

	/** Get Address 1.
		@return Address line 1 for this location
	  */
	public String getreceiveraddress () 
	{
		return (String)get_Value(COLUMNNAME_receiveraddress);
	}

	public I_C_Location getreceiverendloc() throws RuntimeException
    {
		return (I_C_Location)MTable.get(getCtx(), I_C_Location.Table_Name)
			.getPO(getreceiverendloc_ID(), get_TrxName());	}

	/** Set Receiver End Localization.
		@param receiverendloc_ID Receiver End Localization	  */
	public void setreceiverendloc_ID (int receiverendloc_ID)
	{
		if (receiverendloc_ID < 1) 
			set_Value (COLUMNNAME_receiverendloc_ID, null);
		else 
			set_Value (COLUMNNAME_receiverendloc_ID, Integer.valueOf(receiverendloc_ID));
	}

	/** Get Receiver End Localization.
		@return Receiver End Localization	  */
	public int getreceiverendloc_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_receiverendloc_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Receiver Name.
		@param receivername Receiver Name	  */
	public void setreceivername (String receivername)
	{
		set_Value (COLUMNNAME_receivername, receivername);
	}

	/** Get Receiver Name.
		@return Receiver Name	  */
	public String getreceivername () 
	{
		return (String)get_Value(COLUMNNAME_receivername);
	}

	/** Set Receiver Phone.
		@param receiverphone Receiver Phone	  */
	public void setreceiverphone (String receiverphone)
	{
		set_Value (COLUMNNAME_receiverphone, receiverphone);
	}

	/** Get Receiver Phone.
		@return Receiver Phone	  */
	public String getreceiverphone () 
	{
		return (String)get_Value(COLUMNNAME_receiverphone);
	}

	/** Set Referral Number.
		@param referralnumber Referral Number	  */
	public void setreferralnumber (String referralnumber)
	{
		set_Value (COLUMNNAME_referralnumber, referralnumber);
	}

	/** Get Referral Number.
		@return Referral Number	  */
	public String getreferralnumber () 
	{
		return (String)get_Value(COLUMNNAME_referralnumber);
	}

	/** Set Remittance Date.
		@param remittancedate Remittance Date	  */
	public void setremittancedate (Timestamp remittancedate)
	{
		set_Value (COLUMNNAME_remittancedate, remittancedate);
	}

	/** Get Remittance Date.
		@return Remittance Date	  */
	public Timestamp getremittancedate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_remittancedate);
	}

	/** Set remittancenumber.
		@param remittancenumber remittancenumber	  */
	public void setremittancenumber (String remittancenumber)
	{
		set_Value (COLUMNNAME_remittancenumber, remittancenumber);
	}

	/** Get remittancenumber.
		@return remittancenumber	  */
	public String getremittancenumber () 
	{
		return (String)get_Value(COLUMNNAME_remittancenumber);
	}

	/** Set Address 1.
		@param senderaddress 
		Address line 1 for this location
	  */
	public void setsenderaddress (String senderaddress)
	{
		throw new IllegalArgumentException ("senderaddress is virtual column");	}

	/** Get Address 1.
		@return Address line 1 for this location
	  */
	public String getsenderaddress () 
	{
		return (String)get_Value(COLUMNNAME_senderaddress);
	}

	/** Set sendername.
		@param sendername sendername	  */
	public void setsendername (String sendername)
	{
		set_Value (COLUMNNAME_sendername, sendername);
	}

	/** Get sendername.
		@return sendername	  */
	public String getsendername () 
	{
		return (String)get_Value(COLUMNNAME_sendername);
	}

	/** Set Sender Phone.
		@param senderphone Sender Phone	  */
	public void setsenderphone (String senderphone)
	{
		set_Value (COLUMNNAME_senderphone, senderphone);
	}

	/** Get Sender Phone.
		@return Sender Phone	  */
	public String getsenderphone () 
	{
		return (String)get_Value(COLUMNNAME_senderphone);
	}

	public I_C_Location getsenderstartloc() throws RuntimeException
    {
		return (I_C_Location)MTable.get(getCtx(), I_C_Location.Table_Name)
			.getPO(getsenderstartloc_ID(), get_TrxName());	}

	/** Set Sender Start Localization.
		@param senderstartloc_ID Sender Start Localization	  */
	public void setsenderstartloc_ID (int senderstartloc_ID)
	{
		if (senderstartloc_ID < 1) 
			set_Value (COLUMNNAME_senderstartloc_ID, null);
		else 
			set_Value (COLUMNNAME_senderstartloc_ID, Integer.valueOf(senderstartloc_ID));
	}

	/** Get Sender Start Localization.
		@return Sender Start Localization	  */
	public int getsenderstartloc_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_senderstartloc_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** smj_internalnumber AD_Reference_ID=1000050 */
	public static final int SMJ_INTERNALNUMBER_AD_Reference_ID=1000050;
	/** Set Internal Number.
		@param smj_internalnumber Internal Number	  */
	public void setsmj_internalnumber (String smj_internalnumber)
	{

		set_Value (COLUMNNAME_smj_internalnumber, smj_internalnumber);
	}

	/** Get Internal Number.
		@return Internal Number	  */
	public String getsmj_internalnumber () 
	{
		return (String)get_Value(COLUMNNAME_smj_internalnumber);
	}

	/** Set Merchandise Description.
		@param smj_merchandise Merchandise Description	  */
	public void setsmj_merchandise (String smj_merchandise)
	{
		set_Value (COLUMNNAME_smj_merchandise, smj_merchandise);
	}

	/** Get Merchandise Description.
		@return Merchandise Description	  */
	public String getsmj_merchandise () 
	{
		return (String)get_Value(COLUMNNAME_smj_merchandise);
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		throw new IllegalArgumentException ("smj_plate is virtual column");	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	/** Set Remittances.
		@param smj_remittances_ID Remittances	  */
	public void setsmj_remittances_ID (int smj_remittances_ID)
	{
		if (smj_remittances_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_remittances_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_remittances_ID, Integer.valueOf(smj_remittances_ID));
	}

	/** Get Remittances.
		@return Remittances	  */
	public int getsmj_remittances_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_remittances_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Volume.
		@param smj_volume Volume	  */
	public void setsmj_volume (BigDecimal smj_volume)
	{
		set_Value (COLUMNNAME_smj_volume, smj_volume);
	}

	/** Get Volume.
		@return Volume	  */
	public BigDecimal getsmj_volume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_volume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Weight.
		@param smj_weight Weight	  */
	public void setsmj_weight (BigDecimal smj_weight)
	{
		set_Value (COLUMNNAME_smj_weight, smj_weight);
	}

	/** Get Weight.
		@return Weight	  */
	public BigDecimal getsmj_weight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_weight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}