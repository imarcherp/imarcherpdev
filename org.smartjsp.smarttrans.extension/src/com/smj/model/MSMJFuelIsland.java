package com.smj.model;

import java.util.Properties;

public class MSMJFuelIsland extends X_smj_FuelIsland {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8301709279017817658L;

	/***
	 * 
	 * @param ctx
	 * @param smj_FuelIsland_ID
	 * @param trxName
	 */
	public MSMJFuelIsland(Properties ctx, int smj_FuelIsland_ID, String trxName) {
		super(ctx, smj_FuelIsland_ID, trxName);
	}//MSMJFuelIsland

}//MSMJFuelIsland
