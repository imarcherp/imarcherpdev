package com.smj.model;

import java.util.Properties;

public class MSMJChangeTire extends X_smj_changeTire {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1354993591746338101L;

	/**
	 * MSMJChangeTire constructor
	 * @param ctx
	 * @param smj_changeTire_ID
	 * @param trxName
	 */
	public MSMJChangeTire(Properties ctx, int smj_changeTire_ID, String trxName) {
		super(ctx, smj_changeTire_ID, trxName);
	}

	
}//smj_changeTire
