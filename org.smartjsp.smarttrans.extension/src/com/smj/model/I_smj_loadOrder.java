/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_loadOrder
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_loadOrder 
{

    /** TableName=smj_loadOrder */
    public static final String Table_Name = "smj_loadOrder";

    /** AD_Table_ID=1000051 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name c_bpartnerdriver_ID */
    public static final String COLUMNNAME_c_bpartnerdriver_ID = "c_bpartnerdriver_ID";

	/** Set Partner Driver	  */
	public void setc_bpartnerdriver_ID (int c_bpartnerdriver_ID);

	/** Get Partner Driver	  */
	public int getc_bpartnerdriver_ID();

	public I_C_BPartner getc_bpartnerdriver() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name orderdate */
    public static final String COLUMNNAME_orderdate = "orderdate";

	/** Set Order Date	  */
	public void setorderdate (Timestamp orderdate);

	/** Get Order Date	  */
	public Timestamp getorderdate();

    /** Column name ordernumber */
    public static final String COLUMNNAME_ordernumber = "ordernumber";

	/** Set Order Number	  */
	public void setordernumber (String ordernumber);

	/** Get Order Number	  */
	public String getordernumber();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public I_R_Request getR_Request() throws RuntimeException;

    /** Column name sendername */
    public static final String COLUMNNAME_sendername = "sendername";

	/** Set Sender Name	  */
	public void setsendername (String sendername);

	/** Get Sender Name	  */
	public String getsendername();

    /** Column name smj_loadOrder_ID */
    public static final String COLUMNNAME_smj_loadOrder_ID = "smj_loadOrder_ID";

	/** Set Load Order	  */
	public void setsmj_loadOrder_ID (int smj_loadOrder_ID);

	/** Get Load Order	  */
	public int getsmj_loadOrder_ID();

    /** Column name smj_mainstartloc_ID */
    public static final String COLUMNNAME_smj_mainstartloc_ID = "smj_mainstartloc_ID";

	/** Set Main Start Localization	  */
	public void setsmj_mainstartloc_ID (int smj_mainstartloc_ID);

	/** Get Main Start Localization	  */
	public int getsmj_mainstartloc_ID();

	public I_C_Location getsmj_mainstartloc() throws RuntimeException;

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set Plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get Plate	  */
	public String getsmj_plate();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
