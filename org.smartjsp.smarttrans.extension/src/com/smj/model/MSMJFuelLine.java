package com.smj.model;

import java.util.Properties;

public class MSMJFuelLine extends X_smj_FuelLine{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3718342617382919734L;

	/**
	 * constructor MSMJFuelLine
	 * @param ctx
	 * @param smj_FuelLine_ID
	 * @param trxName
	 */
	public MSMJFuelLine(Properties ctx, int smj_FuelLine_ID, String trxName) {
		super(ctx, smj_FuelLine_ID, trxName);
	}//MSMJFuelLine constructor

}//MSMJFuelLine
