/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_HistoryTire
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_HistoryTire extends PO implements I_smj_HistoryTire, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20131226L;

    /** Standard Constructor */
    public X_smj_HistoryTire (Properties ctx, int smj_HistoryTire_ID, String trxName)
    {
      super (ctx, smj_HistoryTire_ID, trxName);
      /** if (smj_HistoryTire_ID == 0)
        {
			setdatemount (new Timestamp( System.currentTimeMillis() ));
			setdateumount (new Timestamp( System.currentTimeMillis() ));
			setM_Product_ID (0);
			setPosition (0);
			setsmj_changeTire_ID (0);
			setsmj_HistoryTire_ID (0);
			setsmj_vehicle_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_HistoryTire (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_HistoryTire[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Mount.
		@param datemount Date Mount	  */
	public void setdatemount (Timestamp datemount)
	{
		set_Value (COLUMNNAME_datemount, datemount);
	}

	/** Get Date Mount.
		@return Date Mount	  */
	public Timestamp getdatemount () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datemount);
	}

	/** Set Date Umount.
		@param dateumount Date Umount	  */
	public void setdateumount (Timestamp dateumount)
	{
		set_Value (COLUMNNAME_dateumount, dateumount);
	}

	/** Get Date Umount.
		@return Date Umount	  */
	public Timestamp getdateumount () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateumount);
	}

	/** Set Kms.
		@param kms Kms	  */
	public void setkms (BigDecimal kms)
	{
		set_Value (COLUMNNAME_kms, kms);
	}

	/** Get Kms.
		@return Kms	  */
	public BigDecimal getkms () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_kms);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Position.
		@param Position Position	  */
	public void setPosition (int Position)
	{
		set_Value (COLUMNNAME_Position, Integer.valueOf(Position));
	}

	/** Get Position.
		@return Position	  */
	public int getPosition () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Position);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Serial.
		@param serial Serial	  */
	public void setserial (int serial)
	{
		set_Value (COLUMNNAME_serial, Integer.valueOf(serial));
	}

	/** Get Serial.
		@return Serial	  */
	public int getserial () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_serial);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_changeTire getsmj_changeTire() throws RuntimeException
    {
		return (I_smj_changeTire)MTable.get(getCtx(), I_smj_changeTire.Table_Name)
			.getPO(getsmj_changeTire_ID(), get_TrxName());	}

	/** Set Change Tire.
		@param smj_changeTire_ID Change Tire	  */
	public void setsmj_changeTire_ID (int smj_changeTire_ID)
	{
		if (smj_changeTire_ID < 1) 
			set_Value (COLUMNNAME_smj_changeTire_ID, null);
		else 
			set_Value (COLUMNNAME_smj_changeTire_ID, Integer.valueOf(smj_changeTire_ID));
	}

	/** Get Change Tire.
		@return Change Tire	  */
	public int getsmj_changeTire_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_changeTire_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set History Tire.
		@param smj_HistoryTire_ID History Tire	  */
	public void setsmj_HistoryTire_ID (int smj_HistoryTire_ID)
	{
		if (smj_HistoryTire_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_HistoryTire_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_HistoryTire_ID, Integer.valueOf(smj_HistoryTire_ID));
	}

	/** Get History Tire.
		@return History Tire	  */
	public int getsmj_HistoryTire_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_HistoryTire_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException
    {
		return (I_SMJ_Vehicle)MTable.get(getCtx(), I_SMJ_Vehicle.Table_Name)
			.getPO(getsmj_vehicle_ID(), get_TrxName());	}

	/** Set Vehicle.
		@param smj_vehicle_ID Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID)
	{
		if (smj_vehicle_ID < 1) 
			set_Value (COLUMNNAME_smj_vehicle_ID, null);
		else 
			set_Value (COLUMNNAME_smj_vehicle_ID, Integer.valueOf(smj_vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getsmj_vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehicle_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException
    {
		return (I_smj_workOrderLine)MTable.get(getCtx(), I_smj_workOrderLine.Table_Name)
			.getPO(getsmj_workOrderLine_ID(), get_TrxName());	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tire State.
		@param tirestate Tire State	  */
	public void settirestate (String tirestate)
	{
		set_Value (COLUMNNAME_tirestate, tirestate);
	}

	/** Get Tire State.
		@return Tire State	  */
	public String gettirestate () 
	{
		return (String)get_Value(COLUMNNAME_tirestate);
	}

	/** Set Value Brake.
		@param valuebrake Value Brake	  */
	public void setvaluebrake (BigDecimal valuebrake)
	{
		set_Value (COLUMNNAME_valuebrake, valuebrake);
	}

	/** Get Value Brake.
		@return Value Brake	  */
	public BigDecimal getvaluebrake () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_valuebrake);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Value Tire.
		@param valuetire Value Tire	  */
	public void setvaluetire (BigDecimal valuetire)
	{
		set_Value (COLUMNNAME_valuetire, valuetire);
	}

	/** Get Value Tire.
		@return Value Tire	  */
	public BigDecimal getvaluetire () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_valuetire);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}