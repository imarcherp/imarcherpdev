/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for smj_fuellinehistory
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_fuellinehistory extends PO implements I_smj_fuellinehistory, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130830L;

    /** Standard Constructor */
    public X_smj_fuellinehistory (Properties ctx, int smj_fuellinehistory_ID, String trxName)
    {
      super (ctx, smj_fuellinehistory_ID, trxName);
      /** if (smj_fuellinehistory_ID == 0)
        {
			setactualhundred (Env.ZERO);
			setdatechange (new Timestamp( System.currentTimeMillis() ));
			setlimithundred (Env.ZERO);
			setName (null);
			setsmj_FuelIsland_ID (0);
			setsmj_FuelLine_ID (0);
			setsmj_fuellinehistory_ID (0);
			setsmj_rchangefuelline_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_fuellinehistory (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_fuellinehistory[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Actual Hundred.
		@param actualhundred Actual Hundred	  */
	public void setactualhundred (BigDecimal actualhundred)
	{
		set_Value (COLUMNNAME_actualhundred, actualhundred);
	}

	/** Get Actual Hundred.
		@return Actual Hundred	  */
	public BigDecimal getactualhundred () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_actualhundred);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Date Change.
		@param datechange Date Change	  */
	public void setdatechange (Timestamp datechange)
	{
		set_Value (COLUMNNAME_datechange, datechange);
	}

	/** Get Date Change.
		@return Date Change	  */
	public Timestamp getdatechange () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datechange);
	}

	/** Set Limit Hundred.
		@param limithundred Limit Hundred	  */
	public void setlimithundred (BigDecimal limithundred)
	{
		set_Value (COLUMNNAME_limithundred, limithundred);
	}

	/** Get Limit Hundred.
		@return Limit Hundred	  */
	public BigDecimal getlimithundred () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_limithundred);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	public I_smj_FuelIsland getsmj_FuelIsland() throws RuntimeException
    {
		return (I_smj_FuelIsland)MTable.get(getCtx(), I_smj_FuelIsland.Table_Name)
			.getPO(getsmj_FuelIsland_ID(), get_TrxName());	}

	/** Set Fuel Island.
		@param smj_FuelIsland_ID Fuel Island	  */
	public void setsmj_FuelIsland_ID (int smj_FuelIsland_ID)
	{
		if (smj_FuelIsland_ID < 1) 
			set_Value (COLUMNNAME_smj_FuelIsland_ID, null);
		else 
			set_Value (COLUMNNAME_smj_FuelIsland_ID, Integer.valueOf(smj_FuelIsland_ID));
	}

	/** Get Fuel Island.
		@return Fuel Island	  */
	public int getsmj_FuelIsland_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_FuelIsland_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_FuelLine getsmj_FuelLine() throws RuntimeException
    {
		return (I_smj_FuelLine)MTable.get(getCtx(), I_smj_FuelLine.Table_Name)
			.getPO(getsmj_FuelLine_ID(), get_TrxName());	}

	/** Set Fuel Line.
		@param smj_FuelLine_ID Fuel Line	  */
	public void setsmj_FuelLine_ID (int smj_FuelLine_ID)
	{
		if (smj_FuelLine_ID < 1) 
			set_Value (COLUMNNAME_smj_FuelLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_FuelLine_ID, Integer.valueOf(smj_FuelLine_ID));
	}

	/** Get Fuel Line.
		@return Fuel Line	  */
	public int getsmj_FuelLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_FuelLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Fuel Line History.
		@param smj_fuellinehistory_ID Fuel Line History	  */
	public void setsmj_fuellinehistory_ID (int smj_fuellinehistory_ID)
	{
		if (smj_fuellinehistory_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_fuellinehistory_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_fuellinehistory_ID, Integer.valueOf(smj_fuellinehistory_ID));
	}

	/** Get Fuel Line History.
		@return Fuel Line History	  */
	public int getsmj_fuellinehistory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_fuellinehistory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_rchangefuelline getsmj_rchangefuelline() throws RuntimeException
    {
		return (I_smj_rchangefuelline)MTable.get(getCtx(), I_smj_rchangefuelline.Table_Name)
			.getPO(getsmj_rchangefuelline_ID(), get_TrxName());	}

	/** Set Reason Change FuelLine.
		@param smj_rchangefuelline_ID Reason Change FuelLine	  */
	public void setsmj_rchangefuelline_ID (int smj_rchangefuelline_ID)
	{
		if (smj_rchangefuelline_ID < 1) 
			set_Value (COLUMNNAME_smj_rchangefuelline_ID, null);
		else 
			set_Value (COLUMNNAME_smj_rchangefuelline_ID, Integer.valueOf(smj_rchangefuelline_ID));
	}

	/** Get Reason Change FuelLine.
		@return Reason Change FuelLine	  */
	public int getsmj_rchangefuelline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_rchangefuelline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}