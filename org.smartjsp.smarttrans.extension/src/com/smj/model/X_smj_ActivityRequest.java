/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for smj_ActivityRequest
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_ActivityRequest extends PO implements I_smj_ActivityRequest, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130510L;

    /** Standard Constructor */
    public X_smj_ActivityRequest (Properties ctx, int smj_ActivityRequest_ID, String trxName)
    {
      super (ctx, smj_ActivityRequest_ID, trxName);
      /** if (smj_ActivityRequest_ID == 0)
        {
			setdateprogram (new Timestamp( System.currentTimeMillis() ));
			setM_Product_ID (0);
			setR_Request_ID (0);
			setsmj_ActivityRequest_ID (0);
			setsmj_vehicle_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_ActivityRequest (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_ActivityRequest[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date Program.
		@param dateprogram Date Program	  */
	public void setdateprogram (Timestamp dateprogram)
	{
		set_Value (COLUMNNAME_dateprogram, dateprogram);
	}

	/** Get Date Program.
		@return Date Program	  */
	public Timestamp getdateprogram () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateprogram);
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Activity Request.
		@param smj_ActivityRequest_ID Activity Request	  */
	public void setsmj_ActivityRequest_ID (int smj_ActivityRequest_ID)
	{
		if (smj_ActivityRequest_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_ActivityRequest_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_ActivityRequest_ID, Integer.valueOf(smj_ActivityRequest_ID));
	}

	/** Get Activity Request.
		@return Activity Request	  */
	public int getsmj_ActivityRequest_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_ActivityRequest_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException
    {
		return (I_SMJ_Vehicle)MTable.get(getCtx(), I_SMJ_Vehicle.Table_Name)
			.getPO(getsmj_vehicle_ID(), get_TrxName());	}

	/** Set Vehicle.
		@param smj_vehicle_ID Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID)
	{
		if (smj_vehicle_ID < 1) 
			set_Value (COLUMNNAME_smj_vehicle_ID, null);
		else 
			set_Value (COLUMNNAME_smj_vehicle_ID, Integer.valueOf(smj_vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getsmj_vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehicle_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}