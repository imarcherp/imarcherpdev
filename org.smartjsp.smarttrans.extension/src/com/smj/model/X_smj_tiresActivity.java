/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_tiresActivity
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_tiresActivity extends PO implements I_smj_tiresActivity, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130510L;

    /** Standard Constructor */
    public X_smj_tiresActivity (Properties ctx, int smj_tiresActivity_ID, String trxName)
    {
      super (ctx, smj_tiresActivity_ID, trxName);
      /** if (smj_tiresActivity_ID == 0)
        {
			setdateactivity (new Timestamp( System.currentTimeMillis() ));
			setPosition (0);
			setsmj_tires_ID (0);
			setsmj_tiresActivity_ID (0);
			setsmj_workOrderLine_ID (0);
			setvaluebrake (Env.ZERO);
			setvaluetire (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_tiresActivity (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_tiresActivity[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date Activity.
		@param dateactivity Date Activity	  */
	public void setdateactivity (Timestamp dateactivity)
	{
		set_Value (COLUMNNAME_dateactivity, dateactivity);
	}

	/** Get Date Activity.
		@return Date Activity	  */
	public Timestamp getdateactivity () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateactivity);
	}

	/** Set Position.
		@param Position Position	  */
	public void setPosition (int Position)
	{
		set_Value (COLUMNNAME_Position, Integer.valueOf(Position));
	}

	/** Get Position.
		@return Position	  */
	public int getPosition () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Position);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_tires getsmj_tires() throws RuntimeException
    {
		return (I_smj_tires)MTable.get(getCtx(), I_smj_tires.Table_Name)
			.getPO(getsmj_tires_ID(), get_TrxName());	}

	/** Set Tires.
		@param smj_tires_ID Tires	  */
	public void setsmj_tires_ID (int smj_tires_ID)
	{
		if (smj_tires_ID < 1) 
			set_Value (COLUMNNAME_smj_tires_ID, null);
		else 
			set_Value (COLUMNNAME_smj_tires_ID, Integer.valueOf(smj_tires_ID));
	}

	/** Get Tires.
		@return Tires	  */
	public int getsmj_tires_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tires_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tires Activity.
		@param smj_tiresActivity_ID Tires Activity	  */
	public void setsmj_tiresActivity_ID (int smj_tiresActivity_ID)
	{
		if (smj_tiresActivity_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_tiresActivity_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_tiresActivity_ID, Integer.valueOf(smj_tiresActivity_ID));
	}

	/** Get Tires Activity.
		@return Tires Activity	  */
	public int getsmj_tiresActivity_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tiresActivity_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException
    {
		return (I_smj_workOrderLine)MTable.get(getCtx(), I_smj_workOrderLine.Table_Name)
			.getPO(getsmj_workOrderLine_ID(), get_TrxName());	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Value Brake.
		@param valuebrake Value Brake	  */
	public void setvaluebrake (BigDecimal valuebrake)
	{
		set_Value (COLUMNNAME_valuebrake, valuebrake);
	}

	/** Get Value Brake.
		@return Value Brake	  */
	public BigDecimal getvaluebrake () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_valuebrake);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Value Tire.
		@param valuetire Value Tire	  */
	public void setvaluetire (BigDecimal valuetire)
	{
		set_Value (COLUMNNAME_valuetire, valuetire);
	}

	/** Get Value Tire.
		@return Value Tire	  */
	public BigDecimal getvaluetire () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_valuetire);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}