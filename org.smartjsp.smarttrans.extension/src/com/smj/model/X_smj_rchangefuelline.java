/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for smj_rchangefuelline
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_rchangefuelline extends PO implements I_smj_rchangefuelline, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130829L;

    /** Standard Constructor */
    public X_smj_rchangefuelline (Properties ctx, int smj_rchangefuelline_ID, String trxName)
    {
      super (ctx, smj_rchangefuelline_ID, trxName);
      /** if (smj_rchangefuelline_ID == 0)
        {
			setName (null);
			setsmj_rchangefuelline_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_rchangefuelline (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_rchangefuelline[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Reason Change FuelLine.
		@param smj_rchangefuelline_ID Reason Change FuelLine	  */
	public void setsmj_rchangefuelline_ID (int smj_rchangefuelline_ID)
	{
		if (smj_rchangefuelline_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_rchangefuelline_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_rchangefuelline_ID, Integer.valueOf(smj_rchangefuelline_ID));
	}

	/** Get Reason Change FuelLine.
		@return Reason Change FuelLine	  */
	public int getsmj_rchangefuelline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_rchangefuelline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}