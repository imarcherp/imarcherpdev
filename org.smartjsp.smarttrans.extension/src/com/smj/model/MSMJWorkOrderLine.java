package com.smj.model;

import java.util.Properties;

public class MSMJWorkOrderLine extends X_smj_workOrderLine{

	private String warehouseName;
	private String productName;
	private String productValue;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2224721604456124192L;

	/**
	 * MSMJWorkOrderLine constructor
	 * @param ctx
	 * @param smj_workOrderLine_ID
	 * @param trxName
	 */
	public MSMJWorkOrderLine(Properties ctx, int smj_workOrderLine_ID,
			String trxName) {
		super(ctx, smj_workOrderLine_ID, trxName);
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductValue() {
		return productValue;
	}

	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}

	
}//MSMJWorkOrderLine
