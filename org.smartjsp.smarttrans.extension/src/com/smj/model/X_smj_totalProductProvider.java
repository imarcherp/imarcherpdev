/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_totalProductProvider
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_totalProductProvider extends PO implements I_smj_totalProductProvider, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130216L;

    /** Standard Constructor */
    public X_smj_totalProductProvider (Properties ctx, int smj_totalProductProvider_ID, String trxName)
    {
      super (ctx, smj_totalProductProvider_ID, trxName);
      /** if (smj_totalProductProvider_ID == 0)
        {
			setC_BPartner_ID (0);
			setM_Product_ID (0);
			setsmj_total (Env.ZERO);
			setsmj_totalProductProvider_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_totalProductProvider (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_totalProductProvider[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total.
		@param smj_total Total	  */
	public void setsmj_total (BigDecimal smj_total)
	{
		set_Value (COLUMNNAME_smj_total, smj_total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal getsmj_total () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Product Provider.
		@param smj_totalProductProvider_ID Total Product Provider	  */
	public void setsmj_totalProductProvider_ID (int smj_totalProductProvider_ID)
	{
		if (smj_totalProductProvider_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_totalProductProvider_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_totalProductProvider_ID, Integer.valueOf(smj_totalProductProvider_ID));
	}

	/** Get Total Product Provider.
		@return Total Product Provider	  */
	public int getsmj_totalProductProvider_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_totalProductProvider_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}