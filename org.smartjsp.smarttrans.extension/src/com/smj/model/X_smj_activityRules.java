/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for smj_activityRules
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_activityRules extends PO implements I_smj_activityRules, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130422L;

    /** Standard Constructor */
    public X_smj_activityRules (Properties ctx, int smj_activityRules_ID, String trxName)
    {
      super (ctx, smj_activityRules_ID, trxName);
      /** if (smj_activityRules_ID == 0)
        {
			setM_Product_ID (0);
			setsmj_activityRules_ID (0);
			setsmj_ScheduleRules_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_activityRules (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_activityRules[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Activity Rules.
		@param smj_activityRules_ID Activity Rules	  */
	public void setsmj_activityRules_ID (int smj_activityRules_ID)
	{
		if (smj_activityRules_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_activityRules_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_activityRules_ID, Integer.valueOf(smj_activityRules_ID));
	}

	/** Get Activity Rules.
		@return Activity Rules	  */
	public int getsmj_activityRules_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_activityRules_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_ScheduleRules getsmj_ScheduleRules() throws RuntimeException
    {
		return (I_smj_ScheduleRules)MTable.get(getCtx(), I_smj_ScheduleRules.Table_Name)
			.getPO(getsmj_ScheduleRules_ID(), get_TrxName());	}

	/** Set Schedule Rules.
		@param smj_ScheduleRules_ID Schedule Rules	  */
	public void setsmj_ScheduleRules_ID (int smj_ScheduleRules_ID)
	{
		if (smj_ScheduleRules_ID < 1) 
			set_Value (COLUMNNAME_smj_ScheduleRules_ID, null);
		else 
			set_Value (COLUMNNAME_smj_ScheduleRules_ID, Integer.valueOf(smj_ScheduleRules_ID));
	}

	/** Get Schedule Rules.
		@return Schedule Rules	  */
	public int getsmj_ScheduleRules_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_ScheduleRules_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}