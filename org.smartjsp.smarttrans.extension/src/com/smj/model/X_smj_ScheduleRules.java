/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_ScheduleRules
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_ScheduleRules extends PO implements I_smj_ScheduleRules, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130516L;

    /** Standard Constructor */
    public X_smj_ScheduleRules (Properties ctx, int smj_ScheduleRules_ID, String trxName)
    {
      super (ctx, smj_ScheduleRules_ID, trxName);
      /** if (smj_ScheduleRules_ID == 0)
        {
			setName (null);
			setsmj_ScheduleRules_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_ScheduleRules (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_ScheduleRules[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (String Line)
	{
		set_Value (COLUMNNAME_Line, Line);
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public String getLine () 
	{
		return (String)get_Value(COLUMNNAME_Line);
	}

	/** Set Max Threshold.
		@param maxthreshold Max Threshold	  */
	public void setmaxthreshold (BigDecimal maxthreshold)
	{
		set_Value (COLUMNNAME_maxthreshold, maxthreshold);
	}

	/** Get Max Threshold.
		@return Max Threshold	  */
	public BigDecimal getmaxthreshold () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_maxthreshold);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Min Threshold.
		@param minthreshold Min Threshold	  */
	public void setminthreshold (BigDecimal minthreshold)
	{
		set_Value (COLUMNNAME_minthreshold, minthreshold);
	}

	/** Get Min Threshold.
		@return Min Threshold	  */
	public BigDecimal getminthreshold () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_minthreshold);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Model End.
		@param modelend Model End	  */
	public void setmodelend (int modelend)
	{
		set_Value (COLUMNNAME_modelend, Integer.valueOf(modelend));
	}

	/** Get Model End.
		@return Model End	  */
	public int getmodelend () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_modelend);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Model Start.
		@param modelstart Model Start	  */
	public void setmodelstart (int modelstart)
	{
		set_Value (COLUMNNAME_modelstart, Integer.valueOf(modelstart));
	}

	/** Get Model Start.
		@return Model Start	  */
	public int getmodelstart () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_modelstart);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** periodicity AD_Reference_ID=1000040 */
	public static final int PERIODICITY_AD_Reference_ID=1000040;
	/** Kms = K */
	public static final String PERIODICITY_Kms = "K";
	/** Time = T */
	public static final String PERIODICITY_Time = "T";
	/** Set Periodicity.
		@param periodicity Periodicity	  */
	public void setperiodicity (String periodicity)
	{

		set_Value (COLUMNNAME_periodicity, periodicity);
	}

	/** Get Periodicity.
		@return Periodicity	  */
	public String getperiodicity () 
	{
		return (String)get_Value(COLUMNNAME_periodicity);
	}

	/** Set Process.
		@param process Process	  */
	public void setprocess (String process)
	{
		set_Value (COLUMNNAME_process, process);
	}

	/** Get Process.
		@return Process	  */
	public String getprocess () 
	{
		return (String)get_Value(COLUMNNAME_process);
	}

	public I_smj_AdministrationType getsmj_AdministrationType() throws RuntimeException
    {
		return (I_smj_AdministrationType)MTable.get(getCtx(), I_smj_AdministrationType.Table_Name)
			.getPO(getsmj_AdministrationType_ID(), get_TrxName());	}

	/** Set Administration Type.
		@param smj_AdministrationType_ID Administration Type	  */
	public void setsmj_AdministrationType_ID (int smj_AdministrationType_ID)
	{
		if (smj_AdministrationType_ID < 1) 
			set_Value (COLUMNNAME_smj_AdministrationType_ID, null);
		else 
			set_Value (COLUMNNAME_smj_AdministrationType_ID, Integer.valueOf(smj_AdministrationType_ID));
	}

	/** Get Administration Type.
		@return Administration Type	  */
	public int getsmj_AdministrationType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_AdministrationType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_brand getsmj_brand() throws RuntimeException
    {
		return (I_smj_brand)MTable.get(getCtx(), I_smj_brand.Table_Name)
			.getPO(getsmj_brand_ID(), get_TrxName());	}

	/** Set Brand.
		@param smj_brand_ID Brand	  */
	public void setsmj_brand_ID (int smj_brand_ID)
	{
		if (smj_brand_ID < 1) 
			set_Value (COLUMNNAME_smj_brand_ID, null);
		else 
			set_Value (COLUMNNAME_smj_brand_ID, Integer.valueOf(smj_brand_ID));
	}

	/** Get Brand.
		@return Brand	  */
	public int getsmj_brand_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_brand_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_programmingGroup getsmj_programmingGroup() throws RuntimeException
    {
		return (I_smj_programmingGroup)MTable.get(getCtx(), I_smj_programmingGroup.Table_Name)
			.getPO(getsmj_programmingGroup_ID(), get_TrxName());	}

	/** Set Programming Group.
		@param smj_programmingGroup_ID Programming Group	  */
	public void setsmj_programmingGroup_ID (int smj_programmingGroup_ID)
	{
		if (smj_programmingGroup_ID < 1) 
			set_Value (COLUMNNAME_smj_programmingGroup_ID, null);
		else 
			set_Value (COLUMNNAME_smj_programmingGroup_ID, Integer.valueOf(smj_programmingGroup_ID));
	}

	/** Get Programming Group.
		@return Programming Group	  */
	public int getsmj_programmingGroup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_programmingGroup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Schedule Rules.
		@param smj_ScheduleRules_ID Schedule Rules	  */
	public void setsmj_ScheduleRules_ID (int smj_ScheduleRules_ID)
	{
		if (smj_ScheduleRules_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_ScheduleRules_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_ScheduleRules_ID, Integer.valueOf(smj_ScheduleRules_ID));
	}

	/** Get Schedule Rules.
		@return Schedule Rules	  */
	public int getsmj_ScheduleRules_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_ScheduleRules_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** threshold AD_Reference_ID=1000041 */
	public static final int THRESHOLD_AD_Reference_ID=1000041;
	/** Tire Depth = T */
	public static final String THRESHOLD_TireDepth = "T";
	/** Brake Depth = B */
	public static final String THRESHOLD_BrakeDepth = "B";
	/** Set Threshold.
		@param threshold Threshold	  */
	public void setthreshold (String threshold)
	{

		set_Value (COLUMNNAME_threshold, threshold);
	}

	/** Get Threshold.
		@return Threshold	  */
	public String getthreshold () 
	{
		return (String)get_Value(COLUMNNAME_threshold);
	}

	/** Set Value Periodicity.
		@param valueperiodicity Value Periodicity	  */
	public void setvalueperiodicity (BigDecimal valueperiodicity)
	{
		set_Value (COLUMNNAME_valueperiodicity, valueperiodicity);
	}

	/** Get Value Periodicity.
		@return Value Periodicity	  */
	public BigDecimal getvalueperiodicity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_valueperiodicity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** vehicletype AD_Reference_ID=1000029 */
	public static final int VEHICLETYPE_AD_Reference_ID=1000029;
	/** CAMION = C */
	public static final String VEHICLETYPE_CAMION = "C";
	/** AUTOMOVIL = A */
	public static final String VEHICLETYPE_AUTOMOVIL = "A";
	/** DOBLETROQUE = D */
	public static final String VEHICLETYPE_DOBLETROQUE = "D";
	/** TURBO = T */
	public static final String VEHICLETYPE_TURBO = "T";
	/** CAMIONETA = M */
	public static final String VEHICLETYPE_CAMIONETA = "M";
	/** BUS = B */
	public static final String VEHICLETYPE_BUS = "B";
	/** TRACTOCAMION = X */
	public static final String VEHICLETYPE_TRACTOCAMION = "X";
	/** MINIMULA = L */
	public static final String VEHICLETYPE_MINIMULA = "L";
	/** CARGA OTRO = O */
	public static final String VEHICLETYPE_CARGAOTRO = "O";
	/** REMOLQUE = R */
	public static final String VEHICLETYPE_REMOLQUE = "R";
	/** BUSETA = S */
	public static final String VEHICLETYPE_BUSETA = "S";
	/** MICROBUS = U */
	public static final String VEHICLETYPE_MICROBUS = "U";
	/** Set Vehicle Type.
		@param vehicletype Vehicle Type	  */
	public void setvehicletype (String vehicletype)
	{

		set_Value (COLUMNNAME_vehicletype, vehicletype);
	}

	/** Get Vehicle Type.
		@return Vehicle Type	  */
	public String getvehicletype () 
	{
		return (String)get_Value(COLUMNNAME_vehicletype);
	}
}