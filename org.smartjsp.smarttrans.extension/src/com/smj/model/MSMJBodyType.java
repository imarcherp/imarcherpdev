package com.smj.model;

import java.util.Properties;

public class MSMJBodyType extends X_smj_bodytype{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8500405685476153588L;

	/***
	 * MSMJBodyType constructor
	 * @param ctx
	 * @param smj_bodytype_ID
	 * @param trxName
	 */
	public MSMJBodyType(Properties ctx, int smj_bodytype_ID, String trxName) {
		super(ctx, smj_bodytype_ID, trxName);
	}

}//MSMJBodyType
