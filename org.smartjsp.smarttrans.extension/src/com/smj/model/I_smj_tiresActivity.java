/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_tiresActivity
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_tiresActivity 
{

    /** TableName=smj_tiresActivity */
    public static final String Table_Name = "smj_tiresActivity";

    /** AD_Table_ID=1000049 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name dateactivity */
    public static final String COLUMNNAME_dateactivity = "dateactivity";

	/** Set Date Activity	  */
	public void setdateactivity (Timestamp dateactivity);

	/** Get Date Activity	  */
	public Timestamp getdateactivity();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Position */
    public static final String COLUMNNAME_Position = "Position";

	/** Set Position	  */
	public void setPosition (int Position);

	/** Get Position	  */
	public int getPosition();

    /** Column name smj_tires_ID */
    public static final String COLUMNNAME_smj_tires_ID = "smj_tires_ID";

	/** Set Tires	  */
	public void setsmj_tires_ID (int smj_tires_ID);

	/** Get Tires	  */
	public int getsmj_tires_ID();

	public I_smj_tires getsmj_tires() throws RuntimeException;

    /** Column name smj_tiresActivity_ID */
    public static final String COLUMNNAME_smj_tiresActivity_ID = "smj_tiresActivity_ID";

	/** Set Tires Activity	  */
	public void setsmj_tiresActivity_ID (int smj_tiresActivity_ID);

	/** Get Tires Activity	  */
	public int getsmj_tiresActivity_ID();

    /** Column name smj_workOrderLine_ID */
    public static final String COLUMNNAME_smj_workOrderLine_ID = "smj_workOrderLine_ID";

	/** Set Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID);

	/** Get Work Order Line	  */
	public int getsmj_workOrderLine_ID();

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name valuebrake */
    public static final String COLUMNNAME_valuebrake = "valuebrake";

	/** Set Value Brake	  */
	public void setvaluebrake (BigDecimal valuebrake);

	/** Get Value Brake	  */
	public BigDecimal getvaluebrake();

    /** Column name valuetire */
    public static final String COLUMNNAME_valuetire = "valuetire";

	/** Set Value Tire	  */
	public void setvaluetire (BigDecimal valuetire);

	/** Get Value Tire	  */
	public BigDecimal getvaluetire();
}
