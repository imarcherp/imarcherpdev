/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_controlAdvances
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_controlAdvances extends PO implements I_smj_controlAdvances, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130809L;

    /** Standard Constructor */
    public X_smj_controlAdvances (Properties ctx, int smj_controlAdvances_ID, String trxName)
    {
      super (ctx, smj_controlAdvances_ID, trxName);
      /** if (smj_controlAdvances_ID == 0)
        {
			setIsApproved (false);
			setR_Request_ID (0);
			setsmj_controlAdvances_ID (0);
			setsmj_value (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_controlAdvances (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_controlAdvances[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Account No.
		@param AccountNo 
		Account Number
	  */
	public void setAccountNo (String AccountNo)
	{
		set_Value (COLUMNNAME_AccountNo, AccountNo);
	}

	/** Get Account No.
		@return Account Number
	  */
	public String getAccountNo () 
	{
		return (String)get_Value(COLUMNNAME_AccountNo);
	}

	public I_C_Payment getC_Payment() throws RuntimeException
    {
		return (I_C_Payment)MTable.get(getCtx(), I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_Value (COLUMNNAME_C_Payment_ID, null);
		else 
			set_Value (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Micr.
		@param Micr 
		Combination of routing no, account and check no
	  */
	public void setMicr (String Micr)
	{
		set_Value (COLUMNNAME_Micr, Micr);
	}

	/** Get Micr.
		@return Combination of routing no, account and check no
	  */
	public String getMicr () 
	{
		return (String)get_Value(COLUMNNAME_Micr);
	}

	/** Set payterm.
		@param payterm payterm	  */
	public void setpayterm (String payterm)
	{
		set_Value (COLUMNNAME_payterm, payterm);
	}

	/** Get payterm.
		@return payterm	  */
	public String getpayterm () 
	{
		return (String)get_Value(COLUMNNAME_payterm);
	}

	/** Set Process.
		@param process Process	  */
	public void setprocess (String process)
	{
		set_Value (COLUMNNAME_process, process);
	}

	/** Get Process.
		@return Process	  */
	public String getprocess () 
	{
		return (String)get_Value(COLUMNNAME_process);
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_charges getsmj_charges() throws RuntimeException
    {
		return (I_smj_charges)MTable.get(getCtx(), I_smj_charges.Table_Name)
			.getPO(getsmj_charges_ID(), get_TrxName());	}

	/** Set Charges.
		@param smj_charges_ID Charges	  */
	public void setsmj_charges_ID (int smj_charges_ID)
	{
		if (smj_charges_ID < 1) 
			set_Value (COLUMNNAME_smj_charges_ID, null);
		else 
			set_Value (COLUMNNAME_smj_charges_ID, Integer.valueOf(smj_charges_ID));
	}

	/** Get Charges.
		@return Charges	  */
	public int getsmj_charges_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_charges_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Control Advances.
		@param smj_controlAdvances_ID Control Advances	  */
	public void setsmj_controlAdvances_ID (int smj_controlAdvances_ID)
	{
		if (smj_controlAdvances_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_controlAdvances_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_controlAdvances_ID, Integer.valueOf(smj_controlAdvances_ID));
	}

	/** Get Control Advances.
		@return Control Advances	  */
	public int getsmj_controlAdvances_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_controlAdvances_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_value.
		@param smj_value smj_value	  */
	public void setsmj_value (BigDecimal smj_value)
	{
		set_Value (COLUMNNAME_smj_value, smj_value);
	}

	/** Get smj_value.
		@return smj_value	  */
	public BigDecimal getsmj_value () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_value);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** TenderType AD_Reference_ID=214 */
	public static final int TENDERTYPE_AD_Reference_ID=214;
	/** Credit Card = C */
	public static final String TENDERTYPE_CreditCard = "C";
	/** Check = K */
	public static final String TENDERTYPE_Check = "K";
	/** Direct Deposit = A */
	public static final String TENDERTYPE_DirectDeposit = "A";
	/** Direct Debit = D */
	public static final String TENDERTYPE_DirectDebit = "D";
	/** Cash = T */
	public static final String TENDERTYPE_Cash = "T";
	/** Cash Box = X */
	public static final String TENDERTYPE_CashBox = "X";
	/** Set Tender type.
		@param TenderType 
		Method of Payment
	  */
	public void setTenderType (String TenderType)
	{

		set_Value (COLUMNNAME_TenderType, TenderType);
	}

	/** Get Tender type.
		@return Method of Payment
	  */
	public String getTenderType () 
	{
		return (String)get_Value(COLUMNNAME_TenderType);
	}
}