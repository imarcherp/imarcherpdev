package com.smj.model;

import java.util.Properties;

public class MSMJTmpWebSales extends X_smj_tmpWebSales {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4901047666220271095L;

	/**
	 * MSMJTmpWebSales constructor
	 * @param ctx
	 * @param smj_tmpWebSalesLine_ID
	 * @param trxName
	 */
	public MSMJTmpWebSales(Properties ctx, int smj_tmpWebSalesLine_ID,
			String trxName) {
		super(ctx, smj_tmpWebSalesLine_ID, trxName);
	}//MSMJTmpWebSales

}//MSMJTmpWebSales
