package com.smj.model;

import java.util.Properties;

public class MSMJProgrammingGroup extends X_smj_programmingGroup{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1407664685676501246L;

	/**
	 * cosntructor X_smj_programmingGroup
	 * @param ctx
	 * @param smj_programmingGroup_ID
	 * @param trxName
	 */
	public MSMJProgrammingGroup(Properties ctx, int smj_programmingGroup_ID,
			String trxName) {
		super(ctx, smj_programmingGroup_ID, trxName);
	}

}//X_smj_programmingGroup
