package com.smj.model;

import java.util.Properties;

public class MSMJFuelLineHistory extends X_smj_fuellinehistory{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8706569693519379759L;

	/**
	 * constructor.
	 * @param ctx
	 * @param smj_FuelLineHistory_ID
	 * @param trxName
	 */
	public MSMJFuelLineHistory(Properties ctx, int smj_FuelLineHistory_ID,
			String trxName) {
		super(ctx, smj_FuelLineHistory_ID, trxName);
	}//MSMJFuelLineHistory

}//MSMJFuelLineHistory
