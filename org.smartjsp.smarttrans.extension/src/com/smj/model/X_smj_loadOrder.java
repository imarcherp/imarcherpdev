/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for smj_loadOrder
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_loadOrder extends PO implements I_smj_loadOrder, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130902L;

    /** Standard Constructor */
    public X_smj_loadOrder (Properties ctx, int smj_loadOrder_ID, String trxName)
    {
      super (ctx, smj_loadOrder_ID, trxName);
      /** if (smj_loadOrder_ID == 0)
        {
			setordernumber (null);
			setR_Request_ID (0);
			setsmj_loadOrder_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_loadOrder (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_loadOrder[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getc_bpartnerdriver() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getc_bpartnerdriver_ID(), get_TrxName());	}

	/** Set Partner Driver.
		@param c_bpartnerdriver_ID Partner Driver	  */
	public void setc_bpartnerdriver_ID (int c_bpartnerdriver_ID)
	{
		throw new IllegalArgumentException ("c_bpartnerdriver_ID is virtual column");	}

	/** Get Partner Driver.
		@return Partner Driver	  */
	public int getc_bpartnerdriver_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_c_bpartnerdriver_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Order Date.
		@param orderdate Order Date	  */
	public void setorderdate (Timestamp orderdate)
	{
		set_Value (COLUMNNAME_orderdate, orderdate);
	}

	/** Get Order Date.
		@return Order Date	  */
	public Timestamp getorderdate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_orderdate);
	}

	/** Set Order Number.
		@param ordernumber Order Number	  */
	public void setordernumber (String ordernumber)
	{
		set_Value (COLUMNNAME_ordernumber, ordernumber);
	}

	/** Get Order Number.
		@return Order Number	  */
	public String getordernumber () 
	{
		return (String)get_Value(COLUMNNAME_ordernumber);
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Sender Name.
		@param sendername Sender Name	  */
	public void setsendername (String sendername)
	{
		set_Value (COLUMNNAME_sendername, sendername);
	}

	/** Get Sender Name.
		@return Sender Name	  */
	public String getsendername () 
	{
		return (String)get_Value(COLUMNNAME_sendername);
	}

	/** Set Load Order.
		@param smj_loadOrder_ID Load Order	  */
	public void setsmj_loadOrder_ID (int smj_loadOrder_ID)
	{
		if (smj_loadOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_loadOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_loadOrder_ID, Integer.valueOf(smj_loadOrder_ID));
	}

	/** Get Load Order.
		@return Load Order	  */
	public int getsmj_loadOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_loadOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Location getsmj_mainstartloc() throws RuntimeException
    {
		return (I_C_Location)MTable.get(getCtx(), I_C_Location.Table_Name)
			.getPO(getsmj_mainstartloc_ID(), get_TrxName());	}

	/** Set Main Start Localization.
		@param smj_mainstartloc_ID Main Start Localization	  */
	public void setsmj_mainstartloc_ID (int smj_mainstartloc_ID)
	{
		if (smj_mainstartloc_ID < 1) 
			set_Value (COLUMNNAME_smj_mainstartloc_ID, null);
		else 
			set_Value (COLUMNNAME_smj_mainstartloc_ID, Integer.valueOf(smj_mainstartloc_ID));
	}

	/** Get Main Start Localization.
		@return Main Start Localization	  */
	public int getsmj_mainstartloc_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_mainstartloc_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		throw new IllegalArgumentException ("smj_plate is virtual column");	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}
}