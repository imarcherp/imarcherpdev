package com.smj.model;

import java.util.Properties;

public class MSMJHistoryTire extends X_smj_HistoryTire{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1350103218180352861L;

	/***
	 * MSMJHistoryTire constructor
	 * @param ctx
	 * @param smj_HistoryTire_ID
	 * @param trxName
	 */
	public MSMJHistoryTire(Properties ctx, int smj_HistoryTire_ID,
			String trxName) {
		super(ctx, smj_HistoryTire_ID, trxName);
	}

	
}//MSMJHistoryTire
