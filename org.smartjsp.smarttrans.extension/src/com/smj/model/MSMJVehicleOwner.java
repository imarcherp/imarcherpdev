package com.smj.model;

import java.util.Properties;

public class MSMJVehicleOwner extends X_smj_vehicleOwner{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6006168856348800439L;

	/**
	 * constructor MSMJVehicleOwner
	 * @param ctx
	 * @param smj_vehicleOwner_ID
	 * @param trxName
	 */
	public MSMJVehicleOwner(Properties ctx, int smj_vehicleOwner_ID,
			String trxName) {
		super(ctx, smj_vehicleOwner_ID, trxName);
	}

}//MSMJVehicleOwner
