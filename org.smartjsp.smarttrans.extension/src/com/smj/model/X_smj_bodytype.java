/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for smj_bodytype
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_bodytype extends PO implements I_smj_bodytype, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130225L;

    /** Standard Constructor */
    public X_smj_bodytype (Properties ctx, int smj_bodytype_ID, String trxName)
    {
      super (ctx, smj_bodytype_ID, trxName);
      /** if (smj_bodytype_ID == 0)
        {
			setName (null);
			setsmj_bodytype_id (0);
        } */
    }

    /** Load Constructor */
    public X_smj_bodytype (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_bodytype[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set smj_bodytype_id.
		@param smj_bodytype_id smj_bodytype_id	  */
	public void setsmj_bodytype_id (int smj_bodytype_id)
	{
		set_ValueNoCheck (COLUMNNAME_smj_bodytype_id, Integer.valueOf(smj_bodytype_id));
	}

	/** Get smj_bodytype_id.
		@return smj_bodytype_id	  */
	public int getsmj_bodytype_id () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_bodytype_id);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}