package com.smj.model;

import java.util.Properties;

public class MSMJTmpWebSalesLine extends X_smj_tmpWebSalesLine{

	/**
	 * MSMJTmpWebSalesLine constructor
	 */
	private static final long serialVersionUID = 1579531262107586328L;

	public MSMJTmpWebSalesLine(Properties ctx, int smj_tmpWebSalesLine_ID,
			String trxName) {
		super(ctx, smj_tmpWebSalesLine_ID, trxName);
	}//MSMJTmpWebSalesLine
	
	private String oldCode = "";

	public String getOldCode() {
		return oldCode;
	}

	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	

}//MSMJTmpWebSalesLine
