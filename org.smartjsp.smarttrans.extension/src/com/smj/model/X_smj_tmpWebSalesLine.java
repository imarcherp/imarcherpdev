/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_tmpWebSalesLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_tmpWebSalesLine extends PO implements I_smj_tmpWebSalesLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130806L;

    /** Standard Constructor */
    public X_smj_tmpWebSalesLine (Properties ctx, int smj_tmpWebSalesLine_ID, String trxName)
    {
      super (ctx, smj_tmpWebSalesLine_ID, trxName);
      /** if (smj_tmpWebSalesLine_ID == 0)
        {
			setsmj_iswarranty (false);
			setsmj_iswarrantyapplied (false);
			setsmj_isworkorder (false);
			setsmj_tmpWebSales_ID (0);
			setsmj_tmpWebSalesLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_tmpWebSalesLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_tmpWebSalesLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_UOM getC_UOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_Value (COLUMNNAME_M_Locator_ID, null);
		else 
			set_Value (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (I_M_Warehouse)MTable.get(getCtx(), I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set partnername.
		@param partnername partnername	  */
	public void setpartnername (String partnername)
	{
		set_Value (COLUMNNAME_partnername, partnername);
	}

	/** Get partnername.
		@return partnername	  */
	public String getpartnername () 
	{
		return (String)get_Value(COLUMNNAME_partnername);
	}

	/** Set Price.
		@param Price 
		Price
	  */
	public void setPrice (BigDecimal Price)
	{
		set_Value (COLUMNNAME_Price, Price);
	}

	/** Get Price.
		@return Price
	  */
	public BigDecimal getPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Price);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Product Name.
		@param ProductName 
		Name of the Product
	  */
	public void setProductName (String ProductName)
	{
		set_Value (COLUMNNAME_ProductName, ProductName);
	}

	/** Get Product Name.
		@return Name of the Product
	  */
	public String getProductName () 
	{
		return (String)get_Value(COLUMNNAME_ProductName);
	}

	/** Set Product Key.
		@param ProductValue 
		Key of the Product
	  */
	public void setProductValue (String ProductValue)
	{
		set_Value (COLUMNNAME_ProductValue, ProductValue);
	}

	/** Get Product Key.
		@return Key of the Product
	  */
	public String getProductValue () 
	{
		return (String)get_Value(COLUMNNAME_ProductValue);
	}

	/** Set Purchase Price.
		@param purchasePrice Purchase Price	  */
	public void setpurchasePrice (BigDecimal purchasePrice)
	{
		set_Value (COLUMNNAME_purchasePrice, purchasePrice);
	}

	/** Get Purchase Price.
		@return Purchase Price	  */
	public BigDecimal getpurchasePrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_purchasePrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param QtyEntered 
		The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered)
	{
		set_Value (COLUMNNAME_QtyEntered, QtyEntered);
	}

	/** Get Quantity.
		@return The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Is Warranty.
		@param smj_iswarranty Is Warranty	  */
	public void setsmj_iswarranty (boolean smj_iswarranty)
	{
		set_Value (COLUMNNAME_smj_iswarranty, Boolean.valueOf(smj_iswarranty));
	}

	/** Get Is Warranty.
		@return Is Warranty	  */
	public boolean issmj_iswarranty () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iswarranty);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Warranty Applied.
		@param smj_iswarrantyapplied Is Warranty Applied	  */
	public void setsmj_iswarrantyapplied (boolean smj_iswarrantyapplied)
	{
		set_Value (COLUMNNAME_smj_iswarrantyapplied, Boolean.valueOf(smj_iswarrantyapplied));
	}

	/** Get Is Warranty Applied.
		@return Is Warranty Applied	  */
	public boolean issmj_iswarrantyapplied () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iswarrantyapplied);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Work Order.
		@param smj_isworkorder Is Work Order	  */
	public void setsmj_isworkorder (boolean smj_isworkorder)
	{
		set_Value (COLUMNNAME_smj_isworkorder, Boolean.valueOf(smj_isworkorder));
	}

	/** Get Is Work Order.
		@return Is Work Order	  */
	public boolean issmj_isworkorder () 
	{
		Object oo = get_Value(COLUMNNAME_smj_isworkorder);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	public I_smj_tmpWebSales getsmj_tmpWebSales() throws RuntimeException
    {
		return (I_smj_tmpWebSales)MTable.get(getCtx(), I_smj_tmpWebSales.Table_Name)
			.getPO(getsmj_tmpWebSales_ID(), get_TrxName());	}

	/** Set Tmp Web Sales.
		@param smj_tmpWebSales_ID Tmp Web Sales	  */
	public void setsmj_tmpWebSales_ID (int smj_tmpWebSales_ID)
	{
		if (smj_tmpWebSales_ID < 1) 
			set_Value (COLUMNNAME_smj_tmpWebSales_ID, null);
		else 
			set_Value (COLUMNNAME_smj_tmpWebSales_ID, Integer.valueOf(smj_tmpWebSales_ID));
	}

	/** Get Tmp Web Sales.
		@return Tmp Web Sales	  */
	public int getsmj_tmpWebSales_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpWebSales_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tmp Web Sales Line.
		@param smj_tmpWebSalesLine_ID Tmp Web Sales Line	  */
	public void setsmj_tmpWebSalesLine_ID (int smj_tmpWebSalesLine_ID)
	{
		if (smj_tmpWebSalesLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_tmpWebSalesLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_tmpWebSalesLine_ID, Integer.valueOf(smj_tmpWebSalesLine_ID));
	}

	/** Get Tmp Web Sales Line.
		@return Tmp Web Sales Line	  */
	public int getsmj_tmpWebSalesLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpWebSalesLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException
    {
		return (I_SMJ_Vehicle)MTable.get(getCtx(), I_SMJ_Vehicle.Table_Name)
			.getPO(getsmj_vehicle_ID(), get_TrxName());	}

	/** Set Vehicle.
		@param smj_vehicle_ID Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID)
	{
		if (smj_vehicle_ID < 1) 
			set_Value (COLUMNNAME_smj_vehicle_ID, null);
		else 
			set_Value (COLUMNNAME_smj_vehicle_ID, Integer.valueOf(smj_vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getsmj_vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehicle_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException
    {
		return (I_smj_workOrderLine)MTable.get(getCtx(), I_smj_workOrderLine.Table_Name)
			.getPO(getsmj_workOrderLine_ID(), get_TrxName());	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tax.
		@param tax Tax	  */
	public void settax (BigDecimal tax)
	{
		set_Value (COLUMNNAME_tax, tax);
	}

	/** Get Tax.
		@return Tax	  */
	public BigDecimal gettax () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_tax);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total.
		@param total Total	  */
	public void settotal (BigDecimal total)
	{
		set_Value (COLUMNNAME_total, total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal gettotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set UOM Name.
		@param uomname UOM Name	  */
	public void setuomname (String uomname)
	{
		set_Value (COLUMNNAME_uomname, uomname);
	}

	/** Get UOM Name.
		@return UOM Name	  */
	public String getuomname () 
	{
		return (String)get_Value(COLUMNNAME_uomname);
	}

	/** Set Warehouse.
		@param WarehouseName 
		Warehouse Name
	  */
	public void setWarehouseName (String WarehouseName)
	{
		set_Value (COLUMNNAME_WarehouseName, WarehouseName);
	}

	/** Get Warehouse.
		@return Warehouse Name
	  */
	public String getWarehouseName () 
	{
		return (String)get_Value(COLUMNNAME_WarehouseName);
	}
}