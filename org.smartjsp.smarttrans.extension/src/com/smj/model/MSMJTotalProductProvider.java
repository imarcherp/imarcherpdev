package com.smj.model;

import java.util.Properties;

public class MSMJTotalProductProvider extends X_smj_totalProductProvider {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1756522767364401079L;

	/**
	 * costructor MSMJTotalProductProvider
	 * @param ctx
	 * @param smj_totalProductProvider_ID
	 * @param trxName
	 */
	public MSMJTotalProductProvider(Properties ctx,
			int smj_totalProductProvider_ID, String trxName) {
		super(ctx, smj_totalProductProvider_ID, trxName);
		
	}

}//MSMJTotalProductProvider
