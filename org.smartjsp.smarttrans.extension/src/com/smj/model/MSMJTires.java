package com.smj.model;

import java.util.Properties;

public class MSMJTires extends X_smj_tires {

	private String name = "";
	private String plate = ""; 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6970361452595825992L;

	/**
	 * contructor MSMJTires
	 * @param ctx
	 * @param smj_tires_ID
	 * @param trxName
	 */
	public MSMJTires(Properties ctx, int smj_tires_ID, String trxName) {
		super(ctx, smj_tires_ID, trxName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}
}//MSMJTires
