/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_fuellinehistory
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_fuellinehistory 
{

    /** TableName=smj_fuellinehistory */
    public static final String Table_Name = "smj_FuelLineHistory";

    /** AD_Table_ID=1000052 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name actualhundred */
    public static final String COLUMNNAME_actualhundred = "actualhundred";

	/** Set Actual Hundred	  */
	public void setactualhundred (BigDecimal actualhundred);

	/** Get Actual Hundred	  */
	public BigDecimal getactualhundred();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name datechange */
    public static final String COLUMNNAME_datechange = "datechange";

	/** Set Date Change	  */
	public void setdatechange (Timestamp datechange);

	/** Get Date Change	  */
	public Timestamp getdatechange();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name limithundred */
    public static final String COLUMNNAME_limithundred = "limithundred";

	/** Set Limit Hundred	  */
	public void setlimithundred (BigDecimal limithundred);

	/** Get Limit Hundred	  */
	public BigDecimal getlimithundred();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name smj_FuelIsland_ID */
    public static final String COLUMNNAME_smj_FuelIsland_ID = "smj_FuelIsland_ID";

	/** Set Fuel Island	  */
	public void setsmj_FuelIsland_ID (int smj_FuelIsland_ID);

	/** Get Fuel Island	  */
	public int getsmj_FuelIsland_ID();

	public I_smj_FuelIsland getsmj_FuelIsland() throws RuntimeException;

    /** Column name smj_FuelLine_ID */
    public static final String COLUMNNAME_smj_FuelLine_ID = "smj_FuelLine_ID";

	/** Set Fuel Line	  */
	public void setsmj_FuelLine_ID (int smj_FuelLine_ID);

	/** Get Fuel Line	  */
	public int getsmj_FuelLine_ID();

	public I_smj_FuelLine getsmj_FuelLine() throws RuntimeException;

    /** Column name smj_fuellinehistory_ID */
    public static final String COLUMNNAME_smj_fuellinehistory_ID = "smj_fuellinehistory_ID";

	/** Set Fuel Line History	  */
	public void setsmj_fuellinehistory_ID (int smj_fuellinehistory_ID);

	/** Get Fuel Line History	  */
	public int getsmj_fuellinehistory_ID();

    /** Column name smj_rchangefuelline_ID */
    public static final String COLUMNNAME_smj_rchangefuelline_ID = "smj_rchangefuelline_ID";

	/** Set Reason Change FuelLine	  */
	public void setsmj_rchangefuelline_ID (int smj_rchangefuelline_ID);

	/** Get Reason Change FuelLine	  */
	public int getsmj_rchangefuelline_ID();

	public I_smj_rchangefuelline getsmj_rchangefuelline() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
