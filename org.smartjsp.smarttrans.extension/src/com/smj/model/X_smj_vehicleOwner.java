/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for smj_vehicleOwner
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_vehicleOwner extends PO implements I_smj_vehicleOwner, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130822L;

    /** Standard Constructor */
    public X_smj_vehicleOwner (Properties ctx, int smj_vehicleOwner_ID, String trxName)
    {
      super (ctx, smj_vehicleOwner_ID, trxName);
      /** if (smj_vehicleOwner_ID == 0)
        {
			setC_BPartner_ID (0);
			setsmj_vehicle_ID (0);
			setsmj_vehicleOwner_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_vehicleOwner (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_vehicleOwner[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set End Date.
		@param EndDate 
		Last effective date (inclusive)
	  */
	public void setEndDate (Timestamp EndDate)
	{
		set_Value (COLUMNNAME_EndDate, EndDate);
	}

	/** Get End Date.
		@return Last effective date (inclusive)
	  */
	public Timestamp getEndDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_EndDate);
	}

	/** relationship AD_Reference_ID=1000036 */
	public static final int RELATIONSHIP_AD_Reference_ID=1000036;
	/** OWNER = O */
	public static final String RELATIONSHIP_OWNER = "O";
	/** HOLDER 1 = 1 */
	public static final String RELATIONSHIP_HOLDER1 = "1";
	/** HOLDER 2 = 2 */
	public static final String RELATIONSHIP_HOLDER2 = "2";
	/** HOLDER 3 = 3 */
	public static final String RELATIONSHIP_HOLDER3 = "3";
	/** Set Relationship.
		@param relationship Relationship	  */
	public void setrelationship (String relationship)
	{

		set_Value (COLUMNNAME_relationship, relationship);
	}

	/** Get Relationship.
		@return Relationship	  */
	public String getrelationship () 
	{
		return (String)get_Value(COLUMNNAME_relationship);
	}

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException
    {
		return (I_SMJ_Vehicle)MTable.get(getCtx(), I_SMJ_Vehicle.Table_Name)
			.getPO(getsmj_vehicle_ID(), get_TrxName());	}

	/** Set Vehicle.
		@param smj_vehicle_ID Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID)
	{
		if (smj_vehicle_ID < 1) 
			set_Value (COLUMNNAME_smj_vehicle_ID, null);
		else 
			set_Value (COLUMNNAME_smj_vehicle_ID, Integer.valueOf(smj_vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getsmj_vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehicle_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle Owner.
		@param smj_vehicleOwner_ID Vehicle Owner	  */
	public void setsmj_vehicleOwner_ID (int smj_vehicleOwner_ID)
	{
		if (smj_vehicleOwner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_vehicleOwner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_vehicleOwner_ID, Integer.valueOf(smj_vehicleOwner_ID));
	}

	/** Get Vehicle Owner.
		@return Vehicle Owner	  */
	public int getsmj_vehicleOwner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehicleOwner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Start Date.
		@param StartDate 
		First effective day (inclusive)
	  */
	public void setStartDate (Timestamp StartDate)
	{
		set_Value (COLUMNNAME_StartDate, StartDate);
	}

	/** Get Start Date.
		@return First effective day (inclusive)
	  */
	public Timestamp getStartDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartDate);
	}
}