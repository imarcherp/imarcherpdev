package com.smj.model;

import java.math.BigDecimal;

public class PaymentList {

	private Integer typeTerm;
	private  String termName;
	private Integer bankAccountId;
	private  String tenderType;
	private  String tenderName;
	private String creditCardType;
	private  String creditCardNumber;
	private  String newCreditCardVV;
	private Integer creditCardExpMM;
	private  Integer newCreditCardExpYY;
	private  String name;
	private String routingNo;
	private  String accountNo;
	private  String checkNo;
	private  String micr;
	private BigDecimal value;
	
	public String getTermName() {
		return termName;
	}
	public void setTermName(String termName) {
		this.termName = termName;
	}
	public String getTenderName() {
		return tenderName;
	}
	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}
	public Integer getTypeTerm() {
		return typeTerm;
	}
	public void setTypeTerm(Integer typeTerm) {
		this.typeTerm = typeTerm;
	}
	public Integer getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getTenderType() {
		return tenderType;
	}
	public void setTenderType(String tenderType) {
		this.tenderType = tenderType;
	}
	public String getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getNewCreditCardVV() {
		return newCreditCardVV;
	}
	public void setNewCreditCardVV(String newCreditCardVV) {
		this.newCreditCardVV = newCreditCardVV;
	}
	public Integer getCreditCardExpMM() {
		return creditCardExpMM;
	}
	public void setCreditCardExpMM(Integer creditCardExpMM) {
		this.creditCardExpMM = creditCardExpMM;
	}
	public Integer getNewCreditCardExpYY() {
		return newCreditCardExpYY;
	}
	public void setNewCreditCardExpYY(Integer newCreditCardExpYY) {
		this.newCreditCardExpYY = newCreditCardExpYY;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoutingNo() {
		return routingNo;
	}
	public void setRoutingNo(String routingNo) {
		this.routingNo = routingNo;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getMicr() {
		return micr;
	}
	public void setMicr(String micr) {
		this.micr = micr;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
		
}
