package com.smj.model;

import java.util.Properties;

public class MSMJScheduleRules extends X_smj_ScheduleRules{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9216395147003551250L;

	/**
	 * MSMJScheduleRules constructor
	 * @param ctx
	 * @param smj_ScheduleRules_ID
	 * @param trxName
	 */
	public MSMJScheduleRules(Properties ctx, int smj_ScheduleRules_ID,
			String trxName) {
		super(ctx, smj_ScheduleRules_ID, trxName);
	}

}//MSMJScheduleRules
