/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for smj_tmpPayment
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_tmpPayment extends PO implements I_smj_tmpPayment, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130402L;

    /** Standard Constructor */
    public X_smj_tmpPayment (Properties ctx, int smj_tmpPayment_ID, String trxName)
    {
      super (ctx, smj_tmpPayment_ID, trxName);
      /** if (smj_tmpPayment_ID == 0)
        {
			setC_Order_ID (0);
			setsmj_tmpPayment_ID (0);
			setTenderType (null);
			settotal (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_tmpPayment (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_tmpPayment[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Account No.
		@param AccountNo 
		Account Number
	  */
	public void setAccountNo (String AccountNo)
	{
		set_Value (COLUMNNAME_AccountNo, AccountNo);
	}

	/** Get Account No.
		@return Account Number
	  */
	public String getAccountNo () 
	{
		return (String)get_Value(COLUMNNAME_AccountNo);
	}

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException
    {
		return (I_C_PaymentTerm)MTable.get(getCtx(), I_C_PaymentTerm.Table_Name)
			.getPO(getC_PaymentTerm_ID(), get_TrxName());	}

	/** Set Payment Term.
		@param C_PaymentTerm_ID 
		The terms of Payment (timing, discount)
	  */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID)
	{
		if (C_PaymentTerm_ID < 1) 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, null);
		else 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, Integer.valueOf(C_PaymentTerm_ID));
	}

	/** Get Payment Term.
		@return The terms of Payment (timing, discount)
	  */
	public int getC_PaymentTerm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaymentTerm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set ccexpmm.
		@param ccexpmm ccexpmm	  */
	public void setccexpmm (int ccexpmm)
	{
		set_Value (COLUMNNAME_ccexpmm, Integer.valueOf(ccexpmm));
	}

	/** Get ccexpmm.
		@return ccexpmm	  */
	public int getccexpmm () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ccexpmm);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set ccexpyy.
		@param ccexpyy ccexpyy	  */
	public void setccexpyy (int ccexpyy)
	{
		set_Value (COLUMNNAME_ccexpyy, Integer.valueOf(ccexpyy));
	}

	/** Get ccexpyy.
		@return ccexpyy	  */
	public int getccexpyy () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ccexpyy);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set ccnumber.
		@param ccnumber ccnumber	  */
	public void setccnumber (String ccnumber)
	{
		set_Value (COLUMNNAME_ccnumber, ccnumber);
	}

	/** Get ccnumber.
		@return ccnumber	  */
	public String getccnumber () 
	{
		return (String)get_Value(COLUMNNAME_ccnumber);
	}

	/** Set cctype.
		@param cctype cctype	  */
	public void setcctype (String cctype)
	{
		set_Value (COLUMNNAME_cctype, cctype);
	}

	/** Get cctype.
		@return cctype	  */
	public String getcctype () 
	{
		return (String)get_Value(COLUMNNAME_cctype);
	}

	/** Set ccverfication.
		@param ccverfication ccverfication	  */
	public void setccverfication (String ccverfication)
	{
		set_Value (COLUMNNAME_ccverfication, ccverfication);
	}

	/** Get ccverfication.
		@return ccverfication	  */
	public String getccverfication () 
	{
		return (String)get_Value(COLUMNNAME_ccverfication);
	}

	/** Set Check No.
		@param CheckNo 
		Check Number
	  */
	public void setCheckNo (String CheckNo)
	{
		set_Value (COLUMNNAME_CheckNo, CheckNo);
	}

	/** Get Check No.
		@return Check Number
	  */
	public String getCheckNo () 
	{
		return (String)get_Value(COLUMNNAME_CheckNo);
	}

	/** Set Micr.
		@param Micr 
		Combination of routing no, account and check no
	  */
	public void setMicr (String Micr)
	{
		set_Value (COLUMNNAME_Micr, Micr);
	}

	/** Get Micr.
		@return Combination of routing no, account and check no
	  */
	public String getMicr () 
	{
		return (String)get_Value(COLUMNNAME_Micr);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Routing No.
		@param RoutingNo 
		Bank Routing Number
	  */
	public void setRoutingNo (String RoutingNo)
	{
		set_Value (COLUMNNAME_RoutingNo, RoutingNo);
	}

	/** Get Routing No.
		@return Bank Routing Number
	  */
	public String getRoutingNo () 
	{
		return (String)get_Value(COLUMNNAME_RoutingNo);
	}

	/** Set tmp Payment.
		@param smj_tmpPayment_ID tmp Payment	  */
	public void setsmj_tmpPayment_ID (int smj_tmpPayment_ID)
	{
		if (smj_tmpPayment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_tmpPayment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_tmpPayment_ID, Integer.valueOf(smj_tmpPayment_ID));
	}

	/** Get tmp Payment.
		@return tmp Payment	  */
	public int getsmj_tmpPayment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpPayment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tender type.
		@param TenderType 
		Method of Payment
	  */
	public void setTenderType (String TenderType)
	{
		set_Value (COLUMNNAME_TenderType, TenderType);
	}

	/** Get Tender type.
		@return Method of Payment
	  */
	public String getTenderType () 
	{
		return (String)get_Value(COLUMNNAME_TenderType);
	}

	/** Set total.
		@param total total	  */
	public void settotal (BigDecimal total)
	{
		set_Value (COLUMNNAME_total, total);
	}

	/** Get total.
		@return total	  */
	public BigDecimal gettotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}