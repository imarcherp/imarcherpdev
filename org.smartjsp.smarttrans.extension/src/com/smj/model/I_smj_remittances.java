/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_remittances
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_remittances 
{

    /** TableName=smj_remittances */
    public static final String Table_Name = "smj_remittances";

    /** AD_Table_ID=1000058 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name c_bpartnerdriver_ID */
    public static final String COLUMNNAME_c_bpartnerdriver_ID = "c_bpartnerdriver_ID";

	/** Set Partner Driver	  */
	public void setc_bpartnerdriver_ID (int c_bpartnerdriver_ID);

	/** Get Partner Driver	  */
	public int getc_bpartnerdriver_ID();

	public I_C_BPartner getc_bpartnerdriver() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public I_R_Request getR_Request() throws RuntimeException;

    /** Column name receiveraddress */
    public static final String COLUMNNAME_receiveraddress = "receiveraddress";

	/** Set Address 1.
	  * Address line 1 for this location
	  */
	public void setreceiveraddress (String receiveraddress);

	/** Get Address 1.
	  * Address line 1 for this location
	  */
	public String getreceiveraddress();

    /** Column name receiverendloc_ID */
    public static final String COLUMNNAME_receiverendloc_ID = "receiverendloc_ID";

	/** Set Receiver End Localization	  */
	public void setreceiverendloc_ID (int receiverendloc_ID);

	/** Get Receiver End Localization	  */
	public int getreceiverendloc_ID();

	public I_C_Location getreceiverendloc() throws RuntimeException;

    /** Column name receivername */
    public static final String COLUMNNAME_receivername = "receivername";

	/** Set Receiver Name	  */
	public void setreceivername (String receivername);

	/** Get Receiver Name	  */
	public String getreceivername();

    /** Column name receiverphone */
    public static final String COLUMNNAME_receiverphone = "receiverphone";

	/** Set Receiver Phone	  */
	public void setreceiverphone (String receiverphone);

	/** Get Receiver Phone	  */
	public String getreceiverphone();

    /** Column name referralnumber */
    public static final String COLUMNNAME_referralnumber = "referralnumber";

	/** Set Referral Number	  */
	public void setreferralnumber (String referralnumber);

	/** Get Referral Number	  */
	public String getreferralnumber();

    /** Column name remittancedate */
    public static final String COLUMNNAME_remittancedate = "remittancedate";

	/** Set Remittance Date	  */
	public void setremittancedate (Timestamp remittancedate);

	/** Get Remittance Date	  */
	public Timestamp getremittancedate();

    /** Column name remittancenumber */
    public static final String COLUMNNAME_remittancenumber = "remittancenumber";

	/** Set remittancenumber	  */
	public void setremittancenumber (String remittancenumber);

	/** Get remittancenumber	  */
	public String getremittancenumber();

    /** Column name senderaddress */
    public static final String COLUMNNAME_senderaddress = "senderaddress";

	/** Set Address 1.
	  * Address line 1 for this location
	  */
	public void setsenderaddress (String senderaddress);

	/** Get Address 1.
	  * Address line 1 for this location
	  */
	public String getsenderaddress();

    /** Column name sendername */
    public static final String COLUMNNAME_sendername = "sendername";

	/** Set sendername	  */
	public void setsendername (String sendername);

	/** Get sendername	  */
	public String getsendername();

    /** Column name senderphone */
    public static final String COLUMNNAME_senderphone = "senderphone";

	/** Set Sender Phone	  */
	public void setsenderphone (String senderphone);

	/** Get Sender Phone	  */
	public String getsenderphone();

    /** Column name senderstartloc_ID */
    public static final String COLUMNNAME_senderstartloc_ID = "senderstartloc_ID";

	/** Set Sender Start Localization	  */
	public void setsenderstartloc_ID (int senderstartloc_ID);

	/** Get Sender Start Localization	  */
	public int getsenderstartloc_ID();

	public I_C_Location getsenderstartloc() throws RuntimeException;

    /** Column name smj_internalnumber */
    public static final String COLUMNNAME_smj_internalnumber = "smj_internalnumber";

	/** Set Internal Number	  */
	public void setsmj_internalnumber (String smj_internalnumber);

	/** Get Internal Number	  */
	public String getsmj_internalnumber();

    /** Column name smj_merchandise */
    public static final String COLUMNNAME_smj_merchandise = "smj_merchandise";

	/** Set Merchandise Description	  */
	public void setsmj_merchandise (String smj_merchandise);

	/** Get Merchandise Description	  */
	public String getsmj_merchandise();

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set Plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get Plate	  */
	public String getsmj_plate();

    /** Column name smj_remittances_ID */
    public static final String COLUMNNAME_smj_remittances_ID = "smj_remittances_ID";

	/** Set Remittances	  */
	public void setsmj_remittances_ID (int smj_remittances_ID);

	/** Get Remittances	  */
	public int getsmj_remittances_ID();

    /** Column name smj_volume */
    public static final String COLUMNNAME_smj_volume = "smj_volume";

	/** Set Volume	  */
	public void setsmj_volume (BigDecimal smj_volume);

	/** Get Volume	  */
	public BigDecimal getsmj_volume();

    /** Column name smj_weight */
    public static final String COLUMNNAME_smj_weight = "smj_weight";

	/** Set Weight	  */
	public void setsmj_weight (BigDecimal smj_weight);

	/** Get Weight	  */
	public BigDecimal getsmj_weight();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
