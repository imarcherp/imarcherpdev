/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_ScheduleRules
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_ScheduleRules 
{

    /** TableName=smj_ScheduleRules */
    public static final String Table_Name = "smj_ScheduleRules";

    /** AD_Table_ID=1000043 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (String Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public String getLine();

    /** Column name maxthreshold */
    public static final String COLUMNNAME_maxthreshold = "maxthreshold";

	/** Set Max Threshold	  */
	public void setmaxthreshold (BigDecimal maxthreshold);

	/** Get Max Threshold	  */
	public BigDecimal getmaxthreshold();

    /** Column name minthreshold */
    public static final String COLUMNNAME_minthreshold = "minthreshold";

	/** Set Min Threshold	  */
	public void setminthreshold (BigDecimal minthreshold);

	/** Get Min Threshold	  */
	public BigDecimal getminthreshold();

    /** Column name modelend */
    public static final String COLUMNNAME_modelend = "modelend";

	/** Set Model End	  */
	public void setmodelend (int modelend);

	/** Get Model End	  */
	public int getmodelend();

    /** Column name modelstart */
    public static final String COLUMNNAME_modelstart = "modelstart";

	/** Set Model Start	  */
	public void setmodelstart (int modelstart);

	/** Get Model Start	  */
	public int getmodelstart();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name periodicity */
    public static final String COLUMNNAME_periodicity = "periodicity";

	/** Set Periodicity	  */
	public void setperiodicity (String periodicity);

	/** Get Periodicity	  */
	public String getperiodicity();

    /** Column name process */
    public static final String COLUMNNAME_process = "process";

	/** Set Process	  */
	public void setprocess (String process);

	/** Get Process	  */
	public String getprocess();

    /** Column name smj_AdministrationType_ID */
    public static final String COLUMNNAME_smj_AdministrationType_ID = "smj_AdministrationType_ID";

	/** Set Administration Type	  */
	public void setsmj_AdministrationType_ID (int smj_AdministrationType_ID);

	/** Get Administration Type	  */
	public int getsmj_AdministrationType_ID();

	public I_smj_AdministrationType getsmj_AdministrationType() throws RuntimeException;

    /** Column name smj_brand_ID */
    public static final String COLUMNNAME_smj_brand_ID = "smj_brand_ID";

	/** Set Brand	  */
	public void setsmj_brand_ID (int smj_brand_ID);

	/** Get Brand	  */
	public int getsmj_brand_ID();

	public I_smj_brand getsmj_brand() throws RuntimeException;

    /** Column name smj_programmingGroup_ID */
    public static final String COLUMNNAME_smj_programmingGroup_ID = "smj_programmingGroup_ID";

	/** Set Programming Group	  */
	public void setsmj_programmingGroup_ID (int smj_programmingGroup_ID);

	/** Get Programming Group	  */
	public int getsmj_programmingGroup_ID();

	public I_smj_programmingGroup getsmj_programmingGroup() throws RuntimeException;

    /** Column name smj_ScheduleRules_ID */
    public static final String COLUMNNAME_smj_ScheduleRules_ID = "smj_ScheduleRules_ID";

	/** Set Schedule Rules	  */
	public void setsmj_ScheduleRules_ID (int smj_ScheduleRules_ID);

	/** Get Schedule Rules	  */
	public int getsmj_ScheduleRules_ID();

    /** Column name threshold */
    public static final String COLUMNNAME_threshold = "threshold";

	/** Set Threshold	  */
	public void setthreshold (String threshold);

	/** Get Threshold	  */
	public String getthreshold();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name valueperiodicity */
    public static final String COLUMNNAME_valueperiodicity = "valueperiodicity";

	/** Set Value Periodicity	  */
	public void setvalueperiodicity (BigDecimal valueperiodicity);

	/** Get Value Periodicity	  */
	public BigDecimal getvalueperiodicity();

    /** Column name vehicletype */
    public static final String COLUMNNAME_vehicletype = "vehicletype";

	/** Set Vehicle Type	  */
	public void setvehicletype (String vehicletype);

	/** Get Vehicle Type	  */
	public String getvehicletype();
}
