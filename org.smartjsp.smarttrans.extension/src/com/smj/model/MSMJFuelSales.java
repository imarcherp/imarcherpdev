package com.smj.model;

import java.util.Properties;

public class MSMJFuelSales extends X_smj_FuelSales{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4618520292696806076L;

	/**
	 * cosntructor
	 * @param ctx
	 * @param smj_FuelSales_ID
	 * @param trxName
	 */
	public MSMJFuelSales(Properties ctx, int smj_FuelSales_ID, String trxName) {
		super(ctx, smj_FuelSales_ID, trxName);
	}

	private String islandName = "";
	private String lineName = "";

	public String getIslandName() {
		return islandName;
	}
	public void setIslandName(String islandName) {
		this.islandName = islandName;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	
	
}//MSMJFuelSales
