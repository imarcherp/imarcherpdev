/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_FuelSales
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_FuelSales extends PO implements I_smj_FuelSales, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130830L;

    /** Standard Constructor */
    public X_smj_FuelSales (Properties ctx, int smj_FuelSales_ID, String trxName)
    {
      super (ctx, smj_FuelSales_ID, trxName);
      /** if (smj_FuelSales_ID == 0)
        {
			setactualhundred (Env.ZERO);
			setC_BPartner_ID (0);
			setC_Order_ID (0);
			setentrydate (new Timestamp( System.currentTimeMillis() ));
			setisprocessed (false);
			setM_Warehouse_ID (0);
			setqtysold (Env.ZERO);
			setsmj_FuelIsland_ID (0);
			setsmj_FuelLine_ID (0);
			setsmj_FuelSales_ID (0);
			settotalprice (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_FuelSales (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_FuelSales[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Actual Hundred.
		@param actualhundred Actual Hundred	  */
	public void setactualhundred (BigDecimal actualhundred)
	{
		set_Value (COLUMNNAME_actualhundred, actualhundred);
	}

	/** Get Actual Hundred.
		@return Actual Hundred	  */
	public BigDecimal getactualhundred () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_actualhundred);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Entry Date.
		@param entrydate Entry Date	  */
	public void setentrydate (Timestamp entrydate)
	{
		set_Value (COLUMNNAME_entrydate, entrydate);
	}

	/** Get Entry Date.
		@return Entry Date	  */
	public Timestamp getentrydate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_entrydate);
	}

	/** Set Galon Price.
		@param galonprice Galon Price	  */
	public void setgalonprice (BigDecimal galonprice)
	{
		set_Value (COLUMNNAME_galonprice, galonprice);
	}

	/** Get Galon Price.
		@return Galon Price	  */
	public BigDecimal getgalonprice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_galonprice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Hundred End.
		@param hundredend Hundred End	  */
	public void sethundredend (BigDecimal hundredend)
	{
		set_Value (COLUMNNAME_hundredend, hundredend);
	}

	/** Get Hundred End.
		@return Hundred End	  */
	public BigDecimal gethundredend () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_hundredend);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Is Processed.
		@param isprocessed Is Processed	  */
	public void setisprocessed (boolean isprocessed)
	{
		set_Value (COLUMNNAME_isprocessed, Boolean.valueOf(isprocessed));
	}

	/** Get Is Processed.
		@return Is Processed	  */
	public boolean isprocessed () 
	{
		Object oo = get_Value(COLUMNNAME_isprocessed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (I_M_Warehouse)MTable.get(getCtx(), I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity Sold.
		@param qtysold Quantity Sold	  */
	public void setqtysold (BigDecimal qtysold)
	{
		set_Value (COLUMNNAME_qtysold, qtysold);
	}

	/** Get Quantity Sold.
		@return Quantity Sold	  */
	public BigDecimal getqtysold () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_qtysold);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_smj_FuelIsland getsmj_FuelIsland() throws RuntimeException
    {
		return (I_smj_FuelIsland)MTable.get(getCtx(), I_smj_FuelIsland.Table_Name)
			.getPO(getsmj_FuelIsland_ID(), get_TrxName());	}

	/** Set Fuel Island.
		@param smj_FuelIsland_ID Fuel Island	  */
	public void setsmj_FuelIsland_ID (int smj_FuelIsland_ID)
	{
		if (smj_FuelIsland_ID < 1) 
			set_Value (COLUMNNAME_smj_FuelIsland_ID, null);
		else 
			set_Value (COLUMNNAME_smj_FuelIsland_ID, Integer.valueOf(smj_FuelIsland_ID));
	}

	/** Get Fuel Island.
		@return Fuel Island	  */
	public int getsmj_FuelIsland_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_FuelIsland_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_FuelLine getsmj_FuelLine() throws RuntimeException
    {
		return (I_smj_FuelLine)MTable.get(getCtx(), I_smj_FuelLine.Table_Name)
			.getPO(getsmj_FuelLine_ID(), get_TrxName());	}

	/** Set Fuel Line.
		@param smj_FuelLine_ID Fuel Line	  */
	public void setsmj_FuelLine_ID (int smj_FuelLine_ID)
	{
		if (smj_FuelLine_ID < 1) 
			set_Value (COLUMNNAME_smj_FuelLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_FuelLine_ID, Integer.valueOf(smj_FuelLine_ID));
	}

	/** Get Fuel Line.
		@return Fuel Line	  */
	public int getsmj_FuelLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_FuelLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Fuel Sales.
		@param smj_FuelSales_ID Fuel Sales	  */
	public void setsmj_FuelSales_ID (int smj_FuelSales_ID)
	{
		if (smj_FuelSales_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_FuelSales_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_FuelSales_ID, Integer.valueOf(smj_FuelSales_ID));
	}

	/** Get Fuel Sales.
		@return Fuel Sales	  */
	public int getsmj_FuelSales_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_FuelSales_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_rchangefuelline getsmj_rchangefuelline() throws RuntimeException
    {
		return (I_smj_rchangefuelline)MTable.get(getCtx(), I_smj_rchangefuelline.Table_Name)
			.getPO(getsmj_rchangefuelline_ID(), get_TrxName());	}

	/** Set Reason Change FuelLine.
		@param smj_rchangefuelline_ID Reason Change FuelLine	  */
	public void setsmj_rchangefuelline_ID (int smj_rchangefuelline_ID)
	{
		if (smj_rchangefuelline_ID < 1) 
			set_Value (COLUMNNAME_smj_rchangefuelline_ID, null);
		else 
			set_Value (COLUMNNAME_smj_rchangefuelline_ID, Integer.valueOf(smj_rchangefuelline_ID));
	}

	/** Get Reason Change FuelLine.
		@return Reason Change FuelLine	  */
	public int getsmj_rchangefuelline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_rchangefuelline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Price.
		@param totalprice Total Price	  */
	public void settotalprice (BigDecimal totalprice)
	{
		set_Value (COLUMNNAME_totalprice, totalprice);
	}

	/** Get Total Price.
		@return Total Price	  */
	public BigDecimal gettotalprice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_totalprice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}