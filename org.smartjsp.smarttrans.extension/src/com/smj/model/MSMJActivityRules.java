package com.smj.model;

import java.util.Properties;

public class MSMJActivityRules extends X_smj_activityRules{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5961015621678700413L;

	/**
	 * MSMJActivityRules constructor
	 * @param ctx
	 * @param smj_activityRules_ID
	 * @param trxName
	 */
	public MSMJActivityRules(Properties ctx, int smj_activityRules_ID,
			String trxName) {
		super(ctx, smj_activityRules_ID, trxName);
	}

}//MSMJActivityRules
