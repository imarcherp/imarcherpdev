/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.compiere.model.I_C_BPartner;
import org.compiere.model.I_M_Product;
import org.compiere.model.MTable;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_HistoryTire
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_HistoryTire 
{

    /** TableName=smj_HistoryTire */
    public static final String Table_Name = "smj_HistoryTire";

    /** AD_Table_ID=1000048 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name datemount */
    public static final String COLUMNNAME_datemount = "datemount";

	/** Set Date Mount	  */
	public void setdatemount (Timestamp datemount);

	/** Get Date Mount	  */
	public Timestamp getdatemount();

    /** Column name dateumount */
    public static final String COLUMNNAME_dateumount = "dateumount";

	/** Set Date Umount	  */
	public void setdateumount (Timestamp dateumount);

	/** Get Date Umount	  */
	public Timestamp getdateumount();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name kms */
    public static final String COLUMNNAME_kms = "kms";

	/** Set Kms	  */
	public void setkms (BigDecimal kms);

	/** Get Kms	  */
	public BigDecimal getkms();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name Position */
    public static final String COLUMNNAME_Position = "Position";

	/** Set Position	  */
	public void setPosition (int Position);

	/** Get Position	  */
	public int getPosition();

    /** Column name serial */
    public static final String COLUMNNAME_serial = "serial";

	/** Set Serial	  */
	public void setserial (int serial);

	/** Get Serial	  */
	public int getserial();

    /** Column name smj_changeTire_ID */
    public static final String COLUMNNAME_smj_changeTire_ID = "smj_changeTire_ID";

	/** Set Change Tire	  */
	public void setsmj_changeTire_ID (int smj_changeTire_ID);

	/** Get Change Tire	  */
	public int getsmj_changeTire_ID();

	public I_smj_changeTire getsmj_changeTire() throws RuntimeException;

    /** Column name smj_HistoryTire_ID */
    public static final String COLUMNNAME_smj_HistoryTire_ID = "smj_HistoryTire_ID";

	/** Set History Tire	  */
	public void setsmj_HistoryTire_ID (int smj_HistoryTire_ID);

	/** Get History Tire	  */
	public int getsmj_HistoryTire_ID();

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set Plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get Plate	  */
	public String getsmj_plate();

    /** Column name smj_vehicle_ID */
    public static final String COLUMNNAME_smj_vehicle_ID = "smj_vehicle_ID";

	/** Set Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID);

	/** Get Vehicle	  */
	public int getsmj_vehicle_ID();

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException;

    /** Column name smj_workOrderLine_ID */
    public static final String COLUMNNAME_smj_workOrderLine_ID = "smj_workOrderLine_ID";

	/** Set Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID);

	/** Get Work Order Line	  */
	public int getsmj_workOrderLine_ID();

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException;

    /** Column name tirestate */
    public static final String COLUMNNAME_tirestate = "tirestate";

	/** Set Tire State	  */
	public void settirestate (String tirestate);

	/** Get Tire State	  */
	public String gettirestate();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name valuebrake */
    public static final String COLUMNNAME_valuebrake = "valuebrake";

	/** Set Value Brake	  */
	public void setvaluebrake (BigDecimal valuebrake);

	/** Get Value Brake	  */
	public BigDecimal getvaluebrake();

    /** Column name valuetire */
    public static final String COLUMNNAME_valuetire = "valuetire";

	/** Set Value Tire	  */
	public void setvaluetire (BigDecimal valuetire);

	/** Get Value Tire	  */
	public BigDecimal getvaluetire();
}
