/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_tmpWebSalesLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_smj_tmpWebSalesLine 
{

    /** TableName=smj_tmpWebSalesLine */
    public static final String Table_Name = "smj_tmpWebSalesLine";

    /** AD_Table_ID=1000037 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_AttributeSetInstance_ID */
    public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";

	/** Set Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID);

	/** Get Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException;

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name partnername */
    public static final String COLUMNNAME_partnername = "partnername";

	/** Set partnername	  */
	public void setpartnername (String partnername);

	/** Get partnername	  */
	public String getpartnername();

    /** Column name Price */
    public static final String COLUMNNAME_Price = "Price";

	/** Set Price.
	  * Price
	  */
	public void setPrice (BigDecimal Price);

	/** Get Price.
	  * Price
	  */
	public BigDecimal getPrice();

    /** Column name ProductName */
    public static final String COLUMNNAME_ProductName = "ProductName";

	/** Set Product Name.
	  * Name of the Product
	  */
	public void setProductName (String ProductName);

	/** Get Product Name.
	  * Name of the Product
	  */
	public String getProductName();

    /** Column name ProductValue */
    public static final String COLUMNNAME_ProductValue = "ProductValue";

	/** Set Product Key.
	  * Key of the Product
	  */
	public void setProductValue (String ProductValue);

	/** Get Product Key.
	  * Key of the Product
	  */
	public String getProductValue();

    /** Column name purchasePrice */
    public static final String COLUMNNAME_purchasePrice = "purchasePrice";

	/** Set Purchase Price	  */
	public void setpurchasePrice (BigDecimal purchasePrice);

	/** Get Purchase Price	  */
	public BigDecimal getpurchasePrice();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name QtyEntered */
    public static final String COLUMNNAME_QtyEntered = "QtyEntered";

	/** Set Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered);

	/** Get Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public I_R_Request getR_Request() throws RuntimeException;

    /** Column name smj_iswarranty */
    public static final String COLUMNNAME_smj_iswarranty = "smj_iswarranty";

	/** Set Is Warranty	  */
	public void setsmj_iswarranty (boolean smj_iswarranty);

	/** Get Is Warranty	  */
	public boolean issmj_iswarranty();

    /** Column name smj_iswarrantyapplied */
    public static final String COLUMNNAME_smj_iswarrantyapplied = "smj_iswarrantyapplied";

	/** Set Is Warranty Applied	  */
	public void setsmj_iswarrantyapplied (boolean smj_iswarrantyapplied);

	/** Get Is Warranty Applied	  */
	public boolean issmj_iswarrantyapplied();

    /** Column name smj_isworkorder */
    public static final String COLUMNNAME_smj_isworkorder = "smj_isworkorder";

	/** Set Is Work Order	  */
	public void setsmj_isworkorder (boolean smj_isworkorder);

	/** Get Is Work Order	  */
	public boolean issmj_isworkorder();

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set Plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get Plate	  */
	public String getsmj_plate();

    /** Column name smj_tmpWebSales_ID */
    public static final String COLUMNNAME_smj_tmpWebSales_ID = "smj_tmpWebSales_ID";

	/** Set Tmp Web Sales	  */
	public void setsmj_tmpWebSales_ID (int smj_tmpWebSales_ID);

	/** Get Tmp Web Sales	  */
	public int getsmj_tmpWebSales_ID();

	public I_smj_tmpWebSales getsmj_tmpWebSales() throws RuntimeException;

    /** Column name smj_tmpWebSalesLine_ID */
    public static final String COLUMNNAME_smj_tmpWebSalesLine_ID = "smj_tmpWebSalesLine_ID";

	/** Set Tmp Web Sales Line	  */
	public void setsmj_tmpWebSalesLine_ID (int smj_tmpWebSalesLine_ID);

	/** Get Tmp Web Sales Line	  */
	public int getsmj_tmpWebSalesLine_ID();

    /** Column name smj_vehicle_ID */
    public static final String COLUMNNAME_smj_vehicle_ID = "smj_vehicle_ID";

	/** Set Vehicle	  */
	public void setsmj_vehicle_ID (int smj_vehicle_ID);

	/** Get Vehicle	  */
	public int getsmj_vehicle_ID();

	public I_SMJ_Vehicle getsmj_vehicle() throws RuntimeException;

    /** Column name smj_workOrderLine_ID */
    public static final String COLUMNNAME_smj_workOrderLine_ID = "smj_workOrderLine_ID";

	/** Set Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID);

	/** Get Work Order Line	  */
	public int getsmj_workOrderLine_ID();

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException;

    /** Column name tax */
    public static final String COLUMNNAME_tax = "tax";

	/** Set Tax	  */
	public void settax (BigDecimal tax);

	/** Get Tax	  */
	public BigDecimal gettax();

    /** Column name total */
    public static final String COLUMNNAME_total = "total";

	/** Set Total	  */
	public void settotal (BigDecimal total);

	/** Get Total	  */
	public BigDecimal gettotal();

    /** Column name uomname */
    public static final String COLUMNNAME_uomname = "uomname";

	/** Set UOM Name	  */
	public void setuomname (String uomname);

	/** Get UOM Name	  */
	public String getuomname();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WarehouseName */
    public static final String COLUMNNAME_WarehouseName = "WarehouseName";

	/** Set Warehouse.
	  * Warehouse Name
	  */
	public void setWarehouseName (String WarehouseName);

	/** Get Warehouse.
	  * Warehouse Name
	  */
	public String getWarehouseName();
}
