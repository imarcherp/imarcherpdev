package com.smj.model;

import java.util.Properties;

public class MSMJWoMechanicAssigned extends X_smj_WoMechanicAssigned{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1492114462529731128L;

	/**
	 * MSMJWoMechanicAssigned constructor
	 * @param ctx
	 * @param smj_WoMechanicAssigned_ID
	 * @param trxName
	 */
	public MSMJWoMechanicAssigned(Properties ctx,
			int smj_WoMechanicAssigned_ID, String trxName) {
		super(ctx, smj_WoMechanicAssigned_ID, trxName);
	}

}//MSMJWoMechanicAssigned
