package com.smj.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.util.GenerateModel;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MLocation;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

/**
 * @version <li>SmartJSP:  2013/01/15 <ul TYPE ="circle">
 *          <li>Clase para importar terceros en vertical de transportes
 *          </ul>
 * @author Pedro Rozo - "SmartJSP" - http://www.smartjsp.com/
 */
public class ImportBusinessParterTransport extends SvrProcess {

	/**	Logger			*/
	private static CLogger	log	= CLogger.getCLogger (GenerateModel.class);
	/** Table				*/
	private int		p_AD_Table_ID = 0;
	/**
	 * parametros iniciales - Inicial parameters
	 */
	@Override
	protected void prepare() {
		//id del registro de reglas
		p_AD_Table_ID = getRecord_ID();
	}//prepare

	@Override
	protected String doIt() throws Exception {
          procesaBPTemporal();
			return "Registros importados";
	
		
	}//doit

	

	/**
	 * Lee registros en la tabla: I_BPartner_Import para inyectarlos a terceros
	 * @return
	 */
	protected Integer procesaBPTemporal(){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM i_bpartner WHERE ad_client_id = 1000000 ");
//		System.out.println("getClient SQL.."+sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String orgId = "1000000";
		Properties ctx = Env.getCtx();
		//orgId = ctx.g
		
		HashMap <String,Object> to = new HashMap<String, Object>() ; 
		int j = 0;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next () ) { //&& j++ < 50){
				// lee campos de la tabla i_bpartner estandar
				to.put("ad_org_id", orgId);
				to.put("ad_client_id", getClient(orgId));
				to.put("lco_taxidtype_id", rs.getString("lco_taxidtype_id"));
				to.put("taxid", rs.getString("taxid"));
				to.put("taxiddigit", rs.getString("taxiddigit"));
				to.put("name", rs.getString("name"));
				to.put("firstname1", rs.getString("firstname1"));
				to.put("firstname2", rs.getString("firstname2"));
				to.put("lastname1", rs.getString("lastname1"));
				to.put("lastname2", rs.getString("lastname2"));
				to.put("iscustomer", rs.getString("iscustomer"));
				to.put("isvendor", rs.getString("isvendor"));
				to.put("isemployee", rs.getString("isemployee"));
				to.put("email", rs.getString("email"));
			
				to.put("address1", rs.getString("address1"));
				to.put("phone", rs.getString("phone"));
				to.put("phone2", rs.getString("phone2"));
				to.put("mobilephone", rs.getString("mobilephone"));
				to.put("city", rs.getString("city"));
				to.put("regionname", rs.getString("regionname"));
				to.put("c_country_id", rs.getString("c_country_id"));
				to.put("lco_taxpayertype_id", rs.getString("lco_taxpayertype_id"));
				to.put("lco_isic_id", rs.getString("lco_isic_id"));
				to.put("name2", rs.getString("name2"));
				
				// nuevos
				to.put("driverlicense", rs.getString("driverlicense"));
				to.put("driverlicensecategory", rs.getString("driverlicensecategory"));
				if (rs.getDate("driverlicenseexpiration") != null)
				 to.put("driverlicenseexpiration", new Timestamp(rs.getDate("driverlicenseexpiration").getTime()));
				to.put("driverlicenseplace", rs.getString("driverlicenseplace"));
				to.put("sizeshirt", rs.getString("sizeshirt"));
				to.put("sizepants", rs.getString("sizepants"));
				to.put("sizeboots", rs.getString("sizeboots"));
				createBPartner(to);
				j++;
				log.log (Level.SEVERE, j + ".Tercero Importado: NIT -->" + rs.getString("taxid"));
				
				
				// pobla coleccion con info de terceros
				
				// invoca creacion de terceros
			}
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return 0;
	}//procesaBPTemporal
	
	private Integer createBPartner(HashMap <String,Object> to){
		Integer code = 0;
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		Integer orgId = Integer.parseInt((String)to.get("ad_org_id"));
		Integer clientId = (Integer)to.get("ad_client_id");
		HashMap<String, Integer> dataLogin = getSalesRep(getAdminLogin(clientId+""));
		try {
			MBPartner partner = new MBPartner(Env.getCtx(), code, trx.getTrxName());
			//partner.setAD_Client_ID(clientId);
			partner.setAD_Org_ID(orgId);
			partner.setIsActive(true);
			//partner.setC_BP_Group_ID(dataLogin.get("c_bp_group_id"));
			//partner.setName2((String) to.get("name2"));
			partner.setName((String) to.get("name"));
			partner.setValue(((String) to.get("name")));
			partner.set_ValueOfColumn("lco_taxidtype_id",to.get("lco_taxidtype_id") );
			partner.setTaxID((String) to.get("taxid"));
			partner.set_ValueOfColumn("taxiddigit", to.get("taxiddigit"));
			partner.set_ValueOfColumn("firstname1", to.get("firstname1"));
			partner.set_ValueOfColumn("firstname2", to.get("firstname2"));
			partner.set_ValueOfColumn("lastname1",  to.get("lastname1"));
			partner.set_ValueOfColumn("lastname2",  to.get("lastname2"));
			partner.set_ValueOfColumn("name2",  to.get("name2"));
	
			partner.set_ValueOfColumn("driverlicense", to.get("driverlicense"));
			partner.set_ValueOfColumn("driverlicensecategory", to.get("driverlicensecategory"));
			partner.set_ValueOfColumn("driverlicenseexpiration", to.get("driverlicenseexpiration"));
			partner.set_ValueOfColumn("sizeshirt", to.get("sizeshirt"));

			partner.set_ValueOfColumn("sizepants", to.get("sizepants"));
			partner.set_ValueOfColumn("sizeboots", to.get("sizeboots"));			
			partner.set_ValueOfColumn("lco_taxpayertype_id", 
					 Integer.parseInt(getTaxpayertypeCode((String)to.get("lco_taxpayertype_id"))));
			 if ((String)to.get("lco_isic_id") != null  )
			    partner.set_ValueOfColumn("lco_isic_id", new Integer (getLcoIsicId((String)to.get("lco_isic_id"))));
			//partner.setIsTaxExempt(Boolean.parseBoolean((String)to.get("taxExempt")));
			
			if (((String)to.get("iscustomer")).equalsIgnoreCase("Y"))
			   partner.setIsCustomer(true);
			else
			   partner.setIsCustomer(false); 	
			
			if (((String)to.get("isvendor")).equalsIgnoreCase("Y"))
				   partner.setIsVendor(true);
			else
				   partner.setIsVendor(false);
			if (((String)to.get("isemployee")).equalsIgnoreCase("Y"))
				   partner.setIsEmployee(true);
			else
				   partner.setIsEmployee(false);
			
			Boolean ok = partner.save();
			
			if (ok){
				if(!partner.getValue().trim().equals(partner.get_ID()+"")){
					partner.setValue(partner.get_ID()+"");
					partner.save();
				}
//				createUser(trx,  to, partner);
				Integer loc = 0;
				// System.out.println("entra a crear Bpartner location con ID: .....  "+ partner.getC_BPartner_ID() );
				if ( (String)to.get("city") != null || (String)to.get("address1") != null) 
				  loc = createBPartnerLocation(trx, to, partner);
				if (loc <= 0){
					code = -1;
					trx.commit();
				}
				else {
					trx.commit();
				}
				
			}else{
				code = -1;
			}//if loc
		}catch (Exception e) {
			log.log (Level.SEVERE, null, e);
            e.printStackTrace();
		} finally{
			// if(trx != null && trx.isActive())
				trx.close();
		}
		
		return code;
	}//createBPartner
	
	/**
	 * Creates contact (user) information for a business partner 
	 * @param trx
	 * @param codes
	 * @param to
	 * @param partner
	 * @return
	 */
	@Deprecated
	private Integer createUser(Trx trx, HashMap<String,Object> to, MBPartner partner){
		Integer code = 0;
		String email =(String)to.get("email") ;
		MUser   mUser = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			if (email==null || email.trim().equals("null")){
					email = "";
			}
				mUser = new MUser(partner);
				
		//	mUser.setAD_Client_ID(dataLogin.get("ad_client_id"));
			//mUser.setAD_Org_ID(dataLogin.get("ad_org_id"));
			mUser.setEMail(email);
			mUser.setName((String) to.get("name"));
			mUser.setPhone((String) to.get("phone2"));
			mUser.setPhone2((String) to.get("mobilephone"));
			
			Boolean lok = mUser.save();
			if (lok){
				code = mUser.get_ID();
			}else{
				return -1;
			}
		}catch(Exception e){
			log.log(Level.WARNING, "al crear la informacion de contacto del tercero ",e);
			e.printStackTrace();
		}finally{
			DB.close(rs, pstmt); 
			rs = null; pstmt = null;
		}
		
		return code;
	}//createUser
	
	
	/**
	 * Creates business partner location
	 *  
	 * @param trx
	 * @param codes
	 * @param to
	 * @param partner
	 * @return
	 */
	private Integer createBPartnerLocation(Trx trx, HashMap<String,Object> to, MBPartner partner){
		Integer code = 0;
		String city = (String) to.get("city");
		Integer location = 0;
		location = createLocation(trx,  to);
		if (location > 0){
			MBPartnerLocation bl = null;
			bl = new MBPartnerLocation(partner);
			bl.setC_Location_ID(location);
			//bl.setAD_Client_ID(to.get("ad_client_id"));
			//bl.setAD_Org_ID(to.get("ad_org_id"));
			bl.setPhone((String) to.get("phone"));
			bl.setPhone2((String) to.get("mobilephone"));
			bl.setFax((String) to.get("phone2"));
			bl.setC_BPartner_ID(partner.get_ID() );
			Boolean lok = bl.save();
			if (lok){
				code = bl.getC_BPartner_Location_ID();
			}else{
				return -1;
			}
		}else{
			return -1;
		}
		return code;
	}//createBPartnerLocation
	
	
	
	
	/**
	 * Creates a location
	 * @param trx
	 * @param codes
	 * @param to
	 * @param create
	 * @param localization
	 * @return
	 */
	private Integer createLocation(Trx trx,  HashMap<String,Object> to){
		MLocation lc = null;
		Integer code = 0;

		
		String country = (String) to.get("c_country_id");
		String region =   getRegionCode((String) to.get("regionname"));
		String cityCod   =   getCityCode( (String) to.get("city"));
		
		if (country.trim()==null || country.trim().equals("null") ||
				region.trim()==null || region.trim().equals("null") ||
				cityCod.trim()==null || cityCod.trim().equals("null")){
			return 0;
		}

		Integer countryId = new Integer(country.trim());
		Integer regionId = new Integer(region.trim());
		Integer cityId = new Integer(cityCod.trim());
		String cityName = (String) to.get("city");
			
			lc = new MLocation(Env.getCtx(), countryId, regionId,cityName, trx.getTrxName());

			lc.setC_City_ID(cityId);
		    lc.setC_Region_ID(regionId);
		    lc.setC_Country_ID(countryId);
		    lc.setCity(cityName);
		    lc.setAddress1(((String) to.get("address1")).trim() + " ");
		
		Boolean ok = lc.save();
		if (ok){
			code = lc.getC_Location_ID();
		}
		return code;
	}//createLocation

	/**
	 * regresa el nombre de la ciudad segun el codigo
	 * @param cityCod
	 * @return
	 */
	protected String getCityName(String cityCod){
		String name = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT name FROM c_city WHERE c_city_id = "+cityCod.trim()+" ");
//		System.out.println("getCityName SQL.."+sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				name = rs.getString("name");
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return name;
	}//getCityName
	
	/**
	 * Retorna el codigo del Ciudad segun el nombre
	 * @param cityCod
	 * @return
	 */
	protected String getCityCode(String cityName){
		String code = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_city_id FROM c_city WHERE name = '"+cityName.trim()+"' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				code = rs.getString("c_city_id");
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return code;
	}//getCityCode
	

	/**
	 * Retorna el codigo del departamento/region segun el nombre
	 * @param regionCod
	 * @return
	 */
	protected String getRegionCode(String regionName){
		String code = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_region_id FROM c_region WHERE name = '"+regionName.trim()+"' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				code = rs.getString("c_region_id");
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return code;
	}//getRegionCode
	
	
	/**
	 * Retorna el codigo del tipo de contribuyente segun el nombre
	 * @param getTaxpayertypeCode
	 * @return
	 */
	protected String getTaxpayertypeCode(String name){
		String code = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT lco_taxpayertype_id  FROM lco_taxpayertype WHERE name = '"+name.trim()+"' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				code = rs.getString("lco_taxpayertype_id");
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//getTaxpayertypeCode	
	
	
	/**
	 * Retorna el codigo interno del tipo de CIIU segun el codigo CIUU (DIAN)
	 * @param getlco_isic
	 * @return
	 */
	protected String getLcoIsicId(String name){
		String code = "";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT lco_isic_id  FROM lco_isic WHERE value = '"+name.trim()+"' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				code = rs.getString("lco_isic_id");
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//getlco_isic
	
	
	
	
	/**
	 * regresa el codigo del cliente a partir de la organizacion
	 * @param code
	 * @return
	 */
	protected Integer getClient(String code){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ad_client_id FROM ad_org WHERE ad_org_id = "+code.trim()+" ");
//		System.out.println("getClient SQL.."+sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		Integer items = 0;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ()){
				items = rs.getInt("ad_client_id");
			}
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return items;
	}//getClient
	
	

	/**
	 * Retrieves the current admin login name for a client
	 * @param clientId
	 * @return
	 */
	protected String getAdminLogin(String clientId){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT U.NAME AS NAME");
		sql.append(" FROM AD_ROLE AS R");
		sql.append(" ,AD_USER_ROLES AS UR");
		sql.append(" ,AD_USER AS U");
		sql.append(" WHERE R.NAME ILIKE '%Admin%' ");
		sql.append(" AND R.ISACTIVE = 'Y' ");
		sql.append(" AND R.AD_CLIENT_ID = "+clientId);
		sql.append(" AND R.AD_ORG_ID = 0");
		sql.append(" AND UR.AD_ROLE_ID = R.AD_ROLE_ID");
		sql.append(" AND U.AD_USER_ID = UR.AD_USER_ID");
		sql.append(" AND R.AD_CLIENT_ID = U.AD_CLIENT_ID");
		sql.append(" AND R.AD_ORG_ID = U.AD_ORG_ID");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String items = "";
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ()){
				items = rs.getString("NAME");
			}else
			//	items =  Msg.getMsg(Env.getCtx(), "userAdmin").trim();
			items = MSysConfig.getValue("SMJ-USERADMIN",clientId).trim();
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return items;
	}//getClient
	
	
	/**
	 * Regresa el valor del ID del representante de ventas, la organizacion, cliente y otros
	 * segun el loginName
	 * @param loginName
	 * @return
	 */
	protected HashMap <String, Integer> getSalesRep(String loginName){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT u.ad_user_id, u.ad_org_id, u.ad_client_id, u.c_bpartner_id, p.c_bp_group_id  FROM ad_user u, c_bpartner p ");
		sql.append(" WHERE u.isactive = 'Y' AND u.c_bpartner_id = p.c_bpartner_id AND u.name = '"+loginName+"' ");
		HashMap <String, Integer> data = new HashMap <String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if(rs.next ()){
				data.put("ad_user_id", rs.getInt("ad_user_id"));
				data.put("ad_org_id", rs.getInt("ad_org_id"));
				data.put("ad_client_id", rs.getInt("ad_client_id"));
				data.put("c_bpartner_id", rs.getInt("c_bpartner_id"));
				data.put("c_bp_group_id", rs.getInt("c_bp_group_id"));
			}
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return data;
	}//getSalesRep
	
	
} 
