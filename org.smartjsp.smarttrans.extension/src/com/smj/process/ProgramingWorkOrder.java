package com.smj.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.smj.model.MSMJScheduleRules;
import com.smj.util.DataQueriesTrans;
import com.smj.webui.component.CreateWOProgram;

/**
 * @version
 *          <li>SmartJSP: WSimplifiedSales, 2013/05/14
 *          <ul TYPE ="circle">
 *          <li>Clase proceso para crear la agenda segun la regla
 *          <li>Class create schedule by rule
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProgramingWorkOrder extends SvrProcess {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	/** Table */
	private Integer p_AD_Table_ID = 0;
	private MSMJScheduleRules rule = null;
	private Boolean isPartial;

	@Override
	protected void prepare() {
		try {
			p_AD_Table_ID = getRecord_ID();
			rule = new MSMJScheduleRules(Env.getCtx(), p_AD_Table_ID, null);
			for (ProcessInfoParameter par: getParameter()) {
				if (par.getParameterName().equals("Partial")) {
					isPartial = par.getParameterAsBoolean();
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName() + "prepare - ERROR: " + e.getMessage(), e);
		}
	}// prepare

	@Override
	protected String doIt() throws Exception {

		CreateWOProgram wop = new CreateWOProgram();
		String msg = null;

		if (rule == null) {
			throw new AdempiereUserError("@SMJMSGCreateProgrammingRule@");
		}
		if (rule.getsmj_brand_ID() <= 0 && rule.getLine() == null && rule.getvehicletype() == null
				&& rule.getsmj_AdministrationType_ID() <= 0 && rule.getsmj_programmingGroup_ID() <= 0
				&& rule.getC_BPartner_ID() <= 0 && rule.getmodelstart() <= 0 && rule.getmodelend() <= 0) {
			throw new AdempiereUserError("@SMJMSGSelecSearchCriteria@");
		}
		if (rule.getperiodicity() == null || rule.getvalueperiodicity().compareTo(Env.ZERO) <= 0) {
			throw new AdempiereUserError("@SMJMSGSelecFrecuency@");
		}
		LinkedList<Integer> acts = wop.getActivity(rule.getsmj_ScheduleRules_ID());
		if (acts == null || acts.size() <= 0) {
			throw new AdempiereUserError("@SMJMSGActivitiesForPRule@");
		}
		LinkedList<HashMap<String, Object>> codes = queryData();
		if (codes == null || codes.size() <= 0) {
			throw new AdempiereUserError("@SMJMSGNoRecordsProgramming@");
		}
		Iterator<HashMap<String, Object>> itmc = codes.iterator();
		String plates = "";
		while (itmc.hasNext()) {
			HashMap<String, Object> map = itmc.next();
			plates = plates + map.get("PLATE") + "; ";
		}
		
		if (isPartial) {
			return plates;
		}
		
		/*msg = String.format(Msg.translate(Env.getCtx(), "SMJMSGPlatesFoundContinue"), plates);
		int response = Messagebox.showDialog(msg, labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);

		// ...reply.."+response);
		if (response == Messagebox.CANCEL) {
			return Msg.translate(Env.getCtx(), "SMJMSGProcesCancelled");
		} // response Ok*/
		
		Iterator<HashMap<String, Object>> itc = codes.iterator();
		String reqs = "";
		while (itc.hasNext()) {
			HashMap<String, Object> map = itc.next();
			Integer code = (Integer) map.get("CODE");
			String ok = wop.saveRequest(get_TrxName(), rule, code, acts);
			
			if (ok == null) {
				throw new AdempiereUserError("@SMJMSGErrorProgrammingRule@");
			} else {
				reqs = reqs + ok + ",";
			}
		} // while(itc.hasNext())
		
		if (reqs.length() > 0) {
			return "@SMJMSGWOCreated@ " + reqs;
		}
		
		return msg;
	}// doIt

	/**
	 * regresa el listado de vehiculos a los que aplica la regla de programacion
	 * - returns vehicle list which the programming rule applies
	 * 
	 * @return
	 */
	private LinkedList<HashMap<String, Object>> queryData() {
		StringBuffer sql = new StringBuffer();

		LinkedList<HashMap<String, Object>> codes = new LinkedList<HashMap<String, Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			sql.append(" SELECT DISTINCT v.smj_vehicle_ID, v.smj_plate FROM smj_vehicle v ");
			sql.append(" WHERE v.isactive = 'Y' AND v.smj_servicerategroup_ID IS NOT NULL ");
			if (rule.getsmj_brand_ID() > 0) {
				sql.append(" AND v.smj_vehiclebrand_id = " + rule.getsmj_brand_ID() + " ");
			}
			if (rule.getLine() != null) {
				sql.append(" AND v.Line = '" + rule.getLine().trim() + "' ");
			}
			if (rule.getvehicletype() != null) {
				sql.append(" AND v.vehicletype = '" + rule.getvehicletype() + "' ");
			}
			if (rule.getsmj_AdministrationType_ID() > 0) {
				sql.append(" AND v.smj_vehicle_id IN ( SELECT DISTINCT smj_vehicle_ID FROM smj_vehicleAdministration ");
				sql.append(" WHERE smj_AdministrationType_ID = " + rule.getsmj_AdministrationType_ID() + ") ");
			}
			if (rule.getsmj_programmingGroup_ID() > 0) {
				sql.append(" AND v.smj_programmingGroup_ID = " + rule.getsmj_programmingGroup_ID() + " ");
			}
			if (rule.getC_BPartner_ID() > 0) {
				sql.append(" AND v.smj_vehicle_id IN ( SELECT DISTINCT smj_vehicle_ID FROM smj_vehicleOwner ");
				sql.append(" WHERE C_BPartner_ID = " + rule.getC_BPartner_ID() + ") ");
			}
			if (rule.getmodelstart() > 0 && rule.getmodelend() > 0) {
				sql.append(" AND v.model BETWEEN " + rule.getmodelstart() + " AND " + rule.getmodelend() + " ");
			} else if (rule.getmodelstart() > 0) {
				sql.append(" AND v.model > " + rule.getmodelstart() + " ");
			}
			if (rule.getthreshold() != null) {
				if (rule.getthreshold().equals("B")) {
					sql.append(" AND v.smj_vehicle_id IN ( SELECT DISTINCT smj_vehicle_ID FROM smj_tires t ");
					sql.append(" WHERE t.isactive = 'Y' ");
					sql.append(" AND valuebrake BETWEEN " + rule.getminthreshold() + " AND " + rule.getmaxthreshold()
							+ ") ");
				} else if (rule.getthreshold().equals("T")) {
					sql.append(" AND v.smj_vehicle_id IN ( SELECT DISTINCT smj_vehicle_ID FROM smj_tires t ");
					sql.append(" WHERE t.isactive = 'Y' ");
					sql.append(" AND valuetire BETWEEN " + rule.getminthreshold() + " AND " + rule.getmaxthreshold()
							+ ") ");
				}
			} // if (rule.getthreshold()
			// SQL::"+sql);
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("CODE", rs.getInt("smj_vehicle_ID"));
				map.put("PLATE", rs.getString("smj_plate"));
				codes.add(map);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueriesTrans.class.getName() + ".queryData - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return codes;
	}// queryData

}// ProgramingWorkOrder
