package com.smj.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.logging.Level;

import org.adempiere.util.GenerateModel;
import org.compiere.model.MBPartnerProduct;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;
import com.smj.util.UtilTrans;


/**
 * @version <li>SmartJSP: ImportProductTransport, 2013/01/22
 *          <ul TYPE ="circle">
 *          <li>Clase para importar productos en vertical de transportes
 *          <li>Class to import products
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ImportProductTransport extends SvrProcess {

	/**	Logger			*/
	private static CLogger	log	= CLogger.getCLogger (GenerateModel.class);
	/** Table				*/
//	private int		p_AD_Table_ID = 0;
	
	private Integer purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_PURCHASELIST",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",Env.getAD_Client_ID(Env.getCtx())).trim());
//	private Integer locatorId = 1000000;
//	private Integer warehouseId = 1000000;
//	private Integer docTypeId = 0;
	private Integer orgId = 1000000;
	private Integer clientId = 1000000;
	private HashMap<String, Integer> category = new HashMap<String, Integer>();
	private HashMap<String, Integer> unit = new HashMap<String, Integer>();
	private HashMap<String, Integer> brand = new HashMap<String, Integer>();
	private Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer tiresWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer navitransWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-NAVITRANSWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer satorcolWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-SATORCOLWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer mercedesWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-MERCEDESWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
	
	/**
	 * parametros iniciales - Inicial parameters
	 */
	@Override
	protected void prepare() {
		//id del registro de reglas
		getCategory();
		getBrand();
		getUnit();
//		p_AD_Table_ID = getRecord_ID();
	}//prepare

	@Override
	protected String doIt() throws Exception {
		procesaProducto();
		return "productos creados";
	}

	/**
	 * Lee registros en la tabla: I_BPartner_Import para inyectarlos a terceros
	 * @return
	 */
	protected Integer procesaProducto(){
		Integer code = 0;
//		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM I_Product WHERE ad_client_id = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		sql.append(" ORDER by  smj_newValue , smj_oldvalue ASC ");
		
		HashMap <String,Object> to = new HashMap<String, Object>() ; 
//		int j = 0;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			//while ( j++ < 15){rs.next();
			while (rs.next () ) {
				// lee campos de la tabla I_Product estandar
				to.put("ad_org_id", orgId);
				to.put("ad_client_id", clientId);
				to.put("Name", rs.getString("Name"));
				to.put("smj_oldvalue", rs.getString("smj_oldvalue"));
				to.put("DocumentNote", rs.getString("DocumentNote"));
				to.put("Value", rs.getString("smj_newValue"));
				to.put("smj_categorydesc", rs.getString("smj_categorydesc"));
				to.put("c_uom_id", rs.getInt("c_uom_id"));
				to.put("smj_uomdesc", rs.getString("smj_uomdesc"));
				to.put("smj_brand", rs.getString("smj_brand"));
				to.put("smj_reference", rs.getString("smj_reference"));
				to.put("Nit", rs.getString("BPartner_Value"));
				to.put("VendorProductNo", rs.getString("smj_vendor"));
				to.put("smj_parentcategorydesc", rs.getString("smj_parentcategorydesc"));
//				to.put("smj_sales", rs.getInt("smj_sales"));
				to.put("PriceList", rs.getBigDecimal("PriceList"));
				to.put("PriceStd", rs.getBigDecimal("PriceStd"));
				to.put("smj_util", rs.getBigDecimal("smj_util"));
				to.put("smj_iva", rs.getString("smj_iva"));
				to.put("smj_codcat", rs.getString("smj_codcat"));
				to.put("smj_codpadcat", rs.getString("smj_codpadcat"));
				createProduct(to);
//				System.out.println("---------------------------------"+(String) to.get("Name")+"---------------------------------");
			}//while
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//procesaBPTemporal
	
	/**
	 * crea el producto - create product
	 * @param trx
	 * @param m
	 * @return HashMap<String, Object> 
	 */
	private void  createProduct(HashMap <String,Object> to){
		String name = "";
		String value = "";
		Trx trx = Trx.get(Trx.createTrxName("AL"), true);
		try	{
			Boolean ok = true;
			name = (String) to.get("Name");
			value = (String) to.get("Value");
			String oldValue = (String) to.get("smj_oldvalue");
			Integer productId = DataQueriesTrans.getProductByValue(clientId, value);
			MProduct p = new MProduct(Env.getCtx(), productId, trx.getTrxName());
			Boolean pricList = true;
			Boolean proveeedor = true;
			Boolean created = false;
			BigDecimal tax = Env.ZERO;
			String nit = (String) to.get("Nit");
			if (nit == null)
				nit = "";
			Integer bparnerId = DataQueriesTrans.getBpartnerId(clientId, nit);
			Boolean list = true;
			BigDecimal price = (BigDecimal) to.get("PriceList");
			String listName = "";

			if(productId <= 0 ){
			p.setName(name);
			p.setAD_Org_ID(orgId);
			p.setDescription((String)to.get("smj_reference"));
			p.setValue(value);
			p.set_CustomColumn("smj_oldProductCode", oldValue);
			p.set_CustomColumn("smj_oldProductName", to.get("DocumentNote"));
			p.setSKU(oldValue);
			
			Integer categoryId = category.get((String) to.get("smj_categorydesc"));
			if (categoryId == null || categoryId <= 0){
				categoryId = createCategory(trx.getTrxName(), (String)to.get("smj_codcat"), (String)to.get("smj_categorydesc"), (String)to.get("smj_parentcategorydesc"), (String)to.get("smj_codpadcat"));
			}
			p.setM_Product_Category_ID(categoryId);
			if (categoryId == 1000024){
				p.set_CustomColumn("LCO_WithholdingCategory_ID", 1000036);
			}else{
				p.set_CustomColumn("LCO_WithholdingCategory_ID", 1000032);
			}
			String iva = (String) to.get("smj_iva");
			if (iva.equals("16")){
				p.setC_TaxCategory_ID(1000005);
				
			}else{
				p.setC_TaxCategory_ID(1000004);
			}
			tax = new BigDecimal(iva);
			Integer brandId = brand.get((String) to.get("smj_brand"));
			if (brandId != null){
				p.set_ValueOfColumn("smj_brand_ID", brandId);
			}
			if (categoryId.equals(tiresCategory)){
				p.setM_Locator_ID(tiresWarehouse);
				listName = "Llantas";
			}else if(oldValue.startsWith("25")){ //navitrans
				p.setM_Locator_ID(navitransWarehouse);
				if(bparnerId>0){
					listName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bparnerId);
				}
			}else if(oldValue.startsWith("26")){ //satorcol e industrias y negocios
				p.setM_Locator_ID(satorcolWarehouse);
				if(bparnerId>0){
					listName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bparnerId);
				}
			}else if(oldValue.startsWith("32")){ //mercedes benz
				p.setM_Locator_ID(mercedesWarehouse);
				if(bparnerId>0){
					listName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bparnerId);
				}
			}else{
				p.setM_Locator_ID(principalWarehouse);
				listName = "Mostrador";
			}
			Integer uomId = unit.get("smj_uomdesc");
			if (uomId == null || uomId<= 0){
				uomId = 100;
			}
			p.setC_UOM_ID(uomId);
			p.set_ValueOfColumn("smj_utility", (BigDecimal) to.get("smj_util"));
			ok = p.save();
			productId = p.getM_Product_ID();
			if(ok){
				created = true;
//					System.out.println("antes de lista de precios.....");
					pricList = createPriceList(trx.getTrxName(), price, bparnerId, productId, (BigDecimal) to.get("smj_util"), listName, tax);
					list = false;
				}else{
					System.out.println("Error al guardar el producto");
				}//if(productId > 0 ){
				
			}//(productId <= 0 )
			/////// tab Compras
			if (productId > 0 ){
					String vendor = (String) to.get("VendorProductNo");
					if(bparnerId>0){
//						MProductPO mpo = new MProductPO(Env.getCtx(), 0, trx.getTrxName());
//						mpo.setAD_Org_ID(orgId);
//						mpo.setM_Product_ID(productId);
//						mpo.setC_BPartner_ID(bparnerId);
//						if (vendor!=null && vendor.length() > 0){
//							mpo.setVendorProductNo(vendor);
//						}else{
//							mpo.setVendorProductNo(value);
//						}
//						mpo.setC_UOM_ID((Integer) to.get("c_uom_id"));
//						proveeedor = mpo.save();
						MBPartnerProduct mpro = new MBPartnerProduct(Env.getCtx(), 0, trx.getTrxName());
						mpro.setAD_Org_ID(orgId);
						mpro.setM_Product_ID(productId);
						mpro.setC_BPartner_ID(bparnerId);
						mpro.setShelfLifeMinPct(0);
						mpro.setShelfLifeMinDays(0);
						mpro.setQualityRating(Env.ZERO);
						
						if (vendor!=null && vendor.length() > 0){
							mpro.setVendorProductNo(vendor);
						}
//						else{
//							mpro.setVendorProductNo(value);
//						}
						proveeedor = mpro.save();
						if	(!proveeedor){
							System.out.println("error pro....."+name+".."+value+"..cbpar..."+bparnerId);
						}
						if (p.getM_Product_Category_ID()!= tiresCategory && p.getM_Locator_ID() != principalWarehouse && list){
							listName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bparnerId);
							pricList = createPriceList(trx.getTrxName(), price, bparnerId, productId, (BigDecimal) to.get("smj_util"), listName, tax);
						}//No llantas, no principal warehouse
					}//if(bparnerId>0){
				if(!created){
					if (!p.get_ValueAsString("smj_oldProductCode").trim().equals(oldValue.trim())){
						String val = p.get_ValueAsString("smj_oldProductCode")+";"+oldValue;
						p.set_CustomColumn("smj_oldProductCode", val);
						p.setSKU(val);
						p.save();
					}	
				}//(!created)
			}//productId > 0)
			if(ok && pricList && proveeedor){
				trx.commit();
			}else{
				trx.rollback();
			}
		}catch (Exception e) {
			System.out.println("Name: "+name+", value: "+value);
			System.out.println("ERROR: "+e.getMessage());
			System.out.println("ERROR: "+e.toString());
		}
		finally{
			trx.close();
		}
	}//createProduct

	/**
	 * crea lista de precios - create pricelist 
	 * @param trxName
	 * @param price
	 * @param bparnerId
	 * @param productId
	 * @param util
	 * @param listName
	 * @return
	 */
	private Boolean createPriceList(String trxName, BigDecimal price, Integer bparnerId, Integer productId, 
					BigDecimal util, String listName, BigDecimal tax){
		Boolean flag = true;
		Boolean pru = true;
		Boolean su = true;
		// /// lista de precios de compra
		
		String namePrice = "";
		try{
			Integer pId = 0;
			if (listName.length()>0){
				namePrice = "Compra_"+listName.trim();
				pId = DataQueriesTrans.getPriceListVersion(trxName,Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (pId <=0){
					pId = UtilTrans.createPriceListVersion(namePrice, purchaseId);
				}
				if (pId <=0){
					pId =  purchaseId;
				}
			}else{
				pId =  purchaseId;
			}
			if(!DataQueriesTrans.validatePriceListExists(productId, pId)){
				MProductPrice pur = new MProductPrice(Env.getCtx(), pId, productId,
						price, price, price, trxName);
				pur.setAD_Org_ID(orgId);
				if (pur != null) {
					pru = pur.save();
				}// if(pur != null){
			}
				// ///LIstas de precios de venta
			Integer sId = 0;
			if (listName.length()>0){
				namePrice = "Venta_"+listName.trim();
				sId = DataQueriesTrans.getPriceListVersion(trxName, Env.getAD_Client_ID(Env.getCtx()), namePrice);
				if (sId <=0){
					sId = UtilTrans.createPriceListVersion(namePrice, salesId);
				}
				if (sId <=0){
					sId =  salesId;
				}
			}else{
				sId =  salesId;
			}
	
			if(!DataQueriesTrans.validatePriceListExists(productId, sId)){
				BigDecimal salesPrice = UtilTrans.calculateSalesPrice(price, util, tax);
				MProductPrice sal = new MProductPrice(Env.getCtx(), sId, productId,
						salesPrice, salesPrice, salesPrice, trxName);
				sal.setAD_Org_ID(orgId);
				if (sal != null) {
					su = sal.save();
				}// if(sal != null){
			}
			
			if (!pru || !su) {
				flag = false;
				System.out.println("Error en listas de precios - compra:"+pru+" - Venta: "+su);
				System.out.println("namePrice "+namePrice);
			}// if(!pru || !su){
		}catch (Exception e) {
			System.out.println("Error en listas de precios");
			System.out.println("ERROR:: "+e.getMessage()+ "e::"+e);
		}
		return flag;
	}//crearPriceList
	
	/**
	 * obtiene lista de categorias de producto - 
	 * get product category list
	 */
	private void getCategory (){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT M_Product_Category_ID, name FROM M_Product_Category ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = "+clientId+" ");
//		System.out.println("getCategory SQL::"+sql.toString());
		category = new HashMap<String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String key = rs.getString("name").trim();
				Integer value = rs.getInt("M_Product_Category_ID");
				category.put(key.trim(), value);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getCategory
	
	/**
	 * obtiene lista de unidades - 
	 * get unit list 
	 */
	private void getUnit (){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_UOM_ID, name FROM C_UOM_trl  ");
		sql.append(" WHERE ad_client_id in ( 1000000,0) ");
//		System.out.println("getCategory SQL::"+sql.toString());
		unit = new HashMap<String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String key = rs.getString("name").trim();
				Integer value = rs.getInt("C_UOM_ID");
				unit.put(key.trim(), value);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getUnit
	
	/**
	 * obtiene lista de marcas de producto - 
	 * get product brand list
	 */
	private void getBrand(){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_brand_ID, name FROM smj_brand ");
//		System.out.println("getBrand SQL::"+sql.toString());
		brand = new HashMap<String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String key = rs.getString("name").trim();
				Integer value = rs.getInt("smj_brand_ID");
				brand.put(key.trim(), value);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getCategory

	/***
	 * crea la categoria del producto y el padre si no existe -
	 * create product category and parent if not exist
	 * @param trxName
	 * @param value
	 * @param desc
	 * @param pName
	 * @param pvalue
	 * @return
	 */
	private Integer createCategory(String trxName, String value, String desc, String pName, String pvalue){
		Integer padreId = category.get(pName);
		if (padreId==null || padreId<=0){
			MProductCategory padre = new MProductCategory(Env.getCtx(), 0, trxName);
			padre.setAD_Org_ID(orgId);
			padre.setName(pName);
			padre.setValue("0000"+pvalue);
			padre.save();
			padreId = padre.getM_Product_Category_ID();
		}
		MProductCategory cat = new MProductCategory(Env.getCtx(), 0, trxName);
		cat.setAD_Org_ID(orgId);
		cat.setName(desc);
		cat.setValue("0000"+value);
		cat.setM_Product_Category_Parent_ID(padreId);
		cat.save();
		category.put(desc, cat.getM_Product_Category_ID());
		return cat.getM_Product_Category_ID();
	}//createCategory
	
}//ImportProductTransport
