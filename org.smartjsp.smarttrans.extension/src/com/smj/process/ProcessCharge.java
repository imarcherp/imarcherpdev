package com.smj.process;

import java.sql.Timestamp;
import java.util.Date;

import org.adempiere.util.GenerateModel;
import org.compiere.model.MCharge;
import org.compiere.model.MInvoice;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.model.MSMJCharges;
import com.smj.util.DataQueriesTrans;
import com.smj.util.DocumentsTrans;
import com.smj.util.ShowWindowTrans;
import com.smj.util.UtilTrans;

/**
 * @version <li>SmartJSP: ImportProductTransport, 2013/08/09
 *          <ul TYPE ="circle">
 *          <li>Clase para crear pagos de cargos -Infotrans
 *          <li>Class to create payment from charges
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProcessCharge extends SvrProcess{

	/**	Logger			*/
	protected static CLogger	log	= CLogger.getCLogger (GenerateModel.class);

	private Integer		p_AD_Table_ID = 0;
	Timestamp dateT = Env.getContextAsDate(Env.getCtx(), "#Date");
	@Override
	protected void prepare() {
		p_AD_Table_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {

		if(p_AD_Table_ID <= 0){
			throw new AdempiereUserError("@SMJMSGSaveRecord@");
		}

		if (!DataQueriesTrans.isPeriodOpen(new Date(dateT.getTime()), Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()))){
			throw new AdempiereUserError("@SMJMSGClosedPeriod@");
		}
		Integer logRol = Env.getAD_Role_ID(Env.getCtx());
		String rols = MSysConfig.getValue("SMJ-INFCREATEPAYMENT",Env.getAD_Client_ID(Env.getCtx())).trim();
		Boolean rolPer = UtilTrans.validateRol(logRol, rols);

		if(!rolPer){
			throw new AdempiereUserError("@SMJMSGActionNoRole@");
		}

		MSMJCharges ca = new MSMJCharges(Env.getCtx(), p_AD_Table_ID, get_TrxName());

		if(!ca.isApproved()){
			throw new AdempiereUserError("@SMJMSGAppCharge@");
		}

		if (ca.getC_Invoice_ID()>0){
			throw new AdempiereUserError("@SMJMSGInvExistbyCharge@");
		}

		MRequest req = new MRequest(Env.getCtx(), ca.getR_Request_ID(), get_TrxName());
		Integer salesRepId = Env.getAD_User_ID(Env.getCtx());

		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		int clientCode = Env.getAD_Client_ID(  Env.getCtx());
		Integer documentARInv= Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTAPINVOICE",clientCode,orgId).trim()); // obtiene el tipo por organizacion

		Integer priceListId = Integer.parseInt(MSysConfig.getValue("SMJ_INFPURCHASELIST",Env.getAD_Client_ID(Env.getCtx()), orgId).trim());
		Integer paymentIdDef = Integer.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMDEFAULT",Env.getAD_Client_ID(Env.getCtx())).trim());
		MCharge ch = MCharge.get(Env.getCtx(), ca.getC_Charge_ID());

		String description = "Solicitud: "+req.getDocumentNo()+ " - Cargo: "+ch.getName();
		//String cusPlate = req.get_ValueAsString("smj_customerplate");
		Boolean isadvanced = DataQueriesTrans.validateChargeWithAdvances(get_TrxName(), ca.getsmj_charges_ID());

		String msgAdvanced = "";
		if (isadvanced){
			msgAdvanced = "@SMJMSGChargeHasAdvances@";
		}

		Timestamp dateT = Env.getContextAsDate(Env.getCtx(), "#Date");
		MInvoice inv = DocumentsTrans.createInvoiceNoIo(get_TrxName(), description, ca.getC_BPartner_ID(), dateT, 
				priceListId, documentARInv, paymentIdDef, "", 0, "", false, salesRepId,"0",false);

		if(inv != null){
			Integer taxId = DataQueriesTrans.getTaxIdByCategory(ch.getC_TaxCategory_ID(), true);
			if(taxId <= 0){
				taxId = DataQueriesTrans.getTaxIdByCategory(ch.getC_TaxCategory_ID(), false);
			}
			Integer iLine = DocumentsTrans.createInvoiceLine(inv, description, 0, ca.getsmj_value(), taxId, 
					ca.getsmj_value(), Env.ONE, 0, 0, "", 10, 0, ch.getC_Charge_ID());

			if (iLine == null || iLine <= 0){
				throw new AdempiereUserError("@SMJMSGErrorCreateInvoiceLine@");
			}

			if (inv != null && inv.getC_Invoice_ID() > 0){
				DataQueriesTrans.updateCharge(get_TrxName(), p_AD_Table_ID, inv.getC_Invoice_ID());
				ShowWindowTrans.openInvoiceVendor(inv.get_ID());
				return "@SMJMSGInvocesCreated@: " + inv.getDocumentNo() + ". \n" + msgAdvanced;
			}

		}else{
			throw new AdempiereUserError("@SMJMSGErrorCreateInvoice@");
		}

		return null;
	}

}//ProcessCharge
