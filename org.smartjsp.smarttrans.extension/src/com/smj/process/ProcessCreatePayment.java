package com.smj.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.util.GenerateModel;
import org.adempiere.webui.apps.AEnv;
import org.compiere.model.MCharge;
import org.compiere.model.MPayment;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.model.MSMJCharges;
import com.smj.model.MSMJControlAdvances;
import com.smj.util.DataQueriesTrans;
import com.smj.util.DocumentsTrans;
import com.smj.util.Message;
import com.smj.util.ShowWindow;
import com.smj.util.UtilTrans;

/**
 * @version <li>SmartJSP: ImportProductTransport, 2013/08/08
 *          <ul TYPE ="circle">
 *          <li>Clase para crear pagos de adelantos -Infotrans
 *          <li>Class to create payment from advance
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProcessCreatePayment  extends SvrProcess {

	/**	Logger			*/
	private static CLogger	log	= CLogger.getCLogger (GenerateModel.class);
	
//	private final static String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",Env.getAD_Client_ID(Env.getCtx())).trim();
//	private final static String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",Env.getAD_Client_ID(Env.getCtx())).trim();
//	private final static String MsgClosedPeriod = Msg.translate(Env.getCtx(), "smjMsgClosedPeriod").trim();
	Timestamp dateT = Env.getContextAsDate(Env.getCtx(), "#Date");
	private Integer		p_AD_Table_ID = 0;
	
	@Override
	protected void prepare() {
		p_AD_Table_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
			if(p_AD_Table_ID <= 0){
				throw new AdempiereUserError("@SMJMSGSaveRecord@");
			}
			
			if (!DataQueriesTrans.isPeriodOpenAPP(new Date(dateT.getTime()), Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()))){
				throw new AdempiereUserError("@SMJMSGClosedPeriod@");
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			String rols = MSysConfig.getValue("SMJ-INFCREATEPAYMENT",Env.getAD_Client_ID(Env.getCtx())).trim();
			Boolean rolPer = UtilTrans.validateRol(logRol, rols);
			if(!rolPer){
				throw new AdempiereUserError("@SMJMSGActionNoRole@");
			}
			MSMJControlAdvances ca = new MSMJControlAdvances(Env.getCtx(), p_AD_Table_ID, get_TrxName());
			if(!ca.isApproved()){
				throw new AdempiereUserError("@SMJMSGAppAdvance@");
			}
			if (ca.getC_Payment_ID()>0){
				throw new AdempiereUserError("@SMJMSGPayExistbyAdvance@");
			}
			MRequest req = new MRequest(Env.getCtx(), ca.getR_Request_ID(), get_TrxName());
			try {
				BigDecimal vhCharge = new BigDecimal(req.get_ValueAsString("smj_vehiclecharge"));
				BigDecimal tot = DataQueriesTrans.getAdvanceTotalRequest(null, ca.getR_Request_ID());
				if(ca.getsmj_charges_ID() <= 0){
					if(tot.compareTo(vhCharge)>0) {
						AEnv.executeDesktopTask(new Runnable() {							
							@Override
							public void run() {
								Message.showWarning("SMJMSGAdvancesExcedVhCharge");								
							}
						});
					}
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName() + ".doIt - ERROR: "
						+ e.getMessage(), e);
				throw e;
			}
			String description = "Solicitud: "+req.getDocumentNo()+ " - Adelanto Placa: "+req.get_ValueAsString("smj_plate");
			Integer chPartner = 0;
			if(ca.getsmj_charges_ID() > 0){
				MSMJCharges cx = new MSMJCharges(Env.getCtx(), ca.getsmj_charges_ID(), get_TrxName());
				MCharge ch = MCharge.get(Env.getCtx(), cx.getC_Charge_ID());
				description = "Solicitud: "+req.getDocumentNo()+ " - Cargo: "+ch.getName();
				chPartner = cx.getC_BPartner_ID();
			}
			Integer tenderID = req.get_ValueAsInt("c_bpartnerowner_ID");
			// solicitud 1000407 - validacion de que pagos a tercero de infotrans con org. infotrans son al conductor
			// requiere creacion de variable sysconfig a nivel de compa�ia, con el id interno de tercero infotrans: SMJ_BPINFOTRANS
			int orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
			int bpInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_BPINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
			//Adicion iMarch / Tercero Organizacion---
			int bpOrg = Integer.parseInt(MSysConfig.getValue("iMarch_bpOrg", "0", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim());
			String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
			//--------------------------------------
			
			Integer driverId = req.get_ValueAsInt("c_bpartnerdriver_id");
			
			if (tenderID.equals(bpInfotrans) && (req.getAD_Org_ID() == orgInfotrans)) 
			{   
				tenderID = driverId;
			}
			//Adicion iMarch / Organizacion Transportadora--
			if(orgfreighttrans.equals("true") && tenderID.equals(bpOrg)) tenderID = driverId;
			//----------------------------------------------
			// termina codigo de solicitud
			if(tenderID == null || tenderID <= 0){
				throw new AdempiereUserError("@SMJMSGErrorAdvanceNOOwner@");
			}
			if(ca.getsmj_charges_ID() > 0 && chPartner > 0){
				tenderID = chPartner;
			}
			MPayment pay =  createPayment(get_TrxName(), ca.getsmj_value(), description, tenderID, ca.getTenderType(), 
					"", "", "", 0, 0, "", "", ca.getAccountNo(), "", ca.getMicr(), 0, Env.ZERO, Env.ZERO);
			if (pay != null && pay.getC_Payment_ID() > 0){
				DataQueriesTrans.updateAdvance(get_TrxName(), p_AD_Table_ID, pay.getC_Payment_ID());
				ShowWindow.openPayment(pay.get_ID());
				return "@PaymentCreated@ " + pay.getDocumentNo();
			}else{
				throw new AdempiereUserError("@SMJMSGErrorCreatePayment@");
			}
	}//doIt 
	
	/**
	 * crea el pago correspondiente - create payment
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param A_Name
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @return Integer
	 */
	private MPayment createPayment(String trxName, BigDecimal value, String description, Integer bpartnerId,
			String tenderType, String CreditCardType, 
			String CreditCardNumber, String newCreditCardVV, Integer CreditCardExpMM, Integer newCreditCardExpYY, 
			String A_Name, String routingNo, String accountNo ,String checkNo,
			String micr, Integer payTermId, BigDecimal writeOffAmt, BigDecimal discountAmt){
//		System.out.println(this.getClass().getName()+".createPayment ================inico de createPayment........."+Env.getAD_Org_ID(Env.getCtx()));
		Integer paymentId = -1;
		int orgCode = Env.getAD_Org_ID(Env.getCtx());
		int clientCode = Env.getAD_Client_ID(  Env.getCtx());
		Integer bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-ACCOUNTDEFAULT",clientCode,orgCode).trim());
		
		MPayment pay = null;
		Integer docTypeID = Integer.parseInt(MSysConfig.getValue("SMJ-APPAYMENTDOC",clientCode,orgCode).trim()); 
			   // Integer.parseInt(MSysConfig.getValue("SMJ-APPAYMENTDOC",Env.getAD_Client_ID(Env.getCtx())).trim());
		pay = DocumentsTrans.createPaymentNoinvoice(trxName, description, bpartnerId, value, bankAccountId, tenderType, 
					CreditCardType, CreditCardNumber, newCreditCardVV, CreditCardExpMM, newCreditCardExpYY, 
					"", routingNo, accountNo, checkNo, micr, writeOffAmt, discountAmt, docTypeID, true);
		paymentId = pay.getC_Payment_ID();
		pay.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		pay.save();
//		System.out.println(this.getClass().getName()+".createPayment-pago creado...."+pay.getDocumentNo()+"......org..."+pay.getAD_Org_ID());
		if (pay == null || paymentId <= 0){
//			System.out.println(this.getClass().getName()+".createPayment no creo pago...."+paymentId+"..pay..."+pay);
			log.log(Level.SEVERE,
					this.getClass().getName() + ".createPayment - Error No Create Payment :: " + description);
			return null;
		}
		return pay;
	}//createPayment

	
}//ProcessCreatePayment
