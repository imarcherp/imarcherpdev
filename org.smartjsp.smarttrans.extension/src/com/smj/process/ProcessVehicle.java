package com.smj.process;

import java.io.File;

import org.compiere.process.SvrProcess;

import com.smj.csv.VehicleCsvDAO;

/**
 * @version <li>SmartJSP: ProcessVehicle, 2013/02/28 <ul TYPE ="circle">
 *          <li>proceso para la carga tabla vehiculo y sus relacionadas
 *          <li>process to load table vehicle and relateed
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProcessVehicle  extends SvrProcess {

	@Override
	protected void prepare() {
		
	}

	@Override
	protected String doIt() throws Exception {
		String baseDirectory = "F:\\smartjsp\\seveter\\Buses\\new";
		String noLoadFiles = "F:\\smartjsp\\seveter\\Buses\\noload";
		String processedDirectory = "F:\\smartjsp\\seveter\\Buses\\proc";
		VehicleCsvDAO d = new VehicleCsvDAO();
		File dirDaily = new File(baseDirectory);
		String myFilesD[] = dirDaily.list();
		Boolean flag = true;
		if (myFilesD.length<=0)
			return noLoadFiles+" "+baseDirectory;
		// process all the files included in the postpago directory
		for (String myFile : myFilesD) {
			File f = new File(baseDirectory, myFile);
			flag = d.load(f, ";", processedDirectory);
		}// for
		
		if (flag)
			return "proceso Exitoso";
		else
			return "fallo proceso";
	}//doIt

}//ProcessVehicle
