package com.smj.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.GenerateModel;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zul.Messagebox;

import com.smj.util.DataQueriesTrans;
import com.smj.util.DocumentsTrans;
import com.smj.util.UtilTrans;

/**
 * @version
 *          <li>SmartJSP: ImportProductTransport, 2013/09/03
 *          <ul TYPE ="circle">
 *          <li>Clase para crear factura de proveedor en la orden de servicio
 *          -Infotrans
 *          <li>Class to create provider invoice in servide order
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProcessCreateInvoiceProvider extends SvrProcess {

	/** Logger */
	private static CLogger log = CLogger.getCLogger(GenerateModel.class);
	private Integer paytermDef = Integer
			.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMDEFAULT", Env.getAD_Client_ID(Env.getCtx())).trim());
	Integer orgId = Env.getAD_Org_ID(Env.getCtx());
	int clientCode = Env.getAD_Client_ID(Env.getCtx());
	private Integer docAPInvoice = Integer
			.parseInt(MSysConfig.getValue("SMJ-DOCUMENTAPINVOICE", clientCode, orgId).trim()); // obtiene
																								// el
																								// tipo
																								// por
																								// organizacion
	private Integer purchaseList = Integer
			.parseInt(MSysConfig.getValue("SMJ_INFPURCHASELIST", clientCode, orgId).trim());
	Timestamp dateT = Env.getContextAsDate(Env.getCtx(), "#Date");

	private Integer p_AD_Table_ID = 0;

	@Override
	protected void prepare() {
		p_AD_Table_ID = getRecord_ID();
	}// prepare

	@Override
	protected String doIt() throws Exception {
		if (p_AD_Table_ID <= 0) {
			throw new AdempiereException("@SMJMSGSaveRecord@");
		}
		// fix 381 -- validar periodo basado en la fecha de login (antes la del
		// servidor)
		if (!DataQueriesTrans.isPeriodOpenAPI(new Date(dateT.getTime()), Env.getAD_Client_ID(Env.getCtx()),
				Env.getAD_Org_ID(Env.getCtx()))) {
			throw new AdempiereException("@SMJMSGClosedPeriod@");
		}

		MRequest req = new MRequest(Env.getCtx(), p_AD_Table_ID, get_TrxName());
		try {
			Integer invId = req.get_ValueAsInt("smj_invoiceprovider_id");
			if (invId != null && invId > 0) {
				throw new AdempiereException("@SMJMSGProviderInvoiceExist@");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName() + ".doIt - ERROR: " + e.getMessage(), e);
			throw e;
		}
		String customerPlate = req.get_ValueAsString("smj_plate");
		String description = "Solicitud: " + req.getDocumentNo();
		if (customerPlate != null && customerPlate.length() > 0) {
			description = description + " - " + Msg.translate(Env.getCtx(), "smj_plate") + " : " + customerPlate;
		}
		String manifest = req.get_ValueAsString("smj_manifest");

		if (manifest != null && manifest.length() > 0) {
			description = description + " - " + Msg.getMsg(Env.getCtx(), "smj_manifest") + " : " + manifest;
		}

		Boolean approvated = req.get_ValueAsBoolean("smj_isinvoicecharge");
		if (!approvated) {
			throw new AdempiereException("@SMJMSGChargeMustBeApprobated@");
		}
		Integer bpartnerId = req.get_ValueAsInt("c_bpartnerowner_ID");
		if (bpartnerId == null || bpartnerId <= 0) {
			throw new AdempiereException("@SMJMSGErrorAdvanceNOOwner@");
		}
		Integer locationId = DataQueriesTrans.getLocationPartner(bpartnerId);
		if (locationId == null || locationId <= 0) {
			throw new AdempiereException("@SMJMSGPartnerValidLocation@");
		}

		Integer paymentTermId = paytermDef;
		MBPartner partner = MBPartner.get(Env.getCtx(), bpartnerId);
		if (partner.getC_PaymentTerm_ID() > 0) {
			paymentTermId = partner.getC_PaymentTerm_ID();
		}
		// Solicitud 1000387 aqui se adiciona logica para que dependiendo del
		// tipo de contribuyente
		// se pueda generar doucmentos diferentes

		Integer idTaxTypeBP = partner.get_ValueAsInt("lco_taxpayertype_id"); // obtiene
																				// el
																				// tipo
																				// de
																				// contribuyente
		//
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		int clientCode = Env.getAD_Client_ID(Env.getCtx());
		String taxTypesRequiredToInvoice = MSysConfig.getValue("SMJ-TAXPAYERTYPES-INVOICEREQUIRED", clientCode, orgId)
				.trim();
		// identifica si el tipo de contribuyente del tercero actual
		// esta dentro de la lista de los obligados a facturar
		boolean requiredInvoice = UtilTrans.validateRol(idTaxTypeBP, taxTypesRequiredToInvoice);

		// obtiene el numero manual de factura proveedor y lo usa como
		// consecutivo por defecto
		String sequence = req.get_ValueAsString("smj_invoiceprovider_ID2");

		if (!requiredInvoice) // no es requerido que se facture
		{
			// se desactiva generacion de documento equivalente, segun solicitud
			// 1000494

			docAPInvoice = Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTAPINVOICE-IT3", clientCode, orgId).trim()); // obtiene
																															// el
																															// tipo
																															// por
																															// organizacion
			sequence = "0"; // secuencia automatica

		} else {
			if (sequence.equalsIgnoreCase("")) { // si no se especifico numero
													// de factura y erea
													// requerido devuelve
													// mensaje de error
				throw new AdempiereException("N�mero de factura de proveedor manual no puede estar vac�a ");
			}
		}

		Integer salesRepId = Env.getAD_User_ID(Env.getCtx());
		// solicitud 1000381 - cambia al dia de entrada - antes lo dejaba con el
		// dia del servidor

		MInvoice inv = DocumentsTrans.createInvoiceNoIo(get_TrxName(), description, bpartnerId, dateT, purchaseList,
				docAPInvoice, paymentTermId, customerPlate, 0, req.getDocumentNo(), false, salesRepId, sequence, false);
		if (inv != null) {
			BigDecimal price = new BigDecimal(req.get_ValueAsString("smj_vehiclecharge"));
			Integer taxId = DataQueriesTrans.getProductTax(req.getM_Product_ID());
			Integer iLineId = DocumentsTrans.createInvoiceLine(inv, description, req.getM_Product_ID(), price, taxId, price,
					Env.ONE, 0, 0, customerPlate, 10, 0, 0);
			if (iLineId <= 0) {
				throw new AdempiereException("@SMJMSGErrorCreateInvoiceLine@");
			}

			DataQueriesTrans.updateRequestInvoiceProvider(get_TrxName(), p_AD_Table_ID, inv.getC_Invoice_ID());

			return "@SMJMSGInvocesCreated@: " + inv.getDocumentNo();
		} else {
			Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGErrorCreateInvoice"), "Info", Messagebox.OK,
					Messagebox.EXCLAMATION);
			throw new AdempiereException("@@");
		}
	}// doIt

}// ProcessCreateInvoiceProvider
