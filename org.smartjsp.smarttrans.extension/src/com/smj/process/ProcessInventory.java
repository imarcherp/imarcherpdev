package com.smj.process;

import java.io.File;

import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.smj.csv.InventoryCsvDAO;

public class ProcessInventory extends SvrProcess {

private String basePath = MSysConfig.getValue("SMJ-LOADFILEPATH",Env.getAD_Client_ID(Env.getCtx())).trim(); 
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected String doIt() throws Exception {
		String baseDirectory = basePath+"/files/inventory";
		String noLoadFiles = basePath+"/noload";
		String processedDirectory = basePath+"/proc";
//		String basePath = "F:\\smartjsp\\seveter\\load";
//		String baseDirectory = basePath+"\\files\\inventory";
//		String noLoadFiles = basePath+"\\noload";
//		String processedDirectory = basePath+"\\proc";
		System.out.println(this.getClass().getCanonicalName()+".path...."+baseDirectory);
		InventoryCsvDAO d = new InventoryCsvDAO();
		File dirDaily = new File(baseDirectory);
		String myFilesD[] = dirDaily.list();
		System.out.println("myfiles.."+myFilesD);
		String msg = "";
		if (myFilesD.length<=0)
			return noLoadFiles+" "+baseDirectory;
		// process all the files included in the postpago directory
		for (String myFile : myFilesD) {
			File f = new File(baseDirectory, myFile);
			msg = d.load(f, ";", processedDirectory);
		}// for
		
		if (msg != null && msg.length() > 0)
			return msg;
		else
			return "proceso Exitoso";
	}//doIt

}//ProcessInventory
