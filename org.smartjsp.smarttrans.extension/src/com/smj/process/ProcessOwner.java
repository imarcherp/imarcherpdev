package com.smj.process;

import java.io.File;

import org.compiere.process.SvrProcess;

import com.smj.csv.OwnerCsvDAO;

public class ProcessOwner  extends SvrProcess {

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String doIt() throws Exception {
		String baseDirectory = "F:\\smartjsp\\seveter\\Buses\\driver";
		String noLoadFiles = "F:\\smartjsp\\seveter\\Buses\\noload";
		String processedDirectory = "F:\\smartjsp\\seveter\\Buses\\proc";
		OwnerCsvDAO d = new OwnerCsvDAO();
		File dirDaily = new File(baseDirectory);
		String myFilesD[] = dirDaily.list();
		System.out.println("myfiles.."+myFilesD);
		Boolean flag = true;
		if (myFilesD.length<=0)
			return noLoadFiles+" "+baseDirectory;
		// process all the files included in the postpago directory
		for (String myFile : myFilesD) {
			File f = new File(baseDirectory, myFile);
			flag = d.load(f, ";", processedDirectory);
		}// for
		
		if (flag)
			return "proceso Exitoso";
		else
			return "fallo proceso";
	}

}//ProcessDriver
