package com.smj.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJVehicle;
import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: WSimplifiedSales, 2013/05/14
 *          <ul TYPE ="circle">
 *          <li>Clase proceso para monitorear los KM del vehiculo.
 *          <li>Class create monitoring mileage vehicle
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class MonitoringKM  extends SvrProcess {

	private Integer stateProgram = Integer.parseInt(MSysConfig.getValue("SMJ-STATEWOPROGRAM",Env.getAD_Client_ID(Env.getCtx())).trim());
	private final static BigDecimal threshold = new BigDecimal(MSysConfig.getValue("SMJ-THRESHOLDKM", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer clientID = 0;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		clientID = Env.getAD_Client_ID(Env.getCtx());
		System.out.println(this.getClass().getName()+".prepare.. client.."+clientID);
	}//prepare

	@Override
	protected String doIt() throws Exception {
		System.out.println(this.getClass().getName()+".doIt ....");
		String msg = "";
		try {
			LinkedList<HashMap<String, Object>> data = queryData();
			Iterator<HashMap<String, Object>> itData = data.iterator();
			while(itData.hasNext()){
				HashMap<String, Object> map = itData.next();
				Integer code = (Integer) map.get("CODE");
				Integer vhCode = (Integer) map.get("VH");
				MSMJVehicle vh = new MSMJVehicle(Env.getCtx(), vhCode, null);
				BigDecimal kmVh = vh.getkms();
				BigDecimal km = new BigDecimal((String)map.get("KM"));
				BigDecimal kmMax = km.add(threshold);
				BigDecimal kmMin = km.subtract(threshold);
				if (kmMin.compareTo(Env.ZERO)<=0){
					kmMin = Env.ZERO;
				}
//				System.out.println(this.getClass().getName()+".doIt kmVh.."+kmVh+"..kmMin.."+kmMin+"..kmMax.."+kmMax);
				if((kmMax.compareTo(kmVh)>=0 && kmMin.compareTo(kmVh)<=0 )){
					System.out.println(this.getClass().getName()+".doIt en rango ..."+vhCode);
					MRequest req = new MRequest(Env.getCtx(), code, null);
					req.setDateNextAction(Env.getContextAsDate(Env.getCtx(), "#Date"));
					req.setDueType("3");
					req.save();
					msg = msg +req.getDocumentNo()+"; ";
				}//if
			}//while 
			
		} catch (Exception e) {
			log.log(Level.SEVERE,this.getClass().getName() + ".doIt - ERROR: " + e.getMessage(), e);
			msg = e.toString();
		}
		if (msg.length() > 0){
			msg = Msg.translate(Env.getCtx(), "SMJMSGProMonitoringOK")+msg;
		}else{
			msg = Msg.translate(Env.getCtx(), "SMJMSGProMonitoringEmpty");
		}
		return msg;
	}//doIt

	/**
	 * regresa la informacion de solicitudes programadas de Km - 
	 * returns request mileage programming 
	 * @return
	 */
	private LinkedList<HashMap<String, Object>> queryData(){
		StringBuffer sql = new StringBuffer();
		
		LinkedList<HashMap<String, Object>> codes  = new LinkedList<HashMap<String, Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			sql.append(" SELECT r.R_Request_ID, r.smj_vehicle_ID, r.kmactual FROM R_Request r ");
			sql.append(" WHERE r.isActive = 'Y' AND r.R_Status_ID = "+stateProgram);
			sql.append(" AND periodicity = 'K' AND r.AD_Client_ID = "+clientID);
			sql.append(" AND (r.DateNextAction IS NULL OR r.DateNextAction >  Now() ) ");
			
//			System.out.println(this.getClass().getName()+".queryData SQL::"+sql);
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("CODE", rs.getInt("R_Request_ID"));
				map.put("VH", rs.getInt("smj_vehicle_ID"));
				map.put("KM", rs.getString("kmactual"));
//				System.out.println(this.getClass().getCanonicalName()+".queryData data: "+map.toString());
				codes.add(map);
			}//while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueriesTrans.class.getName()+".queryData - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return codes;
	}//queryData
	
}//MonitoringKM
