package com.smj.process;

import java.io.File;

import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.smj.csv.ServiceCsvDAO;

public class ProcessService  extends SvrProcess {
	
	private String basePath = MSysConfig.getValue("SMJ-LOADFILEPATH",Env.getAD_Client_ID(Env.getCtx())).trim(); 
	
	@Override
	protected void prepare() {
		
	}

	
	@Override
	protected String doIt() throws Exception {
		String baseDirectory = basePath+"/files/service";
		String noLoadFiles = basePath+"/noload";
		String processedDirectory = basePath+"/proc";
		System.out.println(this.getClass().getCanonicalName()+".path...."+baseDirectory);
		ServiceCsvDAO d = new ServiceCsvDAO();
		File dirDaily = new File(baseDirectory);
		String myFilesD[] = dirDaily.list();
		//System.out.println("myfiles.."+myFilesD);
		Boolean flag = true;
		if (myFilesD.length<=0)
			return noLoadFiles+" "+baseDirectory;
		// process all the files included in the postpago directory
		for (String myFile : myFilesD) {
			File f = new File(baseDirectory, myFile);
			flag = d.load(f, ";", processedDirectory);
		}// for
		
		if (flag)
			return "proceso Exitoso";
		else
			return "fallo proceso";
	}//doIt
	
}//processService
