package com.smj.process;

import java.io.File;

import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.smj.csv.PriceCsvDAO;

public class ProcessServicePrice  extends SvrProcess {

	private String basePath = MSysConfig.getValue("SMJ-LOADFILEPATH",Env.getAD_Client_ID(Env.getCtx())).trim(); 
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String doIt() throws Exception {
		String baseDirectory = basePath+"/files/price";
		String noLoadFiles = basePath+"/noload";
		String processedDirectory = basePath+"/proc";
//		String baseDirectory = "F:\\smartjsp\\seveter\\load\\files\\price";
//		String noLoadFiles = "F:\\smartjsp\\seveter\\load\\files\\noload";
//		String processedDirectory = "F:\\smartjsp\\seveter\\load\\files\\proc";
		System.out.println(this.getClass().getCanonicalName()+".path...."+baseDirectory);
		PriceCsvDAO d = new PriceCsvDAO();
		File dirDaily = new File(baseDirectory);
		String myFilesD[] = dirDaily.list();
		System.out.println("myfiles.."+myFilesD);
		Boolean flag = true;
		if (myFilesD.length<=0)
			return noLoadFiles+" "+baseDirectory;
		// process all the files included in the postpago directory
		for (String myFile : myFilesD) {
			File f = new File(baseDirectory, myFile);
			flag = d.load(f, ";", processedDirectory);
		}// for
		
		if (flag)
			return "proceso Exitoso";
		else
			return "fallo proceso";
	}//doIt
	
}//ProcessServicePrice
