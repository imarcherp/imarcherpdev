package com.smj.factories;

import org.adempiere.base.IModelValidatorFactory;
import org.compiere.model.ModelValidator;

import com.smj.util.SMJTransModelValidator;

public class TransModelValidatorFactory implements IModelValidatorFactory {

	@Override
	public ModelValidator newModelValidatorInstance(String className) {
		
		if (className.equals("com.smj.util.SMJTransModelValidator")) {
			return new SMJTransModelValidator();
		}
		
		return null;
	}

}
