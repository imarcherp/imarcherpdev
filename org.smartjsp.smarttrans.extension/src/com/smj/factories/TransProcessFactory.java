package com.smj.factories;

import java.util.logging.Level;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;

import com.smj.trans.erp.process.InOutCreateInvoice;

public class TransProcessFactory implements IProcessFactory {

	protected transient CLogger log = CLogger.getCLogger(getClass());
	
	@Override
	public ProcessCall newProcessInstance(String className) {
		
		if (className.startsWith("com.smj.process")) {
			Class<?> clazz = null;
			ClassLoader loader = getClass().getClassLoader();
			SvrProcess process = null;
						
			try {
				clazz = loader.loadClass(className);
			} catch (Exception e) {
			}
			
			if (clazz != null) {
				try {
					process = (SvrProcess) clazz.newInstance();
				} catch (Exception e) {
					log.log(Level.WARNING, "Process Class Initiate Failed in org.smartjsp.smarttrans.extension", e);
				}
			}
			
			return process;
		}
		
		if (className.equals("org.compiere.process.InOutCreateInvoice")) {
			return new InOutCreateInvoice();
		}
		
		return null;
	}

}
