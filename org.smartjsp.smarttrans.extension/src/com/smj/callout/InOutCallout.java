package com.smj.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrderLine;
import org.compiere.util.Env;

import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: FuelCallOut, 2013/03/19
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para recepcion de producto
 *          <li>Callouts for product receipt
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class InOutCallout  extends CalloutEngine { 

	/**
	 * valida que la orden de 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateQtyEntered(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		System.out.println("inio...... validateQtyEntered.....");
		log.info("");
		try {
			Integer productId = (Integer) mTab.getValue(MInOutLine.COLUMNNAME_M_Product_ID);
			Integer orderLineId = (Integer) mTab.getValue(MInOutLine.COLUMNNAME_C_OrderLine_ID);
			Integer ioLineId = (Integer) mTab.getValue(MInOutLine.COLUMNNAME_M_InOutLine_ID);
			BigDecimal qty =  (BigDecimal) mTab.getValue(MInOutLine.COLUMNNAME_QtyEntered);
			System.out.println("prod:"+productId+"..oline.."+orderLineId+"..qty.."+qty);
			Integer inOutId = (Integer) mTab.getValue(MInOutLine.COLUMNNAME_M_InOut_ID);
			BigDecimal totalInOuts = DataQueriesTrans.getQtyTotalInOuts(null, productId, inOutId, ioLineId);
			BigDecimal total = totalInOuts.add(qty);
			System.out.println("totIO.."+totalInOuts+"..TOT.."+total);
			if (orderLineId == null || orderLineId <= 0){
				return "";
			}
			MOrderLine oLine = new MOrderLine(Env.getCtx(), orderLineId, null);
			BigDecimal qtyOrdered = oLine.getQtyEntered();
			System.out.println("ordered...."+qtyOrdered);
			if (total.compareTo(qtyOrdered) > 0){
				mTab.setValue(MInOutLine.COLUMNNAME_QtyEntered,Env.ZERO);
				//TODO mensaje....
//				return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
				return "X1. La cantidad Ingresada Excede la cantidad Ordenada: "+qtyOrdered;
			}
//			Integer orderId = DataQueriesTrans.getOrderId(orderLineId);
//			if (orderId == null || orderId <= 0){
//				return "";
//			}
//			MOrder order = new MOrder(Env.getCtx(), orderId, null);
//			Integer warehouseId = order.getM_Warehouse_ID();
//			MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
//			Boolean isFuel = warehouse.get_ValueAsBoolean("smj_isfueltank");
//			System.out.println("wareh.."+warehouseId+"..istank...."+isFuel);
//			if (isFuel){
//				BigDecimal capacity = new BigDecimal(warehouse.get_ValueAsString("smj_capacitytank")); 
//				BigDecimal tankTotal = DataQueriesTrans.getQtyWarehouse(productId, warehouseId);
//				total = tankTotal.add(qty);
//				System.out.println("cap..."+capacity+"...tankTot.."+tankTotal+"...tot.."+total);
//				if (total.compareTo(capacity) > 0){
//					mTab.setValue(MInOutLine.COLUMNNAME_QtyEntered,Env.ZERO);
////					return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
//					//TODO mensaje....
//					return "La cantidad Ingresada Excede la Capacidad del Tanque, total actualmente en el tanque: "+tankTotal;
//				}
//				
//			}//if isFuel

		} catch (Exception e) {
			log.severe("ERROR: InOutCallout.validateQtyEntered :: "+e);
		}//try / Catch

		return "";
	}//validateQtyEntered
	
}//InOutCallout
