package com.smj.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJFuelLine;

/**
 * @version <li>SmartJSP: FuelCallOut, 2013/01/22
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para combustible
 *          <li>Callouts for fuel
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class FuelCallOut extends CalloutEngine { 

	/**
	 * valida que la decima actual no supere la centena final - 
	 * validate Hundred Actual doesn't exceed limit Hundred
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return String 
	 */
	public String validateHundredActual(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		log.info("");
		BigDecimal actualHundred = (BigDecimal) mTab.getValue(MSMJFuelLine.COLUMNNAME_actualhundred);
		BigDecimal limitHundred = (BigDecimal) mTab.getValue(MSMJFuelLine.COLUMNNAME_limithundred);
		
		if (actualHundred.compareTo(limitHundred)>0){
			mTab.setValue(MSMJFuelLine.COLUMNNAME_actualhundred, Env.ZERO);
			return Msg.getMsg(Env.getCtx(), "SMJMSGHendredActualBigger");
		}
		
		return "";
	}//validateHundredActual
	
}//FuelCallOut
