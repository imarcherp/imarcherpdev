package com.smj.callout;

import java.math.BigDecimal;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;

import com.smj.util.DataQueriesTrans;
import com.smj.util.ServiceOrderValidations;
import com.smj.util.UtilTrans;

/**
 * @version <li>SmartJSP: ServiceOrderCallout, 2013/08/08
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para ordenes de servicio
 *          <li>Callouts for service order
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ServiceOrderCallout  extends CalloutEngine { 
	
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	private Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
	
	//Adicion iMarch / Organizacion Transportadora
	private String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
	
	
	/**
	 * valida el estado de la solicitud - 
	 * validate request state
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateStatus(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Integer status = (Integer) mTab.getValue(MRequest.COLUMNNAME_R_Status_ID);
			msg = ServiceOrderValidations.validateServiceOrder(logRol, status);
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + " - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateStatus
	
	/**
	 * replica el numero interno del vehiculo 
	 * validate request state
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateVehicleId(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		String msg = "";
		try {


			if (mTab.getValue("smj_vehicle_ID") != null)
			{
			Integer X_ItCode = (Integer)mTab.getValue("smj_vehicle_ID");   
			String sql= "select smj_internalnumber from smj_vehicle where smj_vehicle_id = ?";

			String X_ItDesc = org.compiere.util.DB.getSQLValueString( null, sql, X_ItCode);
			if (X_ItDesc != null)
			{mTab.setValue("smj_internalnumber",X_ItDesc);}
			else
			{mTab.setValue("ssmj_internalnumber","");}
			}
			msg = "";
			
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + " - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateStatus
	
	/**
	 * valida si el rol puede aprobar un adelanto - 
	 * validate if role can approve advance
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	/*
	public String validateControlAdvance(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue ){
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Boolean aprovated = mTab.getValueAsBoolean(MSMJControlAdvances.COLUMNNAME_IsApproved);
			msg = ServiceOrderValidations.validateControlAdvances(logRol, aprovated);
			if (msg != null && msg.length() > 0){
				mTab.setValue(MSMJControlAdvances.COLUMNNAME_IsApproved, false);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateControlAdvance - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateControlAdvance
	*/
	/**
	 * valida si el rol puede aprobar un cargo - 
	 * validate if role can approve charges 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	/*
	public String validateCharges(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Boolean aprovated = mTab.getValueAsBoolean(MSMJCharges.COLUMNNAME_IsApproved);
			msg = ServiceOrderValidations.validateControlAdvances(logRol, aprovated);
			if (msg != null && msg.length() > 0){
				mTab.setValue(MSMJCharges.COLUMNNAME_IsApproved, false);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateCharges - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateCharges
	*/
	/**
	 * llena el campo de telefono a partir del propietario - 
	 * fill phone owner field by owner
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String findDataOwner(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//com.smj.callout.ServiceOrderCallout.findDataOwner
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer id =  (Integer) mTab.getValue("c_bpartnerowner_ID");
			if(id == null){
				mTab.setValue("phone", "");
				return "";
			}
			String phone = DataQueriesTrans.getPhoneByBPartnerId(null, id);
//			System.out.println(this.getClass().getName()+".findDataOwner id.."+id+"..phone.."+phone);
			mTab.setValue("smj_phone", phone);
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".findDataOwner - ERROR: " + e.getMessage(),
					e);
		}
		return "";
	}//findDataOwner
	
	/**
	 * llena el campo de telefono a partir del conductor - 
	 * fill phone driver field by driver
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String findDataDriver(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//com.smj.callout.ServiceOrderCallout.findDataDriver
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer id =  (Integer) mTab.getValue("c_bpartnerdriver_ID");
			if(id == null){
				mTab.setValue("smj_phonedriver", "");
				return "";
			}
			String phone = DataQueriesTrans.getPhoneByBPartnerId(null, id);
//			System.out.println(this.getClass().getName()+".findDataDriver id.."+id+"..phone.."+phone);
			mTab.setValue("smj_phonedriver", phone);
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".findDataOwner - ERROR: " + e.getMessage(),
					e);
		}
		return "";
	}//findDataOwner
	
	
	
	
	
	/**
	 * calcaula el porcentaje de comision - 
	 * calculate brokerage percent
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String calculateBrokeragePercent(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//com.smj.callout.ServiceOrderCallout.calculateBrokeragePercent
		try {
			BigDecimal totCharges;
			BigDecimal percent ;
			BigDecimal invoiceValue;
			BigDecimal chargeValue;
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			String requestId =  Env.getCtx().getProperty("1|R_Request_ID");
			    invoiceValue =  (BigDecimal) mTab.getValue("smj_invoicevalue");
				if (invoiceValue == null || invoiceValue.compareTo(Env.ZERO)<=0){
					return "";
				}
				chargeValue = (BigDecimal) mTab.getValue("smj_vehiclecharge");
				totCharges = DataQueriesTrans.getTotalChargesByRequest(requestId,mTab.getTrxInfo());
				BigDecimal completeTotCharges = chargeValue;
				completeTotCharges = completeTotCharges.add(totCharges);
				
				if (completeTotCharges == null || completeTotCharges.compareTo(Env.ZERO)<=0){
					return "";
				}
				percent = UtilTrans.calculateBrokeragePercent(invoiceValue, completeTotCharges);
				mTab.setValue("smj_brokeragepercent", percent);
			    log.log(Level.SEVERE, "Request ID ="+ Env.getCtx().getProperty("1|R_Request_ID"));

				// compara si el total de cargos + flete es < que el facturado
				if  (completeTotCharges.compareTo(invoiceValue) == 1)  // fix solicitud 1000368
						{
					return Msg.translate(Env.getCtx(), "SMJMSGChargesExcedInvValue");
						}
			
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".calculateBrokeragePercent - ERROR: " + e.getMessage(),
					e);
		}
		return "";
	}//calculateBrokeragePercent

	
	/**
	 * Valida que el total de los cargos + flete sea < valor a facturar para infotrans
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @retur	n
	 */
	public String validateTotalCharges(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			BigDecimal charge;
			try {
				charge = (BigDecimal) value;   // solo procesa estas validaciones si el valor del cargo es mayor a cero				
			}
			catch (ClassCastException ce)
			{
				charge = new BigDecimal (0);
			}
			if (charge == null || charge.compareTo(Env.ZERO)<=0){
				return "";
			}
			
			mTab.dataSave(true);
			String requestId =  Env.getCtx().getProperty("1|R_Request_ID");
			MRequest tRequest = new MRequest(ctx,Integer.parseInt(requestId),mTab.getTrxInfo());
			
			BigDecimal totCharges = DataQueriesTrans.getTotalChargesByRequest(requestId,mTab.getTrxInfo());
			
			BigDecimal invoiceValue = (BigDecimal)tRequest.get_Value("smj_invoicevalue");
			BigDecimal chargeValue = (BigDecimal) tRequest.get_Value("smj_vehiclecharge");
			BigDecimal completeTotCharges = chargeValue;
			completeTotCharges = completeTotCharges.add(totCharges);
			if (invoiceValue == null || invoiceValue.compareTo(Env.ZERO)<=0){
				return "";
			}
			

			if (completeTotCharges == null || completeTotCharges.compareTo(Env.ZERO)<=0){
				return "";
			}

			// actualiza % de interm ediacion luego de las validaciones ..
			
			BigDecimal percent = UtilTrans.calculateBrokeragePercent(invoiceValue, completeTotCharges);
			mTab.getParentTab().setValue("smj_brokeragepercent", percent );
			// compara si el total de cargos + flete es < que el facturado
			mTab.getParentTab().dataSave(true);
			mTab.getParentTab().dataRefresh();
			if  (completeTotCharges.compareTo(invoiceValue) == 1)  // fix solicitud 1000368
			{
		return Msg.translate(Env.getCtx(), "SMJMSGChargesExcedInvValue");
			}
			
			
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateTotalCharges - ERROR: " + e.getMessage(),
					e);
		}
		return "";
	}//validateTotalCharges

	
	/**
	 * valida si el rol puede aprobar valor venta - 
	 * validate if role can approve invoiced value 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateInvoiceApproved(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue  ){
		//com.smj.callout.ServiceOrderCallout.validateInvoiceApproved
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Boolean aprobateOld = false ;
			Boolean aprobateNew = false;

			try {
				aprobateOld =   (Boolean) oldValue;
				aprobateNew =   (Boolean) value;
			}
			catch (ClassCastException ce)
			{
				//log.log(Level.SEVERE,this.getClass().getName() + ".Casting error: " + ce.getMessage(),ce);
			}

			// solicitud 1000399 - validaciones de   si esta generada factura/pago no se puede desaprobar
			if (mTab.getValue("C_Invoice_ID") != null  )
			{
			int invoiceId = (Integer) mTab.getValue("C_Invoice_ID");
			   if (invoiceId > 0 )   // tiene datos, si esta aprobada no se puede desaprobar
			     {
				  if (aprobateOld && !aprobateNew)  {
					    msg =  Msg.translate(Env.getCtx(), "SMJMSGInvoiceChargeAssigned");
					    //msg = "Factura ya esta asignada, no puede desaprobar";
					    String campo = mField.getColumnName();
					    mTab.setValue(campo, aprobateOld);
						return msg;  
				  }
   	    	  }
		   }   
			msg = ServiceOrderValidations.validateApproveAdvancesCharges(logRol, aprobateOld, aprobateNew);
			if (msg != null && msg.length() > 0){
				mTab.setValue("smj_isinvoiceapproved", aprobateOld);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateInvoiceApproved - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateInvoiceApproved
	
	
		
	/**
	 * valida si se puede desaprobar el cargo de la factura 
	 * validate if role can dis-approve the invoice charge 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateInvoiceChargeApproved(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue  ){
		//com.smj.callout.ServiceOrderCallout.validateInvoiceApproved
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());

			Boolean aprobateOld = false ;
			Boolean aprobateNew = false;

			try {
				aprobateOld =   (Boolean) oldValue;
				aprobateNew =   (Boolean) value;
			}
			catch (ClassCastException ce)
			{
				//log.log(Level.SEVERE,this.getClass().getName() + ".Casting error: " + ce.getMessage(),ce);
			}

			// solicitud 1000399 - validaciones de   si esta generada factura/pago no se puede desaprobar
			if (mTab.getValue("smj_invoiceprovider_ID") != null  )
			{
			int invoiceId = (Integer) mTab.getValue("smj_invoiceprovider_ID");
			   if (invoiceId > 0 )   // tiene datos, si esta aprobada no se puede desaprobar
			     {
				  if (aprobateOld && !aprobateNew)  {
					    msg =  Msg.translate(Env.getCtx(), "SMJMSGInvoiceChargeAssigned");
					    String campo = mField.getColumnName();
					    mTab.setValue(campo, aprobateOld);
						return msg;  
				  }
   	    	  }
		   }   
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateInvoiceChargeApproved - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateInvoiceChargeApproved
	
	/**
	 * valida si el rol puede aprobar valor cargo vehiculo - 
	 * validate if role can approve charge vehicle
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateChargeApproved(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue){
		//com.smj.callout.ServiceOrderCallout.validateChargeApproved
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Boolean aprobateOld = false ;
			Boolean aprobateNew = false;

			try {
				aprobateOld =   (Boolean) oldValue;
				aprobateNew =   (Boolean) value;
			}
			catch (ClassCastException ce)
			{
				//log.log(Level.SEVERE,this.getClass().getName() + ".Casting error: " + ce.getMessage(),ce);
			}

			// mTab.getValueAsBoolean("smj_isinvoiceapproved");
			
			
			// solicitud 1000399 - validaciones de   si esta generada factura/pago no se puede desaprobar
			if (mTab.getValue("C_Invoice_ID") != null  )
			{
			int paymentId = (Integer) mTab.getValue("C_Invoice_ID");
			   if (paymentId > 0 )   // tiene datos, si esta aprobada no se puede desaprobar
			     {
				  if (aprobateOld && !aprobateNew)  {
					    msg =  Msg.translate(Env.getCtx(), "SMJMSGInvoiceChargeAssigned");
					    //msg = "Factura ya esta asignada, no puede desaprobar";
					    String campo = mField.getColumnName();
					    mTab.setValue(campo, aprobateOld);
						return msg;  
				  }
   	    	  }
			}
			
			msg = ServiceOrderValidations.validateApproveAdvancesCharges(logRol, aprobateOld, aprobateNew);

//			msg = ServiceOrderValidations.validateControlAdvances(logRol, aprobateNew);
			if (msg != null && msg.length() > 0){
					if (msg != null && msg.length() > 0){
						
						String campo = mField.getColumnName();
						
						mTab.setValue(campo, aprobateOld);
					}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateChargeApproved - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateChargeApproved
	
	/**
	 * valida si el rol puede aprobar valor de avances al vehiculo - 
	 * validate if role can approve vehicle advances
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateAdvancesApproved(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue){

		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			Integer logRol = Env.getAD_Role_ID(Env.getCtx());
			Boolean aprobateOld = false ;
			Boolean aprobateNew = false;

			try {
				aprobateOld =   (Boolean) oldValue;
				aprobateNew =   (Boolean) value;
			}
			catch (ClassCastException ce)
			{
				//log.log(Level.SEVERE,this.getClass().getName() + ".Casting error: " + ce.getMessage(),ce);
			}

			// solicitud 1000399 - validaciones de   si esta generada factura/pago no se puede desaprobar
			if (mTab.getValue("C_Payment_ID") != null  )
			{
			int paymentId = (Integer) mTab.getValue("C_Payment_ID");
			   if (paymentId > 0 )   // tiene datos, si esta aprobada no se puede desaprobar
			     {
				  if (aprobateOld && !aprobateNew)  {
					    msg =  Msg.translate(Env.getCtx(), "SMJMSGInvoiceChargeAssigned");
					    //msg = "Factura ya esta asignada, no puede desaprobar";
					    String campo = mField.getColumnName();
					    mTab.setValue(campo, aprobateOld);
						return msg;  
				  }
   	    	  }
			}
			
			msg = ServiceOrderValidations.validateApproveAdvancesCharges(logRol, aprobateOld, aprobateNew);

//			msg = ServiceOrderValidations.validateControlAdvances(logRol, aprobateNew);
			if (msg != null && msg.length() > 0){
					if (msg != null && msg.length() > 0){
						
						String campo = mField.getColumnName();
						
						mTab.setValue(campo, aprobateOld);
					}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validateChargeApproved - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validateApproved

	
	
	
	
	/**
	 * valida que la placa digitada es un vehiculo registrado - 
	 * validate enter plate is a vehicle registered vehicle
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return String
	 */
	public String validatePlate(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//com.smj.callout.ServiceOrderCallout.validatePlate
		String msg = "";
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
			if(orgfreighttrans.equals("true"))	//Adicion iMarch / Organizacion Transportadora----
				orgInfotrans=orgId;				//---
			if (!orgId.equals(orgInfotrans)){
				return "";
			}
			String plate = mTab.get_ValueAsString("smj_plate");
			Integer vehicleID = DataQueriesTrans.getVehicleId(plate.toUpperCase());
			mTab.setValue("smj_plate", plate.toUpperCase());
			if (vehicleID == null || vehicleID <= 0){
				mTab.setValue("smj_plate", null);
				msg = Msg.translate(Env.getCtx(), "SMJMSGOnlyVehicleRegistred");
			}
			
			mTab.setValue("smj_phone", "");
			mTab.setValue("smj_phonedriver", "");
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".validatePlate - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//validatePlate
	
	
	/**
	 * valida si hay cargos o anticipos activos para esta solicitud 
	 * validate if there is charges or advances for this request
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String hasChargesAdvancesApproved(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,Object oldValue  ){
		String msg = "";
		try {
			
			Trx trx = Trx.get(Trx.createTrxName("AL"), true);
			Integer oldV = 0 ;
			
			//Integer requestId =  Integer.parseInt(Env.getCtx().getProperty("1|R_Request_ID", "-1"));
			Integer requestId =  mTab.getRecord_ID();
			
			if (requestId < 1) {
				return msg;
			}
			
			Boolean aprovated = mTab.getValueAsBoolean("smj_isinvoicecharge");

			boolean hasChargesOrAdvances = DataQueriesTrans.getAdvancesChargesByRequest(trx.getTrxName(), requestId);
			
			try {
				oldV =   (Integer) oldValue;
			}
			catch (ClassCastException ce)
			{
				//log.log(Level.SEVERE,this.getClass().getName() + ".Casting error: " + ce.getMessage(),ce);
			}

			if (aprovated || hasChargesOrAdvances)
		       	{	
					    msg =  Msg.translate(Env.getCtx(), "SMJMSGHasChargesAdvancedAssigned");
					    String campo = mField.getColumnName();
					    mTab.setValue(campo, oldV);
					    
						return msg;  
				  }
		} catch (Exception e) {
			log.log(Level.SEVERE,
					this.getClass().getName() + ".hasChargesAdvancesApproved - ERROR: " + e.getMessage(),
					e);
		}
		return msg;
	}//hasChargesAdvancesApproved
	
	
	
	
	
}//ServiceOrderCallout
