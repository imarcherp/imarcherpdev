package com.smj.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.apps.form.Allocation;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: ProductCallout, 2013/02/16
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para producto
 *          <li>Callouts for product
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class ProductCallout extends CalloutEngine { 
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	public String getQtyPartner(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		Integer orderId = (Integer) mTab.getValue("C_Order_ID");
		Integer productId = (Integer) mTab.getValue("M_Product_ID"); 
		if (productId == null)
			return "";
		if(orderId == null || orderId <= 0){
			return "";
		}
		MOrder order = new MOrder(Env.getCtx(), orderId, null);
		Boolean isSales = order.get_ValueAsBoolean(MOrder.COLUMNNAME_IsSOTrx);
		if (isSales){
			return "";
		}
		Integer bparnerId = order.getC_BPartner_ID();
		Integer warehouseId = order.getM_Warehouse_ID();
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
		Integer warehouseOwnerId = 0;
		Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
		if (isConsigment){
			warehouseOwnerId  = warehouse.get_ValueAsInt("C_BPartner_ID");
		}
		Integer locatorOrderId = DataQueriesTrans.getLocatorId(order.getM_Warehouse_ID());
		BigDecimal total = DataQueriesTrans.getTotalProductByPartner(productId, bparnerId, null);
		MProduct pr = MProduct.get(Env.getCtx(), productId);
		Integer principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
		String listName = "";

		if (pr.getM_Product_Category_ID()==tiresCategory){
			listName = "Llantas";
		}else
		if (locatorOrderId.equals(principalWarehouse)){
			listName = "Mostrador";
		}else 
			if(bparnerId.equals(warehouseOwnerId)){
			listName = DataQueriesTrans.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bparnerId);
		}
		else{
			listName = "Mostrador";
		}
		BigDecimal price = DataQueriesTrans.getPriceProductPartnerName(productId, listName.trim());
		if (total == null)
			total = Env.ZERO;
		if (price != null && price.compareTo(Env.ZERO)>0){
			mTab.setValue(MOrderLine.COLUMNNAME_PriceEntered, price);
			mTab.setValue(MOrderLine.COLUMNNAME_PriceActual, price);
			mTab.setValue(MOrderLine.COLUMNNAME_PriceList, price);
		}
		mTab.setValue("smj_total", total);
		
		return "";
	}//getQtyParter
	
}//ProductCallout
