package com.smj.callout;

import java.util.Properties;

import org.compiere.apps.form.Allocation;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MOrder;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @version <li>SmartJSP: OrderCallout, 2013/04/09
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para ordenes 
 *          <li>Callouts for order
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class OrderCallout extends CalloutEngine { 
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	/**
	 * valida que la orden de almacen de consignacion solo se ponga el proveedor propietario -
	 * validate than purchase order
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String validateWarehouseOwner(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		Integer warehouseId = (Integer) mTab.getValue("M_Warehouse_ID");
		Integer partnerId = (Integer) mTab.getValue("C_BPartner_ID"); 
		Boolean isSales = (Boolean) mTab.getValue(MOrder.COLUMNNAME_IsSOTrx);
		if (warehouseId == null || partnerId == null){
			return "";
		}
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), warehouseId);
		Integer warehouseOwnerId = 0;
		Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
		if (isConsigment && !isSales && warehouseId>0 && partnerId >0){
			warehouseOwnerId  = warehouse.get_ValueAsInt("C_BPartner_ID");
			if (!partnerId.equals(warehouseOwnerId)  ){
				mTab.setValue("C_BPartner_ID", warehouseOwnerId);
				return Msg.getMsg(Env.getCtx(), "SMJMSGPartnerNOWarehouse");
			}
		}
		return "";
	}//validateWarehouseOwner

}//OrderCallout
