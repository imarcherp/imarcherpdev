package com.smj.callout;

import java.util.Properties;

import org.compiere.apps.form.Allocation;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.util.DataQueriesTrans;

/**
 * @version <li>SmartJSP: BPartnerCallout, 2013/01/21
 *          <ul TYPE ="circle">
 *          <li>Callouts varios para terceros
 *          <li>Callouts for BPartner
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class BPartnerCallout extends CalloutEngine { 
	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);

	/**
	 * llena nombre orden apellidos nombres - Fill name field, order Lastnames Firstnames
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return String
	 */
	public String fillName(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		log.info("");

		if (mTab.getValue("FirstName1") == null
				&& mTab.getValue("FirstName2") == null
				&& mTab.getValue("LastName1") == null
				&& mTab.getValue("LastName2") == null)
			mTab.setValue(MBPartner.COLUMNNAME_Name, null);
			
		String filledName = new String("");

		if (mTab.getValue("LastName1") != null)
			filledName = filledName
					+ ((String) mTab.getValue("LastName1")).trim();

		if (mTab.getValue("LastName2") != null)
			filledName = filledName + " "
					+ ((String) mTab.getValue("LastName2")).trim();

		if (mTab.getValue("FirstName1") != null)
			filledName = filledName + " "
					+ ((String) mTab.getValue("FirstName1")).trim();

		if (mTab.getValue("FirstName2") != null)
			filledName = filledName + " "
					+ ((String) mTab.getValue("FirstName2")).trim();

		if (filledName != null)
			filledName = filledName + " ";

		if (filledName.length() > 60) {
			// log.warning("Length > 60 - truncated");
			filledName = filledName.substring(0, 60);
		}
		mTab.setValue(MBPartner.COLUMNNAME_Name, filledName);
		return "";
	}//fillName
	
	/***
	 * valida que no exista un tercero con ese numero de identificacion para el cliente -
	 * validate no exist a partner with this taxId for this client
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return String
	 */
	public String validateTaxId(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		log.info("");
		Integer clientId = Env.getAD_Client_ID(Env.getCtx());
		String taxId = "";
		try{
			taxId =  (String) mTab.getValue(MBPartner.COLUMNNAME_TaxID);
			if (taxId == null)
				return null;
		}catch (Exception e) {
			return null;
		}
		
		Integer partnerId = DataQueriesTrans.getPartnerByTaxId(clientId, taxId);
		if (partnerId > 0){
			mTab.setValue(MBPartner.COLUMNNAME_TaxID, "");
			return Msg.getMsg(Env.getCtx(), "smjMsgTaxIdExist");
		}else{
			mTab.setValue(MBPartner.COLUMNNAME_TaxID, taxId.trim());
		}
		
		return "";
	}//validateTaxId
	
}// BPartnerCallout
