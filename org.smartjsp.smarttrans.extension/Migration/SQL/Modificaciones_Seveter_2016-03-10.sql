ALTER TABLE SMJ_TmpWebSales ALTER COLUMN SMJ_WorkOrder TYPE text USING smj_workorder::text;
ALTER TABLE SMJ_WorkOrderLine ALTER COLUMN value TYPE character varying(30);
UPDATE AD_Client SET ModelValidationClasses = '' WHERE AD_Client_ID = 1000000;
ALTER TABLE C_Order ALTER COLUMN SMJ_WorkOrder TYPE text;
ALTER TABLE C_Order ALTER COLUMN smj_tmpwebsales_id TYPE numeric(10, 0) USING smj_tmpwebsales_id::numeric;
ALTER TABLE C_Order ALTER COLUMN smj_tmpwebsales_id DROP NOT NULL;
ALTER TABLE C_Order ALTER COLUMN smj_tmpwebsales_id SET DEFAULT NULL;

--Se requiere eliminar, para que no presente problemas a la hora de modificar el campo
DROP VIEW v_imarch_portfolio;
DROP VIEW imarch_portfolio_v;
DROP VIEW c_invoice_header_vt;
DROP VIEW c_invoice_header_v;
ALTER TABLE C_Invoice ALTER COLUMN SMJ_WorkOrder TYPE text;
-- Ejecutar el archivo Modificaciones_Seveter_2016-06-15.sql despues de este.
-- Para crear nuevamente las vistas eliminadas en este paso.

UPDATE ad_system
   SET version = '2015-10-31'
 WHERE ad_system_id = 0;
