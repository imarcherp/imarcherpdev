-- Actualizar cuentas bancarias 

UPDATE C_BankAccount SET Value = '1234', Name = '1234' WHERE C_BankAccount_ID = 100;
UPDATE C_BankAccount SET Value = '5678', Name = '5678' WHERE C_BankAccount_ID = 101;
UPDATE C_BankAccount SET Value = '104', Name = 'Caja1_iMarch' WHERE C_BankAccount_ID = 1000025;
UPDATE C_BankAccount SET Value = '1002', Name = 'Bancolombia_Seveter' WHERE C_BankAccount_ID = 1000012;
UPDATE C_BankAccount SET Value = '1003', Name = 'Bancolombia_Infotrans' WHERE C_BankAccount_ID = 1000010;
UPDATE C_BankAccount SET Value = '1005', Name = 'Bancolombia_MInmobiliaria' WHERE C_BankAccount_ID = 1000014;
UPDATE C_BankAccount SET Value = '1000003', Name = 'CAJA RECAUDO' WHERE C_BankAccount_ID = 1000008;
UPDATE C_BankAccount SET Value = '1000001', Name = 'CAJA TESORERIA' WHERE C_BankAccount_ID = 1000006;
UPDATE C_BankAccount SET Value = '2', Name = '2' WHERE C_BankAccount_ID = 1000003;
UPDATE C_BankAccount SET Value = '202', Name = 'Caja2ConsumosInternas' WHERE C_BankAccount_ID = 1000005;
UPDATE C_BankAccount SET Value = '201', Name = 'Caja2Ventas' WHERE C_BankAccount_ID = 1000015;
UPDATE C_BankAccount SET Value = '1000005', Name = 'Caja3_FlotaPropia' WHERE C_BankAccount_ID = 1000017;
UPDATE C_BankAccount SET Value = '2003', Name = 'Bancodebogota_CTE_Infotrans' WHERE C_BankAccount_ID = 1000011;
UPDATE C_BankAccount SET Value = '2004', Name = 'Bancodebogota_CTE_imarch' WHERE C_BankAccount_ID = 1000024;
UPDATE C_BankAccount SET Value = '2002', Name = 'Bancodebogota_CTE_Seveter' WHERE C_BankAccount_ID = 1000023;
UPDATE C_BankAccount SET Value = '2001', Name = 'Bancodebogota_CTE_Ucolbus' WHERE C_BankAccount_ID = 1000022;
UPDATE C_BankAccount SET Value = '1001', Name = 'Bancolombia_Ucolbus' WHERE C_BankAccount_ID = 1000020;
UPDATE C_BankAccount SET Value = '3002', Name = 'BancoPopular_AH_Seveter' WHERE C_BankAccount_ID = 1000021;
UPDATE C_BankAccount SET Value = '3004', Name = 'BancoPopular_CTE_iMarch' WHERE C_BankAccount_ID = 1000026;
UPDATE C_BankAccount SET Value = '102', Name = 'Caja1_Infotrans' WHERE C_BankAccount_ID = 1000009;
UPDATE C_BankAccount SET Value = '101', Name = 'Caja1_Seveter' WHERE C_BankAccount_ID = 1000000;
UPDATE C_BankAccount SET Value = '103', Name = 'Caja1_Ucolbus' WHERE C_BankAccount_ID = 1000019;
