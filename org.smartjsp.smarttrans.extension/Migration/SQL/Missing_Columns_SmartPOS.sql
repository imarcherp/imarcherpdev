--------------------------------------------------------------------------------------------
--EJECUTAR COMO SQL, CAMPOS FALTANTES DETECTADOS
---------------------------------------------------------------------------------------------

ALTER TABLE smj_tmpwebsales ADD COLUMN smj_ticket character varying(30);
ALTER TABLE smj_tmpwebsales ADD COLUMN c_order_id numeric(10,0);
ALTER TABLE smj_tmpwebsales ADD COLUMN smj_tmpwebsales_uu character varying(36);
ALTER TABLE c_order ADD COLUMN smj_tmpwebsales_id character varying(10) NOT NULL DEFAULT 0;
ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_destination character varying(60);
ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_notes character varying(100);
ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_discount numeric;
ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_percentage character(1);
ALTER TABLE smj_tmpwebsalesline ADD COLUMN c_orderline_id numeric(10,0);
ALTER TABLE c_payment ADD COLUMN isdeposit character(1) NOT NULL DEFAULT 'N'::bpchar;
ALTER TABLE c_payment ADD COLUMN smj_closecash_id numeric(10,0);
ALTER TABLE smj_tmpwebsalesline RENAME qty TO qtyordered;
ALTER TABLE smj_tmpwebsalesline RENAME price TO priceentered;
ALTER TABLE smj_tmpwebsalesline ALTER COLUMN priceentered SET DEFAULT 0;
ALTER TABLE smj_tmpwebsalesline ADD COLUMN m_pricelist_version_id numeric(10,0);
ALTER TABLE smj_tmpwebsalesline ADD COLUMN priceactual numeric DEFAULT 0;
UPDATE SMJ_TmpWebSalesLine SET M_PriceList_Version_ID = 1000011 WHERE M_PriceList_Version_ID IS NULL;
ALTER TABLE m_product ADD COLUMN smj_prod_efficiency numeric(10,2);
ALTER TABLE m_product ADD COLUMN smjdiscount_allowed character(1) NOT NULL DEFAULT 'N'::bpchar;

ALTER TABLE m_warehouse ADD COLUMN ispos character(1) NOT NULL DEFAULT 'N'::bpchar;
ALTER TABLE m_warehouse ADD COLUMN ad_role_id numeric(10,0);
ALTER TABLE m_warehouse ADD COLUMN c_bank_id numeric(10,0);
ALTER TABLE m_warehouse ADD COLUMN c_bankaccount_id numeric(10,0);

UPDATE SMJ_TmpWebSalesLine SET M_PriceList_Version_ID = 1000011 WHERE M_PriceList_Version_ID IS NULL;


-- Script para crear campos que reemplazaran las SysConfig que poseen valor por Organizacion

ALTER TABLE ad_org ADD COLUMN adminroles TEXT NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_bpartner_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_charge_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_chargedeposit_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_chargetips_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_currency_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_paymentterm_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_tax_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN defaultprinter TEXT NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN isdiscountrestricted CHAR(1) NOT NULL DEFAULT 'N';
ALTER TABLE ad_org ADD COLUMN isposclientprinting CHAR(1) NOT NULL DEFAULT 'Y';
ALTER TABLE ad_org ADD COLUMN ispostax CHAR(1) NOT NULL DEFAULT 'N';
ALTER TABLE ad_org ADD COLUMN issendmandatory CHAR(1) NOT NULL DEFAULT 'N';
ALTER TABLE ad_org ADD COLUMN listpayterm TEXT NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN c_bpartner_location_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN m_pricelist_version_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN m_product_category_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN m_warehouse_id NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN pos_checkopenorderstime NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN pos_checkorderstime NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN pos_decimals NUMERIC(10,0) NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN pos_locale TEXT NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN pos_printersbygroup TEXT NULL DEFAULT NULL;
ALTER TABLE ad_org ADD COLUMN sizeimage NUMERIC(10,0) NULL DEFAULT NULL;

COMMENT ON COLUMN ad_org.adminroles IS 'Id of admin roles for privileged operations';
COMMENT ON COLUMN ad_org.c_bpartner_id IS 'Internal id of default  business partner for POS transactions';
COMMENT ON COLUMN ad_org.c_charge_id IS 'charge id for insurance/club discounts';
COMMENT ON COLUMN ad_org.c_chargedeposit_id IS 'Charge ID, Transfer between Bank Accounts';
COMMENT ON COLUMN ad_org.c_chargetips_id IS 'default charge id for tips posAdmin';
COMMENT ON COLUMN ad_org.c_currency_id IS 'Default currency ID for SmartPOS';
COMMENT ON COLUMN ad_org.c_paymentterm_id IS 'default payment term';
COMMENT ON COLUMN ad_org.c_tax_id IS 'Default tax rate id for POS discounts (standard)';
COMMENT ON COLUMN ad_org.defaultprinter IS 'Default Printer (Direct Print From SmartPOS)';
COMMENT ON COLUMN ad_org.isdiscountrestricted IS 'If enabled (Y) indicated that just admin roles can perform discounts on SmartPOS';
COMMENT ON COLUMN ad_org.isposclientprinting IS 'If is true open the invoice window else print direct to the default printer (Server)';
COMMENT ON COLUMN ad_org.ispostax IS 'Check if always applies the POS tax.';
COMMENT ON COLUMN ad_org.issendmandatory IS 'Define if it is required to sent the items before to pay (values: Y/N)';
COMMENT ON COLUMN ad_org.listpayterm IS 'List fo payment terms such as: Cash, Credit (delimited by ,) ';
COMMENT ON COLUMN ad_org.c_bpartner_location_id IS 'Default location id for SmartPOS transactions';
COMMENT ON COLUMN ad_org.m_pricelist_version_id IS 'Purchase list id';
COMMENT ON COLUMN ad_org.m_product_category_id IS 'Product category id for combos (POS)';
COMMENT ON COLUMN ad_org.m_warehouse_id IS 'Main Warehouse ID';
COMMENT ON COLUMN ad_org.pos_checkopenorderstime IS 'Value to determine the time the information is refreshed in the window (Open Tickets) ';
COMMENT ON COLUMN ad_org.pos_checkorderstime IS 'Timeout in millisecond to close the tickets window';
COMMENT ON COLUMN ad_org.pos_decimals IS 'Number of decimal to visualice with SmartPOS screens';
COMMENT ON COLUMN ad_org.pos_locale IS 'Locale descripcion for language settings (ft - french language   ,    CI   Ivory coast)';
COMMENT ON COLUMN ad_org.pos_printersbygroup IS 'Printer config for POS';
COMMENT ON COLUMN ad_org.sizeimage IS 'Size Image in pixels for product preview';

-- Codigo para migrar de la tabla AD_SysConfig a la tabla AD_Org
UPDATE AD_Org SET AdminRoles = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_ADMINROLES';
UPDATE AD_Org SET C_BPartner_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_DEFAULTCUSTOMER';
UPDATE AD_Org SET C_Charge_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_CHARGEMEMBERDISCOUNTS';
UPDATE AD_Org SET C_ChargeDeposit_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_CHARGE_DEPOSITS_POS';
UPDATE AD_Org SET C_ChargeTips_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_CHARGETIPS';
UPDATE AD_Org SET C_Currency_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-CURRENCYID';
UPDATE AD_Org SET C_PaymentTerm_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-PAYMENTTERMDEFAULT';
UPDATE AD_Org SET C_Tax_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-TAXDISCOUNT';
UPDATE AD_Org SET DefaultPrinter = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_DefaultPrinter';
UPDATE AD_Org SET IsDiscountRestricted = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_DISCOUNTSRESTRICTED';
UPDATE AD_Org SET isPosClientPrinting = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_POSCLIENTPRINTING';
UPDATE AD_Org SET IsPosTax = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_ISPOSTAX';
UPDATE AD_Org SET IsSendMandatory = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-SendIsMandatory';
UPDATE AD_Org SET ListPayTerm = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-ListPayTerm';
UPDATE AD_SysConfig SET Value = '0' WHERE Name = 'SMJ-PARTNERLOCATION' AND Value = 'NOREQUIRED';
UPDATE AD_Org SET C_BPartner_Location_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-PARTNERLOCATION';
UPDATE AD_Org SET M_PriceList_Version_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_PURCHASELISTVERSION';
UPDATE AD_Org SET M_Product_Category_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-POSCOMBOCATEGORY';
UPDATE AD_Org SET M_Warehouse_ID = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-PRINCIPALWAREHOUSE';
UPDATE AD_Org SET POS_CheckOpenOrdersTime = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_POSCheckOpenOrdersTime';
UPDATE AD_Org SET POS_CheckOrdersTime = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_POSCheckOrdersTime';
UPDATE AD_Org SET POS_Decimals = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_POS_DECIMALS';
UPDATE AD_Org SET POS_Locale = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_POS_LOCALE';
UPDATE AD_Org SET POS_PrintersByGroup = TRIM(AD_SysConfig.Value) FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ-POSPRINTERSBYGROUP';
UPDATE AD_Org SET SizeImage = TRIM(AD_SysConfig.Value)::numeric FROM AD_SysConfig WHERE AD_Org.AD_Org_ID = AD_SysConfig.AD_Org_ID AND AD_SysConfig.Name = 'SMJ_SizeImage';

-- Eliminar SysConfig que no se usan en Loyalty
DELETE FROM AD_SysConfig WHERE Name IN ('SMJ_ADMINROLES','SMJ-DEFAULTCUSTOMER','SMJ_CHARGEMEMBERDISCOUNTS','SMJ_CHARGE_DEPOSITS_POS','SMJ_CHARGETIPS','SMJ-CURRENCYID','SMJ-TAXDISCOUNT',
'SMJ_DefaultPrinter','SMJ_DISCOUNTSRESTRICTED','SMJ_POSCLIENTPRINTING','SMJ_ISPOSTAX','SMJ-SendIsMandatory','SMJ-PARTNERLOCATION','SMJ_PURCHASELISTVERSION','SMJ-POSCOMBOCATEGORY',
'SMJ_POSCheckOpenOrdersTime','SMJ_POSCheckOrdersTime','SMJ-POSPRINTERSBYGROUP','SMJ_SizeImage');

-- Eliminar SysConfig que ya no estan siendo usadas
--DELETE FROM AD_SysConfig WHERE Name IN ('SMJ-ACCOUNTDEFAULT','SMJ-ACCOUNTDEFAULTCC','SMJ-ACCOUNTDEFAULTCHECK','SMJ_CHARGEDISCOUNTS','SMJ_CHARGEMEMBERDISCOUNTS','SMJ_SALESLISTVERSION','SMJ_PURCHASELISTVERSION');

-- Actualizar los procesos de los JasperReport, Eliminando la clase y actualizando jasper por jrxml
UPDATE AD_Process SET JasperReport = 'salesCategory.jrxml' WHERE AD_Process_ID = 1000010;
UPDATE AD_Process SET JasperReport = REPLACE(JasperReport, 'jasper', 'jrxml'), ClassName = '' WHERE UPPER(value) LIKE '%SMJ%' AND JasperReport IS NOT NULL;

UPDATE AD_SYSTEM
       SET releaseno = '2.0',
       VERSION = '2014-10-31',
	   isfailonbuilddiffer = 'N'
 WHERE ad_system_id = 0 AND ad_client_id = 0;