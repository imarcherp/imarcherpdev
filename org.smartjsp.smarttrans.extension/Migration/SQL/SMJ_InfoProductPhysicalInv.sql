
CREATE OR REPLACE FUNCTION smj_infoproductphysicalinv(_suc text, _value text, _column text, _productCategory text)
  RETURNS TABLE
  (
  m_product_id		numeric(10,0)
  , c_bpartner_id	numeric(10,0)
  , partnername		character varying
  , m_locator_id	numeric(10,0)
  , m_warehouse_id	numeric(10,0)
  , warehousename	character varying
  , qty				numeric(10,0)
  , ad_client_id	numeric(10,0)
  , ad_org_id		numeric(10,0)
  ) AS
$func$
BEGIN
   
   RETURN QUERY EXECUTE 'SELECT p.m_product_id,
    NULL::numeric AS c_bpartner_id,
    NULL::character varying AS partnername,
    l.m_locator_id,
    w.m_warehouse_id,
    w.Name AS warehousename,
    sum(s.qtyonhand) AS qty,
    i.ad_client_id,
    i.ad_org_id
   FROM m_storageonhand s
     JOIN m_inventoryline il ON il.m_attributesetinstance_id = s.m_attributesetinstance_id AND il.qtycount > 0::numeric
     JOIN m_inventory i ON i.m_inventory_id = il.m_inventory_id
     JOIN m_product p ON p.m_product_id = s.m_product_id
     JOIN m_locator l ON l.m_locator_id = s.m_locator_id
     JOIN m_warehouse w ON w.m_warehouse_id = l.m_warehouse_id
  WHERE s.qtyonhand > 0::numeric AND UPPER(' || _column || ') LIKE UPPER(' || chr(39) || _value || chr(39) || ') AND l.x = ' || chr(39) || _suc || chr(39)  || ' AND p.M_Product_Category_ID NOT IN (' || _productCategory || ') 
    AND i.DocStatus IN (' || chr(39) || 'CO' || chr(39) || ',' || chr(39) || 'CL' || chr(39) || ',' || chr(39) || 'CU' || chr(39) || ')
  GROUP BY p.m_product_id, l.m_locator_id, w.m_warehouse_id, w.name, i.ad_client_id, i.ad_org_id
 HAVING sum(s.qtyonhand) > 0::numeric';
END;
$func$  LANGUAGE plpgsql;