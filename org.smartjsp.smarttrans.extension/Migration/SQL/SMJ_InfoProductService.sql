-- Function: adempiere.smj_infoproductservice(text, text, text, text)

-- DROP FUNCTION adempiere.smj_infoproductservice(text, text, text, text);

CREATE OR REPLACE FUNCTION adempiere.smj_infoproductservice(
    IN _value text,
    IN _column text,
    IN _productcategorynopos text,
    IN _productcategoryadmservice text)
  RETURNS TABLE(m_product_id numeric, c_bpartner_id numeric, partnername character varying, m_locator_id numeric, m_warehouse_id numeric, warehousename character varying, qty numeric, ad_client_id numeric, ad_org_id numeric) AS
$BODY$
BEGIN

   RETURN QUERY EXECUTE 'SELECT p.m_product_id,
      NULL::numeric AS c_bpartner_id,
      NULL::character varying AS partnername,
      NULL::numeric AS m_locator_id,
      NULL::numeric AS m_warehouse_id,
      NULL::character varying AS warehousename,
      99999::numeric AS Qty,
    p.ad_client_id,
    p.ad_org_id
     FROM m_product p
    WHERE p.IsActive = ' || chr(39) || 'Y' || chr(39) || ' AND p.ProductType = ' || chr(39) || 'S' || chr(39) || ' AND UPPER(' || _column || ') LIKE UPPER(' || chr(39) || _value || chr(39) || ') AND (p.M_Product_Category_ID NOT IN (' || _productCategoryNoPOS || ')
     OR p.M_Product_Category_ID IN (' || _productCategoryAdmService || '));';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION adempiere.smj_infoproductservice(text, text, text, text)
  OWNER TO adempiere;
