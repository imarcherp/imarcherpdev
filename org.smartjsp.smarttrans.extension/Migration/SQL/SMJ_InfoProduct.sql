
CREATE OR REPLACE FUNCTION smj_infoproduct(_suc text, _value text, _column text, _productCategory text)
  RETURNS TABLE
  (
  m_product_id		numeric(10,0)
  , c_bpartner_id	numeric(10,0)
  , partnername		character varying
  , m_locator_id	numeric(10,0)
  , m_warehouse_id	numeric(10,0)
  , warehousename	character varying
  , qty				numeric(10,0)
  , ad_client_id	numeric(10,0)
  , ad_org_id		numeric(10,0)
  ) AS
$func$
BEGIN
   
   RETURN QUERY EXECUTE 'SELECT p.m_product_id,
	    NULL::numeric AS c_bpartner_id,
	    NULL::character varying AS partnername,
	    l.m_locator_id,
	    w.m_warehouse_id,
	    w.Name AS warehousename,
	    sum(s.qtyonhand) AS qty,
		i.ad_client_id,
		i.ad_org_id
	   FROM m_storageonhand s
	     JOIN m_inoutline il ON il.m_attributesetinstance_id = s.m_attributesetinstance_id AND il.movementqty > 0::numeric
	     JOIN m_inout i ON i.m_inout_id = il.m_inout_id
	     JOIN c_order c ON c.c_order_id = i.c_order_id
	     JOIN m_product p ON p.m_product_id = s.m_product_id
	     JOIN c_bpartner b ON b.c_bpartner_id = c.c_bpartner_id
	     JOIN m_locator l ON l.m_locator_id = s.m_locator_id
	     JOIN m_warehouse w ON w.m_warehouse_id = l.m_warehouse_id
	     JOIN c_bpartner_product bp ON bp.m_product_id = p.m_product_id AND bp.c_bpartner_id = c.c_bpartner_id
	  WHERE s.qtyonhand > 0::numeric AND (w.name::text IN ( SELECT m_locator.x
		   FROM m_locator)) AND UPPER(' || _column || ') LIKE UPPER(' || chr(39) || _value || chr(39) || ') AND l.x = ' || chr(39) || _suc || chr(39)  || ' AND p.M_Product_Category_ID NOT IN (' || _productCategory || ') 
		   	AND i.DocStatus IN (' || chr(39) || 'CO' || chr(39) || ',' || chr(39) || 'CL' || chr(39) || ',' || chr(39) || 'CU' || chr(39) || ')	
	  GROUP BY p.m_product_id, l.m_locator_id, w.m_warehouse_id, w.name, i.ad_client_id, i.ad_org_id
	 HAVING sum(s.qtyonhand) > 0::numeric
	UNION
	 SELECT p.m_product_id,
	    c.c_bpartner_id,
	    b.name AS partnername,
	    l.m_locator_id,
	    w.m_warehouse_id,
	    w.name AS warehousename,
	    sum(s.qtyonhand) AS qty,
		i.ad_client_id,
		i.ad_org_id
	   FROM m_storageonhand s
	     JOIN m_inoutline il ON il.m_attributesetinstance_id = s.m_attributesetinstance_id AND il.movementqty > 0::numeric
	     JOIN m_inout i ON i.m_inout_id = il.m_inout_id
	     JOIN c_order c ON c.c_order_id = i.c_order_id
	     JOIN m_product p ON p.m_product_id = s.m_product_id
	     JOIN c_bpartner b ON b.c_bpartner_id = c.c_bpartner_id
	     JOIN m_locator l ON l.m_locator_id = s.m_locator_id
	     JOIN m_warehouse w ON w.m_warehouse_id = l.m_warehouse_id
	     JOIN c_bpartner_product bp ON bp.m_product_id = p.m_product_id AND bp.c_bpartner_id = c.c_bpartner_id
	  WHERE s.qtyonhand > 0::numeric AND NOT (w.name::text IN ( SELECT m_locator.x
		   FROM m_locator)) AND UPPER(' || _column || ') LIKE UPPER(' || chr(39) || _value || chr(39) || ') AND l.x = ' || chr(39) || _suc || chr(39)  || ' AND p.M_Product_Category_ID NOT IN (' || _productCategory || ')
			AND i.DocStatus IN (' || chr(39) || 'CO' || chr(39) || ',' || chr(39) || 'CL' || chr(39) || ',' || chr(39) || 'CU' || chr(39) || ')	
	  GROUP BY p.m_product_id, c.c_bpartner_id, b.name, l.m_locator_id, w.m_warehouse_id, w.name, i.ad_client_id, i.ad_org_id
	 HAVING sum(s.qtyonhand) > 0::numeric';
END;
$func$  LANGUAGE plpgsql;