ALTER TABLE ad_org ADD COLUMN smj_doctype_consignreceipt_id numeric(10,0);
ALTER TABLE ad_org ADD COLUMN smj_doctype_consignorder_id numeric(10,0);
ALTER TABLE ad_org ADD COLUMN smj_doctype_order_id numeric(10,0);
ALTER TABLE ad_org ADD COLUMN smj_doctype_receipt_id numeric(10,0);

--Delete
ALTER TABLE m_warehouse DROP COLUMN smj_acctconsigment1;
ALTER TABLE m_warehouse DROP COLUMN smj_acctconsigment2;
