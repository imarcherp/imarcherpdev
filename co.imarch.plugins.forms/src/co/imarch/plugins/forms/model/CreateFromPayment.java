package co.imarch.plugins.forms.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;

public abstract class CreateFromPayment extends CreateFrom
{
	/**
	 *  Protected Constructor
	 *  @param mTab MTab
	 */
	public CreateFromPayment(GridTab gridTab) {
		super(gridTab);
		if (log.isLoggable(Level.INFO)) log.info(gridTab.toString());
	}

	/**
	 *  Dynamic Init
	 *  @return true if initialized
	 */
	public boolean dynInit() throws Exception
	{
		log.config("");
		setTitle(Msg.getElement(Env.getCtx(), "C_Payment_ID", false) + " .. " + Msg.translate(Env.getCtx(), "CreateFrom"));
		
		return true;
	}  
	
	protected Vector<String> getIColumnNames()
	{
		//  Header Info
	    Vector<String> columnNames = new Vector<String>(7);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add(Msg.translate(Env.getCtx(), "DocumentNo"));
	    columnNames.add(Msg.translate(Env.getCtx(), "GrandTotal"));
	    columnNames.add(Msg.translate(Env.getCtx(), "OpenAmt"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "C_Withholding_ID")+" sin Aplicar");

	    
	    return columnNames;
	}
	
	protected void configureMiniTable (IMiniTable miniTable)
	{
		miniTable.setColumnClass(0, Boolean.class, false);     //  Selection
		miniTable.setColumnClass(1, String.class, true);          //  DocumentNo
		miniTable.setColumnClass(2, BigDecimal.class, true);      //  GrandTotal
		miniTable.setColumnClass(3, BigDecimal.class, true);      //  OpenAmt
		miniTable.setColumnClass(4, BigDecimal.class, true);      //  Withholding		
		//  Table UI
		miniTable.autoSize();
		
	}
	
	protected Vector<Vector<Object>> getInvoiceData (int C_BPartner_ID, int AD_Org_ID)
	{
		/**
		 *  Selected        - 0
		 *  DocumentNo      - 1
		 *  GrandTotal      - 2
		 *  OpenAmt         - 3
		 *  Withholding     - 4
		 */
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID=" + C_BPartner_ID);
		
		StringBuilder sql = new StringBuilder("SELECT i.C_Invoice_ID, i.DocumentNo,"
		+ " i.grandtotal, invoiceOpen(i.C_Invoice_ID,i.C_InvoicePaySchedule_ID), COALESCE(SUM(taxamt),0) "
		+ " FROM C_Invoice_v i "
		+ " LEFT JOIN LCO_InvoiceWithholding lco ON lco.C_Invoice_id = i.C_Invoice_id AND lco.processed='N'"
		+ " WHERE i.issotrx=? AND i.DocStatus='CO' AND i.IsPaid='N' AND i.C_BPartner_ID=? AND i.AD_Org_ID=?"
		+ " AND NOT EXISTS( SELECT pa.C_Invoice_id FROM C_PaymentAllocate pa WHERE pa.AD_Org_ID=i.AD_Org_ID AND pa.C_Invoice_id=i.C_Invoice_id "
		+ " AND pa.C_payment_ID IN (SELECT C_payment_ID FROM C_payment p WHERE processed='N' AND p.C_payment_ID=pa.C_payment_ID)) "
		+ " GROUP BY i.DocumentNo, i.C_Invoice_ID, i.C_InvoicePaySchedule_ID,i.grandtotal ");
		
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			if((Boolean)getGridTab().getValue("IsReceipt"))
				pstmt.setString(1,"Y" );
			else
				pstmt.setString(1,"N" );
			pstmt.setInt(2, C_BPartner_ID);
			pstmt.setInt(3, ((Integer) getGridTab().getValue("AD_Org_ID")).intValue());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<Object> line = new Vector<Object>();
				KeyNamePair doc = new KeyNamePair(rs.getInt(1), rs.getString(2));
				line.add(new Boolean(false));  
				line.add(doc);
				line.add(rs.getBigDecimal(3));
				line.add(rs.getBigDecimal(4));
				line.add(rs.getBigDecimal(5));
				data.add(line);				
			}
		}
		catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}
	
	/**
	 *  Save - Create Invoice Lines
	 *  @return true if saved
	 */
	public boolean save(IMiniTable miniTable, String trxName) 
	{
	
		int C_Payment_ID = ((Integer) getGridTab().getValue("C_Payment_ID")).intValue();
		
		MPayment payment = new MPayment(Env.getCtx(), C_Payment_ID, trxName);
		
		
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue()) {
				
				MPaymentAllocate pallocate = new MPaymentAllocate(Env.getCtx(), 0, trxName);
				int AD_Org_ID = payment.getAD_Org_ID();	
				KeyNamePair iddoc = (KeyNamePair)miniTable.getValueAt(i, 1);
				int C_Invoice_ID = iddoc.getKey();
				BigDecimal InvoiceAmt = (BigDecimal)miniTable.getValueAt(i, 3);
				BigDecimal WriteOffAmt = (BigDecimal)miniTable.getValueAt(i, 4);
				BigDecimal Amount = BigDecimal.ZERO;
				if(WriteOffAmt!=null)
					Amount = InvoiceAmt.subtract(WriteOffAmt);
				
				pallocate.setAD_Org_ID(AD_Org_ID);
				pallocate.setC_Invoice_ID(C_Invoice_ID);
				pallocate.setC_Payment_ID(C_Payment_ID);
				pallocate.setAmount(Amount);
				pallocate.setInvoiceAmt(InvoiceAmt);
				pallocate.setWriteOffAmt(WriteOffAmt);
				pallocate.saveEx();
				
			}
		}
		
		return true;
	}
	
	/**
	 *  List number of rows selected
	 */
	public void info(IMiniTable miniTable, IStatusBar statusBar)
	{

		
	}   //  infoInvoice
	
}
