package co.imarch.plugins.forms.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.apps.IStatusBar;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

public class WCreateFromPayment extends CreateFromPayment implements EventListener<Event>, ValueChangeListener
{

	private WCreateFromWindow window;
	
	/** Window No               */
	private int p_WindowNo;
	
	public WCreateFromPayment(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow(this, getGridTab().getWindowNo());
		p_WindowNo = getGridTab().getWindowNo();
		
		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
		
	}

	protected Label bPartnerLabel = new Label();
	protected Label lbvaltitulo = new Label("Valor a pagar:  ");
	protected Textbox txtval = new Textbox("0");
	
	private Grid parameterStdLayout;
	
	protected WEditor bPartnerField;
	
	
	@Override
	public Object getWindow() {
		return window;
	}

	@Override
	public boolean dynInit() throws Exception {
		
		log.config("");
		super.dynInit();
		
		window.setTitle("Pagar Varias Facturas");
		
		
		initBPartner(false);
		bPartnerField.addValueChangeListener(this);
		
		return true;
	}
	
	protected void zkInit() throws Exception{
		
		bPartnerLabel.setText(Msg.getElement(Env.getCtx(), "C_BPartner_ID"));
		
		Vlayout vlayout = new Vlayout();
		ZKUpdateUtil.setVflex(vlayout, "min");
		ZKUpdateUtil.setWidth(vlayout, "100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
    	ZKUpdateUtil.setVflex(vlayout, "parameterStdLayout");
    	
    	Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
		row.appendChild(bPartnerLabel.rightAlign());
		if (bPartnerField != null) {
			row.appendChild(bPartnerField.getComponent());
			bPartnerField.fillHorizontal();
		}
		row = rows.newRow();
		
		lbvaltitulo.setStyle("color:blue");
		txtval.setStyle("color:blue");
		//txtval.setEnabled(false);
		txtval.setReadonly(true);
		row.appendCellChild(lbvaltitulo.rightAlign());
		row.appendCellChild(txtval);
		
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		if (m_actionActive)
			return;
		m_actionActive = true;
		
		m_actionActive = false;
		
	}

	private boolean 	m_actionActive = false;
	@Override
	public void onEvent(Event event) throws Exception {

		if (m_actionActive)
			return;
		m_actionActive = true;
		
		m_actionActive = false;
	}
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	protected void initBPartner (boolean forInvoice) throws Exception
	{
		//  load BPartner
		int AD_Column_ID = 3499;        //  C_Invoice.C_BPartner_ID
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		bPartnerField = new WSearchEditor ("C_BPartner_ID", true, false, true, lookup);
		//
		int C_BPartner_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_BPartner_ID");
		bPartnerField.setValue(new Integer(C_BPartner_ID));
		bPartnerField.setReadWrite(false);
		
		int AD_Org_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_Org_ID");
		
		loadTableI(getInvoiceData(C_BPartner_ID, AD_Org_ID));
	} 
	
	/**
	 *  Load Invoice data into Table
	 *  @param data data
	 */
	protected void loadTableI (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getIColumnNames());
		//
		fixWidthColumns(window.getWListbox());
		configureMiniTable(window.getWListbox());
	}   //  loadOrder

	
	private void fixWidthColumns(WListbox table) {
		int i=0;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(i==2 || i==3)	//columnas Pago Aplicado y Cantidad Ajuste
				ZKUpdateUtil.setHflex(header, "max");
			else
				ZKUpdateUtil.setHflex(header, "min");
		
			i++;
		}
	}
	
	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		
		BigDecimal valorpago = BigDecimal.ZERO;
		DecimalFormat formato = new DecimalFormat("#,###.##");
		int rows = miniTable.getRowCount();
		if(rows<1)
			return;
		
		for(int i=0; i<rows; i++)
		{
			if ((Boolean) miniTable.getValueAt(i, 0) )
			{
				valorpago = valorpago.add((BigDecimal) miniTable.getValueAt(i, 3));
				valorpago = valorpago.subtract((BigDecimal) miniTable.getValueAt(i, 4));
			}
		}
		txtval.setValue(formato.format(valorpago.doubleValue()));
	}
	
}
