package co.imarch.plugins.forms;

import java.math.BigDecimal;
//import java.net.URL;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.DynamicMediaLink;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WNumberEditor;


import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MSysConfig;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
//import org.eclipse.core.runtime.Path;
//import org.eclipse.core.runtime.Platform;
//import org.osgi.framework.Bundle;
import org.zkoss.zk.au.out.AuFocus;
//import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
//import org.zkoss.zul.Borderlayout;
import org.adempiere.webui.component.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Messagebox;
//import org.adempiere.webui.component.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.smj.model.MSMJFuelSales;

public class WFuelCalibrate extends FuelCalibrate implements IFormController, EventListener<Event>,
			WTableModelListener, ValueChangeListener{

	private CustomForm form = new CustomForm(); 
	private int m_smj_rchangefuelline_ID = MSysConfig.getIntValue("iMarch_RChangeCalibrate", 0, Env.getAD_Client_ID(Env.getCtx()));
	private int MaxValueCalibrate = MSysConfig.getIntValue("iMarch_MaxValueCalibrate", 0, Env.getAD_Client_ID(Env.getCtx()));
	private int m_user_id = Env.getAD_User_ID(Env.getCtx());
	
	private HashMap<String, LinkedList<MSMJFuelSales>> fuelList = null;
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private Vector<Vector<Object>> dataSales = new Vector<Vector<Object>>();
	
	@Override
	public ADForm getForm() {
		return form;
	}
	/**
	 *	Initialize Panel
	 *  @param WindowNo window
	 *  @param frame frame
	 */
	public WFuelCalibrate() {
		// Env.setContext(Env.getCtx(), form.getWindowNo(), "IsSOTrx", "Y");
		 try
		 {
			 //super.dynInit();			 
			 dynInit();
			 zkInit(); 
				form.addEventListener(Events.ON_OPEN, this);
				AuFocus auf = new AuFocus(fuelLineListbox);
				Clients.response(auf);	 
		 }
		 catch (Exception e)
		 {
			 log.log(Level.SEVERE, "", e);
		 }
	}	

	 private Borderlayout mainLayout = new Borderlayout();
	 private Panel parameterPanel = new Panel();
	 private Grid gridLayout = GridFactory.newGridLayout();
	 private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	 
	 private Label datefuelcaliblabel = new Label();
	 private WDateEditor datefuelcalibrate = new WDateEditor();
	 private Label fuelLineLabel = new Label();
	 private Listbox fuelLineListbox =  ListboxFactory.newDropdownListbox();;
	 private Label calibratelabel = new Label();
	 private WNumberEditor calibrateNumericEdit = new WNumberEditor();
	 
	 private Button addButton = new Button ();
	 
	 private Label title = new Label();
	 
	//panel Tabla
	 private Panel tablePanel = new Panel();
	 
	 private WListbox dataTable = ListboxFactory.newDataTable();
	//---Panel de informacion ------
	 private Borderlayout  infolayout = new Borderlayout();
	
	 private DynamicMediaLink lbimagen = new DynamicMediaLink();
	 
	/**
	 * Inicialice Panel
	 */
	public void dynInit() throws Exception {
		 	        
		datefuelcalibrate = new WDateEditor("EntryDate", false, false, true, "EntryDate");
		//datefuelcalibrate.setValue(getEntrydateFuelSales());
		 datefuelcalibrate.setReadWrite(true);
		 datefuelcalibrate.addValueChangeListener(this);

		 fuelLineListbox = FuelLine();
		 fuelLineListbox.addActionListener(this);
		 fuelLineListbox.setSelectedIndex(0);
		 fuelLineListbox.setEnabled(false);
		 
		 addButton.addActionListener(this);
		 addButton.setEnabled(false);
		 
		 //Definiendo texto en los labels
		 datefuelcaliblabel.setText("" + Msg.getElement(Env.getCtx(), "DateTrx"));
		 fuelLineLabel.setText("" + Msg.translate(Env.getCtx(), "smj_FuelLine_ID"));
		 calibratelabel.setText(Msg.translate(Env.getCtx(), "fuelcalibration"));
		 addButton.setLabel("Adicionar");
		 
		 title.setStyle("color: blue; font-weight:bold; font-size:150%; text-align:center;");
		 title.setText(Msg.translate(Env.getCtx(), "fuelcalibration").toUpperCase());
		 
		 confirmPanel.setEnabledAll(true);
		 confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
		 confirmPanel.addActionListener(this);
		 
		 fuelList = new HashMap<String, LinkedList<MSMJFuelSales>>();
		
		 dataTable.addEventListener(Events.ON_DOUBLE_CLICK, this);
		 
		 
		 lbimagen.setImage(ThemeManager.getThemeResource("images/imicon.png")); //Carpeta del Theme
		 lbimagen.setDisabled(true);
		 
		 calibrateNumericEdit = new WNumberEditor("CalibrateNumber", false, false, true, DisplayType.Number, "CalibrateNumber");
		 calibrateNumericEdit.setValue(Env.ZERO);
		 calibrateNumericEdit.addValueChangeListener(this);
		 
	 }
	 
	 //@SuppressWarnings("deprecation")
	private void zkInit() throws Exception
	 {
		 
		 form.appendChild(mainLayout);
		 //Definiendo DIMENCIONES 
		 //mainLayout.setWidth("99%");
		 //mainLayout.setHeight("100%");
		 ZKUpdateUtil.setWidth(mainLayout, "99%");
		 ZKUpdateUtil.setHeight(mainLayout, "100%");

	//--North 
		 North north = new North();
		 north.appendChild(parameterPanel);
		 mainLayout.appendChild(north);
		
		 	Hbox boxtitle = new Hbox();
		 	boxtitle = new Hbox();
		 	//boxtitle.setWidth("100%");
		 	ZKUpdateUtil.setWidth(boxtitle, "100%");
		 	boxtitle.setPack("center");
		 	boxtitle.appendChild(title);
			 
			 Hbox boximg = new Hbox();
			 boximg = new Hbox();
			 //boximg.setWidth("100%");
			 ZKUpdateUtil.setWidth(boximg, "100%");
			 boximg.setPack("center");
			 boximg.appendChild(lbimagen);
		 Div d = new Div();
		 //imagen.setImage("http://www.imarch.co/wp-content/uploads/2017/08/imgcab1.png");
		 
		 
		 parameterPanel.appendChild(boximg);
		 parameterPanel.appendChild(d);
		 parameterPanel.appendChild(boxtitle);
		 parameterPanel.appendChild(d);
		 parameterPanel.appendChild(gridLayout);
			 
		 
		 Rows rows = gridLayout.newRows();
		 Row row = rows.newRow();
		 row.appendCellChild(new Label());
		 row = rows.newRow();
		 //Fecha
		 row.appendCellChild(datefuelcaliblabel.rightAlign());
		 row.appendCellChild(datefuelcalibrate.getComponent());
		 //Mangueras
		 row.appendCellChild(fuelLineLabel.rightAlign());
		 row.appendCellChild(fuelLineListbox);
		 //Calibracio
		 row.appendCellChild(calibratelabel.rightAlign());
		 row.appendCellChild(calibrateNumericEdit.getComponent());
		 //Boton
		 row.appendCellChild(new Label());
		 row.appendCellChild(addButton);
		 row = rows.newRow();
		 row.appendCellChild(new Label());
	//---end North
		 
	/**
	 * Se coloca el South antes del Center 
	 * para ajustar el Foco, el cual es asignado en la clase dynInit()
	 * tambien influye el hecho de que el boton OK del panel de confirmacion
	 * este desactivado. Si el boton estuviera activo
	 * independiente de la asignacion en dynInit, queda con el foco el boton
	 */
	//---South	 (
		 South south = new South();
		 south.setStyle("boder: 1px solid red");
		 mainLayout.appendChild(south);		
		 //Botones de Confirmacion
		 south.appendChild(confirmPanel);
		 
	//---end South 
		 
	//---Center	 
		 Center center = new Center();
		 //center.setStyle("boder: 1px solid red");
		 center.appendChild(infolayout);
		 mainLayout.appendChild(center);
		 infolayout.setStyle("border: none");
		 //infolayout.setWidth("100%");
		 //infolayout.setHeight("100%");
		 ZKUpdateUtil.setWidth(infolayout, "100%");
		 ZKUpdateUtil.setHeight(infolayout, "100%");
		 
		 //TABLA
		 North tc = new North();
		 //tc.setHeight("100%");
		 ZKUpdateUtil.setHeight(tc, "100%");
		 tc.appendChild(tablePanel);
		 tc.setSplittable(true);
		 infolayout.appendChild(tc);
		 //tablePanel.setWidth("100%");
		 //tablePanel.setHeight("100%");
		 ZKUpdateUtil.setWidth(tablePanel, "100%");
		 ZKUpdateUtil.setHeight(tablePanel, "100%");
		 dataTable.setStyle("color: grey");
		 tablePanel.appendChild(dataTable);
		 
	//---end Center	 
	 }
	 
	@Override
	public void valueChange(ValueChangeEvent evt) {
		
		Object value = evt.getNewValue();
		String name = evt.getPropertyName();
		if(name.equals("CalibrateNumber")){
			BigDecimal input = (BigDecimal)value;
			if( input.compareTo(new BigDecimal(MaxValueCalibrate)) == 1) // input > MaxValueCalibrate
			{
				calibrateNumericEdit.setValue(Env.ZERO);
				Messagebox.show("La Calibracion supera el limite permitido");
				addButton.setEnabled(false);
				return;
			}
			else if(MaxValueCalibrate == 0){
				Messagebox.show("Variable *iMarch_MaxValueCalibrate* no esta configurada");
				addButton.setEnabled(false);
				return;
			}
			if(input.compareTo(Env.ZERO) > 0 && fuelLineListbox.getSelectedIndex()>0){
				addButton.setEnabled(true);
				addButton.setFocus(true);
				AuFocus auf = new AuFocus(addButton);
				Clients.response(auf);
			}
			else
				addButton.setEnabled(false);
		}//if CalibrateNumber
		else if(name.equals("EntryDate")){
			if(datefuelcalibrate.getValue()==null)
				fuelLineListbox.setEnabled(false);
			else
				fuelLineListbox.setEnabled(true);
		}
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		Object acction = arg0.getTarget();
		
		//ListBox
		if(acction.equals(fuelLineListbox) )
		{
			if(fuelLineListbox.getSelectedIndex()>0){
				datefuelcalibrate.setReadWrite(false);
				if( IsCalibrate((Timestamp)datefuelcalibrate.getValue(), (Integer) fuelLineListbox.getSelectedItem().getValue())
						 > 0 ){
					Messagebox.show("Esta Manguear ya tiene una Calibracion o venta con fecha "+datefuelcalibrate.getValue());
					calibrateNumericEdit.setReadWrite(false);
					addButton.setEnabled(false);	
				}
				else{
					calibrateNumericEdit.setReadWrite(true);
					addButton.setEnabled(false);
				}	
			}
			else
				datefuelcalibrate.setReadWrite(true);
			
			calibrateNumericEdit.setValue(Env.ZERO);
			
		}//end ListBox
		
		//Boton Adicionar
		else if(acction.equals(addButton))
		{
			if(fuelLineListbox.getSelectedIndex() <= 0 && Env.ZERO.compareTo((BigDecimal)calibrateNumericEdit.getValue()) <= 0)
				return;
			else if( IsCalibrate((Timestamp)datefuelcalibrate.getValue(), (Integer) fuelLineListbox.getSelectedItem().getValue())
					 > 0 ){
				Messagebox.show("Esta Manguear ya tiene una Calibracion o venta con fecha "+datefuelcalibrate.getValue());
				return;	
			}
			addElement();
			AuFocus auf = new AuFocus(fuelLineListbox);
			Clients.response(auf);
		}
		
		//Change Tabla -> Eliminar ingreso
		else if(Events.ON_DOUBLE_CLICK.equals(arg0.getName()) && acction instanceof WListbox)
		{
			if (acction.equals(dataTable)){
				Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGDeleteRecord"), labelInfo,
				        Messagebox.OK | Messagebox.CANCEL,
				        Messagebox.QUESTION,
				            new EventListener<Event>(){
				                public void onEvent(Event e){
				                    if("onOK".equals(e.getName())){
				                    	deleteElement();
				                    }else if("onCancel".equals(e.getName())){
				                    	return;
				                    }                    
				                }
				            }
				        );
			}//if dataTable
		}//ON_DOUBLE_CLICK
		
		else if (acction.equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
			confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
			procesar();
			clearFields();
			
		}
	}//end onEvent

	@SuppressWarnings("rawtypes")
	private void procesar() throws InterruptedException {

		
		Boolean flag = true;
		String trx = Trx.createTrxName("WFuelCalibrate" + form.getId());
		Trx trxName = Trx.get(trx, true);
		try {
				Iterator fuellineiterator = fuelList.entrySet().iterator();
				while(fuellineiterator.hasNext() && flag == true)
				{
					flag = true;
					Map.Entry fuellineentry = (Map.Entry)fuellineiterator.next();
					LinkedList<MSMJFuelSales> lista = fuelList.get(fuellineentry.getKey().toString());
					if(lista.size()>0){
						Iterator<MSMJFuelSales> itcal = lista.iterator();
						while(itcal.hasNext() && flag == true){
							MSMJFuelSales Calibraciones = itcal.next();
							Calibraciones.set_TrxName(trx);
							Boolean sok = Calibraciones.save();
							Boolean fok = updateFuelLine(trx, Calibraciones.getsmj_FuelLine_ID(), Calibraciones.gethundredend());
							if (!sok || !fok){
								flag = false;
								trxName.rollback();
								trxName.close();
								confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
								log.log(Level.SEVERE, "Error Fuel Sales :: "+ Calibraciones.getsmj_FuelLine_ID());
								return ;
							}
						}//while(itcal.hasNext() && flag == true)
					}//if(lista.size()>0)
				}//while 1		
		}//try
		catch(Exception e ){
			trxName.rollback();
			trxName.close();
			confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
			log.log(Level.SEVERE, "Error Create Fuel Sales :: ", e);
			Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGProcessError")+" -(iMarch)- "+ e.getMessage(), labelInfo, Messagebox.OK, Messagebox.ERROR);
			return;
		}
		finally{
			trxName.commit();
			trxName.close();
		}
		Messagebox.show("Calibracion Exitosa", "Calibrate", Messagebox.OK, Messagebox.EXCLAMATION);
		dataSales = new Vector<Vector<Object>>();
		loadTable(dataSales);
		fuelList = new HashMap<String, LinkedList<MSMJFuelSales>>();
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
		
	}
	
	private void deleteElement(){
		
		int index = dataTable.getSelectedIndex();
		String island = (String)dataTable.getValueAt(index, 1);
		String line = (String)dataTable.getValueAt(index, 2);
		String warehouse = (String)dataTable.getValueAt(index, 0);	

		LinkedList<MSMJFuelSales> list = fuelList.get(warehouse);
		if (list == null){
			list = new LinkedList<MSMJFuelSales>();
		}
		MSMJFuelSales fuel = getElementList(island, line, list);
		if	(fuel != null){
			list.remove(fuel);
		}
		fuelList.put(warehouse, list);
		dataTable.removeItemAt(index);
		dataSales.remove(index);
	}//end lessElement()
	
	private void addElement() throws InterruptedException{
		
		Vector<Object> DataFuelLine = null;
		DataFuelLine = getDataFuelLine(fuelLineListbox.getSelectedItem().toString());
		BigDecimal actualhundred = new BigDecimal(DataFuelLine.get(1).toString());
		BigDecimal finalhundred = actualhundred.add((BigDecimal)calibrateNumericEdit.getValue());
		String lineName = fuelLineListbox.getSelectedItem().getLabel();
		String warehouseName = DataFuelLine.get(5).toString();
		String islandName = DataFuelLine.get(2).toString();
		
		LinkedList<MSMJFuelSales> list = fuelList.get(warehouseName);
		if (list == null){
			list = new LinkedList<MSMJFuelSales>();
		}
		MSMJFuelSales fuel = getElementList(islandName,	lineName, list);
		if	(fuel != null){//verifica que no se ingrese dos veces la misma manguera
			Messagebox.show(Msg.translate(Env.getCtx(), "SMJMSGFuelLineExist"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		MSMJFuelSales f = new MSMJFuelSales(Env.getCtx(), 0, null);
		f.setC_BPartner_ID(getBParnterUser(m_user_id));
		f.setentrydate((Timestamp)datefuelcalibrate.getValue());
		f.setM_Warehouse_ID((Integer) DataFuelLine.get(4));
		f.setsmj_rchangefuelline_ID((Integer)m_smj_rchangefuelline_ID);
		f.setsmj_FuelIsland_ID((Integer) DataFuelLine.get(3));
		f.setsmj_FuelLine_ID((Integer) fuelLineListbox.getSelectedItem().getValue());
		f.setIslandName(islandName);
		f.setLineName(lineName);
		f.sethundredend(finalhundred);
		f.setactualhundred(actualhundred);
		f.setqtysold(Env.ZERO);
		f.settotalprice(Env.ZERO);
		f.setisprocessed(false);
		f.setgalonprice(Env.ZERO);
		list.add(f);
		
		fuelList.put(warehouseName, list);
		Vector<Object> rc = new Vector<Object>();
		rc.add(warehouseName);
		rc.add(islandName);
		rc.add(lineName);
		rc.add(NumberFormat.getInstance().format(actualhundred));
		rc.add(NumberFormat.getInstance().format(calibrateNumericEdit.getValue()));
		dataSales.add(rc);
		loadTable(dataSales);
		clearFields();
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
	}//end addElement
	
	private void clearFields(){
		calibrateNumericEdit.setValue(Env.ZERO);
		fuelLineListbox.setSelectedIndex(0);
		addButton.setEnabled(false);
		datefuelcalibrate.setReadWrite(false);
		fuelLineListbox.setEnabled(true);
	}//clearFields
	
	private void loadTable(Vector<Vector<Object>> datat){
		Vector<String> columnNames = getTableColumnNames();
		dataTable.clear();
		dataTable.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelP = new ListModelTable(datat);
		dataTable.setData(modelP, columnNames);
		dataTable.addActionListener(this);
		setTableColumnClass(dataTable);
	}//loadTable
	
	private MSMJFuelSales getElementList(String islandName, String lineName, LinkedList<MSMJFuelSales> list){
		MSMJFuelSales fuel = null;
		Boolean flag = true;
		Iterator<MSMJFuelSales> it = list.iterator();
		while (it.hasNext() && flag){
			MSMJFuelSales e = it.next();
			if (e.getIslandName().equals(islandName) && e.getLineName().equals(lineName)){
				flag = false;
				fuel = e;
			}
		}
		return fuel;
	}//getElementList

}
