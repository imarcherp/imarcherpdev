package co.imarch.plugins.forms;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

public class UnassingTool {
	
	public static CLogger log = CLogger.getCLogger(UnassingTool.class);
	
	public DecimalFormat formato = DisplayType.getNumberFormat(DisplayType.Amount);
	
	private boolean    	m_calculating 	= false;
	public int			m_AD_Org_ID		= 0;
	public int         	m_C_BPartner_ID = 0;
	public int          m_selected 	= 0;
	public BigDecimal	totalToolAssigned 	= Env.ZERO;
	public BigDecimal 	totalToolUnassigned 	= Env.ZERO;
	
	public int m_C_DocType_ID = MSysConfig.getIntValue("iMarch_UnassignedToolDocType", 0);		//Tipo Documento para devolucion de Herramienta Asignada
	public int m_M_locator_id = MSysConfig.getIntValue("iMarch_UnassignedToolLocator", 0);  	//Bodega Asignaciones
	public int m_M_locatorTo_id = MSysConfig.getIntValue("iMarch_UnassignedToolLocatorTo", 0);	//Bodega Compras
	public int m_AD_Process_ID = MSysConfig.getIntValue("iMarch_UnassignedToolADProcessID", 0);	//ID Informe / Proceso creado donde se relaciona el Jasper
	
	public Timestamp MovementDate = null;
	
	public void dynInit() throws Exception
	{
		m_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		
	}
	
	
	/*
	 * Obtiene las herramientas asigandas a un mecanico
	 */
	public Vector<Vector<Object>> getAssignedToolData(boolean SelectTodo)
	{
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		StringBuffer sql = new StringBuffer("SELECT * FROM( "
				+ "SELECT  m_product_id, "
				+ "(SELECT value || '_' || name FROM adempiere.m_product p WHERE p.m_product_id= ml.m_product_id ) as product, "
				+ "SUM(CASE WHEN m_locator_id=? THEN movementqty*-1 ELSE movementqty END) AS qty "
				+ "FROM M_MovementLine ml "
				+ "WHERE EXISTS "
				+ "(SELECT M_Movement_ID FROM M_Movement m "
				+ "WHERE m.M_Movement_ID=ml.M_Movement_ID AND m.docstatus='CO' AND m.C_BPartner_ID=?) "
				+ "AND m_locator_id IN (?,?) "
				+ "GROUP BY m_product_id "
				+ ") AS tools GROUP BY m_product_id, product, qty HAVING qty > 0 ");
		
		if(log.isLoggable(Level.FINE)) log.fine("SQLAsignedTool" + sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		m_selected=0;		//Deshabilita boton proceso
		
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, m_M_locator_id);
			pstmt.setInt(2, m_C_BPartner_ID);
			pstmt.setInt(3, m_M_locator_id);
			pstmt.setInt(4, m_M_locatorTo_id);
			
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				Vector<Object> row = new Vector<Object>();
				if(SelectTodo)
					row.add(new Boolean(true));       //  0-Selection
				else
					row.add(new Boolean(false)); 
				
				KeyNamePair prod = new KeyNamePair(rs.getInt(1), rs.getString(2));
				row.add(prod);							//1-Producto Herramienta
				row.add(rs.getBigDecimal(3));			//2-Cantidad Asignada
				if(SelectTodo)
				{
					row.add(rs.getBigDecimal(3));		//3-Qty a desasignar
					row.add(new BigDecimal(0));			//4-Total qty asignada
					m_selected++;		//Habilita boton proceso
				}
				else
				{
					row.add(new BigDecimal(0));			//3-Qty a desasignar
					row.add(rs.getBigDecimal(3));				//4-Total qty asignada
				}				
				
				
				data.add(row);		
			}
		}
		catch(SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		return data;
	}	
	
	public Vector<String> getAsignToolColumnName()
	{
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
		columnNames.add("Herramienta");
		columnNames.add("Cantidad Actual");
		columnNames.add("Cantidad que Entrega");
		columnNames.add("Cantidad Asignada");  //totalToolAssigned
		return columnNames;
	}
	
	public void setAsigToolColumnNameClass(IMiniTable assignedtoolTable)
	{
		int c=0;
		assignedtoolTable.setColumnClass(c++, Boolean.class, false);  		//select
		assignedtoolTable.setColumnClass(c++, String.class , true);			//Producto Herramienta
		assignedtoolTable.setColumnClass(c++, BigDecimal.class , true);		//Cantidad Actial Asignada
		assignedtoolTable.setColumnClass(c++, BigDecimal.class , false);		//Cantidad a quitar
		assignedtoolTable.setColumnClass(c++, BigDecimal.class , true);		//Cantidad a asignar
		
		assignedtoolTable.autoSize();
	}
	
	
	public String writeOff(int row, int col, IMiniTable assignedtoolTable)
	{
		String msg="";
		
		/**
		 *  Setting defaults
		 */
		if (m_calculating)  //  Avoid recursive calls
			return msg;
		m_calculating = true;
		
		if (log.isLoggable(Level.CONFIG)) log.config("Row=" + row 
				+ ", Col=" + col + ", MovementTable=");
		
		boolean selected = ((Boolean)assignedtoolTable.getValueAt(row, 0)).booleanValue();
		BigDecimal m_asignedTool = (BigDecimal)assignedtoolTable.getValueAt(row, 2);
		BigDecimal m_qtyunasignedTool = new BigDecimal(((BigDecimal)assignedtoolTable.getValueAt(row, 3)).intValue());
		
		if(m_qtyunasignedTool.compareTo(m_asignedTool) > 0 || m_qtyunasignedTool.compareTo(Env.ZERO) < 0)
			m_qtyunasignedTool=m_asignedTool;
		
		if (col == 0)
		{
			// selection of payment row
			if (selected)
			{
				m_qtyunasignedTool = m_asignedTool;   //  Cantidad a Desasignar herramienta
				totalToolAssigned = m_asignedTool.subtract(m_qtyunasignedTool);
				m_selected++;
			}
			else    //  deselected
			{
				m_qtyunasignedTool	= Env.ZERO;	
				totalToolAssigned	= Env.ZERO;
				m_selected--;
			}
			
		}
		
		if ( selected && col != 0 )
		{
			totalToolAssigned = m_asignedTool.subtract(m_qtyunasignedTool);
		}
			
		assignedtoolTable.setValueAt(m_qtyunasignedTool, row, 3);
		assignedtoolTable.setValueAt(totalToolAssigned, row, 4);
		
		m_calculating = false;
		return msg;
		
	}
	
	public MMovement saveData(int m_WindowNo, Object date, IMiniTable assignedtoolTable, String trxName)
	{
		//  fixed fields
		
		int AD_Client_ID = Env.getContextAsInt(Env.getCtx(), m_WindowNo, "AD_Client_ID");
		int AD_Org_ID = Env.getContextAsInt(Env.getCtx(), m_WindowNo, "AD_Org_ID");
		int C_BPartner_ID = m_C_BPartner_ID;
		int C_DocType_ID = m_C_DocType_ID;
		int M_Locator_ID = m_M_locator_id;
		int M_LocatorTo_ID = m_M_locatorTo_id;
		Timestamp DateTrx = (Timestamp)date;
		
		
		if (AD_Org_ID == 0)	throw new AdempiereException("@Org0NotAllowed@");
		//
		if (log.isLoggable(Level.CONFIG)) log.config("Client=" + AD_Client_ID + ", Org=" + AD_Org_ID
			+ ", BPartner=" + C_BPartner_ID + ", Date=" + DateTrx);
		
			
		MMovement inventorymove = new MMovement(Env.getCtx(), 0, trxName);
		inventorymove.setAD_Org_ID(AD_Org_ID);
		inventorymove.setC_BPartner_ID(C_BPartner_ID);
		inventorymove.setC_DocType_ID(C_DocType_ID);
		inventorymove.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
		inventorymove.setMovementDate(DateTrx);
		
		inventorymove.saveEx();
		
		//Obtiene el numero de registros encontrados (No. de Herramientas Asignada)
		int row = assignedtoolTable.getRowCount();
		
		for(int i=0; i < row; i++)
		{
			if(((Boolean)assignedtoolTable.getValueAt(i, 0)).booleanValue())
			{
				MMovementLine lineaMov = new MMovementLine(inventorymove);
				lineaMov.setM_Locator_ID(M_Locator_ID);
				lineaMov.setM_LocatorTo_ID(M_LocatorTo_ID);
				
				KeyNamePair product = (KeyNamePair)assignedtoolTable.getValueAt(i, 1);
				int M_Product_ID = product.getKey();
				lineaMov.setM_Product_ID(M_Product_ID);
				
				BigDecimal MovementQty = new BigDecimal(((BigDecimal)assignedtoolTable.getValueAt(i, 3)).intValue());
				lineaMov.setMovementQty(MovementQty);
				
				lineaMov.saveEx();	
			}
		}
		
		//	Should start WF
		if(inventorymove.get_ID() != 0)
		{
			if(!inventorymove.processIt(DocAction.ACTION_Complete))
					throw new AdempiereException("Document no complete: " + inventorymove.getProcessMsg());
			inventorymove.saveEx();
		}
		
		return inventorymove;
	}
	
}
