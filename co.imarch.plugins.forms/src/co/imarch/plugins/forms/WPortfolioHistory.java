package co.imarch.plugins.forms;

import static org.adempiere.webui.ClientInfo.MEDIUM_WIDTH;
import static org.adempiere.webui.ClientInfo.SMALL_WIDTH;
import static org.adempiere.webui.ClientInfo.maxWidth;
import static org.compiere.model.SystemIDs.COLUMN_C_INVOICE_C_BPARTNER_ID;
import static org.compiere.model.SystemIDs.COLUMN_C_PERIOD_AD_ORG_ID;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Vector;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.tools.FileUtil;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.North;
import org.zkoss.zul.South;


public class WPortfolioHistory extends PortfolioHistory
	implements IFormController, EventListener<Event>, WTableModelListener, ValueChangeListener 
{

	
	private CustomForm form = new CustomForm();
	
	public WPortfolioHistory() {
	
		Env.setContext(Env.getCtx(), form.getWindowNo(), "IsSOTrx", "Y");
		try 
		{
			super.dynInit();
			dynInit();
			zkInit();
		}
		catch(Exception e) {
			
		}
	}
	
	private int noOfColumn;
	
	private static final String TITLE = "Info";
	
	private Borderlayout mainLayout = new Borderlayout();
	private Borderlayout tableLayout = new Borderlayout();
	private Borderlayout infoPanel = new Borderlayout();
	
	private Label dateLabel = new Label();
	private Label bpartnerLabel = new Label();
	private Label organizationLabel = new Label();
	private Label portfolioLabel = new Label();
	private Label tituloSaldos = new Label();
	private Label TotalSaldosAbiertos = new Label();
	private Label TotalSaldosDocTercero = new Label();
	private Label TotalJournalTercero = new Label();
	private Label TotalSaldoTercero = new Label();
	
	
	private Panel parameterPanel = new Panel();
	private Panel tablePanel = new Panel();
	private Panel southPanel = new Panel();
	
	private Grid parameterLayout = GridFactory.newGridLayout();
	private Grid southLayout = GridFactory.newGridLayout();
	private Grid southresultLayout = GridFactory.newGridLayout();
	
	private WDateEditor dateField = new WDateEditor();
	private WSearchEditor bpartnerSearch = null;
	private WTableDirEditor organizationPick;
	
	private Button zoomButton = new Button();
	private Button searchButton = new Button();
	private Button newButton = new Button();
	private Button bExport = new Button();	//Exporta excel tabla cartera
	private Button bExport2 = new Button();	//Exporta excel tabla detalle cartera
	private Button bExport3 = new Button();	//Exporta excel tabla journal
	
	private Checkbox issotrxcheck = new Checkbox();
	
	private Hlayout statusBar = new Hlayout();
	private WListbox portfolioTable = ListboxFactory.newDataTable();
	private WListbox table = ListboxFactory.newDataTable();
	private WListbox tableJournal = ListboxFactory.newDataTable();

	private Tabbox tabbedPane = new Tabbox();
	
	private Tab tabQuery = new Tab();
	private Tab tabResult = new Tab();
	private Tabs tabs = new Tabs();
	private Tabpanel result = new Tabpanel();
	private Tabpanel query = new Tabpanel();
	private Tabpanels tabpanels = new Tabpanels();
	
	public void dynInit() {
		
		// Organization filter selection
		int AD_Column_ID = COLUMN_C_PERIOD_AD_ORG_ID; //C_Period.AD_Org_ID (needed to allow org 0)
		MLookup lookupOrg = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), 0, AD_Column_ID, DisplayType.TableDir);
		organizationPick = new WTableDirEditor("AD_Org_ID", true, false, true, lookupOrg);
		organizationPick.setValue(Env.getAD_Org_ID(Env.getCtx()));
		organizationPick.addValueChangeListener(this);
		
		//  BPartner
		AD_Column_ID = COLUMN_C_INVOICE_C_BPARTNER_ID;        //  C_Invoice.C_BPartner_ID
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, AD_Column_ID, DisplayType.Search);
		bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
		bpartnerSearch.addValueChangeListener(this);

		//  Translation
		Label lbstatusini = new Label("*Seleccione un Tercero y de clic al boton Zoom para mirar el Detalle de solo ese tercero");
		lbstatusini.setStyle("color: grey; font-style: italic;");
		statusBar.appendChild(lbstatusini);
		ZKUpdateUtil.setVflex(statusBar, "min");
		
		//  Date set to Login Date
		Calendar cal = Calendar.getInstance();
		cal.setTime(Env.getContextAsDate(Env.getCtx(), "#Date"));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		dateField.setValue(new Timestamp(cal.getTimeInMillis()));
		dateField.addValueChangeListener(this);
		
		zoomButton.addActionListener(this);
		zoomButton.setEnabled(false);
		searchButton.addActionListener(this);
		newButton.addActionListener(this);
		
		tabQuery.addEventListener(Events.ON_SELECT, this);
		tabQuery.setLabel("Informe x Tercero");
		tabResult.addEventListener(Events.ON_SELECT, this);
		tabResult.setLabel("Detallado x Documento");
		
		issotrxcheck.setLabel("Transacciones en Ventas");
		issotrxcheck.addEventListener(Events.ON_CHECK, this);
		issotrxcheck.setStyle("color:red");
		issotrxcheck.setChecked(true);
		issotrxcheck.setEnabled(true);
		
		tituloSaldos.setStyle("color:blue; font-size: 14px; text-shadow: 1px 1px grey; text-align:right;");
		tituloSaldos.setText("Total Saldos Abiertos:  $ ");
		tituloSaldos.setVisible(false);
		TotalSaldosAbiertos.setStyle("color:blue; font-size: 14px; text-shadow: 1px 1px grey;");
		TotalSaldosAbiertos.setText(" ");
		TotalSaldosAbiertos.setVisible(false);
		
		
		bExport.addEventListener(Events.ON_CLICK, this);
		bExport2.addEventListener(Events.ON_CLICK, this);
		bExport3.addEventListener(Events.ON_CLICK, this);
		//bExport.setVisible(true);

		
	}
	
	private void zkInit() {
		
		//Tab 
		Borderlayout layout = new Borderlayout();
		
		tabs.appendChild(tabQuery);
		tabs.appendChild(tabResult);

		ZKUpdateUtil.setHflex(tabpanels, "1");
		tabpanels.appendChild(query);
		tabpanels.appendChild(result);

		ZKUpdateUtil.setHflex(tabbedPane, "1");
		ZKUpdateUtil.setVflex(tabbedPane, "1");
		tabbedPane.appendChild(tabs);
		tabbedPane.appendChild(tabpanels);
		
		Center centertab = new Center();
		centertab.setParent(layout);
		centertab.setStyle("background-color: transparent; padding: 2px;");
		tabbedPane.setParent(centertab);
		ZKUpdateUtil.setHflex(tabbedPane, "1");
		ZKUpdateUtil.setVflex(tabbedPane, "1");
		
		form.appendChild(layout);
		
		//-----Tab Result
		// Result Tab
		layautTabResult();		
		
		//-----Tab Query
		Div div = new Div();
		div.setStyle("height: 100%; width: 100%; overflow: auto;");
		div.appendChild(mainLayout);
		//form.appendChild(div);
		query.appendChild(div);
		ZKUpdateUtil.setWidth(mainLayout, "100%");
		mainLayout.setStyle("min-height: 400px");
		
		dateLabel.setText(Msg.getMsg(Env.getCtx(), "Date"));
		bpartnerLabel.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		organizationLabel.setText(Msg.translate(Env.getCtx(), "AD_Org_ID"));
		portfolioLabel.setText(" Saldos Abiertos por Tercero");
		zoomButton.setImage(ThemeManager.getThemeResource("images/Zoom16.png"));
		zoomButton.setTooltiptext(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Zoom")));
		searchButton.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Search")));
		newButton.setLabel("Limpiar Consulta");
		bExport.setImage(ThemeManager.getThemeResource("images/Export16.png"));
		bExport.setTooltiptext(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Export")));
		bExport2.setImage(ThemeManager.getThemeResource("images/Export16.png"));
		bExport2.setTooltiptext(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Export")));
		bExport3.setImage(ThemeManager.getThemeResource("images/Export16.png"));
		bExport3.setTooltiptext(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Export")));

		
		parameterPanel.appendChild(parameterLayout);
		tablePanel.appendChild(tableLayout);
		
		// parameters layout
		North north = new North();
		north.setBorder("none");
		north.setSplittable(true);
		north.setCollapsible(true);
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		layoutParameterAndSummary();
		
		// tab query layout
		tablePanel.appendChild(tableLayout);
		ZKUpdateUtil.setWidth(tablePanel, "100%");
		ZKUpdateUtil.setWidth(tableLayout, "100%");
		ZKUpdateUtil.setVflex(tablePanel, "1");
		ZKUpdateUtil.setVflex(tableLayout, "1");
		
		// tab query  layout north - label
		north = new North();
		north.setBorder("none");
		tableLayout.appendChild(north);
		north.appendChild(portfolioLabel);
		ZKUpdateUtil.setVflex(portfolioLabel, "min");
		Center center = new Center();
		tableLayout.appendChild(center);
		center.appendChild(portfolioTable);
		ZKUpdateUtil.setWidth(portfolioTable, "100%");
		ZKUpdateUtil.setVflex(portfolioTable, "1");
		center.setBorder("none");
		
		// mainlayout center - payment + invoice 
		center = new Center();
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		ZKUpdateUtil.setHflex(infoPanel, "1");
		ZKUpdateUtil.setVflex(infoPanel, "1");
		
		infoPanel.setStyle("border: none");
		ZKUpdateUtil.setWidth(infoPanel, "100%");
		
		// north of mainlayout center - payment
		north = new North();
		north.setBorder("none");
		infoPanel.appendChild(north);
		north.appendChild(tablePanel);
		north.setAutoscroll(true);
		north.setSplittable(true);
		north.setSize("95%");
		north.setCollapsible(true);


		infoPanel.setStyle("min-height: 280px;");
	}
	
	protected void layautTabResult(){
		
		// Result Tab
		Borderlayout resultPanel = new Borderlayout();
		Borderlayout doclayout = new Borderlayout();
		Borderlayout journallayout = new Borderlayout();
		Panel paneldoc = new Panel();
		Panel paneljournal = new Panel();
		
		Div div = new Div();
		div.setStyle("height: 100%; width: 100%; overflow: auto;");
		div.appendChild(resultPanel);
		result.appendChild(div);
		ZKUpdateUtil.setWidth(resultPanel, "100%");
		//resultPanel.setStyle("min-height: 600px");
		
		
		// document layout
		paneldoc.appendChild(doclayout);
		ZKUpdateUtil.setWidth(paneldoc, "100%");
		ZKUpdateUtil.setWidth(doclayout, "100%");
		ZKUpdateUtil.setVflex(paneldoc, "1");
		ZKUpdateUtil.setVflex(doclayout, "1");
		
		// journal layout
		paneljournal.appendChild(journallayout);
		ZKUpdateUtil.setWidth(paneljournal, "100%");
		ZKUpdateUtil.setWidth(journallayout, "100%");
		ZKUpdateUtil.setVflex(paneljournal, "1");
		ZKUpdateUtil.setVflex(journallayout, "1");
		//journallayout.setStyle("min-height: 100px;");
		
		//resultPanel.setStyle("position: absolute");
		ZKUpdateUtil.setWidth(resultPanel, "99%");
		ZKUpdateUtil.setHeight(resultPanel, "99%");		
		
		North docnorth = new North();
		resultPanel.appendChild(docnorth);
		docnorth.appendChild(paneldoc);
		docnorth.setAutoscroll(true);
		docnorth.setSplittable(true);
		docnorth.setSize("50%");
		docnorth.setCollapsible(true);
		
		Center journalCenter = new Center();
		resultPanel.appendChild(journalCenter);
		journalCenter.appendChild(paneljournal);
		journalCenter.setAutoscroll(true);
		ZKUpdateUtil.setVflex(journalCenter, "1");
		journalCenter.setBorder("none");
		
		South southresult = new South();
		resultPanel.appendChild(southresult);
		Panel southresultpanel = new Panel();
		southresult.appendChild(southresultpanel);	
		southresultpanel.appendChild(southresultLayout);
		Rows rowsSouth = southresultLayout.newRows();
		Row rowsouth = rowsSouth.newRow();
		Label tituloSaldo = new Label("Saldo Documentos: $");
		tituloSaldo.setStyle("text-align:right; font-weight: bold;");
		rowsouth.appendCellChild(tituloSaldo);
		rowsouth.appendCellChild(TotalSaldosDocTercero);
		Label tituloJournal = new Label("Saldo Notas Contables: $");
		tituloJournal.setStyle("text-align:right; font-weight: bold;");
		rowsouth.appendCellChild(tituloJournal);
		rowsouth.appendCellChild(TotalJournalTercero);
		Label tituloTotal = new Label("Total Saldos: $");
		tituloTotal.setStyle("text-align:right; font-weight: bold;");
		rowsouth.appendCellChild(tituloTotal);
		rowsouth.appendCellChild(TotalSaldoTercero,2);
		
		//table document
		paneldoc.appendChild(doclayout);
		North doclabelNorth = new North();
		doclayout.appendChild(doclabelNorth);
		Label docLabel = new Label("Documentos");
		doclabelNorth.appendChild(docLabel);
		ZKUpdateUtil.setVflex(docLabel, "min");
		
		Center docresultCenter = new Center();
		doclayout.appendChild(docresultCenter);
		docresultCenter.appendChild(table);
		ZKUpdateUtil.setWidth(table, "100%");
		ZKUpdateUtil.setVflex(table, "1");
		docresultCenter.setBorder("none");
		
		South docresultSouth = new South();
		doclayout.appendChild(docresultSouth);
		
		Hbox boxdocresult = new Hbox();
		boxdocresult.setPack("end");
		boxdocresult.appendChild(bExport2);
		ZKUpdateUtil.setHflex(boxdocresult, "1");
		docresultSouth.appendChild(boxdocresult);

		
		//table journal
		paneljournal.appendChild(journallayout);
		North jlabelNorth = new North();
		journallayout.appendChild(jlabelNorth);
		Label journalLabel = new Label("Notas Contables que NO AFECTA Saldos de Documentos");
		journalLabel.setStyle("color:red");
		jlabelNorth.appendChild(journalLabel);
		ZKUpdateUtil.setVflex(journalLabel, "min");
		
		Center jresultCenter = new Center();
		journallayout.appendChild(jresultCenter);
		jresultCenter.appendChild(tableJournal);
		ZKUpdateUtil.setWidth(tableJournal, "100%");
		ZKUpdateUtil.setVflex(tableJournal, "1");
		jresultCenter.setBorder("none");
		
		South southt = new South();
		journallayout.appendChild(southt);
		//Hbox divbox = new Hbox();
		//divbox.setHeight("10px");
		//southt.appendChild(divbox);
		Hbox box = new Hbox();
		box.setPack("end");
		box.appendChild(bExport3);
		ZKUpdateUtil.setHflex(box, "1");
		southt.appendChild(box);
		
		
				
	}
	
	protected void layoutParameterAndSummary() {
		
		Rows rows = null;
		Row row = null;
		
		setupParameterColumns();
		
		rows = parameterLayout.newRows();
		row = rows.newRow();
		
		row.appendCellChild(dateLabel.rightAlign());
		row.appendCellChild(dateField.getComponent(),1);
		row.appendCellChild(bpartnerLabel.rightAlign());
		ZKUpdateUtil.setHflex(bpartnerSearch.getComponent(), "true");
		row.appendCellChild(bpartnerSearch.getComponent(),2);
		bpartnerSearch.showMenu();
		
		row.appendCellChild(organizationLabel.rightAlign());
		ZKUpdateUtil.setHflex(organizationPick.getComponent(), "true");
		row.appendCellChild(organizationPick.getComponent(),1);
		organizationPick.showMenu();
		row = rows.newRow();
		row.appendCellChild(new Label(""));
		row.appendCellChild(searchButton);
		
		row.appendCellChild(new Label(""));
		row.appendCellChild(issotrxcheck);
		row.appendCellChild(new Label(""));
		row.appendCellChild(new Label(""));
		row.appendCellChild(newButton);
		
		South south = new South();
		south.setBorder("none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		southPanel.appendChild(southLayout);
		southPanel.appendChild(statusBar);
		ZKUpdateUtil.setWidth(southLayout, "100%");
		ZKUpdateUtil.setHflex(southPanel, "1");
		ZKUpdateUtil.setVflex(southPanel, "min");
		ZKUpdateUtil.setVflex(southLayout, "min");
		ZKUpdateUtil.setVflex(statusBar, "min");
		ZKUpdateUtil.setVflex(south, "min");
		rows = southLayout.newRows();
		row = rows.newRow();
		row.appendCellChild(tituloSaldos,3);
		row.appendCellChild(TotalSaldosAbiertos);
		Hbox box = new Hbox();
		box.setPack("end");
		box.appendChild(zoomButton);
		box.appendChild(bExport);
		ZKUpdateUtil.setHflex(box, "1");
		row.appendCellChild(box, 2);
		
	}
	
	protected void setupParameterColumns() {
		noOfColumn = 6;
		if (maxWidth(MEDIUM_WIDTH-1))
		{
			if (maxWidth(SMALL_WIDTH-1))
				noOfColumn = 2;
			else
				noOfColumn = 4;
		}
		if (noOfColumn == 2)
		{
			Columns columns = new Columns();
			Column column = new Column();
			column.setWidth("35%");
			columns.appendChild(column);
			column = new Column();
			column.setWidth("65%");
			columns.appendChild(column);
			parameterLayout.appendChild(columns);
		}
	}
	
	private void loadTable(Vector<Vector<Object>> data, WListbox wTabel, Vector<String> columnNames) {
		
		//Vector<Vector<Object>> data = getPortfoliData( bpartner, dateField.getValue(), wTabel);
		//Vector<String> columnNames = getColumnNames();
		
		wTabel.clear();
		wTabel.getModel().removeTableModelListener(this);
		
		ListModelTable modelP = new ListModelTable(data);
		modelP.addTableModelListener(this);
		
		wTabel.setData(modelP, columnNames);
		
		//setPortfolioColumnClass(wTabel);
		
	}
	
	@Override
	public void valueChange(ValueChangeEvent e) {
	
		String name = e.getPropertyName();
		Object value = e.getNewValue();
		if (name.equals("AD_Org_ID"))
		{
			m_AD_Org_ID = ((Integer) value).intValue();
		}
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event e) throws Exception {
		log.config("");
		if(e.getTarget().equals(searchButton)) {
			limpiarinfo();
			bpartnerSearch.setReadWrite(false);
			zoomButton.setEnabled(true);
			issotrxcheck.setEnabled(false);
			dateField.setReadWrite(false);
			
			if(issotrxcheck.isChecked())
				tituloSaldos.setText("Total Saldos Abiertos CxC:  $ ");
			else
				tituloSaldos.setText("Total Saldos Abiertos CxP:  $ ");
			
			Vector<String> columnNames = getColumnNames();
			Vector<Vector<Object>> data = getPortfoliData( bpartnerSearch.getValue(), dateField.getValue(), portfolioTable);
			portfolioTable.setName("portfolio");
			loadTable(data, portfolioTable, columnNames);
			setPortfolioColumnClass(portfolioTable);
			portfolioTable.autoSize();
			fixWidthColumns(portfolioTable);
			if(portfolioTable.getRowCount() > 0)
				totaliceSaldos();
			zoominfo();
		}
		else if(e.getTarget().equals(zoomButton)) {
			if(portfolioTable.getItemCount()>0) 
				zoominfo();
			tabbedPane.setSelectedIndex(1);
		}
		else if(e.getTarget().equals(newButton)) {
			//bpartnerSearch.setValue(null);
			limpiarinfo();
			
		}
		else if(e.getTarget().equals(bExport)) {
			if(portfolioTable.getRowCount()>0) {
				actionExport(portfolioTable);
			}
		}
		else if(e.getTarget().equals(bExport2)) {
			if(table.getRowCount()>0) {
				actionExport(table);
			}
		}
		else if(e.getTarget().equals(bExport3)) {
			if(tableJournal.getRowCount()>0) {
				actionExport(tableJournal);
			}
		}
		else if(e.getTarget().equals(issotrxcheck)) {
			m_isSoTrx = issotrxcheck.isChecked();
		}
		
	}
	
	private void zoominfo() {
		int indexSelec = -1;	
		
		if(portfolioTable.getSelectedIndex()>=0)
			indexSelec = portfolioTable.getSelectedIndex();
		loadTablePortfolioDetail(indexSelec);
		loadTableJournal(indexSelec);
		totaliceSaldosResult();
		
	}
	private void limpiarinfo() {
		issotrxcheck.setEnabled(true);
		dateField.setReadWrite(true);
		portfolioTable.clear();
		table.clear();
		tableJournal.clear();
		tituloSaldos.setVisible(false);
		TotalSaldosAbiertos.setText(" ");
		TotalSaldosDocTercero.setText(" ");
		TotalJournalTercero.setText(" ");
		TotalSaldoTercero.setText(" ");
		bpartnerSearch.setReadWrite(true);
		zoomButton.setEnabled(false);
	}
	
	private void loadTablePortfolioDetail(int indexSelec) {
		
		Object bpartner = bpartnerSearch.getValue();
	
		if(indexSelec>=0) {
			KeyNamePair idpartner = (KeyNamePair)portfolioTable.getValueAt(indexSelec, 1);
			bpartner = (Object)idpartner.getKey();
		}
		Vector<String> columnNames = getColumnNamesDetail();
		Vector<Vector<Object>> data = getPortfoliDataDetail( bpartner, dateField.getValue(), table);
		table.setName("portfolioDetail");
		loadTable(data, table, columnNames);
		setPortfolioDetailColumnClass(table);
		table.autoSize();
		fixWidthColumns(table);
	}
	
	private void loadTableJournal(int indexSelec) {
		
		Object bpartner = bpartnerSearch.getValue();
		if(indexSelec>=0) {
			KeyNamePair idpartner = (KeyNamePair)portfolioTable.getValueAt(indexSelec, 1);
			bpartner = (Object)idpartner.getKey();
		}
		Vector<String> columnNames = getColumnNamesJournal();
		Vector<Vector<Object>> data = getDataJournal( bpartner, dateField.getValue(), tableJournal);
		tableJournal.setName("journalTable");
		loadTable(data, tableJournal, columnNames);
		setTableJournal(tableJournal);
		tableJournal.autoSize();
		fixWidthColumns(tableJournal);
	}
	
	private void totaliceSaldos() {
		
		DecimalFormat formato = new DecimalFormat("#,###.##");
		int totalrow = portfolioTable.getRowCount();
		BigDecimal totalsaldo = BigDecimal.ZERO;
		for(int i=0;i<totalrow; i++) {
			totalsaldo= totalsaldo.add((BigDecimal)portfolioTable.getValueAt(i, 3));
		}
		
		TotalSaldosAbiertos.setText(formato.format(totalsaldo));
		tituloSaldos.setVisible(true);
		TotalSaldosAbiertos.setVisible(true);
		
	}
	
	private void totaliceSaldosResult() {
		
		DecimalFormat formato = new DecimalFormat("#,###.##");
		BigDecimal totalDr = BigDecimal.ZERO;
		BigDecimal totalCr = BigDecimal.ZERO;
		BigDecimal totalAccount = BigDecimal.ZERO;
		BigDecimal totalDoc = BigDecimal.ZERO;
		
		int totalrowjournal = tableJournal.getRowCount();
		for(int i=0;i<totalrowjournal; i++) {
			totalDr= totalDr.add((BigDecimal)tableJournal.getValueAt(i, 6));
			totalCr= totalCr.add((BigDecimal)tableJournal.getValueAt(i, 7));
		}
		totalAccount = totalDr.subtract(totalCr);
		if(portfolioTable.getSelectedIndex()>=0)
			totalDoc= totalDoc.add((BigDecimal)portfolioTable.getValueAt(portfolioTable.getSelectedIndex(), 3));
		else {
			int size = portfolioTable.getRowCount();
			for(int i=0;i<size; i++) {
				totalDoc= totalDoc.add((BigDecimal)portfolioTable.getValueAt(i, 3));
			}
		}
		//if(!issotrxcheck.isChecked())
			//totalDoc=totalDoc.negate();
			
		TotalSaldosDocTercero.setText(formato.format(totalDoc));
		TotalJournalTercero.setText(formato.format(totalAccount));
		TotalSaldoTercero.setText(formato.format(totalDoc.add(totalAccount)));
		
	}

	@Override
	public ADForm getForm() {
		return form;
	}
	
	private void fixWidthColumns(WListbox table) {
		int i=0;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(i<2)
				ZKUpdateUtil.setHflex(header, "min");
			else
				ZKUpdateUtil.setHflex(header, "max");
			i++;
		}
	}
	
	private void actionExport(WListbox mTable) {
		
		XSSFWorkbook workbook = createFileExcel(mTable);		
		if (workbook != null && workbook.getNumberOfSheets()>0) {

			File file;
			try {
				file = new File(FileUtil.getTempMailName( TITLE, ".xlsx"));
				workbook.write(new FileOutputStream(file));
				Filedownload.save(file, "application/vnd.ms-excel");
			} catch (Exception e) {
				throw new RuntimeException(e);
			}			
		}
		
	}

}
