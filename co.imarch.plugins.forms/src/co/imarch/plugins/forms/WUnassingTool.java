package co.imarch.plugins.forms;

import static org.compiere.model.SystemIDs.COLUMN_C_INVOICE_C_BPARTNER_ID;

import java.sql.Timestamp;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.util.ProcessUtil;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.DocumentLink;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListCell;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.factory.ButtonFactory;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MMovement;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MProcess;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfo;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.TrxRunnable;
import org.compiere.util.Util;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;




public class WUnassingTool extends UnassingTool 
	implements IFormController, EventListener<Event>, WTableModelListener, ValueChangeListener
{

	public static final String JASPER_STARTER_CLASS = "org.adempiere.report.jasper.ReportStarter";
	public static final String A_PRINT = "Print";
	
	private CustomForm form = new CustomForm();
	
	private Panel southPanel = new Panel();
	private Hlayout statusBar = new Hlayout();
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Panel assignedtoolPanel = new Panel();
	private Panel toolPanel = new Panel();
	private Borderlayout toolLayout = new Borderlayout();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private Label bpartnerLabel = new Label();
	private Label titleLabel = new Label();
	private WSearchEditor bpartnerSearch = null;
	private WListbox assignedtoolTable = ListboxFactory.newDataTable();
	private Button processButton = new Button();
	private Button refreshButton = new Button();
	private Button printButton = ButtonFactory.createNamedButton(A_PRINT, false, true);
	
	private Hlayout pnlBtnRight;
    private Hlayout pnlBtnLeft;
	
	private Checkbox allToolCheckbox = new Checkbox();
	
	private String Msg_uno = "Ingrese el Mecanico para mostrar relacion de Herramientas";
	private String Msg_dos = "Seleccione la Herramienta para editar cantidad asignada";

	private Boolean editingTable = false;
	
	public WUnassingTool()
	{
		Env.setContext(Env.getCtx(), form.getWindowNo(), "IsSoTrx", "Y");
		
		try
		{
			super.dynInit();
			dynInit();
			zkInit();
			southPanel.appendChild(new Separator());
			southPanel.appendChild(statusBar);
			form.addEventListener(Events.ON_OPEN, this);
			AuFocus auf = new AuFocus(bpartnerSearch.getComponent());
			Clients.response(auf);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
	}
	
	public void dynInit() throws Exception
	{
		//  BPartner
		int AD_Column_ID = COLUMN_C_INVOICE_C_BPARTNER_ID;        //  C_Invoice.C_BPartner_ID
		int AD_Reference_ID = 252; 		//C_BPartner Employee
		
		MLookup lookupBP = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), 
				AD_Column_ID, DisplayType.Search, Env.getLanguage(Env.getCtx()), "C_BPartner_ID", AD_Reference_ID,
				false, "C_BPartner.smj_ismechanic='Y'");
		
		bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
		bpartnerSearch.addValueChangeListener(this);
		bpartnerSearch.getComponent().setFocus(true);
		
		statusBar.appendChild(new Label(Msg_uno));
		statusBar.setVflex("min");
		
		allToolCheckbox.setChecked(false);
		allToolCheckbox.setEnabled(false);
		refreshButton.setEnabled(false);
		processButton.setEnabled(false);
		printButton.setEnabled(false);
		
	}
	
	public void zkInit()
	{
		form = new CustomForm();
		form.setStyle("");
		//form.setWidth("100%");
		//form.setHeight("100%");
		//form.setHflex("1");
		//form.setVflex("1");
		ZKUpdateUtil.setWidth(form, "100%");
		ZKUpdateUtil.setHeight(form, "100%");
		ZKUpdateUtil.setHflex(form, "1");
		ZKUpdateUtil.setVflex(form, "1");
		
		form.appendChild(mainLayout);
		//mainLayout.setWidth("99%");
		//mainLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		
		
		bpartnerLabel.setText("Mecanico");
		allToolCheckbox.setText(Msg.translate(Env.getCtx(),"SelectAll"));
		allToolCheckbox.addActionListener(this);
		titleLabel.setText("HERRAMIENTA TALLER ASIGNADA");
		titleLabel.setStyle("color: blue; font-weight:bold; font-size:150%; text-align:center;"); 
		
		refreshButton.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Refresh")));
		refreshButton.addActionListener(this);
		refreshButton.setAutodisable("self");

		//printButton.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Print")));
		printButton.setImage(ThemeManager.getThemeResource("images/Print24.png"));
		printButton.addActionListener(this);
		
		processButton.setLabel(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Process")));
		processButton.addActionListener(this);
		
		assignedtoolTable.setFocus(true);
		assignedtoolTable.addEventListener(Events.ON_CLICK, this);
		
		pnlBtnLeft = new Hlayout();
        pnlBtnLeft.setSclass("confirm-panel-left");
        pnlBtnRight = new Hlayout();
        pnlBtnRight.setSclass("confirm-panel-right");
		
		
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = null;
		Row row = null;
		
		Hbox boxtitle = new Hbox();
	 	boxtitle = new Hbox();
	 	//boxtitle.setWidth("100%");
	 	ZKUpdateUtil.setWidth(boxtitle, "100%");
	 	boxtitle.setPack("center");
	 	boxtitle.appendChild(titleLabel);
	 	Div d = new Div();
	 	parameterPanel.appendChild(boxtitle);
	 	parameterPanel.appendChild(d);
	 		 	
	 	parameterPanel.appendChild(parameterLayout);
	 	//parameterLayout.setWidth("90%");
	 	ZKUpdateUtil.setWidth(parameterLayout, "90%");
		rows = parameterLayout.newRows();
		row = rows.newRow();
		row.appendCellChild(new Label(""));
		row = rows.newRow();
		row.appendCellChild(bpartnerLabel.rightAlign());
		//bpartnerSearch.getComponent().setHflex("true");
		ZKUpdateUtil.setHflex(bpartnerSearch.getComponent(), "true");
		row.appendCellChild(bpartnerSearch.getComponent(),3);
		bpartnerSearch.showMenu();
		
		row.appendCellChild(refreshButton);
		
		South south = new South();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		south.appendChild(southPanel);
		southPanel.appendChild(assignedtoolPanel);
		
		Div dvfoot = new Div();
		//dvfoot.setWidth("99%");
		//dvfoot.setVflex("min");
		ZKUpdateUtil.setWidth(dvfoot, "99%");
		ZKUpdateUtil.setVflex(dvfoot, "min");
		dvfoot.setStyle("border: none;");
		Div dvbtnleft = new Div();
		//dvbtnleft.setWidth("50%");
		//dvbtnleft.setVflex("min");
		ZKUpdateUtil.setWidth(dvbtnleft, "50%");
		ZKUpdateUtil.setVflex(dvbtnleft, "min");
		dvbtnleft.setStyle("border: none; float:left;");
		Div dvbtnright = new Div();
		//dvbtnright.setWidth("49%");
		//dvbtnright.setVflex("min");
		ZKUpdateUtil.setWidth(dvbtnright, "49%");
		ZKUpdateUtil.setVflex(dvbtnright, "min");
		dvbtnright.setStyle("border: none; float:right; padding-right:10px;");
		Hbox boxBtnleft = new Hbox();
		//boxBtnleft.setWidth("100%");
		ZKUpdateUtil.setWidth(boxBtnleft, "100%");
		boxBtnleft.setPack("right");
		boxBtnleft.appendChild(processButton);
		dvbtnright.appendChild(boxBtnleft);
		Div dv = new Div();
		//dv.setWidth("100%");
		//dv.setVflex("min");
		ZKUpdateUtil.setWidth(dv, "100%");
		ZKUpdateUtil.setVflex(dv, "min");
		dv.setStyle("border: none; padding-left:10px;");
		dv.appendChild(printButton);
		dvfoot.appendChild(dvbtnleft);
		dvfoot.appendChild(dvbtnright);
		dvfoot.appendChild(dv);
		assignedtoolPanel.appendChild(dvfoot);
		
		toolPanel.appendChild(toolLayout);
		//toolPanel.setWidth("100%");
		//toolPanel.setHeight("100%");
		ZKUpdateUtil.setWidth(toolPanel, "100%");
		ZKUpdateUtil.setHeight(toolPanel, "100%");
		//toolLayout.setWidth("100%");
		//toolLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(toolLayout, "100%");
		ZKUpdateUtil.setHeight(toolLayout, "100%");
		toolLayout.setStyle("border: none");
		
		// seccion de la tabla
		north = new North();
		north.setStyle("border: none");
		toolLayout.appendChild(north);
		north.appendChild(allToolCheckbox); //imarch
		south = new South();
		south.setStyle("border: none");
		toolLayout.appendChild(south);
		south.appendChild(new Label("."));
		Center center = new Center();
		toolLayout.appendChild(center);
		center.appendChild(assignedtoolTable);
		//toolLayout.setWidth("99%");
		//toolLayout.setHeight("99%");
		ZKUpdateUtil.setWidth(toolLayout, "99%");
		ZKUpdateUtil.setHeight(toolLayout, "99%");
		center.setStyle("border: none");
		//---------------------------
		
		center = new Center();
		mainLayout.appendChild(center);
		center.appendChild(toolPanel);
		//toolPanel.setHflex("1");
		//toolPanel.setVflex("1");
		ZKUpdateUtil.setHflex(toolPanel, "1");
		ZKUpdateUtil.setVflex(toolPanel, "1");
		
	}
	@Override
	public void valueChange(ValueChangeEvent evt) {
		
		String name = evt.getPropertyName();
		Object value = evt.getNewValue();
		
		if(log.isLoggable(Level.CONFIG)) log.config("name:"+name+" value:"+value);
		
		if(value == null)
			return;
		
		if(name.equals("C_BPartner_ID"))
		{
			bpartnerSearch.setValue(value);
			m_C_BPartner_ID = ((Integer)value).intValue();
			allToolCheckbox.setChecked(false);
			processButton.setEnabled(false);
			loadAssignedTool();
			if(assignedtoolTable.getItems().size()>0){
				allToolCheckbox.setEnabled(true);
				refreshButton.setEnabled(true);
				processButton.setEnabled(true);
				printButton.setEnabled(true);
			}
			statusBar.getChildren().clear();
			statusBar.appendChild(new Label(Msg_dos));
		}
		
	}
	
	public void loadAssignedTool()
	{
		boolean selectall = allToolCheckbox.isChecked();
		Vector<Vector<Object>> datatool = getAssignedToolData(selectall);
		Vector<String> columntool = getAsignToolColumnName();
		
		assignedtoolTable.clear();
		assignedtoolTable.getModel().removeTableModelListener(this);
		
		ListModelTable MTable = new ListModelTable(datatool);
		MTable.addTableModelListener(this);
				
		assignedtoolTable.setData(MTable, columntool);
		
		setAsigToolColumnNameClass(assignedtoolTable);
		
		fixWidthColumns(assignedtoolTable);
				
	}
	
	/**
	 * autor: SmartJSP
	 * @param table
	 */
	private void fixWidthColumns(WListbox table) {
		int i=0;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(i==0 || i==2)
				ZKUpdateUtil.setHflex(header, "min");
			else
				ZKUpdateUtil.setHflex(header, "max");
			i++;
		}
	}

	@Override
	public void tableChanged(WTableModelEvent event) {

		//iMarch
		boolean isUpdate = (event.getType() == WTableModelEvent.CONTENTS_CHANGED);
		//  Not a table update
		if (!isUpdate) return;
		
		int fila = event.getFirstRow();
		int columna = event.getColumn();
		//if not select row
		if(fila < 0 || editingTable) return; 
		//if action is in check selected

		if(columna==0)
		{
			editingTable=true;
			try{
				editTable(fila,columna);
				focusRowTable(fila);
			}
			catch (Exception e){
				log.log(Level.SEVERE, "Error Process edit or Focus GridTabel", e);
				editingTable=false;
			}
			editingTable=false;
		}
		//if action is in otrher column		
		if(columna > 0)
		{
			editingTable=true;
			try{
				editTable(fila,columna);
			}
			catch(Exception e){
				log.log(Level.SEVERE, "Error Process edit GridTabel", e);
				editingTable=false;
			}
			editingTable=false;
		}
	
	}
	
	public void focusRowTable(int fila){
		
		if(!((Boolean)assignedtoolTable.getValueAt(fila, 0)).booleanValue())
			return;
		
		assignedtoolTable.repaint();
		assignedtoolTable.renderAll();
				
		ListItem listitem = new ListItem();
		listitem = assignedtoolTable.getItemAtIndex(fila);
		assignedtoolTable.renderItem(listitem);
		listitem = (ListItem)assignedtoolTable.getItems().get(fila);
		
		ListCell listCell = (ListCell)listitem.getChildren().get(3);
		((NumberBox) listCell.getFirstChild()).setFocus(true);
		((NumberBox) listCell.getFirstChild()).getDecimalbox().select();
		
		
	}
	/**
	 * Edicion de la tabla
	 * @param fila
	 * @param columna
	 */
	public void editTable(int fila, int columna){
		
		
		String msg = writeOff(fila, columna, assignedtoolTable);
		ListModelTable modelt = assignedtoolTable.getModel();
		
		modelt.updateComponent(fila);
		modelt = new ListModelTable();
		assignedtoolTable.repaint();
		
		if(columna == 0 && allToolCheckbox.isChecked()) allToolCheckbox.setChecked(false);
		
		activeProcessButton();
		
		if(msg!= null && msg.length() > 0)
			FDialog.warn(form.getWindowNo(), "class writeOff");
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {

		Object accion = arg0.getTarget();
		if(log.isLoggable(Level.CONFIG)) log.config("target:"+accion);
		
		if(accion.equals(refreshButton))
		{
			allToolCheckbox.setChecked(false);
			processButton.setEnabled(false);
			statusBar.getChildren().clear();
			loadAssignedTool();
			if(assignedtoolTable.getRowCount() > 0)
				statusBar.appendChild(new Label(Msg_dos));
			else
				statusBar.appendChild(new Label(Msg_uno));
		}
		else if(accion.equals(allToolCheckbox))
		{
			if(m_selected == 0 || m_selected == assignedtoolTable.getRowCount() || allToolCheckbox.isChecked())
				loadAssignedTool();
			activeProcessButton();
		}
		else if(accion.equals(processButton))
		{

				Messagebox.show("Desea desaignar la Herramienta?", "Procesar",
				        Messagebox.OK | Messagebox.CANCEL,
				        Messagebox.QUESTION,
				            new EventListener<Event>(){
				                public void onEvent(Event e){
				                    if("onOK".equals(e.getName())){
				                    	saveunassigtollData();
				                    }else if("onCancel".equals(e.getName())){
				                    	return;
				                    }                    
				                }
				            }
				        );
			
		}
		else if(Events.ON_CLICK.equals(arg0.getName()) && accion instanceof WListbox)
		{
			int fila = assignedtoolTable.getSelectedIndex();
			if(fila>=0)	focusRowTable(fila);	
		}
		else if(accion.equals(printButton))
		{
			//OK to print
			onPrint();
		}
	}

	public void onPrint() 
	{
		
		String info = "";
		int C_BPartner_ID = m_C_BPartner_ID; //Mecanico;
		int AD_Process_ID = m_AD_Process_ID;
		int M_Locator_ID = m_M_locator_id;	//bodega asignacion de herramienta
		PO proceso = new MProcess(Env.getCtx(), AD_Process_ID, null);
		ProcessInfo pi = new ProcessInfo("Reporte Asignacion de Herramienta Taller", proceso.get_ID());
		MPInstance instance = new MPInstance(Env.getCtx(), proceso.get_ID(), 0);
		if (!instance.save())
		{
			info = Msg.getMsg(Env.getCtx(), "ProcessNoInstance");
			log.log(Level.WARNING, info);
			return ;
		}
		pi.setRecord_ID(0);
		pi.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		pi.setClassName(JASPER_STARTER_CLASS);
		
		//----Parameter Locator
		MPInstancePara parametro = new MPInstancePara(instance, 0);
		parametro.setParameterName("M_Locator_ID");	//Name of Column DB in  Process
		parametro.setP_Number(M_Locator_ID);	//ID or Value of parameter
		parametro.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		if (!parametro.save())
		{
			log.log(Level.SEVERE, "No se pudo Guardar datos del Parametro M_Locator_ID. (AD_PInstance_Para)");
			return ;
		}
		//----Parameter Bpartner
		parametro = new MPInstancePara(instance, 1);
		parametro.setParameterName("C_BPartner_ID");	//Name of Column DB in  Process
		parametro.setP_Number(C_BPartner_ID);	//ID or Value of parameter
		parametro.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		if (!parametro.save())
		{
			log.log(Level.SEVERE, "No se pudo Guardar datos del Parametro C_BPartner_ID. (AD_PInstance_Para)");
			return ;
		}
		
		if(!ProcessUtil.startJavaProcess(Env.getCtx(), pi, null))
			log.log(Level.SEVERE, "Error en Proceso Generacion PDF Jasper");
		
	}
	@Override
	public ADForm getForm() {

		return form;
	}

	public void activeProcessButton()
	{
		
		if(m_selected > 0)
				processButton.setEnabled(true);
		else
			processButton.setEnabled(false);
		
	}
	
	public void saveunassigtollData()
	{
		MovementDate = new Timestamp(System.currentTimeMillis());
		statusBar.getChildren().clear();
		
		try
		{
			final MMovement[] movement = new MMovement[1];
			Trx.run( new TrxRunnable() 
						{
							public void run(String trxName)
							{
								movement[0] = saveData(form.getWindowNo(), MovementDate, assignedtoolTable, trxName);				
							}
						}
					 );
			
			MMovement move = movement[0];
			if (move != null) 
			{
				DocumentLink link = new DocumentLink(move.getDocumentNo(), move.get_Table_ID(), move.get_ID());				
				statusBar.appendChild(new Label("Movimineto No.: "));
				statusBar.appendChild(link);
			}
		}
		catch(Exception e)
		{
			FDialog.error(form.getWindowNo(), form, "Error", e.getLocalizedMessage());
		}
		
		loadAssignedTool();
	
	}
	
}
