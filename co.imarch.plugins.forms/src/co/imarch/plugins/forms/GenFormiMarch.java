package co.imarch.plugins.forms;

import java.util.ArrayList;

import org.compiere.minigrid.IMiniTable;
import org.compiere.print.MPrintFormat;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Trx;

public abstract class GenFormiMarch {

	private boolean m_selectionActive = true;
	private String title;
	private int reportEngineType;
	private MPrintFormat printFormat = null;
	private String askPrintMsg;
	
	private Trx trx;
	private ProcessInfo pi;
	
	/** User selection */
	private ArrayList<Integer> selection = null;
	
	private Object m_AD_Org_ID;
	
	public void dynInit() throws Exception
	{
		
	}
	
	public abstract void configureMiniTable(IMiniTable miniTable);

	public abstract void saveSelection(IMiniTable miniTable);
	
	public void validate()
	{
		
	}

	public String generate()
	{
		return null;
	}
	
	public void executeQuery()
	{
		
	}
	
	public void selectAllItems() {
		
	}
	
	public void unSelectAllItems() {
		
	}

	public Trx getTrx() {
		return trx;
	}

	public void setTrx(Trx trx) {
		this.trx = trx;
	}

	public ProcessInfo getProcessInfo() {
		return pi;
	}

	public void setProcessInfo(ProcessInfo pi) {
		this.pi = pi;
	}

	public boolean isSelectionActive() {
		return m_selectionActive;
	}

	public void setSelectionActive(boolean active) {
		m_selectionActive = active;
	}

	public ArrayList<Integer> getSelection() {
		return selection;
	}

	public void setSelection(ArrayList<Integer> selection) {
		this.selection = selection;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getReportEngineType() {
		return reportEngineType;
	}

	public void setReportEngineType(int reportEngineType) {
		this.reportEngineType = reportEngineType;
	}
	
	public MPrintFormat getPrintFormat() {
		return this.printFormat;
	}

	public void setPrintFormat(MPrintFormat printFormat) {
		this.printFormat = printFormat;
	}
	
	public String getAskPrintMsg() {
		return askPrintMsg;
	}

	public void setAskPrintMsg(String askPrintMsg) {
		this.askPrintMsg = askPrintMsg;
	}
	
	public void cleanField() {
		
	}
	
	public void setOrgSelected( Object m_AD_Org_ID) {
		this.m_AD_Org_ID=m_AD_Org_ID;
	}
	
	public Object getOrgSelected() {
		return m_AD_Org_ID;
	}

}
