package co.imarch.plugins.forms;

import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;


public class WResetPasswordiMarch implements IFormController, EventListener<Event>,
WTableModelListener, ValueChangeListener{
	
	private static CLogger log = CLogger.getCLogger(WResetPasswordiMarch.class);
	
	private CustomForm form = new CustomForm();
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid gridLayout = GridFactory.newGridLayout();
	
	private Label lblUser = new Label();
	private Label lblOldPassword = new Label();
    private Label lblNewPassword = new Label();
    private Label lblRetypeNewPassword = new Label();

    private WSearchEditor fUser = null;
    private Textbox txtOldPassword;
    private Textbox txtNewPassword;
    private Textbox txtRetypeNewPassword;
    
    private ConfirmPanel confirmPanel;

    private MRole roloperator = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
    public int windowNo = 0;
    
    
	public ADForm getForm() {
		return form;
	}
    
    public WResetPasswordiMarch(){
    	
    	try {
    		dynInit();
        	zkInit();
    	}
    	catch(Exception e){
    		log.log(Level.SEVERE, "Error iniciando co.imarch.forms.WResetPasswordiMarch", e);
    	}  	
    }

    public void dynInit() throws Exception {
    	    		
//    		AD_User.AD_User_ID
    		MLookup userLkp = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, 212, DisplayType.Search);
    		fUser = new WSearchEditor("AD_User_ID", true, false, true, userLkp);
        	fUser.setValue(Env.getAD_User_ID(Env.getCtx()));
        	fUser.addValueChangeListener(this);
        	if(roloperator.get_ValueAsBoolean("IsAccessAllOrgs"))
        		fUser.setReadWrite(true);
        	else
        		fUser.setReadWrite(false);

        	txtOldPassword = new Textbox();
        	txtOldPassword.setName("txtOldPassword");
        	txtOldPassword.setStyle("-webkit-text-security: disc;");
        	txtNewPassword = new Textbox();
            txtNewPassword.setName("txtNewPassword");
            txtNewPassword.setStyle("-webkit-text-security: disc;");
            txtRetypeNewPassword = new Textbox();
            txtRetypeNewPassword.setName("txtRetypeNewPassword");
            txtRetypeNewPassword.setStyle("-webkit-text-security: disc;");
            
            lblUser.setText(Msg.translate(Env.getCtx(), "AD_User_ID"));
        	lblOldPassword.setText(Msg.getMsg(Env.getCtx(), "Old Password"));        	
        	lblNewPassword.setText(Msg.getMsg(Env.getCtx(), "New Password"));
        	lblRetypeNewPassword.setText(Msg.getMsg(Env.getCtx(), "New Password Confirm"));
        	        	
        	confirmPanel = new ConfirmPanel(false, false, false, false, false, false, false);
        	confirmPanel.addActionListener(this);
        	
        	
    }
    
    public void zkInit(){
    	
    	form.appendChild(mainLayout);
    	//mainLayout.setWidth("99%");
		//mainLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		
		North north = new North();
		mainLayout.appendChild(north);
		
		north.appendChild(parameterPanel);
		parameterPanel.appendChild(gridLayout);
		
		Rows rows = gridLayout.newRows();
		Row row = rows.newRow();

		row.appendCellChild(lblUser.rightAlign());
		row.appendCellChild(fUser.getComponent());
		row.appendCellChild(new Label("*"));
		
		row = rows.newRow();
		row.appendCellChild(lblOldPassword.rightAlign());
		row.appendCellChild(txtOldPassword);
    	
		row = rows.newRow();
		row.appendCellChild(lblNewPassword.rightAlign());
		row.appendCellChild(txtNewPassword);
		
		row = rows.newRow();
		row.appendCellChild(lblRetypeNewPassword.rightAlign());
		row.appendCellChild(txtRetypeNewPassword);
		
		South south = new South();
		mainLayout.appendChild(south);
		
		south.appendChild(confirmPanel);
			
    }

	@Override
	public void valueChange(ValueChangeEvent evt) {
		
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		
	}

	
	@SuppressWarnings("static-access")
	public void onEvent(Event arg0) throws Exception {
		
		if(arg0.getTarget().getId().equals(confirmPanel.A_OK)){
			Messagebox.show("Confirma que desea Cambiar su contraseņa","Reset Password",
					Messagebox.OK | Messagebox.CANCEL,
					Messagebox.EXCLAMATION,
					new EventListener<Event>() {
						public void onEvent(Event e){
							if("onOK".equals(e.getName())){
								validateChangePassword();
							}
							else
								return;
						}
					});
		}
					
	}
    
	
	private void validateChangePassword(){
		
		int p_AD_User_ID = -1;
		if(fUser.getValue() != null){
			p_AD_User_ID = Integer.parseInt(fUser.getValue().toString());
		}
		if(p_AD_User_ID < 0){
			Messagebox.show("EL USUARIO no es reconocido", "ERROR", Messagebox.OK , Messagebox.ERROR);
			return;
		}
		String p_OldPassword = new String(txtOldPassword.getValue());
		String p_NewPassword = new String(txtNewPassword.getValue());
		String p_NewPasswordConfirm = new String(txtRetypeNewPassword.getValue());
		
		MUser user = MUser.get(Env.getCtx(), p_AD_User_ID);
		
		if (log.isLoggable(Level.FINE)) log.fine("User=" + user);
		
		//	Do we need a password ?
		if (Util.isEmpty(p_OldPassword))		//	Password required
		{
			MUser operator = MUser.get(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()));
			if (log.isLoggable(Level.FINE)) log.fine("Operator=" + operator);
			
			if (p_AD_User_ID == 0			//	change of System
					|| p_AD_User_ID == 100		//	change of SuperUser
					|| !operator.isAdministrator())
			{
				Messagebox.show(Msg.getMsg(Env.getCtx(), "OldPasswordMandatory"), "Error", Messagebox.OK , Messagebox.ERROR);
				return;
			}
		}
		else{
			boolean hash_password = MSysConfig.getBooleanValue(MSysConfig.USER_PASSWORD_HASH, false);
			boolean diferen_password = MSysConfig.getBooleanValue(MSysConfig.CHANGE_PASSWORD_MUST_DIFFER, true);
			if (hash_password){
				if(!user.authenticateHash(p_OldPassword)){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "OldPasswordNoMatch"), "Error", Messagebox.OK , Messagebox.ERROR);
					return;
				}
			}
			else{
				if(!p_OldPassword.equals(user.getPassword())){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "OldPasswordNoMatch"), "Error", Messagebox.OK , Messagebox.ERROR);
					return;
				}
			}
			if (diferen_password){
				if(p_OldPassword.equals(p_NewPassword)){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "NewPasswordMustDiffer"), "Error", Messagebox.OK , Messagebox.ERROR);
					return;
				}
			}
		}
		
		//Verifica confirmacion de nuevo password y que NO este vacio
		if (Util.isEmpty(p_NewPasswordConfirm)){
			Messagebox.show(Msg.getMsg(Env.getCtx(), "NewPasswordConfirmMandatory"), "Error", Messagebox.OK , Messagebox.ERROR);
			return;
		}
		else{
			if (!p_NewPassword.equals(p_NewPasswordConfirm)){
				Messagebox.show(Msg.getMsg(Env.getCtx(), "PasswordNotMatch"), "Error", Messagebox.OK , Messagebox.ERROR);
				return;
			}
		}
		
		//Verifica que no este vacio el nuevo password y porcede a cambiar registro
		if (!Util.isEmpty(p_NewPassword))
			user.set_ValueOfColumn("Password", p_NewPassword);
		
		//SAVE
		try{
			user.saveEx();
		}
		catch(Exception e){
			log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			Messagebox.show(e.getLocalizedMessage(), "Error", Messagebox.OK , Messagebox.ERROR);
			user.load(user.get_TrxName());
			return;
		}
		
		clearForm();
		Messagebox.show(Msg.getMsg(Env.getCtx(), "RecordSaved"), "Cambio Generado", Messagebox.OK , Messagebox.EXCLAMATION);
		return;
		
	}//End validateChangePassword()
	
	private void clearForm(){
		txtOldPassword.setText(null);
	    txtNewPassword.setText(null);
	    txtRetypeNewPassword.setText(null);
	}
	
}
    
