/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package co.imarch.plugins.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MOrder;
import org.compiere.model.MRMA;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;


/**
 * Generate Invoice (manual) view class
 *
 */
public class WInvoiceGeniMarch extends InvoiceGeniMarch implements IFormController, EventListener<Event>, ValueChangeListener
{
	private WGenFormiMarch form;

	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WInvoiceGeniMarch.class);
	//
	private Label lOrg = new Label();
	private WTableDirEditor fOrg;
	private Label lBPartner = new Label();
	private WSearchEditor fBPartner;
	private Label     lDocType = new Label();
	private Listbox  cmbDocType = ListboxFactory.newDropdownListbox();
	private Label   lDocAction = new Label();
	private WTableDirEditor docAction;
	
	//imarch
	private Label ldoctarget = new Label();
	private WTableDirEditor fdoctarget;
	private Label   textlb = new Label();
	private Textbox textbox = new Textbox();
	private Label   placalb = new Label();
	private Textbox placabox = new Textbox();

	public WInvoiceGeniMarch()
	{
		log.info("");

		form = new WGenFormiMarch(this);
		Env.setContext(Env.getCtx(), form.getWindowNo(), "IsSOTrx", "Y");

		try
		{
			super.dynInit();
			dynInit();
			zkInit();

			m_textbox = textbox.getValue();
			m_placabox = placabox.getValue();
			//form.postQueryEvent();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "init", ex);
		}
	}	//	init

	/**
	 *	Static Init.
	 *  <pre>
	 *  selPanel (tabbed)
	 *      fOrg, fBPartner
	 *      scrollPane & miniTable
	 *  genPanel
	 *      info
	 *  </pre>
	 *  @throws Exception
	 */
	void zkInit() throws Exception
	{
		lOrg.setText(Msg.translate(Env.getCtx(), "AD_Org_ID"));
		lBPartner.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		textlb.setText("Filtar x No.Documento");
		placalb.setText("Placa");
		

		Row row = form.getParameterPanel().newRows().newRow();
		row.appendCellChild(lOrg.rightAlign());
		//fOrg.getComponent().setHflex("true");
		ZKUpdateUtil.setHflex(fOrg.getComponent(), "true");
		row.appendCellChild(fOrg.getComponent());
		//row.appendCellChild(new Space());
		row.appendCellChild(lBPartner.rightAlign());
		//fBPartner.getComponent().setHflex("true");
		ZKUpdateUtil.setHflex(fBPartner.getComponent(), "true");
		fBPartner.getComponent().setStyle("width:60px; height:10px");
		row.appendCellChild(fBPartner.getComponent());
		//row.appendCellChild(new Space());
		
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendCellChild(lDocType.rightAlign());
		//cmbDocType.setHflex("true");
		ZKUpdateUtil.setHflex(cmbDocType, "true");
		row.appendCellChild(cmbDocType);
		//row.appendCellChild(new Space());
		//imarch
		ldoctarget.setText(Msg.translate(Env.getCtx(), "C_DocTypeTarget_ID"));
		row.appendCellChild(ldoctarget.rightAlign());
		//fdoctarget.getComponent().setHflex("true");
		ZKUpdateUtil.setHflex(fdoctarget.getComponent(), "true");
		row.appendCellChild(fdoctarget.getComponent());
		//row.appendCellChild(new Space());
		
		row.appendCellChild(lDocAction.rightAlign());
		//docAction.getComponent().setHflex("true");
		ZKUpdateUtil.setHflex(docAction.getComponent(), "true");
		row.appendCellChild(docAction.getComponent());
		//row.appendCellChild(new Space());
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendCellChild(textlb.rightAlign());
		ZKUpdateUtil.setWidth(textbox, "100%");
		row.appendCellChild(textbox, 3);
		Label lbhelptextbox = new Label("Ingrese los No.Documentos separados por coma y luego clic en el boton Refrescar.");
		lbhelptextbox.setStyle("color:blue; font-style:italic; font-size: 10px; padding-top:15px"); 
		row.appendCellChild(lbhelptextbox,2);
		
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendCellChild(placalb.rightAlign());
		ZKUpdateUtil.setHflex(placabox, "true");
		row.appendCellChild(placabox);
	}	//	jbInit

	/**
	 *	Fill Picks.
	 *		Column_ID from C_Order
	 *  @throws Exception if Lookups cannot be initialized
	 */
	public void dynInit() throws Exception
	{
		MLookup orgL = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, 2163, DisplayType.TableDir);
		fOrg = new WTableDirEditor ("AD_Org_ID", false, false, true, orgL);
	//	lOrg.setText(Msg.translate(Env.getCtx(), "AD_Org_ID"));
		fOrg.setValue(Env.getAD_Org_ID(Env.getCtx()));
		fOrg.addValueChangeListener(this);
		setOrgSelected(Env.getAD_Org_ID(Env.getCtx()));
		
		//imarch
		MLookup doctargetL = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 259, 2173, DisplayType.TableDir);
		fdoctarget = new WTableDirEditor ("C_DocTypeTarget_ID", false, false, true, doctargetL);
		fdoctarget.setValue(MSysConfig.getIntValue("iMarch_DefaultStandarOrder", 0, Env.getAD_Client_ID(Env.getCtx())));
		fdoctarget.addValueChangeListener(this);
		//imarch*/
		//
		MLookup bpL = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, 2762, DisplayType.Search);
		fBPartner = new WSearchEditor ("C_BPartner_ID", false, false, true, bpL);
	//	lBPartner.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		fBPartner.addValueChangeListener(this);
		//      Document Action Prepared/ Completed
		lDocAction.setText(Msg.translate(Env.getCtx(), "DocAction"));
		MLookup docActionL = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), 3495 /* C_Invoice.DocAction */,
				DisplayType.List, Env.getLanguage(Env.getCtx()), "DocAction", 135 /* _Document Action */,
				false, "AD_Ref_List.Value IN ('CO','PR')");
		docAction = new WTableDirEditor("DocAction", true, false, true,docActionL);
		docAction.setValue(DocAction.ACTION_Prepare);
		// docAction.addValueChangeListener(this); // IDEMPIERE-768

//      Document Type Sales Order/Vendor RMA
        lDocType.setText(Msg.translate(Env.getCtx(), "C_DocType_ID"));
        cmbDocType.addItem(new KeyNamePair(MOrder.Table_ID, Msg.translate(Env.getCtx(), "Order")));
        cmbDocType.addItem(new KeyNamePair(MRMA.Table_ID, Msg.translate(Env.getCtx(), "CustomerRMA")));
        cmbDocType.addActionListener(this);
        cmbDocType.setSelectedIndex(0);

        textbox.addEventListener(Events.ON_CHANGE, this);
        placabox.setEnabled(false);
        placabox.addEventListener(Events.ON_CHANGE, this);
        //textbox.addEventListener(Events.ON_CHANGING, this);
        //form.getStatusBar().setStatusLine(Msg.getMsg(Env.getCtx(), "InvGenerateSel"));//@@
	}	//	fillPicks

	/**
	 *  Query Info
	 */
	public void executeQuery()
	{
		//Boolean selectall = form.getSelectAll();
		KeyNamePair docTypeKNPair = cmbDocType.getSelectedItem().toKeyNamePair();
		executeQuery(docTypeKNPair, form.getMiniTable(), form.getStatusBarLabel());
		
		form.getMiniTable().repaint();
		form.getMiniTable().autoSize();
		form.invalidate();
		fixWidthColumns(form.getMiniTable());
	}   //  executeQuery

	/**
	 *	Action Listener
	 *  @param e event
	 */
	public void onEvent(Event e)
	{
		if (log.isLoggable(Level.INFO)) log.info("Cmd=" + e.getTarget().getId());
		//
		if(cmbDocType.equals(e.getTarget()))
		{
		    form.postQueryEvent();
		    return;
		}
		else if(Events.ON_CHANGING.equals(e.getName()) || Events.ON_CHANGE.equals(e.getName())) {
			if(e.getTarget() instanceof Textbox) {
				//filtro OVs
				if(e.getTarget() == textbox) {
					StringBuilder cadena = new StringBuilder("");
					m_textbox = textbox.getValue();
					if(!textbox.getValue().equals("")) {
						List<String> items = Arrays.asList(m_textbox.toString().trim().split(","));
						for(int i=0; i<items.size(); i++) {
							cadena = cadena.append("'").append(items.get(i).trim()).append("'");
							if(items.size() != (i+1))
								cadena=cadena.append(",");
						}
						m_textbox= cadena;
					}
				}
				//filtro placa
				if(e.getTarget() == placabox) {
					m_placabox = placabox.getText();
				}
			}
				
			
		}

		//
		//validate();
	}	//	actionPerformed

	public void validate()
	{
		String docActionSelected = (String)docAction.getValue();
		if ( docActionSelected==null || docActionSelected.isEmpty() )
			throw new WrongValueException(docAction.getComponent(), Msg.translate(Env.getCtx(), "FillMandatory"));

		form.saveSelection();

		ArrayList<Integer> selection = getSelection();
		if (selection != null && selection.size() > 0 && isSelectionActive())
			form.generate();
		else
			form.dispose();
	}

	/**
	 *	Value Change Listener - requery
	 *  @param e event
	 */
	public void valueChange(ValueChangeEvent e)
	{
		if (log.isLoggable(Level.INFO)) log.info(e.getPropertyName() + "=" + e.getNewValue());
		if (e.getPropertyName().equals("AD_Org_ID")) {
			if(e.getNewValue()==null)
				m_AD_Org_ID=0;
			else
				m_AD_Org_ID = e.getNewValue();
			setOrgSelected(m_AD_Org_ID);
		}
		if (e.getPropertyName().equals("C_BPartner_ID"))
		{
			m_C_BPartner_ID = e.getNewValue();
			if(m_C_BPartner_ID==null)
				placabox.setEnabled(false);
			else
				placabox.setEnabled(true);
			fBPartner.setValue(m_C_BPartner_ID);	//	display value
		}
		if(e.getPropertyName().equals("C_DocTypeTarget_ID")){
			m_C_DocTypeTarget_ID = e.getNewValue();
		}
		form.postQueryEvent();
	}	//	vetoableChange

	/**************************************************************************
	 *	Generate Shipments
	 */
	public String generate()
	{
		KeyNamePair docTypeKNPair = (KeyNamePair)cmbDocType.getSelectedItem().toKeyNamePair();
		String docActionSelected = (String)docAction.getValue();
		return generate(form.getStatusBar(), docTypeKNPair, docActionSelected, form.getStatusBarLabel());
	}	//	generateShipments

	public ADForm getForm()
	{
		return form;
	}
	
	/**
	 * Fix width of columns
	 * @param table
	 */
	private void fixWidthColumns(WListbox table) {
		int col= 0;
		for (Component component: table.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			if(col==2 || col==5 || col==6  || col==8)
				ZKUpdateUtil.setHflex(header, "min");
			else if(col==1 || col==3 || col==7)
				ZKUpdateUtil.setHflex(header, "max");
			col++;
		}
	}
	
	public void selectAllItems() {
		
		textbox.setEnabled(false);
		int row = form.getMiniTable().getRowCount();
		for( int i=0; i<row; i++)
			form.getMiniTable().setValueAt(true, i, 0);
	}
	
	public void unSelectAllItems() {
		
		textbox.setEnabled(true);
		int row = form.getMiniTable().getRowCount();
		for( int i=0; i<row; i++)
			form.getMiniTable().setValueAt(false, i, 0);
	}
	
	public void cleanField() {
		
		textbox.setEnabled(true);
		if(m_C_BPartner_ID==null)
			placabox.setEnabled(false);
		else
			placabox.setEnabled(true);
	}
}