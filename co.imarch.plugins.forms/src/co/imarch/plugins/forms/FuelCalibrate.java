package co.imarch.plugins.forms;

import java.util.*;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.*;


import java.math.BigDecimal;
import java.sql.*;

public class FuelCalibrate {
	
	public static CLogger log = CLogger.getCLogger(FuelCalibrate.class);
	
	
	public int         	m_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx()); //0;
	/**
	public void dynInit () throws Exception
	{
		m_AD_Org_ID= Env.getAD_Org_ID(Env.getCtx());
	}
	**/
	public Listbox FuelLine()
	{
		Listbox fuelline = ListboxFactory.newDropdownListbox();
		KeyNamePair pp = new KeyNamePair(0,"");
		fuelline.addItem(pp);
		
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_FuelLine_ID, name FROM  smj_FuelLine WHERE isactive='Y' ORDER BY name ASC ");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				pp = new KeyNamePair(rs.getInt(1),rs.getString(2));
				fuelline.addItem(pp);
			}
		}
		catch( SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			pstmt=null;
			rs=null;
		}
		
		return fuelline;
	}
	
	public Timestamp getEntrydateFuelSales()
	{
		Timestamp entrydate = null;
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT MAX(entrydate) FROM smj_fuelsales ");
		if(m_AD_Org_ID!=0)
			sql.append(" WHERE AD_Org_ID="+m_AD_Org_ID+" AND smj_rchangefuelline_id IS NULL ");
		else
			sql.append(" WHERE smj_rchangefuelline_id IS NULL  ");
		
		//sql.append(" ORDER BY smj_fuelsales_id DESC ");
		
		
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql.toString(),null);
			
			rs = st.executeQuery();
			while(rs.next())
			{
				entrydate = rs.getTimestamp(1);
			}
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, st);
			st = null;
			rs = null;
		}
		
		/**
		 * Suma un dia a la Fecha del ultimo ingreso de combustibles
		 * es el unico dia habilitado para hacer la calibracion
		 */
		
		if(entrydate == null)
			entrydate = (Timestamp) new Date();
		else
		{
			Calendar fch = Calendar.getInstance();
			fch.setTime(entrydate);
			fch.add(Calendar.DAY_OF_MONTH, 1);
			
			entrydate = new Timestamp(fch.getTimeInMillis());
		}
		
		
		return entrydate;
		
	}

	
	public Vector<Object> getDataFuelLine(String fuellineID)
	{
		 Vector<Object> result= new Vector<Object>();
	
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_fuelisland_id, actualhundred ");//1,2
		sql.append(" ,(select fi.name from smj_fuelisland fi where fi.smj_fuelisland_id=fl.smj_fuelisland_id) as isla ");//3
		sql.append(" , smj_fuelisland_id, fl.M_Warehouse_ID ");//4,5
		sql.append(" ,(select w.name from M_Warehouse w where w.M_Warehouse_ID=fl.M_Warehouse_ID) as tanque ");//6
		sql.append(" FROM  smj_FuelLine fl WHERE smj_FuelLine_ID='"+fuellineID+"' ");
		
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			
			while(rs.next())
			{
					result.add(rs.getInt(1));			//smj_fuelisland_id
					result.add(rs.getBigDecimal(2));	//actualhundred
					result.add(rs.getString(3));		//Isla Name
					result.add(rs.getInt(4));			//Isla ID
					result.add(rs.getInt(5)); 			//Tanque ID
					result.add(rs.getString(6));		//Tanque Name
				
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs,st);
			rs = null;
			st = null;
		}
		
		return result;
	}//end getDataFuelLine
	
	public int getBParnterUser(int userid){
		
		int reuslt=0;
		StringBuffer sql = new StringBuffer();
		sql.append("select c_bpartner_id from AD_User where AD_User_ID='"+userid+"' ");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try{
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next()){
				reuslt=rs.getInt(1);
			}
		}
		catch(SQLException e){
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs, st);
			rs = null;
			st = null;
		}
		return reuslt;
	}//end getBParnerUser
	
	public Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_FuelIsland_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_FuelLine_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "actualhundred"));
		columnNames.add(Msg.translate(Env.getCtx(), "fuelcalibration"));

		return columnNames;
	}//getTableColumnNames
	
	public void setTableColumnClass(IMiniTable table)	{
		int i = 0;
  
		table.setColumnClass(i++, String.class, true);   //M_Warehouse_ID
		table.setColumnClass(i++, String.class, true);   //smj_FuelIsland_ID
		table.setColumnClass(i++, String.class, true);	//smj_FuelLine_ID
		table.setColumnClass(i++, String.class, true);   //hundredEnd
		table.setColumnClass(i++, String.class, true);   //qtySold

		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	/**
	 * VErifica que la manguera no tanga calibraciones con la fecha ingresada
	 * @param datefuelcalibrate
	 * @param fuelline
	 * @return
	 */
	public int IsCalibrate( Timestamp datefuelcalibrate, int fuelline){

		int result=0;
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT COUNT(entrydate) FROM adempiere.smj_fuelsales ");
		if(m_AD_Org_ID!=0)
			sql.append(" WHERE AD_Org_ID="+m_AD_Org_ID+" AND ");
		else
			sql.append(" WHERE ");
		sql.append(" entrydate='"+datefuelcalibrate+"' ");
		sql.append(" AND smj_fuelline_id='"+fuelline+"' ");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try{
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getInt(1);
			}		
		}
		catch(Exception e){
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs, st);
			rs = null;
			st = null;
		}
		return result;
	}//IsCalibrate
	
	public static Boolean updateFuelLine(String trxName, Integer fuelLineId, BigDecimal hundred) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" UPDATE smj_FuelLine SET smj_RChangeFuelLine_ID = null, actualhundred ="+hundred+" ");
		sql.append(" WHERE smj_FuelLine_ID = "+fuelLineId+" ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs>0?true:false;
		}catch (Exception e)	{
			flag = false;
			log.log (Level.SEVERE, FuelCalibrate.class.getName()+".updateFuelLine - ERROR::"+e.getMessage() , e);
		}
		finally	{
			DB.close(pstmt);
			rs = null; pstmt = null;
		}
		
		return flag;
	}//updateFuelLine

}
