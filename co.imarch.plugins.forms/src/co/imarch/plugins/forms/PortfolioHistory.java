/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package co.imarch.plugins.forms;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.WListbox;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.compiere.minigrid.IMiniTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

public class PortfolioHistory
{
	public DecimalFormat format = DisplayType.getNumberFormat(DisplayType.Amount);

	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(PortfolioHistory.class);

	public int         	m_C_BPartner_ID = 0;	
	public int         	m_AD_Org_ID = 0;
	public Boolean		m_isSoTrx = true;
 

	public void dynInit() throws Exception
	{
	
		m_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		
	}
	
	/**
	 *  SQL Protfolio
	 */	
	public Vector<Vector<Object>> getPortfoliData(Object bpartner, Object date, IMiniTable paymentTable)
	{		
		/********************************
		 *  Load unallocated Payments
		 *      1-C_BPartner_ID, 2-BPartnerName, 
		 *      3-TotalInvoice, 4-TotalOpen,)
		 *      5-Open30, 6-Open60, 7-Open90
		 *      8-Open180, 9-Openmore
		 */
		BigDecimal signo = new BigDecimal(1);
		if(!m_isSoTrx)
			signo = signo.negate();
		
		if(bpartner == null)
			m_C_BPartner_ID = 0;
		else
			m_C_BPartner_ID = (int)bpartner;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT *, "
			+ "(SELECT taxid FROM c_bpartner bp WHERE bp.c_bpartner_id=imarch_historyportfolio.c_bpartner_id) AS nit "
			+ "FROM imarch_historyportfolio(?, ?, ?, ?) ORDER BY tercero");                   		//      #5

		if (log.isLoggable(Level.FINE)) log.fine("PaySQL=" + sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setTimestamp(1, (Timestamp)date);
			pstmt.setInt(2, m_AD_Org_ID);
			pstmt.setInt(3, m_C_BPartner_ID);
			if(m_isSoTrx)
				pstmt.setString(4, "Y");
			else
				pstmt.setString(4, "N");

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();

				line.add(rs.getString(10));		//  0-Nit
				KeyNamePair bp = new KeyNamePair(rs.getInt(1), rs.getString(2));
				line.add(bp);                       //  1-BPartner
				line.add(rs.getBigDecimal(3).multiply(signo));      //  2-TotalInvoice
				line.add(rs.getBigDecimal(4).multiply(signo));      //  3-TotalOpen
				line.add(rs.getBigDecimal(5).multiply(signo));      //  4-Open30
				line.add(rs.getBigDecimal(6).multiply(signo));      //  5-Open60
				line.add(rs.getBigDecimal(7).multiply(signo));      //  6-Openm90
				line.add(rs.getBigDecimal(8).multiply(signo));      //  7-Open180
				line.add(rs.getBigDecimal(9).multiply(signo));      //  6-Openmore
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		
		return data;
	}
	
	public Vector<Vector<Object>> getPortfoliDataDetail(Object bpartner, Object date, IMiniTable paymentTable)
	{		

		BigDecimal signo = new BigDecimal(1);
		if(!m_isSoTrx)
			signo = signo.negate();
		if(bpartner == null)
			m_C_BPartner_ID = 0;
		else
			m_C_BPartner_ID = (int)bpartner;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append("(SELECT value FROM C_ElementValue WHERE C_ElementValue_ID=account_id) AS cuenta, ");
		sql.append("documentno, dateinvoiced, ");
		sql.append("(SELECT taxid FROM c_bpartner bp WHERE bp.c_bpartner_id=imarch.c_bpartner_id) AS nit, ");
		sql.append("(SELECT name FROM c_bpartner bp WHERE bp.c_bpartner_id=imarch.c_bpartner_id) AS tercero, ");
		sql.append("grandtotal, (grandtotal - pago) AS saldo, vencido, typedoc ");
		sql.append("FROM imarch_detailportfolio(?, ?, ?, ?) imarch ");
		sql.append("LEFT JOIN Fact_Acct on record_id=c_invoice_id and ad_table_id='318' AND ");
		sql.append("(CASE ");
		sql.append(" WHEN typedoc='API' THEN amtacctcr=grandtotal WHEN typedoc='APC' THEN amtacctdr=ABS(grandtotal) ");
		sql.append(" WHEN typedoc='ARI' THEN amtacctdr=grandtotal WHEN typedoc='ARC' THEN amtacctcr=ABS(grandtotal) ");
		sql.append(" END) ");
		sql.append("ORDER BY tercero, vencido DESC");                   		//      #5

		//1 - Cuenta
		//2 - Documento
		//3 - Fecha Factura
		//4 - Nit/CC
		//5 - Tercero
		//6 - Gran Total
		//7 - Saldo
		//8 - Dias Vencido
		//9 - Tipo Documento	
		
		if (log.isLoggable(Level.FINE)) log.fine("PaySQL=" + sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setTimestamp(1, (Timestamp)date);
			pstmt.setInt(2, m_AD_Org_ID);
			pstmt.setInt(3, m_C_BPartner_ID);
			if(m_isSoTrx)
				pstmt.setString(4, "Y");
			else
				pstmt.setString(4, "N");

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();

				//KeyNamePair bp = new KeyNamePair(rs.getInt(1), rs.getString(2));
				line.add(rs.getString(4));             	//  1-Nit
				line.add(rs.getString(5));             	//  2-tercero
				line.add(rs.getString(2));             	//  3-Documento
				line.add(rs.getDate(3));      			//  4-DateInvoice
				line.add(rs.getInt(8));     			//  5-Due Days
				line.add(rs.getString(1));             	//  6-Cuenta
				line.add(rs.getBigDecimal(6).multiply(signo));      	//  7-TotalInvoiced
				line.add(rs.getBigDecimal(7).multiply(signo));     		//  8-Saldo
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		
		return data;
	}

	public Vector<String> getColumnNames()
	{
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("CC/NIT");
		columnNames.add(Msg.getMsg(Env.getCtx(), "BPartner"));	
		columnNames.add("Total Facturado");
		columnNames.add("Total Abierto");
		columnNames.add("0 a 30 Dias Vencidos");
		columnNames.add("31 a 60 Dias Vencidos"); 
		columnNames.add("61 a 90 Dias Vencidos");
		columnNames.add("91 a 180 Dias Vencidos");
		columnNames.add("Mas de 180 Dias Vencidos");
		
		return columnNames;
	}
	
	public Vector<String> getColumnNamesDetail()
	{
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("CC/NIT");
		columnNames.add("Tercero");
		columnNames.add("No. Documento");	
		columnNames.add("Fecha de Facturacion");
		columnNames.add("Dias Vencidos");
		columnNames.add("Cuenta Contable");
		columnNames.add("Total Facturado"); 
		columnNames.add("Total Abierto");
		
		return columnNames;
	}
	
	public void setPortfolioColumnClass(IMiniTable portfolioTable)
	{
		int i = 0;
		portfolioTable.setColumnClass(i++, String.class, true);          // 0-NIT
		portfolioTable.setColumnClass(i++, String.class, true);          // 1-BPartner
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 2-TotalInvoice
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 3-TotalOpen
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 4-Open30
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 5-Open60
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 6-Open90
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 6-Open180
		portfolioTable.setColumnClass(i++, BigDecimal.class, true);      // 6-Openmore
		portfolioTable.autoSize();
	}
	
	public void setPortfolioDetailColumnClass(IMiniTable portfolioDetailTable)
	{
		int i = 0;
		portfolioDetailTable.setColumnClass(i++, String.class, true);      	    // 1-Nit
		portfolioDetailTable.setColumnClass(i++, String.class, true);      	    // 2-Tercero
		portfolioDetailTable.setColumnClass(i++, String.class, true);      	    // 3-DocumentNo
		portfolioDetailTable.setColumnClass(i++, Date.class, true);      		// 4-DateInvoice
		portfolioDetailTable.setColumnClass(i++, BigDecimal.class, true);   	// 5-Due Days
		portfolioDetailTable.setColumnClass(i++, String.class, true);      	    // 6-Cuenta
		portfolioDetailTable.setColumnClass(i++, BigDecimal.class, true);      	// 7-TotalInvoiced
		portfolioDetailTable.setColumnClass(i++, BigDecimal.class, true);      	// 8-Saldo
		portfolioDetailTable.autoSize();
	}
	
	public Vector<Vector<Object>> getDataJournal(Object bpartner, Object date, IMiniTable paymentTable)
	{
		/********************************
		 *  Load journals detail for acount 130505_Nacionales 
		 *      1-BPartner, 2-DocumentNo, 
		 *      3-DateAcct, 4-Description,)
		 *      5-Account, 6-AmtDr, 7-AmtCr
		 */
		if(bpartner == null)
			m_C_BPartner_ID = 0;
		else
			m_C_BPartner_ID = (int)bpartner;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append("(SELECT name FROM c_bpartner cbp WHERE cbp.c_bpartner_id=jl.c_bpartner_id) AS bpartner, ");
		sql.append("(SELECT Documentno FROM GL_Journal gl WHERE gl.GL_Journal_ID=jl.GL_Journal_ID) AS document, ");
		sql.append("dateacct, ");
		sql.append("(SELECT description FROM GL_Journal gl WHERE gl.GL_Journal_ID=jl.GL_Journal_ID) AS description,  ");
		sql.append("(SELECT ev.value FROM C_ElementValue ev WHERE ev.C_ElementValue_ID=jl.account_id) AS account, ");
		sql.append("amtacctdr, amtacctcr, ");
		sql.append("(SELECT taxid FROM c_bpartner cbp WHERE cbp.c_bpartner_id=jl.c_bpartner_id) AS nit ");
		sql.append("FROM GL_JournalLine jl WHERE ");
		sql.append((m_AD_Org_ID==1000000 ? "dateacct BETWEEN '2015/12/01' AND ? ":"dateacct<=? "));
		sql.append("AND AD_Org_ID=? ");
		if(m_isSoTrx)
			sql.append(" AND account_id=1000034 ");
		else	
			sql.append(" AND account_id=1000013 ");
		if(m_C_BPartner_ID>0)
			sql.append(" AND c_bpartner_id= ? ");
		
		if (log.isLoggable(Level.FINE)) log.fine("PaySQL=" + sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setTimestamp(1, (Timestamp)date);
			pstmt.setInt(2, m_AD_Org_ID);
			if(m_C_BPartner_ID>0)
				pstmt.setInt(3, m_C_BPartner_ID);

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();

				line.add(rs.getString(8));          //  0-Nit
				line.add(rs.getString(1));          //  1-BPartner
				line.add(rs.getString(2));      	//  2-DocumentNo
				line.add(rs.getDate(3));      		//  3-DateAcct
				line.add(rs.getString(4));          //  4-Description
				line.add(rs.getString(5));      	//  5-Account
				line.add(rs.getBigDecimal(6));      //  6-AmtDr
				line.add(rs.getBigDecimal(7));      //  7-AmtCr
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		
		return data;
	}
	
	public void setTableJournal(IMiniTable tableJournal)
	{
		int i = 0;
		tableJournal.setColumnClass(i++, String.class, true);           // 0-Nit
		tableJournal.setColumnClass(i++, String.class, true);           // 1-BPartner
		tableJournal.setColumnClass(i++, String.class, true);   		// 2-DocumentNo
		tableJournal.setColumnClass(i++, Date.class, true);     		// 3-DateAcct
		tableJournal.setColumnClass(i++, String.class, true);  		    // 4-Description
		tableJournal.setColumnClass(i++, BigDecimal.class, true);       // 5-Account
		tableJournal.setColumnClass(i++, BigDecimal.class, true);       // 6-AmtDr
		tableJournal.setColumnClass(i++, BigDecimal.class, true);       // 7-AmtCr
		tableJournal.autoSize();
	}
	
	public Vector<String> getColumnNamesJournal()
	{
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("CC/NIT");
		columnNames.add("Tercero");	
		columnNames.add("No. Documento");
		columnNames.add("Fecha Contable");
		columnNames.add("Descripcion");
		columnNames.add("Cuenta Contable"); 
		columnNames.add("Valor Debito");
		columnNames.add("Valor Credito");
		
		return columnNames;
	}
	
	public XSSFWorkbook createFileExcel(WListbox mTable) {
		
		Vector<String> nameColumn = null;
		if(mTable.getName().equals("portfolio"))
			nameColumn = getColumnNames();
		if(mTable.getName().equals("portfolioDetail"))
			nameColumn = getColumnNamesDetail();
		if(mTable.getName().equals("journalTable"))
			nameColumn = getColumnNamesJournal();
		
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet hoja = wb.createSheet();
		CellStyle cellStyle = wb.createCellStyle();
	    cellStyle.setDataFormat((short)14);
		try {
			for(int f=0; f<=mTable.getRowCount();f++) 
			{
				Row fila = hoja.createRow(f);
				for(int c=0;c<mTable.getColumnCount();c++) 
				{
					Cell celda = fila.createCell(c);
					if(f==0)
						celda.setCellValue(nameColumn.get(c));
					else {
						//tipo de tabla
						if(mTable.getName().equals("portfolio")) 
						{
							if(c==0 || c==1)
								celda.setCellValue(String.valueOf(mTable.getValueAt(f-1, c)));
							else
								celda.setCellValue(((BigDecimal)mTable.getValueAt(f-1, c)).doubleValue());
						}
						else if(mTable.getName().equals("portfolioDetail")) 
						{
							if( c < 3 )
								celda.setCellValue(mTable.getValueAt(f-1, c).toString());
							else if(c == 3 )
							{
								celda.setCellValue((Date)mTable.getValueAt(f-1, c));
								celda.setCellStyle(cellStyle);
							}
							else if( c == 4 )
								celda.setCellValue((int)mTable.getValueAt(f-1, c));
							else if( c == 5 ) {
								if(mTable.getValueAt(f-1, c)!=null)
									celda.setCellValue(mTable.getValueAt(f-1, c).toString());
								else
									celda.setCellValue("");
							}
							else
								celda.setCellValue(((BigDecimal)mTable.getValueAt(f-1, c)).doubleValue());
						}
						else if(mTable.getName().equals("journalTable")) 
						{
							if( c == 6 || c == 7 )
								celda.setCellValue(((BigDecimal)mTable.getValueAt(f-1, c)).doubleValue());
							else if(c == 3 )
							{
								celda.setCellValue((Date)mTable.getValueAt(f-1, c));
								celda.setCellStyle(cellStyle);
							}
							else
								celda.setCellValue(mTable.getValueAt(f-1, c).toString());
						}
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}			
		
		return wb;
	}
	
	
	
}
