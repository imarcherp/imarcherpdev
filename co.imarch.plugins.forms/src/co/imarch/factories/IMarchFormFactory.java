package co.imarch.factories;

import java.util.logging.Level;

import org.adempiere.webui.factory.IFormFactory;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.IFormController;
import org.compiere.util.CLogger;

public class IMarchFormFactory implements IFormFactory {

	protected transient CLogger log = CLogger.getCLogger(getClass());
	
	@Override
	public ADForm newFormInstance(String formName) {
		if (formName.startsWith("co.imarch.plugins.forms")) {
			Object form = null;
			Class<?> clazz = null;
			ClassLoader loader = getClass().getClassLoader();
			
			try {
				clazz = loader.loadClass(formName);
			} catch (Exception e) {
				if (log.isLoggable(Level.INFO) || log.isLoggable(Level.ALL)) {
					log.log(Level.INFO, "Load Form Class Failed in co.imarch.plugins.forms", e);
				}
			}
			
			if (clazz != null) {
				try {
					form = clazz.newInstance();
				} catch (Exception e) {
					if (log.isLoggable(Level.INFO) || log.isLoggable(Level.ALL)) {
						log.log(Level.INFO, "Form Class Initiate Failed in co.imarch.plugins.forms", e);
					}
				}
			}
			
			if (form != null) {
				if (form instanceof ADForm) {
					return (ADForm) form;
				} else if (form instanceof IFormController) {
					IFormController controller = (IFormController) form;
					ADForm adForm = controller.getForm();
					adForm.setICustomForm(controller);
					return adForm;
				}
			}
		}
		
		return null;
	}

}
