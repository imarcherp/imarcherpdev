package co.imarch.factories;

import org.compiere.grid.ICreateFrom;
import org.compiere.grid.ICreateFromFactory;
import org.compiere.model.GridTab;
import org.compiere.model.I_C_Payment;

import co.imarch.plugins.forms.model.WCreateFromPayment;

public class CreateFromFactoryiMarch implements ICreateFromFactory {

	@Override
	public ICreateFrom create(GridTab mTab) {
		
		String tableName = mTab.getTableName();
		if (tableName.equals(I_C_Payment.Table_Name))
			return new WCreateFromPayment(mTab);
		return null;
	}

}
