<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.idempiere.org/dsp/web/util" prefix="u" %>

html,body {
	margin: 0;
	padding: 0;
	height: 100%;
	width: 100%;
	background-color: #D4E3F4;
	color: #333;
	font-family: Helvetica,Arial,sans-serif;
	overflow: hidden;
}

.z-html p{
	margin:0px;
}

<%-- Tablet --%>
.tablet-scrolling {
	-webkit-overflow-scrolling: touch;
}

<%-- vbox fix for firefox and ie --%>
table.z-vbox > tbody > tr > td > table {
	width: 100%;	
}

<%-- workflow activity --%>
.workflow-activity-form {
}
.workflow-panel-table {
	border: 0px;
}

<%-- payment form --%>
.payment-form-content {
}

<%-- pintal el HR heder de las grillas --%>
.z-listheader {
    border-left: 1px solid #cfcfcf !important;
    border-bottom: 1px solid #cfcfcf !important;
    background: -moz-linear-gradient(top,#fefefe 0,#ECE9D7 40%) ;
    background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#fefefe),color-stop(40%,#ECE9D7)) !important;
    background: -webkit-linear-gradient(top,#fefefe 0,#ECE9D7 40%) !important;
    background: -o-linear-gradient(top,#fefefe 0,#ECE9D7 40%) !important;
    background: -ms-linear-gradient(top,#fefefe 0,#ECE9D7 40%) !important;
    background: linear-gradient(to bottom,#fefefe 0,#ECE9D7 40%) !important;

}
.z-textbox {
    background-color: #FFFFFF;
    border: 1px solid #0099ff; <%-- cdd7bb --%>
    color: #333;
}
<%-- Define el color de la linea de las grillas --%>
.z-listitem.z-listitem-selected>.z-listcell {
    background: #FBD87E !important;
}
<%-- Define altura del contenido de la grilla --%>
.z-listcell-content {
    line-height: 15px !important;
}

.z-listbox-odd.z-listitem {
    background: #f5f5f5!important; <%-- f5f5f5 --%>
}

.desktop-left-column .z-caption, .desktop-left-column .z-caption-content {
    background-color: #76746b;
    border-radius: 10px 0 0 0;
    color: #ffffff !important;
}

.desktop-left-column .z-panel-icon {
	color: black;
    display: block;
    border: 1px solid #0D1E44;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -o-border-radius: 4px;
    -ms-border-radius: 4px;
    border-radius: 4px;
    margin: auto 1px;
    background: #fefefe;
    background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zd�IgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCN6a2llOSkiIC8+PC9zdmc+);
    background: -moz-linear-gradient(top,#fefefe 0,#eee 100%);
    background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#fefefe),color-stop(100%,#eee));
    background: -webkit-linear-gradient(top,#fefefe 0,#eee 100%);
    background: -o-linear-gradient(top,#fefefe 0,#eee 100%);
    background: -ms-linear-gradient(top,#fefefe 0,#eee 100%);
    background: linear-gradient(to bottom,#f7f6ef 0,#fff 100%);
    text-align: center;
    overflow: hidden;
    cursor: pointer;
    float: right;
}
.z-center-body .z-caption {
    border-bottom: solid 2px #FCC654;
}
.favourites-box .z-hbox:hover {
    background-color: #FBD87E;
}
.favourites-box .z-hbox {
    border: solid 1px #ccc;
    background-color: #DFEBF5;
    cursor: pointer;
}
.favourites-box .z-vbox-separator {
    height: .1em;
    background-color: #ffffff;
}

.recentitems-box .z-vbox-separator {
    height: .2em;
    background-color: #ffffff;
}
.recentitems-box A:hover {
    background-color: #DFEBF5;
}
.recentitems-box A {
    border: solid 1px #ccc;
    background-color: #ECE9D7;
    cursor: pointer;
}

/** --- SCROLL -- **/
::-webkit-scrollbar {
    width: 14px;
    height: 14px;
}  

::-webkit-scrollbar-button:start:decrement,
::-webkit-scrollbar-button:end:increment {
    display: block;
}

::-webkit-scrollbar-button:start:increment,
::-webkit-scrollbar-button:end:decrement {
    display: none;
}

::-webkit-scrollbar-button:vertical:end:increment {
    background-image: url(../images/scroll/scroll_cntrl_dwn_grey14.png);
}

::-webkit-scrollbar-button:vertical:start:decrement {
    background-image: url(../images/scroll/scroll_cntrl_up_grey14.png);
}

::-webkit-scrollbar-track-piece:vertical:start {
    background-image: url(../images/scroll/scroll_gutter_top_grey14.png), url(../images/scroll/scroll_gutter_mid_grey14.png);
    background-repeat: no-repeat, repeat-y;
}
 
::-webkit-scrollbar-track-piece:vertical:end {
    background-image: url(../images/scroll/scroll_gutter_btm_grey14.png), url(../images/scroll/scroll_gutter_mid_grey14.png);
    background-repeat: no-repeat, repeat-y;
    background-position: bottom left, 0 0;
}

::-webkit-scrollbar-button:horizontal:start:decrement {
    background-image: url(../images/scroll/scroll_cntrl_left_grey14.png);
}

::-webkit-scrollbar-button:horizontal:end:increment {
    background-image: url(../images/scroll/scroll_cntrl_right_grey14.png);
}

::-webkit-scrollbar-track-piece:horizontal:start {
    background-image: url(../images/scroll/scroll_gutter_left_grey14.png), url(../images/scroll/scroll_gutter_midh_grey14.png);
    background-repeat: no-repeat, repeat-x;
}
 
::-webkit-scrollbar-track-piece:horizontal:end {
    background-image: url(../images/scroll/scroll_gutter_right_grey14.png), url(../images/scroll/scroll_gutter_midh_grey14.png);
    background-repeat: no-repeat, repeat-x;
    background-position:  right, 0 0;
}

::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
    background-color: #D6EAFE; /*#f5f5f5;*/
    /*background-image: -webkit-linear-gradient(45deg, rgba(161, 180, 250, .2) 25%, transparent 25%, transparent 50%, rgba(161, 180, 250, .2) 50%, rgba(161, 180, 250, .2) 75%, transparent 75%, transparent);*/
} 

/** -- **/

.z-combobox-disabled * {
   background-color: #D9E0EB !important;
   border: solid 1px #CBD0D5 !important;
   box-shadow: 1px 1px 1px #ccc;
}

/* Seccion MENU */
.z-treerow [class^="z-icon-"], .z-treerow [class*=" z-icon-"]{
    display: inline-block;
    background-image: url(../images/arrow-toggle.png) !important;
    width: 18px;
    height: 18px;
    font-size: 15px;
    color: transparent;
}

.z-treerow .z-treecell-text {
    padding-left: 2px;
}

.z-popup-content { /* border padding a la seccion men�*/
    background: #fff; /*606C7D;*/
}

.z-treerow div.z-treecell-content{
    color: black; /*#fff;*/
    background: #fff; /*dfe3ee - 606C7D - fondo menu*/
}

.z-treerow .menu-href {
    color: #23355b !important;  /*#fff - Color Letras menu carpetas*/
}

.z-treerow .z-treecell-text {
    color: #111a2d; /*#fff - Color letras menur subarchivos*/
     /*font-weight: bold;*/
     text-shadow: -0.4px 0 #899bc1, 0.4px 0 #899bc1, 0 0.4px #d7ddea, 0 -0.4px #899bc1;
}

.z-treerow .z-treecell-text:hover {
    color: blue;
}

.z-treerow .menu-href:hover, .z-treerow .menu-href:active {
    color: #fbd87e !important;
    text-decoration: none !important;
}

.desktop-menu-popup .z-popup-content { 		/*dimensiones Menu cuadro interno */
    width: 520px !important; /*320px*/
    height: 465px !important; /*620px*/
    border: solid 5px #dfe3ee;
    padding: 1px;
}
.desktop-menu-popup {						/*dimensiones Menu cuadro externo */
    width: 524px !important; /*320px*/
    height: 469px !important; /*620px*/
    border: solid 2px #17233c;
}

.z-treerow {
  background: transparent;
}

.z-tree-body {
    background: #fff; /*606C7D;*/
    /width: 500px !important;*/
    height: 400px !important;
    /*border: solid gray;*/
}

.z-popup-content .z-panel-body {
    background-color: #fff !important; /*606C7D*/
}

.z-window-content .z-center-body { /*Fondo para graficos nativos*/
    background-color: #fff;
}

.z-listbox-body {  /*Fondo para data grid ventanas Info*/
    background-color: #ffffff;
}

.z-row:hover > .z-row-inner, .z-row:hover > .z-cell { /* efecto sombreado al pasar en grillas*/
	background-image: linear-gradient(#b0dfd4, #e7f5f2, #b0dfd4);
}

.adwindow-detailpane-tabbox { /*color fondo pestanhas detalle*/
    background-color: #EEF1F6;
}

.info-panel .z-listbox-body .z-listitem.z-listitem > .z-listcell > .z-listcell-content {
    color: #636363; 
}

/*header de lso hd en las grillas de las ordenes*/

.z-grid-header table th, .z-grid-header table td {
    background-clip: padding-box;
    padding: 0;
    border-left: 1px solid #cfcfcf !important;
    border-bottom: 1px solid #cfcfcf !important;
    background: -moz-linear-gradient(top,#fefefe 0,#ece9d7 40%);
    background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#fefefe),color-stop(40%,#ece9d7)) !important;
    background: -webkit-linear-gradient(top,#fefefe 0,#ece9d7 40%) !important;
    background: -o-linear-gradient(top,#fefefe 0,#ece9d7 40%) !important;
    background: -ms-linear-gradient(top,#fefefe 0,#ece9d7 40%) !important;
    background: linear-gradient(to bottom,#fefefe 0,#ece9d7 40%) !important;
}

/*Efecto de fondo en el detalle de la grilla antes de pintar*/

.adwindow-detailpane-tabpanel .z-grid-body {
    margin-top: auto;
    position: relative;
    overflow: hidden;
    border: solid 1px #999 !important;
    background: #fff;
}

.info-panel .z-window-header {
    padding: 4px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    /* background-color: #A1A0A0; */
    border-color: #484848;
    border-bottom: solid 3px #FCC654;
    background-image: -moz-linear-gradient(top, #fff, #ddd);
    background-image: -ms-linear-gradient(top, #fff, #ddd);
    background-image: -o-linear-gradient(top, #fff, #ddd);
    -moz-box-shadow: 2px 2px 2px rgba(0,0,0,.4);
    -webkit-box-shadow: 2px 2px 2px rgba(0,0,0,.4);
    box-shadow: 2px 2px 2px rgba(0,0,0,.0);
    background: #7abcff !important;
    background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%) !important;
    background: -webkit-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%) !important;
    background: linear-gradient(to bottom, #7FA1DD 3%,#3B5EAF 50%,#28478E 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7abcff', endColorstr='#4096ee',GradientType=0) !important;
}

<c:include page="fragment/login.css.dsp" />

<c:include page="fragment/desktop.css.dsp" />

<c:include page="fragment/application-menu.css.dsp" />

<c:include page="fragment/gadget.css.dsp" />

<c:include page="fragment/toolbar.css.dsp" />

<c:include page="fragment/button.css.dsp" />

<c:include page="fragment/adwindow.css.dsp" />
			
<c:include page="fragment/grid.css.dsp" />

<c:include page="fragment/input-element.css.dsp" />

<c:include page="fragment/tree.css.dsp" /> 

<c:include page="fragment/field-editor.css.dsp" />

<c:include page="fragment/group.css.dsp" />

<c:include page="fragment/tab.css.dsp" />

<c:include page="fragment/menu-tree.css.dsp" />

<c:include page="fragment/info-window.css.dsp" />

<c:include page="fragment/window.css.dsp" />

<c:include page="fragment/form.css.dsp" />

<c:include page="fragment/toolbar-popup.css.dsp" />	

<c:include page="fragment/setup-wizard.css.dsp" />

<c:include page="fragment/about.css.dsp" />

<c:include page="fragment/tab-editor.css.dsp" />

<c:include page="fragment/find-window.css.dsp" />

<c:include page="fragment/help-window.css.dsp" />

<c:include page="fragment/borderlayout.css.dsp" />

<c:include page="fragment/parameter-process.css.dsp" />

<c:include page="fragment/window-size.css.dsp" />
