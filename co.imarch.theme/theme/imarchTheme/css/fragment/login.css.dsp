.login-window {
	background-color: black; /*#E5E5E5;*/
}

.login-window .z-window-content {
     background-image: url('../images/imarch/FondoLogin4.png') !important;
     background-position: fixed;
     background-top: center;
     background-left: center;
	 background-attachment: scroll;
	 background-repeat: no-repeat;
	 background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
	padding: 1px 1px 1px 1px;
	/*background-color: #E5E5E5;*/
}
.login-box {
	width: 100%;

}

.login-box-body {
	width: 1px;
    background: rgba(234,229,232,0.5); /*234,240,256,1*/
	border-radius: 20px;
	font-weight: 100;
	color: #222222;
	z-index: 1;
	padding: 10px 40px 60px 40px;
	margin: 0;
	text-align: center;
}

.login-box-header {
	<%-- background-image: url(../images/login-box-header.png); --%>
	background-color: transparent;
	z-index: 2;
	height: 120px;	/*bordes arriba para +/- ancho fondo negro*/
	width: 100px;
}

.login-box-header-txt {
	color: transparent !important;
	font-weight: bold;
	position: relative;
	top: 30px;
}

.login-box-header-logo {
	padding-top: 20px;
	padding-bottom: 25px;
}

.login-box-footer {
	<%-- background-image: url(../images/login-box-footer.png); --%>
	background-position: top right;
	background-attachment: scroll;
	background-repeat: repeat-y;
	z-index: 2;
	height: 120px; /*bordes baja para +/- ancho fondo negro*/
	width: 660px;
	position: relative;
}
.login-label .z-label {
    color: #222222;
}
.login-box-footer .confirm-panel {
	width: 220px !important;
	height: 50px !important;
	top: -60px;	/*Altura botones*/
	/*border: solid green;*/
}

.login-box-footer-pnl {
	width: 604px;
	margin-left: 10px;
	margin-right: 10px;
	padding-top: 10px !important;
}

.login-label {
	color: black;
	text-align: right;
	width: 20%;
}

.login-field {
	text-align: left;
	width: 55%;
}

.login-btn {
	padding: 4px 20px !important;
}

.login-east-panel, .login-west-panel {
	width: 10px;
	background-color: #E0EAF7;
	position: relative;
}
@media screen and (max-width: 659px) {
	.login-box-body {
		background-image: none;
		width: 90%;
	}
	.login-box-header {
		background-image: none;
		height: 80px !important;
		/*border: solid green;*/
	}
	.login-box-footer {
		background-image: none;
		width: 90%;
		height: 100px;
		/*border: solid green;*/
	}
	.login-box-footer .confirm-panel {
		width: 50% !important;
		height: 50px !important;
		top: -10px;	/*Altura botones*/
		/*border: solid green;*/
	}
	.login-box-footer-pnl {
		width: 90% !important;
	}
	.login-box-header-txt {
		display: none;
	}
}
@media screen and (max-height: 600px) {
	.login-box-header-txt {
		display: none;
	}
	.login-box-body {
		background-image: none;
	}
	.login-box-header {
		background-image: none;
		height: 80px !important;
		/*border: solid green;*/
	}
	.login-box-footer {
		background-image: none;
		width: 90%;
		height: 100px;
		/*border: solid green;*/
	}
	.login-box-footer .confirm-panel {
		width: 50% !important;
		height: 50px !important;
		top: -10px;	/*Altura botones*/
		/*border: solid green;*/
	}
	.login-box-footer-pnl {
		width: 90% !important;
	}
	.login-box-body {
		padding-bottom: 10px;
	}
	.login-box-header {
		height: 0px;
	}
}
@media screen and (max-width: 359px) {
	.login-window .z-center > .z-center-body .z-window.z-window-embedded > .z-window-content {
		padding: 0px
	}
}
