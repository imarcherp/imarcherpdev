package co.imarch.theme.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

public class GetInfoOrg {
	
private static final CLogger logger = CLogger.getCLogger(GetInfoOrg.class);
	
	public static  String getColumnValue(String Column, int Org ){
		//1- AD_Org_ID -> Org
		StringBuilder sql = new StringBuilder("SELECT "
				+Column
				+" FROM AD_OrgInfo io WHERE io.AD_Org_ID='"
				+Org+"'");
		
		if (logger.isLoggable(Level.FINE)) logger.fine("getSQL=" + sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String result="";
		try 
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			//pstmt.setInt(1,Org);
			rs = pstmt.executeQuery();
			rs.next();
			result = rs.getString(1);
			if (rs.getString(1) == null) result=""; //color default
		}
		catch (SQLException e)
		{
			logger.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		
		return result;
	}

}
