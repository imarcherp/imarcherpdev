package co.imarch.toolbar.action;

import org.adempiere.webui.action.IAction;
import org.adempiere.webui.adwindow.ADWindow;
import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.apps.AEnv;

public class ArchiveDocAction implements IAction{

	
	public ArchiveDocAction(){
	}
	
	
	@Override
	public void execute(Object target) {
		
		ADWindow adwin = (ADWindow) target;
		ADWindowContent panel = adwin.getADWindowContent();
		
		doArchiveDoc(panel);
	}

	
	private void doArchiveDoc(ADWindowContent pwin){
		
		ArchiveDocWindow dfswin = new ArchiveDocWindow();
		dfswin.init(pwin);
		dfswin.setClosable(true);
		AEnv.showWindow(dfswin);
		
	}
}
