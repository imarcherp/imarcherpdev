package co.imarch.toolbar.action;

import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.GridTab;
import org.compiere.model.MSysConfig;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vbox;


import co.imarch.plugins.forms.WArchiveVieweriMarch;

public class ArchiveDocWindow extends Window implements EventListener<Event> , ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7532575389006676781L;

	protected CLogger			log = CLogger.getCLogger (super.getClass());
	
	//Elementos
	private Button bLoad = new Button();
	private Button bview = new Button();	

	
	ADWindowContent panel;
	GridTab tab;
	PO po;
	
	public ArchiveDocWindow() {
	}
	
	public void init(ADWindowContent panel) {
		
		this.panel = panel;
		this.tab = panel.getActiveGridTab();
		
		Grid parameterLayout = GridFactory.newGridLayout();
		ZKUpdateUtil.setWidth(parameterLayout, "99%");
		parameterLayout.setStyle("padding: 5px;");
		
		setTitle("Archivador");
		setWidth("380px");
		setClosable(true);
		setSizable(true);
		setBorder("normal");
		setStyle("position:absolute");
		
		Vbox vb = new Vbox();
		appendChild(vb);		
		
		bLoad.setImage(ThemeManager.getThemeResource("images/Import24.png"));
		bLoad.setSclass("img-btn");
		bLoad.setLabel("Cargar Archivo PDF");
		bLoad.setWidth("100%");
		bLoad.addActionListener(this);
		
		bview.setImage(ThemeManager.getThemeResource("images/Archive24.png"));
		bview.setSclass("img-btn");
		bview.setWidth("100%");
		bview.setEnabled(false);
		bview.addActionListener(this);
		
		int qtyarchload = getArchiveLoad();
		if (qtyarchload>0)
			bview.setEnabled(true);
		bview.setLabel("Ver Archivos("+qtyarchload+")");
		
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();

		row.appendChild(bLoad);
		row = rows.newRow();
		row.appendChild(bview);
		
		vb.appendChild(parameterLayout);
		
	}
	
	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event event) throws Exception {

		if(event.getTarget().equals(bLoad)) {
			ArchiveLoadWindow dfswin = new ArchiveLoadWindow();
			dfswin.init(panel);
			dfswin.setClosable(false);
			AEnv.showWindow(dfswin);
			onClose();
		}
		else if(event.getTarget().equals(bview)) {
			
			int AD_Form_ID = MSysConfig.getIntValue("iMarch_WArchiveViewerFormiMarch", 0, 0);
			//int AD_Form_ID = 118;	//	ArchiveViewer
			int m_AD_Table_ID = panel.getActiveGridTab().getAD_Table_ID();
			int m_Record_ID = panel.getActiveGridTab().getRecord_ID();
			if(AD_Form_ID!=0) {
				ADForm form = ADForm.openForm(AD_Form_ID);
				
				WArchiveVieweriMarch av = (WArchiveVieweriMarch) form.getICustomForm();
				av.setShowQuery(false);
				av.query(false, m_AD_Table_ID, m_Record_ID);
	
				form.setAttribute("mode", form.getWindowMode());
				SessionManager.getAppDesktop().showWindow(form);
			}
			onClose();
		}
	}
	
	private int getArchiveLoad() {
		
		int table = panel.getActiveGridTab().getAD_Table_ID();
		int record = panel.getActiveGridTab().getRecord_ID();
		String sql = "SELECT COUNT(ad_archive_id) FROM ad_archive WHERE "
				+"ad_table_id=? AND record_id=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] { table, record });
		if (noquery > 0)
			return noquery;
		
		return 0;
	}

}
