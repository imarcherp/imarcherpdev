package co.imarch.toolbar.action;

import org.adempiere.webui.action.IAction;
import org.adempiere.webui.adwindow.ADWindow;
import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.apps.AEnv;

public class DeleteFuelSalesAction implements IAction{
	
	/**
	 * default constructor
	 */
	public DeleteFuelSalesAction(){
	}

	@Override
	public void execute(Object target) {
		// TODO Auto-generated method stub
		ADWindow adwin = (ADWindow) target;
		ADWindowContent panel = adwin.getADWindowContent();
		
		doDeleteFuelSales(panel);
	}
	
	private void doDeleteFuelSales(ADWindowContent pwin){
		
		DeleteFuelSalesWindow dfswin = new DeleteFuelSalesWindow();
		dfswin.init(pwin);
		dfswin.setClosable(false);
		AEnv.showWindow(dfswin);
		
	}

}
