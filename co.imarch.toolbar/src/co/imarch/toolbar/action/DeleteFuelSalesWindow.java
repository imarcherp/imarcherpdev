package co.imarch.toolbar.action;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridTab;
import org.compiere.model.MOrder;
import org.compiere.model.MRecentItem;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Vbox;

import co.imarch.plugins.forms.*;


public class DeleteFuelSalesWindow extends Window implements EventListener<Event> , ValueChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5359309608138901607L;
	
	protected CLogger			log = CLogger.getCLogger (super.getClass());
	
	//Elementos
	private ConfirmPanel confirmpanel = new ConfirmPanel(true);
	private Label txtconfirmacion = new Label("  �Confirma que desea reversar el Movimiento de Combustible?");
	private Label txtnovalido = new Label("  No es posible reversar."
								+"\n"
								+" No tiene ventas asociadas o las mangueras relacionadas"
								+" con la Orden de Venta, tienen calibraciones o ventas posteriores");
	
	private int fuelSaleID=0;
	private List<Object> fuelSalesIDs=new ArrayList<Object>();
	private int rcchangefuelline=0;
	private int orderID = 0;
	
	ADWindowContent panel;
	GridTab tab;
	//GridTable gtab;
	//GridTable gridtable;
	PO po;
	
	public DeleteFuelSalesWindow(){
	}
	
	public void init(ADWindowContent panel) {
		
		this.panel = panel;
		this.tab = panel.getActiveGridTab();
		fuelSaleID = panel.getActiveGridTab().getRecord_ID();
		
		/**
		gridtable = new GridTable (Env.getCtx(), tab.getAD_Table_ID(), tab.getTableName(), 
				tab.getWindowNo(), tab.getTabNo(), true, true);
		
		gridtable.setReadOnly(false);
		gridtable.setDeleteable(true);
		
		GridField[] filed = tab.getFields();
		for(int f=0;f<tab.getFieldCount();f++)
			gridtable.addField(filed[f]);
		gridtable = tab.getTableModel();
		gridtable.loadComplete();
		**/
		//tab.setCurrentRow(2487);
		
		setTitle("Reversar Ventas Combustible");
		setWidth("280px");
		setClosable(true);
		setSizable(true);
		setBorder("normal");
		setStyle("position:absolute");
		
		Vbox vb = new Vbox();
		appendChild(vb);
		
		txtconfirmacion.setWidth("100%");
		txtconfirmacion.setStyle("color: red");
		
		if(validateFuelSales(fuelSaleID)){
			vb.appendChild(txtconfirmacion);
			confirmpanel.getButton("Ok").setEnabled(true);
		}
		else{
			vb.appendChild(txtnovalido);
			confirmpanel.getButton("Ok").setEnabled(false);
		}

		Div div = new Div();
		div.appendChild(new Label());
		vb.appendChild(div);
		
		vb.appendChild(confirmpanel);
		confirmpanel.addActionListener(this);
		
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getTarget().equals(confirmpanel.getButton("Cancel"))){
			onClose();
		}
		else if(event.getTarget().equals(confirmpanel.getButton("Ok"))){
			confirmpanel.getButton("Cancel").setEnabled(false);
			onClose();
			if(deleteFuelSales()){	
				panel.onRefresh(true,false);
			}
		}
	}//onEvent

	public boolean validateFuelSales(int fuelsale_ID){
		String sql="";
		int param=0;
		orderID = getCOrderID(fuelSaleID);
		rcchangefuelline = getRCchangeFuelLine(fuelSaleID);
		
		if(orderID==0){
			if(rcchangefuelline==0)
				return false;
			else{
				sql = 	"SELECT smj_FuelSales_id FROM smj_FuelSales" 
						+" WHERE smj_fuelline_id IN (SELECT fs.smj_fuelline_id FROM smj_FuelSales fs WHERE fs.smj_FuelSales_ID=?)"
						+" AND entrydate >=  (SELECT fs.entrydate FROM smj_FuelSales fs WHERE fs.smj_FuelSales_ID=?)"
						+" AND smj_FuelSales_id!=?";
				param = fuelSaleID;
			}
		}
		else{
			sql = "SELECT smj_FuelSales_id FROM smj_FuelSales"  
						+" WHERE entrydate > (SELECT fs.entrydate FROM smj_FuelSales fs WHERE fs.C_Order_ID=? group by fs.entrydate)"
						+" AND smj_fuelline_id IN (SELECT fs.smj_fuelline_id FROM smj_FuelSales fs WHERE fs.C_Order_ID=?)"
						+" ";
			param = orderID;
		}
		if (log.isLoggable(Level.INFO)) log.fine(sql.toString());
		int noquery = 0;
		if(param == orderID)
			noquery = DB.getSQLValue(null, sql, new Object[] { param, param });
		else
			noquery = DB.getSQLValue(null, sql, new Object[] { param, param, param });
		
		if (noquery>0)
			return false;
		
		return true;
	}//validateFuelSales
	
	
	public boolean deleteFuelSales() throws InterruptedException {
		
		List<List<Object>> smj_fuelsaleID = null;
		Trx trx = Trx.get(Trx.createTrxName("FS"),true);
		Boolean updatelineok=false;
		String FuelLineName = "";
		boolean sucessful = false;
		String documentsaleorder ="";
		
		//int orderID = getCOrderID(fuelSaleID);
		if(orderID==0 && rcchangefuelline==0)
			Messagebox.show("No es una VEnta o Calibracion - Proceso Cancelado");
		else{
						
			try{
				if(rcchangefuelline==0){
					smj_fuelsaleID=getsmjFuelSales(orderID);
					MOrder saleorder = new MOrder(Env.getCtx(), orderID, trx.getTrxName());
					documentsaleorder = saleorder.getDocumentNo();
					if(saleorder.voidIt()){//if void OK
						saleorder.setGrandTotal(Env.ZERO);
						saleorder.setTotalLines(Env.ZERO);
						saleorder.setDocStatus(MOrder.DOCSTATUS_Voided);
						saleorder.setDocAction(MOrder.DOCACTION_None);
						saleorder.saveEx();
						System.out.print("Void OrderID:"+orderID+" - Document No."+saleorder.get_ValueAsString("DocumentNo")+"\n");
						sucessful=true;
					}
				}
				else{
					sucessful=true; //habilita proceso por ser calibracion
					smj_fuelsaleID=getsmjFuelSales(fuelSaleID);
					//fuelSalesIDs.add(fuelSaleID);
				}
				if(sucessful){
					for(int i=0;i<smj_fuelsaleID.size();i++){
						List<Object> fuelsalesid = smj_fuelsaleID.get(i);
						List<Object> datafuelline = getDataFuelLine(Integer.parseInt(fuelsalesid.get(0).toString()));
						String line = datafuelline.get(0).toString();
						String actualhundred  = datafuelline.get(1).toString();
						String hundredend = datafuelline.get(2).toString();
						FuelLineName += "\n - " + getNameFuelLine(Integer.parseInt(line));
						fuelSalesIDs.add(fuelsalesid.get(0));
						//Update hundred
						updatelineok = FuelCalibrate.updateFuelLine(trx.getTrxName(), Integer.parseInt(line), new BigDecimal(actualhundred));
						if(!updatelineok){
							i=smj_fuelsaleID.size();
							trx.rollback();
							trx.close();
							log.log(Level.SEVERE, "Error al Actualizar Centena");
							Messagebox.show("ERROR:"
									+ "\n"
									+ "Se Anulo la Orden de Venta : "
									+ "\n - "
									+ documentsaleorder
									+ "\n"
									+ "Pero NO se elimino Historial y tampoco se actualizaron las Centenas");
							sucessful = false;
						}
						else
							System.out.print("FuelLineID:"+line+" - Centena Anterior:"+hundredend+" - Centena Actualizada:"+actualhundred+"\n");
					}
					if(updatelineok){//if update hundred OK	
						if(deleteHistoryFuelSales((orderID==0?fuelSaleID:orderID), trx.getTrxName())){//delete reg smj_fuelsale
							
							Messagebox.show("Movimiento Combustible Reversado"
									+ "\n"
									+ "Orden de Venta Anulada: "
									+ "\n - "
									+ documentsaleorder
									+ "\n"
									+ "Mangeras actualizadas:"+FuelLineName);
							sucessful = true;
						}
						else{
							trx.rollback();
							trx.close();
							log.log(Level.SEVERE, "Error al Eliminar el historico de Ventas Combustible");
							sucessful = false;
						}
					}
				}
				else{
					System.out.print("Error OrderID:"+orderID+" - No."+documentsaleorder+"\n");
					Messagebox.show("ERROR:"
							+ "\n"
							+ "NO se pudo Anular la Orden de Venta : "+ documentsaleorder);
					sucessful = false;
				}
					
			}
			catch(Exception e ){
				sucessful = false;
				trx.rollback();
				trx.close();
				log.log(Level.SEVERE, "Error al borrar ventas Combustible", e);
			}
			finally{
				trx.commit();
				trx.close();
			}
		}
		return sucessful;
	}//deleteFuelSales
	
	public String getNameFuelLine(int smj_fuelline_id){
		String result="";
		String sql = "SELECT name FROM smj_fuelline WHERE smj_fuelline_id=?";
		
		result = DB.getSQLValueString(null, sql, smj_fuelline_id);
		
		return result;
	}
	
	public List<Object> getDataFuelLine(int smj_fuelsaleID){
		
		List<Object> result = new Vector<Object>();
		
		String sql = "SELECT smj_FuelLine_ID, actualhundred, hundredend FROM smj_FuelSales WHERE smj_fuelsales_ID=?";
		result = DB.getSQLValueObjectsEx(null, sql, smj_fuelsaleID);
		
		return result;	
	}//getDataFuelLine
	
	public boolean deleteHistoryFuelSales(int recordID, String trxName){
		String sql ="";
		int noquery = 0;

		for(int i=0;i<fuelSalesIDs.size();i++){
			MRecentItem ri = new MRecentItem(Env.getCtx(), getAD_RecentItem_ID(fuelSalesIDs.get(i)), trxName);
			if (ri.get_ID()>0){
				ri.deleteEx(true);
			}	
		}
		if(orderID==0)
			sql = "DELETE FROM smj_FuelSales WHERE smj_FuelSales_ID=?";
		else
			sql = "DELETE FROM smj_FuelSales WHERE C_Order_ID=?";
		
		if (log.isLoggable(Level.INFO)) log.fine(sql.toString());
		
		noquery = DB.executeUpdateEx(sql, new Object[]{recordID}, trxName);
		
		if(noquery > 0){
			System.out.print("Delete HistoryFuelSales - "+"@OK@"+":"+noquery+"\n");
			log.log(Level.INFO, "Delete HistoryFuelSales - "+"@OK@"+":"+noquery);
			return true;
		}
		System.out.print("Delete HistoryFuelSales - "+"@ERROR@"+":"+noquery+" - sql:"+sql+recordID+"\n");
		return false;
		
	}//deleteHistoryFuelSales
	
	public int getCOrderID (int fuelSale_ID ){
		
		String sql = "SELECT C_Order_ID FROM smj_FuelSales WHERE smj_FuelSales_ID=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] {fuelSale_ID});
		if (noquery>0)
			return noquery;
		
		return 0;
	}//getCOrderID
	
	public int getAD_RecentItem_ID (Object fuelSale_ID){
		String sql = "SELECT ad_recentitem_id FROM ad_recentitem WHERE ad_table_id=? AND record_id=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] {tab.getAD_Table_ID(), fuelSale_ID});
		if (noquery>0)
			return noquery;
		
		return 0;
	}
	
	public int getRCchangeFuelLine (int fuelSale_ID ){
		
		String sql = "SELECT smj_rchangefuelline_id FROM smj_FuelSales WHERE smj_FuelSales_ID=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] {fuelSale_ID});
		if (noquery>0)
			return noquery;
		
		return 0;
	}//getCOrderID
	
	public int getFuelLineID (int fuelSale_ID ){
		
		String sql = "SELECT smj_FuelLine_ID FROM smj_FuelSales WHERE smj_FuelSales_ID=?";
		int noquery = DB.getSQLValue(null, sql, new Object[] {fuelSale_ID});
		if (noquery>0)
			return noquery;
		
		return 0;
	}//getCOrderID

	public List<List<Object>> getsmjFuelSales(int recordID){
		
		List<List<Object>> result = new Vector<List<Object>>();
		String sql = "";
		if(orderID==0)
			sql = "SELECT smj_fuelsales_id FROM smj_FuelSales WHERE smj_FuelSales_ID=?";
		else
		    sql = "SELECT smj_fuelsales_id FROM smj_FuelSales WHERE C_Order_ID=?";
		result = DB.getSQLArrayObjectsEx(null, sql, recordID);
		
		
		return result;
	}//getsmjFuelsales
	

}
