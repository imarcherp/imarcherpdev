package co.imarch.toolbar.action;

import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.GridTab;
import org.compiere.model.MArchive;
import org.compiere.model.PO;
import org.compiere.model.PrintInfo;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Vbox;

public class ArchiveLoadWindow extends Window implements EventListener<Event> , ValueChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1225467443794105168L;

	protected CLogger			log = CLogger.getCLogger (super.getClass());
	
	//Elementos
	private ConfirmPanel confirmpanel = new ConfirmPanel(true);
	private Button bLoad = new Button();
	private Label lbfile = new Label("Archivo a Cargar:");
	private Textbox txtfile = new Textbox();
	
	private int recordID=0;
	private int tableID = 0;
	private Object t_C_BPartner_ID = 0;
	private Object t_DocumentNo = 0;
	private Media archivoUp;
	
	ADWindowContent panel;
	GridTab tab;
	PO po;
	
	public ArchiveLoadWindow() {
		
	}
	
	public void init(ADWindowContent panel) {
		
		this.panel = panel;
		this.tab = panel.getActiveGridTab();
		recordID = panel.getActiveGridTab().getRecord_ID();
		tableID = panel.getActiveGridTab().getAD_Table_ID();
		t_C_BPartner_ID = panel.getActiveGridTab().getField("C_BPartner_ID").getValue();
		t_DocumentNo = panel.getActiveGridTab().getField("DocumentNo").getValue();
		
		Grid parameterLayout = GridFactory.newGridLayout();
		ZKUpdateUtil.setWidth(parameterLayout, "99%");
		parameterLayout.setStyle("padding: 5px;");
		
		setTitle("Guardar documento en Archivador");
		setWidth("380px");
		setClosable(true);
		setSizable(true);
		setBorder("normal");
		setStyle("position:absolute");
		
		Vbox vb = new Vbox();
		appendChild(vb);
		
		//lbask.setWidth("100%");
		//lbask.setStyle("color: blue");
		//vb.appendChild(lbask);
		
		
		bLoad.setImage(ThemeManager.getThemeResource("images/Import16.png"));
		bLoad.setSclass("img-btn");
		bLoad.setId("bLoad");
		bLoad.setTooltiptext(Msg.getMsg(Env.getCtx(), "Load"));
		bLoad.setUpload("multiple=true," + AdempiereWebUI.getUploadSetting());
		bLoad.setLabel("Seleccionar archivo a cargar");
		bLoad.setWidth("100%");
		bLoad.addEventListener(Events.ON_UPLOAD, this);
		
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();
		
		row.appendCellChild(bLoad,3);
		row = rows.newRow();
		ZKUpdateUtil.setWidth(lbfile, "40px");
		row.appendCellChild(lbfile.rightAlign(),1);
		ZKUpdateUtil.setWidth(txtfile, "100%");
		row.appendCellChild(txtfile,2);
		txtfile.setReadonly(true);
		//row.appendCellChild(bLoad,1);
		
		vb.appendChild(parameterLayout);
		
		confirmpanel.getButton("Ok").setEnabled(false);

		Div div = new Div();
		div.appendChild(new Label());
		vb.appendChild(div);
		
		vb.appendChild(confirmpanel);
		confirmpanel.addActionListener(this);
		
	}
	
	
	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event event) throws Exception {

		if(event.getTarget().equals(confirmpanel.getButton("Cancel"))){
			onClose();
		}
		
		else if(event.getTarget().equals(confirmpanel.getButton("Ok"))){
			confirmpanel.getButton("Cancel").setEnabled(false);
			if (archivoUp != null && archivoUp.getByteData().length>0)
				cmd_archive(archivoUp.getByteData(), t_DocumentNo.toString());
			
		}
		
		else if (event instanceof UploadEvent) {	
			UploadEvent ue = (UploadEvent) event;
			for (Media media : ue.getMedias()) {
				processUploadMedia(media);
			}
		}
		
	}
	
	private void processUploadMedia(Media media) {
		
		archivoUp=null;
		if(!media.getFormat().toLowerCase().equals("pdf")) {
			Messagebox.showDialog("Tipo de Archivo no valido", "Aviso", 0, null);
			confirmpanel.getButton("Ok").setEnabled(false);
			media=null;
			txtfile.setValue("");
			return;
		}
		
		if (media != null && media.getByteData().length>0) {
			confirmpanel.getButton("Ok").setEnabled(true);
		}
		else {
			Messagebox.showDialog("El Archivo no contiene datos", "Aviso", 0, null);
			media=null;
			txtfile.setValue("");
			return;
		}
		
		String fileName = media.getName();
			
		log.config(fileName);
		txtfile.setValue(fileName);
		archivoUp = media;
		
	}

	private void cmd_archive (byte[] data, String Name)
	{
		boolean success = false;
		
		
		//byte[] data = Document.getPDFAsArray(m_reportEngine.getLayout().getPageable(false));	//	No Copy
		if (data != null)
		{
			PrintInfo info = new PrintInfo(Name, tableID, recordID, Integer.valueOf(t_C_BPartner_ID.toString()));
			MArchive archive = new MArchive (Env.getCtx(), info, null);
			archive.setBinaryData(data);
			success = archive.save();
		}
		if (success) {
			FDialog.info(panel.getWindowNo(), this, "Archived");
			onClose();
		}
		else {
			FDialog.error(panel.getWindowNo(), this, "ArchiveError");
		}
	}	//	cmd_archive
	
}
