package co.imarch.toolbar.action;

import org.adempiere.webui.action.IAction;
import org.adempiere.webui.adwindow.ADWindow;
import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.apps.AEnv;

public class DeletePostedAction implements IAction{
	
	/**
	 * default constructor
	 */
	public DeletePostedAction(){
	}

	@Override
	public void execute(Object target) {
		ADWindow adwin = (ADWindow) target;
		ADWindowContent panel = adwin.getADWindowContent();
		
		doDeletePosted(panel);
	}
	
	private void doDeletePosted(ADWindowContent pwin){
		
		DeletePostedWindow dfswin = new DeletePostedWindow();
		dfswin.init(pwin);
		dfswin.setClosable(false);
		AEnv.showWindow(dfswin);
		
	}

}
