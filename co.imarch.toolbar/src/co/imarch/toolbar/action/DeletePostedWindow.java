package co.imarch.toolbar.action;

import org.adempiere.webui.adwindow.ADWindowContent;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridTab;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.idempiere.base.SpecialEditorUtils;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Vbox;


public class DeletePostedWindow extends Window implements EventListener<Event> , ValueChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3220194133441766313L;
	
	protected CLogger			log = CLogger.getCLogger (super.getClass());
	
	//Elementos
	private ConfirmPanel confirmpanel = new ConfirmPanel(true);
	private Label lbask = new Label("�Desea Eliminar los Asientos Contables?");
	
	private Checkbox chkPostedZero = new Checkbox();
	private Checkbox chkNoPosted = new Checkbox();
	
	
	private int recordID=0;
	
	ADWindowContent panel;
	GridTab tab;
	PO po;
	
	public DeletePostedWindow(){
	}
	
	public void init(ADWindowContent panel) {
		
		this.panel = panel;
		this.tab = panel.getActiveGridTab();
		recordID = panel.getActiveGridTab().getRecord_ID();
		chkNoPosted.setChecked(true);
		chkNoPosted.setText(" Dejar como No Contabilizado");
		chkNoPosted.addActionListener(this);
		chkPostedZero.setChecked(false);
		chkPostedZero.setText(" Dejar Contabilidad en Cero");
		chkPostedZero.addActionListener(this);
				
		setTitle("Borrar Contabilidad");
		setWidth("280px");
		setClosable(true);
		setSizable(true);
		setBorder("normal");
		setStyle("position:absolute");
		
		Vbox vb = new Vbox();
		appendChild(vb);
		
		lbask.setWidth("100%");
		lbask.setStyle("color: blue");
		vb.appendChild(lbask);
		
		vb.appendChild(chkNoPosted);
		vb.appendChild(chkPostedZero);
		
		confirmpanel.getButton("Ok").setEnabled(true);

		Div div = new Div();
		div.appendChild(new Label());
		vb.appendChild(div);
		
		vb.appendChild(confirmpanel);
		confirmpanel.addActionListener(this);
		
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getTarget().equals(confirmpanel.getButton("Cancel"))){
			onClose();
		}
		else if(event.getTarget().equals(confirmpanel.getButton("Ok"))){
			confirmpanel.getButton("Cancel").setEnabled(false);
			onClose();
			deletePosted();	
			panel.onRefresh(true,false);
		}
		else if(event.getTarget().equals(chkNoPosted)){
			chkNoPosted.setChecked(true);
			chkPostedZero.setChecked(false);
		}
		else if(event.getTarget().equals(chkPostedZero)){
			chkNoPosted.setChecked(false);
			chkPostedZero.setChecked(true);
		}	
		
	}//onEvent

		
	public void deletePosted() {
		
		po = tab.getTableModel().getPO(tab.getCurrentRow());
		if(po!=null){ 
			SpecialEditorUtils.deletePosting(po);
			if(chkPostedZero.isChecked()){
				po.set_ValueOfColumn("Posted", true);
				po.saveEx();
			}
			System.out.print("\n Process deletePosting - RecordID:"+recordID+"\n");
		}
		System.out.print("\n No Process deletePosting - PO=null\n");
		

	}//deletePosted

}
