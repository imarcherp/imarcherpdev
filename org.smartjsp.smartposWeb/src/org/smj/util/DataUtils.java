package org.smj.util;

import java.io.StringWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Trx;

/**
 * Class with utility methods for strings
 * 
 * @author pedrorozo
 * @author Dany Diaz
 *
 */
public class DataUtils {

	/**
	 * Method that return a BigDecimal as minimum unit taking as
	 * reference the number of decimals
	 * 
	 * @param numDecimals Number of decimals
	 * @return Minimum Unit
	 */
	public static BigDecimal getMinUnit(int numDecimals) {
		String minUnit = "1";

		for (int i = 0; i < numDecimals; i++) {
			minUnit = "0" + minUnit;
		}

		if (numDecimals > 0) {
			minUnit = minUnit.substring(0, 1) + "." + minUnit.substring(1, minUnit.length());
		}

		return new BigDecimal(minUnit);
	}

	static public String getWithoutNull(String var) {
		String var2 = "";
		if (var == null || var.equals("null"))
			return var2;
		else
			return var;
	}

	/**
	 * Mashall the PO to XML
	 * 
	 * @param po
	 * @return
	 */

	static public String getXmlRepresentation(PO po) {
		try {
			if (null != po) {
				StringWriter res = new StringWriter();
				XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(res);

				// writer.writeStartDocument("UTF-8","1.0");
				writer.writeStartDocument();
				writer.writeStartElement("entityDetail");
				writer.writeStartElement("type");
				writer.writeCharacters(po.get_TableName().substring(po.get_TableName().indexOf('_') + 1).toUpperCase());
				writer.writeEndElement();
				writer.writeStartElement("detail");
				Field[] fields = po.getClass().getFields();
				for (int i = 0; i < fields.length; i++) {
					Field tmp = fields[i];
					if (tmp.getName().contains("COLUMNNAME")) {
						writer.writeStartElement(tmp.getName().substring(11));
						// writer.writeCharacters(po.get_DisplayValue(tmp.getName().substring(11),
						// true));
						// System.out.println("Campo:"+tmp.getName().substring(11)+"--Valor:"+po.get_Value(tmp.getName().substring(11)));
						String campo = "";
						if (po.get_Value(tmp.getName().substring(11)) != null) {
							// System.out.println(tmp.getName()+":Tipo
							// Campo:"+po.get_Value(tmp.getName().substring(11)).getClass());

							// if
							// (po.get_Value(tmp.getName().substring(11)).getClass().getSimpleName().equalsIgnoreCase("String"))
							campo = (String) (po.get_ValueAsString(tmp.getName().substring(11)));
							if (campo == null)
								campo = "nulo";
						} else
							campo = po.get_DisplayValue(tmp.getName().substring(11), true);
						writer.writeCharacters(campo);
						writer.writeEndElement();
					}
				}
				// System.out.println("Nombre de
				// Tabla"+po.get_TableName().toUpperCase().trim());
				// calculo del total de existencias para cuando el PO es de tipo
				// STORAGEONHAND
				if ((po.get_TableName().substring(po.get_TableName().indexOf('_') + 1).toUpperCase().trim()
						.equalsIgnoreCase("STORAGEONHAND"))) {
					String idCampo = po.get_ValueAsString("M_Product_ID");
					int totalProducto = 0;

					// Properties ctx = Env.getCtx();
					PreparedStatement pstmt = null;
					ResultSet rs = null;
					Trx trx = Trx.get(Trx.createTrxName("AL"), true);
					try {
						// System.out.println("calcula sumatoria de existencias
						// en m_storageonhand");
						StringBuffer sql = new StringBuffer();
						sql.append("select sum(qtyonhand) from m_storageonhand  ");
						sql.append("where m_product_id = " + idCampo);
						// System.out.println(sql);
						// System.out.println(sql);

						pstmt = DB.prepareStatement(sql.toString(), trx.getTrxName());
						rs = pstmt.executeQuery();

						if (rs.next()) {
							totalProducto = rs.getInt(1);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Sincronizacion con POS tuvo errores consultar log !";
					} finally {
						DB.close(rs, pstmt);
						trx.close();
						rs = null;
						pstmt = null;
					}

					writer.writeStartElement("M_Product_TotalStock");
					writer.writeCharacters(Integer.toString(totalProducto));
					writer.writeEndElement();
				}

				writer.writeEndElement(); // detail
				writer.writeEndElement(); // entityDetail
				writer.writeEndDocument();
				return res.toString();
			} else {
				return null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
