package org.smj.util;

import java.util.Arrays;

import org.adempiere.webui.apps.AEnv;
import org.compiere.model.MQuery;


/**
 * Clase para abrir ventanas de forma asincronica en un thread 
 * independiente
 * @author pedrorozo, Dany Diaz - SmartJSP
 *
 */
public class OpenWindow implements Runnable {

	public Integer AD_Window_ID;
	public String tableName;
	public Integer[] records; 
	
	@Override
	public void run() {
		String whereString = tableName + "_ID IN " + Arrays.asList(records).toString().replace("[", "(").replace("]", ")");
		  MQuery query = new MQuery(tableName); 
		  query.addRestriction(whereString);
		  AEnv.zoom(AD_Window_ID, query);
	}

}
