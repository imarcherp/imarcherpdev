package org.smj.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class ReplenishStock extends SvrProcess {

	private Integer mWarehouseID;
	private BigDecimal qtyOnHand;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (ProcessInfoParameter processInfoParameter : para) {
			String name = processInfoParameter.getParameterName();
			
			if (name.equals("M_Warehouse_ID")) {
				mWarehouseID = processInfoParameter.getParameterAsInt();
			} else if (name.equals("qty")) {
				qtyOnHand = processInfoParameter.getParameterAsBigDecimal();
			}
		}
	}

	@Override
	protected String doIt() throws Exception {
		try {
			createLines();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		
		return Msg.translate(Env.getCtx(), "SMJ-MSGPROCESSSUCCESFULL");
	}

	/**
	 * Obtiene los datos que cumplan con los filtros
	 * Gets the data that meet the filter
	 * @return
	 */
	private ArrayList<HashMap<String, Object>> getDataFiltered() throws Exception {
		StringBuffer sql = new StringBuffer()
			.append("SELECT p.M_Product_ID, w.M_Warehouse_ID, SUM(s.qtyonhand) AS qtyonhand, p.name AS product, w.name AS warehouse ")
			.append("FROM M_Storage s ")
			.append("JOIN M_Locator l ON l.M_Locator_ID = s.M_Locator_ID ")
			.append("JOIN M_Product p ON p.M_Product_ID = s.M_Product_ID ")
			.append("JOIN M_Warehouse w ON w.M_Warehouse_ID = l.M_Warehouse_ID ")
			.append("WHERE w.M_Warehouse_ID = " + mWarehouseID + " ")
			.append("GROUP BY p.M_Product_ID, w.M_Warehouse_ID ")
			.append("HAVING SUM(qtyonhand) < " + qtyOnHand);
		
		ArrayList<HashMap<String, Object>> dataFiltered = new ArrayList<HashMap<String,Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				HashMap<String, Object> row = new HashMap<String, Object>();
				row.put("M_Warehouse_ID", rs.getInt("M_Warehouse_ID"));
				row.put("M_Product_ID", rs.getInt("M_Product_ID"));				
				row.put("qtyonhand", rs.getBigDecimal("qtyonhand"));
				row.put("warehouse", rs.getString("warehouse"));
				row.put("product", rs.getString("product"));
				dataFiltered.add(row);
			}
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return dataFiltered;
	}
	
	/**
	 * Crea los registros de la tabla t_smj_storage
	 * Create records t_smj_storage table
	 * @throws Exception
	 */
	private void createLines() throws Exception {
		ArrayList<HashMap<String, Object>> lines = getDataFiltered();
		
		int clientId = Env.getAD_Client_ID(Env.getCtx());
		int orgId = Env.getAD_Org_ID(Env.getCtx());
		
		StringBuffer sql = new StringBuffer()
			.append("TRUNCATE T_SMJ_Storage RESTART IDENTITY;");
		
		DB.executeUpdate(sql.toString(), null);
		
		if (lines == null || lines.isEmpty()) 
			return;
		
		
		sql = new StringBuffer()
			.append("INSERT INTO T_SMJ_Storage(m_warehouse_id, m_product_id, qtyonhand,")
			.append(" warehousename, product, ad_client_id, ad_org_id, qty) VALUES ");
		
		for (int i = 0; i < lines.size(); i++) {
			
			if (i != 0)
				sql.append(",");
			
			HashMap<String, Object> line = lines.get(i);
			sql.append("(" + line.get("M_Warehouse_ID"))
			   .append("," + line.get("M_Product_ID"))
			   .append("," + line.get("qtyonhand"))
			   .append(",'" + line.get("warehouse") + "'")
			   .append(",'" + line.get("product") + "'")
			   .append("," + clientId)
			   .append("," + orgId)
			   .append("," + qtyOnHand + ")");
		}
		
		DB.executeUpdateEx(sql.toString(), null);			
	}
}
