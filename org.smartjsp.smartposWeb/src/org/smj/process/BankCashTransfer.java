package org.smj.process;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MCharge;
import org.compiere.model.MPayment;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJCloseCash;
import com.smj.util.DataQueries;
import com.smj.util.SMJConfig;

/**
 * 
 * @author Dany - SmartJSP
 *
 */
public class BankCashTransfer extends SvrProcess {

	private MWarehouse warehouse;
	private int p_To_C_BankAccount_ID;
	private int p_From_C_BankAccount_ID;
	private int p_C_BPartner_ID;
	private BigDecimal p_Amount;
	private String p_Description;
	private Timestamp p_StatementDate;
	private int p_SMJ_Sequence;
	private int p_SMJ_Sequence_Verification;
	private int p_SMJ_CloseCash_ID;
	
	private int p_AD_Client_ID;
	private int p_AD_Org_ID;
	private int p_AD_User_ID;
	private int p_C_Currency_ID;
	private int p_C_Charge_ID;

	@Override
	protected void prepare() {
		p_AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		p_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		p_AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		p_C_Currency_ID = DataQueries.getCurrencyDefault(p_AD_Client_ID);
		p_C_Charge_ID = SMJConfig.getIntValue("C_ChargeDeposit_ID", 0, p_AD_Org_ID);
		
		ProcessInfoParameter[] listPar = getParameter();

		for (ProcessInfoParameter par : listPar) {
			if (par.getParameterName().equals("From_C_BankAccount_ID")) {
				p_From_C_BankAccount_ID =  par.getParameterAsInt();
			} else if (par.getParameterName().equals("To_C_BankAccount_ID")) {
				p_To_C_BankAccount_ID = par.getParameterAsInt();
			} else if (par.getParameterName().equals("C_BPartner_ID")) {
				p_C_BPartner_ID = par.getParameterAsInt();
			} else if (par.getParameterName().equals("Amount")) {
				p_Amount = par.getParameterAsBigDecimal();
			} else if (par.getParameterName().equals("Description")) {
				p_Description = par.getParameterAsString();
			} else if (par.getParameterName().equals("StatementDate")) {
				p_StatementDate = par.getParameterAsTimestamp();
			} else if (par.getParameterName().equals("SMJ_Sequence")) {
				p_SMJ_Sequence = par.getParameterAsInt();
			} else if (par.getParameterName().equals("SMJ_Sequence_Verification")) {
				p_SMJ_Sequence_Verification = par.getParameterAsInt();
			}
		}
	}

	@Override
	protected String doIt() throws Exception {
		// Validate that all required parameters are OK
		validate();

		createPayment(p_From_C_BankAccount_ID, false); // From Payment
		createPayment(p_To_C_BankAccount_ID, true); // To Payment

		return "OK";
	}

	/**
	 * Method for Validate that all required parameters are OK
	 * @throws Exception 
	 */
	private void validate() throws Exception {		
		warehouse = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID")));
		
		// Validate if Amount is Greater than Zero
		if (p_Amount.signum() < 1)
			throw new AdempiereUserError (Msg.getMsg(getCtx(), "SMJMSGValueGreaterZero"));

		// Validate if warehouse is OK
		if (warehouse == null) 
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(), "SMJMSGEnterWithWarehouse"));

		// Validate if warehouse is POS
		if (!warehouse.get_ValueAsBoolean("ispos")) 
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(),"SMJ_MsgWarehouseIsNotPOS"));
		
		int AD_Role_ID = warehouse.get_ValueAsInt("AD_Role_ID");		
		String sqlUser = "SELECT 1 FROM AD_User_Roles WHERE AD_User_ID = ? AND AD_Role_ID = ?";
		boolean isAvailable = DB.getSQLValue(null, sqlUser, p_AD_User_ID, AD_Role_ID) > 0;
	
		if (!isAvailable) 
			throw new AdempiereException(Msg.getMsg(Env.getCtx(), "SMJMSGRolNoAllowOperation"));

		// Validate if p_SMJ_Sequence it is equal to p_SMJ_Sequence_Verification
		if (p_SMJ_Sequence != p_SMJ_Sequence_Verification)
			throw new AdempiereException(Msg.getMsg(Env.getCtx(), "SMJ_MsgSeqVerification"));
		
		// Validate if C_Charge_ID is OK
		if (MCharge.get(Env.getCtx(), p_C_Charge_ID) == null) 
			throw new AdempiereException(Msg.parseTranslation(Env.getCtx(), "@NotValid@: @C_Charge_ID@: " + p_C_Charge_ID + " - @AD_Org_ID@: " + p_AD_Org_ID));
		
		if (p_To_C_BankAccount_ID == 0 || p_From_C_BankAccount_ID == 0)
			throw new AdempiereUserError (Msg.parseTranslation(getCtx(), "@FillMandatory@: @To_C_BankAccount_ID@, @From_C_BankAccount_ID@"));

		if (p_To_C_BankAccount_ID == p_From_C_BankAccount_ID)
			throw new AdempiereUserError (Msg.getMsg(getCtx(), "BankFromToMustDiffer"));

		// Validate if SMJ_CloseCash_ID exits
		String sql = "SELECT SMJ_CloseCash_ID FROM SMJ_CloseCash WHERE SMJ_Sequence = ? AND AD_Client_ID = ? AND AD_Org_ID = ? AND M_Warehouse_ID = ? AND IsActive = 'Y'";
		p_SMJ_CloseCash_ID = DB.getSQLValue(get_TrxName(), sql, p_SMJ_Sequence, p_AD_Client_ID, p_AD_Org_ID, warehouse.getM_Warehouse_ID());
		if (p_SMJ_CloseCash_ID < 1)
			throw new AdempiereUserError(Msg.parseTranslation(Env.getCtx(), "@NotValid@: @SMJ_CloseCash_ID@")); // Crear Mensaje
		
		sql = "SELECT COALESCE(SUM(payamt), 0) FROM C_Payment WHERE SMJ_CloseCash_ID = ? AND IsDeposit = 'Y'";
		MSMJCloseCash closeCash = new MSMJCloseCash(Env.getCtx(), p_SMJ_CloseCash_ID, get_TrxName());		
		BigDecimal totalTransferred = DB.getSQLValueBD(get_TrxName(), sql, p_SMJ_CloseCash_ID);
		if ((totalTransferred.add(p_Amount)).compareTo(closeCash.getsmj_total()) > 0) 
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(), "SMJ_MsgAmtSent>TotalAmt") + Msg.parseTranslation(Env.getCtx(), " -> @IsTransferred@: ") + totalTransferred);
	}

	private void createPayment(int C_BankAccount_ID, boolean isReceipt) throws Exception {
		MPayment payment = new MPayment(Env.getCtx(), 0, get_TrxName());
		payment.setAD_Org_ID(p_AD_Org_ID);
		payment.setC_Charge_ID(p_C_Charge_ID);
		payment.setC_BankAccount_ID(C_BankAccount_ID);
		payment.setC_DocType_ID(isReceipt);
		payment.setDateTrx(p_StatementDate);
		payment.setDateAcct(p_StatementDate);
		payment.setC_BPartner_ID(p_C_BPartner_ID);
		payment.setDescription(p_Description);
		payment.setAmount(p_C_Currency_ID, p_Amount);
		payment.setTenderType(MPayment.TENDERTYPE_DirectDeposit);
		payment.set_ValueOfColumn("SMJ_CloseCash_ID", p_SMJ_CloseCash_ID);
		payment.set_ValueOfColumn("isDeposit", !isReceipt);

		payment.saveEx();		
		if (!payment.processIt(MPayment.DOCACTION_Complete)) {
			log.warning("Payment Process Failed: " + payment + " - " + payment.getProcessMsg());
			throw new IllegalStateException("Payment Process Failed: " + payment + " - " + payment.getProcessMsg());
		}		
	}
}
