package org.smj.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJCloseCash;

/**
 * 
 * @author Dany
 *
 */
public class ReverseCloseCash extends SvrProcess {

	private int p_SMJ_CloseCash_ID;
	private int p_M_Warehouse_ID;
	private String p_Description;
	private int p_AD_User_ID;
	
	@Override
	protected void prepare() {
		p_M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		p_AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		
		ProcessInfoParameter[] paras = getParameter();
		for (ProcessInfoParameter par : paras) {
			if (par.getParameterName().equalsIgnoreCase("Description")) {
				p_Description = par.getParameterAsString();
			} else if (par.getParameterName().equalsIgnoreCase("SMJ_CloseCash_ID")) {
				p_SMJ_CloseCash_ID = par.getParameterAsInt();
			}
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		MSMJCloseCash closeCash = new MSMJCloseCash(Env.getCtx(), p_SMJ_CloseCash_ID, get_TrxName());
		if (p_SMJ_CloseCash_ID < 1 || closeCash == null || closeCash.is_new())
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(), "NoRecordID"));
		
		//Validate if Warehouse exists
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), p_M_Warehouse_ID);
		if (warehouse == null) 
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(), "SMJMSGEnterWithWarehouse"));
		
		//Validate if Warehouse is POS
		if (!warehouse.get_ValueAsBoolean("ispos"))
			throw new AdempiereUserError(Msg.getMsg(Env.getCtx(),"SMJ_MsgWarehouseIsNotPOS"));
		
		int AD_Role_ID = warehouse.get_ValueAsInt("AD_Role_ID");		
		String sqlUser = "SELECT 1 FROM AD_User_Roles WHERE AD_User_ID = ? AND AD_Role_ID = ?";
		boolean isAvailable = DB.getSQLValue(null, sqlUser, p_AD_User_ID, AD_Role_ID) > 0;
	
		if (!isAvailable) 
			throw new AdempiereException(Msg.getMsg(Env.getCtx(), "SMJMSGRolNoAllowOperation"));
		
		int seq = closeCash.getSMJ_Sequence().intValue();
	
		
		String sqlInvoice = "UPDATE C_Invoice i SET POReference = 'SmartPOS' "
						+ "FROM C_Order o WHERE o.C_Order_ID = i.C_Order_ID AND o.M_Warehouse_ID = " + p_M_Warehouse_ID + " AND "
						+ "i.POReference = 'POSCloseCash:" + seq + "'";
		DB.executeUpdateEx(sqlInvoice, get_TrxName());
		
		String sqlPayment = "UPDATE C_Payment SET Description = replace(Description, ' POSCloseCash:" + seq + "', '') "
						+ "WHERE C_BankAccount_ID = " + warehouse.get_ValueAsInt("C_BankAccount_ID") + " AND "
						+ "Description LIKE '%POSCloseCash:" + seq + "%'";
		DB.executeUpdateEx(sqlPayment, get_TrxName());
		
		closeCash.set_ValueOfColumn("Description", p_Description);
		closeCash.setIsActive(false);
		closeCash.saveEx();
		
		return "OK";
	}

}
