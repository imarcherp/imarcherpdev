package org.smj.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import org.compiere.model.MWarehouse;
import org.compiere.model.MWindow;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;

import com.smj.model.MSMJCloseCash;
import com.smj.model.MSMJCloseCashLine;
import com.smj.util.DataQueries;
import com.smj.util.ShowWindow;

/**
 * @version <li>SmartJSP: LoadProducts 2012/10/01 <ul TYPE ="circle">
 *          <li>procesa facturas y recibos de cierre de caja por bodegda
 *          <li>process  invoices and payments for close cash per warehouse
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 * @author Pedro Rozo - "SmartJSP" - http://www.smartjsp.com/
 */
public class PrepareCloseCash extends SvrProcess{

	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(PrepareCloseCash.class);
	private String closeCashType ; 
	private static Timestamp processDate;
	private static Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private static Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());
	private static int M_Warehouse_ID = 0;
	private static int C_BankAccount_ID = 0;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	int docTypeInvoices = 0;
	int docTypeCreditMemos = DataQueries.getDocumentTypeId("AR Credit Memo", lclientId);
	//private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private final static String notInvoicesFound = Msg.translate(Env.getCtx(), "SMJMSGNoInvoicesFound").trim();

	/**
	 * obtienen los valores de los parametros - 
	 * get values form parameters
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null);
			else if (name.equals("CloseCashType")) {
				closeCashType = para[i].getParameterAsString();
			}
			else if (name.equals("date_process")) {
				processDate = para[i].getParameterAsTimestamp();				
			}
		}//for
	}//prepare

	/**
	 * inicia el proceso - start process
	 */
	@Override
	protected String doIt() throws Exception {		
		M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		if (M_Warehouse_ID < 1)
			return Msg.getMsg(Env.getCtx(), "SMJMSGEnterWithWarehouse");
		
		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		docTypeInvoices = warehouse.get_ValueAsInt("C_DocTypeTarget_ID");
		
		boolean ispos = warehouse.get_ValueAsBoolean("ispos");
		log.log (Level.SEVERE,"isPos -> " + ispos);
		
		if (!ispos) {
			throw new Exception(Msg.getMsg(Env.getCtx(), "SMJ_MsgWarehouseIsNotPOS"));
		}
		
		C_BankAccount_ID = warehouse.get_ValueAsInt("C_BankAccount_ID");
		
		String msg = "Close cash "+ closeCashType + " ";
		Trx trxcc = Trx.get(Trx.createTrxName("AL"), true);
		//Timestamp systemDate = Env.getContextAsDate(Env.getCtx(), "#Date");
		Timestamp systemDate = processDate;
		
		int latestCCSeq = 0;
		int latestCCId = 0;
		Timestamp latestCCDate = null;
		try {
			// get last sequence
			latestCCSeq = getLastSequence();		
			// look for the date of the first outstanding invoice
			Timestamp invoiceDate = getDateFirstInvoice();
			Timestamp paymentDate = getDateFirstPayment();
			
			if (invoiceDate != null || paymentDate != null) {
				if (invoiceDate == null) {
					latestCCDate = paymentDate;
				} else if (paymentDate == null) {
					latestCCDate = invoiceDate;
				} else {
					if (invoiceDate.compareTo(paymentDate) >= 0) {
						latestCCDate = invoiceDate;
					} else {
						latestCCDate = paymentDate;
					}
				}					
			} else {
				return "No Data";
			}
			
			
			if (latestCCDate == null) {
				return "";
			}
			
			//  reset the time to the latest of the day to include all the payment transactions within the filters
			processDate.setHours(23);
			processDate.setMinutes(59);
			processDate.setSeconds(59);
			
			// look for new invoices since the last close cash
			HashMap<String, Object> invoiceT = getInvoiceTotals();
			HashMap<String, Object> paymentT = getPaymentTotals();
			BigDecimal returnT = getReturnTotals();
			int nInvoices = (Integer)invoiceT.get("smj_totaltickets");
			if (nInvoices > 0 || paymentT.size() > 0 || returnT.compareTo(Env.ZERO) == 1)
			{
				// it creates the new close cash
				MSMJCloseCash ncc = new MSMJCloseCash(Env.getCtx(),0 , trxcc.getTrxName());				
				
				if (nInvoices > 0) {
					ncc.setsmj_totaltickets(nInvoices);
					//ncc.setsmj_totaldiscounts((BigDecimal)invoiceT.get("smj_totaldiscounts"));
					ncc.setsmj_totaldiscounts(getTotalDiscountsInvoice()); 
					ncc.setsmj_total((BigDecimal)invoiceT.get("smj_total"));
				} else {
					ncc.setsmj_totaltickets(0);
					ncc.setsmj_totaldiscounts(new BigDecimal(0));
					ncc.setsmj_total(new BigDecimal(0));										
				}
				
				if (paymentT.size() > 0) {
					BigDecimal totalInTmp = ncc.getsmj_totalin().add((BigDecimal) paymentT.get("smj_total_in"));
					ncc.setsmj_totalin(totalInTmp);	
					ncc.setsmj_totalout((BigDecimal) paymentT.get("smj_total_out"));
					BigDecimal tmpTotal = ((BigDecimal) paymentT.get("smj_total_payments")).add(ncc.getsmj_total());
					ncc.setsmj_total(tmpTotal);
				} else {
					ncc.setsmj_totalin(new BigDecimal(0));
					ncc.setsmj_totalout(new BigDecimal(0));
				}
				
				ncc.setsmj_totalreturns(returnT);
				
				if (returnT.compareTo(Env.ZERO) == 1) {
					BigDecimal tmpTotal = ncc.getsmj_total().subtract(returnT);
					ncc.setsmj_total(tmpTotal);
				}
				
				int m_warehouse_id = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID", "0"));
				
				Map<String, Integer> ids = getInvoiceIDs();
				Map<String, String> docIds = getInvoiceDocIds();
				
				ncc.setsmj_startdate(latestCCDate);
				ncc.setsmj_enddate(systemDate);	
				ncc.setsmj_initial(new BigDecimal(ids.get("first_id")));
				ncc.setsmj_final(new BigDecimal(ids.get("last_id")));
				ncc.setdocno_initial(docIds.get("first_id"));
				ncc.setdocno_final(docIds.get("last_id"));
				ncc.setM_Warehouse_ID(m_warehouse_id);
				
				if (closeCashType.equalsIgnoreCase("Partial") ) {
					ncc.setIsActive(false);
					ncc.setSMJ_Sequence(new BigDecimal(0));
				}
				else {   // close cash is complete
					ncc.setIsActive(true);
					ncc.setSMJ_Sequence((new BigDecimal(latestCCSeq + 1)));
				}

				boolean ok = ncc.save();
				latestCCId =  ncc.getSMJ_CloseCash_ID();
				if (ok) {
					// totals by product as close cash lines
					TreeMap <String, BigDecimal>  qtyProds  = getQtyInvoiceProducts();
					//create close cash lines per product total
					int k = 1;

					// Header for products
					MSMJCloseCashLine nccl = new MSMJCloseCashLine(Env.getCtx(),0 , trxcc.getTrxName());
					nccl.setsmj_closecash_ID(latestCCId);
					nccl.setsmj_sequence(new BigDecimal(k++));
					nccl.setsmj_concept(Msg.translate(Env.getCtx(), "SMJLabelProd"));
					nccl.setsmj_unit(" ");
					nccl.setsmj_totalline(new BigDecimal(0));
					nccl.setsmj_units(new BigDecimal(0));
					ok = nccl.save();

					for (String prod: qtyProds.keySet()){

						nccl = new MSMJCloseCashLine(Env.getCtx(),0 , trxcc.getTrxName());
						nccl.setsmj_closecash_ID(latestCCId);
						nccl.setsmj_sequence(new BigDecimal(k++));
						nccl.setsmj_concept(prod);
						nccl.setsmj_totalline(qtyProds.get(prod ));
						nccl.setsmj_unit(" ");
						nccl.setsmj_units(new BigDecimal(0));
						ok = nccl.save();
					}

					// totals by tendertype on invoice payment as close cash lines
					HashMap <String, BigDecimal>  qtyPays  = getTotPaymentForms(trxcc);

					// Header for payments
					nccl = new MSMJCloseCashLine(Env.getCtx(),0 , trxcc.getTrxName());
					nccl.setsmj_closecash_ID(latestCCId);
					nccl.setsmj_sequence(new BigDecimal(k++));
					nccl.setsmj_concept(Msg.translate(Env.getCtx(), "TenderType"));
					nccl.setsmj_unit(" ");
					nccl.setsmj_totalline(new BigDecimal(0));
					nccl.setsmj_units(new BigDecimal(0));
					ok = nccl.save();

					for (String pay: qtyPays.keySet()){

						MSMJCloseCashLine nccl2 = new MSMJCloseCashLine(Env.getCtx(),0 , trxcc.getTrxName());
						nccl2.setsmj_closecash_ID(latestCCId);
						nccl2.setsmj_sequence(new BigDecimal(k++));
						nccl2.setsmj_concept(pay);
						nccl2.setsmj_totalline(qtyPays.get(pay));
						nccl2.setsmj_unit(" ");
						nccl2.setsmj_units(new BigDecimal(0));
						ok = nccl2.save();
					}
					// mark all the invoices s processed
					if (!closeCashType.equalsIgnoreCase("Partial") ) {							
						
						int AD_User_ID = Integer.parseInt(Env.getCtx().getProperty("#AD_User_ID"));
						int AD_Role_ID = warehouse.get_ValueAsInt("AD_Role_ID");
						
						String sql = "SELECT 1 FROM AD_User_Roles WHERE AD_User_ID = ? AND AD_Role_ID = ?";
						boolean isAvailable = DB.getSQLValue(null, sql, AD_User_ID, AD_Role_ID) > 0;
						
						if (!isAvailable) {
							throw new Exception(Msg.getMsg(Env.getCtx(), "SMJMSGRolNoAllowOperation"));
						}
						
						updateInvoiceCloseCash(trxcc, ncc.getSMJ_Sequence().intValue());
					}
					trxcc.commit();
					trxcc.close();

					// 	show the generated close cash document
					int M_Window_ID = new Query(Env.getCtx(), MWindow.Table_Name, MWindow.COLUMNNAME_AD_Window_UU + " = '1816751e-f44b-4f79-bc0f-7f76895a3954'", null).firstIdOnly();
					ShowWindow.openWindowAsync(M_Window_ID ,"smj_closecash",latestCCId);
				}
				else
				{
					trxcc.rollback();
					trxcc.close();
				}
			}
			else {   // we do not have invoices a warning messages is showed
				msg+= ""+ notInvoicesFound;
			}
		} catch (Exception ex ) {
			trxcc.rollback();
			trxcc.close();
			msg += ex.getMessage();			
		}

		log.log (Level.SEVERE,msg);
		return msg;
	}//doIt
	
	

	/**
	 * obtiene totales de formas de pago por facturas para cierre de caja - 
	 * get total of payment forms of invoice products to close cash 
	 */
	private HashMap <String,BigDecimal> getTotPaymentForms(Trx trxcc){
		StringBuffer sql = new StringBuffer();
		HashMap <String, BigDecimal>  qtyPays = new HashMap <String,BigDecimal>(); 

		sql.append("SELECT p.tendertype, sum(p.payamt) as total ");
		sql.append("FROM c_invoice i, c_payment p, C_Order o  ");
		sql.append("WHERE (p.c_invoice_id = i.c_invoice_id AND i.isactive ='Y'  AND i.ad_client_id = "+ lclientId+
				"  AND i.ad_org_id = "+lorgId+
				"  AND o.C_Order_ID = i.C_Order_ID" +
				"  AND i.poreference = 'SmartPOS') "+
				"  AND i.c_doctype_id = "+docTypeInvoices +
				"  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" + 
				"  AND o.M_Warehouse_ID = " + M_Warehouse_ID +
				"  AND p.docstatus <> '" + DocAction.STATUS_Reversed +"'"+
				"  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"+
				"  GROUP  by p.tendertype ");
		 
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String tenderType = getReferenceListNameByValue(rs.getString("tendertype"),trxcc);
				BigDecimal qty = rs.getBigDecimal("total");
				qtyPays.put(tenderType , qty);	

			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return qtyPays;
	}//

	/**
	 * obtiene cantiddes de productos por facturas para cierre de caja - 
	 * get quantities of invoice products to close cash 
	 */
	private TreeMap <String,BigDecimal> getQtyInvoiceProducts(){
		StringBuffer sql = new StringBuffer();
		TreeMap <String, BigDecimal>  qtyProds = new TreeMap <String,BigDecimal>(); 

		
		sql.append("SELECT il.c_invoice_id,il.c_invoiceline_id,il.m_product_id,p.name,il.qtyinvoiced,il.pricelist,il.linenetamt,il.c_charge_id ");
		sql.append("FROM c_invoice i, c_invoiceline il, m_product p, C_Order o ");
		sql.append("WHERE i.isactive ='Y' AND i.ad_client_id = "+ lclientId+
				"  AND i.ad_org_id = "+lorgId+
				"  AND i.poreference = 'SmartPOS' " +
				"  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" +
				"  AND o.C_Order_ID = i.C_Order_ID" +
				"  AND il.c_invoice_id = i.c_invoice_id  AND p.m_product_id = il.m_product_id  "+ 
				"  AND il.m_product_id IS NOT NULL " +
				"  AND il.c_charge_id IS NULL AND i.c_doctype_id = " + docTypeInvoices + " " +
				"  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"+ 
				" AND o.M_Warehouse_ID = " + M_Warehouse_ID);
		log.log (Level.SEVERE,"SQL getQtyInvoiceProducts() -> " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String prod = rs.getString("name");
				BigDecimal qty = rs.getBigDecimal("qtyinvoiced");
				BigDecimal at = qtyProds.get(prod);
				if (at == null)  // prod was not on list
				{
					qtyProds.put(prod , qty);	
				}
				else {
					qtyProds.remove(prod);
					qtyProds.put(prod , qty.add(at));	
				}

			}//while
			
   // aqui busca las devoluciones 
			qtyProds = 	getQtyInvoiceReturnedProducts(qtyProds);
			
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return qtyProds;
	}//

	
	
	/**
	 * obtiene cantiddes de productos devueltos, para restarlos del cierre de caja - 
	 * get quantities of returned products to substract from close cash quantities 
	 */
	private TreeMap <String,BigDecimal> getQtyInvoiceReturnedProducts(TreeMap <String, BigDecimal>  qtyProds){
		StringBuffer sql = new StringBuffer();
		
		// se localiza primero las notas credito y dentro de las las que tengan devolucion (inventario)
		// dentro de los detalles de la devolocion de buscan las cantidades de productos, para luego restarse a los valores en el listado del cierre de caja
		
		sql.append("SELECT il.c_invoice_id,il.c_invoiceline_id,il.m_product_id,p.name,il.pricelist,il.linenetamt,il.c_charge_id,r.qtyinvoiced ");
		sql.append("FROM c_invoice i, c_invoiceline il, m_product p, C_Order o, m_rmaline r ");
		sql.append("WHERE i.isactive ='Y' AND i.ad_client_id = "+ lclientId+
				"  AND i.ad_org_id = "+lorgId+
				"  AND i.poreference = 'SmartPOS' " +
				"  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" +
				"  AND o.C_Order_ID = i.C_Order_ID" +
				"  AND il.c_invoice_id = i.c_invoice_id  AND p.m_product_id = il.m_product_id  "+ 
				"  AND il.m_rmaline_id = r.m_rmaline_id   "+
				"  AND il.m_product_id IS NOT NULL " +
				"  AND il.c_charge_id IS NULL AND i.c_doctype_id = " + docTypeCreditMemos + " " +    // identifica las notas credito
				"  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"+ 
				" AND o.M_Warehouse_ID = " + M_Warehouse_ID);
		log.log (Level.SEVERE,"SQL getQtyInvoiceProducts() -> " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String prod = rs.getString("name");
				BigDecimal qty = rs.getBigDecimal("qtyinvoiced");
				BigDecimal at = qtyProds.get(prod);
				if (at == null)  // prod was not on list
				{
					 qtyProds.put(prod , qty.multiply(new BigDecimal(-1)));	
				}
				else {
					
					qtyProds.remove(prod);
					if (at.subtract(qty).intValue() > 0) {   // only if there is stock after the return was substracted
					   qtyProds.put(prod , at.subtract(qty));
					}
				}

			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return qtyProds;
	}//
	
	
	/**
	 * obtiene totales por facturas no procesadas para cierre de caja - 
	 * get totals by outsstading invoices to close cash 
	 */
	private Timestamp  getDateFirstPayment(){
		StringBuffer sql = new StringBuffer();
		Timestamp ret = null;

		Integer docTypeIn = DataQueries.getDocumentTypeId("AR Receipt", lclientId);
		Integer docTypeOut = DataQueries.getDocumentTypeId("AP Payment", lclientId);
		
		sql.append("SELECT dateacct FROM c_payment ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = " + lclientId + 
				"  AND ad_org_id = " + lorgId +
				"  AND dateacct <= '"+ sdf.format(processDate)+"'" +
				"  AND (c_doctype_id = " + docTypeIn + " OR c_doctype_id = " + docTypeOut + ") " +
			    "AND C_BankAccount_ID = " + C_BankAccount_ID +
			    "  AND docstatus <> '" + DocAction.STATUS_Reversed +"'"+
				" AND description LIKE 'SmartPOS%' AND description NOT LIKE '%POSCloseCash%'" +
				"  ORDER BY dateacct ASC");
		log.log (Level.SEVERE,"getDateFirstPayment() -> " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				ret = rs.getTimestamp("dateacct");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ret;
	}//

	/**
	 * obtiene totales por facturas no procesadas para cierre de caja - 
	 * get totals by outsstading invoices to close cash 
	 */
	private Timestamp  getDateFirstInvoice(){
		StringBuffer sql = new StringBuffer();
		Timestamp ret = null;
		
		sql.append("SELECT i.dateinvoiced FROM c_invoice i");
		sql.append(" JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID) ");
		sql.append(" WHERE i.isactive ='Y' AND i.ad_client_id = "+ lclientId);
		sql.append("  AND i.ad_org_id = " + lorgId);
		sql.append("  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" );
		sql.append("  AND i.poreference = 'SmartPOS' AND i.c_doctype_id = " + docTypeInvoices + " ");
		sql.append(" AND o.M_Warehouse_ID = " + M_Warehouse_ID );
		sql.append("  AND i.docstatus <> '" + DocAction.STATUS_Reversed+"'");
		sql.append("  ORDER BY i.dateinvoiced ASC ");
		log.log (Level.SEVERE,"getDateFirstInvoice() -> " + sql);
 		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				ret = rs.getTimestamp("dateinvoiced");
				break;
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ret;
	}//

	/**
	 * obtiene totales por facturas no procesadas para cierre de caja - 
	 * get totals by outsstading invoices to close cash 
	 */
	private HashMap <String, Object> getInvoiceTotals(){
		StringBuffer sql = new StringBuffer();
		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		
		sql.append("SELECT  count(1) as smj_totaltickets , sum(i.grandtotal) as smj_total,sum(i.chargeamt) as smj_totaldiscounts  FROM c_invoice i");
		sql.append(" JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID)");
		sql.append(" WHERE i.isactive ='Y' AND i.ad_client_id = "+ lclientId+
				"  AND i.ad_org_id = "+lorgId+
				"  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" +
				"  AND i.poreference = 'SmartPOS' AND i.c_doctype_id = " + docTypeInvoices + " " +
				"  AND i.docstatus <> '" + DocAction.STATUS_Reversed + "'"+
				 " AND o.M_Warehouse_ID = " + M_Warehouse_ID);
		log.log (Level.SEVERE,"getInvoiceTotals() -> " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				ret.put("smj_totaltickets",new Integer(rs.getInt("smj_totaltickets")));
				ret.put("smj_total",rs.getBigDecimal("smj_total"));
				ret.put("smj_totaldiscounts",rs.getBigDecimal("smj_totaldiscounts"));
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ret;
	}//

	/**
	 * Obtiene el total de devoluciones por fecha
	 * get return totals by date
	 */
	private BigDecimal getReturnTotals(){
		StringBuffer sql = new StringBuffer();
		BigDecimal total = new BigDecimal(0);
		

		sql.append("SELECT SUM(i.grandtotal) as smj_total_returns FROM c_invoice i");
		sql.append(" JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID)");
		sql.append(" WHERE i.c_doctype_id = " + docTypeCreditMemos);
		sql.append("  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"' " );
		sql.append(" AND i.isactive ='Y' AND i.ad_client_id = " + lclientId + "  AND i.ad_org_id = " + lorgId + " ");
		sql.append(" AND i.docstatus <> '" + DocAction.STATUS_Reversed+"'");
		sql.append(" AND o.M_Warehouse_ID = " + M_Warehouse_ID);
		sql.append(" AND i.poreference = 'SmartPOS'");
		//System.out.println("SQL -> " + sql);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				total = rs.getBigDecimal("smj_total_returns");
			}
			
			if (total == null) {
				total = Env.ZERO;
			}
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return total;
	}//
	
	/**
	 * Obtiene totales por movimientos de caja (Entrada/Salida).
	 * get totals by cash movements (In/Out)
	 * @return
	 */
	private HashMap<String, Object> getPaymentTotals() {
		StringBuffer sql = new StringBuffer();
		HashMap<String, Object> results = new HashMap<String, Object>();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		Integer docTypeIn = DataQueries.getDocumentTypeId("AR Receipt", lclientId);
		Integer docTypeOut = DataQueries.getDocumentTypeId("AP Payment", lclientId);
        // Se filtran por bodega (POS) tomando el nombre de la bodega que viene en el campo descripcion
		
		sql.append("SELECT (SELECT SUM(payamt) FROM c_payment WHERE c_doctype_id = " + docTypeIn + " ");
		sql.append(" AND isactive ='Y' AND ad_client_id = " + lclientId + "  AND ad_org_id = " + lorgId + "  "  );
		sql.append("  AND dateacct <= '"+ sdf.format(processDate)+"' " );
		sql.append(" AND C_BankAccount_ID = " + C_BankAccount_ID + " ");
		sql.append("  AND docstatus <> '" + DocAction.STATUS_Reversed+"'");
		sql.append(" AND isDeposit = 'N' ");
		sql.append(" AND description LIKE 'SmartPOS%' AND description NOT LIKE '%POSCloseCash%' AND c_invoice_id IS NULL) AS smj_total_in, ");
		
		sql.append("(SELECT SUM(payamt) FROM c_payment WHERE c_doctype_id = " + docTypeOut + "  ");
		sql.append(" AND isactive ='Y' AND ad_client_id = " + lclientId + "  AND ad_org_id = " + lorgId + "  ");	
		sql.append(" AND C_BankAccount_ID = " + C_BankAccount_ID + "  ");
		sql.append("  AND dateacct <= '"+ sdf.format(processDate)+"' " );
		sql.append("  AND docstatus <> '" + DocAction.STATUS_Reversed+"'");
		sql.append(" AND isDeposit = 'N' ");
		sql.append(" AND description LIKE 'SmartPOS%' AND description NOT LIKE '%POSCloseCash%' AND c_invoice_id IS NULL) AS smj_total_out ");
		log.log (Level.SEVERE,"getPaymentTotals() -> " + sql);
		
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())	{
				BigDecimal totalIn = rs.getBigDecimal("smj_total_in");
				BigDecimal totalOut = rs.getBigDecimal("smj_total_out");
				
				if (totalIn == null) {
					totalIn = Env.ZERO;
				}
				
				if (totalOut == null) {
					totalOut = Env.ZERO;
				}
				
				results.put("smj_total_in", totalIn);
				results.put("smj_total_out", totalOut);
				results.put("smj_total_payments", (totalIn.subtract(totalOut)));
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; 
			pstmt = null;
		}

		return results;
	}

	/**
	 * obtiene fecha final y ultima secuencia del ultimo cierre de caja - 
	 * get finish date and last secuence of the latest close cash
	 */
	private int getLastSequence (){
		StringBuffer sql = new StringBuffer();
		int lastSequence = 0;
		
		sql.append("SELECT COALESCE(MAX(smj_sequence),0) AS lastSequence FROM smj_closecash ");
		sql.append("WHERE smj_sequence > 0 AND ad_client_id = "+ lclientId +
				"  AND ad_org_id = " + lorgId +
				" AND M_Warehouse_ID = " + M_Warehouse_ID);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				lastSequence = rs.getInt("lastSequence");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return lastSequence;
	}//

	/**
	 * marca todas las facturas como procesadas por el cierre de caja
	 * change all the invoices as processed ny the close cash
	 * @param trx
	 * @param close cash sequence
	 * @return
	 */
	private void  updateInvoiceCloseCash(Trx trx, Integer closeCashSeq){
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE c_invoice i SET  poreference = 'POSCloseCash:" + closeCashSeq + "' ");
		sql.append(" WHERE i.poreference= 'SmartPOS' AND (select o.M_Warehouse_ID from c_order o where o.c_order_id = i.c_order_id )  = " + M_Warehouse_ID +
				   "  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"+
				   "  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'" +  ";"
				+ "UPDATE c_payment SET description = description || ' POSCloseCash:" + closeCashSeq + "' "
				+ "WHERE description LIKE 'SmartPOS%' AND description NOT LIKE '%POSCloseCash%' AND C_BankAccount_ID = " + C_BankAccount_ID +
				  "  AND docstatus <> '" + DocAction.STATUS_Reversed +"'"
				+ "  AND dateacct <= '"+ sdf.format(processDate)+"'  AND isDeposit = 'N' ");
		log.log (Level.INFO, sql.toString());
		PreparedStatement pstmt = null;

		try{
			pstmt = DB.prepareStatement (sql.toString(), trx.getTrxName());
			pstmt.executeUpdate();
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(pstmt);
			pstmt = null;
		}
		return ;
	}//updateDefaultsalespos


	/**
	 * Obtener el ID de la primera y ultima factura, del cierre de caja
	 */
	private Map<String, Integer> getInvoiceIDs() {
		Map<String, Integer> ids = new HashMap<String, Integer>();
		
		String sql = "SELECT (SELECT COALESCE(MIN(i.C_Invoice_ID), 0) FROM C_Invoice i JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID) "
				+ "WHERE i.POReference = 'SmartPOS' AND o.M_Warehouse_ID = " + M_Warehouse_ID + "  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'"
				+ "  AND i.c_doctype_id = "+docTypeInvoices 
				+ "  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"
				+  ") AS first_id, "
				+ "(SELECT COALESCE(MAX(i.C_Invoice_ID), 0) FROM C_Invoice i JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID)"
				+ " WHERE i.POReference = 'SmartPOS' AND o.M_Warehouse_ID = " + M_Warehouse_ID + "  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'"
				+ "  AND i.c_doctype_id = "+docTypeInvoices 
				+ "  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"
				+ ") AS last_id";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql, null);
			rs = st.executeQuery();
			
			if (rs.next()) {
				ids.put("first_id", rs.getInt("first_id"));
				ids.put("last_id", rs.getInt("last_id"));
			}
		} catch (Exception e) {
			log.log (Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, st);
		}
		
		return ids;
	}

	/**
	 * Obtener el documentNo de la primera y ultima factura, del cierre de caja
	 */
	private Map<String, String> getInvoiceDocIds() {
		Map<String, String> ids = new HashMap<String, String>();
		
		String sql = "SELECT (SELECT COALESCE(MIN(i.C_Invoice_ID), 0) FROM C_Invoice i JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID) "
				+ "WHERE i.POReference = 'SmartPOS' AND o.M_Warehouse_ID = " + M_Warehouse_ID 
				+ "  AND i.c_doctype_id = "+docTypeInvoices 
				+ "  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'"
				+ "  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'"+ ") AS first_id, "
				+ "(SELECT COALESCE(MAX(i.C_Invoice_ID), 0) FROM C_Invoice i JOIN C_Order o ON (o.C_Order_ID = i.C_Order_ID)"
				+ " WHERE i.POReference = 'SmartPOS' AND o.M_Warehouse_ID = " + M_Warehouse_ID 
				+ "  AND i.c_doctype_id = "+docTypeInvoices  
				+ "  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'" 
				+ "  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'"+ ") AS last_id";
		
		
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql, null);
			rs = st.executeQuery();
			
			if (rs.next()) {
				ids.put("first_id", getDocIDByInternalIdInvoice(rs.getInt("first_id")));
				ids.put("last_id", getDocIDByInternalIdInvoice(rs.getInt("last_id")));
			}
		} catch (Exception e) {
			log.log (Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, st);
		}
		
		return ids;
	}
	
	
	/**
	 * obtiene el DocID de una factura dado su id interno
	 * get the documentID based in the internal c_invoice_id
	 */
	private String getDocIDByInternalIdInvoice (int c_invoice_id){
		StringBuffer sql = new StringBuffer();
		String docId  = "";
		
		sql.append("SELECT documentno FROM c_invoice ");
		sql.append("WHERE c_invoice_id = " + c_invoice_id);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				docId = rs.getString("documentno");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return docId;
	}//
	

	/**
	 * obtiene el total de descuentos de facturas para un cierre 
	 * get the total of discount for invoices in the current close cahs 
	 */
	private BigDecimal getTotalDiscountsInvoice (){
		StringBuffer sql = new StringBuffer();
		BigDecimal totDiscounts = BigDecimal.ZERO  ;
		
		
        /*
         * Se obtienen de la tabla de ordenes restando el precio de lista del actual y multiplicando por la cantidad,
         * se hace un join con factura para efectuar filtros unicamente, y se omite las notas credito.
         */
		
		sql.append("SELECT  sum((l.pricelist - l. priceactual) * l.qtyordered) as smj_totaldiscounts  FROM c_orderline l"); 
		sql.append("  JOIN C_invoice i ON (i.C_Order_ID = l.C_Order_ID) WHERE i.isactive ='Y' AND i.ad_client_id = "+ lclientId);
		sql.append("  AND i.ad_org_id = "+lorgId); 
		sql.append("  AND i.poreference = 'SmartPOS' "); 
		sql.append("  AND l.M_Warehouse_ID = "+ M_Warehouse_ID );
		sql.append("  AND i.c_doctype_id = " + docTypeInvoices );
		sql.append("  AND i.docstatus <> '" + DocAction.STATUS_Reversed +"'");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sql.append("  AND i.dateinvoiced <= '"+ sdf.format(processDate)+"'");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				totDiscounts = rs.getBigDecimal("smj_totaldiscounts");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return totDiscounts;
	}//
	
	/**
	 * Get string value from a AD reference list 
	 */
	protected String getReferenceListNameByValue( String value, Trx trx ) {
		PreparedStatement	pstmt = null;
		ResultSet 			rs = null;
		String  			key = null; 
		//		String 				id = Msg.getMsg(Env.getCtx(), "currencyId").trim();
		String lang =  Env.getAD_Language( Env.getCtx());		
		StringBuffer sql = new StringBuffer();
		if ( !lang.equalsIgnoreCase("en_US")  )  
		{
			sql.append(" SELECT t.name as name FROM AD_Ref_List l, AD_Ref_List_Trl t ");
			sql.append(" WHERE l.AD_Ref_List_ID = t.AD_Ref_List_ID AND l.value='"+value+"'");
			sql.append(" AND l.isactive='Y' AND t.ad_language = '"+lang +"'");
			sql.append("  AND l.AD_Reference_ID=214  AND l.isactive='Y' ");
		}
		else {   // no busca traducciones si esta en ingles
			sql.append(" SELECT l.value as value, l.name as name FROM AD_Ref_List l");
			sql.append(" WHERE l.AD_Reference_ID=214  AND l.isactive='Y' ");	
			sql.append(" AND  l.value='"+value+"'");
		}


		try{
			pstmt = DB.prepareStatement (sql.toString(), trx.getTrxName());
			rs = pstmt.executeQuery ();

			if(rs.next()){
				key = rs.getString("name");
			}
		}catch(SQLException e){
			log.log (Level.SEVERE, sql.toString(), e);
			e.printStackTrace();
		}finally	{
			DB.close(rs, pstmt); 
			rs = null; pstmt = null;
		}

		return key;
	}


}
