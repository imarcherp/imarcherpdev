package org.smj.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.logging.Level;

import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MReplenish;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.smj.model.MSMJ_Medicine;

import com.smj.util.DataQueries;
import com.smj.util.ShowWindow;

/**
 * @version <li>SmartJSP: LoadProducts 2012/10/01 <ul TYPE ="circle">
 *          <li>proceso para cargar de forma masiva productos
 *          <li>load masive productos process
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class LoadProducts extends SvrProcess {

	private Integer purchaseId = 0;
	private Integer salesId = 0;
	private Integer locatorId = 0;
	private Integer warehouseId = 0;
	private Integer docTypeId = 0;
	private Integer orgId = 0;
	private String printer = "bar";
	private HashMap<String, Integer> tax = new HashMap<String, Integer>();
	private HashMap<String, Integer> category = new HashMap<String, Integer>();
	private HashMap<String, Integer> locators = new HashMap<String, Integer>();
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(LoadProducts.class);

	/**
	 * obtienen los valores de los parametros - 
	 * get values form parameters
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null);
			else if (name.equals("sales_ID")) {
				salesId = para[i].getParameterAsInt();
			} else if (name.equals("purchase_ID")){
				purchaseId = para[i].getParameterAsInt();
			} else if (name.equals("M_Warehouse_ID")){
				warehouseId = para[i].getParameterAsInt();
			} else if (name.equals("C_DocType_ID")){
				docTypeId = para[i].getParameterAsInt();
			} else if (name.equals("printer")){
				printer = (String)para[i].getParameter();	
			}
		}//for
	}//prepare

	/**
	 * inicia el proceso - start process
	 */
	@Override
	protected String doIt() throws Exception {
		getCategory();
		getTax();

		orgId = getOrgId();
		getLocators(orgId); 		
		String msg = start();
		return msg;
	}//doIt

	/**
	 * Start the loading process
	 */

	private String start(){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT I_smj_medicine_ID ");
		sql.append("FROM adempiere.I_smj_medicine WHERE i_isimported = 'N' ");
		int cont = 0;
		String msgReceipt = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Trx trxReceipt = Trx.get(Trx.createTrxName("AL"), true);
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();			
			MInOut io = createMInOut(trxReceipt);
			HashMap<String, Object> result = new HashMap<String, Object>();
			String ok = "";
			while (rs.next ())	{
				Trx trx = Trx.get(Trx.createTrxName("AL"), true);
				Integer id = rs.getInt("I_smj_medicine_ID");
				MSMJ_Medicine m = new MSMJ_Medicine(Env.getCtx(),id , trx.getTrxName());
				locatorId = locators.get((String)(m.getm_locator_value()));	

				result = createProduct(trx, m);
				ok = (String)result.get("MSG");

				if(ok.isEmpty()) {					
					m.setI_IsImported(true);
					m.save();
					trx.commit();
					trx.close();
					Integer code = (Integer)result.get("CODE");
					MProduct product = new MProduct(Env.getCtx(), code, trx.getTrxName());

					if (m.getQty() > 0){
						Boolean lok = createLine(io, product, new BigDecimal(m.getQty()));
						if (lok){
							cont ++;
						}
					}
				} else {
					log.fine(ok + " -> " + id);
					trx.rollback();
					trx.close();
					Trx trx2 = Trx.get(Trx.createTrxName("AL"), true);
					try {
						MSMJ_Medicine me = new MSMJ_Medicine(Env.getCtx(),id , trx2.getTrxName());
						me.setI_ErrorMsg(ok);
						me.save();
						trx2.commit();
					} catch (Exception e) {
						trx2.rollback();						
					} finally {
						trx2.close();
						trxReceipt.close();
						DB.close(rs, pstmt);
						rs = null; pstmt = null;
					}

					throw new Exception(ok);
				}

			}//while

			try{
				if (cont > 0){
					io.completeIt();
					io.setDocAction(DocAction.ACTION_Close);
					io.setDocStatus(DocAction.STATUS_Completed);
					Boolean rok = io.save();
					if (!rok) {
						trxReceipt.rollback();
						trxReceipt.close();
					}else{
						trxReceipt.commit();
						trxReceipt.close();
					}
					msgReceipt = " - Receipt: "+io.getDocumentNo();

					//				 	show the generated material receipt
					ShowWindow.openWindowAsync( 184 ,"M_InOut",io.get_ID());
					return Msg.translate(Env.getCtx(), "SMJ-MSGPROCESSSUCCESFULL") + msgReceipt;
				}
			} catch (Exception ex) {
				msgReceipt = "Receipt Error: " + ex.getMessage();
				throw new Exception(msgReceipt);
			}
		} catch (Exception e){
			trxReceipt.rollback();
			log.log (Level.SEVERE, sql.toString(), e);
			return e.getMessage();
		} finally	{
			trxReceipt.close();
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		return "Error, please check your log files";		
	}//start

	/**
	 * crea el producto - create product
	 * @param trx
	 * @param m
	 * @return HashMap<String, Object> 
	 */
	private HashMap<String, Object>  createProduct(Trx trx, MSMJ_Medicine m) throws Exception {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Boolean ok = true;
		String name = m.getName().replaceAll("[']"," ");
		Integer productId = getProductByName(name);
		Integer cUomId = DataQueries.getUOMByName(m.getuom_name(), trx.getTrxName());

		MProduct p = new MProduct(Env.getCtx(), productId, trx.getTrxName());
		p.setName(name);
		String varcode = m.getcode_pharma_ml().replaceAll("[.]","");
		p.setUPC(varcode);
		p.setValue(varcode);
		p.setClassification(m.getcode_cip());   //max 12
		p.setDescription(m.getManufacturer());
		p.setDocumentNote(m.getschedule());
		p.setAD_Org_ID(orgId);
		p.setGroup2(printer);
		p.setIsActive(m.issmj_active());
		p.set_ValueOfColumn("smj_points", m.getPoints());

		Boolean isNew = productId == 0;

		if (isNew) {
			if (cUomId != null && cUomId > 0) {
				p.setC_UOM_ID(cUomId);	
			} else {
				log.severe(name+Msg.translate(Env.getCtx(), "SMJ-MSGUOMNOTFOUND")+" - "+m.getuom_name());
				result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGUOMNOTFOUND")+" - "+m.getuom_name());
				result.put("CODE", productId);
				return result;
			}
		}

		if (locatorId == null) {
			log.severe(name+Msg.translate(Env.getCtx(), "SMJ-MSGLOCATORNOTFOUND") + " - " + m.getm_locator_value());
			result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGLOCATORNOTFOUND") + " - " + m.getm_locator_value());
			return result;
		}

		if (p.getM_Locator_ID() == 0)  //  just when is a new product      m.getQty() == 0 ||
		{	
			p.setM_Locator_ID(locatorId);
		}  

		Integer taxId = tax.get(m.getsalestax().trim());

		if (taxId != null){
			p.setC_TaxCategory_ID(taxId);
		}else{
			log.severe(name+Msg.translate(Env.getCtx(), "SMJ-MSGTAXCATEGORYNOTFOUND")+" - "+m.getsalestax());
			result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGTAXCATEGORYNOTFOUND")+" - "+m.getsalestax());
			result.put("CODE", productId);
			return result;
		}

		Integer categoryId = category.get(m.getCategory().trim());
		if(categoryId != null){
			p.setM_Product_Category_ID(categoryId);
		}else{
			log.severe(name+Msg.translate(Env.getCtx(), "SMJ-MSGPRODUCTCATEGORYNOTFOUND")+" - "+m.getCategory());
			result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGPRODUCTCATEGORYNOTFOUND")+" - "+m.getCategory());
			return result;
		}
		ok = p.save();
		if(ok) {
			if (isNew) {
				//Create Replenish
				MLocator locator = new MLocator(Env.getCtx(), locatorId, null);
				MReplenish replenish = new MReplenish(Env.getCtx(), 0, trx.getTrxName());
				replenish.setM_Product_ID(p.getM_Product_ID());
				replenish.setM_Warehouse_ID(locator.getM_Warehouse_ID());
				replenish.setM_Locator_ID(locatorId);
				replenish.setReplenishType("0");
				replenish.setLevel_Min(m.getLevel_Min());
				replenish.setLevel_Max(Env.ZERO);
				replenish.save();
			}

			BigDecimal price = new BigDecimal(m.getpurchaseprice());
			BigDecimal salesPrice = new BigDecimal(m.getsellingprice());
			if(productId > 0 ) {
				Boolean pru = true;
				Boolean su = true;
				if (m.getpurchaseprice()>0){    // update purchase list of prices
					MProductPrice pur = MProductPrice.get(Env.getCtx(), purchaseId, productId, trx.getTrxName());
					if(pur != null) {
						pur.set_ValueOfColumn("defaultsalespos", false);
						pur.setPriceStd(price);
						pur.setPriceLimit(price);
						pur.setPriceList(price);
						pur.setAD_Org_ID(orgId);
						pru = pur.save();
					} else {
						MProductPrice purn = new MProductPrice(Env.getCtx(), purchaseId, productId,
								price, price, price, trx.getTrxName());
						purn.set_ValueOfColumn("defaultsalespos", false);
						pru = purn.save();
					}
				}
				if(m.getsellingprice() > 0){   // update sales list of prices
					updateDefaultSalesPos(trx, productId);
					MProductPrice sal =  MProductPrice.get(Env.getCtx(), salesId, productId, trx.getTrxName());
					if(sal != null){
						sal.set_ValueOfColumn("defaultsalespos", true);
						sal.setPriceStd(salesPrice);
						sal.setPriceLimit(salesPrice);
						sal.setPriceList(salesPrice);
						sal.setAD_Org_ID(orgId);
						su = sal.save();
					}else{
						MProductPrice saln = new MProductPrice(Env.getCtx(), salesId, productId,
								salesPrice, salesPrice, salesPrice, trx.getTrxName());
						saln.set_ValueOfColumn("defaultsalespos", true);
						su = saln.save();
					}
				}
				if(!pru || !su){
					result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGERRORPRODUCTPRICE"));
					return result;
				}
			}
			else{
				MProductPrice pur = new MProductPrice(Env.getCtx(), purchaseId, p.getM_Product_ID(),
						price, price, price, trx.getTrxName());
				pur.set_ValueOfColumn("defaultsalespos", false);
				pur.setAD_Org_ID(orgId);
				MProductPrice sal = new MProductPrice(Env.getCtx(), salesId, p.getM_Product_ID(),
						salesPrice, salesPrice, salesPrice, trx.getTrxName());
				sal.set_ValueOfColumn("defaultsalespos", true);
				sal.setAD_Org_ID(orgId);
				Boolean pr = pur.save();
				Boolean s = sal.save();
				if(!pr || !s){
					result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGERRORPRODUCTPRICE"));
					return result;
				}
				productId = p.getM_Product_ID();
			}
		}else{
			result.put("MSG", Msg.translate(Env.getCtx(), "SMJ-MSGERRORPRODUCTSAVE")+" - "+m.getName());
			return result;
		}
		result.put("MSG", "");
		result.put("CODE", productId);
		return result;
	}//createProduct

	/**
	 * cambia todas las los valores de defaultsalespos = false -
	 * change all values of defaultsalespos = false
	 * @param trx
	 * @param productId
	 * @return
	 */
	private void  updateDefaultSalesPos(Trx trx, Integer productId){
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE M_ProductPrice SET defaultsalespos = 'N' ");
		sql.append(" WHERE M_Product_ID= "+productId+" ");
		//		System.out.println("updateDefaultSalesPos SQL::"+sql.toString());

		PreparedStatement pstmt = null;

		try{
			pstmt = DB.prepareStatement (sql.toString(), trx.getTrxName());
			pstmt.executeUpdate();
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(pstmt);
			pstmt = null;
		}
		return ;
	}//updateDefaultsalespos

	/**
	 * obtiene lista de categorias de producto - 
	 * get product category list
	 */
	private void getCategory (){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT M_Product_Category_ID, name FROM M_Product_Category ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		//		System.out.println("getCategory SQL::"+sql.toString());
		category = new HashMap<String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String key = rs.getString("name").trim();
				Integer value = rs.getInt("M_Product_Category_ID");
				category.put(key.trim(), value);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getCategory

	/**
	 * obtiene lista de categorias de impuesto - 
	 * get tax category list
	 */
	private void getTax(){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C_TaxCategory_ID, name FROM C_TaxCategory ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		//		System.out.println("getTax SQL::"+sql.toString());
		tax = new HashMap<String, Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ())	{
				String key = rs.getString("name").trim();
				Integer value = rs.getInt("C_TaxCategory_ID");
				tax.put(key.trim(), value);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return ;
	}//getTax

	/**
	 * Regresa el id del producto buscandolo por nombre - 
	 * returns product id by name
	 * @param nombreProducto
	 * @return Integer
	 */
	private Integer getProductByName(String nombreProducto){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT M_Product_ID, name FROM M_Product ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		sql.append("AND TRIM(name) = '"+nombreProducto.trim()+"' ");
		// System.out.println("getProductByName SQL::"+sql.toString());

		Integer productId = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				productId = rs.getInt("M_Product_ID");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return productId;
	}//getProductByName


	/**
	 * regresa el codigo del tercero - returns BPartner Id
	 * @param SalesRep_ID
	 * @return
	 */
	private Integer getBpartnerId(Integer SalesRep_ID){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C_BPartner_ID, name FROM AD_User ");
		sql.append("WHERE AD_User_ID = "+SalesRep_ID+" ");
		//		System.out.println("getBpartnerId SQL::"+sql.toString());

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				code = rs.getInt("C_BPartner_ID");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//getBpartnerId

	/**
	 * regresa el codigo de la localizacion de tercero - 
	 * returns Location BPartner Id
	 * @param Bpartner_ID
	 * @return
	 */
	private Integer getBpartnerLocation(Integer Bpartner_ID){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C_BPartner_Location_ID, name FROM C_BPartner_Location ");
		sql.append("WHERE C_BPartner_ID = "+Bpartner_ID+" ");
		//		System.out.println("getBpartnerLocation SQL::"+sql.toString());

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				code = rs.getInt("C_BPartner_Location_ID");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//getBpartnerId

	/**
	 * Documento para el manejo de inventarios - 
	 * Document to manage inventory
	 */
	private MInOut createMInOut(Trx trx) {
		Integer bPartnerId = getBpartnerId(Env.getAD_User_ID(Env.getCtx()));
		Integer locationId = getBpartnerLocation(bPartnerId);
		MInOut io = new MInOut(Env.getCtx(), 0, trx.getTrxName());
		io.setAD_Org_ID(orgId);//
		io.setC_DocType_ID(docTypeId);
		io.setDescription("InOut_"+Env.getContextAsDate(Env.getCtx(), "#Date"));
		io.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
		io.setC_BPartner_ID(bPartnerId);
		io.setC_BPartner_Location_ID(locationId);
		io.setM_Warehouse_ID(warehouseId);
		io.setFreightAmt(Env.ZERO);
		io.setChargeAmt(Env.ZERO);
		io.setCreateFrom("N");
		io.setGenerateTo("N");
		io.setCreateConfirm("N");
		io.setCreatePackage("N");
		io.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
		io.setMovementDate(Env.getContextAsDate(Env.getCtx(), "#Date"));
		io.setDateAcct(Env.getContextAsDate(Env.getCtx(), "#Date"));
		io.save();
		return io ;
	}// createMInOut

	/**
	 * Crea la linea del documento de inventario - 
	 * create line for inventory document
	 * @param io
	 * @param productId
	 * @param qty
	 * @return
	 */
	private Boolean createLine(MInOut io, MProduct product, BigDecimal qty){
		MInOutLine line = new MInOutLine(io);
		//line.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		line.setM_InOut_ID(io.getM_InOut_ID());
		line.setM_Product_ID(product.getM_Product_ID());
		line.setM_Locator_ID(locatorId);
		line.setQtyEntered(qty);
		line.setMovementQty(qty);
		line.setM_AttributeSetInstance_ID(0);
		line.setC_UOM_ID(product.getC_UOM_ID());
		line.setDescription("");
		Boolean lok = line.save();
		return lok;
	}//createLine

	/**
	 * regresa el codigo de la organizacion del almacen -  
	 * returns Org Id by from warehouse
	 * @return
	 */
	private Integer getOrgId(){
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT AD_Org_ID FROM M_Warehouse ");
		sql.append(" WHERE M_Warehouse_ID = "+warehouseId+" ");
		//		System.out.println("getOrgId SQL::"+sql.toString());
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ())	{
				code = rs.getInt("AD_Org_ID");
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return code;
	}//getOrgId



	/**
	 * Pre-load all the locators of one irganization in hashMap
	 * @param code
	 * @return
	 */
	protected void getLocators(int orgId){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT m_locator_id,value FROM m_locator WHERE ad_org_id = "+orgId+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;


		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			while (rs.next ()){
				String key = rs.getString("value");
				Integer value = rs.getInt("m_locator_id");
				locators.put(key.trim(), value);
			}
		}catch (Exception e)	{
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{			
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		return ;
	}//getWarehouse





}//LoadProducts
