package org.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueries;
import com.smj.util.SMJConfig;

/**
 * Create an Simple Invoice for insurance companies
 * @author pedrorozo
 *
 */


public class InvoiceInsurance {
	protected CLogger log = CLogger.getCLogger(super.getClass());
	private static Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private static Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());
	
    public void generate(Trx trx, HashMap <String,Object> to,MOrder order)
    {
        MInvoice invoice = createInvoice(trx,to,order);
        createInvoiceLines(invoice, to, trx,order);
        
        if (!invoice.save())
        {
            throw new IllegalStateException("Could not update invoice");
        }
        invoice.completeIt();
        invoice.setDocStatus(DocAction.STATUS_Completed);
        invoice.setDocAction(DocAction.ACTION_Close);
        invoice.save();
        log.log(Level.INFO,"***************** Crea factura Simple desde datos ******************** ");
    }
    
    private MInvoice createInvoice(Trx trx, HashMap <String,Object> to,MOrder order)
    {
    	MInvoice data = new MInvoice(Env.getCtx(), 0, null);
    	data.setClientOrg(order.getAD_Client_ID(), order.getAD_Org_ID());
    	data.setAD_Org_ID(order.getAD_Org_ID());
		data.setIsActive(true);
		data.setDateInvoiced(order.getDateOrdered());
		data.setDateAcct(order.getDateAcct());
		data.setOrder(order);
		data.setC_BPartner_ID(Integer.parseInt((String)to.get("idInsurance")) );   // SMJ 5
		MBPartner bp = new MBPartner(Env.getCtx(), order.getC_BPartner_ID(), trx.getTrxName());
		data.setDescription("Insurance Customer:"+bp.getName()+" -Plan: "+to.get("pName")+" Copay: "+to.get("Copay"));
		data.setC_BPartner_Location_ID((Integer)to.get("iLocationB"));
		data.setC_Currency_ID(DataQueries.getCurrencyDefault(lclientId))   ;
		data.setM_PriceList_ID(Integer.parseInt((String)to.get("PriceListID")));
		data.setPOReference(order.getPOReference());
		data.setProcessed(true);
		data.setPaymentRule("S");
		data.setIsSOTrx(true);
		int docType = getInvoiceDocTypeIdInsuranceFromClient(order.getAD_Client_ID() , trx);
		data.setC_DocType_ID(docType);
		data.setC_DocTypeTarget_ID(docType);
		order.setSalesRep_ID(order.getSalesRep_ID());
		
		order.setIsSOTrx(order.isSOTrx());
		Boolean pok = data.save();
		if (!pok){
			throw new IllegalStateException("Could not Create Invoice for Insurance Company: "+data.toString());
		}
		return data;
    }

    private void createInvoiceLines(MInvoice invoice,HashMap <String,Object> to,Trx trx,MOrder order)
    {
    
    	MOrderLine[] oLineas = order.getLines(); 
    	BigDecimal total = new BigDecimal(0);
    	LinkedList<HashMap<String, Object>> lista = (LinkedList<HashMap<String, Object>>) to.get("m_aLine");
    	
    	
		for (int k = 0; k < oLineas.length ; k ++){
			log.log( Level.SEVERE , "Tamano de lista : " + lista.size());
			
			String productId = "0";
			//log.log( Level.SEVERE , "ProductID : " + oLineas[k].getProduct().get_ID());
			
				MInvoiceLine line = new MInvoiceLine(Env.getCtx(), 0, null);
				line.setAD_Org_ID(oLineas[k].getAD_Org_ID());
				line.set_ValueOfColumn("ad_client_id",oLineas[k].getAD_Client_ID() );	
				line.setIsActive(true);
				line.setC_Invoice_ID(invoice.getC_Invoice_ID());
				line.setC_Charge_ID(oLineas[k].getC_Charge_ID());
				line.setM_Product_ID(oLineas[k].getM_Product_ID());
				MBPartner bp = new MBPartner(Env.getCtx(), order.getC_BPartner_ID(), trx.getTrxName());
				line.setDescription("Insurance Customer:"+bp.getName()+" -Plan: "+to.get("pName")+" Copay: "+to.get("Copay"));
				
				Integer insuranceChargeId = SMJConfig.getIntValue("C_Charge_ID", 0, lorgId);
				
				if (oLineas[k].getC_Charge_ID() == insuranceChargeId   )    // es un cargo por aseguradora
				{	
				line.setQtyEntered(oLineas[k].getQtyEntered());				
				line.setPrice(oLineas[k].getPriceActual() );
				line.setPriceActual(oLineas[k].getPriceActual().multiply(new BigDecimal(-1)));
				line.setPriceList(oLineas[k].getPriceActual().multiply(new BigDecimal(-1))) ;
				line.setQtyInvoiced(oLineas[k].getQtyEntered());
				BigDecimal totalLine = oLineas[k].getQtyEntered().multiply(oLineas[k].getPriceActual());
				//totalLine = totalLine.multiply(new BigDecimal(-1));
				line.setLineNetAmt(totalLine);
				total = total.add(totalLine);
				}
				else {    // es una linea normal del producto comprado, que se dejara en 0, solo para referencia de reportes luego
					line.setQtyEntered(oLineas[k].getQtyEntered());				
					line.setQtyInvoiced(oLineas[k].getQtyInvoiced());
					line.setPriceList(oLineas[k].getPriceList());
					line.setPrice(new BigDecimal(0) );
					line.setPriceActual(new BigDecimal(0));
					//BigDecimal totalLine = oLineas[k].getLineNetAmt();
					line.setLineNetAmt(new BigDecimal(0));
				}
				line.setC_Tax_ID(oLineas[k].getC_Tax_ID());  //taxud
				line.setC_UOM_ID(oLineas[k].getC_UOM_ID()); //   uomid
				Boolean lok = line.save();
				
				if (!lok){
					throw new IllegalStateException("Could not Create Invoice Encounter: "+line.toString());
				}
		}
    	
    	invoice.setTotalLines(total);
    }
    
    

    private int getInvoiceDocTypeId(int M_RMA_ID,Trx trx)
    {
        String docTypeSQl = "SELECT dt.C_DocTypeInvoice_ID FROM C_DocType dt "
            + "INNER JOIN M_RMA rma ON dt.C_DocType_ID=rma.C_DocType_ID "
            + "WHERE rma.M_RMA_ID=?";
        
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, M_RMA_ID);
        
        return docTypeId;
    }
    
    private int getInvoiceDocTypeIdFromClient(int clientId,Trx trx)
    {
        String docTypeSQl = "SELECT dt.c_doctype_id FROM C_DocType dt "
           // + "WHERE dt.ad_client_id=? and name = 'Customer Return Material' and isactive='Y'";
             + "WHERE dt.ad_client_id=? and name = 'AP CreditMemo' and isactive='Y'";
        System.out.println("sql para buscar tipo de factura:"+docTypeSQl+"--"+clientId);	
        	
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);
        
        return docTypeId;
    }
    
    private int getInvoiceDocTypeIdInsuranceFromClient(int clientId,Trx trx)
    {
        String docTypeSQl = "SELECT dt.c_doctype_id FROM C_DocType dt "
                 + "WHERE dt.ad_client_id=? and name = 'AR Invoice Indirect' and isactive='Y'";
        System.out.println("sql para buscar tipo de factura:"+docTypeSQl+"--"+clientId);	
        	
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);
        
        return docTypeId;
    }
    
    private int getInvoiceStandarTax(int clientId,Trx trx)
    {
        String docTypeSQl = "SELECT c_tax_id FROM C_TAX "
            + " WHERE ad_client_id=? and rate=0 and isactive = 'Y' and isdefault ='Y'";
        
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);
        
        return docTypeId;
    }
    
    
    
}
