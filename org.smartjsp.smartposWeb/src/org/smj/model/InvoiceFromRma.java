package org.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MRMA;
import org.compiere.model.MRMALine;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueries;
import com.smj.util.SMJConfig;

/**
 * Create an Invoice from a RMA
 * @author pedrorozo
 *
 */


public class InvoiceFromRma {
	protected CLogger log = CLogger.getCLogger(super.getClass());
	private static Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private static Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());

    public void generateInvoice(Trx trx, HashMap <String,String> to, int inOutId)
    {
        MInvoice invoice = createInvoice(trx,to,inOutId);
        createInvoiceLines(inOutId, invoice, to, trx);
        
        if (!invoice.save())
        {
            throw new IllegalStateException("Could not update invoice");
        }
        invoice.completeIt();
        invoice.setDocStatus(DocAction.STATUS_Completed);
        invoice.setDocAction(DocAction.ACTION_Close);
        invoice.save();
        log.log(Level.INFO,"***************** Crea factura desde RMA ******************** ");
    }
    
    public MInvoice generateInvoiceFromRMA(int M_RMA_ID, Trx trx)
    {
        MRMA rma = new MRMA(Env.getCtx(), M_RMA_ID, trx.getTrxName());
        
        MInvoice invoice = createInvoice(rma,trx);
        MInvoiceLine invoiceLines[] = createInvoiceLines(rma, invoice, trx);
        
        if (!invoice.save())
        {
            throw new IllegalStateException("Could not update invoice");
        }
        invoice.completeIt();
        invoice.setDocStatus(DocAction.STATUS_Completed);
        invoice.setDocAction(DocAction.ACTION_Complete);
        invoice.save();
        log.log(Level.INFO,"***************** Crea factura desde RMA ******************** ");
        return invoice;
    }
    
    private MInvoice createInvoice(MRMA rma,Trx trx)
    {
        int docTypeId = getInvoiceDocTypeId(rma.get_ID(),trx);
            
        MInvoice invoice = new MInvoice(Env.getCtx(), 0, trx.getTrxName());
        invoice.setRMA(rma);
        //invoice.setC_PaymentTerm_ID(Integer.parseInt(Msg.getMsg(Env.getCtx(), "PaymentTerm").trim()));
        invoice.setC_PaymentTerm_ID(SMJConfig.getIntValue("C_PaymentTerm_ID", 0, lorgId));
        invoice.setC_DocTypeTarget_ID(docTypeId);
        if (!invoice.save())
        {
            throw new IllegalStateException("Could not create invoice");
        }
        return invoice;
    }
    
    private MInvoiceLine[] createInvoiceLines(MRMA rma, MInvoice invoice,Trx trx)
    {
        ArrayList<MInvoiceLine> invLineList = new ArrayList<MInvoiceLine>();
        MRMALine rmaLines[] = rma.getLines(true);
        for (MRMALine rmaLine : rmaLines)
        {
            if (rmaLine.getM_InOutLine_ID() == 0)
            {
                throw new IllegalStateException("No customer return line - RMA = " 
                        + rma.getDocumentNo() + ", Line = " + rmaLine.getLine());
            }
            
            MInvoiceLine invLine = new MInvoiceLine(invoice);

            invLine.setRMALine(rmaLine);
            
            if(rmaLine.getDescription().toUpperCase().indexOf("PROPI") >= 0){
            	// Integer charge = Integer.parseInt(Msg.getMsg(Env.getCtx(), "c_charge_id").trim());
            	Integer charge = SMJConfig.getIntValue("C_ChargeTips_ID", 0, lorgId);
            	invLine.setC_Charge_ID(charge);
            }
            
            if (!invLine.save())
            {
                throw new IllegalStateException("Could not create invoice line");
            }
            
            invLineList.add(invLine);
        }
        MInvoiceLine invLines[] = new MInvoiceLine[invLineList.size()];
        invLineList.toArray(invLines);
        return invLines;
    }
    
 
    private int getInvoiceDocTypeId(int M_RMA_ID,Trx trx)
    {
        String docTypeSQl = "SELECT dt.C_DocTypeInvoice_ID FROM C_DocType dt "
            + "INNER JOIN M_RMA rma ON dt.C_DocType_ID=rma.C_DocType_ID "
            + "WHERE rma.M_RMA_ID=?";
        
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, M_RMA_ID);
        
        return docTypeId;
    }
    
    private int getInvoiceDocTypeIdFromClient(int clientId,Trx trx)
    {
        String docTypeSQl = "SELECT dt.c_doctype_id FROM C_DocType dt "
           // + "WHERE dt.ad_client_id=? and name = 'Customer Return Material' and isactive='Y'";
             + "WHERE dt.ad_client_id=? and name = 'AP CreditMemo' and isactive='Y'";
        System.out.println("sql para buscar tipo de factura:"+docTypeSQl+"--"+clientId);	
        	
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);
        
        return docTypeId;
    }
    
    private int getInvoiceStandarTax(int clientId,Trx trx)
    {
        String docTypeSQl = "SELECT c_tax_id FROM C_TAX "
            + " WHERE ad_client_id=? and rate=0 and isactive = 'Y' and isdefault ='Y'";
        
        int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);
        
        return docTypeId;
    }
    
    
    
    private MInvoice createInvoice(Trx trx, HashMap <String,String> to, int inOutId)
    {
    	MInOut pInOut = new MInOut(Env.getCtx(), inOutId, trx.getTrxName() );
    	
    	MInvoice data = new MInvoice(Env.getCtx(), 0, null);
    	data.setClientOrg(pInOut .getAD_Client_ID(), pInOut .getAD_Org_ID());
    	data.setAD_Org_ID(Integer.parseInt(to.get("ad_org_id").trim()));
		data.setIsActive(true);
		data.setDateInvoiced(new Timestamp((new Date().getTime())));
		data.setDateAcct(new Timestamp((new Date().getTime())));
		data.setC_BPartner_ID(pInOut.getC_BPartner_ID());
		data.setC_BPartner_Location_ID(pInOut.getC_BPartner_Location_ID());
		data.setC_Currency_ID(DataQueries.getCurrencyDefault(lclientId) );
		data.setM_PriceList_ID(Integer.parseInt(to.get("PriceListID")));
		data.setSalesRep_ID(Integer.parseInt(to.get("ad_user_id")));
		data.setDescription(to.get("affects-stock-description"));
		data.setPOReference(to.get("poReference"));
		data.setProcessed(true);
		data.setPaymentRule("S");
		data.setIsSOTrx(false);
		int docType = getInvoiceDocTypeIdFromClient(Integer.parseInt(to.get("ad_client_id").trim()), trx);
		data.setC_DocType_ID(docType);
		data.setC_DocTypeTarget_ID(docType);

		Boolean pok = data.save();
		if (!pok){
			throw new IllegalStateException("Could not Create Invoice Encounter: "+data.toString());
		}
		return data;
    }

    private void createInvoiceLines(int inOutId, MInvoice invoice,HashMap <String,String> to,Trx trx)
    {

    	MInOut pInOut = new MInOut(Env.getCtx(), inOutId, trx.getTrxName() );
    	MInOutLine pInOutLines[] = pInOut.getLines();
    	BigDecimal total = new BigDecimal(0); 
    	for (int i = 0; i < pInOutLines.length;i++)
		{
    		String productId = "0";
			if (!to.get("productId").equalsIgnoreCase("null"))
			{
				productId = to.get("productId");
			}
			
			if (Integer.parseInt(productId) == pInOutLines[i].getM_Product_ID() )  // only processs the return product id	
			{
				MInvoiceLine line = new MInvoiceLine(Env.getCtx(), 0, null);
				line.setAD_Org_ID(Integer.parseInt(to.get("ad_org_id").trim()));
				line.set_ValueOfColumn("ad_client_id",pInOutLines[i].getAD_Client_ID());	
				line.setIsActive(true);
				line.setC_Invoice_ID(invoice.getC_Invoice_ID());
				line.setQtyEntered((new BigDecimal(to.get("Qty"))));
				line.setM_Product_ID(Integer.parseInt(productId));
				line.setPrice(pInOutLines[i].getC_OrderLine().getPriceEntered());
				line.setPriceActual(pInOutLines[i].getC_OrderLine().getPriceEntered());
				line.setPriceList(new BigDecimal(0));
				line.setC_Tax_ID(pInOutLines[i].getC_OrderLine().getC_Tax_ID());
				line.setC_UOM_ID(pInOutLines[i].getC_OrderLine().getC_UOM_ID());
				line.setQtyInvoiced(new BigDecimal(to.get("Qty")));
				BigDecimal totalLine = line.getQtyEntered().multiply(pInOutLines[i].getC_OrderLine().getPriceEntered());
				line.setLineNetAmt(totalLine);
				total = total.add(totalLine);
				Boolean lok = line.save();
				
				if (!lok){
					throw new IllegalStateException("Could not Create Invoice Encounter: "+line.toString());
				}
			}
		}
    	
    	invoice.setTotalLines(total);
    }
    

	    
}
