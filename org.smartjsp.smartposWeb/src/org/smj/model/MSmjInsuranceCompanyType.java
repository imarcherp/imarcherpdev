package org.smj.model;

import java.util.Properties;

public class MSmjInsuranceCompanyType extends X_smj_insuranceCompanyType {

	public MSmjInsuranceCompanyType(Properties ctx,
			int smj_insuranceCompanyType_ID, String trxName) {
		super(ctx, smj_insuranceCompanyType_ID, trxName);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8564945728017096844L;

}
