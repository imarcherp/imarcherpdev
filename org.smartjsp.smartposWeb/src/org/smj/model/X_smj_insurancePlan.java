/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for smj_insurancePlan
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_insurancePlan extends PO implements I_smj_insurancePlan, I_Persistent 
{

	
	/**
	 *
	 */
	private static final long serialVersionUID = 20121013L;

    /** Standard Constructor */
    public X_smj_insurancePlan (Properties ctx, int smj_insurancePlan_ID, String trxName)
    {
      super (ctx, smj_insurancePlan_ID, trxName);
      /** if (smj_insurancePlan_ID == 0)
        {
			setC_BPartner_ID (0);
			setName (null);
			setsmj_insurancePlan_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_insurancePlan (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_insurancePlan[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Co-Pay Percentage.
		@param copay_percentage Co-Pay Percentage	  */
	public void setcopay_percentage (BigDecimal copay_percentage)
	{
		set_Value (COLUMNNAME_copay_percentage, copay_percentage);
	}

	/** Get Co-Pay Percentage.
		@return Co-Pay Percentage	  */
	public BigDecimal getcopay_percentage () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_copay_percentage);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Co-Pay Value.
		@param copay_value Co-Pay Value	  */
	public void setcopay_value (int copay_value)
	{
		set_Value (COLUMNNAME_copay_value, Integer.valueOf(copay_value));
	}

	/** Get Co-Pay Value.
		@return Co-Pay Value	  */
	public int getcopay_value () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_copay_value);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Insurance Plan .
		@param smj_insurancePlan_ID Insurance Plan 	  */
	public void setsmj_insurancePlan_ID (int smj_insurancePlan_ID)
	{
		if (smj_insurancePlan_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_insurancePlan_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_insurancePlan_ID, Integer.valueOf(smj_insurancePlan_ID));
	}

	/** Get Insurance Plan .
		@return Insurance Plan 	  */
	public int getsmj_insurancePlan_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_insurancePlan_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}