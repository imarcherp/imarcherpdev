/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.compiere.model.MTable;
import org.compiere.util.KeyNamePair;

/** Generated Interface for I_SMJ_Medicine
 *  @author iDempiere (generated) 
 *  @version Release 2.0
 */
@SuppressWarnings("all")
public interface I_I_SMJ_Medicine 
{

    /** TableName=I_SMJ_Medicine */
    public static final String Table_Name = "I_SMJ_Medicine";

    /** AD_Table_ID=1000003 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Category */
    public static final String COLUMNNAME_Category = "Category";

	/** Set Category	  */
	public void setCategory (String Category);

	/** Get Category	  */
	public String getCategory();

    /** Column name code_cip */
    public static final String COLUMNNAME_code_cip = "code_cip";

	/** Set Code C.I.P	  */
	public void setcode_cip (String code_cip);

	/** Get Code C.I.P	  */
	public String getcode_cip();

    /** Column name code_pharma_ml */
    public static final String COLUMNNAME_code_pharma_ml = "code_pharma_ml";

	/** Set Code Pharma ML	  */
	public void setcode_pharma_ml (String code_pharma_ml);

	/** Get Code Pharma ML	  */
	public String getcode_pharma_ml();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name I_ErrorMsg */
    public static final String COLUMNNAME_I_ErrorMsg = "I_ErrorMsg";

	/** Set Import Error Message.
	  * Messages generated from import process
	  */
	public void setI_ErrorMsg (String I_ErrorMsg);

	/** Get Import Error Message.
	  * Messages generated from import process
	  */
	public String getI_ErrorMsg();

    /** Column name I_IsImported */
    public static final String COLUMNNAME_I_IsImported = "I_IsImported";

	/** Set Imported.
	  * Has this import been processed
	  */
	public void setI_IsImported (boolean I_IsImported);

	/** Get Imported.
	  * Has this import been processed
	  */
	public boolean isI_IsImported();

    /** Column name I_smj_medicine_ID */
    public static final String COLUMNNAME_I_smj_medicine_ID = "I_smj_medicine_ID";

	/** Set Medicine	  */
	public void setI_smj_medicine_ID (int I_smj_medicine_ID);

	/** Get Medicine	  */
	public int getI_smj_medicine_ID();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Level_Min */
    public static final String COLUMNNAME_Level_Min = "Level_Min";

	/** Set Minimum Level.
	  * Minimum Inventory level for this product
	  */
	public void setLevel_Min (BigDecimal Level_Min);

	/** Get Minimum Level.
	  * Minimum Inventory level for this product
	  */
	public BigDecimal getLevel_Min();

    /** Column name m_locator_value */
    public static final String COLUMNNAME_m_locator_value = "m_locator_value";

	/** Set m_locator_value	  */
	public void setm_locator_value (String m_locator_value);

	/** Get m_locator_value	  */
	public String getm_locator_value();

    /** Column name Manufacturer */
    public static final String COLUMNNAME_Manufacturer = "Manufacturer";

	/** Set Manufacturer.
	  * Manufacturer of the Product
	  */
	public void setManufacturer (String Manufacturer);

	/** Get Manufacturer.
	  * Manufacturer of the Product
	  */
	public String getManufacturer();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Points */
    public static final String COLUMNNAME_Points = "Points";

	/** Set Points	  */
	public void setPoints (int Points);

	/** Get Points	  */
	public int getPoints();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name purchaseprice */
    public static final String COLUMNNAME_purchaseprice = "purchaseprice";

	/** Set Purchase Price	  */
	public void setpurchaseprice (int purchaseprice);

	/** Get Purchase Price	  */
	public int getpurchaseprice();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (int Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public int getQty();

    /** Column name salestax */
    public static final String COLUMNNAME_salestax = "salestax";

	/** Set Tax	  */
	public void setsalestax (String salestax);

	/** Get Tax	  */
	public String getsalestax();

    /** Column name schedule */
    public static final String COLUMNNAME_schedule = "schedule";

	/** Set Schedule	  */
	public void setschedule (String schedule);

	/** Get Schedule	  */
	public String getschedule();

    /** Column name sellingprice */
    public static final String COLUMNNAME_sellingprice = "sellingprice";

	/** Set Selling Price	  */
	public void setsellingprice (int sellingprice);

	/** Get Selling Price	  */
	public int getsellingprice();

    /** Column name smj_active */
    public static final String COLUMNNAME_smj_active = "smj_active";

	/** Set Active	  */
	public void setsmj_active (boolean smj_active);

	/** Get Active	  */
	public boolean issmj_active();

    /** Column name uom_name */
    public static final String COLUMNNAME_uom_name = "uom_name";

	/** Set uom_name	  */
	public void setuom_name (String uom_name);

	/** Get uom_name	  */
	public String getuom_name();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
