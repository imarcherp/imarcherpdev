/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.I_Persistent;
import org.compiere.model.PO;
import org.compiere.model.POInfo;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for I_SMJ_Medicine
 *  @author iDempiere (generated) 
 *  @version Release 2.0 - $Id$ */
public class X_I_SMJ_Medicine extends PO implements I_I_SMJ_Medicine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150325L;

    /** Standard Constructor */
    public X_I_SMJ_Medicine (Properties ctx, int I_SMJ_Medicine_ID, String trxName)
    {
      super (ctx, I_SMJ_Medicine_ID, trxName);
      /** if (I_SMJ_Medicine_ID == 0)
        {
			setI_smj_medicine_ID (0);
			setLevel_Min (Env.ZERO);
			setsmj_active (false);
        } */
    }

    /** Load Constructor */
    public X_I_SMJ_Medicine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_I_SMJ_Medicine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Category.
		@param Category Category	  */
	public void setCategory (String Category)
	{
		set_Value (COLUMNNAME_Category, Category);
	}

	/** Get Category.
		@return Category	  */
	public String getCategory () 
	{
		return (String)get_Value(COLUMNNAME_Category);
	}

	/** Set Code C.I.P.
		@param code_cip Code C.I.P	  */
	public void setcode_cip (String code_cip)
	{
		set_Value (COLUMNNAME_code_cip, code_cip);
	}

	/** Get Code C.I.P.
		@return Code C.I.P	  */
	public String getcode_cip () 
	{
		return (String)get_Value(COLUMNNAME_code_cip);
	}

	/** Set Code Pharma ML.
		@param code_pharma_ml Code Pharma ML	  */
	public void setcode_pharma_ml (String code_pharma_ml)
	{
		set_Value (COLUMNNAME_code_pharma_ml, code_pharma_ml);
	}

	/** Get Code Pharma ML.
		@return Code Pharma ML	  */
	public String getcode_pharma_ml () 
	{
		return (String)get_Value(COLUMNNAME_code_pharma_ml);
	}

	/** Set Import Error Message.
		@param I_ErrorMsg 
		Messages generated from import process
	  */
	public void setI_ErrorMsg (String I_ErrorMsg)
	{
		set_Value (COLUMNNAME_I_ErrorMsg, I_ErrorMsg);
	}

	/** Get Import Error Message.
		@return Messages generated from import process
	  */
	public String getI_ErrorMsg () 
	{
		return (String)get_Value(COLUMNNAME_I_ErrorMsg);
	}

	/** Set Imported.
		@param I_IsImported 
		Has this import been processed
	  */
	public void setI_IsImported (boolean I_IsImported)
	{
		set_Value (COLUMNNAME_I_IsImported, Boolean.valueOf(I_IsImported));
	}

	/** Get Imported.
		@return Has this import been processed
	  */
	public boolean isI_IsImported () 
	{
		Object oo = get_Value(COLUMNNAME_I_IsImported);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Medicine.
		@param I_smj_medicine_ID Medicine	  */
	public void setI_smj_medicine_ID (int I_smj_medicine_ID)
	{
		if (I_smj_medicine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_I_smj_medicine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_I_smj_medicine_ID, Integer.valueOf(I_smj_medicine_ID));
	}

	/** Get Medicine.
		@return Medicine	  */
	public int getI_smj_medicine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_I_smj_medicine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Minimum Level.
		@param Level_Min 
		Minimum Inventory level for this product
	  */
	public void setLevel_Min (BigDecimal Level_Min)
	{
		set_Value (COLUMNNAME_Level_Min, Level_Min);
	}

	/** Get Minimum Level.
		@return Minimum Inventory level for this product
	  */
	public BigDecimal getLevel_Min () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Level_Min);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set m_locator_value.
		@param m_locator_value m_locator_value	  */
	public void setm_locator_value (String m_locator_value)
	{
		set_Value (COLUMNNAME_m_locator_value, m_locator_value);
	}

	/** Get m_locator_value.
		@return m_locator_value	  */
	public String getm_locator_value () 
	{
		return (String)get_Value(COLUMNNAME_m_locator_value);
	}

	/** Set Manufacturer.
		@param Manufacturer 
		Manufacturer of the Product
	  */
	public void setManufacturer (String Manufacturer)
	{
		set_Value (COLUMNNAME_Manufacturer, Manufacturer);
	}

	/** Get Manufacturer.
		@return Manufacturer of the Product
	  */
	public String getManufacturer () 
	{
		return (String)get_Value(COLUMNNAME_Manufacturer);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Points.
		@param Points Points	  */
	public void setPoints (int Points)
	{
		set_Value (COLUMNNAME_Points, Integer.valueOf(Points));
	}

	/** Get Points.
		@return Points	  */
	public int getPoints () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Points);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Purchase Price.
		@param purchaseprice Purchase Price	  */
	public void setpurchaseprice (int purchaseprice)
	{
		set_Value (COLUMNNAME_purchaseprice, Integer.valueOf(purchaseprice));
	}

	/** Get Purchase Price.
		@return Purchase Price	  */
	public int getpurchaseprice () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_purchaseprice);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (int Qty)
	{
		set_Value (COLUMNNAME_Qty, Integer.valueOf(Qty));
	}

	/** Get Quantity.
		@return Quantity
	  */
	public int getQty () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Qty);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tax.
		@param salestax Tax	  */
	public void setsalestax (String salestax)
	{
		set_Value (COLUMNNAME_salestax, salestax);
	}

	/** Get Tax.
		@return Tax	  */
	public String getsalestax () 
	{
		return (String)get_Value(COLUMNNAME_salestax);
	}

	/** Set Schedule.
		@param schedule Schedule	  */
	public void setschedule (String schedule)
	{
		set_Value (COLUMNNAME_schedule, schedule);
	}

	/** Get Schedule.
		@return Schedule	  */
	public String getschedule () 
	{
		return (String)get_Value(COLUMNNAME_schedule);
	}

	/** Set Selling Price.
		@param sellingprice Selling Price	  */
	public void setsellingprice (int sellingprice)
	{
		set_Value (COLUMNNAME_sellingprice, Integer.valueOf(sellingprice));
	}

	/** Get Selling Price.
		@return Selling Price	  */
	public int getsellingprice () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_sellingprice);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Active.
		@param smj_active Active	  */
	public void setsmj_active (boolean smj_active)
	{
		set_Value (COLUMNNAME_smj_active, Boolean.valueOf(smj_active));
	}

	/** Get Active.
		@return Active	  */
	public boolean issmj_active () 
	{
		Object oo = get_Value(COLUMNNAME_smj_active);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set uom_name.
		@param uom_name uom_name	  */
	public void setuom_name (String uom_name)
	{
		set_Value (COLUMNNAME_uom_name, uom_name);
	}

	/** Get uom_name.
		@return uom_name	  */
	public String getuom_name () 
	{
		return (String)get_Value(COLUMNNAME_uom_name);
	}
}