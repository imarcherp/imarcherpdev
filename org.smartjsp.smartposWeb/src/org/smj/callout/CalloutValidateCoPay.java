package org.smj.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * Callout que valida solo se ingrese un copago  -
 * Callout validate Co pay value o percentage
 * @author Freddy Rodriguez
 *
 */
public class CalloutValidateCoPay extends CalloutEngine{

	public String validateCoPay (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		String name = mField.getColumnName();
		Integer copay_value = (Integer)mTab.getValue("copay_value");
		BigDecimal copay_percentage = (BigDecimal)mTab.getValue("copay_percentage");
		 if(copay_value > 0 && copay_percentage.compareTo(Env.ZERO)>0){
			 if(name.equals("copay_value")){
				 mTab.setValue(name, 0);
			 }else
			 {
				 mTab.setValue(name, Env.ZERO);
			 }
			 return Msg.translate(Env.getCtx(), "SMJ-MSGCOPAYNOVALID");
		 }
		return "";
	}//ValidateCoPay
	
}//CalloutValidateCoPay
