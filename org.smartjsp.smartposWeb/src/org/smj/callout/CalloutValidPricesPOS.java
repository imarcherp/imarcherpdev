package org.smj.callout;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
/**
 * Callout - validate POS prices
 * @author Freddy Rodriguez
 *
 */
public class CalloutValidPricesPOS extends CalloutEngine{

	public String checkUnitConversionPOS (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		String retorno = "";
		Integer productId = (Integer)mTab.getValue("m_product_id");
		Integer priceListId = (Integer)mTab.getValue("m_pricelist_version_id");
		
		 String sql = "SELECT c_uom_conversion_id  FROM c_uom_conversion " +
		 		" WHERE m_product_id = "+productId+
		 		" AND  c_uom_to_id = (SELECT c_uom_id FROM c_uom WHERE name = "+
		 		 "( SELECT name FROM m_pricelist_version WHERE m_pricelist_version_id = "+ priceListId +"))";
		 PreparedStatement pstmt = null;   
		 ResultSet rs = null;
		
		 try{
			 pstmt = DB.prepareStatement(sql, null); 
			 rs = pstmt.executeQuery();		
			 if (!rs.next()){          //no hay factor de conversion para la unidad usada en esta lista de precios
			    retorno = Msg.translate(Env.getCtx(), "SMJ-MSGUOMCONVERSIONERROR");
			 }
			 
		 } catch (SQLException e) {
			    log.log(Level.WARNING, "CalloutUnitConversionPOS ", e); 
				  e.getStackTrace();
		 }//try/catch
		 finally{
				  DB.close(rs, pstmt);
				  pstmt = null;
				  rs = null;
		 }//finally
			 return  retorno;   
		 
		 
	 }// 
	
}//CalloutValidProcesPOS
