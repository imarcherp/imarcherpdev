package org.smj.callout;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

public class CalloutGenerateCode extends CalloutEngine {
	
	public String generateCode(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value) {
		String nationalId = mTab.get_ValueAsString("smj_nationalidnumber");
		Boolean isCustomer =  mField.getValue().toString().equalsIgnoreCase("true");

		if ((nationalId == null || nationalId.isEmpty() || nationalId.equals("0")) && isCustomer) {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT MAX((CASE WHEN smj_nationalidnumber IS NULL OR smj_nationalidnumber = 'null' ");
			sql.append("THEN '0' ELSE smj_nationalidnumber END)::numeric) as smj_max FROM adempiere.C_BPartner");

			PreparedStatement pstmt = null;   
			ResultSet rs = null;
			BigDecimal max = new BigDecimal(1);

			try{
				pstmt = DB.prepareStatement(sql.toString(), null); 
				rs = pstmt.executeQuery();		
				if (rs.next()){
					max = rs.getBigDecimal("smj_max");
					max = max.add(new BigDecimal(1));
				}				 
			} catch (SQLException e) {
				log.log(Level.WARNING, "CalloutGenerateUniqueID", e); 
				e.getStackTrace();
			} finally{
				DB.close(rs, pstmt);
				pstmt = null;
				rs = null;
			}//finally
			mTab.setValue("smj_nationalidnumber", max.toString());			
		}
		
		return null;
	}
}
