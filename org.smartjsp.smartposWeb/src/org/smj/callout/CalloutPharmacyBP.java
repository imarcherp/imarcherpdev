package org.smj.callout;

import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.X_C_BPartner;

public class CalloutPharmacyBP extends CalloutEngine {
	
	public String validateCheckbox(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value) {
		Boolean isCustomer = mTab.getValueAsBoolean(X_C_BPartner.COLUMNNAME_IsCustomer);
		Boolean isInsuranceCompany = mTab.getValueAsBoolean("smj_isinsurancecompany");
		
		if (mField.getColumnName().equals(X_C_BPartner.COLUMNNAME_IsCustomer)) {
			if (isCustomer)
				mTab.setValue("smj_isinsurancecompany", false);
		} else {
			if (isInsuranceCompany)
				mTab.setValue(X_C_BPartner.COLUMNNAME_IsCustomer, false);
		}	

		return null;
	}
}
