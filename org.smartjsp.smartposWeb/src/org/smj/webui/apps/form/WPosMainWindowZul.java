package org.smj.webui.apps.form;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.theme.ThemeManager;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.PageDefinition;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Vbox;


public class WPosMainWindowZul extends PosMainWindow implements IFormController, 
          org.zkoss.zk.ui.event.EventListener<Event>, ValueChangeListener {

	private CustomForm m_form = new CustomForm();
	private Textbox txtURL = new Textbox("http://www.smartjsp.com");
	private Vbox mainLayout = new Vbox();
	private Hbox custLayout = new Hbox();
	private Hbox prodLayout = new Hbox();
	
	
	private Tabbox gLayout = new Tabbox();
	
	private Hbox barLayout = new Hbox();
	private Hbox totalLayout = new Hbox();
	
	private Iframe iframe = new Iframe();
	private Integer bpartnerId = 0;
	private Integer productId = 0;
	
	private Label bPartnerLabel = new Label("Tercero:");
	private WSearchEditor bpartnerSearch = null;
	private Label productLabel = new Label("Producto:");
	private WSearchEditor productSearch = null;
	
	
	
	public WPosMainWindowZul() {
		dynInit();
		init();
	}
	 
	private void dynInit() {
	try {
		// 
		// int AD_Column_ID = 2762;        //  C_Order.C_BPartner_ID
		int AD_Column_ID_BPartner = 2893;        //  C_BPartner.C_BPartner_ID
		int AD_Column_ID_Product  = 1402;        //  M_Product.MProduct_ID
		
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), m_form.getWindowNo(), 0, AD_Column_ID_BPartner, DisplayType.Search);
		bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);		
		bpartnerSearch.addValueChangeListener(this);
		
		MLookup lookupProduct = MLookupFactory.get (Env.getCtx(), m_form.getWindowNo(), 0, AD_Column_ID_Product, DisplayType.Search);
		productSearch = new WSearchEditor("M_Product_ID", true, false, true, lookupProduct);
		productSearch.addValueChangeListener(this);
		
		}
	 catch (Exception e) {
		log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".dynInit - ERROR: " + e.getMessage(), e);
	}
	}
	
	private void init() {
		custLayout.setStyle("background:#008db7;");
		prodLayout.setStyle("background:#0000b7;");
		gLayout.setStyle("background:#FF8db7;");
		barLayout.setStyle("background:#008db7;");
		totalLayout.setStyle("background:#00FFb7;");
		
		mainLayout.appendChild(custLayout);
		mainLayout.appendChild(prodLayout);
		mainLayout.appendChild(gLayout);
		mainLayout.appendChild(barLayout);
		mainLayout.appendChild(totalLayout);
		
		//Borderlayout mainLayout = new Borderlayout();
		//North north = new North();
		//Center center = new Center();
		
		// estilos y tama�os  de componentes
		m_form.setStyle("width:100%;height:100%;position:absolute");
		//mainLayout.setStyle("width:100%;height:100%;position:absolute");
		//fPanel.setStyle("width:200px;height:16px;position:absolute");
		
//		iframe.setVflex("true");
//		iframe.setHflex("true");
//		iframe.setStyle("width:100%;height:100%;");
//		center.appendChild(iframe);
		//txtURL.setStyle("width:99%;height:16px;");
		//bpartnerSearch.getComponent().setStyle("width:100px;height:16px;");
		// eventos y relaciones jerarquicas
		//north.appendChild(txtURL);
		//mainLayout.appendChild(north);
		//center.appendChild(fPanel);

		custLayout.appendChild(bPartnerLabel);   //customer
		custLayout.appendChild(bpartnerSearch.getComponent());
		
		prodLayout.appendChild(productLabel);   // product
		prodLayout.appendChild(productSearch.getComponent());
		
		
		//agregarTab("Ventana 1", "v1", ThemeManager.getThemeResource("zul/desktop/test1.zul"));
		//agregarTab("Ventana 2", "v2", ThemeManager.getThemeResource("zul/desktop/test1.zul"));
		//agregarTab("Ventana 3", "v3", ThemeManager.getThemeResource("zul/desktop/test1.zul"));
		
		//m_form.appendChild(mainLayout);
		
		PageDefinition pagedef = Executions.getCurrent().getPageDefinition(ThemeManager.getThemeResource("zul/desktop/smartpos.zul"));
    	Component page = Executions.createComponents(pagedef, m_form, null);
		
		//iframe.setSrc(txtURL.getText());
		//txtURL.addEventListener(Events.ON_OK, this);
		
	}
	
	@AfterCompose
	 public void afterCompose(@ContextParam(ContextType.VIEW) Component view){
	  Selectors.wireComponents(view, this, false);
	 }
	 
	 //Componentes UI
	 @Wire 
	 Tabbox tb_tabbox;

	 @Command
	 public void window1(){
	  agregarTab("Ventana 1", "v1", "v1.zul");
	 }
	 
	 @Command
	 public void window2(){
	  agregarTab("Ventana 2", "v2", "v2.zul");
	 }
	 
	 @Command
	 public void window3(){
	  agregarTab("Ventana 3", "v3", "v3.zul");
	 }
	
	
	 private void agregarTab(String titulo, String id, String zul) {
	  Map<String, Object> arguments = new HashMap<String, Object>();
	  Tabpanel tabpanel = new Tabpanel();
	  Tab tab = new Tab(titulo);
	  tab.setId(id);
	  if(gLayout.hasFellow(id)){
		Messagebox m1 = new Messagebox();  
	    m1.show("Ya esta abierta la ventana!","Titulo",Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
	   return;
	  }
	  
	  tab.setClosable(true);
	  tab.setSelected(true);
	  
	  if (gLayout.getTabs() == null){
		  gLayout.appendChild(new Tabs());   
		  gLayout.appendChild(new Tabpanels());   
	  }

	  gLayout.getTabs().appendChild(tab);
	  arguments.put("tabularIndex", tab.getIndex());

	  gLayout.getTabpanels().appendChild(tabpanel);
	  gLayout.invalidate();
	  Executions.createComponents(zul, tabpanel, arguments);
	 }
	
	
	@Override
	public ADForm getForm() {
		return m_form;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().equals(txtURL)  && txtURL.getValue().trim().length() > 0) {
			iframe.setSrc(txtURL.getValue());
		}
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		try {
			String name = evt.getPropertyName();
			Object value = evt.getNewValue();
			if (value==null){
				if(bpartnerId > 0){
					bpartnerSearch.setValue(bpartnerId);
				}else{
					try {
						Messagebox m1 = new Messagebox();
						int response = m1.show("Crear tercero ", "Info label", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
						
						if (response == Messagebox.OK){
							// ShowWindow.openPartnerWebPOS(0);
							
							System.out.println("abir ventana de creacion de tercero");
						}//response Ok
						System.out.println("Creacion de tercero");
						WSearchEditor.createBPartner(m_form.getWindowNo());  // open bpartner window
						WSearchEditor.createProduct(m_form.getWindowNo());  // open bpartner window
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()+".valueChange ERROR: ", e);
					}
				}
				return;
			}//if Value
			//  BPartner
			if (name.equals("C_BPartner_ID")){
				bpartnerSearch.setValue(value);
				bpartnerId = ((Integer)value).intValue();
				//setPartner();
			}//C_BPartner_ID
			else if (name.equals("M_Product_ID")) {
				productSearch.setValue(value);
				productId = ((Integer)value).intValue();
			}
			//productTextBox.setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".valueChange - ERROR: " + e.getMessage(), e);
		}
		
	}

}
