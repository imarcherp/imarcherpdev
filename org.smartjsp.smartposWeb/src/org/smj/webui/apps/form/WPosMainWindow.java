package org.smj.webui.apps.form;

import java.util.logging.Level;

import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.theme.ThemeManager;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;

public class WPosMainWindow extends PosMainWindow implements IFormController, 
          org.zkoss.zk.ui.event.EventListener<Event>, ValueChangeListener {

	private CustomForm m_form = new CustomForm();
	private Textbox txtURL = new Textbox("http://www.smartjsp.com");
	private Window mainLayout = new Window();
	private Vbox vmainLayout = new Vbox();
	private Hbox custLayout = new Hbox();
	private Window pcustLayout = new Window();
	private Hbox prodLayout = new Hbox();
	private Window pprodLayout = new Window();
	private Vbox gLayout = new Vbox();
	private Window pgLayout = new Window();
	private Hbox barLayout = new Hbox();
	private Window pbarLayout = new Window();
	private Hbox totalLayout = new Hbox();
	private Window ptotalLayout = new Window();
	
	//private Tabbox gLayout = new Tabbox();
	
	private Integer bpartnerId = 0;
	private Integer productId = 0;
	
	private Label bPartnerLabel = new Label("Tercero:");
	private WSearchEditor bpartnerSearch = null;
	private Label productLabel = new Label("Producto:");
	private WSearchEditor productSearch = null;
	private Button sendButton = new Button("Send"); 
	private Button preticketButton = new Button("PreTicket");
	private Button payButton = new Button("Pay");
	
	private Button delete1Button = new Button("Delete");
	private Button exit1Button = new Button("Exit");
	
	private Button plusButton = new Button();
	private Button minusButton = new Button();
	private Button delete2Button = new Button("Delete");
	private Button editButton = new Button("Edit");
	
	public WPosMainWindow() {
		dynInit();
		init();
	}
	 
	private void dynInit() {
	try {
		// 
		// int AD_Column_ID = 2762;        //  C_Order.C_BPartner_ID
		int AD_Column_ID_BPartner = 2893;        //  C_BPartner.C_BPartner_ID
		int AD_Column_ID_Product  = 1402;        //  M_Product.MProduct_ID
		
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), m_form.getWindowNo(), 0, AD_Column_ID_BPartner, DisplayType.Search);
		bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);		
		bpartnerSearch.addValueChangeListener(this);
		
		MLookup lookupProduct = MLookupFactory.get (Env.getCtx(), m_form.getWindowNo(), 0, AD_Column_ID_Product, DisplayType.Search);
		productSearch = new WSearchEditor("M_Product_ID", true, false, true, lookupProduct);
		productSearch.addValueChangeListener(this);
		
		}
	 catch (Exception e) {
		log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".dynInit - ERROR: " + e.getMessage(), e);
	}
	}
	
	private void init() {
		String borderStyle = "border: 1px solid #C0C0C0; border-radius:5px;";
		String borderStyle2 = "border: 1px solid #C0C0C0; border-radius:5px;float: right";
		
		String buttonImg = "background:white;Height:35px;width:30px;border: 0px solid #610B0B;border-radius:10px;padding: 1px 1px;";
		String buttonRed = "background:red;Height:35px;Width:80px;border: 3px solid #610B0B;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 14px;font-weight: bolder;color: white";
		String buttonGreen = "background:#088A08;Height:35px;Width:80px;border: 3px solid #2E2E2E;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 14px;font-weight: bolder;color: white";
		String buttonYellow = "background:yellow;Height:35px;Width:80px;border: 3px solid #2E2E2E;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 14px;font-weight: bolder;color: black";
		String buttonOrange = "background:#ffc477;Height:35px;Width:80px;border: 3px solid #eeb44f;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 14px;font-weight: bolder;color: black;";  // text-shadow:2px 3px 0px #cc9f52;
		String buttonGray = "background:#BDBDBD;Height:35px;Width:80px;border: 3px solid #2E2E2E;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 14px;font-weight: bolder;color: black;";  // text-shadow:2px 3px 0px #cc9f52;
		
		mainLayout.setWidth("97%");        // main layout sizes
		mainLayout.setHeight("97%");
		//mainLayout.setTitle("SmartPOS");
		vmainLayout.setWidth("99%");
		vmainLayout.setHeight("100%");
		vmainLayout.setHeights("10%,10%,60%,10%,10%");
		vmainLayout.setStyle("margin-left: 5px;margin-top: 5px;");
		mainLayout.appendChild(vmainLayout);
		//mainLayout.setBorder("normal");
		//mainLayout.setStyle(borderStyle);
		pcustLayout.setBorder("normal");    // panel borders
		pcustLayout.setStyle(borderStyle);
		pprodLayout.setBorder("normal");
		pprodLayout.setStyle(borderStyle);
		pgLayout.setBorder("normal");
		pgLayout.setStyle(borderStyle);
		pbarLayout.setBorder("normal");
		pbarLayout.setStyle(borderStyle2);
		ptotalLayout.setBorder("normal");
		ptotalLayout.setStyle(borderStyle2);
		//custLayout.setStyle("background:#FFFFFF;");    // customer borders
		//custLayout.setPack("center");
		custLayout.setAlign("left");
		custLayout.setWidth("100%");
		custLayout.setWidths("10%,30%,30%,15%,15%");
		
		//prodLayout.setStyle("background:#FFFFE0;");
		prodLayout.setAlign("left");
		prodLayout.setWidth("100%");
		prodLayout.setWidths("10%,20%,50%,7%,7%,6%");
		
		//gLayout.setStyle("background:#FF8db7;");
		prodLayout.setWidth("100%");
		prodLayout.setHeight("100%");
		barLayout.setWidth("100%");
		barLayout.setWidths("3%,3%,7%,7%,40%,10%,10%,10%,10%");
		barLayout.setAlign("right");
		totalLayout.setWidth("100%");
		totalLayout.setWidths("40%,10%,10%,10%,10%,10%,10%");
		totalLayout.setAlign("right");
		
		vmainLayout.appendChild(pcustLayout);
		  pcustLayout.appendChild(custLayout);
		vmainLayout.appendChild(pprodLayout);
		  pprodLayout.appendChild(prodLayout);
		vmainLayout.appendChild(pgLayout);
		  pgLayout.appendChild(gLayout);
		vmainLayout.appendChild(pbarLayout);
		  pbarLayout.appendChild(barLayout);
		vmainLayout.appendChild(ptotalLayout);
		   ptotalLayout.appendChild(totalLayout);
		
		// estilos y tama�os  de componentes
		m_form.setStyle("width:100%;height:100%;position:absolute");
        
		// Customer layout
		custLayout.appendChild(bPartnerLabel);   //customer
		custLayout.appendChild(bpartnerSearch.getComponent());
		custLayout.appendChild(new Label(" "));
		custLayout.appendChild(new Label(" Customer Name "));
		custLayout.appendChild(new Label(" Insurance Plan "));
		
		// Product Layout
		prodLayout.appendChild(productLabel);   // product label
		prodLayout.appendChild(productSearch.getComponent()); // product
		prodLayout.appendChild(new Label("                       ")); // product

		
		sendButton.setStyle(buttonYellow);
		preticketButton.setStyle(buttonGray);
		prodLayout.appendChild(sendButton);  
		//preticketButton.setImage(ThemeManager.getThemeResource("images/preticket.JPG"));
		
		prodLayout.appendChild(preticketButton);
		//payButton.setImage(ThemeManager.getThemeResource("images/pay.JPG"));
		payButton.setStyle(buttonGreen);
		prodLayout.appendChild(payButton);
		
		
		
		// grid Layout
		gLayout.appendChild(new Label(" Grilla 1"));
		gLayout.appendChild(new Label(" Grilla 2"));
		gLayout.appendChild(new Label(" Grilla 3"));
		gLayout.appendChild(new Label(" Grilla 4"));
		gLayout.appendChild(new Label(" Grilla 5"));
		gLayout.appendChild(new Label(" Grilla 6"));
		gLayout.appendChild(new Label(" Grilla 7"));
		gLayout.appendChild(new Label(" Grilla 8"));
		gLayout.appendChild(new Label(" Grilla 9"));
		// control bar layout

        plusButton.setStyle(buttonImg);		
		plusButton.setImage(ThemeManager.getThemeResource("images/add.JPG"));
		minusButton.setStyle(buttonImg);
		minusButton.setImage(ThemeManager.getThemeResource("images/delete3.JPG"));
		barLayout.appendChild(minusButton);
		barLayout.appendChild(plusButton);
		delete2Button.setStyle(buttonRed);
		editButton.setStyle(buttonOrange);
		barLayout.appendChild(delete2Button);
		barLayout.appendChild(editButton);
		
		
		barLayout.appendChild(new Label(""));
		
		barLayout.appendChild(new Label("Subtotal"));
		barLayout.appendChild(new Label(" $$$$$$$ "));
		
		barLayout.appendChild(new Label("TOTAL"));
		barLayout.appendChild(new Label(" $$$$$$$ "));
		// total Layout
		totalLayout.appendChild(new Label(""));
		totalLayout.appendChild(new Label("Discounts"));
		totalLayout.appendChild(new Label(" $$$$$$$ "));
		
		totalLayout.appendChild(new Label("Taxes"));
		totalLayout.appendChild(new Label(" $$$$$$$ "));
		
		//totalLayout.appendChild(new Label("Ticket#"));
		
		delete1Button.setStyle(buttonRed);
		exit1Button.setStyle(buttonOrange);
		
		totalLayout.appendChild(delete1Button);
		totalLayout.appendChild(exit1Button);
		
		
		
		m_form.appendChild(mainLayout);
		//iframe.setSrc(txtURL.getText());
		//txtURL.addEventListener(Events.ON_OK, this);
		
	}
	
	
	@Override
	public ADForm getForm() {
		return m_form;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().equals(txtURL)  && txtURL.getValue().trim().length() > 0) {
			// iframe.setSrc(txtURL.getValue());
		}
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		try {
			String name = evt.getPropertyName();
			Object value = evt.getNewValue();
			if (value==null){
				if(bpartnerId > 0){
					bpartnerSearch.setValue(bpartnerId);
				}else{
					try {
						Messagebox m1 = new Messagebox();
						int response = m1.show("Crear tercero ", "Info label", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
						
						if (response == Messagebox.OK){
							// ShowWindow.openPartnerWebPOS(0);
							
							System.out.println("abir ventana de creacion de tercero");
						}//response Ok
						System.out.println("Creacion de tercero");
						WSearchEditor.createBPartner(m_form.getWindowNo());  // open bpartner window
						WSearchEditor.createProduct(m_form.getWindowNo());  // open bpartner window
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()+".valueChange ERROR: ", e);
					}
				}
				return;
			}//if Value
			//  BPartner
			if (name.equals("C_BPartner_ID")){
				bpartnerSearch.setValue(value);
				bpartnerId = ((Integer)value).intValue();
				//setPartner();
			}//C_BPartner_ID
			else if (name.equals("M_Product_ID")) {
				productSearch.setValue(value);
				productId = ((Integer)value).intValue();
			}
			//productTextBox.setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".valueChange - ERROR: " + e.getMessage(), e);
		}
		
	}

}
