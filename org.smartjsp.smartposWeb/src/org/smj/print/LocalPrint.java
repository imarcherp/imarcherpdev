package org.smj.print;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Date;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

public class LocalPrint implements Printable {

	private PrintService service = null;
	private String pMessage = "";
	private DocFlavor myFormat = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	private PrintRequestAttributeSet config = new HashPrintRequestAttributeSet();
	private PrinterJob job2 = null;

	/**
	 * @param args
	 */

	public LocalPrint() {
		config.add(new Copies(1));
		// config.add(MediaSizeName.NA_LETTER);
		// config.add(Sides.ONE_SIDED);

	}

	public static void main(String[] args) {

		String sep = ";"; // System.getProperty("line.separator");
		LocalPrint lp = new LocalPrint();
		lp.listAvailablePrinters();
		lp.selectPrinter("PDFCreator"); // HP Deskjet 5520 series (Network)
										// PDFCreator

		String mensaje1 = "   POS Notifications title" + sep;
		mensaje1 += "   Sales Date:\t " + new Date() + sep;
		mensaje1 += "   -------------------------------------------" + sep;
		mensaje1 += "   Qty:\t 12345" + sep;
		mensaje1 += "   Product:" + sep; 
		mensaje1 += "   Tylenol with long description to be sold soon" + sep;
		mensaje1 += "   Locator:\t 12345	" + sep;
		mensaje1 += "   -------------------------------------------" + sep;
		mensaje1 += "   User:\t pharmacyAdm" + sep;

		lp.printTicket(mensaje1);
		// lp.printNotificationFile("j:/NotifyTest1.txt");
	}

	public void printNotificationString2(String message) {
		// FileInputStream textStream = null;
		message = message + "\f";
		StringReader textStream = null;
		try {
			textStream = new StringReader(message); //
			// textStream = new FileInputStream("j:/NotifyTest1.txt");
			// //TicketNuevo1.pdf
		} catch (Exception ffne) {
			System.out.print("Error al leer archivo");
		}
		Doc myDoc = new SimpleDoc(textStream, myFormat, null);
		DocPrintJob job = service.createPrintJob();
		try {
			// PrintJobWatcher2 pjw = new PrintJobWatcher2(job);
			job.print(myDoc, config);
			// pjw.waitForDone();
			textStream.close();
		} catch (Exception pe) {
			pe.printStackTrace();
		}

	}

	/*
	 * Metodo para imprimir ticket con el menor margen posible Es usado como
	 * metodo temporal a cabio de printNotificationString3(String)
	 */
	public void printTicket(String message) {
		try {
			/* Inicializaci�n de las variables basicas para la impresion */
			pMessage = message;
			job2 = PrinterJob.getPrinterJob();
			job2.setPrintService(service);
			job2.setJobName("SmartPOS notification");
			PageFormat pf = job2.defaultPage();
			Paper ticketPaper = new Paper();

			/* Se asigna el ancho, alto y margen de la hoja */
			double paperWidth = 5.111;
			double paperHeight = 2;
			double margin = 10;

			ticketPaper.setSize(paperWidth * 72.0, paperHeight * 72.0);
			ticketPaper.setImageableArea(margin, margin, ticketPaper.getWidth()
					- 2 * margin, ticketPaper.getHeight() - 2 * margin);
			pf.setPaper(ticketPaper);

			job2.setPrintable(this, pf);
			job2.print(config);
		} catch (PrinterException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Metodo para obtener las lineas con tabs y luego obtener la palabra con
	 * mas espacio en pantalla
	 */
	public int getLongestWord(String text, FontMetrics fontMetrics) {
		int longestWord = 0;

		for (String line : text.split(";")) {
			String[] words = line.split("\t");
			if (words.length > 1 && fontMetrics.stringWidth(words[0]) > longestWord) {
				longestWord = fontMetrics.stringWidth(words[0]);
			}
		}

		return longestWord;
	}

	public void printNotificationString(String message) {
		// FileInputStream textStream = null;
		message = message + "\f";
		InputStream textStream = null;
		try {
			textStream = new ByteArrayInputStream(message.getBytes("UTF8")); // textStream
																				// =
																				// new
																				// ByteArrayInputStream(message.getBytes("UTF8"));
																				// //
			// textStream = new FileInputStream("j:/NotifyTest1.txt");
			// //TicketNuevo1.pdf
		} catch (Exception ffne) {
			System.out.print("Error al leer archivo");
		}
		Doc myDoc = new SimpleDoc(textStream, myFormat, null);
		DocPrintJob job = service.createPrintJob();
		try {
			// PrintJobWatcher2 pjw = new PrintJobWatcher2(job);
			job.print(myDoc, config);
			// pjw.waitForDone();
			textStream.close();
		} catch (Exception pe) {
			pe.printStackTrace();
		}

	}

	public void printNotificationFile(String fileName) {
		FileInputStream textStream = null;
		// InputStream textStream = null;
		try {
			// textStream = new ByteArrayInputStream(message.getBytes("UTF8"));
			textStream = new FileInputStream(new File(fileName)); // TicketNuevo1.pdf
		} catch (Exception ffne) {
			System.out.print("Error al leer archivo");
		}
		Doc myDoc = new SimpleDoc(textStream, myFormat, null);
		DocPrintJob job = service.createPrintJob();
		try {
			// PrintJobWatcher2 pjw = new PrintJobWatcher2(job);
			job.print(myDoc, config);
			// pjw.waitForDone();
			textStream.close();

			// send FF to eject the page
			InputStream ff = new ByteArrayInputStream("\f".getBytes());
			Doc docff = new SimpleDoc(ff, myFormat, null);
			DocPrintJob jobff = service.createPrintJob();
			// pjw = new PrintJobWatcher2(jobff);
			jobff.print(docff, null);
			// pjw.waitForDone();
		} catch (Exception pe) {
			pe.printStackTrace();
		}
	}

	public PrintService selectPrinter(String whichone) {
		PrintService[] services = PrintServiceLookup.lookupPrintServices(
				myFormat, null);
		PrintService myService = null;
		for (int i = 0; i < services.length; i++) {
			if (services[i].getName().equalsIgnoreCase(whichone)) {
				myService = services[i];
				System.out.println("Printer: " + services[i].getName());
			} else {
				// System.out.println(services[i].getName());
			}
		}
		service = myService;
		return myService;
	}

	public void selectDefaultPrinter() {
		service = PrintServiceLookup.lookupDefaultPrintService();
	}

	public void listAvailablePrinters() {
		System.out
				.println("SmartPOS: List of Available printers for this Server");
		PrintService[] services = PrintServiceLookup.lookupPrintServices(
				myFormat, null);
		for (int i = 0; i < services.length; i++) {
			System.out.println(services[i].getName());
		}
	}

	public void listSupportedDocByPrinters() {
		DocFlavor[] flavors = service.getSupportedDocFlavors();
		for (int i = 0; i < flavors.length; i++) {
			System.out.println("\t" + flavors[i].getMediaType());

		}

	}

	public void printNotificationString3(String message) {
		pMessage = message;
		try {
			job2 = PrinterJob.getPrinterJob();
			job2.setPrintService(service);
			job2.setJobName("SmartPOS notification ");
			job2.setPrintable(this);
			job2.print();
		} catch (Exception pe) {
			pe.printStackTrace();
		}

	}

	public int print(Graphics g, PageFormat pf, int pageIndex)
			throws PrinterException {
		if (pageIndex > 0) { /* We have only one page, and 'page' is zero-based */
			return NO_SUCH_PAGE;
		}
		int interline = 9;
		Graphics2D g2 = (Graphics2D) g;
		g2.setFont(new Font("Courier New", Font.PLAIN, 8));

		int y = (int) pf.getImageableY();
		int x = (int) pf.getImageableX();

		String s[] = pMessage.split(";");
		int longestWord = getLongestWord(pMessage, g.getFontMetrics());
		for (String string : s) {
			x = (int) pf.getImageableX();
			y += interline;

			/*
			 * (Tener en cuenta tabulaciones) Se divide las palabras por tab, y
			 * luego se situan en la posicion x. Si es la primera palabra la x
			 * sera desde el inicio del margen, si es la segunda o mas desde x +
			 * los parametros de espacio estandar en la fuente de la grafica
			 */
			for (String line : string.split("\t")) {
				g2.drawString(line, x, y);
				// Se suma 1 ya que con la nueva fuente la palabra User queda
				// muy pegada a su valor
				// x += g2.getFontMetrics().get + 1;
				x += longestWord;
			}
		}
		return Printable.PAGE_EXISTS;
	}

}

class PrintJobWatcher2 {
	boolean done = false;

	PrintJobWatcher2(DocPrintJob job) {
		job.addPrintJobListener(new PrintJobAdapter() {
			public void printJobCanceled(PrintJobEvent pje) {
				allDone();
			}

			public void printJobCompleted(PrintJobEvent pje) {
				allDone();
			}

			public void printJobFailed(PrintJobEvent pje) {
				allDone();
			}

			public void printJobNoMoreEvents(PrintJobEvent pje) {
				allDone();
			}

			void allDone() {
				synchronized (PrintJobWatcher2.this) {
					done = true;
					// System.out.println("Printing done ...");
					PrintJobWatcher2.this.notify();
				}
			}
		});
	}

	public synchronized void waitForDone() {
		try {
			while (!done) {
				wait();
			}
		} catch (InterruptedException e) {
		}
	}
}
