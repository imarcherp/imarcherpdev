package com.smj.util;

import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * Class to get the data from Organization that represents a System Configuration Value (Instead of a SysConfig)
 * @author Dany - SmartJSP
 *
 */
public class SMJConfig {

	private static CLogger		log = CLogger.getCLogger (SMJConfig.class);

	/**
	 * Get system configuration property of type string
	 * @param fieldName
	 * @param defaultValue
	 * @param Organization ID
	 * @return String
	 */
	public static String getValueEx(String fieldName, String defaultValue, int AD_Org_ID) throws DBException {
		String sql = "SELECT " + fieldName + " FROM AD_Org"
				+ " WHERE AD_Org_ID IN (0, ?) AND IsActive='Y'"
				+ " ORDER BY AD_Org_ID DESC";

		String value = null;

		value = DB.getSQLValueStringEx(null, sql, AD_Org_ID);

		if (value == null)
			return defaultValue;

		return value.trim();
	}

	/**
	 * Get system configuration property of type string
	 * @param fieldName
	 * @param defaultValue
	 * @param Organization ID
	 * @return String
	 */
	public static String getValue(String fieldName, String defaultValue, int AD_Org_ID) {
		String sql = "SELECT " + fieldName + " FROM AD_Org"
				+ " WHERE AD_Org_ID IN (0, ?) AND IsActive='Y'"
				+ " ORDER BY AD_Org_ID DESC";

		String value = null;

		value = DB.getSQLValueStringEx(null, sql, AD_Org_ID);

		if (value == null)
			return defaultValue;

		return value.trim();
	}

	/**
	 * Get system configuration property of type boolean
	 * @param fieldName
	 * @param defaultValue
	 * @param Client ID
	 * @param Organization ID
	 * @return boolean
	 */
	public static boolean getBooleanValue(String fieldName, boolean defaultValue, int AD_Org_ID) {
		String s = getValue(fieldName, null, AD_Org_ID);
		if (s == null || s.isEmpty())
			return defaultValue;

		if ("Y".equalsIgnoreCase(s))
			return true;
		else if ("N".equalsIgnoreCase(s))
			return false;
		else
			return Boolean.valueOf(s).booleanValue();
	}

	/**
	 * Get system configuration property of type double
	 * @param fieldName
	 * @param defaultValue
	 * @param Organization ID
	 * @return double
	 */
	public static double getDoubleValue(String fieldName, double defaultValue, int AD_Org_ID)
	{
		String s = getValue(fieldName, null, AD_Org_ID);
		if (s == null || s.isEmpty())
			return defaultValue;

		try {
			return Double.parseDouble(s);
		}
		catch (NumberFormatException e)
		{
			log.log(Level.SEVERE, "getDoubleValue (" + s + ")", e);
		}
		return defaultValue;
	}

	/**
	 * Get system configuration property of type int
	 * @param fieldName
	 * @param defaultValue
	 * @param Organization ID
	 * @return int
	 */
	public static int getIntValue(String fieldName, int defaultValue, int AD_Org_ID)
	{
		String s = getValue(fieldName, null, AD_Org_ID);
		if (s == null || s.isEmpty())
			return defaultValue; 

		try {
			return Integer.parseInt(s);
		}
		catch (NumberFormatException e)
		{
			log.log(Level.SEVERE, "getIntValue (" + s + ")", e);
		}
		return defaultValue;
	}

	/**
	 * Get system configuration property of type int[]
	 * @param fieldName
	 * @param regex
	 * @param Organization ID
	 * @return Integer[]
	 */
	public static int[] getArrayIntValue(String fieldName, String regex, int AD_Org_ID) {
		String s = getValue(fieldName, null, AD_Org_ID);

		if (s == null || s.isEmpty())
			return null;

		String[] values = s.split(regex);
		int[] intValues = new int[values.length];

		try {
			for (int i = 0; i < values.length; i++) {
				intValues[i] = Integer.parseInt(values[i].trim());
			}

			return intValues;
		}
		catch (NumberFormatException e)
		{
			log.log(Level.SEVERE, "getIntValue (" + s + ")", e);
		}

		return null;
	}

	/**
	 * Get system configuration property of type String[]
	 * @param fieldName
	 * @param regex
	 * @param Organization ID
	 * @return String[]
	 */
	public static String[] getArrayStringValue(String fieldName, String regex, int AD_Org_ID) {
		String s = getValue(fieldName, null, AD_Org_ID);

		if (s == null || s.isEmpty())
			return null;
		
		String[] values = s.split(regex);
		
		for(int i = 0; i < values.length; i++) 
			values[i] = values[i].trim();
				
		return values;
	}
	
	/**
	 * Method to test different configuration fields (Organization)
	 */
	public static void testCases() {
		int[] 		adminRoles				= SMJConfig.getArrayIntValue("AdminRoles", ";", Env.getAD_Org_ID(Env.getCtx()));
		int 		C_BPartner_ID 			= SMJConfig.getIntValue("C_BPartner_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_Charge_ID 			= SMJConfig.getIntValue("C_Charge_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_ChargeDeposit_ID 		= SMJConfig.getIntValue("C_ChargeDeposit_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_ChargeTips_ID 		= SMJConfig.getIntValue("C_ChargeTips_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_Currency_ID			= SMJConfig.getIntValue("C_Currency_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_PaymentTerm_ID		= SMJConfig.getIntValue("C_PaymentTerm_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		C_Tax_ID 				= SMJConfig.getIntValue("C_Tax_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		String 		defaultPrinter			= SMJConfig.getValue("DefaultPrinter", null, Env.getAD_Org_ID(Env.getCtx()));
		boolean 	clientPrinting			= SMJConfig.getBooleanValue("IsPosClientPrinting", true, Env.getAD_Org_ID(Env.getCtx()));
		boolean 	decountRestricted		= SMJConfig.getBooleanValue("IsDiscountRestricted", false, Env.getAD_Org_ID(Env.getCtx()));
		boolean 	posTax					= SMJConfig.getBooleanValue("IsPosTax", false, Env.getAD_Org_ID(Env.getCtx()));
		boolean 	sendMandatory			= SMJConfig.getBooleanValue("IsSendMandatory", false, Env.getAD_Org_ID(Env.getCtx()));
		int[]		listPayTerm				= SMJConfig.getArrayIntValue("ListPayTerm", ",", Env.getAD_Org_ID(Env.getCtx()));
		int 		C_BPartner_Location_ID	= SMJConfig.getIntValue("C_BPartner_Location_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int			M_PriceList_Version_ID	= SMJConfig.getIntValue("M_PriceList_Version_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int			M_Product_Category_ID	= SMJConfig.getIntValue("M_Product_Category_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		M_Warehouse_ID			= SMJConfig.getIntValue("M_Warehouse_ID", 0, Env.getAD_Org_ID(Env.getCtx()));
		int			checkOpenOrdersTime		= SMJConfig.getIntValue("POS_CheckOpenOrdersTime", 0, Env.getAD_Org_ID(Env.getCtx()));
		int 		checkOrdersTime			= SMJConfig.getIntValue("POS_CheckOrdersTime", 0, Env.getAD_Org_ID(Env.getCtx()));
		int			decimals				= SMJConfig.getIntValue("POS_Decimals", 0, Env.getAD_Org_ID(Env.getCtx()));
		String[]	locale					= SMJConfig.getArrayStringValue("POS_Locale", ",", Env.getAD_Org_ID(Env.getCtx()));
		String[]	printersByGroup			= SMJConfig.getArrayStringValue("POS_PrintersByGroup", ";", Env.getAD_Org_ID(Env.getCtx()));
		int			sizeImage				= SMJConfig.getIntValue("SizeImage", 0, Env.getAD_Org_ID(Env.getCtx()));
		
		System.out.println("\nAdminRoles -> ");
		for (int adminRol: adminRoles)
			System.out.println("AdminRol: " + adminRol);
		
		System.out.println("\nC_BPartner_ID -> " + C_BPartner_ID);
		System.out.println("C_Charge_ID -> " + C_Charge_ID);
		System.out.println("C_ChargeDeposit_ID -> " + C_ChargeDeposit_ID);
		System.out.println("C_ChargeTips_ID -> " + C_ChargeTips_ID);
		System.out.println("C_Currency_ID -> " + C_Currency_ID);
		System.out.println("C_PaymentTerm_ID -> " + C_PaymentTerm_ID);
		System.out.println("C_Tax_ID -> " + C_Tax_ID);
		System.out.println("defaultPrinter -> " + defaultPrinter);
		System.out.println("clientPrinting -> " + clientPrinting);
		System.out.println("decountRestricted -> " + decountRestricted);
		System.out.println("posTax -> " + posTax);
		System.out.println("sendMandatory -> " + sendMandatory);
		
		System.out.println("\nlistPayTerm -> ");
		for (int payTerm: listPayTerm)
			System.out.println("payTerm: " + payTerm);
		
		System.out.println("\nC_BPartner_Location_ID -> " + C_BPartner_Location_ID);
		System.out.println("M_PriceList_Version_ID -> " + M_PriceList_Version_ID);
		System.out.println("M_Product_Category_ID -> " + M_Product_Category_ID);
		System.out.println("M_Warehouse_ID -> " + M_Warehouse_ID);
		System.out.println("checkOpenOrdersTime -> " + checkOpenOrdersTime);
		System.out.println("checkOrdersTime -> " + checkOrdersTime);
		System.out.println("decimals -> " + decimals);
		
		System.out.println("\nlocale -> ");
		for(String localeTmp: locale)
			System.out.println("localeTmp: " + localeTmp);
		
		System.out.println("\nprintersByGroup -> ");
		for(String printer: printersByGroup)
			System.out.println("Printer: " + printer);
		
		System.out.println("\nSizeImage -> " + sizeImage + "\n");
	}
}
