package com.smj.util;

import org.adempiere.webui.apps.AEnv;
import org.compiere.model.MQuery;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;
import org.smj.util.OpenWindow;

public class ShowWindow {


	/**
	 * abre el cierre de caja creado - open th close cash
	 * @param invoiceId
	 * @return boolean
	 */
	public static boolean openCloseCash(Integer elementId){
		String whereString = " smj_closecash_ID= "+elementId+" "; 
		MQuery query = new MQuery("smj_closecash"); 
		query.addRestriction(whereString); 
		int AD_Window_ID = 1000004; // close cash window is 1000004
		AEnv.zoom(AD_Window_ID, query);
		return true; 
	}//openInvoiceCustomer

	/**
	 * abre la factura creada - open created invoice
	 * @param invoiceId
	 * @return boolean
	 */
	public static boolean openInvoiceCustomer(Integer Record_ID){
		int AD_Window_ID = MSysConfig.getIntValue("SMJ_InvoiceWindow", 167, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
		return openWindowAsync(AD_Window_ID, "C_Invoice", Record_ID); 
	}//openInvoiceCustomer

	/**
	 * abre la factura creada - open created invoice
	 * @param invoiceId
	 * @return boolean
	 */
	public static boolean openInvoiceVendor(Integer invoiceId){
		///abre una factura
		//		  System.out.println("abrir factura proveedor ...."+invoiceId);
		String whereString = " C_Invoice_ID= "+invoiceId+" "; 
		MQuery query = new MQuery("C_Invoice"); 
		query.addRestriction(whereString); 
		int AD_Window_ID = 183; // The id invoice-vendor 183.
		AEnv.zoom(AD_Window_ID, query);
		return true; 
	}//openInvoiceCustomer

	/**
	 * abre la ventana con el pago creado - open created payment window
	 * @param Record_ID
	 * @return boolean
	 */
	public static boolean openPayment(Integer Record_ID){		
		int AD_Window_ID = 195; //"Payment";"Process Payments and Receipts" 195
		return openWindowAsync(AD_Window_ID, "C_Payment", Record_ID);
	}//openPayment

	/**
	 * Abre venta de tercero punto de venta Web - 
	 * open Partner Web POS window
	 * @param elementId
	 * @return
	 */
	public static boolean openPartnerWebPOS(Integer elementId){
		///abre una factura
		//		  System.out.println("abrir Partner Web POS ...."+elementId);
		String whereString = " C_BPartner_ID= " + elementId + " "; 
		MQuery query = new MQuery("C_BPartner"); 
		query.addRestriction(whereString); 
		int AD_Window_ID = 123;
		AEnv.zoom(AD_Window_ID, query);
		return true; 
	}//openPartnerWebPOS

	/**
	 * abre la orden de venta creada - open created sales order
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openSalesOrder(Integer Record_ID){
		int AD_Window_ID = MSysConfig.getIntValue("SMJ_SalesOrderWindow", 143, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
		return openWindowAsync(AD_Window_ID, "C_Order", Record_ID);  
	}//openSalesOrder

	/**
	 * abre la orden de compra creada - open created purchase order
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openPurchaseOrder(Integer elementId){
		///abre una factura
		//		  System.out.println("abrir orden compra ...."+elementId);
		String whereString = " C_Order_ID= "+elementId+" "; 
		MQuery query = new MQuery("C_Order"); 
		query.addRestriction(whereString); 
		int AD_Window_ID = 181; // pruchase order 181 
		AEnv.zoom(AD_Window_ID, query);
		return true; 
	}//openPurchaseOrder


	/**
	 * abre la orden de trabajo - open request 
	 * @param elementId
	 * @return boolean
	 */
	public static boolean openRequestOrder(Integer elementId){

		String whereString = " R_Request_ID= "+elementId+" "; 
		MQuery query = new MQuery("R_Request"); 
		query.addRestriction(whereString); 
		int AD_Window_ID = 1000044; //
		AEnv.zoom(AD_Window_ID, query);
		return true; 
	}//openPurchaseOrder




	/**
	 * metodo generico para abrir ventanas - 
	 * generic method to open windows
	 * @param AD_Window_ID
	 * @param tableName
	 * @param records
	 * @return
	 */
	public static boolean openWindow(Integer AD_Window_ID, String tableName, Integer... records) {
		OpenWindow ow = new OpenWindow();
		ow.AD_Window_ID = AD_Window_ID;
		ow.tableName = tableName;
		ow.records = records;
		AEnv.executeAsyncDesktopTask(ow);
		return true; 
	}

	/**
	 * metodo generico para abrir ventanas de forma asincronica en idempiere - 
	 * generic method to open windows
	 * @param AD_Window_ID
	 * @param tableName
	 * @param records
	 * @return
	 */
	public static boolean openWindowAsync(Integer AD_Window_ID, String tableName, Integer... records) {
		///abre una close cash de forma asyncronica
		OpenWindow ow = new OpenWindow();
		ow.AD_Window_ID = AD_Window_ID;
		ow.tableName = tableName;
		ow.records = records;
		AEnv.executeAsyncDesktopTask(ow);
		return true; 
	}//openWindow



}//ShowWindow
