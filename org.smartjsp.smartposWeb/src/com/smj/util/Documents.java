package com.smj.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.form.Allocation;
import org.compiere.model.MAllocationHdr;
import org.compiere.model.MAllocationLine;
import org.compiere.model.MBPartner;
import org.compiere.model.MBankAccount;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPayment;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTax;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.smj.model.PaymentList;
import com.smj.reactive.Consumer;

/**
 * @version <li>SmartJSP: Documents, 2013/03/08
 *          <ul TYPE ="circle">
 *          <li>Clase que contienen los metodos de creacion de documentos
 *          <li>Class to create documents
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class Documents {

	/** Logger */
	public static CLogger log = CLogger.getCLogger(Allocation.class);
	private static Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private static Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());
	private static Boolean useTransport = MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, lclientId, lorgId);

	//	private final static Integer warrantyPriceList = Integer.parseInt(MSysConfig
	//			.getValue("SMJ_WARRANTYPRICELIST", Env.getAD_Client_ID(Env.getCtx()))
	//			.trim(),Env.getAD_Org_ID(Env.getCtx()));
	//	private final static Integer warrantyRStatus = Integer.parseInt(MSysConfig
	//			.getValue("SMJ-STATEWOWARRANTY", Env.getAD_Client_ID(Env.getCtx()))
	//			.trim());

	/**
	 * crea la orden de venta - 
	 * create sales order
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param locationId
	 * @param orderDate
	 * @param SalesRepId
	 * @param payTermId
	 * @param standardOrder
	 * @param warehouseId
	 * @param salesListId
	 * @return MOrder
	 */
	public static MOrder createSalesOrder(String trxName, String desc, Integer partnerId, Integer locationId,
			Date orderDate, Integer SalesRepId, Integer payTermId, String payRule, Integer standardOrder,
			Integer warehouseId, Integer salesListId, String plate, Integer vehicleId, String workOrder,Integer posTicketId){

		MOrder order = null;
		try {
			order = new MOrder(Env.getCtx(), 0, trxName);
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationId);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N");
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);

			order.setC_PaymentTerm_ID(payTermId);
			order.setM_Warehouse_ID(warehouseId);
			order.set_CustomColumn("smj_tmpwebsales_id", posTicketId); // store a reference to the original ticket id

			//para orden de venta - For order Sales
			order.setIsSOTrx(true); 
			order.setC_DocTypeTarget_ID(standardOrder);
			order.setC_DocType_ID(standardOrder);		
			order.setPaymentRule(payRule);			
			order.setM_PriceList_ID(salesListId);

			if(plate!=null){
				order.set_ValueOfColumn("smj_plate", plate);
			}

			Boolean pok = order.save();
			if (!pok) {
				log.log(Level.SEVERE, "Error save Create Order :: "	+ desc);
				return null;
			} 

		} catch (Exception e) {
			order = null;
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createSalesOrder - ERROR: Create Order :: "+desc +" - "+ e.getMessage(), e);
		} 
		return order;
	}// createSalesOrder

	/**
	 * crea la orden de compra - 
	 * create purchase order
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param orderDate
	 * @param SalesRepId
	 * @param payTermId
	 * @param standardOrder
	 * @param warehouseId
	 * @param purchaseListId
	 * @param isSystem
	 * @return
	 */
	public static MOrder createPurchaseOrder(String trxName, String desc, Integer partnerId, 
			Date orderDate, Integer SalesRepId, Integer payTermId, Integer standardOrder,
			Integer warehouseId, Integer purchaseListId, Boolean isSystem){

		MOrder order = null;
		Integer locationId = DataQueries.getLocationPartner(partnerId);
		try {
			order = new MOrder(Env.getCtx(), 0, trxName);
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(SalesRepId);
			order.setDateOrdered(new Timestamp(orderDate.getTime()));
			order.setDatePromised(new Timestamp(orderDate.getTime()));
			order.setC_BPartner_ID(partnerId);
			order.setC_BPartner_Location_ID(locationId);
			order.setDescription(desc);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N");
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);


			order.setC_PaymentTerm_ID(payTermId); 
			order.setM_Warehouse_ID(warehouseId);

			//para orden de compra - For purchase order
			order.setIsSOTrx(false); 
			order.setC_DocTypeTarget_ID(standardOrder); 
			order.setC_DocType_ID(standardOrder);
			order.setPaymentRule("P"); 
			order.setM_PriceList_ID(purchaseListId);

			order.set_ValueOfColumn("smj_isSystem", isSystem);
			Boolean pok = order.save();
			if (!pok) {
				log.log(Level.SEVERE,Documents.class.getName() + 
						".createPurchaseOrder - ERROR: Create Purchase Order :: "+desc );
				return null;
			} 

		} catch (Exception e) {
			order = null;
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPurchaseOrder - ERROR: Create Purchase Order :: "+desc +" - "+ e.getMessage(), e);
		} 
		return order;
	}// createPurchaseOrder

	/**
	 * crea las lineas de la orden - 
	 * create line order
	 * @param order
	 * @param productId
	 * @param qty
	 * @param price
	 * @param total
	 * @param description
	 * @return Boolean
	 */
	public static Integer createOrderLine(MOrder order, Integer productId, BigDecimal qty, Integer uomId,
			BigDecimal price, String description, Integer attInstanceId, Integer locatorId,
			Integer bPartnerId, Boolean isSystem, Integer provider_ID, Date orderDate
			, String plate, Integer workOrderLineId, Integer sequence) throws Exception {
		Integer code = 0;

		BigDecimal divideRate = DataQueries.getUOMDivideRate(productId, uomId);
		MProduct p = MProduct.get(Env.getCtx(), productId);
		Integer taxId = SMJConfig.getIntValue("C_Tax_ID", 0, lorgId);

		// validates if applies always the POS Tax
		if (!SMJConfig.getBooleanValue("IsPosTax", false, lorgId)) {

			Integer taxIdtmp = DataQueries.getProductTax(productId);

			if (taxIdtmp > 0)
				taxId = taxIdtmp;
		}

		if (uomId <= 0)   // for insurance coverage lines we assume each for unit
			uomId = 100;

		Integer bPlocation = DataQueries.getLocationPartner(bPartnerId);
		MOrderLine line = new MOrderLine(order);
		line.setC_Order_ID(order.getC_Order_ID());
		line.setIsActive(true);
		line.setDescription(description);
		line.setC_BPartner_ID(bPartnerId);
		line.setC_BPartner_Location_ID(bPlocation);
		line.setM_Warehouse_ID(order.getM_Warehouse_ID());
		line.setC_Currency_ID(order.getC_Currency_ID());
		line.setDateOrdered(new Timestamp(orderDate.getTime()));
		line.setDatePromised(new Timestamp(orderDate.getTime()));
		line.setM_Product_ID(productId);
		line.setC_UOM_ID(uomId);
		line.setDiscount(Env.ZERO);
		line.setFreightAmt(Env.ZERO); 

		if (p == null || !p.getProductType().equals(MProduct.PRODUCTTYPE_Service))
			line.setC_Tax_ID(taxId);

		line.setIsDescription(false);
		line.setQtyEntered(qty);
		line.setQtyOrdered(qty.multiply(divideRate));
		line.setQtyDelivered(Env.ZERO);
		line.setQtyInvoiced(Env.ZERO);
		line.setQtyReserved(Env.ZERO);
		line.setRRAmt(Env.ZERO);
		line.setPriceLimit(Env.ZERO);			
		line.setPriceEntered(price);
		line.setPriceList(price.divide(divideRate, 2, RoundingMode.HALF_UP));
		line.setPriceActual(price.divide(divideRate, 2, RoundingMode.HALF_UP));
		line.setPriceCost(Env.ZERO);
		line.setProcessed(false);
		line.setQtyLostSales(Env.ZERO);

		if(locatorId > 0)
			line.set_ValueOfColumn("smj_locator_ID", locatorId);

		if(sequence != null && sequence >0)
			line.setLine(sequence);

		if (line.getParent().isComplete()) {
			line.getParent().reActivateIt();
			line.getParent().setDocStatus(DocAction.STATUS_Drafted);
			line.getParent().saveEx();
		}

		line.saveEx();
		code = line.getC_OrderLine_ID();

		return code;
	}// createOrderLine
	
	public static Integer createOrderLineTrans(MOrder order, Integer productId, BigDecimal qty, Integer uomId,
			BigDecimal price, String description, Integer attInstanceId, Integer locatorId,
			Integer bPartnerId, Boolean isSystem, Integer provider_ID, Date orderDate
			, String plate, Integer workOrderLineId, Integer sequence, String trxName) throws Exception {
		Integer code = 0;

		BigDecimal divideRate = DataQueries.getUOMDivideRate(productId, uomId);
		MProduct p = MProduct.get(Env.getCtx(), productId);
		Integer taxId = SMJConfig.getIntValue("C_Tax_ID", 0, lorgId);

		// validates if applies always the POS Tax
		if (!SMJConfig.getBooleanValue("IsPosTax", false, lorgId)) {

			Integer taxIdtmp = DataQueries.getProductTax(productId);

			if (taxIdtmp > 0)
				taxId = taxIdtmp;
		}

		if (uomId <= 0)   // for insurance coverage lines we assume each for unit
			uomId = 100;

		Integer bPlocation = DataQueries.getLocationPartner(bPartnerId);
		MOrderLine line = new MOrderLine(order);
		line.set_TrxName(trxName);
		line.setC_Order_ID(order.getC_Order_ID());
		line.setIsActive(true);
		line.setDescription(description);
		line.setC_BPartner_ID(bPartnerId);
		line.setC_BPartner_Location_ID(bPlocation);
		line.setM_Warehouse_ID(order.getM_Warehouse_ID());
		line.setC_Currency_ID(order.getC_Currency_ID());
		line.setDateOrdered(new Timestamp(orderDate.getTime()));
		line.setDatePromised(new Timestamp(orderDate.getTime()));
		line.setM_Product_ID(productId);
		line.setC_UOM_ID(uomId);
		line.setDiscount(Env.ZERO);
		line.setFreightAmt(Env.ZERO); 

		if (p == null || !p.getProductType().equals(MProduct.PRODUCTTYPE_Service))
			line.setC_Tax_ID(taxId);

		line.setIsDescription(false);
		line.setQtyEntered(qty);
		line.setQtyOrdered(qty.multiply(divideRate));
		line.setQtyDelivered(Env.ZERO);
		line.setQtyInvoiced(Env.ZERO);
		line.setQtyReserved(Env.ZERO);
		line.setRRAmt(Env.ZERO);
		line.setPriceLimit(Env.ZERO);			
		line.setPriceEntered(price);
		line.setPriceList(price.divide(divideRate, 2, RoundingMode.HALF_UP));
		line.setPriceActual(price.divide(divideRate, 2, RoundingMode.HALF_UP));
		line.setPriceCost(Env.ZERO);
		line.setProcessed(false);
		line.setQtyLostSales(Env.ZERO);

		if(locatorId > 0)
			line.set_ValueOfColumn("smj_locator_ID", locatorId);

		if(sequence != null && sequence >0)
			line.setLine(sequence);

		if (line.getParent().isComplete()) {
			line.getParent().reActivateIt();
			line.getParent().setDocStatus(DocAction.STATUS_Drafted);
			line.getParent().saveEx();
		}

		line.saveEx();
		code = line.getC_OrderLine_ID();


		return code;
	}// createOrderLineTrans

	/**
	 * Documento para el manejo de inventarios - 
	 * Document to manage inventory
	 * @param trxName
	 * @param desc
	 * @param order
	 * @param date
	 * @param documentId
	 * @param sales
	 * @return MInOut
	 */
	public static MInOut createMInOut(String trxName, String desc, MOrder order, Date date, 
			Integer documentId, Boolean sales) throws Exception {

		MInOut io = null;
		io = new MInOut(Env.getCtx(), 0, trxName);
		io.setPOReference("SmartPOS");
		io.setDescription(desc);
		io.setIsSOTrx(sales); 
		io.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		io.setC_DocType_ID(documentId);// doctype 1000014-MM Receipt -- Warehouse Order 1000032
		io.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
		io.setC_BPartner_ID(order.getC_BPartner_ID());
		io.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
		io.setM_Warehouse_ID(order.getM_Warehouse_ID());
		io.setC_Order_ID(order.getC_Order_ID());
		io.setFreightAmt(Env.ZERO);
		io.setChargeAmt(Env.ZERO);
		io.setCreateFrom("N");
		io.setGenerateTo("N");
		io.setCreateConfirm("N");
		io.setCreatePackage("N");

		if (sales){
			io.setMovementType(MInOut.MOVEMENTTYPE_CustomerShipment);
		}else{
			io.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
		}

		io.setSalesRep_ID(order.getSalesRep_ID());
		io.setMovementDate(new Timestamp(date.getTime()));
		io.setDateAcct(new Timestamp(date.getTime()));
		io.saveEx();

		return io;
	}// createMInOut

	/**
	 * crea la linea de la entrega - 
	 * create in/out line
	 * @param io
	 * @param desc
	 * @param productId
	 * @param locatorId
	 * @param qty
	 * @param instanceId
	 * @return
	 */
	public static MInOutLine createIOLine(MInOut io, String desc, Integer productId, Integer locatorId, 
			BigDecimal qty, Integer uomId, Integer orderLine, Integer sequence, int m_attributesetinstance_id){
		MInOutLine line = null;
		Boolean flag = true;
		BigDecimal divideRate = DataQueries.getUOMDivideRate(productId, uomId);

		try {
			line = new MInOutLine(io);
			line.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			line.setM_InOut_ID(io.getM_InOut_ID());
			line.setC_OrderLine_ID(orderLine);
			line.setM_Product_ID(productId);
			line.setM_Locator_ID(locatorId);// 1000000
			line.setC_UOM_ID(uomId);
			line.setQtyEntered(qty);
			line.setMovementQty(qty.multiply(divideRate));
			line.setM_AttributeSetInstance_ID(m_attributesetinstance_id);

			if (sequence != null && sequence >0)
				line.setLine(sequence);

			line.setDescription(desc);
			flag = line.save();

			if (!flag)
				return null;

		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createIOLine - ERROR: Create IO Line :: "+desc +" - "+ e.getMessage(), e);
		}
		return line;
	}//createIOLine

	/**
	 * crea la factura de compra apartir de la entrega - create purchace invoice from io
	 * @param trxName
	 * @param io
	 * @param desc
	 * @param date
	 * @param priceListId
	 * @param documentType
	 * @param paymentTermId
	 * @param plate
	 * @param vehicleId
	 * @param workOrder
	 * @return
	 */
	public static MInvoice createInvoice (String trxName, MInOut io, String desc, 
			Date date, Integer priceListId, Integer documentType, Integer paymentTermId, String payRule, 
			String plate, Integer vehicleId, String workOrder, Boolean isSOTrx) {

		if (io==null){
			return null;
		}
		MInvoice invoice = null;
		try{
			invoice = new MInvoice(io, new Timestamp(date.getTime()));
			invoice.setPOReference("SmartPOS");
			invoice.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			invoice.setIsActive(true);
			invoice.setIsSOTrx(isSOTrx); 
			invoice.setDateInvoiced(new Timestamp(date.getTime()));
			invoice.setDateAcct(new Timestamp(date.getTime()));
			invoice.setC_BPartner_ID(io.getC_BPartner_ID());
			invoice.setC_BPartner_Location_ID(io.getC_BPartner_Location_ID());
			invoice.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			invoice.setM_PriceList_ID(priceListId);
			invoice.setSalesRep_ID(io.getSalesRep_ID());
			invoice.setDocStatus(DocAction.STATUS_Drafted);
			invoice.setDocAction(DocAction.ACTION_Complete);
			invoice.setIsApproved(true);
			invoice.setDateOrdered(new Timestamp(date.getTime()));
			invoice.setC_ConversionType_ID(114);
			invoice.setDescription(desc);
			invoice.setC_PaymentTerm_ID(paymentTermId);
			invoice.setProcessed(true);
			invoice.setPosted(false);
			invoice.setC_DocTypeTarget_ID(documentType);
			invoice.setC_DocType_ID(documentType);
			invoice.setPaymentRule(payRule);
			if(plate!=null){
				invoice.set_ValueOfColumn("smj_plate", plate);
			}
			if (vehicleId!= null && vehicleId > 0){
				invoice.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
			}
			if (workOrder!= null){
				invoice.set_ValueOfColumn("smj_workorder", workOrder);
			}
			Boolean ok = invoice.save();
			if (!ok){
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoice - ERROR: Create Invoice :: "+desc +" - "+ e.getMessage(), e);
		} 
		return invoice;
	}//createInvoice

	/**
	 * crea la linea de la factura - create invoice line
	 * @param data
	 * @param desc
	 * @param productId
	 * @param price
	 * @param taxId
	 * @param total
	 * @param qty
	 * @param attibuteId
	 * @return Integer
	 */
	public static Integer createInvoiceLine(MInvoice data, String desc, Integer productId, BigDecimal price, Integer taxId,
			BigDecimal total, BigDecimal qty, Integer uomId, Integer attibuteId, Integer orderLine, String plate
			, Integer sequence, Integer ioLine, Integer chargeId){
		Integer invLineId = 0; 
		MInvoiceLine line = null;
		BigDecimal divideRate = DataQueries.getUOMDivideRate(productId, uomId);

		try{
			line = new MInvoiceLine(data);
			line.setIsActive(true);
			line.setQtyEntered(new BigDecimal(1));
			line.setM_Product_ID(productId);
			line.setC_OrderLine_ID(orderLine);
			line.setPriceActual((price.signum() == -1) ? price : price.divide(divideRate, 2, RoundingMode.HALF_UP));
			line.setPriceList((price.signum() == -1) ? Env.ZERO : price.divide(divideRate, 2, RoundingMode.HALF_UP));
			line.setPriceEntered((price.signum() == -1) ? Env.ZERO : price);
			line.setPriceLimit((price.signum() == -1) ? Env.ZERO : price);
			line.setC_Tax_ID(taxId);
			line.setLineTotalAmt(total);
			line.setLineNetAmt(total);
			line.setDescription(desc);			
			line.setQty(qty);
			line.setQtyInvoiced(qty.multiply(divideRate));
			line.setC_UOM_ID(uomId);
			line.setC_Charge_ID(chargeId);

			if(plate!=null)
				line.set_ValueOfColumn("smj_plate", plate);			

			if(sequence!= null && sequence>0)
				line.setLine(sequence);			

			if (ioLine != null && ioLine > 0)
				line.setM_InOutLine_ID(ioLine);			

			Boolean flag = line.save();

			if (flag)
				invLineId = line.getC_InvoiceLine_ID();

		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoiceLine - ERROR: Create Invoice Line:: "+desc +" - "+ e.getMessage(), e);
		}
		return invLineId;
	}//createInvoiceLine

	/**
	 * crea el pago para la factura - 
	 * create payment by invoice.
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param bankAccountId
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param aName
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @param writeOffAmt
	 * @param discountAmt
	 * @return MPayment
	 */
	public static MPayment createPayment(String trxName, MInvoice inv, BigDecimal value
			, Integer bankAccountId, String tenderType
			,String CreditCardType, String CreditCardNumber, String newCreditCardVV
			,Integer CreditCardExpMM, Integer newCreditCardExpYY, String aName
			,String routingNo, String accountNo, String checkNo, String micr
			, BigDecimal writeOffAmt, BigDecimal discountAmt, Integer orgId, Boolean complete) throws Exception {


		if (useTransport) {
			return createPaymentTrans(trxName, inv, value, bankAccountId, tenderType, CreditCardType, CreditCardNumber, 
					newCreditCardVV, CreditCardExpMM, newCreditCardExpYY, aName, routingNo, accountNo, checkNo, micr, 
					writeOffAmt, discountAmt, orgId, complete);
		}		

		String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",lclientId,lorgId).trim();
		String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",lclientId,lorgId).trim();

		String whereClause = "AD_Org_ID=? AND C_Currency_ID=?";
		MBankAccount ba = new Query(inv.getCtx(),MBankAccount.Table_Name,whereClause, inv.get_TrxName())
				.setParameters(inv.getAD_Org_ID(), inv.getC_Currency_ID())
				.setOrderBy("IsDefault DESC")
				.first();
		if (ba == null) {
			throw new AdempiereException("@NoAccountOrgCurrency@");
		}

		String docBaseType = "";
		if (inv.isSOTrx())
			docBaseType=MDocType.DOCBASETYPE_ARReceipt;
		else
			docBaseType=MDocType.DOCBASETYPE_APPayment;

		MDocType[] doctypes = MDocType.getOfDocBaseType(inv.getCtx(), docBaseType);
		if (doctypes == null || doctypes.length == 0) {
			throw new AdempiereException("No document type ");
		}

		MDocType doctype = null;
		for (MDocType doc : doctypes) {
			if (doc.getAD_Org_ID() == inv.getAD_Org_ID()) {
				doctype = doc;
				break;
			}
		}
		if (doctype == null)
			doctype = doctypes[0];

		MPayment payment = new MPayment(inv.getCtx(), 0, inv.get_TrxName());
		payment.setAD_Org_ID(inv.getAD_Org_ID());
		payment.setTenderType(tenderType);
		payment.setC_BankAccount_ID(bankAccountId);
		payment.setC_BPartner_ID(inv.getC_BPartner_ID());
		payment.setC_Invoice_ID(inv.getC_Invoice_ID());
		payment.setC_Currency_ID(inv.getC_Currency_ID());			
		payment.setC_DocType_ID(doctype.getC_DocType_ID());
		payment.setIsPrepayment(false);					
		payment.setDateAcct(inv.getDateAcct());
		payment.setDiscountAmt(discountAmt);
		payment.setDateTrx(inv.getDateInvoiced());

		boolean useWH = Env.getCtx().getProperty("#LCO_USE_WITHHOLDINGS", "N").equals("Y");
		if (useWH) {
			String sql = "SELECT COALESCE (SUM (TaxAmt), 0) FROM LCO_InvoiceWithholding WHERE C_Invoice_ID = ? AND IsActive = 'Y' " +
					"AND IsCalcOnPayment = 'Y' AND Processed = 'N' AND C_AllocationLine_ID IS NULL";
			writeOffAmt = DB.getSQLValueBD(inv.get_TrxName(), sql, inv.getC_Invoice_ID());
		}

		payment.setWriteOffAmt(writeOffAmt);
		payment.setPayAmt(value.subtract(writeOffAmt));

		if (tenderType.equals(tenderCreditCard)){ //credit card
			payment.setCreditCardType(CreditCardType);
			payment.setCreditCardNumber(CreditCardNumber);
			payment.setCreditCardVV(newCreditCardVV);

			if (CreditCardExpMM != null && CreditCardExpMM > 0)
				payment.setCreditCardExpMM(CreditCardExpMM);

			if (newCreditCardExpYY != null && newCreditCardExpYY > 0)
				payment.setCreditCardExpYY(newCreditCardExpYY);

			payment.setA_Name(aName);
		}else if(tenderType.equals(tenderCheck)){ //cheque - check
			payment.setRoutingNo(routingNo);
			payment.setAccountNo(accountNo);
			payment.setCheckNo(checkNo);
			payment.setMicr(micr);
			payment.setA_Name(aName);
		}

		//	Save payment
		payment.saveEx();

		payment.setDocAction(MPayment.DOCACTION_Complete);
		if (!payment.processIt(MPayment.DOCACTION_Complete)) {
			throw new AdempiereException("Cannot Complete the Payment : [" + payment.getProcessMsg() + "] " + payment);
		}

		payment.saveEx();

		return payment;
	}//createPayment

	public static MPayment createPaymentTrans(String trxName, MInvoice inv, BigDecimal value
			, Integer bankAccountId, String tenderType
			,String CreditCardType, String CreditCardNumber, String newCreditCardVV
			,Integer CreditCardExpMM, Integer newCreditCardExpYY, String aName
			,String routingNo, String accountNo, String checkNo, String micr
			, BigDecimal writeOffAmt, BigDecimal discountAmt, Integer orgId, Boolean complete) throws Exception {
		Boolean flag = true;
		MPayment pay = null;
		String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",Env.getAD_Client_ID(Env.getCtx())).trim();
		String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",Env.getAD_Client_ID(Env.getCtx())).trim();
		try {
			pay = new MPayment(Env.getCtx(), 0, trxName);
			pay.setAD_Org_ID(orgId);
			pay.setIsReceipt(true);
			pay.setC_DocType_ID(true);
			pay.setDescription(inv.getDescription());
			pay.setTrxType("S");
			pay.setC_BankAccount_ID(bankAccountId);
			pay.setC_BPartner_ID(inv.getC_BPartner_ID());
			pay.setC_Invoice_ID(inv.getC_Invoice_ID());
			pay.setC_Currency_ID(inv.getC_Currency_ID());
			pay.setPayAmt(value);
			pay.setWriteOffAmt(writeOffAmt);
			pay.setDiscountAmt(discountAmt);
			pay.setPosted(false);
			pay.setTenderType(tenderType);
			pay.setDateTrx(inv.getDateInvoiced());
			pay.setDateAcct(inv.getDateInvoiced());
			//			pay.setIsAllocated(true);
			//			pay.setIsApproved(true);
			if (tenderType.equals(tenderCreditCard)){ //credit card
				pay.setCreditCardType(CreditCardType);
				pay.setCreditCardNumber(CreditCardNumber);
				pay.setCreditCardVV(newCreditCardVV);
				pay.setCreditCardExpMM(CreditCardExpMM);
				pay.setCreditCardExpYY(newCreditCardExpYY);
				pay.setA_Name(aName);
			}else if(tenderType.equals(tenderCheck)){ //cheque - check
				pay.setRoutingNo(routingNo);
				pay.setAccountNo(accountNo);
				pay.setCheckNo(checkNo);
				pay.setMicr(micr);
				pay.setA_Name(aName);
				//				pay.setDocumentNo(routingNo+": "+checkNo+" #"+micr);
			}
			Boolean ok = pay.save();
			if (ok){

				if (complete){
					pay.completeIt();
					pay.setDocStatus(DocAction.STATUS_Completed);
					pay.setDocAction(DocAction.STATUS_Closed);
					pay.setProcessed(true);
					flag = pay.save();
					if (!flag){
						return null;
					}
				}
				Integer orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
				if (orgId.equals(orgInfotrans)){
					return pay;
				}
				//Modificacion iMarch, organizacion de trasporte de carga
				String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), orgId).trim();
				if (orgfreighttrans.equals("true")){
					return pay;
				}
				//--------------
			}else{
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPayment - ERROR: Create Payment:: "+ e.getMessage(), e);
		}
		return pay;
	}

	/**
	 * crea un movimiento de materiales - 
	 * create a material movement
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param locationId
	 * @param movementDocId
	 * @param date
	 * @param salesRepId
	 * @return MMovement
	 */
	public static MMovement createMovement(String trxName, String desc, Integer partnerId, Integer locationId,
			Integer movementDocId, Date date, Integer salesRepId){
		MMovement mv =  null;
		try {
			mv =  new MMovement(Env.getCtx(), 0, trxName);
			mv.setDescription(desc);
			mv.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			mv.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			mv.setDocAction(DocAction.ACTION_Complete);
			mv.setC_DocType_ID(movementDocId); 
			mv.setMovementDate(new Timestamp(date.getTime()));
			mv.setDateReceived(new Timestamp(date.getTime()));
			mv.setSalesRep_ID(salesRepId);
			mv.setAD_User_ID(salesRepId);
			mv.setC_BPartner_ID(partnerId);
			mv.setC_BPartner_Location_ID(locationId);

			Boolean ok = mv.save();
			if (!ok){
				mv = null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createMovement - ERROR: Create Movement:: "+desc+" - " + e.getMessage(), e);
			return null;
		}
		return mv;
	}//createMovement

	/**
	 * crea la linea del movimiento a partir del movimiento - 
	 * create movement line from movement
	 * @param mv
	 * @param desc
	 * @param productId
	 * @param startLocatorId
	 * @param endLocatorId
	 * @param qty
	 * @return Boolean
	 */
	public static Boolean createMovementLine(MMovement mv, String desc, Integer productId, Integer startLocatorId,
			Integer endLocatorId, BigDecimal qty){
		Boolean flag = true;
		MMovementLine line = null;
		try {
			line = new MMovementLine(mv);
			line.setM_Product_ID(productId);
			line.setDescription(desc);
			line.setM_Locator_ID(startLocatorId);
			line.setM_LocatorTo_ID(endLocatorId);
			line.setMovementQty(qty);
			flag = line.save();

		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createMovementLine - Create Movement Line:: "+desc+" - " + e.getMessage(), e);
			flag = false;
		}
		return flag;
	}//createMovementLine

	/**
	 * crea el pago sin factura - 
	 * create payment without invoice.
	 * @param trxName
	 * @param description
	 * @param bpartnerId
	 * @param value
	 * @param bankAccountId
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param aName
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @param writeOffAmt
	 * @param discountAmt
	 * @return MPayment
	 */
	public static MPayment createPaymentNoinvoice(String trxName, String description, Integer bpartnerId
			, BigDecimal value , Integer bankAccountId, String tenderType
			,String CreditCardType, String CreditCardNumber, String newCreditCardVV
			,Integer CreditCardExpMM, Integer newCreditCardExpYY, String aName
			,String routingNo, String accountNo, String checkNo, String micr
			, BigDecimal writeOffAmt, BigDecimal discountAmt, Integer docTypeID, Boolean isPrepayment){
		MPayment pay = null;
		String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",lclientId,lorgId).trim();
		String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",lclientId,lorgId).trim();
		try {
			pay = new MPayment(Env.getCtx(), 0, trxName);
			pay.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			pay.setIsReceipt(true);
			pay.setC_DocType_ID(docTypeID);
			pay.setDescription(description);
			pay.setTrxType("S");
			pay.setC_BankAccount_ID(bankAccountId);
			pay.setC_BPartner_ID(bpartnerId);
			pay.setC_Invoice_ID(0);
			pay.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			pay.setPayAmt(value);
			pay.setWriteOffAmt(writeOffAmt);
			pay.setDiscountAmt(discountAmt);
			pay.setPosted(false);
			pay.setTenderType(tenderType);
			pay.setAccountNo(accountNo);  // solicitud 1000403
			pay.setDateTrx(Env.getContextAsDate(Env.getCtx(), "#Date"));
			pay.setDateAcct(Env.getContextAsDate(Env.getCtx(), "#Date"));
			pay.setIsPrepayment(isPrepayment);
			//			pay.setIsAllocated(true);
			//			pay.setIsApproved(true);
			if (tenderType.equals(tenderCreditCard)){ //credit card
				pay.setCreditCardType(CreditCardType);
				pay.setCreditCardNumber(CreditCardNumber);
				pay.setCreditCardVV(newCreditCardVV);
				pay.setCreditCardExpMM(CreditCardExpMM);
				pay.setCreditCardExpYY(newCreditCardExpYY);
				pay.setA_Name(aName);
			}else if(tenderType.equals(tenderCheck)){ //cheque - check
				pay.setRoutingNo(routingNo);
				pay.setAccountNo(accountNo);
				pay.setCheckNo(checkNo);
				pay.setMicr(micr);
				pay.setA_Name(aName);
				//				pay.setDocumentNo(routingNo+": "+checkNo+" #"+micr);
			}
			Boolean ok = pay.save();
			if (!ok){
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createPaymentNoinvoice - ERROR: Create Payment:: "+ e.getMessage(), e);
		}
		return pay;
	}//createPaymentNoinvoice

	/**
	 * crea la factura de compra apartir sin entrega - 
	 * create purchace invoice without io
	 * @param trxName
	 * @param desc
	 * @param partnerId
	 * @param date
	 * @param priceListId
	 * @param documentType
	 * @param paymentTermId
	 * @param plate
	 * @param vehicleId
	 * @param workOrder
	 * @param isSOTrx
	 * @param salesRepId
	 * @return MInvoice
	 */
	public static MInvoice createInvoiceNoIo (String trxName, String desc, Integer partnerId,
			Timestamp date, Integer priceListId, Integer documentType, Integer paymentTermId
			, String plate, Integer vehicleId, String workOrder, Boolean isSOTrx, Integer salesRepId,String sequence,boolean taxIncluded){

		MInvoice invoice = null;
		Integer locationId = DataQueries.getLocationPartner(partnerId);
		try{
			invoice = new MInvoice(Env.getCtx(), 0, trxName);
			// solicitud 1000387
			if (!sequence.equalsIgnoreCase("0"))   // es necesario asignar secuenca manual
			{
				invoice.setDocumentNo(sequence);
			}
			invoice.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			invoice.setIsActive(true);
			invoice.setIsSOTrx(isSOTrx); 
			invoice.setDateInvoiced(date);
			invoice.setDateAcct(date);
			invoice.setC_BPartner_ID(partnerId);
			invoice.setC_BPartner_Location_ID(locationId);
			invoice.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			invoice.setM_PriceList_ID(priceListId);
			invoice.setSalesRep_ID(salesRepId);
			invoice.setDocStatus(DocAction.STATUS_Drafted);
			invoice.setDocAction(DocAction.ACTION_Complete);
			invoice.setIsApproved(true);
			invoice.setDateOrdered(date);
			invoice.setC_ConversionType_ID(114);
			invoice.setDescription(desc);
			invoice.setC_PaymentTerm_ID(paymentTermId);
			//			invoice.setProcessed(true);
			invoice.setPosted(false);
			invoice.setC_DocTypeTarget_ID(documentType);
			invoice.setC_DocType_ID(documentType);
			invoice.setPaymentRule("P");
			invoice.setIsTaxIncluded(taxIncluded);
			if(plate!=null){
				invoice.set_ValueOfColumn("smj_plate", plate);
			}
			if (vehicleId!= null && vehicleId > 0){
				invoice.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
			}
			if (workOrder!= null){
				invoice.set_ValueOfColumn("smj_workorder", workOrder);
			}
			Boolean ok = invoice.save();
			if (!ok){
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoice - ERROR: Create Invoice :: "+desc +" - "+ e.getMessage(), e);
		} 
		return invoice;
	}//createInvoice

	/**
	 * 
	 * @param payPoints
	 * @param invoice
	 * @param C_BPartner_ID
	 * @param trxName
	 * @return
	 */
	public static boolean createPaymentForPoints(List<PaymentList> payPoints, MInvoice invoice, int C_BPartner_ID, String trxName) {
		int C_PaymentTerm_ID = MSysConfig.getIntValue("SMJ_Lty_PaymentTerm_Points", 0, lclientId, lorgId);

		MInvoice arCredit = new MInvoice(Env.getCtx(), 0, trxName);
		arCredit.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		arCredit.setC_DocTypeTarget_ID(MDocType.DOCBASETYPE_ARCreditMemo);
		arCredit.setDateInvoiced(Env.getContextAsDate(Env.getCtx(), "#Date"));
		arCredit.setDateAcct(Env.getContextAsDate(Env.getCtx(), "#Date"));
		arCredit.setBPartner(new MBPartner(Env.getCtx(), C_BPartner_ID, null));
		arCredit.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
		arCredit.setC_PaymentTerm_ID(C_PaymentTerm_ID);

		if (!arCredit.save()) 
			return false;


		String whereClause =  MTax.COLUMNNAME_IsDefault + " = 'Y' AND " + MTax.COLUMNNAME_Rate + " = 0";
		int C_Tax_ID = new Query(Env.getCtx(), MTax.Table_Name, whereClause, null).firstId();
		for (PaymentList payp: payPoints) {
			MInvoiceLine line = new MInvoiceLine(Env.getCtx(), 0, trxName);
			line.setC_Invoice_ID(arCredit.getC_Invoice_ID());
			line.setInvoice(arCredit);								
			line.setC_Charge_ID(MSysConfig.getIntValue("SMJ_Lty_SalesPoints_Charge_ID", 0, lclientId, lorgId));
			line.setQty(Env.ONE);
			line.setPrice(payp.getValue());
			line.setPriceList(payp.getValue());		
			line.setC_Tax_ID(C_Tax_ID);

			if (!line.save()) 
				return false;
		}

		arCredit.setDocStatus(arCredit.completeIt());

		if (arCredit.getDocStatus().equals(DocAction.STATUS_Completed)) {
			if (!arCredit.save()) 				
				return false;
		} else {
			return false;
		}

		MAllocationHdr allocation = new MAllocationHdr(Env.getCtx(), 0, trxName);
		allocation.setAD_Org_ID(arCredit.getAD_Org_ID());
		allocation.setDateTrx(Env.getContextAsDate(Env.getCtx(), "#Date"));
		allocation.setC_Currency_ID(arCredit.getC_Currency_ID());

		if (!allocation.save()) 
			return false;

		MAllocationLine lineCreditMemo = new MAllocationLine(allocation);
		lineCreditMemo.setC_BPartner_ID(C_BPartner_ID);
		lineCreditMemo.setC_Invoice_ID(arCredit.getC_Invoice_ID());
		lineCreditMemo.setAmount(arCredit.getTotalLines().negate());

		MAllocationLine lineInvoice = new MAllocationLine(allocation);
		lineInvoice.setC_BPartner_ID(C_BPartner_ID);
		lineInvoice.setC_Invoice_ID(invoice.getC_Invoice_ID());
		lineInvoice.setC_Order_ID(invoice.getC_Order_ID());
		lineInvoice.setAmount(arCredit.getTotalLines());

		if (!lineCreditMemo.save() || !lineInvoice.save()) {
			return false;
		}

		allocation.setDocStatus(allocation.completeIt());

		if (allocation.getDocStatus().equals(DocAction.STATUS_Completed)) {
			if (!allocation.save())
				return false;
		} else {
			return false;
		}

		return true;
	}

	/**
	 * crea la factura de compra apartir de la entrega - create purchace invoice from io
	 * @param trxName
	 * @param io
	 * @param desc
	 * @param date
	 * @param priceListId
	 * @param documentType
	 * @param paymentTermId
	 * @param plate
	 * @param vehicleId
	 * @param workOrder
	 * @return
	 */
	public static MInvoice createInvoiceTrans (String trxName, MInOut io, String desc, 
			Date date, Integer priceListId, Integer documentType, Integer paymentTermId
			, String plate, Integer vehicleId, String workOrder, String payRule, Boolean isSOTrx) throws Exception {

		if (io==null){
			return null;
		}

		MInvoice invoice = new MInvoice(io, new Timestamp(date.getTime()));
		invoice.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		invoice.setIsActive(true);
		invoice.setIsSOTrx(isSOTrx); 
		invoice.setDateInvoiced(new Timestamp(date.getTime()));
		invoice.setDateAcct(new Timestamp(date.getTime()));
		invoice.setC_BPartner_ID(io.getC_BPartner_ID());
		invoice.setC_BPartner_Location_ID(io.getC_BPartner_Location_ID());
		invoice.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
		invoice.setM_PriceList_ID(priceListId);
		invoice.setSalesRep_ID(io.getSalesRep_ID());
		invoice.setDocStatus(DocAction.STATUS_Drafted);
		invoice.setDocAction(DocAction.ACTION_Complete);
		invoice.setIsApproved(true);
		invoice.setDateOrdered(new Timestamp(date.getTime()));
		invoice.setC_ConversionType_ID(114);
		invoice.setDescription(desc);
		invoice.setC_PaymentTerm_ID(paymentTermId);
		invoice.setProcessed(true);
		invoice.setPosted(false);
		invoice.setC_DocTypeTarget_ID(documentType);
		invoice.setC_DocType_ID(documentType);
		invoice.setPaymentRule(MInvoice.PAYMENTRULE_OnCredit);
		invoice.setIsTaxIncluded(true);

		if(plate!=null){
			invoice.set_ValueOfColumn("smj_plate", plate);
		}

		if (vehicleId!= null && vehicleId > 0){
			invoice.set_ValueOfColumn("smj_vehicle_ID", vehicleId);
		}

		if (workOrder!= null && !workOrder.isEmpty()){				
			invoice.set_ValueOfColumn("smj_workorder", workOrder);
		}

		invoice.saveEx();

		return invoice;
	}//createInvoice

	/**
	 * crea las lineas de la orden - 
	 * create line order
	 * @param order
	 * @param productId
	 * @param qty
	 * @param price
	 * @param total
	 * @param description
	 * @return Boolean
	 */
	public static Integer createOrderLineTrans(MOrder order, Integer productId, BigDecimal qty,
			BigDecimal price, String description, Integer attInstanceId, Integer locatorId,
			Integer bPartnerId, Boolean isSystem, Integer provider_ID, Date orderDate
			, String plate, Integer workOrderLineId, Integer sequence) {
		Boolean flag = false;
		Integer code = 0;
		Integer uomId = DataQueries.getProductUom(productId);
		Integer taxId = DataQueries.getProductTax(productId);
		Integer bPlocation = DataQueries.getLocationPartner(bPartnerId);
		try {
			MOrderLine line = new MOrderLine(order);
			line.setC_Order_ID(order.getC_Order_ID());
			line.setIsActive(true);
			line.setDescription(description);
			line.setC_BPartner_ID(bPartnerId);
			line.setC_BPartner_Location_ID(bPlocation);
			line.setM_Warehouse_ID(order.getM_Warehouse_ID());
			line.setC_Currency_ID(order.getC_Currency_ID());
			line.setDateOrdered(new Timestamp(orderDate.getTime()));
			line.setDatePromised(new Timestamp(orderDate.getTime()));
			line.setM_Product_ID(productId);
			line.setC_UOM_ID(uomId);
			line.setDiscount(Env.ZERO);
			line.setFreightAmt(Env.ZERO); 
			line.setC_Tax_ID(taxId);
			line.setIsDescription(false);
			line.setQtyEntered(qty);
			line.setQtyOrdered(qty);
			line.setQtyDelivered(Env.ZERO);
			line.setQtyInvoiced(Env.ZERO);
			line.setQtyReserved(Env.ZERO);
			line.setRRAmt(Env.ZERO);
			line.setPriceLimit(Env.ZERO);
			line.setPriceList(price);
			line.setPriceActual(price);
			line.setPriceEntered(price);
			line.setPrice(price);
			line.setPriceCost(Env.ZERO);
			line.setProcessed(false);
			line.setQtyLostSales(Env.ZERO);
			//			if (attInstanceId >0){
			//				line.setM_AttributeSetInstance_ID(attInstanceId);
			//			}
			//			line.setM_AttributeSetInstance_ID(0);
			if(locatorId >0 ){
				line.set_ValueOfColumn("smj_locator_ID", locatorId);
			}
			if (provider_ID!= null && provider_ID > 0){
				line.set_ValueOfColumn("smj_Provider_ID", provider_ID);
			}
			if(plate!=null){
				line.set_ValueOfColumn("smj_plate", plate);
			}
			if(workOrderLineId!=null){
				line.set_ValueOfColumn("smj_workOrderLine_ID", workOrderLineId);
			}
			line.set_ValueOfColumn("smj_isSystem", isSystem);
			if(sequence != null && sequence >0){
				line.setLine(sequence);
			}
			flag = line.save();
			line.setPriceList(price);
			line.setPriceActual(price);
			line.setPriceEntered(price);
			line.setPrice(price);
			flag = line.save();
			if (flag){
				code = line.getC_OrderLine_ID();
			}
		} catch (Exception e) {
			flag = false;
			description = description+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createOrderLine - ERROR: Create OrderLine :: "+description +" - "+ e.getMessage(), e);
		} 
		return code;
	}// createOrderLineTrans

	/**
	 * crea la linea de la entrega - 
	 * create in/out line
	 * @param io
	 * @param desc
	 * @param productId
	 * @param locatorId
	 * @param qty
	 * @param instanceId
	 * @return
	 */
	public static MInOutLine createIOLineTrans(MInOut io, String desc, Integer productId, Integer locatorId, 
			BigDecimal qty, Integer orderLine, Integer sequence){
		MInOutLine line = null;
		Boolean flag = true;
		Integer uomId = DataQueries.getProductUom(productId);
		try {
			line = new MInOutLine(io);
			line.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			line.setM_InOut_ID(io.getM_InOut_ID());
			line.setC_OrderLine_ID(orderLine);
			line.setM_Product_ID(productId);
			line.setM_Locator_ID(locatorId);// 1000000
			line.setC_UOM_ID(uomId);
			line.setQtyEntered(qty);
			line.setMovementQty(qty);
			
			if (sequence != null && sequence >0){ 
				line.setLine(sequence);
			}
			
			line.setDescription(desc);
			flag = line.save();
			
			if (!flag){
				return null;
			}
			
		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createIOLine - ERROR: Create IO Line :: "+desc +" - "+ e.getMessage(), e);
		}
		
		return line;
	}//createIOLineTrans
	
	public static MInOutLine newIoLineFromAnotherIoLine(Properties ctx, MInOut inout, MInOutLine ioLine, int C_OrderLine_ID, 
			int M_Locator_ID, String description, Integer sequence) throws AdempiereException {	//Adicion iMarch Integer sequence
		
		MInOutLine newIoLine = new MInOutLine(inout);
		newIoLine.setAD_Org_ID(Env.getAD_Org_ID(ctx));
		newIoLine.setM_Product_ID(ioLine.getM_Product_ID());
		newIoLine.setM_AttributeSetInstance_ID(ioLine.getM_AttributeSetInstance_ID());
		newIoLine.setQty(ioLine.getQtyEntered().negate());
		newIoLine.setM_Locator_ID(M_Locator_ID);
		newIoLine.setC_OrderLine_ID(C_OrderLine_ID);
		newIoLine.setDescription(description);
		//Adicion iMarch---
		if (sequence != null && sequence >0){
			Integer seq = new Integer(-1);
			sequence = sequence * seq;
			newIoLine.setLine(sequence);
		}
		//fin adicion iMarch
		newIoLine.saveEx();
		
		return newIoLine;
	}

	/**
	 * crea la linea de la factura - create invoice line
	 * @param data
	 * @param desc
	 * @param productId
	 * @param price
	 * @param taxId
	 * @param total
	 * @param qty
	 * @param attibuteId
	 * @return Integer
	 */
	public static Integer createInvoiceLineTrans(MInvoice data, String desc, Integer productId, BigDecimal price, Integer taxId,
			BigDecimal total, BigDecimal qty, Integer attibuteId, Integer orderLine, String plate
			, Integer sequence, Integer ioLine, Integer chargeId){
		Integer invLineId = 0; 
		MInvoiceLine line = null;
		Integer uomId = DataQueries.getProductUom(productId);
		try{
			line = new MInvoiceLine(data);
			line.setIsActive(true);
			line.setQtyEntered(new BigDecimal(1));
			line.setM_Product_ID(productId);
			line.setC_OrderLine_ID(orderLine);
			line.setPrice(price);
			line.setPriceActual(price);
			line.setPriceList(price);
			line.setC_Tax_ID(taxId);
			line.setLineTotalAmt(total);
			line.setLineNetAmt(total);
			line.setDescription(desc);
			line.setPriceEntered(price);
			line.setPriceLimit(price);
			line.setQty(qty);
			line.setQtyInvoiced(qty);
			line.setC_UOM_ID(uomId);
			line.setC_Charge_ID(chargeId);
			//			if (attibuteId != null && attibuteId >0){
			//				line.setM_AttributeSetInstance_ID(attibuteId);
			//			}
			//			line.setM_AttributeSetInstance_ID(0);
			if(plate!=null){
				line.set_ValueOfColumn("smj_plate", plate);
			}
			if(sequence!= null && sequence>0){
				line.setLine(sequence);
			}
			if (ioLine != null && ioLine>0){
				line.setM_InOutLine_ID(ioLine);
			}
			Boolean flag = line.save();
			if (flag){
				invLineId = line.getC_InvoiceLine_ID();
			}
		} catch (Exception e) {
			desc = desc+ " M_Product_ID: "+productId+" ";
			log.log(Level.SEVERE,Documents.class.getName() + 
					".createInvoiceLine - ERROR: Create Invoice Line:: "+desc +" - "+ e.getMessage(), e);
		}
		return invLineId;
	}//createInvoiceLineTrans

	/**
	 * Create Purchase Order and Material Receipt
	 * @throws Exception
	 */
	public static MInOut createConsignmentDocuments(int C_BPartner_ID, int M_PriceList_Version_ID, int M_Warehouse_ID,
			String description, Boolean isPositive, String trxName) throws Exception {

		int C_DocType_ID;
		int M_PriceList_ID = new MPriceListVersion(Env.getCtx(), M_PriceList_Version_ID, trxName).getM_PriceList_ID();
		Timestamp date = Env.getContextAsDate(Env.getCtx(), "#Date");

		if (isPositive) {
			C_DocType_ID= MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_Order_ID");			
		} else {
			C_DocType_ID= MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_ConsignOrder_ID");
		}

		MOrder order = new MOrder(Env.getCtx(), 0, trxName);
		order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		order.setPOReference("CONSIGNACION");
		order.setBPartner(MBPartner.get(Env.getCtx(), C_BPartner_ID));
		order.setDateOrdered(date);
		order.setDateAcct(date);
		order.setDatePromised(date);
		order.setM_PriceList_ID(M_PriceList_ID);
		order.setM_Warehouse_ID(M_Warehouse_ID);
		order.setDescription(description);
		order.setC_DocTypeTarget_ID(C_DocType_ID);
		order.setC_DocType_ID(C_DocType_ID);
		order.setIsSOTrx(false);

		order.saveEx();

		// Create MInOut (Consignment)
		int C_DocTypeShipment_ID;

		if (isPositive) {
			C_DocTypeShipment_ID = MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_Receipt_ID");			
		} else {
			C_DocTypeShipment_ID = MOrg.get(Env.getCtx(), Env.getAD_Org_ID(Env.getCtx())).get_ValueAsInt("SMJ_DocType_ConsignReceipt_ID");
		}

		MInOut inOut = new MInOut(order, C_DocTypeShipment_ID, order.getDateOrdered());
		inOut.saveEx();

		return inOut;
	}

	/**
	 * Create Order Line (Consignment)
	 * @param order
	 * @param M_Product_ID
	 * @param M_PriceList_Version_ID
	 * @param qty
	 * @param trxName
	 * @throws Exception
	 */
	public static void createConsignmentLinesPositive(MInOut inOut, int M_Product_ID, int M_PriceList_Version_ID, 
			BigDecimal qty, int M_AttributeSetInstance_ID, String trxName) throws Exception {

		MOrder order = new MOrder(Env.getCtx(), inOut.getC_Order_ID(), trxName);

		MProductPrice pp = MProductPrice.get(Env.getCtx(), M_PriceList_Version_ID, M_Product_ID, trxName);

		MOrderLine oLine = new MOrderLine(order);
		oLine.setProduct(MProduct.get(Env.getCtx(), M_Product_ID));
		oLine.setPrice(pp.getPriceList());
		oLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
		oLine.setQty(qty);

		oLine.saveEx();

		createMInOutLineConsignmentPositive(inOut, oLine, M_AttributeSetInstance_ID);
	}
	
	/**
	 * Create Order Line (Consignment)
	 * @param order
	 * @param M_Product_ID
	 * @param M_PriceList_Version_ID
	 * @param qty
	 * @param trxName
	 * @throws Exception
	 */
	public static  Map<Integer, BigDecimal> createConsignmentLinesNegative(MInOut inOutNegative, MInOut inOutPositive, int M_Product_ID, int M_PriceList_Version_ID, 
			BigDecimal qty, Map<Integer, BigDecimal> mapAttributeQty, Consumer<MInOutLine> createShipmentLine, String trxName) throws Exception {

		MOrder order = new MOrder(Env.getCtx(), inOutNegative.getC_Order_ID(), trxName);

		MProductPrice pp = MProductPrice.get(Env.getCtx(), M_PriceList_Version_ID, M_Product_ID, trxName);

		MOrderLine oLine = new MOrderLine(order);
		oLine.setProduct(MProduct.get(Env.getCtx(), M_Product_ID));
		oLine.setPrice(pp.getPriceList());
		oLine.setQty(qty);

		oLine.saveEx();

		return createMInOutLineConsignmentNegative(inOutNegative, inOutPositive, oLine, mapAttributeQty, M_PriceList_Version_ID, createShipmentLine);
	}

	/**
	 * Create InOut Line (Consignment)
	 * @param inOutNegative
	 * @param oLine
	 * @throws Exception 
	 */
	public static Map<Integer, BigDecimal> createMInOutLineConsignmentNegative(MInOut inOutNegative, MInOut inOutPositive, MOrderLine oLine, Map<Integer, BigDecimal> mapAttributeQty, 
			int M_PriceList_Version_ID, Consumer<MInOutLine> createShipmentLine) throws Exception {
		
		MInOutLine inOutLine = new MInOutLine(inOutNegative);
		inOutLine.setOrderLine(oLine, 0, oLine.getQtyOrdered());

		//Get Attribute Set Instance, only when the quantity is negative
		//This is because you need to subtract the quantity at provider location

		String whereClause = "M_Product_ID = ? AND M_Locator_ID = ? AND QtyOnHand > 0";
		Query query = new Query(Env.getCtx(), MStorageOnHand.Table_Name, whereClause, oLine.get_TrxName());
		query.setParameters(inOutLine.getM_Product_ID(), inOutLine.getM_Locator_ID());
		query.setOrderBy("M_StorageOnHand.DateMaterialPolicy");
		List<MStorageOnHand> storages = query.list();

		BigDecimal positiveQty = oLine.getQtyOrdered().negate();
		BigDecimal usedQty = Env.ZERO;
		boolean first = true;
		
		if (mapAttributeQty == null) {
			mapAttributeQty = new HashMap<Integer, BigDecimal>();
		}

		for (MStorageOnHand storage: storages) {
			BigDecimal reqQty = positiveQty.subtract(usedQty);				
			BigDecimal attributeQty = Env.ZERO;
			
			if (reqQty.signum() != 1) {
				break;
			}
			
			//Validates if the attribute was used in other line ((true) = get qty used)
			if (mapAttributeQty.containsKey(storage.getM_AttributeSetInstance_ID())) {
				attributeQty = mapAttributeQty.get(storage.getM_AttributeSetInstance_ID());
			}
			
			BigDecimal qtyOnHand = storage.getQtyOnHand().subtract(attributeQty); //subtract previous qty (Other Lines)
			
			if (qtyOnHand.signum() == 0) {
				continue;
			}

			BigDecimal qtyToUse;

			if (qtyOnHand.compareTo(reqQty) >= 0) {
				qtyToUse = reqQty;
			} else {
				qtyToUse = qtyOnHand;
			}

			if (mapAttributeQty.containsKey(storage.getM_AttributeSetInstance_ID())) {
				mapAttributeQty.remove(storage.getM_AttributeSetInstance_ID());
			}
			
			//Registers the quantity used for the attribute in this line
			mapAttributeQty.put(storage.getM_AttributeSetInstance_ID(), qtyToUse.add(attributeQty));
			
			// First line, not need create a new line
			if (first) {
				inOutLine.setQty(qtyToUse.negate());
				inOutLine.setM_AttributeSetInstance_ID(storage.getM_AttributeSetInstance_ID());
				inOutLine.saveEx();

				if (createShipmentLine != null) {
					createShipmentLine.accept(inOutLine);
				}
				
				first = false;
			} else {
				MInOutLine ioLine = new MInOutLine(inOutNegative);
				ioLine.setOrderLine(oLine, inOutLine.getM_Locator_ID(), qtyToUse);
				ioLine.setQty(qtyToUse.negate());
				ioLine.setM_AttributeSetInstance_ID(storage.getM_AttributeSetInstance_ID());
				ioLine.saveEx();
				
				if (createShipmentLine != null) {
					createShipmentLine.accept(ioLine);
				}
			}

			// Create the positive line, it's necessary for keep the same Attribute
			createConsignmentLinesPositive(inOutPositive, oLine.getM_Product_ID(), M_PriceList_Version_ID, qtyToUse, storage.getM_AttributeSetInstance_ID(), oLine.get_TrxName());
			
			usedQty = usedQty.add(qtyToUse);
		}
		
		
		return  mapAttributeQty;
	}
	
	/**
	 * Create InOut Line (Consignment)
	 * @param inOut
	 * @param oLine
	 */
	public static void createMInOutLineConsignmentPositive(MInOut inOut, MOrderLine oLine, int M_AttributeSetInstance_ID) {
		MInOutLine inOutLine = new MInOutLine(inOut);
		inOutLine.setOrderLine(oLine, 0, oLine.getQtyOrdered());
		inOutLine.setQty(oLine.getQtyOrdered());
		inOutLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
		
		inOutLine.saveEx();
	}
}//Documents