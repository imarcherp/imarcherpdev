package com.smj.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.form.Allocation;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPayment;
import org.compiere.model.MProduct;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJMechanicCommission;
import com.smj.model.MSMJTmpWebSales;
import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;
import com.smj.reactive.Consumer;

public class CompleteSaleWeb {

	/** Logger */
	public static CLogger			log					= CLogger.getCLogger(Allocation.class);
	private static Integer			lclientId			= Env.getAD_Client_ID(Env.getCtx());
	private static Integer			lorgId				= Env.getAD_Org_ID(Env.getCtx());
	private final static String		tenderCheck			= MSysConfig.getValue("SMJ-TENDERTYPECHECK", lclientId, lorgId).trim();
	private final static String		tenderCreditCard	= MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD", lclientId, lorgId).trim();

	// Transport
	private static Boolean			useTransport		= MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, lclientId, lorgId);
	private final static Integer	orgInfotrans		= MSysConfig.getIntValue("SMJ_ORGINFOTRANS", -1, Env.getAD_Client_ID(Env.getCtx()));
	private static Integer			principalWarehouse	= MSysConfig.getIntValue("SMJ-PRINCIPALWAREHOUSE", -1, Env.getAD_Client_ID(Env.getCtx()));
	private static Integer			purchaseId			= MSysConfig.getIntValue("SMJ_PURCHASELIST", -1, Env.getAD_Client_ID(Env.getCtx()));
	private final static Integer	stateInvoiced		= MSysConfig.getIntValue("SMJ-STATEWOINVOICED", -1, Env.getAD_Client_ID(Env.getCtx()));

	//Adicion iMarch / Organizacion Transportadora
	private static String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
	
	/**
	 * crea los documentos de factura, entrega y movimiento - create documents invoice, in/out and movement
	 * 
	 * @param trxName
	 * @param sales
	 * @param order
	 * @return
	 */
	public static MInvoice completeDocumentsSales(String trxName, Boolean sales, MOrder order, boolean generatedOnlyShipment, boolean hasTransportRequest)
			throws Exception {
		if (useTransport) {
			return completeDocumentsSalesTrans(trxName, sales, order, generatedOnlyShipment, hasTransportRequest);
		}

		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		String description = "";
		MInvoice inv = null;
		try {
			String desc = order.getDescription();
			Integer payTermId = order.getC_PaymentTerm_ID();
			String docsh = MSysConfig.getValue("SMJ-DOCUMENTMMSHIPMENT", lclientId, lorgId).trim();
			Integer documentMM = DataQueries.getDocumentTypeId(docsh, lclientId);
			description = "Create In/Out";
			MInOut io = Documents.createMInOut(trxName, desc, order, order.getDateOrdered(), documentMM, sales);
			MOrderLine[] orderLines = order.getLines();
			if (orderLines == null || orderLines.length <= 0) {
				log.log(Level.SEVERE,
						CompleteSaleWeb.class.getCanonicalName() + ".completeDocumentSales - Error Create Order Lines Pruchase Automatic :: " + description);
				return null;
			}
			description = "Create Invoice";
			int clientCode = Env.getAD_Client_ID(Env.getCtx());
			docsh = MSysConfig.getValue("SMJ-DOCUMENTARINVOICE", clientCode, orgId).trim(); // obtiene el tipo por
																							// organizacion
			MWarehouse warehouse = new MWarehouse(Env.getCtx(), order.getM_Warehouse_ID(), null);
			int c_doctype_id = warehouse.get_ValueAsInt("C_DocTypeTarget_ID");

			if (c_doctype_id < 1)
				c_doctype_id = DataQueries.getDocumentTypeId(docsh, lclientId);

			Integer salesId = order.getM_PriceList_ID(); // toma la lista de precios que viene en la orden

			String plate = "";
			Integer vehicleId = 0;
			String workOrder = "";

			if (!generatedOnlyShipment) {
				inv = Documents.createInvoice(trxName, io, desc, order.getDateOrdered(), salesId, c_doctype_id, payTermId, order.getPaymentRule(), plate,
						vehicleId, workOrder, io.isSOTrx());
			}

			Map<Integer, BigDecimal> usedLocators = new HashMap<Integer, BigDecimal>();
			Map<Integer, BigDecimal> usedProducts = new HashMap<Integer, BigDecimal>();

			if (io != null && orderLines != null && orderLines.length > 0) {
				for (MOrderLine oLine : orderLines) {
					// logica de crear orden de compra

					description = "Create In/Out Line";
					String descLine = oLine.getDescription();
					MProduct product = oLine.getProduct();
					boolean hasAttribute = true;
					List<Map<String, Object>> locators = new ArrayList<Map<String, Object>>();

					if (product != null && !product.getProductType().equals(MProduct.PRODUCTTYPE_Service)) {
						hasAttribute = product.getM_AttributeSet_ID() > 0;
						locators = DataQueries.getLocatorsWithStock(oLine, (hasAttribute) ? usedLocators : usedProducts);
					}

					BigDecimal qtySent = Env.ZERO;
					BigDecimal divideRate = Env.ZERO;
					if (oLine.getQtyEntered().intValue() != 0) {
						divideRate = oLine.getQtyOrdered().divide(oLine.getQtyEntered());
					}

					for (Map<String, Object> locator : locators) {

						Integer m_locator_id = (Integer) locator.get("m_locator_id");
						Integer m_attributesetinstance_id = (Integer) locator.get("m_attributesetinstance_id");
						BigDecimal qtyOnHand = (BigDecimal) locator.get("qtyonhand");
						BigDecimal qtyTmp = oLine.getQtyOrdered().subtract(qtySent);
						BigDecimal qtyToSend;

						if (qtyTmp.compareTo(qtyOnHand) <= 0) {
							qtyToSend = qtyTmp.divide(divideRate, RoundingMode.DOWN);
						} else {
							qtyToSend = new BigDecimal(qtyOnHand.divide(divideRate, RoundingMode.DOWN).toBigInteger());
						}

						qtySent = qtySent.add(qtyToSend.multiply(divideRate));

						MInOutLine ioLine = Documents.createIOLine(io, descLine, oLine.getM_Product_ID(), m_locator_id, qtyToSend, oLine.getC_UOM_ID(),
								oLine.getC_OrderLine_ID(), 0, m_attributesetinstance_id);

						if (ioLine == null) {
							return null;
						}

						if (hasAttribute && usedLocators != null) {
							if (usedLocators.containsKey(m_attributesetinstance_id)) {
								BigDecimal oldQtyUsed = usedLocators.get(m_attributesetinstance_id);
								usedLocators.remove(m_attributesetinstance_id);
								usedLocators.put(m_attributesetinstance_id, oldQtyUsed.add(qtyToSend.multiply(divideRate)));
							} else {
								usedLocators.put(m_attributesetinstance_id, qtyToSend.multiply(divideRate));
							}
						} else if (usedProducts != null) {
							if (usedProducts.containsKey(product.getM_Product_ID())) {
								BigDecimal oldQtyUsed = usedProducts.get(product.getM_Product_ID());
								usedProducts.remove(product.getM_Product_ID());
								usedProducts.put(product.getM_Product_ID(), oldQtyUsed.add(qtyToSend.multiply(divideRate)));
							} else {
								usedProducts.put(product.getM_Product_ID(), qtyToSend.multiply(divideRate));
							}
						}
					}

					description = "Create Invoice Line";
					String plateLine = ""; // oLine.get_ValueAsString("smj_plate");
					// se elimino por problema de inventarios oLine.getM_AttributeSetInstance_ID()
					if (!generatedOnlyShipment) { // no ejecuta si es solo entrega
						BigDecimal price = (oLine.getPriceEntered().signum() == 0 && oLine.getPriceActual().signum() == -1) ? oLine.getPriceActual()
								: oLine.getPriceEntered();
						Integer invLineId = Documents.createInvoiceLine(inv, descLine, oLine.getM_Product_ID(), price, oLine.getC_Tax_ID(),
								oLine.getLineNetAmt(), oLine.getQtyEntered(), oLine.getC_UOM_ID(), 0, oLine.getC_OrderLine_ID(), plateLine, oLine.getLine(), 0,
								oLine.getC_Charge_ID());

						if (invLineId <= 0)
							return null;
					} // if (!generatedOnlyShipment)

				} // while ito
				Boolean mvok = true;
				description = "Complete In/Out";
				io.completeIt();
				io.setDocStatus(DocAction.STATUS_Completed);
				io.setDocAction(DocAction.STATUS_Closed);
				io.setProcessed(true);
				Boolean iook = io.save();
				description = "Complete Invoice";

				if (!generatedOnlyShipment) { // no ejecuta si es solo entrega

					if (!inv.completeIt().equals(DocAction.STATUS_Completed)) {
						StringBuilder msglog = new StringBuilder("Invoice Completed Failed: ").append(inv).append(" - ").append(inv.getProcessMsg());
						log.warning(msglog.toString());
						throw new IllegalStateException(msglog.toString());
					}

					inv.setDocStatus(DocAction.STATUS_Completed);
					inv.setDocAction(DocAction.STATUS_Closed);
					inv.setProcessed(true);
					Boolean inok = inv.save();
					if (!iook || !inok || !mvok) {
						return null;
					}
				} // (!generatedOnlyShipment)
			} else {
				return null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".completeDocumentSales - Error Create Documents Sales: " + description);
			throw e;
		}

		return inv;
	}// completeDocumentsSales

	/**
	 * crea el pago correspondiente - create payment Internal
	 * 
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param A_Name
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @return Integer
	 */
	public static Integer createPaymentInternal(String trxName, MInvoice inv, BigDecimal value, String tenderType, String CreditCardType,
			String CreditCardNumber, String newCreditCardVV, Integer CreditCardExpMM, Integer newCreditCardExpYY, String A_Name, String routingNo,
			String accountNo, String checkNo, String micr, Integer payTermId, BigDecimal writeOffAmt, BigDecimal discountAmt) throws Exception {
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		Integer paymentId = -1;
		Integer bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-ACCOUNTDEFAULT", Env.getAD_Client_ID(Env.getCtx()), orgId).trim());
		Integer payInternal = Integer.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMINTERNAL", Env.getAD_Client_ID(Env.getCtx())).trim());
		if (payTermId.equals(payInternal)) {
			bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-ACCOUNTINTERNAL", Env.getAD_Client_ID(Env.getCtx())).trim());
		}
		if (orgId.equals(orgInfotrans)) {
			bankAccountId = Integer.parseInt(MSysConfig.getValue("SMJ-INFBANKACCOUNT", Env.getAD_Client_ID(Env.getCtx())).trim());
		}
		MPayment pay = null;
		if (tenderType.equals(tenderCreditCard)) {

			pay = Documents.createPayment(trxName, inv, value, bankAccountId, tenderType, CreditCardType, CreditCardNumber, newCreditCardVV, CreditCardExpMM,
					newCreditCardExpYY, A_Name, null, null, null, null, writeOffAmt, discountAmt, orgId, true);
			paymentId = pay.getC_Payment_ID();
		} else if (tenderType.equals(tenderCheck)) {
			pay = Documents.createPayment(trxName, inv, value, bankAccountId, tenderType, null, null, null, 0, 0, A_Name, routingNo, accountNo, checkNo, micr,
					writeOffAmt, discountAmt, orgId, true);
			paymentId = pay.getC_Payment_ID();
		} else {
			pay = Documents.createPayment(trxName, inv, value, bankAccountId, tenderType, null, null, null, 0, 0, null, null, null, null, null, writeOffAmt,
					discountAmt, orgId, true);
			paymentId = pay.getC_Payment_ID();

		}
		if (pay == null || paymentId <= 0) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".createPayment - Error No Create Payment :: Invoice:" + inv.getC_Invoice_ID()
					+ " - " + inv.getDescription());
			return -1;
		}
		return paymentId;
	}// createPayment

	/**
	 * crea el pago correspondiente - create payment
	 * 
	 * @param trxName
	 * @param inv
	 * @param value
	 * @param tenderType
	 * @param CreditCardType
	 * @param CreditCardNumber
	 * @param newCreditCardVV
	 * @param CreditCardExpMM
	 * @param newCreditCardExpYY
	 * @param A_Name
	 * @param routingNo
	 * @param accountNo
	 * @param checkNo
	 * @param micr
	 * @return Integer
	 */
	public static Integer createPayment(String trxName, MInvoice inv, int M_Warehouse_ID, BigDecimal value, String tenderType, String CreditCardType,
			String CreditCardNumber, String newCreditCardVV, Integer CreditCardExpMM, Integer newCreditCardExpYY, String A_Name, String routingNo,
			String accountNo, String checkNo, String micr, Integer payTermId, BigDecimal writeOffAmt, BigDecimal discountAmt) throws Exception {

		Integer paymentId = -1;
		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		int bankAccount = warehouse.get_ValueAsInt("C_BankAccount_ID");

		MPayment pay = null;
		if (tenderType.equals(tenderCreditCard)) {

			pay = Documents.createPayment(trxName, inv, value, bankAccount, tenderType, CreditCardType, CreditCardNumber, newCreditCardVV, CreditCardExpMM,
					newCreditCardExpYY, A_Name, null, null, null, null, writeOffAmt, discountAmt, lorgId, true);
			paymentId = pay.getC_Payment_ID();
		} else if (tenderType.equals(tenderCheck)) {
			pay = Documents.createPayment(trxName, inv, value, bankAccount, tenderType, null, null, null, 0, 0, A_Name, routingNo, accountNo, checkNo, micr,
					writeOffAmt, discountAmt, lorgId, true);
			paymentId = pay.getC_Payment_ID();
		} else {
			pay = Documents.createPayment(trxName, inv, value, bankAccount, tenderType, null, null, null, 0, 0, null, null, null, null, null, writeOffAmt,
					discountAmt, lorgId, true);
			paymentId = pay.getC_Payment_ID();
		}

		if (pay == null || paymentId <= 0) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".createPayment - Error No Create Payment :: Invoice:" + inv.getC_Invoice_ID()
					+ " - " + inv.getDescription());
			return -1;
		}

		return paymentId;
	}// createPayment

	// ==================== TRANSPORT ====================

	private static MInvoice completeDocumentsSalesTrans(String trxName, Boolean sales, MOrder order, boolean generatedOnlyShipment, boolean hasTransportRequest)
			throws Exception {
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		String description = "";
		String descPurchaseOrder = MSysConfig.getValue("SMJ-MSGDEFAULTDESCORDER", Env.getAD_Client_ID(Env.getCtx())).trim();
		Integer docPurchaseOrder = Integer.parseInt(MSysConfig.getValue("SMJ-DOCPURCHASEORDER", Env.getAD_Client_ID(Env.getCtx())).trim());
		MInvoice inv = null;

		String desc = order.getDescription();
		Integer payTermId = order.getC_PaymentTerm_ID();
		Integer documentMM = Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTMMSHIPMENT", Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer locatorID = DataQueries.getLocatorId(order.getM_Warehouse_ID());
		description = "Create In/Out";
		MInOut io = Documents.createMInOut(trxName, desc, order, order.getDateOrdered(), documentMM, sales);
		MOrderLine[] listOrders = order.getLines();
		if (listOrders == null || listOrders.length <= 0) {
			log.log(Level.SEVERE,
					CompleteSaleWeb.class.getCanonicalName() + ".completeDocumentSales - Error Create Order Lines Pruchase Automatic :: " + description);
			return null;
		}

		int clientCode = Env.getAD_Client_ID(Env.getCtx());
		Integer documentARInv = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID", "-1")), null)
				.get_ValueAsInt("C_DocTypeTarget_ID");

		// Integer salesId = Integer.parseInt(MSysConfig.getValue("SMJ_SALESLIST",clientCode).trim());
		Integer salesId = order.getM_PriceList_ID(); // toma la lista de precios que viene en la orden
		if (orgId.equals(orgInfotrans)) {
			salesId = Integer.parseInt(MSysConfig.getValue("SMJ_INFSALESLIST", clientCode).trim());
			principalWarehouse = Integer.parseInt(MSysConfig.getValue("SMJ-INFWHPRINCIPAL", clientCode).trim());

			purchaseId = Integer.parseInt(MSysConfig.getValue("SMJ_INFPURCHASELIST", clientCode, orgId).trim());
			// si la primera linea era una orden de transporte, cambia el tipo de factura
			if (hasTransportRequest) // fix solicitud 384/385
			{
				documentARInv = Integer.parseInt(MSysConfig.getValue("SMJ-DOCUMENTARINVOICE-IT2", clientCode, orgId).trim()); // obtiene
																																// el
																																// tipo
																																// por
																																// organizacion
			}
		}
		String plate = order.get_ValueAsString("smj_plate");
		Integer vehicleId = order.get_ValueAsInt("smj_vehicle_ID");
		String workOrder = order.get_ValueAsString("smj_workorder");
		if (!generatedOnlyShipment) {
			inv = Documents.createInvoiceTrans(trxName, io, desc, order.getDateOrdered(), salesId, documentARInv, payTermId, plate, vehicleId, workOrder,
					order.getPaymentRule(), io.isSOTrx());
		}

		if (listOrders != null && listOrders.length > 0) {
			MOrder purchaseOrder = null;
			Map<Integer, MInOut> listInOutConsignmentPositive = null;
			Map<Integer, MInOut> listInOutConsignmentNegative = null;
			Map<Integer, Map<Integer, BigDecimal>> mapProductAttributeQty = new HashMap<Integer, Map<Integer, BigDecimal>>();

			for (MOrderLine oLine : listOrders) {

				String plateLineOrder = oLine.get_ValueAsString("SMJ_Plate");
				if (plateLineOrder != null && !plateLineOrder.isEmpty()) {
					String descriptionOrderLine = oLine.getC_BPartner().getName() + " - ";
					descriptionOrderLine += new SimpleDateFormat("dd/MM/yyyy").format(order.getDateOrdered()) + " - ";
					descriptionOrderLine += Msg.getElement(Env.getCtx(), "SMJ_Plate") + ": " + plateLineOrder;
					if(oLine.getDescription()!=null)
						descriptionOrderLine += "\n" + oLine.getDescription(); 
					oLine.setDescription(descriptionOrderLine);
					oLine.saveEx();
				}

				Integer lineLocator = oLine.get_ValueAsInt("smj_locator_ID");
				MProduct p = oLine.getProduct();
				Boolean isService = p.getProductType().equals("S") ? true : false;
				Integer M_InOutLine_ID = null;
				// logica de crear orden de compra

				if (!locatorID.equals(lineLocator) && !isService) {

					if (listInOutConsignmentNegative == null) {
						listInOutConsignmentNegative = new HashMap<Integer, MInOut>();
					}

					if (listInOutConsignmentPositive == null) {
						listInOutConsignmentPositive = new HashMap<Integer, MInOut>();
					}

					String generalDesc = "";
					if (generatedOnlyShipment) {
						generalDesc = "Orden de Venta: " + order.getDocumentNo() + " - Entrega: " + io.getDocumentNo() + " - " + desc;
					} else {
						generalDesc = "Orden de Venta: " + order.getDocumentNo() + "- Factura: " + inv.getDocumentNo() + " - Entrega: " + io.getDocumentNo()
								+ " - " + desc;
					}

					Integer providerId = oLine.get_ValueAsInt("smj_Provider_ID");
					if (providerId == null || providerId <= 0) {
						MLocator locator = MLocator.get(Env.getCtx(), lineLocator);
						providerId = DataQueries.getwarehouseOwner(locator.getM_Warehouse_ID());
					}

					String listName = DataQueries.getBpartnerName(lclientId, providerId);

					int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
					int M_PriceList_Version_ID = DataQueries.getPriceListVersion(trxName, lclientId, "Compra_" + listName.trim());

					// Positive (Consignment)
					MInOut inOutConsignmentPositive;
					if (listInOutConsignmentPositive.containsKey(providerId)) {
						inOutConsignmentPositive = listInOutConsignmentPositive.get(providerId);
						listInOutConsignmentPositive.remove(providerId);
					} else {
						inOutConsignmentPositive = Documents.createConsignmentDocuments(providerId, M_PriceList_Version_ID, M_Warehouse_ID, generalDesc, true, trxName);
					}

					// Negative (Consignment)
					MInOut inOutConsignmentNegative;
					if (listInOutConsignmentNegative.containsKey(lineLocator)) {
						inOutConsignmentNegative = listInOutConsignmentNegative.get(lineLocator);
						listInOutConsignmentNegative.remove(lineLocator);
					} else {
						int M_Warehouse_Provider_ID = MLocator.get(Env.getCtx(), lineLocator).getM_Warehouse_ID();
						inOutConsignmentNegative = Documents.createConsignmentDocuments(providerId, M_PriceList_Version_ID, M_Warehouse_Provider_ID,
								generalDesc, false, trxName);
					}

					Consumer<MInOutLine> createShipmentLine = createShipmentLine(io, oLine, locatorID);
					Map<Integer, BigDecimal> mapAttributeQty = Documents.createConsignmentLinesNegative(inOutConsignmentNegative, inOutConsignmentPositive,
							oLine.getM_Product_ID(), M_PriceList_Version_ID, oLine.getQtyOrdered().negate(),
							mapProductAttributeQty.get(oLine.getM_Product_ID()), createShipmentLine, trxName);

					listInOutConsignmentNegative.put(lineLocator, inOutConsignmentNegative);
					listInOutConsignmentPositive.put(providerId, inOutConsignmentPositive);

					// Updates the attributes used by product
					if (mapProductAttributeQty.containsKey(oLine.getM_Product_ID())) {
						mapProductAttributeQty.remove(oLine.getM_Product_ID());
						mapProductAttributeQty.put(oLine.getM_Product_ID(), mapAttributeQty);
					} else {
						mapProductAttributeQty.put(oLine.getM_Product_ID(), mapAttributeQty);
					}

					// --------iMARCH-----------busca si es bodega de consignacion--------------------
					String LocatorConsignament = DataQueries.getLocatorConsignment(lineLocator);
					if (LocatorConsignament.equals("Y")) {
						Integer existentPO = DataQueries.getCurrentPurchaseOrderPOS(trxName, descPurchaseOrder, providerId, docPurchaseOrder,
								Env.getAD_Client_ID(Env.getCtx()));
						if (existentPO > 0) {
							purchaseOrder = new MOrder(Env.getCtx(), existentPO, trxName);
						} else {
							purchaseOrder = Documents.createPurchaseOrder(trxName, descPurchaseOrder, providerId, order.getDateOrdered(),
									Env.getAD_User_ID(Env.getCtx()), payTermId, docPurchaseOrder, principalWarehouse, purchaseId, true);
						}
						BigDecimal purchasePrice = DataQueries.getPurchasePriceTrans(trxName, p, providerId, lineLocator);
						String descPurchaseLine = generalDesc + " - " + descPurchaseOrder;

						Timestamp time = Env.getContextAsDate(Env.getCtx(), "#Date");
						Integer code = Documents.createOrderLineTrans(purchaseOrder, oLine.getM_Product_ID(), oLine.getQtyEntered(), purchasePrice,
								descPurchaseLine, oLine.getM_AttributeSetInstance_ID(), locatorID, providerId, true, providerId, time, null, null, null);
						if (code <= 0) {
							log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".completeDocumentSales - Error Create Purchase order Line :: "
									+ descPurchaseOrder);
							return null;
						}
					} // orden de compra si es bodega consignacion				
				} // if locators
				
				String descLine = oLine.getDescription();
				
				if (locatorID.equals(lineLocator) || isService) {
					MInOutLine ioLine = Documents.createIOLineTrans(io, descLine, oLine.getM_Product_ID(), locatorID, oLine.getQtyEntered(),
							oLine.getC_OrderLine_ID(), oLine.getLine());
					
					if (ioLine == null) {
						return null;
					} else {
						M_InOutLine_ID = ioLine.getM_InOutLine_ID();
					}
				}
				
				String plateLine = oLine.get_ValueAsString("smj_plate");
				// se elimino por problema de inventarios oLine.getM_AttributeSetInstance_ID()
				if (!generatedOnlyShipment) { // no ejecuta si es solo entrega
					Integer invLineId = Documents.createInvoiceLineTrans(inv, descLine, oLine.getM_Product_ID(), oLine.getPriceEntered(), oLine.getC_Tax_ID(),
							oLine.getLineNetAmt(), oLine.getQtyEntered(), 0, oLine.getC_OrderLine_ID(), plateLine, oLine.getLine(), M_InOutLine_ID,
							0);
					
					if (invLineId <= 0) {
						return null;
					}

					Integer woLineId = oLine.get_ValueAsInt("smj_workOrderLine_ID");
					if (woLineId != null && woLineId > 0) {
						MSMJWorkOrderLine woLine = new MSMJWorkOrderLine(Env.getCtx(), woLineId, trxName);
						MRequest req = new MRequest(Env.getCtx(), woLine.getR_Request_ID(), trxName);
						req.setR_Status_ID(stateInvoiced);
						req.setC_Invoice_ID(inv.getC_Invoice_ID());
						req.setC_Order_ID(order.get_ID());

						req.saveEx();
						Boolean wok = calculateCommisionProcess(trxName, woLine, invLineId);
						if (!wok) {
							return null;
						}
					}
				} else {
					Integer woLineId = oLine.get_ValueAsInt("smj_workOrderLine_ID");

					if (woLineId != null && woLineId > 0) {
						MSMJWorkOrderLine woLine = new MSMJWorkOrderLine(Env.getCtx(), woLineId, trxName);
						MRequest req = new MRequest(Env.getCtx(), woLine.getR_Request_ID(), trxName);
						req.setR_Status_ID(stateInvoiced);
						req.setC_Order_ID(order.get_ID());

						req.saveEx();
					}
				}
			} // while ito

			// Validate if has consignment documents to complete
			if (listInOutConsignmentPositive != null && listInOutConsignmentNegative != null && !listInOutConsignmentNegative.isEmpty()) {

				// Positive
				Iterator<Integer> listPositive = listInOutConsignmentPositive.keySet().iterator();
				while (listPositive.hasNext()) {
					MInOut inOutConsignmentPositive = listInOutConsignmentPositive.get(listPositive.next());
					MOrder poConsignmentPositive = new MOrder(Env.getCtx(), inOutConsignmentPositive.getC_Order_ID(), trxName);
					
					poConsignmentPositive.setDocAction(DocAction.ACTION_Complete);
					if (!poConsignmentPositive.processIt(DocAction.ACTION_Complete)) {
						throw new AdempiereException(poConsignmentPositive.getProcessMsg());
					}

					poConsignmentPositive.saveEx();

					inOutConsignmentPositive.setDocAction(DocAction.ACTION_Complete);
					if (!inOutConsignmentPositive.processIt(DocAction.ACTION_Complete)) {
						throw new AdempiereException(inOutConsignmentPositive.getProcessMsg());
					}

					inOutConsignmentPositive.saveEx();				
				}

				// Negative
				Iterator<Integer> listNegative = listInOutConsignmentNegative.keySet().iterator();
				while (listNegative.hasNext()) {
					MInOut inOutConsignmentNegative = listInOutConsignmentNegative.get(listNegative.next());
					MOrder poConsignmentNegative = new MOrder(Env.getCtx(), inOutConsignmentNegative.getC_Order_ID(), trxName);

					poConsignmentNegative.setDocAction(DocAction.ACTION_Complete);
					if (!poConsignmentNegative.processIt(DocAction.ACTION_Complete)) {
						throw new AdempiereException(poConsignmentNegative.getProcessMsg());
					}

					poConsignmentNegative.saveEx();

					inOutConsignmentNegative.setDocAction(DocAction.ACTION_Complete);
					if (!inOutConsignmentNegative.processIt(DocAction.ACTION_Complete)) {
						throw new AdempiereException(inOutConsignmentNegative.getProcessMsg());
					}

					inOutConsignmentNegative.saveEx();
				}
			}
			
			io.setDocAction(DocAction.ACTION_Complete);
			if (!io.processIt(DocAction.ACTION_Complete)) {
				throw new AdempiereException(io.getProcessMsg());
			}

			io.saveEx();

			if (!generatedOnlyShipment) { // no ejecuta si es solo entrega
				String status = inv.completeIt();

				if (!status.equals(DocAction.STATUS_Completed)) {
					throw new AdempiereException(inv.getProcessMsg());
				}

				inv.setDocStatus(DocAction.STATUS_Completed);
				inv.setDocAction(DocAction.STATUS_Closed);
				inv.setProcessed(true);
				orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim();
				if (orgId.equals(orgInfotrans) || orgfreighttrans.equals("true")) {		//Modificacion iMarch / Organizacion Transportadora **|| orgfreighttrans.equals("true")**
					String whereClause = "C_Order_ID = " + order.getC_Order_ID();
					MSMJTmpWebSales tmpSales = new Query(Env.getCtx(), MSMJTmpWebSales.Table_Name, whereClause, order.get_TrxName()).firstOnly();

					for (Integer R_Request_ID : tmpSales.getsmj_workorder()) {
						MRequest request = new MRequest(Env.getCtx(), R_Request_ID, trxName);
						request.setC_Invoice_ID(inv.getC_Invoice_ID());
						request.saveEx();
					}
				}

				inv.saveEx();
			} // (!generatedOnlyShipment)
		} else {
			throw new NullPointerException();
		}

		return inv;
	}// completeDocumentsSales

	/**
	 * crea las comisiones de los mecanicos - create mechanic commission line
	 * 
	 * @param trxName
	 * @param woLineId
	 * @return
	 */
	public static Boolean calculateCommisionProcess(String trxName, MSMJWorkOrderLine woLine, Integer invLineId) {
		try {
			LinkedList<MSMJWoMechanicAssigned> listMec = DataQueries.getMechanicList(woLine.getsmj_workOrderLine_ID());
			Iterator<MSMJWoMechanicAssigned> it = listMec.iterator();
			while (it.hasNext()) {
				MSMJWoMechanicAssigned wm = it.next();
				MSMJMechanicCommission mc = new MSMJMechanicCommission(Env.getCtx(), 0, trxName);
				BigDecimal appliedfee = calculateFee(woLine.getM_Product_ID(), wm.getC_BPartner_ID());
				mc.setC_BPartner_ID(wm.getC_BPartner_ID());
				mc.setM_Product_ID(woLine.getM_Product_ID());
				mc.setR_Request_ID(woLine.getR_Request_ID());
				mc.setsmj_workOrderLine_ID(woLine.getsmj_workOrderLine_ID());
				mc.setQty(woLine.getQty());
				mc.setappliedfee(appliedfee);
				BigDecimal total = (woLine.gettotal().multiply(appliedfee)).divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_DOWN);
				mc.settotal(total);
				mc.setC_InvoiceLine_ID(invLineId);
				mc.settotalinvoiced(woLine.gettotal());
				mc.setdatecreated(Env.getContextAsDate(Env.getCtx(), "#Date"));
				Boolean ok = mc.save();
				if (!ok) {
					return false;
				}
			} // while (it.hasNext())
			if (woLine.issmj_iswarranty() || woLine.getsmj_workorderline_parent_ID() > 0) {
				LinkedList<MSMJMechanicCommission> listGar = DataQueries.getMechanicCommisionList(woLine.getsmj_workorderline_parent_ID());
				Iterator<MSMJMechanicCommission> itg = listGar.iterator();
				while (itg.hasNext()) {
					MSMJMechanicCommission gm = itg.next();
					MSMJMechanicCommission mc = new MSMJMechanicCommission(Env.getCtx(), 0, trxName);
					mc.setC_BPartner_ID(gm.getC_BPartner_ID());
					mc.setM_Product_ID(gm.getM_Product_ID());
					mc.setR_Request_ID(gm.getR_Request_ID());
					mc.setsmj_workOrderLine_ID(gm.getsmj_workOrderLine_ID());
					mc.setQty(gm.getQty());
					mc.setappliedfee(gm.getappliedfee().multiply(new BigDecimal(-1)));
					mc.settotal(gm.gettotal().multiply(new BigDecimal(-1)));
					mc.setC_InvoiceLine_ID(invLineId);
					mc.settotalinvoiced(woLine.gettotal());
					mc.setdatecreated(Env.getContextAsDate(Env.getCtx(), "#Date"));
					Boolean ok = mc.save();
					if (!ok) {
						return false;
					}
				} // while (itg.hasNext())
			} // if(woLine.issmj_iswarranty())
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".workOrderProcess - ERROR: " + e.getMessage(), e);
			return false;
		}
		return true;
	}// workOrderProcess

	/**
	 * calcula la comision del mecanico - calculate mechanic commission
	 * 
	 * @param productID
	 * @param partnerID
	 * @return
	 */
	private static BigDecimal calculateFee(Integer productID, Integer partnerID) {
		BigDecimal appliedfee = Env.ZERO;
		try {
			MProduct p = MProduct.get(Env.getCtx(), productID);
			MBPartner b = MBPartner.get(Env.getCtx(), partnerID);
			Boolean isCommission = p.get_ValueAsBoolean("smj_iscommission");
			Boolean isFixed = p.get_ValueAsBoolean("smj_isfixed");
			Boolean isPartnerCom = b.get_ValueAsBoolean("smj_iscommission");
			// si servicio no comisiona
			if (!isCommission) {
				return Env.ZERO;
			}
			// si tercero no comision
			else if (!isPartnerCom) {
				return Env.ZERO;
			}
			// si servicio comision fija
			else if (isFixed) {
				appliedfee = new BigDecimal(p.get_ValueAsString("smj_valuemeccommision"));
				return appliedfee;
			}
			// si tercero comision variable
			else {
				appliedfee = new BigDecimal(b.get_ValueAsString("smj_commissionvalue"));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, CompleteSaleWeb.class.getCanonicalName() + ".calculateFee - ERROR: " + e.getMessage(), e);
		} // try/catch
		return appliedfee;
	}// calculateFee

	private static Consumer<MInOutLine> createShipmentLine(final MInOut io, final MOrderLine oLine, final int locatorId) {
		return new Consumer<MInOutLine>() {
			@Override
			public void accept(MInOutLine ioLine) throws Exception {
				Documents.newIoLineFromAnotherIoLine(Env.getCtx(), io, ioLine, oLine.get_ID(), locatorId, oLine.getDescription(), oLine.getLine()).get_ID();				
			}//Modificacion iMarch , Integer sequence
		};
	}
	// ==================== TRANSPORT ====================
}// CompleteSaleWeb
