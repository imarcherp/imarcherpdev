package com.smj.util;

import org.adempiere.webui.component.Messagebox;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class Message {

	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	
	/**
	 * Muestra un mensjae de alerta
	 * - El mensaje es traducido automaticamente
	 * @param message
	 */
	public static void showWarning(String message) {
		showWarning(message, true);		
	}
	
	/**
	 * Muestra un mensaje de alerta
	 * - El mensaje puede ser traducido si se pasa true com
	 * segundo parametro.
	 * @param message
	 * @param translate
	 */
	public static void showWarning(String message, Boolean translate) {
		if (translate)
			message = Msg.translate(Env.getCtx(), message);
					
		Messagebox.showDialog(message, labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	
	/**
	 * Muestra un mensaje de alerta
	 * - El mensaje puede ser traducido si se pasa true com
	 * segundo parametro.
	 * @param message
	 * @param translate
	 */
	public static void showWarningPlusInfo(String message, String info, Boolean translate) {
		if (translate)
			message = Msg.translate(Env.getCtx(), message);
					
		Messagebox.showDialog(message+" "+info, labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	
	/**
	 * Muestra un mensaje de error
	 * Show a error message
	 * - El mensaje es traducido automaticamente
	 * @param message
	 */
	public static void showError(String message) {
		showError(message, true);
	}
	
	/**
	 * Muestra un mensaje de error
	 * Show a error message
	 * - El mensaje puede ser traducido si se pasa true com
	 * segundo parametro.
	 * @param message
	 * @param translate
	 */
	public static void showError(String message, Boolean translate) {
		if (translate)
			message = Msg.translate(Env.getCtx(), message);
					
		Messagebox.showDialog(message, labelInfo, Messagebox.OK, Messagebox.ERROR);
	}
}
