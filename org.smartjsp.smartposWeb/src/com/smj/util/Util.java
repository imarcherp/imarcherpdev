package com.smj.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.Env;

//import com.smj.model.MSMJTires;

/**
 * @version <li>SmartJSP: Util, 2013/02/11
 *          <ul TYPE ="circle">
 *          <li>Clase para utilidades generales 
 *          <li>Class to general utilities 
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class Util {

	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Util.class);
	
	/**
	 * Redimenciona la imagen
	 * @param img
	 * @param targetWidth
	 * @param targetHeight
	 * @return
	 */
	public static BufferedImage scaleImage(BufferedImage img, int targetWidth, int targetHeight) {

	    int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
	    BufferedImage ret = img;
	    BufferedImage scratchImage = null;
	    Graphics2D g2 = null;

	    int w = img.getWidth();
	    int h = img.getHeight();

	    int prevW = w;
	    int prevH = h;

	    do {
	        if (w > targetWidth) {
	            w /= 2;
	            w = (w < targetWidth) ? targetWidth : w;
	        }

	        if (h > targetHeight) {
	            h /= 2;
	            h = (h < targetHeight) ? targetHeight : h;
	        }

	        if (scratchImage == null) {
	            scratchImage = new BufferedImage(w, h, type);
	            g2 = scratchImage.createGraphics();
	        }

	        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        g2.drawImage(ret, 0, 0, w, h, 0, 0, prevW, prevH, null);

	        prevW = w;
	        prevH = h;
	        ret = scratchImage;
	    } while (w != targetWidth || h != targetHeight);

	    if (g2 != null) {
	        g2.dispose();
	    }

	    if (targetWidth != ret.getWidth() || targetHeight != ret.getHeight()) {
	        scratchImage = new BufferedImage(targetWidth, targetHeight, type);
	        g2 = scratchImage.createGraphics();
	        g2.drawImage(ret, 0, 0, null);
	        g2.dispose();
	        ret = scratchImage;
	    }

	    return ret;

	}
	
	/**
	 * redondea un BigDeciamal a centena - 
	 *  round BigDeciamal a hundred
	 * @param val
	 * @return
	 */
	public static BigDecimal roundingHundreds(BigDecimal val){
		if (val == null)
			return Env.ZERO;
		BigDecimal rounded = Env.ZERO;
		try	{
			//elimina decimales
			BigDecimal xval = val.setScale(0,BigDecimal.ROUND_DOWN);
			String dato = xval.toString();
			//valida valor mayor a 100
			if (xval.compareTo(new BigDecimal(100))<=0){
				return new BigDecimal(100);
			}
			//obtiene los dos ultimos digitos
			String two = dato.substring(dato.length()-2);
			BigDecimal cent = Env.ZERO;
			//obtinene los digitos para completar la centena
			if (!two.equals("00")){
				cent = new BigDecimal(100).subtract(new BigDecimal(two));
			}
			//valor redondeado a centena
			rounded = xval.add(cent);
		}catch (Exception e) {
			log.log(Level.SEVERE, Util.class.getClass().getName() + ".roundingHundreds - ERROR: " + e.getMessage(), e);
		}
		return rounded;
	}//roundingHundreds
	

	/**
	 * calcula el precio de venta - calculate sales price
	 * @param price
	 * @param util
	 * @param tax
	 * @return BigDecimal
	 */
	public static BigDecimal calculateSalesPrice(BigDecimal price, BigDecimal util, BigDecimal tax){
		BigDecimal sales = Env.ZERO;
		try{
			if(util == null)
				util = Env.ZERO;
			BigDecimal valUtil = price.multiply((util.divide(new BigDecimal(100))));
			BigDecimal newPrice = price.add(valUtil);
			BigDecimal valTax = newPrice.multiply((tax.divide(new BigDecimal(100))));
			sales = newPrice.add(valTax);
		}catch (Exception e) {
			log.log(Level.SEVERE,Util.class.getClass().getName() + ".calculateSalesPrice - ERROR: " + e.getMessage(),e);
		}
		
		BigDecimal salesPrice = roundingHundreds(sales);
		return salesPrice;
	}//calculateSalesPrice
	
	
	
	/**
	 * regresa un numero aleatorio entre 10000 y 99999 - 
	 * return randon number between 10000 and 99999
	 * @return
	 */
	public static int getRdmInteger(){
		Random r = new Random();
		int rdm = r.nextInt(99999); 
		if (rdm < 10000)
			rdm = rdm + 10000;
		return rdm; 
	}//getRdmInteger

	/**
	 * Devuelve el formato con el cual se desea mostrar un numero, 
	 * segun los decimales pasados como parametro.
	 * @param nf
	 * @return
	 */
	public static String getNumberFormatAsString(int decimals) {
		String format = "#";
		for (int i = 0; i < decimals; i++) {
			if (i == 0) {
				format += ".";
			}
			
			format += "#";
		}
		return format;
	}
	
	/**
	 * valida si el rol esta en la lista de los permitidos - 
	 * validate if role is in permitted role list 
	 * @param logRol
	 * @param rols
	 * @return
	 */
	public static Boolean validateRol(Integer logRol, String rols){
		Boolean ok = false;
		try {
			if (rols == null || rols.length()<=0){
				return true;
			}
			String[] lRol = rols.split(";");
			for (int c=0;c<lRol.length;c++){
				Integer rol = Integer.parseInt(lRol[c]);
				if (rol.equals(logRol)){
					ok = true;
				}
			} // for
		} catch (Exception e) {
			log.log(Level.SEVERE,Util.class.getCanonicalName() + ".validateRol - ERROR: " + e.getMessage(),e);
		}
//		System.out.println(Util.class.getCanonicalName()+".validateRol ...ok.."+ok);
		return ok;
	}//validateRol

	/**
	 * calcula el valor el porcentaje de comision - 
	 * Calculate Brokerage Percent
	 * @param invoiceValue
	 * @param chargeValue
	 * @return
	 */
	public static BigDecimal calculateBrokeragePercent (BigDecimal invoiceValue, BigDecimal chargeValue){
		BigDecimal percent = Env.ZERO;
		try {
			BigDecimal div = invoiceValue.divide(chargeValue, 4, BigDecimal.ROUND_DOWN);
			percent = (div.subtract(Env.ONE)).multiply(Env.ONEHUNDRED);
		} catch (Exception e) {
			log.log(Level.SEVERE,
					Util.class.getName() + ".calculateBrokeragePercent - ERROR: " + e.getMessage(),
					e);
		}
		return percent;
	}//calculateBrokeragePercent
	
}//Util
