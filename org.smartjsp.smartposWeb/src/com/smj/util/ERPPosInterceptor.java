package com.smj.util;

import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MClient;
import org.compiere.model.MPayment;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.smj.util.DataUtils;

public class ERPPosInterceptor implements ModelValidator {
	
	private int m_AD_Client_ID = 0;
	private int m_AD_Org_ID  = 0;	
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(ERPPosInterceptor.class);

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		// This line must come before registering the model changes
		//client = null for global validator
		if (client != null) {	
			m_AD_Client_ID = client.getAD_Client_ID();
		}
		
		//	register for model Change on C_BPartner
		engine.addModelChange(MBPartner.Table_Name, this);
		engine.addModelChange(MPayment.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {		
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {		
		m_AD_Org_ID = AD_Org_ID;  
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		String message = null;
		
		if (po.get_TableName().equals(MBPartner.Table_Name) && type == TYPE_AFTER_NEW) {
			MBPartner bPartner = (MBPartner) po;
			m_AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
			
			int M_Locator_ID = SMJConfig.getIntValue("C_BPartner_Location_ID", 0, m_AD_Org_ID); //Partner Location
			if (M_Locator_ID != 0) {
				MBPartnerLocation bPartnerLocation = new MBPartnerLocation(Env.getCtx(), M_Locator_ID, po.get_TrxName()); 

				// Create a new MBPartnerLocation From MBPartnerLocation Default
				MBPartnerLocation newBPartnerLocation = new MBPartnerLocation(Env.getCtx(), 0, po.get_TrxName());
				newBPartnerLocation.setC_BPartner_ID(bPartner.get_ID());
				newBPartnerLocation.setName(bPartnerLocation.getName());
				newBPartnerLocation.setPhone(bPartnerLocation.getPhone());
				newBPartnerLocation.setPhone2(bPartnerLocation.getPhone2());
				newBPartnerLocation.setAD_Org_ID(bPartnerLocation.getAD_Org_ID());
				newBPartnerLocation.setC_Location_ID(bPartnerLocation.getC_Location_ID());

				if (!newBPartnerLocation.save()) {
					message = "Error modelChange when tries save new BPartnerLocation";
				}
			}
		} else if (po.get_TableName().equals(MPayment.Table_Name) && type == TYPE_AFTER_NEW) {
			int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
			if (M_Warehouse_ID < 1) {
				return null;
			}

			MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, po.get_TrxName());
			boolean ispos = warehouse.get_ValueAsBoolean("ispos");

			if (!ispos) {
				return null;
			}
			
			int C_BankAccount_ID = warehouse.get_ValueAsInt("C_BankAccount_ID");
			MPayment payment = (MPayment) po;
			payment.setC_BankAccount_ID(C_BankAccount_ID);
			payment.saveEx();
		}
		
		return message;
	}

	@Override
	public String docValidate(PO po, int timing) {
		// TODO Auto-generated method stub
		return null;
	}

}
