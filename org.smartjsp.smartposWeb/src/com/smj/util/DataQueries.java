package com.smj.util;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.compiere.model.MAccount;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MLocator;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.model.X_M_RMAType;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.smj.model.MSMJMechanicCommission;
import com.smj.model.MSMJTmpPayment;
import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;

public class DataQueries {
	/** Logger */
	public static CLogger log = CLogger.getCLogger(DataQueries.class);
	private static Integer lclientId = Env.getAD_Client_ID(Env.getCtx());
	private static Integer lorgId = Env.getAD_Org_ID(Env.getCtx());
	private static String lang = Env.getAD_Language(Env.getCtx());
	private static Integer purchaseListVersionId = SMJConfig.getIntValue("M_PriceList_Version_ID", 0, lorgId);

	/**
	 * Returns the AD message of the value
	 * 
	 * @param value
	 * @return String
	 */
	public static String getMessage(String value) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT msgtext FROM ad_message WHERE value = '" + value + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String msg = "";
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				msg = rs.getString("msgtext");

			} // if rs
		} // try
		catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getMessage - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return msg;
	}// getMessage

	/**
	 * Regresa true si el periodo esta abierto o false si esta cerrado - returns
	 * true if period is opened or false if is closed
	 * 
	 * @param dateInvoice
	 * @param ad_client_id
	 * @return Boolean
	 */
	public static Boolean isPeriodOpen(Date dateInvoice, Integer ad_client_id, Integer ad_org_id) {
		StringBuffer sql = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String myDate = sdf.format(dateInvoice);
		sql.append(" SELECT periodstatus FROM C_PeriodControl WHERE docbasetype = 'SOO' ");
		sql.append(" AND C_Period_id = (SELECT MAX(C_Period_ID) FROM C_Period WHERE '" + myDate + "' ");
		sql.append(" BETWEEN startdate AND enddate AND AD_Client_ID = " + ad_client_id + " ) ");
		// removida la validacion por organizacion, se usa el periodo del
		// cliente.
		// sql.append(" AND AD_Org_ID = "+ad_org_id +") ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Boolean period = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				period = rs.getString("periodstatus").equals("O");
			} // while rs
		} // try
		catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".isPeriodOpen - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return period;
	}// isPeriodOpen

	/**
	 * Regresa true si cualquier linea del ticket fue ya enviada por comanda
	 * returns true if any ticket line was already sent
	 * 
	 * @param ticketId
	 * @param ad_client_id
	 * @return Boolean
	 */
	public static Boolean isTicketSent(int ticketId, Integer ad_client_id, Integer ad_org_id) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT smj_isworkorder  FROM smj_tmpwebsalesline WHERE smj_tmpwebsales_id = " + ticketId);
		sql.append(" AND ad_org_id =  " + ad_org_id);
		sql.append(" AND AD_Client_ID = " + ad_client_id + " ");
		sql.append(" AND smj_isworkorder = 'Y' ");
		// removida la validacion por organizacion, se usa el periodo del
		// cliente.
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Boolean ticketSent = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				ticketSent = rs.getString("smj_isworkorder").equals("Y");
			} // while rs
		} // try
		catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".isPeriodOpen - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return ticketSent;
	}// isTicketSent

	/**
	 * Regresa el valor del ID de la localicacion para un tercero - return
	 * location id by partner
	 * 
	 * @param bpartnerValue
	 * @return Integer
	 */
	public static Integer getLocationPartner(Integer bpartnerValue) {
		Integer location = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_bpartner_location_ID FROM c_bpartner_location WHERE isactive = 'Y' ");
		sql.append(" AND c_bpartner_ID =  " + bpartnerValue + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				location = rs.getInt("c_bpartner_location_ID");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getLotationPartner - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return location;
	}// getLotationPartner

	/**
	 * regresa el codigo de localizacion de la localizacion de un tercero -
	 * retunr location code by partner location
	 * 
	 * @param bpartnerLocation
	 * @return Integer
	 */
	public static Integer getLocation(Integer bpartnerLocation) {
		Integer location = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_location_id FROM c_bpartner_location WHERE isactive = 'Y' ");
		sql.append(" AND c_bpartner_location_id = " + bpartnerLocation + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				location = rs.getInt("c_location_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getLocation - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return location;

	}// getLocation

	/**
	 * regresa el codigo del tercero por su numero de identificacion - returns
	 * bpartner id by tax id
	 * 
	 * @param taxId
	 * @return Integer
	 */
	public static Integer getPartnerByTaxId(Integer clientId, String taxId) {
		Integer code = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_bpartner_id FROM c_bpartner WHERE AD_Client_ID = " + clientId);
		sql.append(" AND TRIM(taxid) = '" + taxId.trim() + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				code = rs.getInt("c_bpartner_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPartnerByTaxId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPartnerByTaxId

	/**
	 * regresa el codigo del tercero por su numero de identificacion - returns
	 * bpartner id by tax id
	 * 
	 * @param taxId
	 * @return Integer
	 */
	public static Integer getPartnerByName(Integer clientId, String name) {
		Integer code = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_bpartner_id FROM c_bpartner WHERE AD_Client_ID = " + clientId);
		sql.append(" AND UPPER(TRIM(name)) = '" + name.trim().toUpperCase().trim() + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				code = rs.getInt("c_bpartner_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPartnerByName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPartnerByName

	/**
	 * regresa el nombre del tercero - returns BPartner name
	 * 
	 * @param bparnerId
	 * @return value
	 */
	public static String getBpartnerName(Integer clientId, Integer bparnerId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT name FROM C_BPartner WHERE AD_Client_Id = " + clientId);
		sql.append(" AND C_BPartner_ID = " + bparnerId + " ");

		String value = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				value = rs.getString("name");
			} // if
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getBpartnerName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getBpartnerName

	/**
	 * regresa el codigo de la lista de precios por nombre - returns pricelist
	 * code by name
	 * 
	 * @param clientId
	 * @param priceListName
	 * @return
	 */
	public static Integer getPriceListVersion(String trxName, Integer clientId, String priceListName) {
		Integer value = 0;
		if (priceListName.length() > 60) {
			priceListName = priceListName.substring(0, 59);
		}
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT M_PriceList_Version_ID FROM M_PriceList_Version ");
		sql.append(" WHERE isActive = 'Y' AND AD_Client_ID = " + clientId + " ");
		sql.append(" AND TRIM(name) = '" + priceListName.trim() + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("M_PriceList_Version_ID");

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPriceListVersion - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getPriceListVersion

	/**
	 * regresa el codigo de la lista de precios por id version - returns
	 * pricelist id by price list version code by
	 * 
	 * @param clientId
	 * @param priceListVersion
	 *            coce
	 * @return
	 */
	public static Integer getPriceListByVersion(String trxName, Integer clientId, int priceListVersion) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT m_pricelist_id  FROM M_PriceList_Version ");
		sql.append(" WHERE isActive = 'Y' AND AD_Client_ID = " + clientId + " ");
		sql.append(" AND m_pricelist_version_id  = " + priceListVersion);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("m_pricelist_id");

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPriceListByVersion - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getPriceListByVersion

	/**
	 * valida si existe la lista de precios para el producto - validate if exist
	 * pricelist by product
	 * 
	 * @param clientId
	 * @param priceListName
	 * @return
	 */
	public static Boolean validatePriceListExists(Integer productId, Integer versionId) {
		Integer value = 0;
		Boolean flag = true;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT count(1) AS total FROM M_ProductPrice ");
		sql.append(" WHERE M_Product_ID = " + productId + " ");
		sql.append(" AND M_PriceList_Version_ID = " + versionId + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("total");
			flag = value > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".validatePriceListExists - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return flag;
	}// validatePriceListExists

	/**
	 * valida si existe la lista de precios para el producto - validate if exist
	 * pricelist by product
	 * 
	 * @param clientId
	 * @param priceListName
	 * @return
	 */
	public static Boolean validatePriceListExistsTrx(Integer productId, Integer versionId, String trxName) {
		Integer value = 0;
		Boolean flag = true;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT count(1) AS total FROM M_ProductPrice ");
		sql.append(" WHERE M_Product_ID = " + productId + " ");
		sql.append(" AND M_PriceList_Version_ID = " + versionId + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("total");
			flag = value > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".validatePriceListExists - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return flag;
	}// validatePriceListExistsTRx

	/**
	 * Regresa el id del producto buscandolo por nombre - returns product id by
	 * name
	 * 
	 * @param nombreProducto
	 * @return Integer
	 */
	public static Integer getProductByName(Integer clientId, String nombreProducto) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT M_Product_ID, name FROM M_Product ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = " + clientId + " ");
		sql.append("AND TRIM(name) = '" + nombreProducto.trim() + "' ");
		Integer productId = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				productId = rs.getInt("M_Product_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getProductByName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return productId;
	}// getProductByName

	/**
	 * Regresa el id del producto buscandolo por value - returns product id by
	 * value
	 * 
	 * @param nombreProducto
	 * @return Integer
	 */
	public static Integer getProductByValue(Integer clientId, String value) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT M_Product_ID, name FROM M_Product ");
		sql.append("WHERE isactive ='Y' AND ad_client_id = " + clientId + " ");
		sql.append("AND TRIM(value) = '" + value.trim() + "' ");

		Integer productId = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				productId = rs.getInt("M_Product_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getProductByValue - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return productId;
	}// getProductByValue

	/**
	 * regresa el codigo del tercero - returns BPartner Id
	 * 
	 * @param nit
	 * @return
	 */
	public static Integer getBpartnerId(Integer clientId, String nit) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_BPartner_ID, name FROM C_BPartner WHERE AD_Client_Id = " + clientId);
		sql.append(" AND TRIM(taxid) = '" + nit.trim() + "' ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("C_BPartner_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getBpartnerId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getBpartnerId

	/**
	 * valida si existe es almacenen consignacion - validate if consignament
	 * warehouse
	 * 
	 * @param locatorId
	 * @return
	 */
	public static Boolean validateIsConsignament(Integer locatorId) {
		String value = "";
		Boolean flag = true;
		StringBuffer sql = new StringBuffer();

		sql.append(
				" SELECT smj_isconsignment FROM M_Warehouse WHERE M_Warehouse_ID = (SELECT M_Warehouse_ID FROM M_Locator ");
		sql.append(" WHERE M_Locator_ID = " + locatorId + " ) ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getString("smj_isconsignment");
			flag = value.equals("Y") ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".validateIsConsignament - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return flag;
	}// validateIsConsignament

	/**
	 * trae el ultimo codigo de entrega para una orden - get last receipt code
	 * by order
	 * 
	 * @param orderIo
	 * @return
	 */
	public static Integer getIoByOrder(String trxName, Integer orderIo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT MAX(M_InOut_ID) AS CODE FROM M_InOut WHERE isactive = 'Y' ");
		sql.append(" AND docstatus = 'CO' AND C_Order_ID  = " + orderIo + " ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("CODE");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getIoByOrder - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getIoByOrder

	/**
	 * trae el ultimo codigo de entrega para una orden - get last receipt code
	 * by order
	 * 
	 * @param taxCategory
	 * @return BigDecimal
	 */
	public static BigDecimal getTaxValue(Integer taxCategory) {
		StringBuffer sql = new StringBuffer();

		if (!SMJConfig.getBooleanValue("IsPosTax", false, lorgId)) {
			sql.append(" SELECT rate FROM C_Tax  ");
			sql.append(" WHERE C_TaxCategory_ID = " + taxCategory + " ");
		} else {
			Integer taxId = SMJConfig.getIntValue("C_Tax_ID", 0, lorgId);
			sql.append(" SELECT rate FROM C_Tax  ");
			sql.append(" WHERE C_Tax_ID = " + taxId);
		}

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("rate");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTaxValue - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTaxValue

	/**
	 * regresa la cantidad total del inventario por tercero y producto - return
	 * total qty by partner and product
	 * 
	 * @param productId
	 * @return Integer
	 */
	public static BigDecimal getTotalProductByPartner(Integer productId, Integer partnerId, String trxName) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT  SUM(qty) as total from (  ");
		sql.append(
				" SELECT l.M_InOut_ID, l.M_InOutLine_ID, l.M_attributeSetInstance_ID , l.M_Product_ID, i.C_BPartner_ID, p.name AS Partner ");
		sql.append(
				" FROM M_InOutLine l, M_InOut i, C_BPartner p WHERE l.M_InOut_ID = i.M_InOut_ID AND p.C_BPartner_ID = i.C_BPartner_ID ");
		sql.append(" AND l.M_attributeSetInstance_ID > 0  AND  l.M_Product_ID =  " + productId + "  ");
		sql.append(" AND m_rmaline_id IS NULL AND  i.C_BPartner_ID = " + partnerId + ") x, ( ");
		sql.append(" SELECT s.M_Product_ID, s.M_attributeSetInstance_ID, SUM(s.qtyonhand) AS qty, p.name AS Product ");
		sql.append(" FROM M_Storage s, M_Product p WHERE p.M_Product_ID = s.M_Product_ID ");
		sql.append(" AND s.M_attributeSetInstance_ID > 0 AND  s.M_Product_ID = " + productId + " ");
		sql.append(" GROUP BY s.M_Product_ID, s.M_attributeSetInstance_ID, p.name ) y ");
		sql.append(" WHERE x.M_attributeSetInstance_ID = y.M_attributeSetInstance_ID ");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("total");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTotalProductByPartner - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTotalProductByPartner

	/**
	 * regresa el Id del registro de smj_totalProductProvider - returns
	 * smj_totalProductProvider_ID
	 * 
	 * @param productId
	 * @param partnerId
	 * @return
	 */
	public static Integer getTotalProductProviderId(Integer productId, Integer partnerId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT smj_totalProductProvider_ID FROM smj_totalProductProvider  ");
		sql.append(" WHERE C_BPartner_ID = " + partnerId + " AND M_Product_ID = " + productId + " ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("smj_totalProductProvider_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTotalProductProviderId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTotalProductProviderId

	/**
	 * regresa el total por proveedor de smj_totalProductProvider returns total
	 * by provider from smj_totalProductProvider
	 * 
	 * @param productId
	 * @param partnerId
	 * @return
	 */
	public static BigDecimal getQtyTotalProductProvider(Integer productId, Integer partnerId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT smj_total FROM smj_totalProductProvider  ");
		sql.append(" WHERE C_BPartner_ID = " + partnerId + " AND M_Product_ID = " + productId + " ");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("smj_total");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getQtyTotalProductProvider - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getQtyTotalProductProvider

	/**
	 * regresa el precio por producto por proveedor - returns pricelist by
	 * product and provider
	 * 
	 * @param productId
	 * @param name
	 * @return
	 */
	public static BigDecimal getPriceProductPartnerName(Integer productId, String name) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT pricestd FROM M_ProductPrice WHERE M_PriceList_Version_ID = ( ");
		sql.append(" SELECT MAX(M_PriceList_Version_ID) FROM M_PriceList_Version v , M_PriceList  p ");
		sql.append(" WHERE v.name like '%" + name + "' AND p.M_PriceList_ID = v.M_PriceList_ID ");
		sql.append(" AND p.issopricelist = 'N' ) AND M_Product_ID = " + productId + " ");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("pricestd");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPriceProductPartnerName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPriceProductPartnerName

	/**
	 * regresa los Id de las versiones de listas de precios para compra y venta
	 * - returns price list version id for puchase and sales
	 * 
	 * @param trxName
	 * @param p
	 * @param bparnerId
	 * @param tiresCategory
	 * @param locatorOrderId
	 * @param warehouseOwnerId
	 * @param service
	 * @return HashMap<String, Integer>
	 */
	public static HashMap<String, Integer> getPListVersion(String trxName, MProduct p, Integer bparnerId,
			Integer tiresCategory, Integer locatorOrderId, Integer warehouseOwnerId, String tempario) {
		HashMap<String, Integer> codes = new HashMap<String, Integer>();
		try {
			String listName = "";
			Boolean isService = p.getProductType().equals("S") ? true : false;
			if (isService) {
				String[] categories = MSysConfig.getValue("SMJ_PCATPROD_ADMSERVICE", "", Env.getAD_Client_ID(Env.getCtx())).trim().split(",");
				
				for (String category : categories) {
					if (p.getM_Product_Category_ID() == Integer.parseInt(category.trim())) {
						tempario = "Mostrador";
						break;
					}
				}
				
				if (tempario == null) {
					return null;
				}
				
				listName = tempario;
			} else {
				MLocator locator = MLocator.get(Env.getCtx(), locatorOrderId);
				MWarehouse warehouse = MWarehouse.get(Env.getCtx(), locator.getM_Warehouse_ID());
				if (p.getM_Product_Category_ID() == tiresCategory) {
					listName = "Llantas";
				} else if (warehouse.get_ValueAsBoolean("IsPOS")) {
					MPriceListVersion priceListVersion = new MPriceListVersion(Env.getCtx(), warehouse.get_ValueAsInt("M_PriceList_Version_ID"), null);
					listName = priceListVersion.getName().replace("Venta_", "");
				} else if (bparnerId.equals(warehouseOwnerId)) {
					listName = DataQueries.getBpartnerName(lclientId, bparnerId);
				} else {
					listName = "Mostrador";
				}
			}
			if (listName.equals("")) {
				return null;
			}
			String namePrice = "Compra_" + listName.trim();
			String nameSales = "Venta_" + listName.trim();
			Integer pId = DataQueries.getPriceListVersion(trxName, lclientId, namePrice);
			Integer sId = DataQueries.getPriceListVersion(trxName, lclientId, nameSales);
			codes.put("PURCHASE", pId);
			codes.put("SALES", sId);
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getCanonicalName() + ".getPListVersion - ERROR: " + e.getMessage(),
					e);
		}
		
		return codes;
	}// getPListVersion

	/**
	 * regresa el costo actual promedio - returns current Cost Price
	 * 
	 * @param productId
	 * @return
	 */
	public static BigDecimal getCost(Integer productId, Integer clientId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT currentCostPrice FROM M_Cost WHERE M_Product_ID = " + productId);
		sql.append(" AND AD_Client_ID = " + clientId + " ");
		sql.append(" AND updated = (SELECT MAX(updated) FROM M_Cost ");
		sql.append(" WHERE M_Product_ID = " + productId + " AND AD_Client_ID = " + clientId + ") ");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("currentCostPrice");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getCost - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getCost

	/**
	 * regresa el codigo del de la unidad del producto return uom id by product
	 * 
	 * @param code
	 * @return Integer
	 */
	public static Integer getProductUom(Integer code) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_uom_id FROM m_product WHERE m_product_ID = " + code + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Integer items = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				items = rs.getInt("c_uom_id");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getProductUom - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return items;
	}// getProductUom

	/**
	 * regresa el codigo impuesto del producto return tax id by product
	 * 
	 * @param code
	 * @return Integer
	 */
	public static Integer getProductTax(Integer code) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT x.C_Tax_ID FROM M_Product p, C_Tax x  ");
		sql.append(" WHERE  p.C_TaxCategory_ID = x.C_TaxCategory_ID ");
		sql.append(" AND p.M_Product_ID =  " + code + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Integer items = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				items = rs.getInt("C_Tax_ID");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getProductTax - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return items;
	}// getProductTax

	/**
	 * regresa la cantidad en existencia por producto y almacen - retuns qty by
	 * product and warehouse
	 * 
	 * @param productId
	 * @param warehouseId
	 * @return
	 */
	public static BigDecimal getQtyWarehouse(String trxName, Integer productId, Integer warehouseId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT SUM(s.qtyonhand) AS qty FROM M_Storage s, M_Locator w ");
		sql.append("  WHERE s.M_Locator_ID = w.M_Locator_ID AND s.M_Product_ID = " + productId + " ");
		sql.append("  AND w.M_Warehouse_ID = " + warehouseId + " ");
		PreparedStatement pstmt = null;
		BigDecimal value = Env.ZERO;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getBigDecimal("qty");
			if (value == null)
				value = Env.ZERO;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getQtyWarehouse - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getQtyWarehouse

	/**
	 * regresa el id y nombre del tercero por taxid - returns partner name and
	 * partner Id by taxId
	 * 
	 * @param value
	 * @return
	 */
	public static HashMap<String, Object> getPartnerDataByTaxId(String value) {
		HashMap<String, Object> data = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_BPartner_ID, name FROM C_BPartner WHERE isactive = 'Y' ");
		sql.append(" AND TaxId LIKE '%" + value + "%' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				data = new HashMap<String, Object>();
				data.put("ID", rs.getInt("C_BPartner_ID"));
				data.put("NAME", rs.getString("name"));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPartnerDataByTaxId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;

	}// getPartnerDataByTaxId

	/**
	 * Regresa el Objeto MUOM que coincida con el nombre enviado como parametro
	 * 
	 * @param name
	 * @return
	 */
	public static Integer getUOMByName(String name, String trxName) {
		Integer C_UOM_ID = null;

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C_UOM_ID FROM C_UOM WHERE name = '" + name + "'");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				C_UOM_ID = rs.getInt("C_UOM_ID");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getUOMByName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return C_UOM_ID;
	}

	/**
	 * regresa el nombre de la unidad - return uom name
	 * 
	 * @param value
	 * @return
	 */
	public static String getUomName(Integer value) {
		String data = "";
		StringBuffer sql = new StringBuffer();
		if (!lang.equalsIgnoreCase("en_US")) {
			sql.append(" SELECT name  FROM C_Uom_Trl WHERE ad_language = '" + lang + "' ");
			sql.append(" AND C_Uom_ID = " + value + " ");
		} else {
			sql.append(" SELECT name  FROM C_Uom WHERE C_Uom_ID = " + value + " ");
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				data = rs.getString("name");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getUomName - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getUomName

	/**
	 * regresa el nombre de la unidad - return uom name
	 * 
	 * @param value
	 * @return
	 */
	public static ArrayList<HashMap<String, Object>> getUOMConversionByProduct(Integer productId) {
		StringBuffer sql = new StringBuffer();
		String table = "c_uom c ";

		if (!lang.equalsIgnoreCase("en_US"))
			table = "c_uom_trl c ";

		sql.append("SELECT c.c_uom_id, c.name, 1 as dividerate FROM ");
		sql.append(table + "INNER JOIN m_product p ON p.c_uom_id = c.c_uom_id ");
		sql.append("WHERE p.m_product_id = " + productId);
		sql.append("UNION ");
		sql.append("SELECT c.c_uom_id, c.name, uc.dividerate FROM ");
		sql.append(table + "INNER JOIN c_uom_conversion uc ON uc.c_uom_to_id = c.c_uom_id ");
		sql.append("WHERE uc.m_product_id = " + productId + " ");

		if (!lang.equalsIgnoreCase("en_US"))
			sql.append("AND c.ad_language = '" + lang + "'");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, Object>> uomList = new ArrayList<HashMap<String, Object>>();

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> hashMap = new HashMap<String, Object>();
				hashMap.put("id", rs.getInt("c_uom_id"));
				hashMap.put("name", rs.getString("name"));
				hashMap.put("dividerate", rs.getBigDecimal("dividerate"));
				uomList.add(hashMap);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getUOMConversionByProduct - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return uomList;
	}// getUOMConversionByProduct

	public static BigDecimal getUOMDivideRate(Integer productId, Integer uomId) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  dividerate ");
		sql.append("FROM  c_uom_conversion ");
		sql.append("WHERE  m_product_id = " + productId + " and c_uom_to_id = " + uomId);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BigDecimal divideRate = Env.ONE;

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				divideRate = rs.getBigDecimal("dividerate");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getUOMDivideRate - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return divideRate;
	}

	/**
	 * regresa el nombre de la unidad - return uom name
	 * 
	 * @param value
	 * @return
	 */
	public static Integer getCurrencyDefault(Integer clientId) {
		Integer data = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(
				" SELECT C_Currency_ID FROM AD_ClientInfo i , C_AcctSchema s WHERE i.C_AcctSchema1_ID = s.C_AcctSchema_ID ");
		sql.append(" AND i.AD_Client_ID= " + clientId + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				data = rs.getInt("C_Currency_ID");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getCurrencyDefault - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getCurrencyDefault

	/**
	 * regresa la primera localizacion del almacen - returns the fist locator by
	 * warehouse
	 * 
	 * @param value
	 * @return
	 */
	public static Integer getLocatorId(Integer value) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = " + value + " ");
		sql.append(" AND isdefault = 'Y' ORDER BY M_Locator_ID ASC ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Locator_ID");
			} // if
			if (code == null || code <= 0) {
				code = getLocatorIdFirst(value);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getLocatorId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getLocatorId

	/**
	 * regresa el id almacen para un locator returns the fist locator by
	 * warehouse id specifing a locatorid
	 * 
	 * @param value
	 * @return
	 */
	public static Integer getWarehouseIdByLocator(Integer value) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Warehouse_ID FROM M_Locator WHERE M_Locator_ID = " + value + " ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Warehouse_ID");
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getWareHouseIdByLocator - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getWareHouseIdByLocator

	/**
	 * regresa la primera localizacion del almacen - returns the fist locator by
	 * warehouse
	 * 
	 * @param value
	 * @return
	 */
	private static Integer getLocatorIdFirst(Integer value) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = " + value + " ");
		sql.append(" ORDER BY M_Locator_ID ASC ");
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Locator_ID");
			} // if
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getLocatorIdFirst - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getLocatorIdFirst

	/**
	 * regresa la lista de terminos de pago returns payment term list
	 * 
	 * @param clientId
	 * @return Vector<Vector<Object>>
	 */
	public static Vector<Vector<Object>> getPayTermList(Integer clientId, Integer payterm, Integer paytermDef,
			Integer bpartnerId) {
		String terms = "";
		if (payterm > 0 && !(payterm.equals(paytermDef))) {
			terms = paytermDef.toString() + "," + payterm.toString();
		} else {
			terms = paytermDef.toString();
		}
		// if (bpartnerId != null && bpartnerId.equals(internalUser)){
		// terms = terms+","+payInternal;
		// }
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_PaymentTerm_ID, name FROM C_PaymentTerm WHERE isActive = 'Y' AND AD_Client_ID = "
				+ clientId);
		sql.append(" AND C_PaymentTerm_ID in (" + terms + ") ORDER BY name ASC ");
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			Vector<Object> line = new Vector<Object>();
			while (rs.next()) {
				line = new Vector<Object>();
				line.add(rs.getInt("C_PaymentTerm_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPayTermList - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getPayTermList

	/**
	 * regresa la lista de terminos de pago returns payment term list
	 * 
	 * @return Vector<Vector<Object>>
	 */
	public static Vector<Vector<Object>> getTenderTypeList(String types) {
		StringBuffer sql = new StringBuffer();
		if (!lang.equalsIgnoreCase("en_US")) {
			sql.append(" SELECT l.value, t.name as name FROM AD_Ref_List l, AD_Ref_List_Trl t ");
			sql.append(
					" WHERE l.AD_Ref_List_ID = t.AD_Ref_List_ID AND l.AD_Reference_ID=214  AND l.isactive='Y' AND ad_language = '"
							+ lang + "'");
		} else { // no busca traducciones si esta en ingles
			sql.append(" SELECT l.value, l.name as name FROM AD_Ref_List l");
			sql.append(" WHERE l.AD_Reference_ID=214  AND l.isactive='Y' ");
		}
		if (types != null) {
			sql.append(" AND value IN (" + types + ") ");
		}
		sql.append(" ORDER BY name ASC ");

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			Vector<Object> line = new Vector<Object>();
			while (rs.next()) {
				line = new Vector<Object>();
				String value = rs.getString("value");
				// if(!value.equals("X")){
				line.add(value);
				line.add(rs.getString("name"));
				data.add(line);
				// }
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTenderTypeList - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getTenderTypeList

	/**
	 * regresa la lista de tipos de tarjetas de credito returns credit card type
	 * list
	 * 
	 * @return Vector<Vector<Object>>
	 */
	public static Vector<Vector<Object>> getCredictCardTypeList() {
		StringBuffer sql = new StringBuffer();

		if (lang.equalsIgnoreCase("en_US")) {
			sql.append("SELECT value, name FROM AD_Ref_List ");
			sql.append("WHERE AD_Reference_ID=149 AND isactive='Y' ");
			sql.append("ORDER BY name ASC ");
		} else {
			sql.append("SELECT l.value, t.name FROM adempiere.AD_Ref_List AS l ");
			sql.append("INNER JOIN adempiere.AD_Ref_List_Trl t ON l.AD_Ref_List_ID = t.AD_Ref_List_ID  ");
			sql.append("WHERE l.AD_Reference_ID = 149  AND l.isactive='Y' AND t.AD_Language = '" + lang + "' ");
			sql.append("ORDER BY t.name ASC ");
		}

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			Vector<Object> line = new Vector<Object>();
			while (rs.next()) {
				line = new Vector<Object>();
				String value = rs.getString("value");
				if (!value.equals("X")) {
					line.add(value);
					line.add(rs.getString("name"));
					data.add(line);
				}
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getCredictCardTypeList - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getTenderTypeList

	/**
	 * regresa la intancia del producto por instancia y localizacion - returns
	 * M_attributeSetInstance_ID by product and locator
	 * 
	 * @param locatorId
	 * @param productId
	 * @return
	 */
	public static Integer getAttInstanceId(Integer locatorId, Integer productId) {
		StringBuffer sql = new StringBuffer();

		sql.append(
				" SELECT s.M_Product_ID, s.M_attributeSetInstance_ID, SUM(s.qtyonhand) AS qty, p.name AS Product, s.M_Locator_ID, l.M_Warehouse_ID, w.name as warehouse, ");
		sql.append(
				" p.value  FROM M_Storage s, M_Product p, M_Locator l, M_Warehouse w  WHERE p.M_Product_ID = s.M_Product_ID  AND s.M_attributeSetInstance_ID > 0 ");
		sql.append(
				" AND s.M_Locator_ID = l.M_Locator_ID  AND w.M_Warehouse_ID = l.M_Warehouse_ID AND s.qtyonhand > 0 ");
		sql.append(" AND s.M_Product_ID = " + productId + " AND s.M_Locator_ID = " + locatorId + " ");
		sql.append(
				" GROUP BY s.M_Product_ID, s.M_attributeSetInstance_ID, p.name, s.M_Locator_ID,l.M_Warehouse_ID, w.name, p.value , s.created ");
		sql.append(" ORDER BY s.created ASC ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_attributeSetInstance_ID");
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getAttInstanceId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getAttInstanceId

	/**
	 * regresa el codigo de la orden para la linea de la orden - returns orderId
	 * by Order Line
	 * 
	 * @param value
	 * @return Integer
	 */
	public static Integer getOrderId(String trxName, Integer value) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT C_Order_ID FROM C_OrderLine WHERE C_OrderLine_ID = " + value + " ");
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("C_Order_ID");
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getOrderId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getOrderId

	/**
	 * regresa el precio de producto para la linea de la orden correspondiente a
	 * la instancia de conjunto de atributos returns product price of the Order
	 * Line for the specific atrubite set instance id
	 * 
	 * @param attribute
	 *            set instance id
	 * @return BigDecimal
	 */
	public static BigDecimal getPriceOrderByASID(String trxName, Integer m_attributesetinstance_id) {
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT PriceActual FROM c_orderline  WHERE c_orderline_id = ");
		sql.append("(SELECT c_orderline_id FROM M_MatchPO WHERE m_attributesetinstance_id = "
				+ m_attributesetinstance_id + ") ");
		//sql.append("SELECT PriceActual FROM C_OrderLine WHERE M_AttributeSetInstance_ID = " + m_attributesetinstance_id);
		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("priceactual");
			} // if
			log.log(Level.SEVERE, DataQueries.class.getName() + ". getPriceOrderByASID - " + sql.toString());

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ". getPriceOrderByASID - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPriceOrderByASID

	/**
	 * regresa la instancia de conjunto de atributos (id) dado el id de la linea
	 * de movimiento de inventario returns the specific atrubite set instance id
	 * for a mmovement line id
	 * 
	 * @param mmovment
	 *            line id
	 * @return integer
	 */
	public static Integer getASIDByMovementLineId(String trxName, Integer m_movementline_id) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_AttributeSetInstance_ID FROM M_MovementLineMA WHERE M_MovementLine_ID  = " + m_movementline_id);
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("m_attributesetinstance_id");
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ". getASIDByMovementLineId- " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getASIDByMovementLineId

	/**
	 * regresa la combinacion contable de la cta y organizacion especificadas
	 * returns the accoutn combination for the account id and orgid
	 * 
	 * @param accountId
	 *            and org Id
	 * @return MAccount
	 */
	public static MAccount getCombiAccountByAccountOrgSchema(String trxName, Integer account_id, Integer orgId,
			Integer schemaId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT c_validcombination_id FROM c_validcombination WHERE account_id = " + account_id + " AND ");
		sql.append(" ad_org_id = " + orgId + " AND c_acctschema_id=" + schemaId);
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("c_validcombination_id");
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ". getPriceOrderByASID - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		MAccount m1 = new MAccount(Env.getCtx(), 0, trxName);
		if (code > 0) {
			m1 = new MAccount(Env.getCtx(), code, trxName);
		} else // no existia se crea y retorna
		{
			m1.setAccount_ID(account_id);
			m1.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			m1.setC_AcctSchema_ID(schemaId);
			m1.save();
		}

		return m1;
	}// getPriceOrderByASID

	/**
	 * regresa la cantidad total de las recepciones para el producto y la
	 * recepcion - returns total qty by product and reception
	 * 
	 * @param productId
	 * @param inOutId
	 * @return BigDecimal
	 */
	public static BigDecimal getQtyTotalInOuts(String trxName, Integer productId, Integer inOutId, Integer ioLineId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT SUM(QtyEntered) AS total FROM M_InOutLine ");
		sql.append(" WHERE M_InOut_ID IN (SELECT M_InOut_ID FROM M_InOut WHERE C_Order_ID = ");
		sql.append(" (SELECT C_Order_ID FROM M_InOut WHERE M_InOut_ID = " + inOutId + ") ) ");
		sql.append(" AND  M_InOutLine_ID <> " + ioLineId + " ");
		sql.append(" AND isActive = 'Y' AND M_Product_ID = " + productId + " ");
		PreparedStatement pstmt = null;
		BigDecimal value = Env.ZERO;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getBigDecimal("total");
			if (value == null)
				value = Env.ZERO;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getQtyTotalInOuts - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getQtyTotalInOuts

	/**
	 * regresa la lista de lineas de recepcion de producto - return list of
	 * lines by InOut
	 * 
	 * @param value
	 * @param trxName
	 * @return
	 */
	public static LinkedList<MInOutLine> getInOutLines(Integer value, String trxName) {
		LinkedList<MInOutLine> data = new LinkedList<MInOutLine>();
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_InOutLine_ID FROM M_InOutLine ");
		sql.append(" WHERE M_InOut_ID = " + value + " ");
		sql.append(" AND isactive = 'Y' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				MInOutLine line = new MInOutLine(Env.getCtx(), rs.getInt("M_InOutLine_ID"), trxName);
				data.add(line);
			}
			if (data.size() <= 0) {
				data = null;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getInOutLines - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getInOutLines

	/**
	 * Returns the order id, if the vendor id has a purchase order id with the
	 * POS description no existe retorna 0
	 * 
	 * @param description
	 * @param vendorId
	 * @param documentType
	 * @param clientId
	 * @param orgId
	 * @return
	 */
	public static Integer getCurrentPurchaseOrderPOS(String trxName, String description, Integer vendorId,
			Integer documentType, Integer clientId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_order_id FROM c_order WHERE  TRIM(description) = '" + description.trim() + "' ");
		sql.append(" AND c_bpartner_id = " + vendorId + "  AND c_doctypetarget_id = " + documentType + " ");
		sql.append(" AND ad_client_id = " + clientId + "  AND isactive = 'Y' AND docstatus = 'DR' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer orderId = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				orderId = rs.getInt("c_order_id");

			} // if rs
		} // try
		catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getCurrentPurchaseOrderPOS - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return orderId;
	}// getCurrentPurchaseOrderPOS

	/**
	 * regresa el codigo del propietario del almacen - returns owner from
	 * warehouse
	 * 
	 * @param nit
	 * @return
	 */
	public static Integer getwarehouseOwner(Integer warehouseId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_BPartner_ID FROM M_Warehouse WHERE M_Warehouse_ID = " + warehouseId);
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("C_BPartner_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getwarehouseOwner - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getwarehouseOwner

	/**
	 * regresa el listado de pagos para completar la orden de autorizacion por
	 * cheque - return payment list to complete order by check autorization
	 * 
	 * @param trxName
	 * @param code
	 * @return LinkedList<MSMJTmpPayment>
	 */
	public static LinkedList<MSMJTmpPayment> getTmpPayment(String trxName, Integer code) {
		LinkedList<MSMJTmpPayment> data = new LinkedList<MSMJTmpPayment>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SMJ_TmpPayment_ID FROM SMJ_TmpPayment WHERE C_Order_ID = " + code);
		// System.out.println(DataQueries.class.getName()+".getTmpPayment
		// SQL::"+sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			MSMJTmpPayment line = null;
			while (rs.next()) {
				Integer id = rs.getInt("SMJ_TmpPayment_ID");
				line = new MSMJTmpPayment(Env.getCtx(), id, trxName);

				data.add(line);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTmpPayment - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getTmpPayment

	/**
	 * regresa el id de la actidad - returns activity id
	 * 
	 * @param trxName
	 * @param tableId
	 * @param recordId
	 * @return
	 */
	public static Integer getActivityId(String trxName, Integer tableId, Integer recordId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT ad_wf_activity_ID FROM ad_wf_activity WHERE ad_table_Id = " + tableId);
		sql.append(" AND record_id = " + recordId);
		PreparedStatement pstmt = null;
		Integer value = 0;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("ad_wf_activity_ID");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getActivityId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getActivityId

	/**
	 * Check if we have stock of all the BOM products for this product
	 * 
	 * @param prodId
	 * @param m_warehouse_id
	 * @param trx_name
	 * @return String (empty if valid stock for all the BOM products) or returns
	 *         the first product withut enought stock
	 */
	public static String validStockBOMProducts(MProduct prod, String trx_name, Integer m_warehouse_id,
			BigDecimal qtyRequired) {
		BigDecimal totq = Env.ZERO;
		String noStock = "";
		MProductBOM[] bomLines = MProductBOM.getBOMLines(prod); // get BOM
																// products
																// array

		for (int j = 0; j < bomLines.length; j++) {
			BigDecimal currentStock = DataQueries.getTotalProductByWarehouse(
					bomLines[j].getM_ProductBOM().getM_Product_ID(), m_warehouse_id, trx_name);
			BigDecimal currentUsedStock = DataQueries.getTotalTempProductByWarehouse(trx_name,
					bomLines[j].getM_ProductBOM().getM_Product_ID(), m_warehouse_id).subtract(totq);
			currentStock = currentStock.subtract(currentUsedStock);
			BigDecimal totBOMQtyRequired = qtyRequired.multiply(bomLines[j].getBOMQty());
			if (currentStock.compareTo(totBOMQtyRequired) == -1) { // current
																	// stock
																	// less than
																	// qtyrequired
				noStock = bomLines[j].getM_ProductBOM().getName(); // returns
																	// the name
																	// of the
																	// products
																	// without
																	// enough
																	// stock
				break;
			}
		}
		return noStock;
	}

	/**
	 * regresa la cantidad total del inventario por almacen y producto - return
	 * total qty by warehouse and product
	 * 
	 * @param productId
	 * @param warehouseId
	 * @return BigDecimal
	 */
	public static BigDecimal getTotalProductByWarehouse(Integer productId, Integer warehouseId, String trxName) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(s.qtyonhand) AS total ");
		sql.append("FROM M_Storage s, M_Product p WHERE p.M_Product_ID = s.M_Product_ID ");
		sql.append("AND  s.M_Product_ID = " + productId + " ");
		sql.append("AND s.M_Locator_ID IN ");
		sql.append("(SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = " + warehouseId + ")");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("total");
			}

			if (code == null) {
				code = Env.ZERO;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTotalProductByLocator - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTotalProductByPartner

	/**
	 * Consulta la cantidad total usada del producto que se encuentra en la
	 * tabla temporal
	 * 
	 * @param trxName
	 * @param productId
	 * @param warehouseId
	 * @return
	 */
	public static BigDecimal getTotalTempProductByWarehouse(String trxName, Integer productId, Integer warehouseId) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT SUM(qtyordered) FROM smj_tmpwebsalesline ");
		sql.append("WHERE m_product_id = " + productId + " AND m_warehouse_id = " + warehouseId);

		Integer value = DB.getSQLValue(trxName, sql.toString());
		BigDecimal qty = Env.ZERO;

		if (value != null) {
			qty = new BigDecimal(value);
		}

		return qty;
	}

	public static BigDecimal getTotalTempProductByLocator(int M_Product_ID, int M_Locator_ID, String trxName) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT SUM(qtyordered) FROM smj_tmpwebsalesline ");
		sql.append("WHERE m_product_id = " + M_Product_ID + " AND m_locator_id = " + M_Locator_ID);

		BigDecimal value = DB.getSQLValueBDEx(trxName, sql.toString());
		BigDecimal qty = Env.ZERO;

		if (value != null) {
			qty = value;
		}

		return qty;
	}
	
	/**
	 * Metodo para consultar las localizaciones, que puedan suplir con la
	 * cantidad del producto.
	 * 
	 * @param productId
	 * @param qty
	 * @param warehouseId
	 * @return
	 */
	public static List<Map<String, Object>> getLocatorsWithStock(MOrderLine oLine,
			Map<Integer, BigDecimal> usedLocators) throws Exception {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT s.m_locator_id, s.qtyonhand, s.m_attributesetinstance_id ");
		sql.append("FROM M_Storage s, M_Product p WHERE p.M_Product_ID = s.M_Product_ID ");
		sql.append("AND  s.M_Product_ID = " + oLine.getM_Product_ID() + " ");
		sql.append("AND (s.qtyonhand - s.qtyreserved) > 0 ");
		sql.append("AND s.M_Locator_ID IN ");
		sql.append("(SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = " + oLine.getM_Warehouse_ID() + ") ");
		sql.append("ORDER BY s.datematerialpolicy, s.qtyonhand DESC");
		// System.out.println("getLocatorWithStock SQL -> \n" + sql);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BigDecimal qtyTmp = Env.ZERO;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Map<String, Object> row = new HashMap<String, Object>();
				int M_AttributeSetInstance_ID = rs.getInt("m_attributesetinstance_id");
				BigDecimal qtyOnHand = rs.getBigDecimal("qtyonhand");

				// If there are lots in use, subtract the quantity used to the
				// quantity on hand
				if (oLine.getProduct().getM_AttributeSet_ID() > 0) {
					if (usedLocators != null && usedLocators.containsKey(M_AttributeSetInstance_ID)) {
						qtyOnHand = qtyOnHand.subtract(usedLocators.get(M_AttributeSetInstance_ID));

						// Verifies if there are still quantity on hand
						if (qtyOnHand.signum() < 1) {
							continue;
						}
					}
				} else {
					if (usedLocators != null && usedLocators.containsKey(oLine.getM_Product_ID())) {
						qtyOnHand = qtyOnHand.subtract(usedLocators.get(oLine.getM_Product_ID()));

						// Verifies if there are still quantity on hand
						if (qtyOnHand.signum() < 1) {
							continue;
						}
					}
				}

				qtyTmp = qtyTmp.add(qtyOnHand);

				row.put("m_locator_id", rs.getInt("m_locator_id"));
				row.put("m_attributesetinstance_id", M_AttributeSetInstance_ID);
				row.put("qtyonhand", qtyOnHand);

				list.add(row);

				if (qtyTmp.compareTo(oLine.getQtyOrdered()) >= 0) {
					break;
				}
			}

			// Show error
			if (qtyTmp.compareTo(oLine.getQtyOrdered()) < 0 && oLine.getM_Product_ID() > 0) {
				log.log(Level.SEVERE, DataQueries.class.getName() + ".getLocatorsWithStock - " + sql.toString());
				throw new AdempiereException("Insufficent Quantity");
			}
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return list;
	}

	/**
	 * valida exista el tercero por producto - validate exist partner by
	 * producto
	 * 
	 * @param trxName
	 * @param productId
	 * @param parnerId
	 * @return Boolean
	 */
	public static Boolean validateProductParnerExist(String trxName, Integer productId, Integer parnerId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT count(1) AS total FROM C_BPartner_Product ");
		sql.append(" WHERE M_Product_ID = " + productId + " ");
		sql.append(" AND C_BPartner_ID = " + parnerId + " ");

		Boolean flag = true;
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("total");
			} // if
			flag = code > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".validateProductParnerExist - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return flag;
	}// validateProductParnerExist

	/**
	 * actualiza el total para C_BPartner_Product - update total by
	 * C_BPartner_Product
	 * 
	 * @param trxName
	 * @param productId
	 * @param parnerId
	 * @param total
	 * @return
	 */
	public static Boolean updateProductParner(String trxName, Integer productId, Integer parnerId, BigDecimal total) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE C_BPartner_Product SET smj_total = " + total + " ");
		sql.append(" WHERE M_Product_ID = " + productId + " ");
		sql.append(" AND C_BPartner_ID = " + parnerId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getName()
							+ "updateProductParner - Error update C_BPartner_Product State:: Product -" + productId
							+ " - Partner -" + parnerId,
					e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateProductParner

	/**
	 * regresa la lista de estados returns state list
	 * 
	 * @param valueId
	 * @return Vector<Vector<Object>>
	 */
	public static Vector<Vector<Object>> getStateList(Integer valueId, Integer state) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT R_Status_ID, name FROM R_Status ");
		sql.append(" WHERE isactive = 'Y' AND R_StatusCategory_ID = " + valueId);
		sql.append(" AND R_Status_ID <> " + state);
		sql.append(" ORDER BY name ASC ");
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			Vector<Object> line = new Vector<Object>();
			while (rs.next()) {
				line = new Vector<Object>();
				line.add(rs.getInt("R_Status_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getCanonicalName() + ".getStateList - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getStateList

	/**
	 * regresa la lista terceros y productos de una entrega returns partner ando
	 * products by M_InOut_ID
	 * 
	 * @param valueId
	 * @return LinkedList<HashMap<String, Integer>>
	 */
	public static LinkedList<HashMap<String, Integer>> getPartnerProductList(String trxName, Integer valueId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT DISTINCT C_BPartner_ID, M_Product_ID  FROM C_BPartner_Product WHERE M_Product_ID IN ( ");
		sql.append(" SELECT distinct M_Product_ID FROM M_InOutLine  ");
		sql.append(" WHERE M_InOut_ID = " + valueId + ") ");
		LinkedList<HashMap<String, Integer>> data = new LinkedList<HashMap<String, Integer>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				HashMap<String, Integer> codes = new HashMap<String, Integer>();
				codes.put("PARTNER", rs.getInt("C_BPartner_ID"));
				codes.put("PRODUCT", rs.getInt("M_Product_ID"));
				data.add(codes);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getCanonicalName() + ".getPartnerProductList - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getPartnerProductList

	/**
	 * regresa el id del almacen por codigo de almacen - return warehouse id by
	 * warehouse value and client id
	 * 
	 * @param value
	 * @param clientId
	 * @return Integer
	 */
	public static Integer getWarehouseIdbyCode(String value, Integer clientId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Warehouse_ID FROM M_Warehouse WHERE TRIM(value) = '" + value.trim() + "' ");
		sql.append(" AND AD_Client_ID = " + clientId + " ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Warehouse_ID");
			} // if
			if (code == null || code <= 0) {
				code = 0;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getWarehouseIdbyCode - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getWarehouseIdbyCode

	/**
	 * regresa el id del almacen por cliente y organizacion - return warehouse
	 * id by client id and org id
	 * 
	 * @param clientId
	 * @return ArrayList<Integer>
	 */
	public static HashMap<Integer, String> getAllWarehouse(Integer clientId, Integer orgId) {
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT M_Warehouse_ID, name FROM M_Warehouse WHERE ");
		sql.append("AD_Client_ID = " + clientId + " AND AD_Org_ID = " + orgId);

		HashMap<Integer, String> listWarehouse = new HashMap<Integer, String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				listWarehouse.put(rs.getInt("M_Warehouse_ID"), rs.getString("name"));
			} // if
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getWarehouseIdbyClient - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return listWarehouse;
	}// getWarehouseIdbyCode

	/**
	 * regresa la localizacion del almacen por codigo de localizacion - returns
	 * the locator by warehouse and locator value
	 * 
	 * @param value
	 * @param locatorCode
	 * @return Integer
	 */
	public static Integer getLocatorIdbyCode(Integer value, String locatorCode) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = " + value + " ");
		sql.append(" AND TRIM(value) = '" + locatorCode.trim() + "' ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Locator_ID");
			} // if
			if (code == null || code <= 0) {
				code = getLocatorIdFirst(value);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getLocatorIdbyCode - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getLocatorIdbyCode

	/**
	 * valida que el usuario logueado es representante de ventas - validate
	 * loggued is sales rep
	 * 
	 * @param salesRepId
	 * @return
	 */
	public static Boolean isSalesRep(Integer salesRepId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT p.issalesrep FROM AD_User u, C_BPartner p ");
		sql.append(" WHERE u.C_BPartner_ID = p.C_BPartner_ID AND AD_User_ID = " + salesRepId + " ");

		Boolean salesRep = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			String value = "";
			if (rs.next()) {
				value = rs.getString("issalesrep");
			} // while
			salesRep = value.equals("Y") ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".isSalesRep - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return salesRep;
	}// isSalesRep

	/**
	 * regresa el almacen del cual el tercero es dueno - returns warehouse that
	 * partner is owner
	 * 
	 * @param bpartnerId
	 * @return
	 */
	public static Integer getWarehouseByOwner(Integer bpartnerId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Warehouse_ID FROM M_Warehouse ");
		sql.append(" WHERE isActive = 'Y' AND C_BPartner_ID = " + bpartnerId + " ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer value = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				value = rs.getInt("M_Warehouse_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getWarehouseByOwner - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getWarehouseByOwner

	/**
	 * regresa la entrega de origen de la devolucion - returns initial io from
	 * devolucion io
	 * 
	 * @param ioReturnId
	 * @return Integer
	 */
	public static Integer getIODelivery(Integer ioReturnId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT InOut_ID FROM M_RMA WHERE M_RMA_ID = ( ");
		sql.append(" SELECT M_RMA_ID FROM M_InOut WHERE M_InOut_ID = " + ioReturnId + ") ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer value = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				value = rs.getInt("InOut_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getIODelivery - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getIODelivery

	/**
	 * regresa linea de orden de una entrega por una linea de devolucion-
	 * returns order line from io line by devolucion io line
	 * 
	 * @param ioReturnId
	 * @return Integer
	 */
	public static Integer getIOOrderLine(Integer ioReturnLineId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_OrderLine_ID FROM M_InOutLine WHERE M_InOutLine_ID = (");
		sql.append(" SELECT M_InOutLine_ID FROM M_RMALine WHERE M_RMALine_ID = ( ");
		sql.append(" SELECT M_RMALine_ID FROM M_InOutLine WHERE M_InOutLine_ID = " + ioReturnLineId + ") ) ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer value = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				value = rs.getInt("C_OrderLine_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getIOOrderLine - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getIOOrderLine

	/**
	 * 
	 * 
	 * /*** regresa el precio de compra - return purchase price.
	 * 
	 * @param trxName
	 * @param p
	 * @param bparnerId
	 * @param warehouseId
	 * @return
	 */
	public static BigDecimal getPurchasePrice(String trxName, MProduct p, Integer bparnerId, Integer locatorId) {

		// precios desde listas configuradas
		MProductPrice pp = MProductPrice.get(Env.getCtx(), purchaseListVersionId, p.get_ID(), null);

		BigDecimal purchasePrice = Env.ZERO;
		if (pp != null) {
			purchasePrice = pp.getPriceStd();
		}
		return purchasePrice;
	}// getPurchasePrice
	
	/***
	 * regresa el precio de compra - Transporte
	 * return purchase price.
	 * @param trxName
	 * @param p
	 * @param bparnerId
	 * @param warehouseId
	 * @return
	 */
	public static BigDecimal getPurchasePriceTrans(String trxName, MProduct p, Integer bparnerId, Integer locatorId){
		Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
		
		MLocator loc = MLocator.get(Env.getCtx(), locatorId);
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
		Integer warehouseOwnerId = 0;
		Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
		if (isConsigment){
			warehouseOwnerId  = warehouse.get_ValueAsInt("C_BPartner_ID");
		}
		HashMap<String, Integer> codes = DataQueries.getPListVersion(trxName, p, bparnerId, tiresCategory, locatorId, warehouseOwnerId, null);
		MProductPrice pp = null;
		if(codes != null){
			pp = MProductPrice.get(Env.getCtx(), codes.get("PURCHASE"), p.getM_Product_ID(), trxName);
		}
		BigDecimal purchasePrice = Env.ZERO;
		if (pp != null){
			purchasePrice = pp.getPriceStd();
		}
		return purchasePrice;
	}//getPurchasePrice

	/**
	 * regresa la linea de entrega correspondiente a la linea de la orden -
	 * returns io line by order line
	 * 
	 * @param value
	 * @param trxName
	 * @return
	 */
	public static Integer getInOutLineByOrderLine(Integer value, String trxName) {
		Integer data = 0;
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_InOutLine_ID FROM M_InOutLine WHERE C_OrderLine_ID = " + value);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				data = rs.getInt("M_InOutLine_ID");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getInOutLineByOrderLine - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getInOutLineByOrderLine

	/**
	 * regresa el impuesto por una categoria de impuesto - returns tax by tax
	 * Category
	 * 
	 * @param taxCategory
	 * @param isDefault
	 * @return
	 */
	public static Integer getTaxIdByCategory(Integer taxCategory, Boolean isDefault) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_Tax_ID FROM C_Tax ");
		sql.append(" WHERE C_TaxCategory_ID = " + taxCategory + " ");
		if (isDefault) {
			sql.append(" AND isDefault = 'Y' ");
		}
		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("C_Tax_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getTaxIdByCategory - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTaxIdByCategory

	/**
	 * valida que la entrega sea la que reversa el movimiento - validate io is
	 * reversal to movement
	 * 
	 * @param ioId
	 * @return
	 */
	public static Boolean validateReversalIO(String trxName, Integer ioId) {
		Boolean flag = false;

		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(QtyEntered) AS total FROM M_InOutLine  ");
		sql.append(" WHERE M_InOut_ID = " + ioId + " ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BigDecimal tot = Env.ZERO;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				tot = rs.getBigDecimal("total");
			} // while
			if (tot.compareTo(Env.ZERO) < 0) {
				flag = true;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".validateReversalIO - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return flag;
	}// validateReversalIO

	/**
	 * regresa el moviento por la descripcion - returns Movement by description
	 * 
	 * @param trxName
	 * @param desc
	 * @return
	 */
	public static Integer getMovementByDesc(String trxName, String desc) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT M_Movement_ID FROM M_Movement ");
		sql.append(" WHERE description LIKE '" + desc + "%' ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("M_Movement_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getMovementByDesc - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getMovementByDesc

	/**
	 * actualizar id de factura en la solicitud - update request with in invoice
	 * id
	 * 
	 * @param trxName
	 * @param requestId
	 * @param invoiceId
	 * @return Boolean
	 */
	public static Boolean updateRequestInvoice(String trxName, Integer requestId, Integer invoiceId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE R_Request SET C_Invoice_ID = " + invoiceId + " ");
		sql.append(" WHERE R_Request_ID= " + requestId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getName() + "updateRequestInvoice - Error update Request" + requestId, e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateRequestInvoice

	/**
	 * regresa el id de la solucitud por numero de documento - returns
	 * R_Request_ID by documentno
	 * 
	 * @param trxName
	 * @param doc
	 * @return Integer
	 */
	public static Integer getRequestIdByDocument(String trxName, String doc) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT R_Request_ID FROM R_Request ");
		sql.append(" WHERE TRIM(documentno) = TRIM('" + doc.trim() + "') ");

		Integer code = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("R_Request_ID");
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getRequestIdByDocument - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getRequestIdByDocument

	/**
	 * regresa el telefono del tercero - returns bpartner phone
	 * 
	 * @param trxName
	 * @param bpartnerId
	 * @return
	 */
	public static String getPhoneByBPartnerId(String trxName, Integer bpartnerId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT phone, phone2 FROM C_BPartner_Location ");
		sql.append(" WHERE C_BPartner_ID = " + bpartnerId + " ");

		String code = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				String p1 = rs.getString("phone");
				String p2 = rs.getString("phone2");
				if ((p1 != null && p1.length() > 0) && (p2 != null && p2.length() > 0)) {
					code = p1 + " - " + p2;
				} else if (p1 != null && p1.length() > 0) {
					code = p1;
				} else if (p2 != null && p2.length() > 0) {
					code = p2;
				}
			} // if
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPhoneByBPartnerId - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPhoneByBPartnerId

	/**
	 * regresa el numero del pago por orden - returns payment document by
	 * orderId
	 * 
	 * @param trxName
	 * @param orderId
	 * @return String
	 */
	public static String getPaymentByOrder(String trxName, Integer orderId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT documentno FROM C_Payment ");
		sql.append(" WHERE C_Invoice_ID=(SELECT C_Invoice_ID FROM C_Invoice ");
		sql.append(" WHERE C_Order_ID= " + orderId + " ) ");

		String code = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getString("documentno");
			} // if
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getPaymentByOrder - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getPaymentByOrder

	/**
	 * actualiza la descripcion de la orden - update description order
	 * 
	 * @param trxName
	 * @param orderId
	 * @param desc
	 * @return
	 */
	public static Boolean updateOrderDescription(String trxName, Integer orderId, String desc) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE C_Order SET description = '" + desc + "' ");
		sql.append(" WHERE C_Order_ID=" + orderId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateOrderDescription - ERROR::" + e.getMessage(),
					e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateOrderDescription

	/**
	 * actualiza la centena de la linea de combustible - update hundred by fuel
	 * line
	 * 
	 * @param trxName
	 * @param fuelLineId
	 * @param hundred
	 * @return
	 */
	public static Boolean updateFuelLine(String trxName, Integer fuelLineId, BigDecimal hundred) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE smj_FuelLine SET smj_RChangeFuelLine_ID = null, actualhundred =" + hundred + " ");
		sql.append(" WHERE smj_FuelLine_ID = " + fuelLineId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateFuelLine - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateFuelLine

	/**
	 * regresa el id del la relacion si existe una relacion previamente creada -
	 * returns smj_vehicleOwner_ID by vehicle and relationship if exists
	 * 
	 * @param trxName
	 * @param vehicleId
	 * @param relation
	 * @param vehicleOwnerID
	 * @return LinkedList<Integer>
	 */
	public static LinkedList<Integer> getVehicleOwnerRelation(String trxName, Integer vehicleId, String relation,
			Integer vehicleOwnerID) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT smj_vehicleOwner_ID FROM smj_vehicleOwner  ");
		sql.append(" WHERE relationship = '" + relation + "' ");
		sql.append(" AND smj_vehicleOwner_ID <> " + vehicleOwnerID + " ");
		sql.append(" AND smj_vehicle_ID = " + vehicleId + "  ");
		LinkedList<Integer> codes = new LinkedList<Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Integer code = rs.getInt("smj_vehicleOwner_ID");
				codes.add(code);
			} // if

		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getVehicleOwnerRelation - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return codes;
	}// getVehicleOwnerRelation

	/**
	 * actualiza la fecha de fin de
	 * 
	 * @param trxName
	 * @param vehicleOwnerID
	 * @param endDate
	 * @param starDate
	 * @return
	 */
	public static Boolean updateVehicleOwnerEnd(String trxName, Integer vehicleOwnerID, Date endDate, Date starDate) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE smj_vehicleOwner SET enddate ='" + endDate + "' ");
		if (starDate != null) {
			sql.append(" , startdate = '" + starDate + "' ");
		}
		sql.append(" WHERE smj_vehicleOwner_ID = " + vehicleOwnerID + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateVehicleOwnerEnd - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateFuelLine

	/**
	 * regresa la cantidad de registros en el rango de fecha para el vehiculo -
	 * returns record count by dates and vehicle
	 * 
	 * @param vehicleId
	 * @param relationship
	 * @param vehicleOwnerID
	 * @param startDate
	 * @param endDate
	 * @return Integer
	 */
	public static Integer getValidateDatesOwner(Integer vehicleId, String relationship, String vehicleOwnerID,
			Date startDate, Date endDate) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT count(1) as TOTAL FROM smj_vehicleOwner WHERE smj_vehicle_ID= " + vehicleId + " ");
		sql.append(
				" AND relationship = '" + relationship + "' AND smj_vehicleOwner_ID NOT IN (" + vehicleOwnerID + ") ");
		sql.append(" AND ('" + startDate + "' BETWEEN startdate AND endDate OR '" + endDate
				+ "' BETWEEN startdate AND endDate");
		sql.append(" OR startdate BETWEEN '" + startDate + "' AND '" + endDate + "' ");
		sql.append(" OR endDate BETWEEN '" + startDate + "' AND '" + endDate + "' ) ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer code = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("TOTAL");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getValidateDatesOwner :: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getValidateDatesOwner

	/**
	 * actualiza el pago en el adelanto - update payment in advance
	 * 
	 * @param trxName
	 * @param advanceId
	 * @param paymentId
	 * @return Boolean
	 */
	public static Boolean updateAdvance(String trxName, Integer advanceId, Integer paymentId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE smj_controlAdvances SET C_Payment_ID=" + paymentId + " ");
		sql.append(" WHERE smj_controlAdvances_ID = " + advanceId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateAdvance - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateAdvance

	/**
	 * actualiza la factura en cargo - update invoice in charge
	 * 
	 * @param trxName
	 * @param advanceId
	 * @param paymentId
	 * @return Boolean
	 */
	public static Boolean updateCharge(String trxName, Integer chargeId, Integer invoiceId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE smj_charges SET C_Invoice_ID=" + invoiceId + " ");
		sql.append(" WHERE smj_charges_ID = " + chargeId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateCharge - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateAdvance

	/**
	 * valida si el cargo tiene adelantos creados - validate if charges has
	 * advances
	 * 
	 * @param trxName
	 * @param chargeId
	 * @return Boolean
	 */
	public static Boolean validateChargeWithAdvances(String trxName, Integer chargeId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT count(1) AS TOTAL FROM smj_controlAdvances WHERE C_Payment_ID IS NOT NULL ");
		sql.append(" AND smj_charges_ID= " + chargeId + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer code = 0;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("TOTAL");
			}
			flag = code > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getName() + ".validateChargeWithAdvances - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// validateChargeWithAdvances

	/**
	 * 
	 * @param trxName
	 * @param rrequestId
	 * @return
	 */
	public static Boolean updateRequestUpdVh(String trxName, Integer rrequestId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE R_Request SET smj_isupdatevehicle='N' ");
		sql.append(" WHERE R_Request_ID = " + rrequestId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateRequestUpdVh - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateRequestUpdVh

	/**
	 * actualiza precio de la linea de la orden - update price line order
	 * 
	 * @param trxName
	 * @param orderLineId
	 * @param price
	 * @param total
	 * @return
	 */
	public static Boolean updateOrderLinePrice(String trxName, Integer orderLineId, BigDecimal price,
			BigDecimal total) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE C_OrderLine SET PriceList = " + price + " ,priceentered = " + price + " ");
		sql.append(" , PriceActual =" + price + " ,linenetamt = " + total + " ");
		sql.append(" WHERE C_OrderLine_ID= " + orderLineId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateOrderLinePrice - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateOrderLinePrice

	/**
	 * actualiza el precio de la orden - update order total price
	 * 
	 * @param trxName
	 * @param orderId
	 * @param total
	 * @return
	 */
	public static Boolean updateOrderTotalPrice(String trxName, Integer orderId, BigDecimal total) {
		StringBuffer sql = new StringBuffer();

		sql.append(" UPDATE C_Order SET totalLines = " + total + " , grandTotal = " + total + " ");
		sql.append(" WHERE C_Order_ID = " + orderId + " ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".updateOrderTotalPrice - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// updateOrderTotalPrice

	/**
	 * regresa el adelanto que tienen asociado el pago - returns advance with
	 * payment asociated
	 * 
	 * @param trxName
	 * @param codeId
	 * @return Integer
	 */
	public static Integer getControlAdByPayment(String trxName, Integer codeId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT smj_controlAdvances_ID FROM smj_controlAdvances ");
		sql.append(" WHERE C_Payment_ID = " + codeId + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer code = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getInt("smj_controlAdvances_ID");
			}
			// flag = code>0?true:false;
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getControlAdByPayment - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return code;
	}// getControlAdByPayment

	/**
	 * regresa el total de lineas de la factura origen dado el id del RMA-
	 * returns the total of lines for the source invoice of the rma
	 * 
	 * @param trxName
	 * @param rmaId
	 * @returnInteger
	 */
	public static Integer getTotalLinesSourceInvoiceByRMAId(String trxName, Integer rmaId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" select totallines from c_invoice where C_Order_ID="
				+ "(select C_Order_ID from M_INOUT where M_INOUT_ID=" + "(select INOUT_ID from m_rma where M_RMA_ID = "
				+ rmaId + " )) ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer data = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				data = rs.getInt("totallines");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getName() + ".getTotalLinesSourceInvoiceByRMAId - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return data;
	}// getTotalLinesSourceInvoiceByRMAId

	/**
	 * regresa el id de la la factura origen dado el id del RMA- returns source
	 * invoice id of the rma
	 * 
	 * @param trxName
	 * @param rmaId
	 * @returnInteger
	 */
	public static Integer getInvoiceIdByRMAId(String trxName, Integer rmaId) {
		StringBuffer sql = new StringBuffer();

		sql.append(" select c_invoice_id from c_invoice where C_Order_ID="
				+ "(select C_Order_ID from M_INOUT where M_INOUT_ID=" + "(select INOUT_ID from m_rma where M_RMA_ID = "
				+ rmaId + " )) ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer data = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				data = rs.getInt("c_invoice_id");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getInvoiceIdByRMAId - ERROR::" + e.getMessage(), e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return data;
	}// getInvoiceIdByRMAId

	/**
	 * actualizar id de factura de la solicitud - update (clean with null) the
	 * invoice id of the request
	 * 
	 * @param trxName
	 * @param invoiceId
	 * @return Boolean
	 */
	public static Boolean cleanRequestInvoice(String trxName, Integer requestId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE R_Request set c_invoice_id = null ");
		sql.append(" WHERE r_request_id= " + requestId + " ");
		log.log(Level.SEVERE, "query to remove the invoice for the requestId:" + sql);

		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeUpdate();
			flag = rs > 0 ? true : false;
		} catch (Exception e) {
			log.log(Level.SEVERE,
					DataQueries.class.getName() + "cleanRequestInvoice- Error update with request" + requestId, e);
		} finally {
			DB.close(pstmt);
			rs = null;
			pstmt = null;
		}

		return flag;
	}// cleanRequestInvoiceByInvoiceId

	/**
	 * regresa el id de la nota contable por doc number returns GL journal Id by
	 * doc number
	 * 
	 * @param name
	 * @return
	 */
	public static Integer getGLJournalByDocNumber(String docNumber, int clientId) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT gl_journal_id FROM gl_journal WHERE isactive = 'Y' AND AD_Client_Id = ");
		sql.append(clientId + " AND documentno = '" + docNumber.trim() + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("gl_journal_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getGLJournalByDocNumber- " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getGLJournalByDocNumber

	/**
	 * regresa el id del proyecto por nombre - returns activity Id by name
	 * 
	 * @param name
	 * @return
	 */
	public static Integer getProjectId(String name, int clientId) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT C_Project_id FROM C_Project WHERE isactive = 'Y'  AND AD_Client_Id =  ");
		sql.append(clientId + " AND name = '" + name + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("C_Project_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getProjectId- " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getProjectId

	/**
	 * regresa el id del la cta contable dado su codigo - returns element value
	 * by code = value
	 * 
	 * @param name
	 * @return
	 */
	public static Integer getElementValueId(String name, int clientId) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_elementvalue_id FROM c_elementvalue WHERE isactive = 'Y'  AND AD_Client_Id =  ");
		sql.append(clientId + " AND value = '" + name + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("c_elementvalue_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getElementValueId- " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getElementValueId

	/**
	 * regresa el id del tipo de document dado su nombre - returns document type
	 * by name
	 * 
	 * @param name
	 * @return
	 */
	public static Integer getDocumentTypeId(String name, int clientId) {
		Integer value = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT c_doctype_id FROM c_doctype WHERE isactive = 'Y'  AND AD_Client_Id =  ");
		sql.append(clientId + " AND name = '" + name + "' ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next())
				value = rs.getInt("c_doctype_id");
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getDocumentTypeId- " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;

	}// getDocumentTypeId

	/**
	 * Obtener todos los tipos de devolucion, con el filtro de cliente y
	 * orgnanizacion
	 * 
	 * @return
	 */
	public static LinkedList<X_M_RMAType> getAllRMATypes(String nameTrx) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT m_rmatype_id FROM m_rmatype ");
		sql.append("WHERE isactive = 'Y' AND ad_client_id = " + lclientId + " AND ad_org_id = " + lorgId);
		LinkedList<X_M_RMAType> lstRmaType = new LinkedList<X_M_RMAType>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				lstRmaType.add(new X_M_RMAType(Env.getCtx(), rs.getInt("m_rmatype_id"), nameTrx));
			}
		} catch (Exception e) {
			lstRmaType = null;
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return lstRmaType;
	}

	public static LinkedList<LinkedList<Object>> getRMALinesByInOutID(Integer inOutID) {
		LinkedList<LinkedList<Object>> data = new LinkedList<LinkedList<Object>>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT rl.M_InOutLine_ID, SUM(rl.qty) as qty FROM M_RMALine rl ");
		sql.append("JOIN M_RMA r ON r.M_RMA_ID = rl.M_RMA_ID ");
		sql.append("WHERE r.M_RMA_ID IN (SELECT M_RMA_ID FROM M_RMA WHERE InOut_ID = " + inOutID + ") ");
		sql.append("GROUP BY rl.M_InOutLine_ID");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				LinkedList<Object> row = new LinkedList<Object>();
				row.add(rs.getInt("M_InOutLine_ID"));
				row.add(rs.getBigDecimal("qty"));
				data.add(row);
			} // while
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName() + ".getRMALinesByInOutID - " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return data;
	}

	/**
	 * Traer el documento (Entrada/Salida) con el id de la orden
	 * 
	 * @param orderId
	 * @param trx
	 * @return
	 */
	public static MInOut getInOutByOrderId(Integer orderId, String trx) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT m_inout_id FROM m_inout WHERE c_order_id = " + orderId + " ");
		sql.append("AND isactive = 'Y' AND ad_client_id = " + lclientId + " AND ad_org_id = " + lorgId);

		MInOut mInOut = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				mInOut = new MInOut(Env.getCtx(), rs.getInt("m_inout_id"), trx);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return mInOut;
	}

	/**
	 * Get warehouse id by org id regresa el codigo del almacen a partir de la
	 * organizacion
	 * 
	 * @param code
	 * @return
	 */
	public static Integer getWarehouse(Integer code) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT M_Warehouse_ID FROM M_Warehouse WHERE ad_org_id = " + code + " ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Integer items = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				items = rs.getInt("M_Warehouse_ID");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return items;
	}// getWarehouse

	/**
	 * Elimina las lineas de Web Sales que no han sido enviadas
	 * 
	 * @param trxName
	 * @param clientId
	 * @param orgId
	 * @return
	 */
	public static Integer deleteWebSalesLine(String trxName, Integer clientId, Integer orgId) throws Exception {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		StringBuffer sql = new StringBuffer();

		sql.append("DELETE FROM C_OrderLine ");
		sql.append("WHERE C_Order_ID IN (");
		sql.append("SELECT t.C_Order_ID FROM SMJ_TmpWebSales t ");
		sql.append("WHERE t.SMJ_TmpWebSales_ID NOT IN (SELECT DISTINCT smj_tmpwebsales_id FROM smj_tmpwebsalesline ");
		sql.append("WHERE smj_isworkorder = 'Y' AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + ") ");
		sql.append(
				" AND t.C_Order_ID NOT IN (SELECT C_OrderLine_ID FROM C_InvoiceLine WHERE C_OrderLine_ID IS NOT NULL)) ");
		sql.append("AND M_Warehouse_ID = " + M_Warehouse_ID);

		Integer rows = DB.executeUpdateEx(sql.toString(), trxName);

		sql = new StringBuffer();
		sql.append("DELETE FROM smj_tmpwebsalesline ");
		sql.append("WHERE smj_tmpwebsales_id NOT IN ");
		sql.append("(SELECT DISTINCT smj_tmpwebsales_id FROM smj_tmpwebsalesline ");
		sql.append("WHERE smj_isworkorder = 'Y' AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + ") ");
		sql.append(
				"AND C_OrderLine_ID NOT IN (SELECT C_OrderLine_ID FROM C_InvoiceLine WHERE C_OrderLine_ID IS NOT NULL) ");
		sql.append("AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + " AND ");
		sql.append("SMJ_TmpWebSales_ID IN (SELECT t.SMJ_TmpWebSales_ID FROM SMJ_TmpWebSales t ");
		sql.append("JOIN C_Order o ON (o.C_Order_ID = t.C_Order_ID)	WHERE o.M_Warehouse_ID = " + M_Warehouse_ID + ")");

		rows += DB.executeUpdateEx(sql.toString(), trxName);

		return rows;
	}

	/**
	 * Elimina los registros Web Sales que no contengan lineas enviadas
	 * 
	 * @param trxName
	 * @param clientId
	 * @param orgId
	 * @return
	 */
	public static Integer deleteWebSalesAndOrders(String trxName, Integer clientId, Integer orgId) throws Exception {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		StringBuffer sql = new StringBuffer();

		sql.append("DELETE FROM C_Order ");
		sql.append("WHERE C_Order_ID IN (");
		sql.append("SELECT t.C_Order_ID FROM SMJ_TmpWebSalesLine l ");
		sql.append("JOIN SMJ_TmpWebSales t ON (t.SMJ_TmpWebSales_ID = l.SMJ_TmpWebSales_ID) ");
		sql.append("WHERE t.SMJ_TmpWebSales_ID NOT IN ");
		sql.append("(SELECT DISTINCT smj_tmpwebsales_id FROM smj_tmpwebsalesline WHERE smj_isworkorder = 'Y' ");
		sql.append("AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + ") ");
		sql.append("AND C_OrderLine_ID NOT IN ");
		sql.append("(SELECT C_OrderLine_ID FROM C_InvoiceLine WHERE C_OrderLine_ID IS NOT NULL)) ");
		sql.append("AND M_Warehouse_ID = " + M_Warehouse_ID);

		Integer rows = DB.executeUpdateEx(sql.toString(), trxName);

		sql = new StringBuffer();
		sql.append("DELETE FROM smj_tmpwebsales ");
		sql.append("WHERE smj_tmpwebsales_id NOT IN ");
		sql.append("(SELECT DISTINCT smj_tmpwebsales_id FROM smj_tmpwebsalesline ");
		sql.append("WHERE smj_isworkorder = 'Y' AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + " ");
		sql.append("AND C_OrderLine_ID NOT IN ");
		sql.append("(SELECT C_OrderLine_ID FROM C_InvoiceLine WHERE C_OrderLine_ID IS NOT NULL)) ");
		sql.append("AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId + " AND ");
		sql.append("SMJ_TmpWebSales_ID IN (SELECT t.SMJ_TmpWebSales_ID FROM SMJ_TmpWebSales t ");
		sql.append("JOIN C_Order o ON (o.C_Order_ID = t.C_Order_ID)	WHERE o.M_Warehouse_ID = " + M_Warehouse_ID + ")");

		rows += DB.executeUpdateEx(sql.toString(), trxName);

		return rows;
	}

	/**
	 * consulta los id's de los registros Web Sales que tienen lineas enviadas
	 * 
	 * @param clientId
	 * @param orgId
	 * @return
	 */
	public static ArrayList<Integer> getSendedWebSalesId(Integer clientId, Integer orgId) {
		ArrayList<Integer> list = new ArrayList<Integer>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT smj_tmpwebsales_id FROM smj_tmpwebsalesline ");
		sql.append("WHERE smj_isworkorder = 'Y' AND ad_org_Id = " + orgId + " AND ad_client_id = " + clientId);

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				list.add(rs.getInt("smj_tmpwebsales_id"));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return list;
	}

	/**
	 * Get Total Points By Business Partner
	 * 
	 * @param C_BPartner_ID
	 * @return
	 */
	public static BigDecimal getPointsByBPartnerID(int C_BPartner_ID) {
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT COALESCE(SUM(points), 0) AS points FROM (")
				.append("SELECT SUM(points) AS points FROM SMJ_Lty_Transaction WHERE (transaction_type IN ('A','B')) AND C_BPartner_ID = "
						+ C_BPartner_ID)
				.append(" UNION ")
				.append("SELECT SUM(points * -1) FROM SMJ_Lty_Transaction WHERE (transaction_type IN ('C','D')) AND C_BPartner_ID = "
						+ C_BPartner_ID)
				.append(" UNION ")
				.append("SELECT SUM(points) FROM SMJ_Lty_Transaction WHERE (transaction_type = 'D') AND Beneficiary_ID = "
						+ C_BPartner_ID)
				.append(") AS x");

		// System.out.println("SQL getPointsByPartnerID() -> " + sql);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BigDecimal points = Env.ZERO;

		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				points = rs.getBigDecimal("points");
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, sql.toString(), ex);
		} finally {
			DB.close(rs, pstmt);
		}

		return points;
	}

	/**
	 * Validate if Payment was generate automaticallyd
	 * 
	 * @param C_Invoice_ID
	 * @param trxName
	 * @return
	 */
	public static boolean existsPayment(int C_Invoice_ID, String trxName) {
		boolean state = true;

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C_AllocationLine_ID FROM C_AllocationLine WHERE C_Invoice_ID = ?");
		state = DB.getSQLValue(trxName, sql.toString(), C_Invoice_ID) > 0;

		return state;
	}

	/**
	 * Get column id with the columnName and tableName.
	 * 
	 * @param columnName
	 * @param tableName
	 * @return first value or -1 if not found
	 * @throws DBException
	 */
	public static int getColumnID(String columnName, String tableName) throws DBException {
		StringBuffer sql = new StringBuffer("SELECT AD_Column_ID FROM AD_Column c ")
				.append("JOIN AD_Table t ON (t.AD_Table_ID = c.AD_Table_ID) ")
				.append("WHERE UPPER(t.TableName) = UPPER(?) AND UPPER(c.ColumnName) = UPPER(?)");

		return DB.getSQLValueEx(null, sql.toString(), tableName, columnName);
	}

	/**
	 * regresa el tenedor del vehiculo por relacion returns vehicle tender by
	 * relationship
	 * 
	 * @param plate
	 * @return
	 */
	public static int getVehicleTender(String plate, String code) {
		int value = 0;
		StringBuffer sql = new StringBuffer("SELECT C_BPartner_ID FROM smj_vehicleOwner o , smj_vehicle v WHERE o.isactive = 'Y' ")
				.append("AND v.smj_vehicle_id = o.smj_vehicle_id AND o.enddate IS NULL AND smj_plate = ? ");
		
		if (code != null) {
			sql.append("AND relationship = '" + code + "' ");
		}
		
		value = DB.getSQLValueEx(null, sql.toString(), plate);
		
		//Se verifica si se obtuvo un Tenedor de lo contrario, se consultara por la fecha de arranque mas reciente.
		if (value < 1 && code != null) {
			sql = new StringBuffer("SELECT C_BPartner_ID FROM smj_vehicleOwner o , smj_vehicle v WHERE o.isactive = 'Y' ")
					.append("AND v.smj_vehicle_id = o.smj_vehicle_id AND o.enddate IS NOT NULL AND smj_plate = ? ");
			
			if (code != null) {
				sql.append(" AND relationship = '" + code + "'  ORDER BY o.startdate DESC ");
			}

			value = DB.getSQLValueEx(null, sql.toString(), plate);
		}
		
		return value;
	}// getVehicleTender
	
	/**
	 * get all plates by bpartner
	 * @param c_bpartner_id
	 * @return
	 */
	public static List<KeyNamePair> getPlatesByBPartner(int c_bpartner_id, String workOrders) {
		StringBuffer sql = new StringBuffer("SELECT DISTINCT v.SMJ_Vehicle_ID, v.SMJ_Plate FROM SMJ_Vehicle v ")
				.append("JOIN SMJ_VehicleOwner vo ON (vo.SMJ_Vehicle_ID = v.SMJ_Vehicle_ID) ")
				.append("WHERE vo.C_BPartner_ID = ? ");
		sql.append("AND relationship='1' ");	//Adicion iMarch
		if (!workOrders.isEmpty()) {
			sql.append(" OR SMJ_Plate IN (SELECT SMJ_Plate FROM R_Request WHERE R_Request_ID IN (" + workOrders + ")) ")
				.append(" ORDER BY v.SMJ_Plate");
		} else {
			sql.append("ORDER BY v.SMJ_Plate");
		}
		
		KeyNamePair[] pairs = DB.getKeyNamePairs(sql.toString(), true, c_bpartner_id);
		
		return Arrays.asList(pairs);
	}
	
	//----------iMARCH--------------NUEVA CONSULTA--------------------------------------------------------
		/**
		 * regresa valor si en bodega de consignacion
		 * 
		 * @param bparnerId
		 * @return value
		 */
		public static String getLocatorConsignment(Integer LocatorId) {
			StringBuffer sql = new StringBuffer();
				
			sql.append(" SELECT w.smj_isconsignment FROM M_Locator l, M_Warehouse w");
			sql.append(" WHERE w.M_Warehouse_ID=l.M_Warehouse_ID");
			sql.append(" AND l.M_Locator_ID = " + LocatorId + " ");

			String Consig = "";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				pstmt = DB.prepareStatement(sql.toString(), null);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					Consig = rs.getString("smj_isconsignment");
				}// if
			} catch (Exception e) {
				log.log(Level.SEVERE, DataQueries.class.getName()+".getLocatorConsignment - "+sql.toString(), e);
			} finally {
				DB.close(rs, pstmt);
				rs = null;
				pstmt = null;
			}
			return Consig;
		}// getLocatorConsignment
	//------------------------------------------------------------------------------------------------
		
	/**
	 * carga la lista de mecanicos asignados - 
	 * load mechanic list
	 * @param code
	 */
	public static LinkedList<MSMJWoMechanicAssigned> getMechanicList(Integer code){
		LinkedList<MSMJWoMechanicAssigned> listMec = new LinkedList<MSMJWoMechanicAssigned>();
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT smj_WoMechanicAssigned_ID FROM smj_WoMechanicAssigned WHERE smj_workOrderLine_ID =  "+code+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			MSMJWoMechanicAssigned line = null;
			while (rs.next ())	{
				Integer id = rs.getInt("smj_WoMechanicAssigned_ID");
				line = new MSMJWoMechanicAssigned(Env.getCtx(),id,null);
				listMec.add(line);
			}//while
		}catch (Exception e){
			log.log(Level.SEVERE, DataQueries.class.getName()+ ".getMechanicList - SQL: " + sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return listMec;
	}//getMechanicList
	
	/**
	 * regresa la lista de las comisiones aplicadas al mecanico por la linea de la orden de trabajo - 
	 * retuns mechanic Commission list by work order line 
	 * @param code
	 * @return
	 */
	public static LinkedList<MSMJMechanicCommission> getMechanicCommisionList(Integer code){
		LinkedList<MSMJMechanicCommission> listMec = new LinkedList<MSMJMechanicCommission>();
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT smj_MechanicCommission_ID FROM smj_MechanicCommission ");
			sql.append(" WHERE  total > 0 AND smj_workOrderLine_ID =  "+code+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			MSMJMechanicCommission line = null;
			while (rs.next ())	{
				Integer id = rs.getInt("smj_MechanicCommission_ID");
				line = new MSMJMechanicCommission(Env.getCtx(),id,null);
				listMec.add(line);
			}//while
		}catch (Exception e){
			log.log(Level.SEVERE, DataQueries.class.getName()+ ".getMechanicCommisionList - SQL: " + sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return listMec;
	}//getMechanicCommisionList
	
	/**
	 * regresa el tipo de  solicitud dado su id 
	 * returns request type of this requestif
	 * @param trxName
	 * @param invoiceId
	 * @param remittanceId
	 * @returnInteger
	 */
	public static Integer getRequestTypeByRequestId(String trxName, Integer requestId ) {
		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT r_requesttype_id FROM R_Request WHERE R_Request_ID= "+requestId+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer data = 0;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				data = rs.getInt("r_requesttype_id");
			}
		}catch (Exception e)	{
			log.log (Level.SEVERE, DataQueries.class.getName()+".getRequestTypeByRequestId - ERROR::"+e.getMessage() , e);
		}
		finally	{
			DB.close(pstmt);
			rs = null; pstmt = null;
		}
		
		return data;
	}//getRequestTypeByRequestId
	
	public static Vector<Vector<Object>> getPayTermListTrans(Integer clientId, Integer payterm, Integer paytermDef, Integer bpartnerId){
		Integer payInternal = Integer.parseInt(MSysConfig.getValue("SMJ-PAYMENTTERMINTERNAL",Env.getAD_Client_ID(Env.getCtx())).trim());
		Integer internalUser = Integer.parseInt(MSysConfig.getValue("SMJ-USERINTERNALPAY","0",Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim());
		String terms = "";
		if (payterm > 0 && !(payterm.equals(paytermDef))){
			terms = paytermDef.toString()+","+payterm.toString();
		}else{
			terms = paytermDef.toString();
		}
		if (bpartnerId != null && bpartnerId.equals(internalUser)){
			//terms = terms+","+payInternal;
			terms = "'"+payInternal+"'";
		}
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT C_PaymentTerm_ID, name FROM C_PaymentTerm WHERE isActive = 'Y' AND AD_Client_ID = "+clientId);
			sql.append(" AND C_PaymentTerm_ID in ("+terms+") ORDER BY name ASC ");
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("C_PaymentTerm_ID"));
				line.add(rs.getString("name"));
				data.add(line);
			}//while
		}catch (Exception e){
			log.log(Level.SEVERE, DataQueries.class.getName()+".getPayTermList - "+sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getPayTermListTrans
	
	/**
	 * regresa la cantidad total del inventario por localizacion de almacen y producto - 
	 * return total qty by locator and product
	 * 
	 * @param productId
	 * @return Integer
	 */
	public static BigDecimal getTotalProductByLocator(Integer productId,
			Integer locatorId, String trxName) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(s.qtyonhand) AS total ");
		sql.append(" FROM M_Storage s, M_Product p WHERE p.M_Product_ID = s.M_Product_ID ");
		sql.append(" AND s.M_attributeSetInstance_ID > 0 AND  s.M_Product_ID = "+ productId + " ");
		sql.append(" AND s.M_Locator_ID = "+locatorId+" ");

		BigDecimal code = Env.ZERO;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				code = rs.getBigDecimal("total");
			}// while
			if (code == null){
				code = Env.ZERO;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, DataQueries.class.getName()+".getTotalProductByLocator - "+sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return code;
	}// getTotalProductByPartner
	
	/**
	 * regresa las lineas de la orden de trabajo
	 * retuns work order lines
	 * @param code
	 * @return LinkedList<MSMJWorkOrderLine>
	 */
	public static LinkedList<MSMJWorkOrderLine> getWOLines(Integer code){
		LinkedList<MSMJWorkOrderLine> data = new LinkedList<MSMJWorkOrderLine>();
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT smj_workOrderLine_ID FROM smj_workOrderLine t WHERE  R_Request_ID = "+code+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			MSMJWorkOrderLine line = null;
			while (rs.next ())	{
				Integer id = rs.getInt("smj_workOrderLine_ID");
				line = new MSMJWorkOrderLine(Env.getCtx(),id,null);
				MProduct prod = MProduct.get(Env.getCtx(), line.getM_Product_ID());
				line.setProductValue(prod.getValue());
				line.setProductName(prod.getName());
				if(line.getlocator_ID()>0 ){
					MLocator loc = MLocator.get(Env.getCtx(), line.getlocator_ID());
					MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
					line.setWarehouseName(wh.getName());
				}
				data.add(line);
			}//while
		}catch (Exception e){
			log.log(Level.SEVERE, DataQueries.class.getName()+".getWOLines :: "+sql.toString()	, e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getWOLines
	
	/**iMarch 
	 * regresa el ID del impuesto del producto 
	 * dependiendo del tipo de documento (OV / OC)
	 * return tax id by product
	 * @param code, sales
	 * @return Integer
	 */
	public static Integer getProductTaxbyTrx(Integer code, Boolean sales){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT x.C_Tax_ID FROM M_Product p, C_Tax x  ");
		sql.append(" WHERE  p.C_TaxCategory_ID = x.C_TaxCategory_ID ");
		sql.append(" AND p.M_Product_ID =  "+code+" ");
		if(sales)
			sql.append(" AND x.sopotype IN ('S','B') ");
		else
			sql.append(" AND x.sopotype IN ('P','B') ");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		Integer ctax = 0;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			if (rs.next ()){
				ctax = rs.getInt("C_Tax_ID");
			}
		}catch (Exception e)	{
			log.log(Level.SEVERE, DataQueries.class.getName()+".getProductTax - "+sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return ctax;
	}//getProductTaxbyDocument
	
	public static int getWOByDocumentNo(String documentNo, int AD_Client_ID) throws DBException {
		String sql = "SELECT R_Request_ID FROM R_Request WHERE DocumentNo = ? AND AD_Client_ID = ?";
		return DB.getSQLValueEx(null, sql, documentNo, AD_Client_ID);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param C_InOut_ID
	 * @param trxName
	 * @return
	 */
	public static MInOutLine findLastInOutLine(Properties ctx, int C_InOut_ID, String trxName) {
		String whereClause = MInOut.COLUMNNAME_M_InOut_ID + " = ?";
		
		return new Query(ctx, MInOutLine.Table_Name, whereClause, trxName)
				.setParameters(C_InOut_ID)
				.first();
	}
	
}// DataQueries