/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_tmpwebsalesline
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_smj_tmpwebsalesline 
{

    /** TableName=smj_tmpwebsalesline */
    public static final String Table_Name = "smj_tmpwebsalesline";

    /** AD_Table_ID=1000027 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_OrderLine_ID */
    public static final String COLUMNNAME_C_OrderLine_ID = "C_OrderLine_ID";

	/** Set Sales Order Line.
	  * Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID);

	/** Get Sales Order Line.
	  * Sales Order Line
	  */
	public int getC_OrderLine_ID();

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_AttributeSetInstance_ID */
    public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";

	/** Set Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID);

	/** Get Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException;

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_PriceList_Version_ID */
    public static final String COLUMNNAME_M_PriceList_Version_ID = "M_PriceList_Version_ID";

	/** Set Price List Version.
	  * Identifies a unique instance of a Price List
	  */
	public void setM_PriceList_Version_ID (int M_PriceList_Version_ID);

	/** Get Price List Version.
	  * Identifies a unique instance of a Price List
	  */
	public int getM_PriceList_Version_ID();

	public org.compiere.model.I_M_PriceList_Version getM_PriceList_Version() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name partnername */
    public static final String COLUMNNAME_partnername = "partnername";

	/** Set partnername	  */
	public void setpartnername (String partnername);

	/** Get partnername	  */
	public String getpartnername();

    /** Column name PriceActual */
    public static final String COLUMNNAME_PriceActual = "PriceActual";

	/** Set Unit Price.
	  * Actual Price 
	  */
	public void setPriceActual (BigDecimal PriceActual);

	/** Get Unit Price.
	  * Actual Price 
	  */
	public BigDecimal getPriceActual();

    /** Column name PriceEntered */
    public static final String COLUMNNAME_PriceEntered = "PriceEntered";

	/** Set Price.
	  * Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered);

	/** Get Price.
	  * Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered();

    /** Column name ProductName */
    public static final String COLUMNNAME_ProductName = "ProductName";

	/** Set Product Name.
	  * Name of the Product
	  */
	public void setProductName (String ProductName);

	/** Get Product Name.
	  * Name of the Product
	  */
	public String getProductName();

    /** Column name ProductValue */
    public static final String COLUMNNAME_ProductValue = "ProductValue";

	/** Set Product Key.
	  * Key of the Product
	  */
	public void setProductValue (String ProductValue);

	/** Get Product Key.
	  * Key of the Product
	  */
	public String getProductValue();

    /** Column name purchaseprice */
    public static final String COLUMNNAME_purchaseprice = "purchaseprice";

	/** Set Purchase Price	  */
	public void setpurchaseprice (BigDecimal purchaseprice);

	/** Get Purchase Price	  */
	public BigDecimal getpurchaseprice();

    /** Column name QtyEntered */
    public static final String COLUMNNAME_QtyEntered = "QtyEntered";

	/** Set Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered);

	/** Get Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered();

    /** Column name QtyOrdered */
    public static final String COLUMNNAME_QtyOrdered = "QtyOrdered";

	/** Set Ordered Quantity.
	  * Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered);

	/** Get Ordered Quantity.
	  * Ordered Quantity
	  */
	public BigDecimal getQtyOrdered();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException;

    /** Column name smj_destination */
    public static final String COLUMNNAME_smj_destination = "smj_destination";

	/** Set smj_destination	  */
	public void setsmj_destination (String smj_destination);

	/** Get smj_destination	  */
	public String getsmj_destination();

    /** Column name smj_discount */
    public static final String COLUMNNAME_smj_discount = "smj_discount";

	/** Set smj_discount	  */
	public void setsmj_discount (BigDecimal smj_discount);

	/** Get smj_discount	  */
	public BigDecimal getsmj_discount();

    /** Column name smj_iswarranty */
    public static final String COLUMNNAME_smj_iswarranty = "smj_iswarranty";

	/** Set smj_iswarranty	  */
	public void setsmj_iswarranty (String smj_iswarranty);

	/** Get smj_iswarranty	  */
	public String getsmj_iswarranty();

    /** Column name smj_iswarrantyapplied */
    public static final String COLUMNNAME_smj_iswarrantyapplied = "smj_iswarrantyapplied";

	/** Set smj_iswarrantyapplied	  */
	public void setsmj_iswarrantyapplied (String smj_iswarrantyapplied);

	/** Get smj_iswarrantyapplied	  */
	public String getsmj_iswarrantyapplied();

    /** Column name smj_isworkorder */
    public static final String COLUMNNAME_smj_isworkorder = "smj_isworkorder";

	/** Set smj_isworkorder	  */
	public void setsmj_isworkorder (String smj_isworkorder);

	/** Get smj_isworkorder	  */
	public String getsmj_isworkorder();

    /** Column name smj_notes */
    public static final String COLUMNNAME_smj_notes = "smj_notes";

	/** Set smj_notes	  */
	public void setsmj_notes (String smj_notes);

	/** Get smj_notes	  */
	public String getsmj_notes();

    /** Column name smj_percentage */
    public static final String COLUMNNAME_smj_percentage = "smj_percentage";

	/** Set smj_percentage	  */
	public void setsmj_percentage (boolean smj_percentage);

	/** Get smj_percentage	  */
	public boolean issmj_percentage();

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set smj_plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get smj_plate	  */
	public String getsmj_plate();

    /** Column name smj_tmpwebsales_ID */
    public static final String COLUMNNAME_smj_tmpwebsales_ID = "smj_tmpwebsales_ID";

	/** Set smj_tmpwebsales	  */
	public void setsmj_tmpwebsales_ID (int smj_tmpwebsales_ID);

	/** Get smj_tmpwebsales	  */
	public int getsmj_tmpwebsales_ID();

	public I_smj_tmpwebsales getsmj_tmpwebsales() throws RuntimeException;

    /** Column name smj_tmpwebsalesline_ID */
    public static final String COLUMNNAME_smj_tmpwebsalesline_ID = "smj_tmpwebsalesline_ID";

	/** Set smj_tmpwebsalesline	  */
	public void setsmj_tmpwebsalesline_ID (int smj_tmpwebsalesline_ID);

	/** Get smj_tmpwebsalesline	  */
	public int getsmj_tmpwebsalesline_ID();

    /** Column name smj_tmpwebsalesline_UU */
    public static final String COLUMNNAME_smj_tmpwebsalesline_UU = "smj_tmpwebsalesline_UU";

	/** Set smj_tmpwebsalesline_UU	  */
	public void setsmj_tmpwebsalesline_UU (String smj_tmpwebsalesline_UU);

	/** Get smj_tmpwebsalesline_UU	  */
	public String getsmj_tmpwebsalesline_UU();

    /** Column name tax */
    public static final String COLUMNNAME_tax = "tax";

	/** Set tax	  */
	public void settax (BigDecimal tax);

	/** Get tax	  */
	public BigDecimal gettax();

    /** Column name total */
    public static final String COLUMNNAME_total = "total";

	/** Set total	  */
	public void settotal (BigDecimal total);

	/** Get total	  */
	public BigDecimal gettotal();

    /** Column name UOMName */
    public static final String COLUMNNAME_UOMName = "UOMName";

	/** Set UOM Name	  */
	public void setUOMName (String UOMName);

	/** Get UOM Name	  */
	public String getUOMName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WarehouseName */
    public static final String COLUMNNAME_WarehouseName = "WarehouseName";

	/** Set Warehouse.
	  * Warehouse Name
	  */
	public void setWarehouseName (String WarehouseName);

	/** Get Warehouse.
	  * Warehouse Name
	  */
	public String getWarehouseName();
}
