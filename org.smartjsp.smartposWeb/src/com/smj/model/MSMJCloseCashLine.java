package com.smj.model;

import java.util.Properties;

public class MSMJCloseCashLine extends X_smj_closecashline {

	public MSMJCloseCashLine(Properties ctx, int smj_closecashline_ID,
			String trxName) {
		super(ctx, smj_closecashline_ID, trxName);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4915553743083298628L;

}
