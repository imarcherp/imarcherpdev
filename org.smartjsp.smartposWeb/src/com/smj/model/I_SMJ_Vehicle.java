/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for SMJ_Vehicle
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_SMJ_Vehicle 
{

    /** TableName=SMJ_Vehicle */
    public static final String Table_Name = "SMJ_Vehicle";

    /** AD_Table_ID=1000028 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name affiliatecompany_ID */
    public static final String COLUMNNAME_affiliatecompany_ID = "affiliatecompany_ID";

	/** Set Company	  */
	public void setaffiliatecompany_ID (int affiliatecompany_ID);

	/** Get Company	  */
	public int getaffiliatecompany_ID();

	public org.compiere.model.I_C_BPartner getaffiliatecompany() throws RuntimeException;

    /** Column name asociatedtrucktrailer_ID */
    public static final String COLUMNNAME_asociatedtrucktrailer_ID = "asociatedtrucktrailer_ID";

	/** Set Asociated Truck Trailer	  */
	public void setasociatedtrucktrailer_ID (int asociatedtrucktrailer_ID);

	/** Get Asociated Truck Trailer	  */
	public int getasociatedtrucktrailer_ID();

	public I_SMJ_Vehicle getasociatedtrucktrailer() throws RuntimeException;

    /** Column name axles */
    public static final String COLUMNNAME_axles = "axles";

	/** Set Axles Number	  */
	public void setaxles (String axles);

	/** Get Axles Number	  */
	public String getaxles();

    /** Column name bodylength */
    public static final String COLUMNNAME_bodylength = "bodylength";

	/** Set Body Length	  */
	public void setbodylength (BigDecimal bodylength);

	/** Get Body Length	  */
	public BigDecimal getbodylength();

    /** Column name C_PaymentTerm_ID */
    public static final String COLUMNNAME_C_PaymentTerm_ID = "C_PaymentTerm_ID";

	/** Set Payment Term.
	  * The terms of Payment (timing, discount)
	  */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID);

	/** Get Payment Term.
	  * The terms of Payment (timing, discount)
	  */
	public int getC_PaymentTerm_ID();

	public org.compiere.model.I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException;

    /** Column name capacitygallons */
    public static final String COLUMNNAME_capacitygallons = "capacitygallons";

	/** Set Capacity Gallons	  */
	public void setcapacitygallons (BigDecimal capacitygallons);

	/** Get Capacity Gallons	  */
	public BigDecimal getcapacitygallons();

    /** Column name capacitypassenger */
    public static final String COLUMNNAME_capacitypassenger = "capacitypassenger";

	/** Set Capacity Passenger	  */
	public void setcapacitypassenger (int capacitypassenger);

	/** Get Capacity Passenger	  */
	public int getcapacitypassenger();

    /** Column name capacitytons */
    public static final String COLUMNNAME_capacitytons = "capacitytons";

	/** Set Capacity Tons	  */
	public void setcapacitytons (BigDecimal capacitytons);

	/** Get Capacity Tons	  */
	public BigDecimal getcapacitytons();

    /** Column name cardoperation */
    public static final String COLUMNNAME_cardoperation = "cardoperation";

	/** Set Card Operation	  */
	public void setcardoperation (String cardoperation);

	/** Get Card Operation	  */
	public String getcardoperation();

    /** Column name cardoperationexpiration */
    public static final String COLUMNNAME_cardoperationexpiration = "cardoperationexpiration";

	/** Set Card Operation Expiration	  */
	public void setcardoperationexpiration (Timestamp cardoperationexpiration);

	/** Get Card Operation Expiration	  */
	public Timestamp getcardoperationexpiration();

    /** Column name chairsnumber */
    public static final String COLUMNNAME_chairsnumber = "chairsnumber";

	/** Set Chairs Number	  */
	public void setchairsnumber (int chairsnumber);

	/** Get Chairs Number	  */
	public int getchairsnumber();

    /** Column name chassisnumber */
    public static final String COLUMNNAME_chassisnumber = "chassisnumber";

	/** Set Chassis Number	  */
	public void setchassisnumber (String chassisnumber);

	/** Get Chassis Number	  */
	public String getchassisnumber();

    /** Column name color */
    public static final String COLUMNNAME_color = "color";

	/** Set Color	  */
	public void setcolor (String color);

	/** Get Color	  */
	public String getcolor();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name cylindercapacity */
    public static final String COLUMNNAME_cylindercapacity = "cylindercapacity";

	/** Set Cylinder Capacity	  */
	public void setcylindercapacity (BigDecimal cylindercapacity);

	/** Get Cylinder Capacity	  */
	public BigDecimal getcylindercapacity();

    /** Column name dateaffiliation */
    public static final String COLUMNNAME_dateaffiliation = "dateaffiliation";

	/** Set Date Affiliation	  */
	public void setdateaffiliation (Timestamp dateaffiliation);

	/** Get Date Affiliation	  */
	public Timestamp getdateaffiliation();

    /** Column name datedisaffiliation */
    public static final String COLUMNNAME_datedisaffiliation = "datedisaffiliation";

	/** Set Date Disaffiliation	  */
	public void setdatedisaffiliation (Timestamp datedisaffiliation);

	/** Get Date Disaffiliation	  */
	public Timestamp getdatedisaffiliation();

    /** Column name dateexpirationaffiliation */
    public static final String COLUMNNAME_dateexpirationaffiliation = "dateexpirationaffiliation";

	/** Set Date Expiration Affiliation	  */
	public void setdateexpirationaffiliation (Timestamp dateexpirationaffiliation);

	/** Get Date Expiration Affiliation	  */
	public Timestamp getdateexpirationaffiliation();

    /** Column name datelastmileage */
    public static final String COLUMNNAME_datelastmileage = "datelastmileage";

	/** Set Date Last Mileage	  */
	public void setdatelastmileage (Timestamp datelastmileage);

	/** Get Date Last Mileage	  */
	public Timestamp getdatelastmileage();

    /** Column name datelastprogramming */
    public static final String COLUMNNAME_datelastprogramming = "datelastprogramming";

	/** Set Date Last Programming	  */
	public void setdatelastprogramming (Timestamp datelastprogramming);

	/** Get Date Last Programming	  */
	public Timestamp getdatelastprogramming();

    /** Column name datetechnicalrevision */
    public static final String COLUMNNAME_datetechnicalrevision = "datetechnicalrevision";

	/** Set Date Technical Revision	  */
	public void setdatetechnicalrevision (Timestamp datetechnicalrevision);

	/** Get Date Technical Revision	  */
	public Timestamp getdatetechnicalrevision();

    /** Column name datetransitlicense */
    public static final String COLUMNNAME_datetransitlicense = "datetransitlicense";

	/** Set Date Transit License	  */
	public void setdatetransitlicense (Timestamp datetransitlicense);

	/** Get Date Transit License	  */
	public Timestamp getdatetransitlicense();

    /** Column name diagnosticcenter_ID */
    public static final String COLUMNNAME_diagnosticcenter_ID = "diagnosticcenter_ID";

	/** Set Giagnostic Center	  */
	public void setdiagnosticcenter_ID (int diagnosticcenter_ID);

	/** Get Giagnostic Center	  */
	public int getdiagnosticcenter_ID();

	public org.compiere.model.I_C_BPartner getdiagnosticcenter() throws RuntimeException;

    /** Column name extinguisherexpiration */
    public static final String COLUMNNAME_extinguisherexpiration = "extinguisherexpiration";

	/** Set Extinguisher Expiration	  */
	public void setextinguisherexpiration (Timestamp extinguisherexpiration);

	/** Get Extinguisher Expiration	  */
	public Timestamp getextinguisherexpiration();

    /** Column name grouprate */
    public static final String COLUMNNAME_grouprate = "grouprate";

	/** Set Group Rate	  */
	public void setgrouprate (String grouprate);

	/** Get Group Rate	  */
	public String getgrouprate();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name istrailer */
    public static final String COLUMNNAME_istrailer = "istrailer";

	/** Set Trailer	  */
	public void setistrailer (boolean istrailer);

	/** Get Trailer	  */
	public boolean istrailer();

    /** Column name istrucktractor */
    public static final String COLUMNNAME_istrucktractor = "istrucktractor";

	/** Set Truck Tractor	  */
	public void setistrucktractor (boolean istrucktractor);

	/** Get Truck Tractor	  */
	public boolean istrucktractor();

    /** Column name kms */
    public static final String COLUMNNAME_kms = "kms";

	/** Set Kms	  */
	public void setkms (BigDecimal kms);

	/** Get Kms	  */
	public BigDecimal getkms();

    /** Column name lastmileage */
    public static final String COLUMNNAME_lastmileage = "lastmileage";

	/** Set Last Mileage	  */
	public void setlastmileage (int lastmileage);

	/** Get Last Mileage	  */
	public int getlastmileage();

    /** Column name liabilitypolicy */
    public static final String COLUMNNAME_liabilitypolicy = "liabilitypolicy";

	/** Set Liability Policy	  */
	public void setliabilitypolicy (String liabilitypolicy);

	/** Get Liability Policy	  */
	public String getliabilitypolicy();

    /** Column name liabilitypolicyexpiration */
    public static final String COLUMNNAME_liabilitypolicyexpiration = "liabilitypolicyexpiration";

	/** Set Liability Policy Expiration	  */
	public void setliabilitypolicyexpiration (Timestamp liabilitypolicyexpiration);

	/** Get Liability Policy Expiration	  */
	public Timestamp getliabilitypolicyexpiration();

    /** Column name liabilitypolicyinsurer */
    public static final String COLUMNNAME_liabilitypolicyinsurer = "liabilitypolicyinsurer";

	/** Set Liability Policy Insurer	  */
	public void setliabilitypolicyinsurer (String liabilitypolicyinsurer);

	/** Get Liability Policy Insurer	  */
	public String getliabilitypolicyinsurer();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (String Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public String getLine();

    /** Column name model */
    public static final String COLUMNNAME_model = "model";

	/** Set Model	  */
	public void setmodel (int model);

	/** Get Model	  */
	public int getmodel();

    /** Column name motornumber */
    public static final String COLUMNNAME_motornumber = "motornumber";

	/** Set Motor Number	  */
	public void setmotornumber (String motornumber);

	/** Get Motor Number	  */
	public String getmotornumber();

    /** Column name odometer */
    public static final String COLUMNNAME_odometer = "odometer";

	/** Set Odometer	  */
	public void setodometer (String odometer);

	/** Get Odometer	  */
	public String getodometer();

    /** Column name oilproduct_ID */
    public static final String COLUMNNAME_oilproduct_ID = "oilproduct_ID";

	/** Set Motor Oil Product	  */
	public void setoilproduct_ID (int oilproduct_ID);

	/** Get Motor Oil Product	  */
	public int getoilproduct_ID();

	public org.compiere.model.I_M_Product getoilproduct() throws RuntimeException;

    /** Column name passengersseated */
    public static final String COLUMNNAME_passengersseated = "passengersseated";

	/** Set Passengers Seated	  */
	public void setpassengersseated (int passengersseated);

	/** Get Passengers Seated	  */
	public int getpassengersseated();

    /** Column name passengersstanding */
    public static final String COLUMNNAME_passengersstanding = "passengersstanding";

	/** Set Passengers Standing	  */
	public void setpassengersstanding (int passengersstanding);

	/** Get Passengers Standing	  */
	public int getpassengersstanding();

    /** Column name qtytires */
    public static final String COLUMNNAME_qtytires = "qtytires";

	/** Set Qty Tires	  */
	public void setqtytires (int qtytires);

	/** Get Qty Tires	  */
	public int getqtytires();

    /** Column name repoweredde */
    public static final String COLUMNNAME_repoweredde = "repoweredde";

	/** Set Repowered	  */
	public void setrepoweredde (String repoweredde);

	/** Get Repowered	  */
	public String getrepoweredde();

    /** Column name servicetype */
    public static final String COLUMNNAME_servicetype = "servicetype";

	/** Set Service Type	  */
	public void setservicetype (String servicetype);

	/** Get Service Type	  */
	public String getservicetype();

    /** Column name smj_bodytype_ID */
    public static final String COLUMNNAME_smj_bodytype_ID = "smj_bodytype_ID";

	/** Set Body Type	  */
	public void setsmj_bodytype_ID (int smj_bodytype_ID);

	/** Get Body Type	  */
	public int getsmj_bodytype_ID();

	public I_smj_bodytype getsmj_bodytype() throws RuntimeException;

    /** Column name smj_bodyworkbrand_ID */
    public static final String COLUMNNAME_smj_bodyworkbrand_ID = "smj_bodyworkbrand_ID";

	/** Set Bodywork Brand	  */
	public void setsmj_bodyworkbrand_ID (int smj_bodyworkbrand_ID);

	/** Get Bodywork Brand	  */
	public int getsmj_bodyworkbrand_ID();

	public I_smj_brand getsmj_bodyworkbrand() throws RuntimeException;

    /** Column name smj_gearboxbrand_ID */
    public static final String COLUMNNAME_smj_gearboxbrand_ID = "smj_gearboxbrand_ID";

	/** Set Gearbox Brand	  */
	public void setsmj_gearboxbrand_ID (int smj_gearboxbrand_ID);

	/** Get Gearbox Brand	  */
	public int getsmj_gearboxbrand_ID();

	public I_smj_brand getsmj_gearboxbrand() throws RuntimeException;

    /** Column name smj_internalnumber */
    public static final String COLUMNNAME_smj_internalnumber = "smj_internalnumber";

	/** Set Internal Number	  */
	public void setsmj_internalnumber (String smj_internalnumber);

	/** Get Internal Number	  */
	public String getsmj_internalnumber();

    /** Column name smj_motorbrand_ID */
    public static final String COLUMNNAME_smj_motorbrand_ID = "smj_motorbrand_ID";

	/** Set Motor Brand	  */
	public void setsmj_motorbrand_ID (int smj_motorbrand_ID);

	/** Get Motor Brand	  */
	public int getsmj_motorbrand_ID();

	public I_smj_brand getsmj_motorbrand() throws RuntimeException;

    /** Column name smj_plate */
    public static final String COLUMNNAME_smj_plate = "smj_plate";

	/** Set Plate	  */
	public void setsmj_plate (String smj_plate);

	/** Get Plate	  */
	public String getsmj_plate();

    /** Column name smj_programmingGroup_ID */
    public static final String COLUMNNAME_smj_programmingGroup_ID = "smj_programmingGroup_ID";

	/** Set Programming Group.
	  * Programming Group
	  */
	public void setsmj_programmingGroup_ID (int smj_programmingGroup_ID);

	/** Get Programming Group.
	  * Programming Group
	  */
	public int getsmj_programmingGroup_ID();

	public I_smj_programmingGroup getsmj_programmingGroup() throws RuntimeException;

    /** Column name smj_servicerategroup_ID */
    public static final String COLUMNNAME_smj_servicerategroup_ID = "smj_servicerategroup_ID";

	/** Set Service Rate Group	  */
	public void setsmj_servicerategroup_ID (int smj_servicerategroup_ID);

	/** Get Service Rate Group	  */
	public int getsmj_servicerategroup_ID();

	public I_smj_servicerategroup getsmj_servicerategroup() throws RuntimeException;

    /** Column name SMJ_Vehicle_ID */
    public static final String COLUMNNAME_SMJ_Vehicle_ID = "SMJ_Vehicle_ID";

	/** Set Vehicle	  */
	public void setSMJ_Vehicle_ID (int SMJ_Vehicle_ID);

	/** Get Vehicle	  */
	public int getSMJ_Vehicle_ID();

    /** Column name smj_vehiclebrand_ID */
    public static final String COLUMNNAME_smj_vehiclebrand_ID = "smj_vehiclebrand_ID";

	/** Set Vehicle Brand	  */
	public void setsmj_vehiclebrand_ID (int smj_vehiclebrand_ID);

	/** Get Vehicle Brand	  */
	public int getsmj_vehiclebrand_ID();

	public I_smj_brand getsmj_vehiclebrand() throws RuntimeException;

    /** Column name soatexpiration */
    public static final String COLUMNNAME_soatexpiration = "soatexpiration";

	/** Set Soat Expiration	  */
	public void setsoatexpiration (Timestamp soatexpiration);

	/** Get Soat Expiration	  */
	public Timestamp getsoatexpiration();

    /** Column name soatinsurer_ID */
    public static final String COLUMNNAME_soatinsurer_ID = "soatinsurer_ID";

	/** Set Soat Insurer	  */
	public void setsoatinsurer_ID (int soatinsurer_ID);

	/** Get Soat Insurer	  */
	public int getsoatinsurer_ID();

	public org.compiere.model.I_C_BPartner getsoatinsurer() throws RuntimeException;

    /** Column name soatnumber */
    public static final String COLUMNNAME_soatnumber = "soatnumber";

	/** Set Soat Number	  */
	public void setsoatnumber (String soatnumber);

	/** Get Soat Number	  */
	public String getsoatnumber();

    /** Column name technicalrevisionnumber */
    public static final String COLUMNNAME_technicalrevisionnumber = "technicalrevisionnumber";

	/** Set Technical Revision Number	  */
	public void settechnicalrevisionnumber (String technicalrevisionnumber);

	/** Get Technical Revision Number	  */
	public String gettechnicalrevisionnumber();

    /** Column name technicalrevisionnumberexpira */
    public static final String COLUMNNAME_technicalrevisionnumberexpira = "technicalrevisionnumberexpira";

	/** Set Technical Revision Expiration	  */
	public void settechnicalrevisionnumberexpira (Timestamp technicalrevisionnumberexpira);

	/** Get Technical Revision Expiration	  */
	public Timestamp gettechnicalrevisionnumberexpira();

    /** Column name tourniquet */
    public static final String COLUMNNAME_tourniquet = "tourniquet";

	/** Set Tourniquet	  */
	public void settourniquet (int tourniquet);

	/** Get Tourniquet	  */
	public int gettourniquet();

    /** Column name transitlicense */
    public static final String COLUMNNAME_transitlicense = "transitlicense";

	/** Set Transit License	  */
	public void settransitlicense (String transitlicense);

	/** Get Transit License	  */
	public String gettransitlicense();

    /** Column name typeadministration */
    public static final String COLUMNNAME_typeadministration = "typeadministration";

	/** Set Type Administration	  */
	public void settypeadministration (String typeadministration);

	/** Get Type Administration	  */
	public String gettypeadministration();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name vehicletype */
    public static final String COLUMNNAME_vehicletype = "vehicletype";

	/** Set Vehicle Type.
	  * Vehicle Type
	  */
	public void setvehicletype (String vehicletype);

	/** Get Vehicle Type.
	  * Vehicle Type
	  */
	public String getvehicletype();
}
