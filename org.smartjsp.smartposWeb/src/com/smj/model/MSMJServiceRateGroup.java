package com.smj.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MSMJServiceRateGroup extends X_smj_servicerategroup{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9119279579388899791L;

	/**
	 * MSMJServiceRateGroup constructor
	 * @param ctx
	 * @param smj_serviceRateGroup_ID
	 * @param trxName 
	 */
	public MSMJServiceRateGroup(Properties ctx, int smj_serviceRateGroup_ID,
			String trxName) {
		super(ctx, smj_serviceRateGroup_ID, trxName);
	}
	
	public MSMJServiceRateGroup(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}


}//MSMJServiceRateGroup
