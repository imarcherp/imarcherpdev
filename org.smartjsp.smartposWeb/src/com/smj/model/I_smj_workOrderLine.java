/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for smj_workOrderLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_smj_workOrderLine 
{

    /** TableName=smj_workOrderLine */
    public static final String Table_Name = "smj_workOrderLine";

    /** AD_Table_ID=1000042 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Invoice_ID */
    public static final String COLUMNNAME_C_Invoice_ID = "C_Invoice_ID";

	/** Set Invoice.
	  * Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID);

	/** Get Invoice.
	  * Invoice Identifier
	  */
	public int getC_Invoice_ID();

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException;

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name dateclose */
    public static final String COLUMNNAME_dateclose = "dateclose";

	/** Set Date Close	  */
	public void setdateclose (Timestamp dateclose);

	/** Get Date Close	  */
	public Timestamp getdateclose();

    /** Column name dateopen */
    public static final String COLUMNNAME_dateopen = "dateopen";

	/** Set Date Open	  */
	public void setdateopen (Timestamp dateopen);

	/** Get Date Open	  */
	public Timestamp getdateopen();

    /** Column name Duration */
    public static final String COLUMNNAME_Duration = "Duration";

	/** Set Duration.
	  * Normal Duration in Duration Unit
	  */
	public void setDuration (BigDecimal Duration);

	/** Get Duration.
	  * Normal Duration in Duration Unit
	  */
	public BigDecimal getDuration();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name kms */
    public static final String COLUMNNAME_kms = "kms";

	/** Set Kms	  */
	public void setkms (BigDecimal kms);

	/** Get Kms	  */
	public BigDecimal getkms();

    /** Column name locator_ID */
    public static final String COLUMNNAME_locator_ID = "locator_ID";

	/** Set Locator	  */
	public void setlocator_ID (int locator_ID);

	/** Get Locator	  */
	public int getlocator_ID();

    /** Column name M_AttributeSetInstance_ID */
    public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";

	/** Set Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID);

	/** Get Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name observations */
    public static final String COLUMNNAME_observations = "observations";

	/** Set Observations	  */
	public void setobservations (String observations);

	/** Get Observations	  */
	public String getobservations();

    /** Column name Price */
    public static final String COLUMNNAME_Price = "Price";

	/** Set Price.
	  * Price
	  */
	public void setPrice (BigDecimal Price);

	/** Get Price.
	  * Price
	  */
	public BigDecimal getPrice();

    /** Column name provider_ID */
    public static final String COLUMNNAME_provider_ID = "provider_ID";

	/** Set Provider	  */
	public void setprovider_ID (int provider_ID);

	/** Get Provider	  */
	public int getprovider_ID();

    /** Column name purchasePrice */
    public static final String COLUMNNAME_purchasePrice = "purchasePrice";

	/** Set Purchase Price	  */
	public void setpurchasePrice (BigDecimal purchasePrice);

	/** Get Purchase Price	  */
	public BigDecimal getpurchasePrice();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name R_Request_ID */
    public static final String COLUMNNAME_R_Request_ID = "R_Request_ID";

	/** Set Request.
	  * Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID);

	/** Get Request.
	  * Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID();

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException;

    /** Column name smj_estimatedtime */
    public static final String COLUMNNAME_smj_estimatedtime = "smj_estimatedtime";

	/** Set Estimated Time	  */
	public void setsmj_estimatedtime (BigDecimal smj_estimatedtime);

	/** Get Estimated Time	  */
	public BigDecimal getsmj_estimatedtime();

    /** Column name smj_isallowchanges */
    public static final String COLUMNNAME_smj_isallowchanges = "smj_isallowchanges";

	/** Set Allow Changes	  */
	public void setsmj_isallowchanges (boolean smj_isallowchanges);

	/** Get Allow Changes	  */
	public boolean issmj_isallowchanges();

    /** Column name smj_iscommission */
    public static final String COLUMNNAME_smj_iscommission = "smj_iscommission";

	/** Set Commission	  */
	public void setsmj_iscommission (boolean smj_iscommission);

	/** Get Commission	  */
	public boolean issmj_iscommission();

    /** Column name smj_isexternallservice */
    public static final String COLUMNNAME_smj_isexternallservice = "smj_isexternallservice";

	/** Set External Service	  */
	public void setsmj_isexternallservice (boolean smj_isexternallservice);

	/** Get External Service	  */
	public boolean issmj_isexternallservice();

    /** Column name smj_isservice */
    public static final String COLUMNNAME_smj_isservice = "smj_isservice";

	/** Set Is Service	  */
	public void setsmj_isservice (boolean smj_isservice);

	/** Get Is Service	  */
	public boolean issmj_isservice();

    /** Column name smj_iswarranty */
    public static final String COLUMNNAME_smj_iswarranty = "smj_iswarranty";

	/** Set Is Warranty	  */
	public void setsmj_iswarranty (boolean smj_iswarranty);

	/** Get Is Warranty	  */
	public boolean issmj_iswarranty();

    /** Column name smj_iswarrantyapplied */
    public static final String COLUMNNAME_smj_iswarrantyapplied = "smj_iswarrantyapplied";

	/** Set Is Warranty Applied	  */
	public void setsmj_iswarrantyapplied (boolean smj_iswarrantyapplied);

	/** Get Is Warranty Applied	  */
	public boolean issmj_iswarrantyapplied();

    /** Column name smj_workOrderLine_ID */
    public static final String COLUMNNAME_smj_workOrderLine_ID = "smj_workOrderLine_ID";

	/** Set Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID);

	/** Get Work Order Line	  */
	public int getsmj_workOrderLine_ID();

    /** Column name smj_workorderline_parent_ID */
    public static final String COLUMNNAME_smj_workorderline_parent_ID = "smj_workorderline_parent_ID";

	/** Set Work Order Line Parent	  */
	public void setsmj_workorderline_parent_ID (int smj_workorderline_parent_ID);

	/** Get Work Order Line Parent	  */
	public int getsmj_workorderline_parent_ID();

    /** Column name taxvalue */
    public static final String COLUMNNAME_taxvalue = "taxvalue";

	/** Set Tax Value	  */
	public void settaxvalue (BigDecimal taxvalue);

	/** Get Tax Value	  */
	public BigDecimal gettaxvalue();

    /** Column name total */
    public static final String COLUMNNAME_total = "total";

	/** Set Total	  */
	public void settotal (BigDecimal total);

	/** Get Total	  */
	public BigDecimal gettotal();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name wolstate */
    public static final String COLUMNNAME_wolstate = "wolstate";

	/** Set State Work Order Line	  */
	public void setwolstate (String wolstate);

	/** Get State Work Order Line	  */
	public String getwolstate();
}
