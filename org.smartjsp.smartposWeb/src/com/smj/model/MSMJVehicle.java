package com.smj.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MSMJVehicle extends X_SMJ_Vehicle {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8288164958278821722L;

	public MSMJVehicle(Properties ctx, int SMJ_Vehicle_ID, String trxName) {
		super(ctx, SMJ_Vehicle_ID, trxName);
	}
	
	public MSMJVehicle(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
}
