package com.smj.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.util.Env;

public class MSMJTmpWebSalesLine extends X_smj_tmpwebsalesline {
	
	/**
	 * MSMJTmpWebSalesLine constructor
	 */
	private static final long serialVersionUID = 1579531262107586328L;
	private Boolean useTransport = MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));

	public MSMJTmpWebSalesLine(Properties ctx, int smj_tmpWebSalesLine_ID, String trxName) {
		super(ctx, smj_tmpWebSalesLine_ID, trxName);
	}

	public MSMJTmpWebSalesLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	private String oldCode = "";

	public String getOldCode() {
		return oldCode;
	}

	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}

	@Override
	public boolean save() {
		calculateTotal();
		calculateQtyAndAmt();

		if (getM_Product_ID() > 0) {
			if (!persistOrderLine()) {
				return false;
			}
		}

		return super.save();
	}

	/**
	 * Ppdates the order line associate to current record
	 * @return
	 */
	private boolean persistOrderLine() {
		boolean state = false;
		
		if (getC_OrderLine_ID() > 0) {
			MOrderLine m_orderline = new MOrderLine(Env.getCtx(), getC_OrderLine_ID(), get_TrxName());
			BigDecimal discount = (issmj_percentage()) ? getPriceEntered().multiply(getsmj_discount().divide(Env.ONEHUNDRED)) : getsmj_discount();			
			BigDecimal price = getPriceEntered().subtract(discount);
			m_orderline.setC_UOM_ID(getC_UOM_ID());
			m_orderline.setQty(getQtyEntered());
			m_orderline.setQtyOrdered(getQtyOrdered());
			m_orderline.setPriceActual(getPriceActual());
			m_orderline.setPriceEntered(price);			
			state = m_orderline.save();
		
		
			if (useTransport) {				
				if (get_ValueAsInt("SMJ_Vehicle_ID") > 0) {
					MSMJVehicle vehicle = new MSMJVehicle(Env.getCtx(), get_ValueAsInt("SMJ_Vehicle_ID"), get_TrxName());
					m_orderline.set_ValueNoCheck("SMJ_PLate", vehicle.getsmj_plate());
				} else if (getsmj_plate() != null && !getsmj_plate().isEmpty()) {
					m_orderline.set_ValueNoCheck("SMJ_PLate", getsmj_plate());
				}
				
				if (get_ValueAsInt("SMJ_Vehicle_ID") < 1) {
					set_ValueNoCheck("SMJ_Vehicle_ID", null);
				}
				
				m_orderline.set_ValueNoCheck("SMJ_Locator_ID", getM_Locator_ID());
				m_orderline.set_ValueNoCheck("SMJ_WorkOrderLine_ID", get_ValueAsInt("SMJ_WorkOrderLine_ID"));
				m_orderline.set_ValueNoCheck("SMJ_Provider_ID", getC_BPartner_ID());
				
				//Tax from Product
				
				if (m_orderline.setTax()) {
					settax(m_orderline.getC_Tax().getRate());
				}
				
				state = m_orderline.save();
			}
		} 
		//MOrder order = new MOrder(Env.getCtx(), m_orderline.getC_Order_ID(), this.get_TrxName());
		//order.completeIt();
		//order.reActivateIt();
		
		return state;
	}

	private void calculateTotal() {
		if (getQtyEntered() == null || getPriceEntered() == null) {
			settotal(Env.ZERO);
			return;
		}		

		BigDecimal totalNet = getQtyEntered().multiply(getPriceEntered());		
		BigDecimal discount = Env.ZERO;
		BigDecimal tax = Env.ONE;

		if (issmj_percentage()) {
			discount = totalNet.multiply(getsmj_discount().divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
		} else {
			discount = getsmj_discount().multiply(getQtyEntered());
		}

		Integer M_PriceList_Version_ID = Integer.parseInt(Env.getCtx().getProperty("#SMJ_M_PriceList_Version_ID"));
		if (M_PriceList_Version_ID > 0) {
			MPriceListVersion priceListVersion = new MPriceListVersion(Env.getCtx(), M_PriceList_Version_ID, null);		
			MPriceList priceList = new MPriceList(Env.getCtx(), priceListVersion.getM_PriceList_ID(), null);
			if (!priceList.isTaxIncluded()) {
				tax = tax.add(gettax().divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			}
		}

		BigDecimal total = totalNet.subtract(discount).multiply(tax);
		settotal(total);
	}
	
	/**
	 * Calculate the amount for all fields, and if UOM is not unity, calculate the unit price
	 */
	private void calculateQtyAndAmt() {
		
		if (getQtyEntered().signum() == 0) {
			setQtyOrdered(Env.ZERO);
			setPriceEntered(getPriceActual());
			return;
		}
		
		MProductPrice pp = MProductPrice.get(Env.getCtx(), getM_PriceList_Version_ID(), getM_Product_ID(), get_TrxName());
		
		if (getM_PriceList_Version_ID() > 0) {
			setPriceEntered(pp.getPriceList());
		}
		
		BigDecimal QtyEntered, PriceActual, PriceEntered, QtyOrdered;
		
		int C_UOM_To_ID = getC_UOM_ID();
		int M_Product_ID = getM_Product_ID();
		QtyEntered = getQtyEntered();
		BigDecimal QtyEntered1 = QtyEntered.setScale(MUOM.getPrecision(getCtx(), C_UOM_To_ID), BigDecimal.ROUND_HALF_UP);
		
		if (QtyEntered.compareTo(QtyEntered1) != 0) {			
			QtyEntered = QtyEntered1;
			setQtyEntered(QtyEntered);
		}
		
		QtyOrdered = MUOMConversion.convertProductFrom (getCtx(), M_Product_ID, C_UOM_To_ID, QtyEntered);
		
		if (QtyOrdered == null)
			QtyOrdered = QtyEntered;
		
		PriceActual = getPriceEntered();		
		PriceEntered = MUOMConversion.convertProductFrom (getCtx(), M_Product_ID, C_UOM_To_ID, PriceActual);
		
		if (PriceEntered == null)
			PriceEntered = PriceActual;
		
		if (issmj_percentage()) {
			PriceActual = BigDecimal.valueOf((100.0 - getsmj_discount().doubleValue()) / 100.0 * PriceActual.doubleValue()); 
		} else {
			BigDecimal qty = QtyOrdered.divide(QtyEntered, MUOM.getPrecision(getCtx(), C_UOM_To_ID), RoundingMode.HALF_UP);
			PriceActual = PriceActual.subtract(getsmj_discount().divide(qty));
		}
		
		setQtyOrdered(QtyOrdered);
		setPriceActual(PriceActual);
		setPriceEntered(PriceEntered);
	}
}//MSMJTmpWebSalesLine
