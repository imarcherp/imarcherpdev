/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_tmpwebsalesline
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_smj_tmpwebsalesline extends PO implements I_smj_tmpwebsalesline, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151115L;

    /** Standard Constructor */
    public X_smj_tmpwebsalesline (Properties ctx, int smj_tmpwebsalesline_ID, String trxName)
    {
      super (ctx, smj_tmpwebsalesline_ID, trxName);
      /** if (smj_tmpwebsalesline_ID == 0)
        {
			setsmj_iswarranty (null);
			setsmj_iswarrantyapplied (null);
			setsmj_isworkorder (null);
			setsmj_tmpwebsales_ID (0);
			setsmj_tmpwebsalesline_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_tmpwebsalesline (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_tmpwebsalesline[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_Value (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_PriceList_Version getM_PriceList_Version() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList_Version)MTable.get(getCtx(), org.compiere.model.I_M_PriceList_Version.Table_Name)
			.getPO(getM_PriceList_Version_ID(), get_TrxName());	}

	/** Set Price List Version.
		@param M_PriceList_Version_ID 
		Identifies a unique instance of a Price List
	  */
	public void setM_PriceList_Version_ID (int M_PriceList_Version_ID)
	{
		if (M_PriceList_Version_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_Version_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_Version_ID, Integer.valueOf(M_PriceList_Version_ID));
	}

	/** Get Price List Version.
		@return Identifies a unique instance of a Price List
	  */
	public int getM_PriceList_Version_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_Version_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set partnername.
		@param partnername partnername	  */
	public void setpartnername (String partnername)
	{
		set_Value (COLUMNNAME_partnername, partnername);
	}

	/** Get partnername.
		@return partnername	  */
	public String getpartnername () 
	{
		return (String)get_Value(COLUMNNAME_partnername);
	}

	/** Set Unit Price.
		@param PriceActual 
		Actual Price 
	  */
	public void setPriceActual (BigDecimal PriceActual)
	{
		set_ValueNoCheck (COLUMNNAME_PriceActual, PriceActual);
	}

	/** Get Unit Price.
		@return Actual Price 
	  */
	public BigDecimal getPriceActual () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceActual);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price.
		@param PriceEntered 
		Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered)
	{
		set_ValueNoCheck (COLUMNNAME_PriceEntered, PriceEntered);
	}

	/** Get Price.
		@return Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Product Name.
		@param ProductName 
		Name of the Product
	  */
	public void setProductName (String ProductName)
	{
		set_ValueNoCheck (COLUMNNAME_ProductName, ProductName);
	}

	/** Get Product Name.
		@return Name of the Product
	  */
	public String getProductName () 
	{
		return (String)get_Value(COLUMNNAME_ProductName);
	}

	/** Set Product Key.
		@param ProductValue 
		Key of the Product
	  */
	public void setProductValue (String ProductValue)
	{
		set_Value (COLUMNNAME_ProductValue, ProductValue);
	}

	/** Get Product Key.
		@return Key of the Product
	  */
	public String getProductValue () 
	{
		return (String)get_Value(COLUMNNAME_ProductValue);
	}

	/** Set Purchase Price.
		@param purchaseprice Purchase Price	  */
	public void setpurchaseprice (BigDecimal purchaseprice)
	{
		set_ValueNoCheck (COLUMNNAME_purchaseprice, purchaseprice);
	}

	/** Get Purchase Price.
		@return Purchase Price	  */
	public BigDecimal getpurchaseprice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_purchaseprice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param QtyEntered 
		The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered)
	{
		set_ValueNoCheck (COLUMNNAME_QtyEntered, QtyEntered);
	}

	/** Get Quantity.
		@return The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ordered Quantity.
		@param QtyOrdered 
		Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered)
	{
		set_ValueNoCheck (COLUMNNAME_QtyOrdered, QtyOrdered);
	}

	/** Get Ordered Quantity.
		@return Ordered Quantity
	  */
	public BigDecimal getQtyOrdered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOrdered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_R_Request getR_Request() throws RuntimeException
    {
		return (org.compiere.model.I_R_Request)MTable.get(getCtx(), org.compiere.model.I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_R_Request_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_destination.
		@param smj_destination smj_destination	  */
	public void setsmj_destination (String smj_destination)
	{
		set_Value (COLUMNNAME_smj_destination, smj_destination);
	}

	/** Get smj_destination.
		@return smj_destination	  */
	public String getsmj_destination () 
	{
		return (String)get_Value(COLUMNNAME_smj_destination);
	}

	/** Set smj_discount.
		@param smj_discount smj_discount	  */
	public void setsmj_discount (BigDecimal smj_discount)
	{
		set_Value (COLUMNNAME_smj_discount, smj_discount);
	}

	/** Get smj_discount.
		@return smj_discount	  */
	public BigDecimal getsmj_discount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_discount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set smj_iswarranty.
		@param smj_iswarranty smj_iswarranty	  */
	public void setsmj_iswarranty (String smj_iswarranty)
	{
		set_Value (COLUMNNAME_smj_iswarranty, smj_iswarranty);
	}

	/** Get smj_iswarranty.
		@return smj_iswarranty	  */
	public String getsmj_iswarranty () 
	{
		return (String)get_Value(COLUMNNAME_smj_iswarranty);
	}

	/** Set smj_iswarrantyapplied.
		@param smj_iswarrantyapplied smj_iswarrantyapplied	  */
	public void setsmj_iswarrantyapplied (String smj_iswarrantyapplied)
	{
		set_Value (COLUMNNAME_smj_iswarrantyapplied, smj_iswarrantyapplied);
	}

	/** Get smj_iswarrantyapplied.
		@return smj_iswarrantyapplied	  */
	public String getsmj_iswarrantyapplied () 
	{
		return (String)get_Value(COLUMNNAME_smj_iswarrantyapplied);
	}

	/** Set smj_isworkorder.
		@param smj_isworkorder smj_isworkorder	  */
	public void setsmj_isworkorder (String smj_isworkorder)
	{
		set_Value (COLUMNNAME_smj_isworkorder, smj_isworkorder);
	}

	/** Get smj_isworkorder.
		@return smj_isworkorder	  */
	public String getsmj_isworkorder () 
	{
		return (String)get_Value(COLUMNNAME_smj_isworkorder);
	}

	/** Set smj_notes.
		@param smj_notes smj_notes	  */
	public void setsmj_notes (String smj_notes)
	{
		set_Value (COLUMNNAME_smj_notes, smj_notes);
	}

	/** Get smj_notes.
		@return smj_notes	  */
	public String getsmj_notes () 
	{
		return (String)get_Value(COLUMNNAME_smj_notes);
	}

	/** Set smj_percentage.
		@param smj_percentage smj_percentage	  */
	public void setsmj_percentage (boolean smj_percentage)
	{
		set_Value (COLUMNNAME_smj_percentage, Boolean.valueOf(smj_percentage));
	}

	/** Get smj_percentage.
		@return smj_percentage	  */
	public boolean issmj_percentage () 
	{
		Object oo = get_Value(COLUMNNAME_smj_percentage);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set smj_plate.
		@param smj_plate smj_plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get smj_plate.
		@return smj_plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	public I_smj_tmpwebsales getsmj_tmpwebsales() throws RuntimeException
    {
		return (I_smj_tmpwebsales)MTable.get(getCtx(), I_smj_tmpwebsales.Table_Name)
			.getPO(getsmj_tmpwebsales_ID(), get_TrxName());	}

	/** Set smj_tmpwebsales.
		@param smj_tmpwebsales_ID smj_tmpwebsales	  */
	public void setsmj_tmpwebsales_ID (int smj_tmpwebsales_ID)
	{
		if (smj_tmpwebsales_ID < 1) 
			set_Value (COLUMNNAME_smj_tmpwebsales_ID, null);
		else 
			set_Value (COLUMNNAME_smj_tmpwebsales_ID, Integer.valueOf(smj_tmpwebsales_ID));
	}

	/** Get smj_tmpwebsales.
		@return smj_tmpwebsales	  */
	public int getsmj_tmpwebsales_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpwebsales_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_tmpwebsalesline.
		@param smj_tmpwebsalesline_ID smj_tmpwebsalesline	  */
	public void setsmj_tmpwebsalesline_ID (int smj_tmpwebsalesline_ID)
	{
		if (smj_tmpwebsalesline_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_tmpwebsalesline_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_tmpwebsalesline_ID, Integer.valueOf(smj_tmpwebsalesline_ID));
	}

	/** Get smj_tmpwebsalesline.
		@return smj_tmpwebsalesline	  */
	public int getsmj_tmpwebsalesline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpwebsalesline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_tmpwebsalesline_UU.
		@param smj_tmpwebsalesline_UU smj_tmpwebsalesline_UU	  */
	public void setsmj_tmpwebsalesline_UU (String smj_tmpwebsalesline_UU)
	{
		set_Value (COLUMNNAME_smj_tmpwebsalesline_UU, smj_tmpwebsalesline_UU);
	}

	/** Get smj_tmpwebsalesline_UU.
		@return smj_tmpwebsalesline_UU	  */
	public String getsmj_tmpwebsalesline_UU () 
	{
		return (String)get_Value(COLUMNNAME_smj_tmpwebsalesline_UU);
	}

	/** Set tax.
		@param tax tax	  */
	public void settax (BigDecimal tax)
	{
		set_Value (COLUMNNAME_tax, tax);
	}

	/** Get tax.
		@return tax	  */
	public BigDecimal gettax () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_tax);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set total.
		@param total total	  */
	public void settotal (BigDecimal total)
	{
		set_Value (COLUMNNAME_total, total);
	}

	/** Get total.
		@return total	  */
	public BigDecimal gettotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set UOM Name.
		@param UOMName UOM Name	  */
	public void setUOMName (String UOMName)
	{
		set_ValueNoCheck (COLUMNNAME_UOMName, UOMName);
	}

	/** Get UOM Name.
		@return UOM Name	  */
	public String getUOMName () 
	{
		return (String)get_Value(COLUMNNAME_UOMName);
	}

	/** Set Warehouse.
		@param WarehouseName 
		Warehouse Name
	  */
	public void setWarehouseName (String WarehouseName)
	{
		set_ValueNoCheck (COLUMNNAME_WarehouseName, WarehouseName);
	}

	/** Get Warehouse.
		@return Warehouse Name
	  */
	public String getWarehouseName () 
	{
		return (String)get_Value(COLUMNNAME_WarehouseName);
	}
}