/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_MechanicCommission
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_MechanicCommission extends PO implements I_smj_MechanicCommission, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130520L;

    /** Standard Constructor */
    public X_smj_MechanicCommission (Properties ctx, int smj_MechanicCommission_ID, String trxName)
    {
      super (ctx, smj_MechanicCommission_ID, trxName);
      /** if (smj_MechanicCommission_ID == 0)
        {
			setappliedfee (Env.ZERO);
			setC_BPartner_ID (0);
			setdatecreated (new Timestamp( System.currentTimeMillis() ));
			setM_Product_ID (0);
			setQty (Env.ZERO);
			setR_Request_ID (0);
			setsmj_MechanicCommission_ID (0);
			setsmj_workOrderLine_ID (0);
			settotal (Env.ZERO);
			settotalinvoiced (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_MechanicCommission (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_MechanicCommission[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Applied Fee.
		@param appliedfee Applied Fee	  */
	public void setappliedfee (BigDecimal appliedfee)
	{
		set_Value (COLUMNNAME_appliedfee, appliedfee);
	}

	/** Get Applied Fee.
		@return Applied Fee	  */
	public BigDecimal getappliedfee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_appliedfee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (I_C_InvoiceLine)MTable.get(getCtx(), I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Created.
		@param datecreated Date Created	  */
	public void setdatecreated (Timestamp datecreated)
	{
		set_Value (COLUMNNAME_datecreated, datecreated);
	}

	/** Get Date Created.
		@return Date Created	  */
	public Timestamp getdatecreated () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datecreated);
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_R_Request getR_Request() throws RuntimeException
    {
		return (I_R_Request)MTable.get(getCtx(), I_R_Request.Table_Name)
			.getPO(getR_Request_ID(), get_TrxName());	}

	/** Set Request.
		@param R_Request_ID 
		Request from a Business Partner or Prospect
	  */
	public void setR_Request_ID (int R_Request_ID)
	{
		if (R_Request_ID < 1) 
			set_Value (COLUMNNAME_R_Request_ID, null);
		else 
			set_Value (COLUMNNAME_R_Request_ID, Integer.valueOf(R_Request_ID));
	}

	/** Get Request.
		@return Request from a Business Partner or Prospect
	  */
	public int getR_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_R_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Mechanic Commission.
		@param smj_MechanicCommission_ID Mechanic Commission	  */
	public void setsmj_MechanicCommission_ID (int smj_MechanicCommission_ID)
	{
		if (smj_MechanicCommission_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_MechanicCommission_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_MechanicCommission_ID, Integer.valueOf(smj_MechanicCommission_ID));
	}

	/** Get Mechanic Commission.
		@return Mechanic Commission	  */
	public int getsmj_MechanicCommission_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_MechanicCommission_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total.
		@param total Total	  */
	public void settotal (BigDecimal total)
	{
		set_Value (COLUMNNAME_total, total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal gettotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Invoiced.
		@param totalinvoiced Total Invoiced	  */
	public void settotalinvoiced (BigDecimal totalinvoiced)
	{
		set_Value (COLUMNNAME_totalinvoiced, totalinvoiced);
	}

	/** Get Total Invoiced.
		@return Total Invoiced	  */
	public BigDecimal gettotalinvoiced () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_totalinvoiced);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}