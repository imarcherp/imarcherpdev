package com.smj.model;

import java.util.Properties;

public class MSMJMechanicCommission extends X_smj_MechanicCommission{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6334504213813413102L;

	/**
	 * MSMJMechanicCommission constructor
	 * @param ctx
	 * @param smj_MechanicCommission_ID
	 * @param trxName
	 */
	public MSMJMechanicCommission(Properties ctx,
			int smj_MechanicCommission_ID, String trxName) {
		super(ctx, smj_MechanicCommission_ID, trxName);
	}

}//MSMJMechanicCommission
