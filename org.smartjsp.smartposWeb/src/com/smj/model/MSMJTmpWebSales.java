package com.smj.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.model.MOrderLine;
import org.compiere.model.MRequest;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

public class MSMJTmpWebSales extends X_smj_tmpwebsales {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4901047666220271095L;

	private MSMJTmpWebSalesLine[] m_lines;

	/**
	 * MSMJTmpWebSales constructor
	 * @param ctx
	 * @param smj_tmpWebSalesLine_ID
	 * @param trxName
	 */
	public MSMJTmpWebSales(Properties ctx, int smj_tmpWebSalesLine_ID,
			String trxName) {
		super(ctx, smj_tmpWebSalesLine_ID, trxName);
	}//MSMJTmpWebSales
	
	public MSMJTmpWebSales(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}//MSMJTmpWebSales

	public MSMJTmpWebSalesLine[] getLines (String whereClause, String orderClause)
	{
		//red1 - using new Query class from Teo / Victor's MDDOrder.java implementation
		StringBuilder whereClauseFinal = new StringBuilder(MSMJTmpWebSales.COLUMNNAME_smj_tmpwebsales_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause.length() == 0)
			orderClause = MOrderLine.COLUMNNAME_Line;
		//
		List<MOrderLine> list = new Query(getCtx(), I_smj_tmpwebsalesline.Table_Name, whereClauseFinal.toString(), get_TrxName())
				.setParameters(get_ID())
				.setOrderBy(orderClause)
				.list();
		//
		return list.toArray(new MSMJTmpWebSalesLine[list.size()]);		
	}	//	getLines

	public MSMJTmpWebSalesLine[] getLines(boolean requery, String orderBy) {
		if (m_lines != null && !requery) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;
		else
			orderClause += "Line";
		m_lines = getLines(null, orderClause);
		return m_lines;
	}

	public MSMJTmpWebSalesLine[] getLines() {
		return getLines(false, null);
	}



	/** Get Work Order.
	@return Work Order	  */
	@Override
	public List<Integer> getsmj_workorder () 
	{
		String ii = (String) get_Value(COLUMNNAME_smj_workorder);
		if (ii == null)
			return new ArrayList<Integer>();

		List<Integer> list = new ArrayList<Integer>();
		list.toString().isEmpty();
		for (String value : ii.split(",")) {
			if (value == null || value.isEmpty())
				continue;
				
			list.add(Integer.parseInt(value.trim()));
		}

		return list;
	}
	
	/** Set Work Order.
	@param smj_workorder Work Order	  */
	@Override
	public boolean setsmj_workorder (List<Integer> workOrders)
	{
		if (workOrders == null) {
			workOrders = new ArrayList<Integer>();
		}

		String smj_workorder = "";
		for (Integer workOrder : workOrders) {
			if (!smj_workorder.isEmpty()) {
				smj_workorder += ",";
			}

			smj_workorder += workOrder.toString().trim();
		}

		return set_Value (COLUMNNAME_smj_workorder, smj_workorder);
	}
	
	public boolean addWorkOrder(Integer workOrder) throws Exception {
		MColumn column = MColumn.get(Env.getCtx(), Table_Name, COLUMNNAME_smj_workorder);
		int fieldSize = column.getFieldLength();
		int actualSize = getWorkOrderAsString().length() + workOrder.toString().length();
		
		if (actualSize > fieldSize) {
			throw new AdempiereUserError("SMJ_MsgExceedMaxInTicket");
		}
		
		List<Integer> list = getsmj_workorder();
		list.add(workOrder);
		return setsmj_workorder(list);
	}
	
	public String getWorkOrderAsString() {
		return getsmj_workorder().toString().replace("[", "").replace("]", "");
	}
	
	public String getWorkOrderDocumentNo() {
		String value = "";
		
		for (Integer workOrder : getsmj_workorder()) {
			MRequest request = new MRequest(getCtx(), workOrder, get_TrxName());
			
			if (!value.isEmpty())
				value += ", ";
			
			value += request.getDocumentNo();
		}
		
		return value;
	}
	
	public Boolean removeWO(Integer R_Request_ID) {		
		List<Integer> tmpList = getsmj_workorder();
		Boolean result = tmpList.remove(R_Request_ID);
		setsmj_workorder(tmpList);
		
		return result;
	}

	@Override
	public void deleteEx(boolean force) throws AdempiereException {
		String sql = "DELETE FROM SMJ_TmpWebSalesLine WHERE SMJ_TmpWebSales_ID = ?";
		DB.executeUpdateEx(sql, new Object[] {this.get_ID()}, this.get_TrxName());
		super.deleteEx(force);
	}
	
	
}//MSMJTmpWebSales
