/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.compiere.model.I_Persistent;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.POInfo;

/** Generated Model for smj_tmpwebsales
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_smj_tmpwebsales extends PO implements I_smj_tmpwebsales, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160310L;

	/** Standard Constructor */
	public X_smj_tmpwebsales (Properties ctx, int smj_tmpwebsales_ID, String trxName)
	{
		super (ctx, smj_tmpwebsales_ID, trxName);
		/** if (smj_tmpwebsales_ID == 0)
        {
			setC_BPartner_ID (0);
			setC_BPartner_Location_ID (0);
			setsmj_tmpwebsales_ID (0);
        } */
	}

	/** Load Constructor */
	public X_smj_tmpwebsales (Properties ctx, ResultSet rs, String trxName)
	{
		super (ctx, rs, trxName);
	}

	/** AccessLevel
	 * @return 7 - System - Client - Org 
	 */
	protected int get_AccessLevel()
	{
		return accessLevel.intValue();
	}

	/** Load Meta Data */
	protected POInfo initPO (Properties ctx)
	{
		POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
		return poi;
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer ("X_smj_tmpwebsales[")
				.append(get_ID()).append("]");
		return sb.toString();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
	{
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
				.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	 */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	 */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
	{
		return (org.compiere.model.I_C_BPartner_Location)MTable.get(getCtx(), org.compiere.model.I_C_BPartner_Location.Table_Name)
				.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	 */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	 */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
	{
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
				.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	 */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	 */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException
	{
		return (org.compiere.model.I_C_PaymentTerm)MTable.get(getCtx(), org.compiere.model.I_C_PaymentTerm.Table_Name)
				.getPO(getC_PaymentTerm_ID(), get_TrxName());	}

	/** Set Payment Term.
		@param C_PaymentTerm_ID 
		The terms of Payment (timing, discount)
	 */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID)
	{
		if (C_PaymentTerm_ID < 1) 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, null);
		else 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, Integer.valueOf(C_PaymentTerm_ID));
	}

	/** Get Payment Term.
		@return The terms of Payment (timing, discount)
	 */
	public int getC_PaymentTerm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaymentTerm_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	/** Set smj_ticket.
		@param smj_ticket smj_ticket	  */
	public void setsmj_ticket (String smj_ticket)
	{
		set_Value (COLUMNNAME_smj_ticket, smj_ticket);
	}

	/** Get smj_ticket.
		@return smj_ticket	  */
	public String getsmj_ticket () 
	{
		return (String)get_Value(COLUMNNAME_smj_ticket);
	}

	/** Set Tmp Web Sales.
		@param smj_tmpwebsales_ID Tmp Web Sales	  */
	public void setsmj_tmpwebsales_ID (int smj_tmpwebsales_ID)
	{
		if (smj_tmpwebsales_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_tmpwebsales_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_tmpwebsales_ID, Integer.valueOf(smj_tmpwebsales_ID));
	}

	/** Get Tmp Web Sales.
		@return Tmp Web Sales	  */
	public int getsmj_tmpwebsales_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_tmpwebsales_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	/** Set smj_tmpWebSales_UU.
		@param smj_tmpWebSales_UU smj_tmpWebSales_UU	  */
	public void setsmj_tmpWebSales_UU (String smj_tmpWebSales_UU)
	{
		set_ValueNoCheck (COLUMNNAME_smj_tmpWebSales_UU, smj_tmpWebSales_UU);
	}

	/** Get smj_tmpWebSales_UU.
		@return smj_tmpWebSales_UU	  */
	public String getsmj_tmpWebSales_UU () 
	{
		return (String)get_Value(COLUMNNAME_smj_tmpWebSales_UU);
	}

	/** Set Vehicle.
		@param SMJ_Vehicle_ID Vehicle	  */
	public void setSMJ_Vehicle_ID (int SMJ_Vehicle_ID)
	{
		if (SMJ_Vehicle_ID < 1) 
			set_Value (COLUMNNAME_SMJ_Vehicle_ID, null);
		else 
			set_Value (COLUMNNAME_SMJ_Vehicle_ID, Integer.valueOf(SMJ_Vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getSMJ_Vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SMJ_Vehicle_ID);
		if (ii == null)
			return 0;
		return ii.intValue();
	}

	/** Set Work Order.
	@param smj_workorder Work Order	  */
	@Override
	public boolean setsmj_workorder (List<Integer> workOrders)
	{
		if (workOrders == null) {
			workOrders = new ArrayList<Integer>();
		}

		String smj_workorder = "";
		for (Integer workOrder : workOrders) {
			if (!smj_workorder.isEmpty()) {
				smj_workorder += ",";
			}

			smj_workorder += workOrder.toString().trim();
		}

		return set_Value (COLUMNNAME_smj_workorder, smj_workorder);
	}

	/** Get Work Order.
	@return Work Order	  */
	@Override
	public List<Integer> getsmj_workorder () 
	{
		String ii = (String) get_Value(COLUMNNAME_smj_workorder);
		if (ii == null)
			return new ArrayList<Integer>();

		List<Integer> list = new ArrayList<Integer>();
		list.toString();
		for (String value : ii.split(",")) {
			list.add(Integer.parseInt(value.trim()));
		}

		return list;
	}
}