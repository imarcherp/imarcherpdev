/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_closecash
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_smj_closecash extends PO implements I_smj_closecash, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150618L;

    /** Standard Constructor */
    public X_smj_closecash (Properties ctx, int smj_closecash_ID, String trxName)
    {
      super (ctx, smj_closecash_ID, trxName);
      /** if (smj_closecash_ID == 0)
        {
			setM_Warehouse_ID (0);
			setSMJ_CloseCash_ID (0);
			setsmj_enddate (new Timestamp( System.currentTimeMillis() ));
			setsmj_final (Env.ZERO);
			setsmj_initial (Env.ZERO);
			setSMJ_Sequence (Env.ZERO);
			setsmj_startdate (new Timestamp( System.currentTimeMillis() ));
			setsmj_total (Env.ZERO);
			setsmj_totaldiscounts (Env.ZERO);
			setsmj_totalin (Env.ZERO);
			setsmj_totalout (Env.ZERO);
			setsmj_totalreturns (Env.ZERO);
			setsmj_totaltickets (0);
        } */
    }

    /** Load Constructor */
    public X_smj_closecash (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_closecash[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set docno_final.
		@param docno_final docno_final	  */
	public void setdocno_final (String docno_final)
	{
		set_Value (COLUMNNAME_docno_final, docno_final);
	}

	/** Get docno_final.
		@return docno_final	  */
	public String getdocno_final () 
	{
		return (String)get_Value(COLUMNNAME_docno_final);
	}

	/** Set docno_initial.
		@param docno_initial docno_initial	  */
	public void setdocno_initial (String docno_initial)
	{
		set_Value (COLUMNNAME_docno_initial, docno_initial);
	}

	/** Get docno_initial.
		@return docno_initial	  */
	public String getdocno_initial () 
	{
		return (String)get_Value(COLUMNNAME_docno_initial);
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Close Cash.
		@param SMJ_CloseCash_ID Close Cash	  */
	public void setSMJ_CloseCash_ID (int SMJ_CloseCash_ID)
	{
		if (SMJ_CloseCash_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SMJ_CloseCash_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SMJ_CloseCash_ID, Integer.valueOf(SMJ_CloseCash_ID));
	}

	/** Get Close Cash.
		@return Close Cash	  */
	public int getSMJ_CloseCash_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SMJ_CloseCash_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_closecash_UU.
		@param smj_closecash_UU smj_closecash_UU	  */
	public void setsmj_closecash_UU (String smj_closecash_UU)
	{
		set_Value (COLUMNNAME_smj_closecash_UU, smj_closecash_UU);
	}

	/** Get smj_closecash_UU.
		@return smj_closecash_UU	  */
	public String getsmj_closecash_UU () 
	{
		return (String)get_Value(COLUMNNAME_smj_closecash_UU);
	}

	/** Set End Date.
		@param smj_enddate End Date	  */
	public void setsmj_enddate (Timestamp smj_enddate)
	{
		set_Value (COLUMNNAME_smj_enddate, smj_enddate);
	}

	/** Get End Date.
		@return End Date	  */
	public Timestamp getsmj_enddate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_smj_enddate);
	}

	/** Set N° Final.
		@param smj_final N° Final	  */
	public void setsmj_final (BigDecimal smj_final)
	{
		set_Value (COLUMNNAME_smj_final, smj_final);
	}

	/** Get N° Final.
		@return N° Final	  */
	public BigDecimal getsmj_final () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_final);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set N° Initial.
		@param smj_initial N° Initial	  */
	public void setsmj_initial (BigDecimal smj_initial)
	{
		set_Value (COLUMNNAME_smj_initial, smj_initial);
	}

	/** Get N° Initial.
		@return N° Initial	  */
	public BigDecimal getsmj_initial () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_initial);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sequence.
		@param SMJ_Sequence Sequence	  */
	public void setSMJ_Sequence (BigDecimal SMJ_Sequence)
	{
		set_Value (COLUMNNAME_SMJ_Sequence, SMJ_Sequence);
	}

	/** Get Sequence.
		@return Sequence	  */
	public BigDecimal getSMJ_Sequence () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SMJ_Sequence);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Start Date.
		@param smj_startdate Start Date	  */
	public void setsmj_startdate (Timestamp smj_startdate)
	{
		set_Value (COLUMNNAME_smj_startdate, smj_startdate);
	}

	/** Get Start Date.
		@return Start Date	  */
	public Timestamp getsmj_startdate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_smj_startdate);
	}

	/** Set Total.
		@param smj_total Total	  */
	public void setsmj_total (BigDecimal smj_total)
	{
		set_Value (COLUMNNAME_smj_total, smj_total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal getsmj_total () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Discounts.
		@param smj_totaldiscounts Total Discounts	  */
	public void setsmj_totaldiscounts (BigDecimal smj_totaldiscounts)
	{
		set_Value (COLUMNNAME_smj_totaldiscounts, smj_totaldiscounts);
	}

	/** Get Total Discounts.
		@return Total Discounts	  */
	public BigDecimal getsmj_totaldiscounts () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_totaldiscounts);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Entries.
		@param smj_totalin Total Entries	  */
	public void setsmj_totalin (BigDecimal smj_totalin)
	{
		set_Value (COLUMNNAME_smj_totalin, smj_totalin);
	}

	/** Get Total Entries.
		@return Total Entries	  */
	public BigDecimal getsmj_totalin () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_totalin);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Output.
		@param smj_totalout Total Output	  */
	public void setsmj_totalout (BigDecimal smj_totalout)
	{
		set_Value (COLUMNNAME_smj_totalout, smj_totalout);
	}

	/** Get Total Output.
		@return Total Output	  */
	public BigDecimal getsmj_totalout () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_totalout);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Returns.
		@param smj_totalreturns Total Returns	  */
	public void setsmj_totalreturns (BigDecimal smj_totalreturns)
	{
		set_Value (COLUMNNAME_smj_totalreturns, smj_totalreturns);
	}

	/** Get Total Returns.
		@return Total Returns	  */
	public BigDecimal getsmj_totalreturns () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_totalreturns);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Tickets.
		@param smj_totaltickets Total Tickets	  */
	public void setsmj_totaltickets (int smj_totaltickets)
	{
		set_Value (COLUMNNAME_smj_totaltickets, Integer.valueOf(smj_totaltickets));
	}

	/** Get Total Tickets.
		@return Total Tickets	  */
	public int getsmj_totaltickets () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_totaltickets);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}