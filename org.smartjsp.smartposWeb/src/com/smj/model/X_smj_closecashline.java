/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_closecashline
 *  @author iDempiere (generated) 
 *  @version Release 2.0 - $Id$ */
public class X_smj_closecashline extends PO implements I_smj_closecashline, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20140713L;

    /** Standard Constructor */
    public X_smj_closecashline (Properties ctx, int smj_closecashline_ID, String trxName)
    {
      super (ctx, smj_closecashline_ID, trxName);
      /** if (smj_closecashline_ID == 0)
        {
			setsmj_closecash_ID (0);
			setsmj_closecashline_ID (0);
			setsmj_concept (null);
			setsmj_sequence (Env.ZERO);
			setsmj_totalline (Env.ZERO);
			setsmj_unit (null);
			setsmj_units (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_smj_closecashline (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_closecashline[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_smj_closecash getsmj_closecash() throws RuntimeException
    {
		return (I_smj_closecash)MTable.get(getCtx(), I_smj_closecash.Table_Name)
			.getPO(getsmj_closecash_ID(), get_TrxName());	}

	/** Set smj_closecash.
		@param smj_closecash_ID smj_closecash	  */
	public void setsmj_closecash_ID (int smj_closecash_ID)
	{
		if (smj_closecash_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_closecash_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_closecash_ID, Integer.valueOf(smj_closecash_ID));
	}

	/** Get smj_closecash.
		@return smj_closecash	  */
	public int getsmj_closecash_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_closecash_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_closecashline.
		@param smj_closecashline_ID smj_closecashline	  */
	public void setsmj_closecashline_ID (int smj_closecashline_ID)
	{
		if (smj_closecashline_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_closecashline_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_closecashline_ID, Integer.valueOf(smj_closecashline_ID));
	}

	/** Get smj_closecashline.
		@return smj_closecashline	  */
	public int getsmj_closecashline_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_closecashline_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set smj_concept.
		@param smj_concept smj_concept	  */
	public void setsmj_concept (String smj_concept)
	{
		set_Value (COLUMNNAME_smj_concept, smj_concept);
	}

	/** Get smj_concept.
		@return smj_concept	  */
	public String getsmj_concept () 
	{
		return (String)get_Value(COLUMNNAME_smj_concept);
	}

	/** Set smj_sequence.
		@param smj_sequence smj_sequence	  */
	public void setsmj_sequence (BigDecimal smj_sequence)
	{
		set_Value (COLUMNNAME_smj_sequence, smj_sequence);
	}

	/** Get smj_sequence.
		@return smj_sequence	  */
	public BigDecimal getsmj_sequence () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_sequence);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set smj_totalline.
		@param smj_totalline smj_totalline	  */
	public void setsmj_totalline (BigDecimal smj_totalline)
	{
		set_Value (COLUMNNAME_smj_totalline, smj_totalline);
	}

	/** Get smj_totalline.
		@return smj_totalline	  */
	public BigDecimal getsmj_totalline () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_totalline);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set smj_unit.
		@param smj_unit smj_unit	  */
	public void setsmj_unit (String smj_unit)
	{
		set_Value (COLUMNNAME_smj_unit, smj_unit);
	}

	/** Get smj_unit.
		@return smj_unit	  */
	public String getsmj_unit () 
	{
		return (String)get_Value(COLUMNNAME_smj_unit);
	}

	/** Set smj_units.
		@param smj_units smj_units	  */
	public void setsmj_units (BigDecimal smj_units)
	{
		set_Value (COLUMNNAME_smj_units, smj_units);
	}

	/** Get smj_units.
		@return smj_units	  */
	public BigDecimal getsmj_units () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_smj_units);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}