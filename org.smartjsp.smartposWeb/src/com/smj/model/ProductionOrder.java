package com.smj.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

//import org.compiere.model.MProduction;
import org.compiere.process.DocAction;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;



public class ProductionOrder {
	
	public static CLogger log = CLogger.getCLogger(ProductionOrder.class);
	private  MProduction production = null;

	public ProductionOrder(int productId,String trxName, Properties ctx,BigDecimal qty, int locatorId){
		
		try {
		production = new MProduction( ctx, 0, trxName );
		production.setM_Product_ID(productId);
		production.setProductionQty(qty);

		Timestamp time = Env.getContextAsDate(Env.getCtx(), "#Date");
		
		production.setMovementDate(time);
		production.setM_Locator_ID(locatorId);

		production.saveEx(trxName);
		production.deleteLines(trxName);
		production.saveEx(trxName);
		production.createLines(false);
		production.setIsCreated("Y");
		production.saveEx(trxName);
		
		production.completeIt();
		
		production.setIsComplete(true);
		production.setDocAction(DocAction.ACTION_Complete);
		production.setDocStatus( "CO");   // complete
		production.saveEx(trxName);
		
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".showDialogInfoProduct - ERROR: " + e.getMessage(), e);
		}
		
	}
	
	
	private boolean costsOK(int M_Product_ID,String trxName) throws AdempiereUserError {
		// Warning will not work if non-standard costing is used
		String sql = "SELECT ABS(((cc.currentcostprice-(SELECT SUM(c.currentcostprice*bom.bomqty)"
            + " FROM m_cost c"
            + " INNER JOIN m_product_bom bom ON (c.m_product_id=bom.m_productbom_id)"
	            + " INNER JOIN m_costelement ce ON (c.m_costelement_id = ce.m_costelement_id AND ce.costingmethod = 'S')"
            + " WHERE bom.m_product_id = pp.m_product_id)"
            + " )/cc.currentcostprice))"
            + " FROM m_product pp"
            + " INNER JOIN m_cost cc on (cc.m_product_id=pp.m_product_id)"
            + " INNER JOIN m_costelement ce ON (cc.m_costelement_id=ce.m_costelement_id)"
            + " WHERE cc.currentcostprice > 0 AND pp.M_Product_ID = ?"
            + " AND ce.costingmethod='S'";
		
		BigDecimal costPercentageDiff = DB.getSQLValueBD(trxName, sql, M_Product_ID);
		
		if (costPercentageDiff == null)
		{
			costPercentageDiff = Env.ZERO;
			String msg = "Could not retrieve costs";
				log.warning(msg);
		}
		
		if ( (costPercentageDiff.compareTo(new BigDecimal("0.005")))< 0 )
			return true;
		
		return false;
	}
	
	public MProduction getProduction() {
		return production;
	}

	public void setProduction(MProduction production) {
		this.production = production;
	}

	
}
