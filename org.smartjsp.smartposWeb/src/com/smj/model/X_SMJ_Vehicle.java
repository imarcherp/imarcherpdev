/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for SMJ_Vehicle
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_SMJ_Vehicle extends PO implements I_SMJ_Vehicle, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160308L;

    /** Standard Constructor */
    public X_SMJ_Vehicle (Properties ctx, int SMJ_Vehicle_ID, String trxName)
    {
      super (ctx, SMJ_Vehicle_ID, trxName);
      /** if (SMJ_Vehicle_ID == 0)
        {
			setaxles (null);
			setcapacitygallons (Env.ZERO);
			setcapacitytons (Env.ZERO);
			setistrailer (false);
			setistrucktractor (false);
			setqtytires (0);
			setsmj_plate (null);
			setSMJ_Vehicle_ID (0);
        } */
    }

    /** Load Constructor */
    public X_SMJ_Vehicle (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_SMJ_Vehicle[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getaffiliatecompany() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getaffiliatecompany_ID(), get_TrxName());	}

	/** Set Company.
		@param affiliatecompany_ID Company	  */
	public void setaffiliatecompany_ID (int affiliatecompany_ID)
	{
		if (affiliatecompany_ID < 1) 
			set_Value (COLUMNNAME_affiliatecompany_ID, null);
		else 
			set_Value (COLUMNNAME_affiliatecompany_ID, Integer.valueOf(affiliatecompany_ID));
	}

	/** Get Company.
		@return Company	  */
	public int getaffiliatecompany_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_affiliatecompany_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_SMJ_Vehicle getasociatedtrucktrailer() throws RuntimeException
    {
		return (I_SMJ_Vehicle)MTable.get(getCtx(), I_SMJ_Vehicle.Table_Name)
			.getPO(getasociatedtrucktrailer_ID(), get_TrxName());	}

	/** Set Asociated Truck Trailer.
		@param asociatedtrucktrailer_ID Asociated Truck Trailer	  */
	public void setasociatedtrucktrailer_ID (int asociatedtrucktrailer_ID)
	{
		if (asociatedtrucktrailer_ID < 1) 
			set_Value (COLUMNNAME_asociatedtrucktrailer_ID, null);
		else 
			set_Value (COLUMNNAME_asociatedtrucktrailer_ID, Integer.valueOf(asociatedtrucktrailer_ID));
	}

	/** Get Asociated Truck Trailer.
		@return Asociated Truck Trailer	  */
	public int getasociatedtrucktrailer_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_asociatedtrucktrailer_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Axles Number.
		@param axles Axles Number	  */
	public void setaxles (String axles)
	{
		set_Value (COLUMNNAME_axles, axles);
	}

	/** Get Axles Number.
		@return Axles Number	  */
	public String getaxles () 
	{
		return (String)get_Value(COLUMNNAME_axles);
	}

	/** Set Body Length.
		@param bodylength Body Length	  */
	public void setbodylength (BigDecimal bodylength)
	{
		set_Value (COLUMNNAME_bodylength, bodylength);
	}

	/** Get Body Length.
		@return Body Length	  */
	public BigDecimal getbodylength () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_bodylength);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException
    {
		return (org.compiere.model.I_C_PaymentTerm)MTable.get(getCtx(), org.compiere.model.I_C_PaymentTerm.Table_Name)
			.getPO(getC_PaymentTerm_ID(), get_TrxName());	}

	/** Set Payment Term.
		@param C_PaymentTerm_ID 
		The terms of Payment (timing, discount)
	  */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID)
	{
		if (C_PaymentTerm_ID < 1) 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, null);
		else 
			set_Value (COLUMNNAME_C_PaymentTerm_ID, Integer.valueOf(C_PaymentTerm_ID));
	}

	/** Get Payment Term.
		@return The terms of Payment (timing, discount)
	  */
	public int getC_PaymentTerm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaymentTerm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Capacity Gallons.
		@param capacitygallons Capacity Gallons	  */
	public void setcapacitygallons (BigDecimal capacitygallons)
	{
		set_Value (COLUMNNAME_capacitygallons, capacitygallons);
	}

	/** Get Capacity Gallons.
		@return Capacity Gallons	  */
	public BigDecimal getcapacitygallons () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_capacitygallons);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Capacity Passenger.
		@param capacitypassenger Capacity Passenger	  */
	public void setcapacitypassenger (int capacitypassenger)
	{
		set_Value (COLUMNNAME_capacitypassenger, Integer.valueOf(capacitypassenger));
	}

	/** Get Capacity Passenger.
		@return Capacity Passenger	  */
	public int getcapacitypassenger () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_capacitypassenger);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Capacity Tons.
		@param capacitytons Capacity Tons	  */
	public void setcapacitytons (BigDecimal capacitytons)
	{
		set_Value (COLUMNNAME_capacitytons, capacitytons);
	}

	/** Get Capacity Tons.
		@return Capacity Tons	  */
	public BigDecimal getcapacitytons () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_capacitytons);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Card Operation.
		@param cardoperation Card Operation	  */
	public void setcardoperation (String cardoperation)
	{
		set_Value (COLUMNNAME_cardoperation, cardoperation);
	}

	/** Get Card Operation.
		@return Card Operation	  */
	public String getcardoperation () 
	{
		return (String)get_Value(COLUMNNAME_cardoperation);
	}

	/** Set Card Operation Expiration.
		@param cardoperationexpiration Card Operation Expiration	  */
	public void setcardoperationexpiration (Timestamp cardoperationexpiration)
	{
		set_Value (COLUMNNAME_cardoperationexpiration, cardoperationexpiration);
	}

	/** Get Card Operation Expiration.
		@return Card Operation Expiration	  */
	public Timestamp getcardoperationexpiration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_cardoperationexpiration);
	}

	/** Set Chairs Number.
		@param chairsnumber Chairs Number	  */
	public void setchairsnumber (int chairsnumber)
	{
		set_Value (COLUMNNAME_chairsnumber, Integer.valueOf(chairsnumber));
	}

	/** Get Chairs Number.
		@return Chairs Number	  */
	public int getchairsnumber () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_chairsnumber);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Chassis Number.
		@param chassisnumber Chassis Number	  */
	public void setchassisnumber (String chassisnumber)
	{
		set_Value (COLUMNNAME_chassisnumber, chassisnumber);
	}

	/** Get Chassis Number.
		@return Chassis Number	  */
	public String getchassisnumber () 
	{
		return (String)get_Value(COLUMNNAME_chassisnumber);
	}

	/** Set Color.
		@param color Color	  */
	public void setcolor (String color)
	{
		set_Value (COLUMNNAME_color, color);
	}

	/** Get Color.
		@return Color	  */
	public String getcolor () 
	{
		return (String)get_Value(COLUMNNAME_color);
	}

	/** Set Cylinder Capacity.
		@param cylindercapacity Cylinder Capacity	  */
	public void setcylindercapacity (BigDecimal cylindercapacity)
	{
		set_Value (COLUMNNAME_cylindercapacity, cylindercapacity);
	}

	/** Get Cylinder Capacity.
		@return Cylinder Capacity	  */
	public BigDecimal getcylindercapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_cylindercapacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Date Affiliation.
		@param dateaffiliation Date Affiliation	  */
	public void setdateaffiliation (Timestamp dateaffiliation)
	{
		set_Value (COLUMNNAME_dateaffiliation, dateaffiliation);
	}

	/** Get Date Affiliation.
		@return Date Affiliation	  */
	public Timestamp getdateaffiliation () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateaffiliation);
	}

	/** Set Date Disaffiliation.
		@param datedisaffiliation Date Disaffiliation	  */
	public void setdatedisaffiliation (Timestamp datedisaffiliation)
	{
		set_Value (COLUMNNAME_datedisaffiliation, datedisaffiliation);
	}

	/** Get Date Disaffiliation.
		@return Date Disaffiliation	  */
	public Timestamp getdatedisaffiliation () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datedisaffiliation);
	}

	/** Set Date Expiration Affiliation.
		@param dateexpirationaffiliation Date Expiration Affiliation	  */
	public void setdateexpirationaffiliation (Timestamp dateexpirationaffiliation)
	{
		set_Value (COLUMNNAME_dateexpirationaffiliation, dateexpirationaffiliation);
	}

	/** Get Date Expiration Affiliation.
		@return Date Expiration Affiliation	  */
	public Timestamp getdateexpirationaffiliation () 
	{
		return (Timestamp)get_Value(COLUMNNAME_dateexpirationaffiliation);
	}

	/** Set Date Last Mileage.
		@param datelastmileage Date Last Mileage	  */
	public void setdatelastmileage (Timestamp datelastmileage)
	{
		set_Value (COLUMNNAME_datelastmileage, datelastmileage);
	}

	/** Get Date Last Mileage.
		@return Date Last Mileage	  */
	public Timestamp getdatelastmileage () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datelastmileage);
	}

	/** Set Date Last Programming.
		@param datelastprogramming Date Last Programming	  */
	public void setdatelastprogramming (Timestamp datelastprogramming)
	{
		set_Value (COLUMNNAME_datelastprogramming, datelastprogramming);
	}

	/** Get Date Last Programming.
		@return Date Last Programming	  */
	public Timestamp getdatelastprogramming () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datelastprogramming);
	}

	/** Set Date Technical Revision.
		@param datetechnicalrevision Date Technical Revision	  */
	public void setdatetechnicalrevision (Timestamp datetechnicalrevision)
	{
		set_Value (COLUMNNAME_datetechnicalrevision, datetechnicalrevision);
	}

	/** Get Date Technical Revision.
		@return Date Technical Revision	  */
	public Timestamp getdatetechnicalrevision () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datetechnicalrevision);
	}

	/** Set Date Transit License.
		@param datetransitlicense Date Transit License	  */
	public void setdatetransitlicense (Timestamp datetransitlicense)
	{
		set_Value (COLUMNNAME_datetransitlicense, datetransitlicense);
	}

	/** Get Date Transit License.
		@return Date Transit License	  */
	public Timestamp getdatetransitlicense () 
	{
		return (Timestamp)get_Value(COLUMNNAME_datetransitlicense);
	}

	public org.compiere.model.I_C_BPartner getdiagnosticcenter() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getdiagnosticcenter_ID(), get_TrxName());	}

	/** Set Giagnostic Center.
		@param diagnosticcenter_ID Giagnostic Center	  */
	public void setdiagnosticcenter_ID (int diagnosticcenter_ID)
	{
		if (diagnosticcenter_ID < 1) 
			set_Value (COLUMNNAME_diagnosticcenter_ID, null);
		else 
			set_Value (COLUMNNAME_diagnosticcenter_ID, Integer.valueOf(diagnosticcenter_ID));
	}

	/** Get Giagnostic Center.
		@return Giagnostic Center	  */
	public int getdiagnosticcenter_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_diagnosticcenter_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Extinguisher Expiration.
		@param extinguisherexpiration Extinguisher Expiration	  */
	public void setextinguisherexpiration (Timestamp extinguisherexpiration)
	{
		set_Value (COLUMNNAME_extinguisherexpiration, extinguisherexpiration);
	}

	/** Get Extinguisher Expiration.
		@return Extinguisher Expiration	  */
	public Timestamp getextinguisherexpiration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_extinguisherexpiration);
	}

	/** TARIFA MODELOS 2001, 2001 Y POSTERIOR = A */
	public static final String GROUPRATE_TARIFAMODELOS20012001YPOSTERIOR = "A";
	/** MODELO 1998,1999Y2000 = B */
	public static final String GROUPRATE_MODELO19981999Y2000 = "B";
	/** TARIFA MODELOS 2001 = C */
	public static final String GROUPRATE_TARIFAMODELOS2001 = "C";
	/** Set Group Rate.
		@param grouprate Group Rate	  */
	public void setgrouprate (String grouprate)
	{

		set_Value (COLUMNNAME_grouprate, grouprate);
	}

	/** Get Group Rate.
		@return Group Rate	  */
	public String getgrouprate () 
	{
		return (String)get_Value(COLUMNNAME_grouprate);
	}

	/** Set Trailer.
		@param istrailer Trailer	  */
	public void setistrailer (boolean istrailer)
	{
		set_Value (COLUMNNAME_istrailer, Boolean.valueOf(istrailer));
	}

	/** Get Trailer.
		@return Trailer	  */
	public boolean istrailer () 
	{
		Object oo = get_Value(COLUMNNAME_istrailer);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Truck Tractor.
		@param istrucktractor Truck Tractor	  */
	public void setistrucktractor (boolean istrucktractor)
	{
		set_Value (COLUMNNAME_istrucktractor, Boolean.valueOf(istrucktractor));
	}

	/** Get Truck Tractor.
		@return Truck Tractor	  */
	public boolean istrucktractor () 
	{
		Object oo = get_Value(COLUMNNAME_istrucktractor);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Kms.
		@param kms Kms	  */
	public void setkms (BigDecimal kms)
	{
		set_Value (COLUMNNAME_kms, kms);
	}

	/** Get Kms.
		@return Kms	  */
	public BigDecimal getkms () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_kms);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Last Mileage.
		@param lastmileage Last Mileage	  */
	public void setlastmileage (int lastmileage)
	{
		set_Value (COLUMNNAME_lastmileage, Integer.valueOf(lastmileage));
	}

	/** Get Last Mileage.
		@return Last Mileage	  */
	public int getlastmileage () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_lastmileage);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Liability Policy.
		@param liabilitypolicy Liability Policy	  */
	public void setliabilitypolicy (String liabilitypolicy)
	{
		set_Value (COLUMNNAME_liabilitypolicy, liabilitypolicy);
	}

	/** Get Liability Policy.
		@return Liability Policy	  */
	public String getliabilitypolicy () 
	{
		return (String)get_Value(COLUMNNAME_liabilitypolicy);
	}

	/** Set Liability Policy Expiration.
		@param liabilitypolicyexpiration Liability Policy Expiration	  */
	public void setliabilitypolicyexpiration (Timestamp liabilitypolicyexpiration)
	{
		set_Value (COLUMNNAME_liabilitypolicyexpiration, liabilitypolicyexpiration);
	}

	/** Get Liability Policy Expiration.
		@return Liability Policy Expiration	  */
	public Timestamp getliabilitypolicyexpiration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_liabilitypolicyexpiration);
	}

	/** Set Liability Policy Insurer.
		@param liabilitypolicyinsurer Liability Policy Insurer	  */
	public void setliabilitypolicyinsurer (String liabilitypolicyinsurer)
	{
		set_Value (COLUMNNAME_liabilitypolicyinsurer, liabilitypolicyinsurer);
	}

	/** Get Liability Policy Insurer.
		@return Liability Policy Insurer	  */
	public String getliabilitypolicyinsurer () 
	{
		return (String)get_Value(COLUMNNAME_liabilitypolicyinsurer);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (String Line)
	{
		set_Value (COLUMNNAME_Line, Line);
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public String getLine () 
	{
		return (String)get_Value(COLUMNNAME_Line);
	}

	/** Set Model.
		@param model Model	  */
	public void setmodel (int model)
	{
		set_Value (COLUMNNAME_model, Integer.valueOf(model));
	}

	/** Get Model.
		@return Model	  */
	public int getmodel () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_model);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Motor Number.
		@param motornumber Motor Number	  */
	public void setmotornumber (String motornumber)
	{
		set_Value (COLUMNNAME_motornumber, motornumber);
	}

	/** Get Motor Number.
		@return Motor Number	  */
	public String getmotornumber () 
	{
		return (String)get_Value(COLUMNNAME_motornumber);
	}

	/** Averiado = A */
	public static final String ODOMETER_Averiado = "A";
	/** Sin establecer = S */
	public static final String ODOMETER_SinEstablecer = "S";
	/** funcionando = F */
	public static final String ODOMETER_Funcionando = "F";
	/** Set Odometer.
		@param odometer Odometer	  */
	public void setodometer (String odometer)
	{

		set_Value (COLUMNNAME_odometer, odometer);
	}

	/** Get Odometer.
		@return Odometer	  */
	public String getodometer () 
	{
		return (String)get_Value(COLUMNNAME_odometer);
	}

	public org.compiere.model.I_M_Product getoilproduct() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getoilproduct_ID(), get_TrxName());	}

	/** Set Motor Oil Product.
		@param oilproduct_ID Motor Oil Product	  */
	public void setoilproduct_ID (int oilproduct_ID)
	{
		if (oilproduct_ID < 1) 
			set_Value (COLUMNNAME_oilproduct_ID, null);
		else 
			set_Value (COLUMNNAME_oilproduct_ID, Integer.valueOf(oilproduct_ID));
	}

	/** Get Motor Oil Product.
		@return Motor Oil Product	  */
	public int getoilproduct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_oilproduct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Passengers Seated.
		@param passengersseated Passengers Seated	  */
	public void setpassengersseated (int passengersseated)
	{
		set_Value (COLUMNNAME_passengersseated, Integer.valueOf(passengersseated));
	}

	/** Get Passengers Seated.
		@return Passengers Seated	  */
	public int getpassengersseated () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_passengersseated);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Passengers Standing.
		@param passengersstanding Passengers Standing	  */
	public void setpassengersstanding (int passengersstanding)
	{
		set_Value (COLUMNNAME_passengersstanding, Integer.valueOf(passengersstanding));
	}

	/** Get Passengers Standing.
		@return Passengers Standing	  */
	public int getpassengersstanding () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_passengersstanding);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Qty Tires.
		@param qtytires Qty Tires	  */
	public void setqtytires (int qtytires)
	{
		set_Value (COLUMNNAME_qtytires, Integer.valueOf(qtytires));
	}

	/** Get Qty Tires.
		@return Qty Tires	  */
	public int getqtytires () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_qtytires);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Repowered.
		@param repoweredde Repowered	  */
	public void setrepoweredde (String repoweredde)
	{
		set_Value (COLUMNNAME_repoweredde, repoweredde);
	}

	/** Get Repowered.
		@return Repowered	  */
	public String getrepoweredde () 
	{
		return (String)get_Value(COLUMNNAME_repoweredde);
	}

	/** CARGA = C */
	public static final String SERVICETYPE_CARGA = "C";
	/** PASAJEROS MASIVO = M */
	public static final String SERVICETYPE_PASAJEROSMASIVO = "M";
	/** PASAJEROS ESPECIAL = E */
	public static final String SERVICETYPE_PASAJEROSESPECIAL = "E";
	/** PASAJEROS COLECTIVO = P */
	public static final String SERVICETYPE_PASAJEROSCOLECTIVO = "P";
	/** PARTICULAR = R */
	public static final String SERVICETYPE_PARTICULAR = "R";
	/** MIXTO = X */
	public static final String SERVICETYPE_MIXTO = "X";
	/** Set Service Type.
		@param servicetype Service Type	  */
	public void setservicetype (String servicetype)
	{

		set_Value (COLUMNNAME_servicetype, servicetype);
	}

	/** Get Service Type.
		@return Service Type	  */
	public String getservicetype () 
	{
		return (String)get_Value(COLUMNNAME_servicetype);
	}

	public I_smj_bodytype getsmj_bodytype() throws RuntimeException
    {
		return (I_smj_bodytype)MTable.get(getCtx(), I_smj_bodytype.Table_Name)
			.getPO(getsmj_bodytype_ID(), get_TrxName());	}

	/** Set Body Type.
		@param smj_bodytype_ID Body Type	  */
	public void setsmj_bodytype_ID (int smj_bodytype_ID)
	{
		if (smj_bodytype_ID < 1) 
			set_Value (COLUMNNAME_smj_bodytype_ID, null);
		else 
			set_Value (COLUMNNAME_smj_bodytype_ID, Integer.valueOf(smj_bodytype_ID));
	}

	/** Get Body Type.
		@return Body Type	  */
	public int getsmj_bodytype_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_bodytype_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_brand getsmj_bodyworkbrand() throws RuntimeException
    {
		return (I_smj_brand)MTable.get(getCtx(), I_smj_brand.Table_Name)
			.getPO(getsmj_bodyworkbrand_ID(), get_TrxName());	}

	/** Set Bodywork Brand.
		@param smj_bodyworkbrand_ID Bodywork Brand	  */
	public void setsmj_bodyworkbrand_ID (int smj_bodyworkbrand_ID)
	{
		if (smj_bodyworkbrand_ID < 1) 
			set_Value (COLUMNNAME_smj_bodyworkbrand_ID, null);
		else 
			set_Value (COLUMNNAME_smj_bodyworkbrand_ID, Integer.valueOf(smj_bodyworkbrand_ID));
	}

	/** Get Bodywork Brand.
		@return Bodywork Brand	  */
	public int getsmj_bodyworkbrand_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_bodyworkbrand_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_brand getsmj_gearboxbrand() throws RuntimeException
    {
		return (I_smj_brand)MTable.get(getCtx(), I_smj_brand.Table_Name)
			.getPO(getsmj_gearboxbrand_ID(), get_TrxName());	}

	/** Set Gearbox Brand.
		@param smj_gearboxbrand_ID Gearbox Brand	  */
	public void setsmj_gearboxbrand_ID (int smj_gearboxbrand_ID)
	{
		if (smj_gearboxbrand_ID < 1) 
			set_Value (COLUMNNAME_smj_gearboxbrand_ID, null);
		else 
			set_Value (COLUMNNAME_smj_gearboxbrand_ID, Integer.valueOf(smj_gearboxbrand_ID));
	}

	/** Get Gearbox Brand.
		@return Gearbox Brand	  */
	public int getsmj_gearboxbrand_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_gearboxbrand_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Internal Number.
		@param smj_internalnumber Internal Number	  */
	public void setsmj_internalnumber (String smj_internalnumber)
	{
		set_Value (COLUMNNAME_smj_internalnumber, smj_internalnumber);
	}

	/** Get Internal Number.
		@return Internal Number	  */
	public String getsmj_internalnumber () 
	{
		return (String)get_Value(COLUMNNAME_smj_internalnumber);
	}

	public I_smj_brand getsmj_motorbrand() throws RuntimeException
    {
		return (I_smj_brand)MTable.get(getCtx(), I_smj_brand.Table_Name)
			.getPO(getsmj_motorbrand_ID(), get_TrxName());	}

	/** Set Motor Brand.
		@param smj_motorbrand_ID Motor Brand	  */
	public void setsmj_motorbrand_ID (int smj_motorbrand_ID)
	{
		if (smj_motorbrand_ID < 1) 
			set_Value (COLUMNNAME_smj_motorbrand_ID, null);
		else 
			set_Value (COLUMNNAME_smj_motorbrand_ID, Integer.valueOf(smj_motorbrand_ID));
	}

	/** Get Motor Brand.
		@return Motor Brand	  */
	public int getsmj_motorbrand_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_motorbrand_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Plate.
		@param smj_plate Plate	  */
	public void setsmj_plate (String smj_plate)
	{
		set_Value (COLUMNNAME_smj_plate, smj_plate);
	}

	/** Get Plate.
		@return Plate	  */
	public String getsmj_plate () 
	{
		return (String)get_Value(COLUMNNAME_smj_plate);
	}

	public I_smj_programmingGroup getsmj_programmingGroup() throws RuntimeException
    {
		return (I_smj_programmingGroup)MTable.get(getCtx(), I_smj_programmingGroup.Table_Name)
			.getPO(getsmj_programmingGroup_ID(), get_TrxName());	}

	/** Set Programming Group.
		@param smj_programmingGroup_ID 
		Programming Group
	  */
	public void setsmj_programmingGroup_ID (int smj_programmingGroup_ID)
	{
		if (smj_programmingGroup_ID < 1) 
			set_Value (COLUMNNAME_smj_programmingGroup_ID, null);
		else 
			set_Value (COLUMNNAME_smj_programmingGroup_ID, Integer.valueOf(smj_programmingGroup_ID));
	}

	/** Get Programming Group.
		@return Programming Group
	  */
	public int getsmj_programmingGroup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_programmingGroup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_servicerategroup getsmj_servicerategroup() throws RuntimeException
    {
		return (I_smj_servicerategroup)MTable.get(getCtx(), I_smj_servicerategroup.Table_Name)
			.getPO(getsmj_servicerategroup_ID(), get_TrxName());	}

	/** Set Service Rate Group.
		@param smj_servicerategroup_ID Service Rate Group	  */
	public void setsmj_servicerategroup_ID (int smj_servicerategroup_ID)
	{
		if (smj_servicerategroup_ID < 1) 
			set_Value (COLUMNNAME_smj_servicerategroup_ID, null);
		else 
			set_Value (COLUMNNAME_smj_servicerategroup_ID, Integer.valueOf(smj_servicerategroup_ID));
	}

	/** Get Service Rate Group.
		@return Service Rate Group	  */
	public int getsmj_servicerategroup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_servicerategroup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle.
		@param SMJ_Vehicle_ID Vehicle	  */
	public void setSMJ_Vehicle_ID (int SMJ_Vehicle_ID)
	{
		if (SMJ_Vehicle_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SMJ_Vehicle_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SMJ_Vehicle_ID, Integer.valueOf(SMJ_Vehicle_ID));
	}

	/** Get Vehicle.
		@return Vehicle	  */
	public int getSMJ_Vehicle_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SMJ_Vehicle_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_brand getsmj_vehiclebrand() throws RuntimeException
    {
		return (I_smj_brand)MTable.get(getCtx(), I_smj_brand.Table_Name)
			.getPO(getsmj_vehiclebrand_ID(), get_TrxName());	}

	/** Set Vehicle Brand.
		@param smj_vehiclebrand_ID Vehicle Brand	  */
	public void setsmj_vehiclebrand_ID (int smj_vehiclebrand_ID)
	{
		if (smj_vehiclebrand_ID < 1) 
			set_Value (COLUMNNAME_smj_vehiclebrand_ID, null);
		else 
			set_Value (COLUMNNAME_smj_vehiclebrand_ID, Integer.valueOf(smj_vehiclebrand_ID));
	}

	/** Get Vehicle Brand.
		@return Vehicle Brand	  */
	public int getsmj_vehiclebrand_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_vehiclebrand_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Soat Expiration.
		@param soatexpiration Soat Expiration	  */
	public void setsoatexpiration (Timestamp soatexpiration)
	{
		set_Value (COLUMNNAME_soatexpiration, soatexpiration);
	}

	/** Get Soat Expiration.
		@return Soat Expiration	  */
	public Timestamp getsoatexpiration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_soatexpiration);
	}

	public org.compiere.model.I_C_BPartner getsoatinsurer() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getsoatinsurer_ID(), get_TrxName());	}

	/** Set Soat Insurer.
		@param soatinsurer_ID Soat Insurer	  */
	public void setsoatinsurer_ID (int soatinsurer_ID)
	{
		if (soatinsurer_ID < 1) 
			set_Value (COLUMNNAME_soatinsurer_ID, null);
		else 
			set_Value (COLUMNNAME_soatinsurer_ID, Integer.valueOf(soatinsurer_ID));
	}

	/** Get Soat Insurer.
		@return Soat Insurer	  */
	public int getsoatinsurer_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_soatinsurer_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Soat Number.
		@param soatnumber Soat Number	  */
	public void setsoatnumber (String soatnumber)
	{
		set_Value (COLUMNNAME_soatnumber, soatnumber);
	}

	/** Get Soat Number.
		@return Soat Number	  */
	public String getsoatnumber () 
	{
		return (String)get_Value(COLUMNNAME_soatnumber);
	}

	/** Set Technical Revision Number.
		@param technicalrevisionnumber Technical Revision Number	  */
	public void settechnicalrevisionnumber (String technicalrevisionnumber)
	{
		set_Value (COLUMNNAME_technicalrevisionnumber, technicalrevisionnumber);
	}

	/** Get Technical Revision Number.
		@return Technical Revision Number	  */
	public String gettechnicalrevisionnumber () 
	{
		return (String)get_Value(COLUMNNAME_technicalrevisionnumber);
	}

	/** Set Technical Revision Expiration.
		@param technicalrevisionnumberexpira Technical Revision Expiration	  */
	public void settechnicalrevisionnumberexpira (Timestamp technicalrevisionnumberexpira)
	{
		set_Value (COLUMNNAME_technicalrevisionnumberexpira, technicalrevisionnumberexpira);
	}

	/** Get Technical Revision Expiration.
		@return Technical Revision Expiration	  */
	public Timestamp gettechnicalrevisionnumberexpira () 
	{
		return (Timestamp)get_Value(COLUMNNAME_technicalrevisionnumberexpira);
	}

	/** Set Tourniquet.
		@param tourniquet Tourniquet	  */
	public void settourniquet (int tourniquet)
	{
		set_Value (COLUMNNAME_tourniquet, Integer.valueOf(tourniquet));
	}

	/** Get Tourniquet.
		@return Tourniquet	  */
	public int gettourniquet () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_tourniquet);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transit License.
		@param transitlicense Transit License	  */
	public void settransitlicense (String transitlicense)
	{
		set_Value (COLUMNNAME_transitlicense, transitlicense);
	}

	/** Get Transit License.
		@return Transit License	  */
	public String gettransitlicense () 
	{
		return (String)get_Value(COLUMNNAME_transitlicense);
	}

	/** RECAUDO DIARIO Y PROGRAMACIÓN MANTENIMIENTO = R */
	public static final String TYPEADMINISTRATION_RECAUDODIARIOYPROGRAMACIONMANTENIMIENTO = "R";
	/** RECAUDO DIARIO = D */
	public static final String TYPEADMINISTRATION_RECAUDODIARIO = "D";
	/** CONTROL DOCUMENTOS = C */
	public static final String TYPEADMINISTRATION_CONTROLDOCUMENTOS = "C";
	/** PROGRAMACIÓN VIAJES = V */
	public static final String TYPEADMINISTRATION_PROGRAMACIONVIAJES = "V";
	/** Set Type Administration.
		@param typeadministration Type Administration	  */
	public void settypeadministration (String typeadministration)
	{

		set_Value (COLUMNNAME_typeadministration, typeadministration);
	}

	/** Get Type Administration.
		@return Type Administration	  */
	public String gettypeadministration () 
	{
		return (String)get_Value(COLUMNNAME_typeadministration);
	}

	/** CAMION = C */
	public static final String VEHICLETYPE_CAMION = "C";
	/** AUTOMOVIL = A */
	public static final String VEHICLETYPE_AUTOMOVIL = "A";
	/** DOBLETROQUE = D */
	public static final String VEHICLETYPE_DOBLETROQUE = "D";
	/** TURBO = T */
	public static final String VEHICLETYPE_TURBO = "T";
	/** CAMIONETA = M */
	public static final String VEHICLETYPE_CAMIONETA = "M";
	/** BUS = B */
	public static final String VEHICLETYPE_BUS = "B";
	/** TRACTOCAMION = X */
	public static final String VEHICLETYPE_TRACTOCAMION = "X";
	/** MINIMULA = L */
	public static final String VEHICLETYPE_MINIMULA = "L";
	/** CARGA OTRO = O */
	public static final String VEHICLETYPE_CARGAOTRO = "O";
	/** REMOLQUE = R */
	public static final String VEHICLETYPE_REMOLQUE = "R";
	/** BUSETA = S */
	public static final String VEHICLETYPE_BUSETA = "S";
	/** MICROBUS = U */
	public static final String VEHICLETYPE_MICROBUS = "U";
	/** GRUA = G */
	public static final String VEHICLETYPE_GRUA = "G";
	/** SEMIREMOLQUE = SR */
	public static final String VEHICLETYPE_SEMIREMOLQUE = "SR";
	/** Set Vehicle Type.
		@param vehicletype 
		Vehicle Type
	  */
	public void setvehicletype (String vehicletype)
	{

		set_Value (COLUMNNAME_vehicletype, vehicletype);
	}

	/** Get Vehicle Type.
		@return Vehicle Type
	  */
	public String getvehicletype () 
	{
		return (String)get_Value(COLUMNNAME_vehicletype);
	}
}