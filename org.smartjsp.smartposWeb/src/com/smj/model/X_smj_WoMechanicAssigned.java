/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.smj.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for smj_WoMechanicAssigned
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_smj_WoMechanicAssigned extends PO implements I_smj_WoMechanicAssigned, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20130510L;

    /** Standard Constructor */
    public X_smj_WoMechanicAssigned (Properties ctx, int smj_WoMechanicAssigned_ID, String trxName)
    {
      super (ctx, smj_WoMechanicAssigned_ID, trxName);
      /** if (smj_WoMechanicAssigned_ID == 0)
        {
			setappliedfee (Env.ZERO);
			setC_BPartner_ID (0);
			setsmj_iscommission (false);
			setsmj_WoMechanicAssigned_ID (0);
			setsmj_workOrderLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_smj_WoMechanicAssigned (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_smj_WoMechanicAssigned[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Applied Fee.
		@param appliedfee Applied Fee	  */
	public void setappliedfee (BigDecimal appliedfee)
	{
		set_Value (COLUMNNAME_appliedfee, appliedfee);
	}

	/** Get Applied Fee.
		@return Applied Fee	  */
	public BigDecimal getappliedfee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_appliedfee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Commission.
		@param smj_iscommission Commission	  */
	public void setsmj_iscommission (boolean smj_iscommission)
	{
		set_Value (COLUMNNAME_smj_iscommission, Boolean.valueOf(smj_iscommission));
	}

	/** Get Commission.
		@return Commission	  */
	public boolean issmj_iscommission () 
	{
		Object oo = get_Value(COLUMNNAME_smj_iscommission);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Mechanic Assigned.
		@param smj_WoMechanicAssigned_ID 
		Work Order Mechanic Assigned
	  */
	public void setsmj_WoMechanicAssigned_ID (int smj_WoMechanicAssigned_ID)
	{
		if (smj_WoMechanicAssigned_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_smj_WoMechanicAssigned_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_smj_WoMechanicAssigned_ID, Integer.valueOf(smj_WoMechanicAssigned_ID));
	}

	/** Get Mechanic Assigned.
		@return Work Order Mechanic Assigned
	  */
	public int getsmj_WoMechanicAssigned_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_WoMechanicAssigned_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_smj_workOrderLine getsmj_workOrderLine() throws RuntimeException
    {
		return (I_smj_workOrderLine)MTable.get(getCtx(), I_smj_workOrderLine.Table_Name)
			.getPO(getsmj_workOrderLine_ID(), get_TrxName());	}

	/** Set Work Order Line.
		@param smj_workOrderLine_ID Work Order Line	  */
	public void setsmj_workOrderLine_ID (int smj_workOrderLine_ID)
	{
		if (smj_workOrderLine_ID < 1) 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_smj_workOrderLine_ID, Integer.valueOf(smj_workOrderLine_ID));
	}

	/** Get Work Order Line.
		@return Work Order Line	  */
	public int getsmj_workOrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_smj_workOrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}