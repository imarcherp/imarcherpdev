package com.smj.model;

import java.math.BigDecimal;

import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.smj.util.DataQueries;

/**
 * Create an Simple Invoice for insurance companies
 * @author pedrorozo
 *
 */


public class InvoiceInsurance {
	protected CLogger log = CLogger.getCLogger(super.getClass());
	private String plan;
	private BigDecimal coverage;
	private int insuranceID;


	public Integer generate(Trx trx, MOrder order,String desc)
	{
		MInvoice invoice = createInvoice(trx,order);
		createInvoiceLines(invoice, trx,order);

		if (!invoice.save())
			return null;
		
		invoice.setDescription(desc);
		invoice.completeIt();
		invoice.setDocStatus(DocAction.STATUS_Completed);
		invoice.setDocAction(DocAction.ACTION_Close);
		invoice.save();
		
		return invoice.get_ID();
	}

	private MInvoice createInvoice(Trx trx, MOrder order)
	{
		MInvoice data = new MInvoice(Env.getCtx(), 0, trx.getTrxName());
		data.setClientOrg(order.getAD_Client_ID(), order.getAD_Org_ID());
		data.setOrder(order);
		data.setAD_Org_ID(order.getAD_Org_ID());
		data.setIsActive(true);
		data.setDateInvoiced(order.getDateOrdered());
		data.setDateAcct(order.getDateAcct());		
		data.setC_BPartner_ID(insuranceID);   // SMJ 5
		MBPartner bp = new MBPartner(Env.getCtx(), order.getC_BPartner_ID(), trx.getTrxName());
		data.setDescription("Insurance Customer:"+bp.getName()+" -Plan: "+plan+" Copay: "+coverage);
		data.setC_BPartner_Location_ID(DataQueries.getLocationPartner(order.getC_BPartner_ID()));
		data.setC_Currency_ID(order.getC_Currency_ID() )   ;
		data.setM_PriceList_ID(order.getM_PriceList_ID());
		data.setPOReference(order.getPOReference());
		data.setProcessed(true);
		data.setPaymentRule("S");
		data.setIsSOTrx(true);
		int docType = getInvoiceDocTypeIdInsuranceFromClient(order.getAD_Client_ID() , trx);
		data.setC_DocType_ID(docType);
		data.setC_DocTypeTarget_ID(docType);
		data.setSalesRep_ID(order.getSalesRep_ID());		
		data.setIsSOTrx(order.isSOTrx());

		if (!data.save()){
			return null;
		}

		trx.commit();

		return data;
	}

	private void createInvoiceLines(MInvoice invoice,Trx trx,MOrder order)
	{

		MOrderLine[] oLineas = order.getLines(); 
		BigDecimal total = new BigDecimal(0);
		//LinkedList<HashMap<String, Object>> lista = (LinkedList<HashMap<String, Object>>) to.get("m_aLine");
		Boolean lok = false;
		MInvoiceLine line = null;
		for (int k = 0; k < oLineas.length ; k++){
			if (oLineas[k].getM_Product_ID() <= 0 ) {
				line = new MInvoiceLine(Env.getCtx(), 0, trx.getTrxName());
				line.setAD_Org_ID(oLineas[k].getAD_Org_ID());
				line.set_ValueOfColumn("ad_client_id",oLineas[k].getAD_Client_ID() );	
				line.setIsActive(true);
				line.setC_Invoice_ID(invoice.getC_Invoice_ID());
				line.setC_Charge_ID(oLineas[k].getC_Charge_ID());
				line.setM_Product_ID(oLineas[k].getM_Product_ID());
				MBPartner bp = new MBPartner(Env.getCtx(), order.getC_BPartner_ID(), trx.getTrxName());
				line.setDescription("Insurance Customer:"+bp.getName()+" -Plan: "+plan+" Copay: "+coverage);				

				line.setQtyEntered(oLineas[k].getQtyEntered());				
				line.setPrice(oLineas[k].getPriceActual() );
				line.setPriceActual(oLineas[k].getPriceActual().multiply(new BigDecimal(-1)));
				line.setPriceList(oLineas[k].getPriceActual().multiply(new BigDecimal(-1))) ;
				line.setQtyInvoiced(oLineas[k].getQtyEntered());
				BigDecimal totalLine = oLineas[k].getQtyEntered().multiply(oLineas[k].getPriceActual());
				line.setLineNetAmt(totalLine);
				total = total.add(totalLine);
				line.setC_Tax_ID(oLineas[k].getC_Tax_ID());  //taxud
				line.setC_UOM_ID(oLineas[k].getC_UOM_ID()); //   uomid
				lok = line.save();
				trx.commit();
				if (!lok){
					throw new IllegalStateException("Could not Create Invoice Encounter: "+line.toString());
				}
			}

		} // for

		invoice.setTotalLines(total);
	}




	private int getInvoiceDocTypeIdInsuranceFromClient(int clientId,Trx trx)
	{
		String docTypeSQl = "SELECT dt.c_doctype_id FROM C_DocType dt "
				+ "WHERE dt.ad_client_id=? and name = 'AP Invoice' and isactive='Y'";

		int docTypeId = DB.getSQLValue(trx.getTrxName(), docTypeSQl, clientId);

		return docTypeId;
	}

	public void setIdInsurance(Integer idInsurance) {
		this.insuranceID = idInsurance;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public void setCoverage(BigDecimal coverage) {
		this.coverage = coverage;
	}




}
