package com.smj.model;

import java.math.BigDecimal;

public class InfoProductList {
	
	private Integer productID;
	private String productName;
	private String productValue;
	private Integer partnerId;
	private String partnerName;
	private BigDecimal price;
	private Integer listVersionId;
	private BigDecimal qty;
	private Integer warehouseId;
	private String warehouseName;
	private Integer locatorId;
	private BigDecimal qtyEntered;
	private BigDecimal total;
	private Integer uomId;
	private String uomName;
	private BigDecimal tax;
	private Integer attInstanceId;
	private BigDecimal purchasePrice;
	private Boolean isService = false;
	private Boolean isExternal = false;
	private BigDecimal estimedTime;
	private Boolean isAllowChange = false;
	
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductValue() {
		return productValue;
	}
	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}
	public Integer getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getListVersionId() {
		return listVersionId;
	}
	public void setListVersionId(Integer listVersionId) {
		this.listVersionId = listVersionId;
	}
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public Integer getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	public Integer getLocatorId() {
		return locatorId;
	}
	public void setLocatorId(Integer locatorId) {
		this.locatorId = locatorId;
	}
	public BigDecimal getQtyEntered() {
		return qtyEntered;
	}
	public void setQtyEntered(BigDecimal qtyEntered) {
		this.qtyEntered = qtyEntered;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Integer getUomId() {
		return uomId;
	}
	public void setUomId(Integer uomId) {
		this.uomId = uomId;
	}
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public Integer getAttInstanceId() {
		return attInstanceId;
	}
	public void setAttInstanceId(Integer attInstanceId) {
		this.attInstanceId = attInstanceId;
	}
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public Boolean getIsService() {
		return isService;
	}
	public void setIsService(Boolean isService) {
		this.isService = isService;
	}
	public Boolean getIsExternal() {
		return isExternal;
	}
	public void setIsExternal(Boolean isExternal) {
		this.isExternal = isExternal;
	}
	public BigDecimal getEstimedTime() {
		return estimedTime;
	}
	public void setEstimedTime(BigDecimal estimedTime) {
		this.estimedTime = estimedTime;
	}
	public Boolean getIsAllowChange() {
		return isAllowChange;
	}
	public void setIsAllowChange(Boolean isAllowChange) {
		this.isAllowChange = isAllowChange;
	}
	
}//InfoProductList
