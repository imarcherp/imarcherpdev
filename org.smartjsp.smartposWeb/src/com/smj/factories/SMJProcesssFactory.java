package com.smj.factories;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.smj.process.BankCashTransfer;
import org.smj.process.ReverseCloseCash;

public class SMJProcesssFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {
		
		if (className.equalsIgnoreCase("org.smj.process.BankCashTransfer"))
			return new BankCashTransfer();
		
		if (className.equalsIgnoreCase("org.smj.process.ReverseCloseCash"))
			return new ReverseCloseCash();
		
		return null;
	}

}
