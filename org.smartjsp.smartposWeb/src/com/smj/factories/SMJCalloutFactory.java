package com.smj.factories;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.I_C_BPartner;
import org.compiere.model.I_C_BankAccount;
import org.compiere.model.I_M_Warehouse;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class SMJCalloutFactory implements IColumnCalloutFactory {

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName,
			String columnName) {

		if (tableName.equalsIgnoreCase(I_C_BPartner.Table_Name)) {
			if (columnName.equalsIgnoreCase(I_C_BPartner.COLUMNNAME_TaxID)) 
				return new IColumnCallout[]{new ValidateUniqueTaxID()};				
		}

		//Table: M_Warehouse
		if (tableName.equalsIgnoreCase(I_M_Warehouse.Table_Name)) {
			if (columnName.equals(I_C_BankAccount.COLUMNNAME_C_BankAccount_ID)) 
				return new IColumnCallout[]{new OnlyBAccountByWarehouse()};
			
		}
		
		//End: M_Warehouse
		
		return null;
	}

	/**
	 * Table: C_BPartner
	 * @author Dany
	 *
	 */
	private static class ValidateUniqueTaxID implements IColumnCallout {

		@Override
		public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
			
			if (value != null) {
				int c_bpartner_id = new Query(Env.getCtx(), I_C_BPartner.Table_Name, "TaxID = '" + value + "'", null).firstId();
				
				if (c_bpartner_id > 0) {
					mTab.setValue(mField, null);
					return Msg.translate(Env.getCtx(), "SMJ_MSGTaxIDAlreadyExist");
				}
			}
			
			return "";
		}

	}

	/**
	 * Table: M_Warehouse
	 * @author Dany
	 *
	 */
	private static class OnlyBAccountByWarehouse implements IColumnCallout {
		@Override
		public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
			
			if (value != null && value != oldValue) {
				Integer C_BankAccount_ID = (Integer) value;
				Integer M_Warehouse_ID = (Integer) mTab.getValue(I_M_Warehouse.COLUMNNAME_M_Warehouse_ID);
				String sql = "SELECT M_Warehouse_ID FROM M_Warehouse WHERE C_BankAccount_ID = ? AND M_Warehouse_ID <> ?";
				int M_WarehouseSource_ID = DB.getSQLValueEx(null, sql, C_BankAccount_ID, M_Warehouse_ID);
				
				if (M_WarehouseSource_ID > 0) {
					mTab.setValue("C_BankAccount_ID", null);
					MWarehouse warehouse = MWarehouse.get(Env.getCtx(), M_WarehouseSource_ID);
					return Msg.parseTranslation(Env.getCtx(), "@SMJ_MsgBankAccountUsed@ @M_Warehouse_ID@: " + warehouse.getName());
				}
			}
			
			return null;
		}		
	}
}
