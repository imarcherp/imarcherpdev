package com.smj.factories;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJTmpWebSales;
import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.model.MSMJVehicle;

public class SMJModelFactory implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
		
		if (tableName.equals(MSMJTmpWebSalesLine.Table_Name))
			return MSMJTmpWebSalesLine.class;
		
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		
		if (tableName.equals(MSMJTmpWebSales.Table_Name))
			return new MSMJTmpWebSales(Env.getCtx(), Record_ID, trxName);
		
		if (tableName.equals(MSMJTmpWebSalesLine.Table_Name))
			return new MSMJTmpWebSalesLine(Env.getCtx(), Record_ID, trxName);
		
		if (tableName.equals(MSMJServiceRateGroup.Table_Name))
			return new MSMJServiceRateGroup(Env.getCtx(), Record_ID, trxName);
		
		if (tableName.equals(MSMJVehicle.Table_Name)) 
			return new MSMJVehicle(Env.getCtx(), Record_ID, trxName);
		
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		
		if (tableName.equals(MSMJTmpWebSales.Table_Name))
			return new MSMJTmpWebSales(Env.getCtx(), rs, trxName);
		
		if (tableName.equals(MSMJTmpWebSalesLine.Table_Name))
			return new MSMJTmpWebSalesLine(Env.getCtx(), rs, trxName);
		
		if (tableName.equals(MSMJServiceRateGroup.Table_Name))
			return new MSMJServiceRateGroup(Env.getCtx(), rs, trxName);
		
		if (tableName.equals(MSMJVehicle.Table_Name)) 
			return new MSMJVehicle(Env.getCtx(), rs, trxName);
		
		return null;
	}

}
