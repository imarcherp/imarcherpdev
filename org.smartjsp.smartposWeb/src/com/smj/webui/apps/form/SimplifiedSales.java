package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MProduct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.smj.model.MSMJTmpWebSalesLine;
//import org.compiere.minigrid.IMiniTable;

/**
 * @version <li>SmartJSP: FuelSales, 2013/03/06
 *          <ul TYPE ="circle">
 *          <li>logica de la forma de venta simplificada
 *          <li>logic to form simplified sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SimplifiedSales {
	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(SimplifiedSales.class);
	
	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	public Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelCode"));
	//	columnNames.add(Msg.translate(Env.getCtx(), "smj_oldproductcode"));
	//	columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProd"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelUom"));   //
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelPrice"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelQty"));	
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTotal"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTax"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelDestination"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelNotes"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelIsSent"));
	//	columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		return columnNames;
	}//getTableColumnNames
	
	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	public void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true); // code
	//	table.setColumnClass(i++, String.class, true);
	//	table.setColumnClass(i++, String.class, true); 
		table.setColumnClass(i++, String.class, true); // prod
		table.setColumnClass(i++, String.class, true); //uom
		table.setColumnClass(i++, BigDecimal.class, true); //price
		table.setColumnClass(i++, BigDecimal.class, false);   //qty	
		table.setColumnClass(i++, BigDecimal.class, true);   //total
    //	table.setColumnClass(i++, String.class, true); 
		table.setColumnClass(i++, BigDecimal.class, true); // tax
		table.setColumnClass(i++, String.class, true); // destiantion
		table.setColumnClass(i++, String.class, true); //notes
		table.setColumnClass(i++, String.class, true); // issent
		//  Table UI
		table.autoSize();
	}//setTableColumnClass
	
	/**
	 * regresa la lista de temporary Sales Web
	 * returns Tmp Web Sales list
	 * @param clientId
	 * @return Vector<Vector<Object>> 
	 */
	protected Vector<Vector<Object>> getTmpWebSales(Integer clientId, Integer orgId){
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT t.smj_tmpWebSales_ID, b.name, t.created FROM smj_tmpWebSales t,  C_BPartner b ");
			sql.append(" WHERE t.isActive = 'Y' AND t.AD_Client_ID = "+clientId);
			sql.append(" AND t.AD_Org_ID = "+orgId);
			sql.append(" AND b.C_BPartner_ID = t.C_BPartner_ID ORDER BY name ASC ");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			Vector<Object> line = new Vector<Object>();
			while (rs.next ())	{
				line = new Vector<Object>();
				line.add(rs.getInt("smj_tmpWebSales_ID"));
				Timestamp cDate = rs.getTimestamp("created");
				String dato = rs.getString("name")+" ** "+sdf.format(cDate);
				line.add(dato);
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getTmpWebSales
	
	/**
	 * regresa las lineas de la venta temportal - 
	 * retuns temp web sales lines
	 * @param code
	 * @return
	 */
	protected LinkedList<MSMJTmpWebSalesLine>getTmpWebSalesLines(Integer code){
		LinkedList<MSMJTmpWebSalesLine> data = new LinkedList<MSMJTmpWebSalesLine>();
		StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * FROM smj_tmpWebSalesLine t WHERE  smj_tmpWebSales_ID = "+code);
			sql.append(" ORDER BY Line ASC ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeQuery ();
			MSMJTmpWebSalesLine line = null;
			while (rs.next ())	{
				Integer id = rs.getInt("smj_tmpWebSalesLine_ID");
				line = new MSMJTmpWebSalesLine(Env.getCtx(),id,null);
				line.setsmj_tmpwebsales_ID(code);
				line.setM_Product_ID(rs.getInt("M_Product_ID"));
				line.setProductName(rs.getString("productName"));
				line.setProductValue(rs.getString("productValue"));
				line.setC_BPartner_ID(rs.getInt("C_BPartner_ID"));
				line.setpartnername(rs.getString("partnerName"));
				line.setPriceEntered(rs.getBigDecimal("price"));
				line.setQtyOrdered(rs.getBigDecimal("qty"));
				line.setM_Warehouse_ID(rs.getInt("M_Warehouse_ID"));
				line.setWarehouseName(rs.getString("warehouseName"));
				line.setM_Locator_ID(rs.getInt("M_Locator_ID"));
				line.setQtyEntered(rs.getBigDecimal("qtyEntered"));
				line.settotal(rs.getBigDecimal("total"));
				line.setC_UOM_ID(rs.getInt("C_Uom_ID"));
				line.setUOMName(rs.getString("uomName"));
				line.settax(rs.getBigDecimal("tax"));
				line.setM_AttributeSetInstance_ID(rs.getInt("M_attributeSetInstance_ID"));
				//line.setsmj_vehicle_ID(rs.getInt("smj_vehicle_ID"));
				//line.setsmj_plate(rs.getString("smj_plate"));
				line.setLine(rs.getInt("Line"));
				line.setsmj_destination(rs.getString("smj_destination"));
				line.setsmj_notes(rs.getString("smj_notes"));
				line.setsmj_isworkorder(rs.getString("smj_isworkorder"));
				MProduct p = new MProduct(Env.getCtx(), line.getM_Product_ID(), null);
				//line.setOldCode(p.getSKU());
				data.add(line);
			}//while
		}catch (Exception e){
			log.log (Level.SEVERE, sql.toString(), e);
		}
		finally	{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}//getTmpWebSales

		
	/**
	 * regresa el listado de lineas temporates de la orden de trabajo - 
	 * returns temp line from work orden
	 * @param woId
	 * @return
	 */
	protected LinkedList<Integer> getWOTmpLines(Integer woId, Integer tmpId) {
		LinkedList<Integer> value = new LinkedList<Integer>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SMJ_TmpWebSalesLine_ID FROM SMJ_TmpWebSalesLine ");
		sql.append(" WHERE R_Request_ID = "+woId+" ");
		sql.append(" AND smj_TmpWebSales_ID = "+tmpId+" ");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()){
				Integer code = rs.getInt("SMJ_TmpWebSalesLine_ID");
				value.add(code);
			}//while
			
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".getWOTmpLines - ERROR: " + sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return value;
	}// getWOTmpLines
	
}//SimplifiedSales
