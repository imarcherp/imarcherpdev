package com.smj.webui.apps.form;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.I_AD_Reference;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MProduct;
import org.compiere.model.MRMA;
import org.compiere.model.MRMALine;
import org.compiere.model.Query;
import org.compiere.model.X_M_RMAType;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.smj.model.InOutFromRma;
import org.smj.model.InvoiceFromRma;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Row;
import org.zkoss.zul.Window;

import com.smj.util.DataQueries;
import com.smj.util.ShowWindow;

/**
 * Ventana para realizar la devolucion simplificada de un producto
 * Window for simplified return of a product
 * @author Dany
 *
 */
public class WSimplifiedReturns extends Window implements IFormController {

	private static final long serialVersionUID = 1L;
	public static CLogger log = CLogger.getCLogger(WSimplifiedReturns.class);
	private Integer orgId = Env.getAD_Org_ID(Env.getCtx());
	private Integer clientId = Env.getAD_Client_ID(Env.getCtx());
	private Trx trx = Trx.get(Trx.createTrxName("WSR" + Calendar.getInstance().getTimeInMillis()), true);
	private final String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private LinkedList<MInOutLine> inOutLines = new LinkedList<MInOutLine>();
	private MInOut inOut;
	private ArrayList<BigDecimal> qtyProducts = new ArrayList<BigDecimal>();
	private int AD_Column_ID_Invoice = 3484; // C_Invoice.C_Invoice_ID
	
	public WSimplifiedReturns() {
		try {

			if (validateLoginOrg()) {
				form.detach();
			} else {
				initComponents();
				fillReturnType();
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, "WSimplifiedSales constructor error", e);
		}		
	}

	/**
	 * valida que se ingresa con una organizacion - validate log in with
	 * organization
	 */
	private boolean validateLoginOrg() {
		boolean closew = false;
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (orgId <= 0) {
				Messagebox.showDialog(Msg.translate(Env.getCtx(),
						"SMJMSGEnterWithOrganization"), labelInfo,
						Messagebox.OK, Messagebox.EXCLAMATION);
				closew = true;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".validateLoginOrg - ERROR: " + e.getMessage(), e);
		}
		return closew;
	}// validateLoginOrg
	
	@Override
	public ADForm getForm() {
		return form;
	}
	
	private void loadTable() {
		tblProducts.clear();
		
		ArrayList<ArrayList<Object>> data = getData();
		
		ListModelTable model = new ListModelTable(data);
		
		if (data.size() <= 0) {
			return;
		}
		
		model.addTableModelListener(new WTableModelListener() {			
			@Override
			public void tableChanged(WTableModelEvent event) {
				tbltProductsTableChanged(event);				
			}
		});
		
		tblProducts.setData(model, getTableColumnNames());
		setTableColumnClass(tblProducts);
		
		for (Component component : tblProducts.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}
	
	private void fillReturnType() {
		LinkedList<X_M_RMAType> lstRMAType = DataQueries.getAllRMATypes(trx.getTrxName());
		if (lstRMAType == null || lstRMAType.size() <= 0) {
			Messagebox.showDialog(translate("SMJ-MSJNoRMATypes"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			form.setVisible(false);
			return;
		}
		
		for (X_M_RMAType rmaType : lstRMAType) {
			cmbReturnType.appendItem(rmaType.getName(),rmaType);
		}
		
		cmbReturnType.setSelectedIndex(0);
	}
	
	private void initComponents() throws Exception {
		form.appendChild(mainLayout);
		grid.setParent(mainLayout);
		grid.appendChild(rows);
		
		txtBusinessPartner.setEnabled(false);
		txtDate.setEnabled(false);		
		txtDate.setValue(Env.getContextAsDate(Env.getCtx(), "#Date").toString());
		chkAffectsInventory.setSelected(true);
		
		String whereClause = I_AD_Reference.COLUMNNAME_Name + " LIKE 'Invoice Info (Simplified Returns)'";			
		Integer AD_Reference_ID = new Query(Env.getCtx(), I_AD_Reference.Table_Name, whereClause, null).firstIdOnly();
		MLookup lookup = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), AD_Column_ID_Invoice, DisplayType.Search, Env.getLanguage(Env.getCtx()), "C_Invoice_ID", AD_Reference_ID, true, null);
		txtInvoice = new WSearchEditor("C_Invoice_ID", true, false, true, lookup);
		
		row = rows.newRow();
		row.appendChild(newCell("9%", "right", lblInvoice));
		row.appendChild(newCell("24%", "left", txtInvoice.getComponent()));
		row.appendChild(newCell("10%", "right", lblBusinessPartner));
		row.appendChild(newCell("24%", "left", txtBusinessPartner));
		row.appendChild(newCell("9%", "right", lblDate));
		row.appendChild(newCell("24%", "left", txtDate));
		
		row = rows.newRow();
		row.appendChild(newCell("9%", "right", lblReturnType));
		row.appendChild(newCell("24%", "left", cmbReturnType));
		row.appendChild(newCell("10%", "right", lblAffectsInventory));
		row.appendChild(newCell("24%", "left", chkAffectsInventory));
		
		row = rows.newRow();	
		row.appendChild(newCell("100%", "center", 6, tblProducts));
		
		row = rows.newRow();
		row.appendChild(newCell("100%", "center", 3, btnCheckAll));
		row.appendChild(newCell("100%", "center", 3, btnUnCheckAll));
		
		row = rows.newRow();
		row.appendChild(newCell("100%", "center", 6, btnProccess));
		
		//add Events
		txtInvoice.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent evt) {
				txtInvoiceOnBlur(evt);
			}
		});
		
		btnProccess.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnProccessOnClick();
			}
		});
		
		btnCheckAll.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnCheckAllOnClick();
			}
		});
		
		btnUnCheckAll.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnUnCheckAllOnClick();
			}
		});
		
		tblProducts.getModel().addTableModelListener(new WTableModelListener() {
			@Override
			public void tableChanged(WTableModelEvent event) {
				tbltProductsTableChanged(event);
			}
		});
		
		form.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				formOnClose();
			}
		});
	}
	
	private Cell newCell(String width, String align, int colspan, Component component) {
		Cell cell = new Cell();
		cell.setWidth(width);
		cell.setAlign(align);
		cell.setColspan(colspan);
		cell.appendChild(component);
		
		return cell;
	}
	
	private Cell newCell(String width, String align, Component component) {
		return newCell(width, align, 1, component);
	}
	
	/**
	 * 
	 */
	private ArrayList<ArrayList<Object>> getData() {
		LinkedList<LinkedList<Object>> deliveredProducts = DataQueries.getRMALinesByInOutID(inOut.getM_InOut_ID());
		ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		qtyProducts.clear();
		
		for (int i = 0; i < inOutLines.size(); i++) {
			if (inOutLines.get(i).getProduct() == null) {
				inOutLines.remove(i);
			}
		}
		
		for (int i = 0; i < inOutLines.size(); i++) {
			qtyProducts.add(inOutLines.get(i).getQtyEntered());
		}
		
		//validate delivered qty
		if (deliveredProducts != null && deliveredProducts.size() > 0) {
			for (int i = 0; i < inOutLines.size(); i++) {
				MInOutLine inOutLine = inOutLines.get(i);
				for (int j = 0; j < deliveredProducts.size(); j++) {
					LinkedList<Object> deliveredProduct = deliveredProducts.get(j);
					if (((Integer)deliveredProduct.get(0)) == inOutLine.getM_InOutLine_ID()) {
						BigDecimal qty = inOutLine.getQtyEntered().subtract((BigDecimal) deliveredProduct.get(1));
						if (qty.compareTo(new BigDecimal(0)) < 1) {
							inOutLines.remove(i);
							qtyProducts.remove(i);
							i--;
						} else {
							qtyProducts.set(i,qty);
						}
						break;
					}
				}
			}
		}
		
		for (int i = 0; i < inOutLines.size(); i++) {
			MInOutLine line = inOutLines.get(i);
			if (line.getProduct().get_ID() > 0) {
				ArrayList<Object> tmp = new ArrayList<Object>();
				MProduct mProduct = line.getProduct();
				tmp.add(false);
				tmp.add(mProduct.getValue());
				tmp.add(mProduct.getName()); 
				tmp.add(line.getC_UOM().getName());
				BigDecimal price = line.getC_OrderLine().getPriceEntered();
				tmp.add(price);
				tmp.add(qtyProducts.get(i));
				tmp.add(price.multiply(line.getQtyEntered()));
				tmp.add(line.getC_OrderLine().getC_Tax().getRate());
				data.add(tmp);
			} 
		}
		
		return data;
	}
	
	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("");
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelCode"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProd"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelUom"));   //
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelPrice"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelQty"));	
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTotal"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTax"));
		return columnNames;
	}//getTableColumnNames
	
	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, Boolean.class, false); //is selected?
		table.setColumnClass(i++, String.class, true); // code
		table.setColumnClass(i++, String.class, true); // prod
		table.setColumnClass(i++, String.class, true); //uom
		table.setColumnClass(i++, BigDecimal.class, true); //price
		table.setColumnClass(i++, BigDecimal.class, false);   //qty	
		table.setColumnClass(i++, BigDecimal.class, true);   //total
		table.setColumnClass(i++, BigDecimal.class, true); // tax
		table.autoSize();
	}//setTableColumnClass
	
	private void cleanFields() {
		btnProccess.setEnabled(false);
		txtBusinessPartner.setValue("");
		cmbReturnType.setSelectedIndex(0);
		chkAffectsInventory.setSelected(true);
		tblProducts.clear();
		qtyProducts.clear();
	}
	
	/**
	 * Method for get Selected items from Table
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<Object>> getSelectedItems() {
		int size = tblProducts.getItems().size();
		ArrayList<ArrayList<Object>> lstSelectedItems = new ArrayList<ArrayList<Object>>();

		for (int i = 0; i < size; i++) {
			ArrayList<Object> dato = (ArrayList<Object>) tblProducts.getModel().get(i);
			if ((Boolean) dato.get(0)) {
				dato.add(inOutLines.get(i));
				lstSelectedItems.add(dato);
			}
		}
		
		return lstSelectedItems;
	}
	
	@SuppressWarnings("unchecked")
	private void checkOrUncheckAll(Boolean selected) {
		int size = tblProducts.getModel().getSize();

		for (int i = 0; i < size; i++) {
			ArrayList<Object> item = (ArrayList<Object>) tblProducts.getModel().get(i);
			item.set(0, selected);
			tblProducts.getModel().set(i, item);
		}
	}
	
	/**
	 * Translates a message
	 * @param text
	 * @return
	 */
	private String translate(String text) {
		return Msg.translate(Env.getCtx(), text).trim();
	}
	
	// Methods for events
	
	private void txtInvoiceOnBlur(ValueChangeEvent evt) {
		Integer invoiceId = (Integer) evt.getNewValue();
		
		if (invoiceId == null || invoiceId <= 0) {
			cleanFields();
			return;
		}
		
		MInvoice invoice = new MInvoice(Env.getCtx(), invoiceId, trx.getTrxName());
		txtBusinessPartner.setValue(invoice.getC_BPartner().getName());
		inOut = DataQueries.getInOutByOrderId(invoice.getC_Order_ID(), trx.getTrxName());
		//inOut.getC_Order()
		
		if (inOut == null) {
			cleanFields();
			txtInvoice.setValue("");
			txtInvoice.getComponent().setFocus(true);
			return;
		}
		
		inOutLines = DataQueries.getInOutLines(inOut.get_ID(), trx.getTrxName());
		
		if (inOutLines == null || inOutLines.size() <= 0) {
			cleanFields();
			return;
		}
		
		btnProccess.setEnabled(true);
		loadTable();
	}
	
	private void btnProccessOnClick() {
		ArrayList<ArrayList<Object>> lstSelectedItems = getSelectedItems();
		
		if (lstSelectedItems == null || lstSelectedItems.size() < 1) {
			Messagebox.showDialog(translate("SMJ-MSJNoItemsSelected"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		
		//create RMA
		MRMA rma = new MRMA(Env.getCtx(), 0, trx.getTrxName());
		rma.set_ValueOfColumn("ad_client_id", inOut.getAD_Client_ID());
		rma.setAD_Org_ID(orgId);
		rma.setIsActive(true);		
		rma.setGenerateTo("N");
		rma.setDocAction(DocAction.ACTION_Complete);
		rma.setDocStatus(DocAction.STATUS_Drafted);
		rma.setIsSOTrx(true);
		rma.setProcessed(true);
		rma.setIsApproved(true);
		rma.setDescription(inOut.getDescription());		
		rma.setC_BPartner_ID(inOut.getC_BPartner_ID());
		rma.setC_Currency_ID(inOut.getC_Currency_ID());
		rma.setSalesRep_ID(inOut.getSalesRep_ID());
		rma.setInOut_ID(inOut.get_ID()); 
		
		X_M_RMAType rmaType = cmbReturnType.getSelectedItem().getValue();
		rma.setName(rmaType.getName());
		rma.setM_RMAType_ID(rmaType.get_ID());
		rma.setC_DocType_ID(DataQueries.getDocumentTypeId("Customer Return Material", clientId));
		
		if (!rma.save()) {
			trx.rollback();
			return;
		}
		
		//Add RMALine From Selected Items
		
		for (ArrayList<Object> line : lstSelectedItems) {
			MRMALine rmaLine = new MRMALine(Env.getCtx(), 0, trx.getTrxName());
			MInOutLine inOutLine = (MInOutLine)line.get(8);
			BigDecimal qty = (BigDecimal) line.get(5);
			BigDecimal divideRate = DataQueries.getUOMDivideRate(inOutLine.getM_Product_ID(), inOutLine.getC_UOM_ID());
			
			rmaLine.set_ValueOfColumn("ad_client_id",inOutLine.getAD_Client_ID());
			rmaLine.setAD_Org_ID(orgId);
			rmaLine.setM_RMA_ID(rma.get_ID());
			rmaLine.setIsActive(true);
			
			rmaLine.setQty(qty);
			rmaLine.setQtyDelivered(qty.multiply(divideRate));
			rmaLine.setProcessed(true);
			rmaLine.setLine(inOutLine.getLine());
			rmaLine.setM_InOutLine_ID(inOutLine.get_ID());
			rmaLine.setDescription(inOutLine.getDescription() + "\n" + line.get(2));
			
			if (!rmaLine.save()) {
				trx.rollback();
				return;
			}
		}
		
		rma.setDocAction(DocAction.ACTION_Close);
		rma.setDocStatus(DocAction.STATUS_Completed);
		
		if (!rma.save()) {
			trx.rollback();
			return;
		}
		
		Timestamp date = Env.getContextAsDate(Env.getCtx(), "#Date");
		
		//Create InOut if affects inventory
		if (chkAffectsInventory.isChecked()) {
			InOutFromRma rma2InOut = new InOutFromRma();
			MInOut inOutTmp = rma2InOut.generateInOutFromRMA(rma.get_ID(), trx);
			inOutTmp.setDateAcct(date);
			inOutTmp.setDateReceived(date);
			inOutTmp.setMovementDate(date);
			
			if (!inOutTmp.save()) {
				trx.rollback();
				return;
			}
		}
		
		InvoiceFromRma rma2Invoice = new InvoiceFromRma();
		MInvoice invoice = rma2Invoice.generateInvoiceFromRMA(rma.get_ID(), trx);
		invoice.setDateAcct(date);
		invoice.setPOReference("SmartPOS");
		invoice.setDateInvoiced(date);
		invoice.setC_Order_ID(inOut.getC_Order_ID());
		
		if (!invoice.save()) {
			trx.rollback();
			return;
		}
		
		trx.commit();
		cleanFields();
		txtInvoice.setValue("");
		qtyProducts = new ArrayList<BigDecimal>();
		ShowWindow.openInvoiceCustomer(invoice.get_ID());
	}
	
	private void btnCheckAllOnClick() {
		checkOrUncheckAll(true);
	}
	
	private void btnUnCheckAllOnClick() {
		checkOrUncheckAll(false);
	}
	
	@SuppressWarnings("unchecked")
	private void tbltProductsTableChanged(WTableModelEvent evx) {
		
		if (evx.getColumn() == 0) {
			return;
		}
		
		ArrayList<Object> line = (ArrayList<Object>) tblProducts.getModel().get(evx.getIndex0());
		BigDecimal qtyRecorded = qtyProducts.get(evx.getIndex0());
		
		BigDecimal qty = (BigDecimal) line.get(5);
		Boolean greaterThanRecorded = qty.compareTo(qtyRecorded) == 1;
		Boolean lessThanOne = qty.compareTo(new BigDecimal(0)) < 1;
		
		if (greaterThanRecorded || lessThanOne) {
			
			if (greaterThanRecorded) {
				Messagebox.showDialog(translate("SMJ-MSJValueRecordedValidate"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			} else {
				Messagebox.showDialog(translate("SMJ-MSJMustBeGreaterThanZero"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
			
			line.set(5, qtyRecorded);
		}
		
		BigDecimal total = ((BigDecimal) line.get(5)).multiply((BigDecimal) line.get(4));
		line.set(6, total);
		tblProducts.getModel().set(evx.getIndex0(), line);
	}
	
	private void formOnClose() {
		trx.close();
		SessionManager.getAppDesktop().closeActiveWindow();
	}
	
	//Form Fields
	private final CustomForm form = new CustomForm();
	private final Window mainLayout = new Window();
	
	private final Grid grid = GridFactory.newGridLayout();
	private final Rows rows = new Rows();
	private Row row;
	
	private WSearchEditor txtInvoice;
	private final Textbox txtBusinessPartner = new Textbox();
	private final Textbox txtDate = new Textbox();
	private final Combobox cmbReturnType = new Combobox();
	private final Checkbox chkAffectsInventory = new Checkbox();
	private final WListbox tblProducts = ListboxFactory.newDataTable();
	
	private final Label lblInvoice = new Label(translate("Invoice"));
	private final Label lblBusinessPartner = new Label(translate("BusPartner"));
	private final Label lblDate = new Label(translate("Date"));
	private final Label lblReturnType = new Label(translate("SMJLabelRMAType"));
	private final Label lblAffectsInventory = new Label(translate("SMJLabelAffectsInventory"));
	
	private final Button btnProccess = new Button(translate("SMJLabelProcess"));
	private final Button btnCheckAll = new Button(translate("SMJLabelCheckAll"));
	private final Button btnUnCheckAll = new Button(translate("SMJLabelUncheckAll"));
}
