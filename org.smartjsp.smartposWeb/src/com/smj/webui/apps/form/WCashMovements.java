package com.smj.webui.apps.form;

import java.util.Calendar;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.compiere.model.MPayment;
import org.compiere.model.MWarehouse;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Row;
import org.zkoss.zul.Window;

import com.smj.util.Message;
import com.smj.util.SMJConfig;
import com.smj.util.ShowWindow;

/**
 * Ventana para realizar movimientos de caja
 * Window for cash movements
 * @author Dany
 *
 */
public class WCashMovements extends Window implements IFormController {

	private Integer orgId = Env.getAD_Org_ID(Env.getCtx());
	private Trx trx = Trx.get(Trx.createTrxName("WCM" + Calendar.getInstance().getTimeInMillis()), true);
	private final String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	public static CLogger log = CLogger.getCLogger(WCashMovements.class);
	private int C_BankAccount_ID = 0;
	private static final long serialVersionUID = 1L;

	public WCashMovements() {
		
		try {

			if (validateLoginOrg() || !validateLoginWarehouse()) { // if login was not for an specific org it is closed
				form.detach();
			} else {
				initComponents();
				addCSS();
				fill();
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, "WSimplifiedSales constructor error", e);
			SessionManager.getAppDesktop().closeActiveWindow();
		}// try/catch
				
	}

	@Override
	public ADForm getForm() {
		return form;
	}

	/**
	 * Translates a message
	 * @param text
	 * @return
	 */
	private String translate(String text) {
		return Msg.translate(Env.getCtx(), text).trim();
	}
	
	/**
	 * Fills Combo Boxes
	 */
	private void fill() {
		movType.appendItem(translate("SMJLabelCashIn"));
		movType.appendItem(translate("SMJLabelCashOut"));
		movType.setSelectedIndex(0);
	}

	/**
	 * Returns the default fields
	 */
	private void clean() {
		movType.setSelectedIndex(0);
		value.setValue(0);
		description.setValue("");
	}
	
	/**
	 * Adds CSS to the fields
	 */
	private void addCSS() {
		String saveBtnStyle = "border-color: #15cb3a #00991f #00841c;background: linear-gradient(#00c328,#00ab23);background: -webkit-linear-gradient(#00c328,#00ab23);background-color: #00bb23;color: #fff;text-shadow: 0 -1px rgba(0,0,0,.15);";
		processButton.setStyle(saveBtnStyle);
	}
	
	/**
	 * Validates fields
	 * @return
	 */
	private boolean validate() {
		if (value.getValue().compareTo(Env.ZERO) <= 0) {
			Messagebox.showDialog(translate("SMJ-MSJMustBeGreaterThanZero"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			return false;
		}

		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		if (M_Warehouse_ID < 1) {
			Message.showError("SMJMSGEnterWithWarehouse");
			return false;
		}
		
		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		boolean isPos = warehouse.get_ValueAsBoolean("ispos");
		if (!isPos) {
			Message.showError("SMJ_MsgWarehouseIsNotPOS");
			return false;
		}
		
		C_BankAccount_ID = warehouse.get_ValueAsInt("C_BankAccount_ID");
		
		return true;
	}
	
	private void initComponents() {
		form.appendChild(mainLayout);
		grid.setParent(mainLayout);
		grid.appendChild(rows);
		
		value.setValue(0);

		row = rows.newRow();
		row.appendChild(movTypeLabel.rightAlign());
		row.appendChild(movType);

		row = rows.newRow();
		row.appendChild(valueLabel.rightAlign());
		row.appendChild(value);

		row = rows.newRow();
		row.appendChild(descriptionLabel.rightAlign());
		description.setRows(5);
		row.appendChild(description);

		row = rows.newRow();
		row.appendChild(new Label());
		row.appendChild(processButton);

		// <<<< add events >>>>

		form.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				formOnClose();
			}
		});

		processButton.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				saveButtonOnClick();
			}
		});
	}

	private void formOnClose() {
		clean();
		trx.close();
		SessionManager.getAppDesktop().closeActiveWindow();
	}

	private void saveButtonOnClick() {
		if (!validate()) {
			return;
		}
		
		MPayment mPayment = new MPayment(Env.getCtx(), 0, trx.getTrxName());
		mPayment.setAD_Org_ID(orgId);
		mPayment.setC_DocType_ID(movType.getSelectedIndex() == 0);
		
		mPayment.setDescription("SmartPOS " + description.getValue());
		mPayment.setPayAmt(value.getValue());
		mPayment.setTenderType(MPayment.TENDERTYPE_Cash);
		mPayment.setDateAcct(Env.getContextAsDate(Env.getCtx(), "#Date"));
		mPayment.setDateTrx(Env.getContextAsDate(Env.getCtx(), "#Date"));
		
		mPayment.setC_BPartner_ID(SMJConfig.getIntValue("C_BPartner_ID", 0, orgId));
		mPayment.setC_BankAccount_ID(C_BankAccount_ID);
		mPayment.setC_Currency_ID(SMJConfig.getIntValue("C_Currency_ID", 0, orgId));
		
		mPayment.completeIt();
		mPayment.setDocStatus(DocAction.STATUS_Completed);
		mPayment.setDocAction(DocAction.STATUS_Closed);
		mPayment.setProcessed(true);
		
		if (mPayment.save()) {			
			trx.commit();			
			clean();
			ShowWindow.openPayment(mPayment.getC_Payment_ID());
		} else {
			Messagebox.showDialog(translate("SMJ-MSJProcessUnsuccessfully"), labelInfo, Messagebox.OK, Messagebox.ERROR);
		}
	}

	//Form Fields
	private final CustomForm form = new CustomForm();
	private final Window mainLayout = new Window();

	private final Grid grid = GridFactory.newGridLayout();
	private final Rows rows = new Rows();
	private Row row;

	private final NumberBox value = new NumberBox(false, true);
	private final Combobox movType = new Combobox();
	private final Textbox description = new Textbox();

	private final Label valueLabel = new Label(translate("SMJLabelValue"));
	private final Label movTypeLabel = new Label(translate("SMJLabelMovementType"));
	private final Label descriptionLabel = new Label(translate("SMJLabelDescription"));

	private final Button processButton = new Button(translate("SMJLabelProcess"));
	
	/**
	 * valida que se ingresa con una organizacion - validate log in with
	 * organization
	 */
	private boolean validateLoginOrg() {
		boolean closew = false;
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (orgId <= 0) {
				Message.showWarning("SMJMSGEnterWithOrganization");
				closew = true;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".validateLoginOrg - ERROR: " + e.getMessage(), e);
		}
		return closew;
	}// validateLoginOrg

	private boolean validateLoginWarehouse() {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		if (M_Warehouse_ID < 1) {
			Message.showWarning("SMJMSGEnterWithWarehouse");
			return false;
		}

		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		boolean ispos = warehouse.get_ValueAsBoolean("ispos");

		if (!ispos) {
			Message.showWarning("SMJ_MsgWarehouseIsNotPOS");
			return false;
		}

		return true;
	}
	
}
