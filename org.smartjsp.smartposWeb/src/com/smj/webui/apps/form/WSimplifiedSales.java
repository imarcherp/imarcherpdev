package com.smj.webui.apps.form;

import java.io.BufferedInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.util.ProcessUtil;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListCell;
import org.adempiere.webui.component.ListHead;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.ZkCssHelper;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.I_AD_Reference;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.MBPartner;
import org.compiere.model.MImage;
import org.compiere.model.MLocator;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrderTax;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MPeriod;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProcess;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRequest;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.model.X_AD_Attachment;
import org.compiere.model.X_M_Substitute;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.smj.model.MSmjInsurancePlan;
import org.smj.print.LocalPrint;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Space;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;

import com.smj.model.InfoProductList;
import com.smj.model.MSMJServiceRateGroup;
import com.smj.model.MSMJTmpWebSales;
import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.model.MSMJVehicle;
import com.smj.model.MSMJWorkOrderLine;
import com.smj.util.DataQueries;
import com.smj.util.Documents;
import com.smj.util.Message;
import com.smj.util.SMJConfig;
import com.smj.util.ShowWindow;
import com.smj.util.Util;
import com.smj.webui.component.SMJChangeUnit;
import com.smj.webui.component.SMJDiscountWindow;
import com.smj.webui.component.SMJInfoProduct;
import com.smj.webui.component.SMJInsuranceCoverage;
import com.smj.webui.component.SMJOpenTickets;
import com.smj.webui.component.SMJPaymentWindow;
import com.smj.webui.component.SMJSearchProducts;
import com.smj.webui.component.SMJSentCancel;
import com.smj.webui.component.SMJSubstituteProduct;
import com.smj.webui.component.SMJZoomImage;
import com.smj.webui.renders.SMJListItemRenderer;

/**
 * @version <li>SmartJSP: WSimplifiedSales
 *          <ul TYPE ="circle">
 *          <li>Clase para maneja la forma de venta simplificada (POS)
 *          <li>Class to form simplified sales (POS)
 *          </ul>
 * @author Pedro Rozo - "SmartJSP" - http://www.smartjsp.com/
 */
public class WSimplifiedSales extends Window implements IFormController,
WTableModelListener, ValueChangeListener, EventListener<Event> {

	private static final long serialVersionUID = 4169799434596051062L;
	private Integer lclientId = Env.getAD_Client_ID(Env.getCtx());
	private Integer lorgId = Env.getAD_Org_ID(Env.getCtx());
	private Boolean activeLoyalty = MSysConfig.getBooleanValue("SMJ_USE_LOYALTY", false, lclientId, lorgId);
	private Boolean useTransport = MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
	private int catCombosPOS = SMJConfig.getIntValue("M_Product_Category_ID", 0, lorgId);
	private Integer currentUser = Env.getAD_User_ID(Env.getCtx());
	private Boolean discountsRestricted = false;
	public static CLogger log = CLogger.getCLogger(SimplifiedSales.class);
	private Integer purchaseListVersionId = 0;
	private String[] printers;
	private Integer salesListVersionId = 0;
	private Integer insuranceId = 0;
	private SMJSentCancel sentCancel;
	private SMJChangeUnit changeUnit;
	private SMJSubstituteProduct substitute;
	private SMJOpenTickets tickets;
	private Timer timer;
	private BigDecimal bPartnerDiscount;
	private Integer m_warehouse_id = 0;
	private MOrder order;
	//iMarch-- variable para cambiar check de solo lectura segun el tercero
	private String BPartnerOnlyOV = MSysConfig.getValue("iMarch_BPartnerOnlyOV","0", Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
	private boolean orgValidateProductPVW = MSysConfig.getBooleanValue("iM_OrgValidateProductoPVW", false, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
	//Informe-Proceso impresion jasper		
	public static final String JASPER_STARTER_CLASS = "org.adempiere.report.jasper.ReportStarter";
	int m_AD_Process_ID = MSysConfig.getIntValue("iM_PrintTicket", 0);	//ID Informe / Proceso creado donde se relaciona el Jasper
	//fin iMarch-----

	private CustomForm form = new CustomForm();

	@Override
	public ADForm getForm() {
		return form;
	}

	/**
	 * WSimplifiedSales constructor
	 */
	public WSimplifiedSales() {
		try {
			m_warehouse_id = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		} catch (Exception e) {
			m_warehouse_id = 0;
		}
		try {
			if (validateLoginOrg() || !validateLoginWarehouse()) { // if login was not for an specific org it is closed
				form.detach();
			} else {
				initOpenTickets();
				dynInit();
				zkInit();
				LocalPrint lp = new LocalPrint();
				lp.listAvailablePrinters();
				form.addEventListener(Events.ON_CLOSE, this);
				this.addEventListener("onPayment", this);
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, "WSimplifiedSales constructor error", e);
			SessionManager.getAppDesktop().closeActiveWindow();
		}// try/catch

	}// WSimplifiedSales

	// variables de la forma web (paneles)
	private Window mainLayout = new Window();
	private Vbox vmainLayout = new Vbox();
	private Grid custLayout = GridFactory.newGridLayout();
	private Window pcustLayout = new Window();
	private Hbox prodLayout = new Hbox();
	private Window pprodLayout = new Window();
	private Vbox gLayout = new Vbox();
	private Window pgLayout = new Window();
	private Hbox barLayout = new Hbox();
	private Window pbarLayout = new Window();
	private Vbox totalLayout = new Vbox();
	private Hbox t1Layout = new Hbox();
	private Hbox t2Layout = new Hbox();
	private Window ptotalLayout = new Window();
	private Hbox tLine1 = new Hbox();
	private Hbox tLine2 = new Hbox();
	private WListbox dataTable = ListboxFactory.newDataTable();
	// campos a ser mostrados
	private Label productLabel = new Label();
	private Label bPartnerLabel = new Label();
	private Label bPartnerPlanLabel = new Label();
	private Label ticketLabel = new Label();
	private WSearchEditor bpartnerSearch = null;
	private WSearchEditor productSearch = null;
	private Button deleteButton = new Button();
	private Button btnDiscounts = new Button();
	private Button btnExit = new Button();
	private Button sendButton = new Button();
	private Button newCButton = new Button();
	private Button clearCButton = new Button();
	private Button clearButton = new Button();
	private Button cancelButton = new Button();
	private Button preticketButton = new Button();
	private Button plusButton = new Button();
	private Button minusButton = new Button();
	private Button payButton = new Button();
	private Label totalLabel = new Label();
	private Label totalDecimalbox = new Label();
	private Label subTotalLabel = new Label();
	private Label subTotalDecimalbox = new Label();
	private Label taxLabel = new Label();
	private Label taxDecimalbox = new Label();
	private Label dctLabel = new Label();
	private Label dctDecimalbox = new Label();
	private Label tmpSalesLabel = new Label();
	private Label lblCredit = new Label();
	private Listbox tmpSalesListbox = new Listbox();
	private Textbox bpartnerTextBox = new Textbox();
	private Textbox txtCredit = new Textbox();
	private String selectedPlate = "";
	private Integer vehicleID = 0;
	private Integer paymentTerm = 0;
	private Button deleteAllButton = new Button();
	private Textbox woTextBox = new Textbox();
	private Label entryDateLabel = new Label();
	private Label lblDateScheduled = new Label();
	private WDateEditor entryDate = new WDateEditor();
	private WDateEditor dateScheduled = new WDateEditor();
	private Integer sequence = 0;
	private Date dateInvoice;
	private int currentSelectedTicket = -1;

	// campos
	private Integer bpartnerId = 0;
	private String bpartnerName = "";
	private Integer locationID = 0;
	private List<MSMJTmpWebSalesLine> list = new LinkedList<MSMJTmpWebSalesLine>();
	private MSMJTmpWebSales tmp = null;
	private BigDecimal grandTotal = Env.ZERO;
	private BigDecimal taxTotal = Env.ZERO;
	private BigDecimal subTotal = Env.ZERO;
	private MLookup lookupProduct = null;
	private MLookup lookupBP = null;
	private int AD_Column_ID_BPartner = 2893; // C_BPartner.C_BPartner_ID
	private int AD_Column_ID_Product = 1402; // M_Product.MProduct_ID
	private int defaultCustomerId = 0;
	private BigDecimal m_coverageP = Env.ZERO;
	private BigDecimal m_coverageM = Env.ZERO;
	private BigDecimal m_totalB = Env.ZERO;
	private BigDecimal m_covered = Env.ZERO;
	private BigDecimal m_copay = Env.ZERO;
	private BigDecimal m_totalA = Env.ZERO;
	private String m_insuranceC = "";
	private String m_insuranceP = "";

	// codigos generales
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();
	private final String insuranceDiscountLabel = Msg.translate(Env.getCtx(), "SMJLabelInsuranceDiscount").trim();
	private NumberFormat nf;

	//Index Table
	private int indexSent;
	
	/**
	 * Prepara los campos dinamicos Dynamic Init (prepare dynamic fields)
	 * 
	 * @throws Exception
	 *             if Lookups cannot be initialized
	 */
	private void dynInit() throws Exception {
		try {

			if (validateLoginOrg() || !validateLoginWarehouse()) { // if login was not for an specific org, it is closed
				SessionManager.getAppDesktop().closeActiveWindow();
			}

			discountsRestricted = SMJConfig.getBooleanValue("IsDiscountRestricted", false, lorgId);
			printers = SMJConfig.getArrayStringValue("POS_PrintersByGroup", ";", lorgId);
			purchaseListVersionId = SMJConfig.getIntValue("M_PriceList_Version_ID", 0, lorgId);
			defaultCustomerId = SMJConfig.getIntValue("C_BPartner_ID", 0, lorgId);
			Integer numDec = SMJConfig.getIntValue("POS_Decimals", 0, lorgId);
			String[] localeVals = SMJConfig.getArrayStringValue("POS_Locale", ",", lorgId);
			nf = NumberFormat.getCurrencyInstance(new Locale(localeVals[0].trim(), localeVals[1].trim()));
			nf.setMaximumFractionDigits(numDec);
			nf.setMinimumFractionDigits(numDec);

			lookupBP = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), 0, AD_Column_ID_BPartner, DisplayType.Search);
			bpartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
			bpartnerSearch.addValueChangeListener(this);

			String whereClause = I_AD_Reference.COLUMNNAME_Name + " LIKE 'Search Product (SmartPOS)'";			
			Integer AD_Reference_ID = new Query(Env.getCtx(), I_AD_Reference.Table_Name, whereClause, null).firstIdOnly();
			lookupProduct = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), AD_Column_ID_Product, DisplayType.Search, Env.getLanguage(Env.getCtx()), "M_Product_ID", AD_Reference_ID, true, null);
			productSearch = new WSearchEditor("M_Product_ID", true, false, true, lookupProduct);
			productSearch.addValueChangeListener(this);
			productSearch.getComponent().addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					productSearch.setValue(null);
				}
			});

			list = new LinkedList<MSMJTmpWebSalesLine>(); // lista de objetos de la tabla
			listTmpSales();
			Calendar c = Calendar.getInstance();
			Timestamp time = Env.getContextAsDate(Env.getCtx(), "#Date");
			c.setTimeInMillis(time.getTime());
			c.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		    c.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));
		    c.set(Calendar.SECOND, Calendar.getInstance().get(Calendar.SECOND));
		    c.set(Calendar.MILLISECOND, 0);
		    time.setTime(c.getTimeInMillis());
		    entryDate.setValue(time);
		    dateScheduled.setValue(time);
			dateInvoice = new Date(time.getTime());
			validateSalesRep();

		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".dynInit - ERROR: " + e.getMessage(), e);
		}
	}// dynInit

	/**
	 * Sirve para asignar un Timer a la ventana, en el cual se mantendra actualizado el
	 * estado de la orden seleccionada, de estar paga, mostrara un mensaje...
	 * 
	 * Used to assign a Timer to the window, where it will be kept updated status of the selected order, to be paid, display a message ...
	 */
	private void runTimer() {
		Integer delay = SMJConfig.getIntValue("POS_CheckOrdersTime", 10000, lorgId);

		if (timer != null && timer.isRunning())
			timer.stop();

		timer = new Timer(delay);
		timer.setRepeats(true);
		timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				
				final Integer invoiceId = validateCloseTicket();

				if (invoiceId > 0) {	
					timer.stop();
					Messagebox.showDialog(Msg.translate(Env.getCtx(),
							"SMJMSGTicketClosedWithInvoice"), labelInfo, Messagebox.OK
							| Messagebox.CANCEL, Messagebox.QUESTION,
							new Callback<Integer>() {
						@Override
						public void onCallback(Integer result) {
							if (result.equals(Messagebox.OK)) {
								ShowWindow.openInvoiceCustomer(invoiceId);
							}
						}
					});		
					clean();
					initOpenTickets();
				} else if (invoiceId == 0) {
					Message.showWarning("SMJMSGTicketClosedNoInvoice");
					SessionManager.getAppDesktop().closeActiveWindow();
				}
			}
		});
		mainLayout.appendChild(timer);
		timer.start();
	}

	/**
	 * Create Columns with its Width
	 * @param widths
	 * @return
	 */
	private Columns getColumns(String ... widths) {
		Columns columns = new Columns();

		for (String width : widths) {
			Column column = new Column();
			column.setWidth(width);
			columns.appendChild(column);
		}

		return columns;
	}

	/**
	 * Set Custom width to the columns
	 * @param head
	 * @param widths
	 */
	public void setWidths(ListHead head) {

		List<String> widths = new ArrayList<String>();
		widths.add("7.8%");
		if (useTransport) {
			widths.add("9%");
			widths.add("9%");
		}
		widths.add("20%");
		widths.add("5%");
		widths.add("9%");
		widths.add("7.8%");
		widths.add("7.8%");
		widths.add("9%");
		widths.add("5%");
		widths.add("7.8%");
		widths.add("10%");
		widths.add("5.8%");
		if (useTransport) widths.add("8%");
		widths.add("6%");

		for (int i = 0; i < head.getChildren().size(); i++) {
			ListHeader header = (ListHeader) head.getChildren().get(i);
			//header.setHflex("max");
			//header.setWidth(widths.get(i));
			ZKUpdateUtil.setHflex(header, "min");
			ZKUpdateUtil.setWidth(header, widths.get(i));
		}
	}

	/**
	 * Inicializa los componentes estaticos Static Init (inicializa los
	 * componentes estaticos)
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private void zkInit() throws Exception {
		String borderStyle = "border: 1px solid #C0C0C0; border-radius:5px;";
		String borderStyle2 = "border: 1px solid #C0C0C0; border-radius:5px;";
		String totalStyle = "border: 2px solid #C0C0C0; border-radius:3px;font-size: 13px;font-weight: bolder;;color: white ;background:#088A08;";
		String labelStyle = "font-size: 9.7px;color: #0B610B;background:#FFFFFF;";

		String buttonImg = "background:white;Height:35px;width:30px;border: 0px solid #610B0B;border-radius:10px;padding: 1px 1px;";
		String buttonRed = "background:red;Height:35px;Width:200px;border: 3px solid #848484;border-radius:8px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: white";
		String buttonGreen = "background:#04B404;Height:35px;Width:200px;border: 3px solid #848484;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: white";
		String buttonYellow = "background:yellow;Height:35px;Width:200px;border: 3px solid #848484;border-radius:8px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: black";
		String buttonTomato = "background:#ffc477;Height:35px;Width:100px;border: 3px solid#848484;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: black;"; // text-shadow:2px
		String buttonOrange = "background:#FFA500;Height:35px;Width:80px;border: 3px solid#848484;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: black;"; // text-shadow:2px
		String buttonGreenYellow = "background:#ADFF2F;Height:35px;Width:80px;border: 3px solid#848484;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: black;"; // text-shadow:2px
		String buttonGray = "background:#BDBDBD;Height:35px;Width:80px;border: 3px solid #848484;border-radius:10px;padding: 1px 1px;font-family: arial;font-size: 12px;font-weight: bolder;color: black;"; // text-shadow:2px
		String rowStyle = "Height:30px;";

		//form.appendChild(mainLayout);

		//mainLayout.setHeight("100%");
		//mainLayout.setVflex("true");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		ZKUpdateUtil.setVflex(mainLayout, "true");

		//vmainLayout.setWidth("99%");
		//vmainLayout.setHeight("100%");
		ZKUpdateUtil.setWidth(vmainLayout, "99%");
		ZKUpdateUtil.setHeight(vmainLayout, "100%");
		vmainLayout.setHeights("10%,10%,70%,10%");
		vmainLayout.setStyle("margin-left: 5px;margin-top: 5px;");
		//vmainLayout.setVflex("true");
		ZKUpdateUtil.setVflex(vmainLayout, "true");
		mainLayout.appendChild(vmainLayout);
		pcustLayout.setBorder("normal"); // panel borders
		pcustLayout.setStyle(borderStyle);
		pprodLayout.setBorder("normal");
		pprodLayout.setStyle(borderStyle);
		pgLayout.setBorder("normal");
		pgLayout.setStyle(borderStyle);
		pbarLayout.setBorder("normal");
		pbarLayout.setStyle(borderStyle2);
		ptotalLayout.setBorder("normal");
		ptotalLayout.setStyle(borderStyle);
		ptotalLayout.setLeft("1px");
		custLayout.setAlign("left");
		//custLayout.setWidth("100%");
		ZKUpdateUtil.setWidth(custLayout, "100%");
		//custLayout.setWidths("5%,25%,5%,10%,5%,15%,10%,10%,15%");
		Columns columns = getColumns("5.4125%","19.0015625%","4.8828125%","","4.8828125%","8.7890625%","5.859375%","9.765625%","6.8359375%","9.765625%","4.8828125%","13.671875%","5.37109375%");
		custLayout.appendChild(columns);
		Rows custRows = custLayout.newRows();		

		prodLayout.setAlign("left");
		//prodLayout.setWidth("100%");
		ZKUpdateUtil.setWidth(prodLayout, "100%");
		prodLayout.setWidths("5%,15%,50%,20%,10%,10%");

		//prodLayout.setWidth("100%");
		//prodLayout.setHeight("100%");
		ZKUpdateUtil.setHeight(prodLayout, "95%");
		ZKUpdateUtil.setWidth(prodLayout, "95%");
		
		//gLayout.setHeight("100%");
		//gLayout.setWidth("100%");
		//gLayout.setVflex("true");
		ZKUpdateUtil.setWidth(gLayout, "100%");
		ZKUpdateUtil.setHeight(gLayout, "100%");
		ZKUpdateUtil.setVflex(gLayout, "true");
		totalLayout.setAlign("left");
		//totalLayout.setWidth("100%");
		ZKUpdateUtil.setWidth(totalLayout, "100%");

		//tLine1.setWidth("100%");
		ZKUpdateUtil.setWidth(tLine1, "100%");
		tLine1.setWidths("60%,5%,10%,5%,20%");
		//tLine2.setWidth("100%");
		ZKUpdateUtil.setWidth(tLine2, "100%");
		tLine2.setWidths("30%,5%,25%,5%,10%,5%,20%");

		//pgLayout.setVflex("true");
		ZKUpdateUtil.setVflex(pgLayout, "true");

		vmainLayout.appendChild(pcustLayout);
		pcustLayout.appendChild(custLayout);
		vmainLayout.appendChild(pprodLayout);
		pprodLayout.appendChild(prodLayout);
		vmainLayout.appendChild(pgLayout);
		pgLayout.appendChild(gLayout);
		vmainLayout.appendChild(ptotalLayout);
		ptotalLayout.appendChild(totalLayout);
		totalLayout.appendChild(tLine1); // first line of totals
		totalLayout.appendChild(tLine2); // second line of totals

		// estilos y tama�os de componentes
		// establecer etiquetas
		bPartnerLabel.setText("" + Msg.translate(Env.getCtx(), "customer"));
		bPartnerLabel.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTPartner"));
		lblCredit.setText(Msg.translate(Env.getCtx(), "Credits"));
		bpartnerTextBox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTPartner"));
		//bpartnerSearch.getComponent().setWidth("100px");
		ZKUpdateUtil.setWidth(bpartnerSearch.getComponent(), "240px");

		bPartnerPlanLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelPlan"));

		productLabel.setText("" + Msg.translate(Env.getCtx(), "M_Product_ID"));
		productTextBox.addEventListener(Events.ON_BLUR, this);

		productLabel.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTProduct"));
		productTextBox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTProduct"));
		totalLabel.setText("" + Msg.translate(Env.getCtx(), "Total"));
		subTotalLabel.setText("" + Msg.translate(Env.getCtx(), "SubTotal"));
		taxLabel.setText("" + Msg.translate(Env.getCtx(), "C_Tax_ID"));
		dctLabel.setText("" + Msg.translate(Env.getCtx(), "discount.amt"));
		tmpSalesLabel.setText("" + Msg.translate(Env.getCtx(), "smj_tmpWebSales_ID"));
		entryDateLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelSalesDate"));
		lblDateScheduled.setText(Msg.translate(Env.getCtx(), "SMJ_LabelScheduledDate"));
		deleteButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelDeleteProduct"));
		deleteButton.addActionListener(this);
		deleteButton.setStyle(buttonRed);
		sendButton.setStyle(buttonTomato);
		sendButton.setLabel("Imprimir");
		sendButton.addActionListener(this);
		clearButton.setStyle(buttonImg);
		clearButton.setImage(ThemeManager.getThemeResource("images/Cancel16.png"));
		clearButton.addActionListener(this);
		newCButton.setStyle(buttonImg);
		newCButton.setImage(ThemeManager.getThemeResource("images/New16.png"));
		newCButton.addActionListener(this);

		btnDiscounts.setLabel(Msg.translate(Env.getCtx(), "SMJLabelDiscounts"));
		btnDiscounts.setStyle(buttonOrange);

		btnExit.setLabel(Msg.translate(Env.getCtx(), "SMJLabelExit"));
		btnExit.setStyle(buttonGreenYellow);

		clearCButton.setStyle(buttonImg);
		clearCButton.setImage(ThemeManager.getThemeResource("images/Cancel16.png"));
		clearCButton.addActionListener(this);

		cancelButton.setStyle(buttonRed);
		cancelButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelDeleteTicket"));
		cancelButton.addActionListener(this);

		preticketButton.setStyle(buttonGray);
		preticketButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelPreTicket"));
		preticketButton.addActionListener(this);
		payButton.setStyle(buttonGreen);
		payButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelPay"));
		payButton.addActionListener(this);
		plusButton.setStyle(buttonImg);
		//plusButton.setWidth("30px");
		ZKUpdateUtil.setWidth(plusButton, "30px");
		plusButton.setImage(ThemeManager.getThemeResource("images/add.JPG"));
		plusButton.addActionListener(this);
		minusButton.setStyle(buttonImg);
		//minusButton.setWidth("30px");
		ZKUpdateUtil.setWidth(minusButton, "30px");
		minusButton.setImage(ThemeManager.getThemeResource("images/delete3.JPG"));
		minusButton.addActionListener(this);
		bpartnerTextBox.addEventListener(Events.ON_BLUR, this);

		deleteAllButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelResetTickets"));
		deleteAllButton.addActionListener(this);
		deleteAllButton.setStyle(buttonYellow);
		//
		//bPartnerLabel.setWidth("98%");
		ZKUpdateUtil.setWidth(bPartnerLabel, "98%");
		bPartnerLabel.setStyle(labelStyle);
		Row row = custRows.newRow();
		row.appendCellChild(bPartnerLabel.rightAlign());

		lblCredit.setStyle(labelStyle);
		txtCredit.setReadonly(true);
		//txtCredit.setWidth("92%");
		ZKUpdateUtil.setWidth(txtCredit, "92%");

		//bPartnerPlanLabel.setWidth("98%");
		ZKUpdateUtil.setWidth(bPartnerPlanLabel, "98%");
		bPartnerPlanLabel.setStyle(labelStyle);

		//bpartnerTextBox.setWidth("92%");
		ZKUpdateUtil.setWidth(bpartnerTextBox, "92%");
		bpartnerTextBox.setReadonly(true);

		row.appendChild(bpartnerSearch.getComponent());
		row.appendChild(clearCButton);
		row.appendChild(new Space());		
		row.appendChild(bPartnerPlanLabel.rightAlign());
		row.appendChild(bpartnerTextBox);

		//entryDateLabel.setWidth("98%");
		ZKUpdateUtil.setWidth(entryDateLabel, "98%");
		entryDateLabel.setStyle(labelStyle);
		//lblDateScheduled.setWidth("98%");
		ZKUpdateUtil.setWidth(lblDateScheduled, "98%");
		lblDateScheduled.setStyle(labelStyle);
		//dateScheduled.getComponent().setWidth("98%");
		ZKUpdateUtil.setWidth(dateScheduled.getComponent(), "98%");
		//entryDate.getComponent().setWidth("98%");
		ZKUpdateUtil.setWidth(entryDate.getComponent(), "98%");

		row.appendChild(entryDateLabel.rightAlign());
		row.appendChild(entryDate.getComponent());
		row.appendChild(lblDateScheduled.rightAlign());
		row.appendChild(dateScheduled.getComponent());
		entryDate.setReadWrite(false);
		dateScheduled.setReadWrite(isAdminRoleWarehouse());
		//tmpSalesLabel.setWidth("98%");
		ZKUpdateUtil.setWidth(tmpSalesLabel, "98%");
		tmpSalesLabel.setStyle(labelStyle);
		row.appendChild(tmpSalesLabel.rightAlign());
		//tmpSalesListbox.setWidth("98%");
		ZKUpdateUtil.setWidth(tmpSalesListbox, "98%");
		row.appendChild(tmpSalesListbox);
		//ticketLabel.setWidth("98%");
		ZKUpdateUtil.setWidth(ticketLabel, "98%");
		row.appendChild(ticketLabel);

		row = custRows.newRow();
		loyaltyLayout(row);
		row.appendChild(lblCredit.rightAlign());
		row.appendChild(txtCredit);

		row = custRows.newRow();
		transLoyaout(row);

		//productLabel.setWidth("100px");
		ZKUpdateUtil.setWidth(productLabel, "100px");
		productLabel.setStyle(labelStyle);
		prodLayout.appendChild(productLabel.rightAlign());
		//productSearch.getComponent().setWidth("250px");
		ZKUpdateUtil.setWidth(productSearch.getComponent(), "250px");
		//productTextBox.setWidth("100%");
		//productTextBox.setHflex("1");
		ZKUpdateUtil.setWidth(productTextBox, "100%");
		ZKUpdateUtil.setHflex(productTextBox, "1");

		if (useTransport) {
			prodLayout.appendChild(productTextBox); // product	
		} else {
			prodLayout.appendChild(productSearch.getComponent()); // product
		}

		prodLayout.appendChild(clearButton);
		prodLayout.appendChild(btnDiscounts);
		prodLayout.appendChild(btnExit);
		prodLayout.appendChild(sendButton);

		// Validar si estan permitidos los descuentos
		if (discountsRestricted || !isAdminRole()) {
			btnDiscounts.setVisible(false);
			prodLayout.setWidths("5%,15%,60%,10%,10%");
		}

		// aqui el layout de la tabla central: Grid

		dataTable.setStyle("div.z-grid-header { background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #CFCFCF; overflow: auto; }");
		dataTable.setClass("div.z-grid-header { background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #CFCFCF; overflow: auto; }");
		//dataTable.setHeight("100%");
		ZKUpdateUtil.setWidth(dataTable, "100%");
		gLayout.appendChild(dataTable);

		// aqui el layout de los totales: Grid

		tLine1.setStyle(rowStyle);
		tLine1.setAlign("right");

		barLayout.appendChild(deleteButton);
		barLayout.appendChild(plusButton);
		barLayout.appendChild(minusButton);
		//barLayout.setWidth("150px");
		ZKUpdateUtil.setWidth(barLayout, "150px");
		tLine1.appendChild(barLayout);

		dctLabel.rightAlign();
		dctLabel.setStyle(labelStyle);
		tLine1.appendChild(dctLabel);
		dctDecimalbox.rightAlign();
		dctDecimalbox.setText(nf.format(0));
		ZkCssHelper.appendStyle(dctDecimalbox, labelStyle);
		tLine1.appendChild(dctDecimalbox);

		//t1Layout.setWidth("150px");
		ZKUpdateUtil.setWidth(t1Layout, "150px");
		t1Layout.setAlign("right");

		subTotalLabel.rightAlign();
		subTotalLabel.setStyle(labelStyle);
		t1Layout.appendChild(subTotalLabel);
		//subTotalDecimalbox.setWidth("150px");
		ZKUpdateUtil.setWidth(subTotalDecimalbox, "150px");
		subTotalDecimalbox.rightAlign();
		subTotalDecimalbox.setText(nf.format(0));
		subTotalDecimalbox.setStyle(labelStyle);
		t1Layout.appendChild(subTotalDecimalbox);
		tLine1.appendChild(t1Layout);

		// taxes & total row

		tLine2.setStyle(rowStyle);
		tLine2.appendChild(deleteAllButton);
		tLine2.appendChild(payButton);
		tLine2.appendChild(cancelButton);
		taxLabel.rightAlign();
		taxLabel.setStyle(labelStyle);
		tLine2.appendChild(taxLabel);
		taxDecimalbox.rightAlign();
		taxDecimalbox.setText(nf.format(0));
		ZkCssHelper.appendStyle(taxDecimalbox, labelStyle);
		tLine2.appendChild(taxDecimalbox);

		//t2Layout.setWidth("150px");
		ZKUpdateUtil.setWidth(t2Layout, "150px");
		t2Layout.setAlign("right");
		totalLabel.rightAlign();
		totalLabel.setStyle(totalStyle);
		t2Layout.appendChild(totalLabel);
		//totalDecimalbox.setWidth("150px");
		ZKUpdateUtil.setWidth(totalDecimalbox, "150px");
		totalDecimalbox.rightAlign();
		totalDecimalbox.setText(nf.format(0));
		totalDecimalbox.setStyle(totalStyle);
		t2Layout.appendChild(totalDecimalbox);
		tLine2.appendChild(t2Layout);

		dataTable.setFocus(true);
		productSearch.getComponent().setFocus(true);
		dataTable.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				dataTableOnDoubleClick();
			}

		});

		bPartnerLabel.setClass("menu-href z-a");
		bPartnerLabel.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				String sql = "SELECT w.AD_Window_ID FROM AD_Window w "
						+ "INNER JOIN AD_Tab t ON (t.AD_Window_ID = w.AD_Window_ID) "
						+ "WHERE t.AD_Table_ID = 291 AND t.tablevel = 0 AND w.isdefault = 'Y' AND w.isactive = 'Y' "
						+ "ORDER BY w.AD_Window_ID DESC";

				int AD_Window_ID = DB.getSQLValue(null, sql);
				ShowWindow.openWindow(AD_Window_ID, MBPartner.Table_Name, bpartnerId);
			}
		});

		btnDiscounts.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnDiscountLineOnClick();
			}
		});

		btnExit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				clean();
				initOpenTickets();		
			}
		});

		form.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				loadPriceList();
			}
		});

		dateScheduled.addValueChangeListener(new ValueChangeListener() {			
			@Override
			public void valueChange(ValueChangeEvent evt) {
				Timestamp datePromisedValue = (Timestamp) evt.getNewValue();
				if (order != null && datePromisedValue != null) {
					order.setDatePromised(datePromisedValue);
					order.saveEx();
				}
			}
		});
	}// zkInit

	/**
	 * Visualiza la ventana SMJOpenTickets, cuando se desea seleccionar
	 * un ticket previamente creado, o crear uno nuevo.
	 * View the SMJOpenTickets window, when you want to select a previously created ticket, or create a new one.
	 */
	private void initOpenTickets() {
		if (!form.getChildren().isEmpty()) {
			form.getChildren().clear();
		}

		EventListener<Event> openTicketsOnEvent = new EventListener<Event>() {			
			@Override
			public void onEvent(Event event) throws Exception {
				openTicketsOnEvent();				
			}
		};

		tickets = new SMJOpenTickets(openTicketsOnEvent);
		//form.setHeight("100%");
		//form.setWidth("100%");
		//form.setVflex("true");
		ZKUpdateUtil.setWidth(form, "100%");
		ZKUpdateUtil.setHeight(form, "100%");
		ZKUpdateUtil.setVflex(form, "true");
		form.appendChild(tickets);
	}

	private void generateOrder() throws Exception {
		if (tmp == null)
			return;

		int salesListId = DataQueries.getPriceListByVersion(null, lclientId, salesListVersionId);
		String docsh =  MSysConfig.getValue("SMJ-DOCSTANDARDORDER",lclientId,lorgId).trim();
		Integer standardOrderId = DataQueries.getDocumentTypeId(docsh, lclientId);

		try {
			order = new MOrder(Env.getCtx(), 0, null);
			order.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
			order.setIsActive(true);
			order.setDocStatus(DocAction.STATUS_Drafted); // draft - borrador
			order.setDocAction(DocAction.ACTION_Complete);
			order.setProcessing(false);
			order.setProcessed(false);
			order.setIsApproved(true);
			order.setIsCreditApproved(false);
			order.setIsDelivered(false);
			order.setIsInvoiced(false);
			order.setIsPrinted(false);
			order.setIsTransferred(false);
			order.setIsSelected(false);
			order.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
			order.setDateOrdered(new Timestamp(dateInvoice.getTime()));
			order.setDateAcct(new Timestamp(dateInvoice.getTime()));
			order.setDatePromised(new Timestamp(dateInvoice.getTime()));
			order.setC_BPartner_ID(bpartnerId);
			order.setC_BPartner_Location_ID(locationID);
			order.setIsDiscountPrinted(false);
			order.setInvoiceRule("D");
			order.setDeliveryRule("F");
			order.setFreightCostRule("I");
			order.setDeliveryViaRule("P");
			order.setIsTaxIncluded(false);
			order.setC_ConversionType_ID(114);
			order.setC_Currency_ID(DataQueries.getCurrencyDefault(Env.getAD_Client_ID(Env.getCtx())));
			order.setCopyFrom("N");
			order.setPosted(false);
			order.setPriorityRule("5");
			order.setFreightAmt (Env.ZERO);
			order.setChargeAmt (Env.ZERO);
			order.setTotalLines (Env.ZERO);
			order.setGrandTotal (Env.ZERO);
			order.setAmountRefunded(Env.ZERO);
			order.setAmountTendered(Env.ZERO);
			order.setIsDropShip(false);
			order.setSendEMail (false);
			order.setIsSelfService(false);
			order.setIsDiscountPrinted(false);
			order.setC_PaymentTerm_ID(paymentTerm);			
			order.setM_Warehouse_ID(m_warehouse_id);
			order.set_CustomColumn("smj_tmpwebsales_id", tmp.getsmj_tmpwebsales_ID()); // store a reference to the original ticket id

			//para orden de venta - For m_order Sales
			order.setIsSOTrx(true); 
			order.setC_DocTypeTarget_ID(standardOrderId);
			order.setC_DocType_ID(standardOrderId);		
			order.setPaymentRule(MOrder.PAYMENTRULE_Cash);			
			order.setM_PriceList_ID(salesListId);

			order.saveEx();

			tmp.setC_Order_ID(order.getC_Order_ID());
			tmp.saveEx();

			dateScheduled.setValue(order.getDatePromised());
		} catch (Exception e) {
			order = null;
			tmp.deleteEx(true);
			initOpenTickets();
			throw e;			
		} 
	}

	/**
	 * Validate if a ticket exists ( return -1) if ticket does not exist: if an
	 * invoice does not exist for that ticket (deleted) return 0 if an invoice
	 * exists for that ticket return the id of the invoice
	 * 
	 * @return int
	 */
	public int validateCloseTicket() {
		int returnId = 0;
		int invoiceId = 0;
		// consulta si el ticket ya no existe
		if (tmp != null && !validateTicketById(lclientId, lorgId, tmp.getsmj_tmpwebsales_ID())) {
			// si existe toma el id, y revisa si hay una order, y obtine l
			// nuemro de factura
			invoiceId = getInvoiceByTicketId(lclientId, lorgId,
					tmp.getsmj_tmpwebsales_ID());
			if (invoiceId > 0) { // there is an invoice
				returnId = invoiceId;
			} else {
				returnId = 0; // there is not an invoice (closed/deleted ticket)
			}
		} else {
			returnId = -1; // ticket exists
		}
		return returnId;
	}

	/**
	 * Evento cuando selecciona un ticket desde SMJOpenTickets
	 * Event when you select a ticket from SMJOpenTickets
	 */
	private void openTicketsOnEvent() {
		try {
			tmp = null;
			order = null;
			//Adicion iMarch----
			Env.getCtx().setProperty("#SMJ_Plate", "");
			Env.getCtx().setProperty("#C_BPartnerPOS_ID", "");
			//Fin Adicion iMarch----
			form.getChildren().clear();			
			form.appendChild(mainLayout);
			loadTmpSales(tickets.getTicketSelected());
			runTimer();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Evento cuando se hace Doble Click sobre la Tabla de Productos...
	 * Double Click event when done on the Table of Products
	 */
	private void dataTableOnDoubleClick() {
		Integer index = dataTable.getSelectedIndex();

		if (index < 0) {
			return;
		}

		MSMJTmpWebSalesLine webSalesLine = list.get(index);

		MProduct p = new MProduct(Env.getCtx(), webSalesLine.getM_Product_ID(), null);
		if (p.getM_Product_Category_ID() == catCombosPOS)  {
			if (p.isBOM() ) {
				showBOMProduct(p,webSalesLine.getQtyOrdered());
				return;
			}
		}


		if (webSalesLine.getsmj_isworkorder().equals("Y")) {
			Message.showWarning("SMJMSGItemSent");
			return;
		}

		ArrayList<HashMap<String, Object>> uomList = DataQueries.getUOMConversionByProduct(webSalesLine.getM_Product_ID());

		if (uomList.size() <= 1) {
			Message.showWarning("SMJ_NoUomConversion");
			return;
		}

		changeUnit = new SMJChangeUnit(uomList, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				HashMap<String, Object> hashMap = changeUnit.getCurrentUOM();
				Integer id = (Integer) hashMap.get("id");
				String name = (String) hashMap.get("name");
				BigDecimal divideRate = (BigDecimal) hashMap.get("dividerate");				

				Integer index = dataTable.getSelectedIndex();
				MSMJTmpWebSalesLine line = list.get(index);

				if (line.getC_UOM_ID() != id) {
					Integer manual = (divideRate.subtract(line.getQtyOrdered())).intValue();
					Boolean hasMany = validateQtyProduct(line.getM_Product_ID(), manual);

					if (!hasMany) {
						Message.showWarning("SMJMSGQtyEntered");
						return;
					}

					line.setC_UOM_ID(id);
					line.setUOMName(name);
					line.setQtyEntered(Env.ONE);
					line.setsmj_discount(Env.ZERO);
					line.setQtyOrdered(divideRate);
					MProductPrice productPrice = MProductPrice.get(Env.getCtx(), salesListVersionId, line.getM_Product_ID(), null);

					line.setPriceEntered(productPrice.getPriceList().multiply(divideRate));			

					line.saveEx();					
					list.set(index, line);
					loadTable();
				}
			}
		});
		changeUnit.setParent(form);
		changeUnit.doModal();
	}

	/**
	 * Discount for the current line
	 */
	private void btnDiscountLineOnClick() {
		Integer rowSelected = dataTable.getSelectedRow();

		if (rowSelected < 0) {
			Message.showWarning("SMJMSGSelectItemList");
			return;
		}

		EventListener<Event> event = new EventListener<Event>() {			
			@Override
			public void onEvent(Event event) throws Exception {
				reloadTable();
			}
		};

		SMJDiscountWindow window = new SMJDiscountWindow(list.get(rowSelected), event);
		window.setParent(form);
		window.doModal();	
		loadTable();
	}

	/**
	 * Load full information from the table
	 */
	private void reloadTable() {
		String whereClause = MSMJTmpWebSalesLine.COLUMNNAME_smj_tmpwebsales_ID + " = " + tmp.getsmj_tmpwebsales_ID();
		list = new Query(Env.getCtx(), MSMJTmpWebSalesLine.Table_Name, whereClause, null).setOrderBy(MSMJTmpWebSalesLine.COLUMNNAME_smj_tmpwebsalesline_ID).list();
		loadTable();
	}

	/**
	 * se ejecuta cuando sucede un evento 
	 * it's executes an event occurs
	 */
	@Override
	public void onEvent(Event ev) throws Exception {		
		final int invoiceId = validateCloseTicket();
		if (invoiceId > 0) { // ticket closed by other user - there is an
			// invoice
			Messagebox.showDialog(Msg.translate(Env.getCtx(),
					"SMJMSGTicketClosedWithInvoice"), labelInfo, Messagebox.OK
					| Messagebox.CANCEL, Messagebox.QUESTION,
					new Callback<Integer>() {
				@Override
				public void onCallback(Integer result) {
					if (result.equals(Messagebox.OK)) {
						ShowWindow.openInvoiceCustomer(invoiceId);
					}
				}
			});
			// cerrar pos pendiente
			SessionManager.getAppDesktop().closeActiveWindow();
			return;
		} else if (invoiceId == 0) { // ticket closed by other user - no invoice
			Message.showWarning("SMJMSGTicketClosedNoInvoice");
			// cerrar pos pendiente
			SessionManager.getAppDesktop().closeActiveWindow();
			return;
		}

		if (ev.getTarget().equals(deleteButton)) {
			String message = "@SMJMSGDeleteRecord@";
			Integer index = dataTable.getSelectedIndex();

			if (useTransport && list.get(index).getR_Request_ID() > 0) {				
				MRequest request = new MRequest(Env.getCtx(), list.get(index).getR_Request_ID(), null);
				message = "@SMJMSGSureDeleteLinesWO@ " + request.getDocumentNo();
			}

			Messagebox.showDialog(
					Msg.parseTranslation(Env.getCtx(), message),
					labelInfo, Messagebox.OK | Messagebox.CANCEL,
					Messagebox.QUESTION, new Callback<Integer>() {
						@Override
						public void onCallback(Integer result) {
							if (result.equals(Messagebox.OK)) {
								try {
									cancelTicketComandas(false);
								} catch (Exception ex) {
									Message.showError(ex.getMessage(), false);
								}
							}
						}
					});
			productSearch.getComponent().setFocus(true);
			return;
		} // delete button

		else if (ev.getTarget().equals(plusButton)) {
			addQuantity(1);
		} else if (ev.getTarget().equals(minusButton)) {
			addQuantity(-1);
		} else if (ev.getTarget().equals(sendButton)) {
			//String msg = sendCommand();
			//String msg = "Enviado";
			//if (!msg.equalsIgnoreCase("")) {
				//Message.showWarning(msg, false);
			//}
			onPrint();
		} else if (ev.getTarget().equals(clearButton)) {
			cleanProductSearch();
		} else if (ev.getTarget().equals(clearCButton)) {
			cleanCustomerSearch();
			return;
		} else if (ev.getTarget().equals(newCButton)) {
			WSearchEditor.createBPartner(1000002);
		} else if (ev.getTarget().equals(preticketButton)) {
			Message.showWarning(" preTicket", false);
		} else if (ev.getTarget().equals(payButton)) {

			if (bpartnerId <= 0) {
				Message.showWarning("SMJMSGSelectBpartner");
				return;
			}

			Timestamp datePromisedValue = (Timestamp) order.getDatePromised();
			Timestamp salesOrder = (Timestamp) entryDate.getValue();

			if (salesOrder.compareTo(datePromisedValue) < 0) {
				Message.showWarning(Msg.parseTranslation(Env.getCtx(),"@SMJ_LabelScheduledDate@, @SMJ_MsgMustBeLessOrEqual@ @SMJLabelSalesDate@"), false);
				return;
			}

			Boolean salesRep = validateSalesRep();
			if (!salesRep) {
				return;
			}

			if (bpartnerId <= 0) {
				Message.showWarning("SMJMSGSelectBpartner");
				return;
			}
			if (list.size() <= 0) {
				Message.showWarning("SMJMSGAddItemList");
				return;
			}

			if (SMJConfig.getBooleanValue("IsSendMandatory", false, lorgId)) {
				boolean isOk = true;
				for (MSMJTmpWebSalesLine line: list) {
					if (line.getM_Product_ID() > 0 && line.getsmj_isworkorder().equalsIgnoreCase("F")) {
						isOk = false;
						break;
					}
				}

				if (!isOk) {
					Message.showWarning("SMJ-MSGSendIsMandatory");
					return;
				}
			}

			MPeriod period = MPeriod.get(Env.getCtx(), order.getDateAcct(), lorgId, null);
			period.isOpen(order.getC_DocType().getDocBaseType(), order.getDateOrdered());

			if (!period.isOpen(order.getC_DocType().getDocBaseType(), order.getDateOrdered())) {
				Message.showWarning("SMJMSGClosedPeriod");
				return;
			}

			Boolean fx = validateQty();
			if (fx) {
				// here the converage for the insurance
				loadTable();
				loadPriceList();
				if (insuranceId > 0) {
					SMJInsuranceCoverage insuW = new SMJInsuranceCoverage(
							bpartnerId, new Callback<Integer>() {
								@Override
								public void onCallback(Integer result) {
									if (result.equals(1)) { // OK
										addCoverageLine();
										showPayment(dateInvoice);
									} else { // it does not want to include
										// coverage
										grandTotal = subTotal.add(taxTotal);
										showPayment(dateInvoice);
									}
								}
							});
					insuW.setPage(form.getPage());
					insuW.setInsuranceCbox(m_insuranceC);
					insuW.setInsurancePlanbox(m_insuranceP);
					insuW.setCopaymDecimalbox(m_coverageM);
					insuW.setCopaypDecimalbox(m_coverageP);
					insuW.setCoveredDecimalbox(m_covered);
					insuW.setTotalbDecimalbox(m_totalB);
					insuW.setTotalaDecimalbox(m_totalA);
					AEnv.showCenterScreen(insuW);
				} else // it does not have insurance
				{
					grandTotal = subTotal.add(taxTotal);
					showPayment(dateInvoice);
				}
			}

			productSearch.getComponent().setFocus(true);
			return;
		}// OK
		else if (ev.getTarget().equals(cancelButton)) { // cancelacion del
			// ticket			
			Messagebox.showDialog(Msg.translate(Env.getCtx(), "DeleteRecord?"), labelInfo,
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
					new Callback<Integer>() {
				@Override
				public void onCallback(Integer result) {
					if (result.equals(Messagebox.OK)) {
						try {
							cancelTicketComandas(true);
						} catch (Exception ex) {
							Message.showError(ex.getMessage(), false);
						}
					}
				}
			});

		}// CANCEL
		else if (ev.getTarget().equals(tmpSalesListbox)) {
			currentSelectedTicket = tmpSalesListbox.getSelectedIndex();

			if (currentSelectedTicket < 0) {
				Message.showWarning("SMJMSGSelectTmpSale");
				return;
			} else if (currentSelectedTicket > 0) {
				loadTmpSales((Integer) tmpSalesListbox.getSelectedItem()
						.getValue());
				productSearch.getComponent().setFocus(true);
			}
		}// tmpSalesListbox

		else if (ev.getTarget().equals(deleteAllButton)) {
			Messagebox.showDialog(
					Msg.translate(Env.getCtx(), "SMJMSGWantDeleteAll"),
					labelInfo, Messagebox.OK | Messagebox.CANCEL,
					Messagebox.QUESTION, new Callback<Integer>() {
						@Override
						public void onCallback(Integer result) {
							if (result.equals(Messagebox.OK)) {
								if (deleteAll()) {
									initOpenTickets();
								}
							}
						}
					});

			return;
		} 

		else if (ev.getTarget().equals(productTextBox)) {
			String value = productTextBox.getValue();
			
			if (value.length() <= 0 || value.trim().equals("")) {
				if (dataTable.getItems() != null && !dataTable.getItems().isEmpty()) {
					Listitem listitem = dataTable.getItems().get(dataTable.getItems().size() - 1);
					int indexQty = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelQty"));
					
					ListCell listCell = (ListCell) listitem.getChildren().get(indexQty);
					if(listCell.getFirstChild()!=null) {
						((NumberBox) listCell.getFirstChild()).setFocus(true);
						((NumberBox) listCell.getFirstChild()).getDecimalbox().select();
					}
				}
				
				return;
			}
			
			if (bpartnerId <= 0){
				HashMap<String, Object> data = DataQueries.getPartnerDataByTaxId(value.trim());
				if (data != null){
					bpartnerId = (Integer) data.get("ID");
					bpartnerName = (String) data.get("NAME");
					bpartnerSearch.setValue(bpartnerId);
					setPartner(false);
				} else {
					int response = Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantCreatePartner"), labelInfo, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);
					if (response == Messagebox.OK){
						ShowWindow.openPartnerWebPOS(0);
					}//response Ok
				}
			}else{// busca producto
				
				if (useTransport && orgValidateProductPVW && tmp.getSMJ_Vehicle_ID()>0) {				
					
					;
				}
				showInfoProductTrans(value.trim());
			}//if / else
			productTextBox.setValue("");
			productTextBox.setFocus(true);
			return;
		}

		productSearch.getComponent().setFocus(true);
	}// onEvent

	protected void addCoverageLine() {
		// adiciona linea de desceunto por cubrimiento al ticket (base de datos)
		// y lo recarga en memoria
		int SMJ_TempWebSalesLine_ID = 0;
		for (MSMJTmpWebSalesLine line : list) {
			if (line.getM_Product_ID() <= 0) {
				SMJ_TempWebSalesLine_ID = line.get_ID();
				break;
			}
		}

		MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), SMJ_TempWebSalesLine_ID, null);
		ipl.setM_Product_ID(0);
		ipl.setPriceEntered(m_covered);
		ipl.setC_BPartner_ID(bpartnerId);
		ipl.setQtyEntered(new BigDecimal(1)); //Prueba Qty
		ipl.setpurchaseprice(m_covered);
		ipl.setProductValue("");
		ipl.setProductName(insuranceDiscountLabel); // identify the discounts
		ipl.settax(Env.ZERO);
		ipl.setUOMName("Each");
		ipl.setC_UOM_ID(100);
		ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));
		ipl.setQtyEntered(Env.ONE);
		Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
		ipl.setM_AttributeSetInstance_ID(attInstance);		
		ipl.setsmj_isworkorder("F"); // not send by default
		ipl.setLine(sequence + 10);
		ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());
		ipl.saveEx();

		if (SMJ_TempWebSalesLine_ID == 0)
			list.add(ipl);

		loadTable();
		// actualiza totales
	}

	/**
	 * Check if all the requested lines of tickt for a product id are less than
	 * the current stock
	 * 
	 * @param prodId
	 * @param locId
	 * @return
	 */
	private boolean validateQtyProduct(int prodId, int manual) {
		boolean valid = true;
		BigDecimal totq = Env.ZERO;
		// Look for all the quatities of the same prodId in the current ticket
		Iterator<MSMJTmpWebSalesLine> it = list.iterator();
		MProduct product = new MProduct(Env.getCtx(), prodId, null);

		//When the product is a service, not validate qty on hand
		if (product.getProductType().equals(MProduct.PRODUCTTYPE_Service)) 
			return valid;			

		while (it.hasNext()) { // check all he items of the list
			MSMJTmpWebSalesLine ipl = it.next();

			if (prodId == ipl.getM_Product_ID()) { // mira el total para el producto todas las lineas

				BigDecimal qtyEntered;

				if (product.getC_UOM_ID() == ipl.getC_UOM_ID()) {
					qtyEntered = ipl.getQtyEntered();
				} else {
					BigDecimal divideRate = DataQueries.getUOMDivideRate(prodId, ipl.getC_UOM_ID());
					qtyEntered = ipl.getQtyEntered().multiply(divideRate);
				}

				totq = totq.add(qtyEntered);
			}
		}

		BigDecimal currentStock = DataQueries.getTotalProductByWarehouse(prodId, m_warehouse_id, null);
		BigDecimal currentUsedStock = DataQueries.getTotalTempProductByWarehouse(null, prodId, m_warehouse_id).subtract(totq);
		currentStock = currentStock.subtract(currentUsedStock);

		totq = totq.add(new BigDecimal(manual));
		// stock less than request quatities
		if (currentStock.compareTo(totq) < 0) {
			if (product.getM_Product_Category_ID() == catCombosPOS   ) {
				if (product.isBOM()) {   // if product is a kit - it will check if we have all the ingredients to build a new one
					// do we have enough stock BOM ingredientes to build a new one
					// create a new method to check if we have enough stock of BOM ingredients.
					String validBOMStock = DataQueries.validStockBOMProducts(product,null,m_warehouse_id, totq.subtract(currentStock  ));
					if (validBOMStock.equalsIgnoreCase("")) {
						valid = true;
					}
					else { 	
						// mostrar nuevo mensage // Neither we have stock of this product nor we have ingredients to build it.
						// mosrar error con el nombre del producto que no tiene existencias.
						Message.showWarningPlusInfo("SMJMSGQtyEntered",validBOMStock, true);
						valid = false;
					} //  if (validBOMStock
				} // if (product.isBOM())
			}
			else {  // it is a normal product (not BOM or KIT)
				valid = false; 
			}

		}
		return valid;

	}


	private void addQuantity(Integer value) throws Exception {
		int index = dataTable.getSelectedIndex();
		if (index < 0) {
			Message.showWarning("SMJMSGSelectItemList");
		} else {
			MSMJTmpWebSalesLine ipl = list.get(index);
			Boolean wasSent = ipl.getsmj_isworkorder().equalsIgnoreCase("Y");

			if (wasSent) {
				Message.showWarning("SMJMSGItemSent"); //Mensaje Faltante
				return;
			}

			BigDecimal rate = DataQueries.getUOMDivideRate(ipl.getM_Product_ID(), ipl.getC_UOM_ID());
			BigDecimal qty = ipl.getQtyEntered();
			BigDecimal oldQty = ipl.getQtyEntered();
			qty = qty.add(new BigDecimal(value)); // obtiene cantidad y le suma
			// el valor
			if (qty.compareTo(Env.ZERO) < 1) {
				return;
			}

			ipl.setQtyEntered(qty);
			ipl.setQtyOrdered(qty.multiply(rate));
			ipl.saveEx();
			list.remove(index);
			list.add(index, ipl);

			if (qty.intValue() >= 0) {
				MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(),
						null);
				Boolean isService = p.getProductType().equals("S") ? true
						: false;
				if (!isService) {
					// BigDecimal qtyWh =
					// DataQueries.getTotalProductByLocator(ipl.getM_Product_ID(),
					// ipl.getM_Locator_ID(), null);
					boolean qtyValid = false;
					
					if (useTransport) {
						qtyValid = validateQtyTrans(ipl.getM_Product_ID(), ipl.getM_Locator_ID(), false, false);
					} else {
						qtyValid = validateQtyProduct(ipl.getM_Product_ID(), 0);
					}
					
					// if (qty.compareTo(qtyWh)>0){
					if (!qtyValid) {
						Message.showWarning("SMJMSGQtyEntered");
						ipl.setQtyEntered(oldQty);
						ipl.setQtyOrdered(oldQty.multiply(rate));
						ipl.saveEx();
						list.remove(index);
						list.add(index, ipl);
					} else {
						ipl.setQtyEntered(qty);
						ipl.setQtyOrdered(qty.multiply(rate));
						ipl.saveEx();
						list.remove(index);
						list.add(index, ipl);

					} // if (!qtyValid){

				} else {
					ipl.setQtyEntered(qty);
					ipl.setQtyOrdered(qty.multiply(rate));
					ipl.saveEx();
					list.remove(index);
					list.add(index, ipl);
				}
				loadTable();
				dataTable.setSelectedIndex(index);
			} // if (qty.intValue() >= 0)
			else {
				ipl.setQtyEntered(Env.ZERO);
				ipl.saveEx();
				list.remove(index);
				list.add(index, ipl);
			}
		}
	}

	private String sendCommand() throws Exception {
		String mensaje = "";
		int c = 0;
		String ret = "<h3>" + Msg.translate(Env.getCtx(), "SMJLabelNotificationTitle").trim() + "</h3>";
		boolean sent = false;
		MUser user = new MUser(Env.getCtx(), currentUser, null);
		Integer index = dataTable.getSelectedIndex();
		Iterator<MSMJTmpWebSalesLine> it = list.iterator();
		while (it.hasNext()) { // check all he items of the list
			c++;
			MSMJTmpWebSalesLine ipl = it.next();
			String wasSent = ipl.getsmj_isworkorder();
			if (wasSent.equalsIgnoreCase("F")) { // just oustanding items will be sent					
				MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
				MLocator ml = new MLocator(Env.getCtx(), ipl.getM_Locator_ID(), null);
				String locatorName = ml.getValue();

				String groupName = "";
				if (p.getGroup2() != null) {
					groupName = p.getGroup2().trim();
				} else {
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSJNoGroup") + p.getName(), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
					continue;
				}

				String printerName = getPrinterByGroup(groupName);
				LocalPrint lp = new LocalPrint();
				if (printerName != null && !printerName.equalsIgnoreCase("")) // product didn't have
					// printer by group /
					// default is used
				{
					lp.selectPrinter(printerName); // Examples: HP Deskjet 5520
					// series (Network)
					// PDFCreator
					ipl.setsmj_destination(groupName);
				} else {
					lp.selectDefaultPrinter();
					ipl.setsmj_destination("Default");
				}	

				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(nf.getMaximumFractionDigits());
				df.setMinimumFractionDigits(nf.getMaximumFractionDigits());
				String sep = ";";
				String mar = "   ";
				mensaje = mar + Msg.translate(Env.getCtx(),
						"SMJLabelNotificationTitle").trim()
						+ sep;
				mensaje += mar + Msg.translate(Env.getCtx(), "SMJLabelSalesDate")
				.trim() + ":\t" + new Date() + sep;
				mensaje += mar + Msg.translate(Env.getCtx(), "smj_tmpWebSales_ID").trim() + ":\t" + tmp.get_ID() + sep;
				mensaje += "   -------------------------------------------" + sep;
				mensaje += mar + Msg.translate(Env.getCtx(), "SMJLabelQty").trim()
						+ ":\t" + df.format(ipl.getQtyEntered()) + "\t" + sep;
				mensaje += "   " + Msg.translate(Env.getCtx(), "SMJLabelPrice").trim()
						+ ":\t" + nf.format(ipl.getPriceEntered()) + "\t" + sep;
				mensaje += mar + Msg.translate(Env.getCtx(), "SMJLabelProd").trim() + ":" + sep;
				mensaje += mar + ipl.getProductName() + sep; 
				mensaje += mar + Msg.translate(Env.getCtx(), "SMJLabelLocator")
				.trim() + ":\t" + locatorName + sep;
				if (ipl.getsmj_notes() != null && !ipl.getsmj_notes().isEmpty()) {
					mensaje += mar + Msg.translate(Env.getCtx(), "SMJLabelNotes")
					.trim() + ":\t" + ipl.getsmj_notes() + sep;
				}
				mensaje += mar + Msg.translate(Env.getCtx(), "user").trim() + ":\t"
						+ user.getName() + sep;
				;
				mensaje += mar + "-------------------------------------------" + sep;

				if (sent)
					ret += "<br>";

				ret += Msg.translate(Env.getCtx(), "SMJLabelProd").trim() + ": " + ipl.getProductName() + " - ";
				ret += Msg.translate(Env.getCtx(), "SMJLabelQty").trim() + ": " + df.format(ipl.getQtyEntered());

				if (ipl.getsmj_notes() != null && !ipl.getsmj_notes().isEmpty()) {
					ret += " - " + Msg.translate(Env.getCtx(), "SMJLabelNotes").trim() + ": " + ipl.getsmj_notes();
				}

				lp.printTicket(mensaje);

				// update the field send within the ticket
				ipl.setsmj_isworkorder("Y"); // i was send
				ipl.saveEx();
				sent = true;
			} // if
		} // while
		loadTable();
		dataTable.setSelectedIndex(index);
		if (!sent) {
			if (c > 0) {
				// items already sent
				ret = Msg.translate(Env.getCtx(), "SMJMSGRequestAlreadyExist");
			} else {
				ret = "";
			}

		}
		return ret;
	}

	private String getPrinterByGroup(String group) {
		String printer = "";
		// group:printer
		for (int c = 0; c < printers.length; c++) {
			String[] info = printers[c].split(":"); // split each one to> group
			// & printer
			if (info[0].trim().equalsIgnoreCase(group)) {
				printer = info[1].trim();
			}
		}
		return printer;
	}

	private void deleteLine(String trxName) throws Exception {
		Integer index = dataTable.getSelectedIndex();
		if (index != null && index >= 0) {
			MSMJTmpWebSalesLine ipl = list.get(index);

			MOrderLine orderLine = new MOrderLine(Env.getCtx(), ipl.getC_OrderLine_ID(), trxName);

			ipl.set_TrxName(trxName);
			ipl.setQtyOrdered(Env.ZERO);
			ipl.saveEx();
			list.remove(ipl); // remove from the ArrayList list
			ipl.deleteEx(true); // remove the physical row
			orderLine.setQtyReserved(Env.ZERO);
			orderLine.deleteEx(true);
		} else {
			Message.showWarning("SMJMSGSelectItemList");
		}

		loadTable();
	}

	/**
	 * muestra la ventana de informacion de productos y trae el producto
	 * seleccionado - show info product window and returns selected product data
	 * 
	 * @param value
	 */
	private void showInfoProduct(int value) {
		try {
			MProduct p = new MProduct(Env.getCtx(), value, null);

			// it shows the BOM components
			if (p.getM_Product_Category_ID() == catCombosPOS   ) {
				if (p.isBOM()) {
					showBOMProduct(p, new BigDecimal (1));			
				}
			}
			// precios desde listas configuradas
			MProductPrice sp = MProductPrice.get(Env.getCtx(), salesListVersionId, p.get_ID(), null);

			if (sp == null) {
				Message.showWarning("SMJMSGCantFindPriceList");
				return;
			}

			MProductPrice pp = MProductPrice.get(Env.getCtx(), purchaseListVersionId, p.get_ID(), null);
			// info del tercero actual

			MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), 0, null);
			ipl.setM_Product_ID(p.get_ID());
			ipl.setPriceEntered(sp.getPriceList());
			ipl.setM_Locator_ID(p.getM_Locator_ID());
			ipl.setC_BPartner_ID(bpartnerId);
			ipl.setQtyEntered(new BigDecimal(1));
			ipl.setQtyOrdered(new BigDecimal(1));
			if (p.get_ValueAsBoolean("smjdiscount_allowed")) {
				ipl.setsmj_percentage(true);
				ipl.setsmj_discount(bPartnerDiscount);
			}

			if (pp != null) {
				ipl.setpurchaseprice(pp.getPriceList());
			}

			ipl.setProductValue(p.getValue());
			ipl.setProductName(p.getName());
			ipl.setOldCode(p.getSKU());
			ipl.setM_PriceList_Version_ID(salesListVersionId);

			try {
				BigDecimal tax = DataQueries.getTaxValue(p.getC_TaxCategory_ID());

				//No add tax when is a service.
				if (tax != null && !p.getProductType().equals(MProduct.PRODUCTTYPE_Service)) {
					ipl.settax(tax);
				}
			} catch (Exception e) {
			}

			String unidad = DataQueries.getUomName(p.getC_UOM_ID());
			ipl.setUOMName(unidad);
			ipl.setC_UOM_ID(p.getC_UOM_ID());
			ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));
			ipl.setQtyEntered(Env.ONE);
			Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
			ipl.setM_AttributeSetInstance_ID(attInstance);
			ipl.setsmj_isworkorder("F"); // not send by default

			if (tmp == null) {
				tmp = new MSMJTmpWebSales(Env.getCtx(), 0, null);
				ticketLabel.setValue(String.valueOf(tmp.get_ID()));
				tmp.setC_BPartner_ID(bpartnerId);
				tmp.setC_BPartner_Location_ID(locationID);
			}

			if (useTransport) {
				ipl.set_ValueNoCheck("SMJ_Vehicle_ID", tmp.getSMJ_Vehicle_ID());
			}

			tmp.saveEx();
			listTmpSales();
			ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());

			MWarehouse warehouseTmp = new MWarehouse(Env.getCtx(), m_warehouse_id, null);

			ipl.setM_Warehouse_ID(warehouseTmp.get_ID());
			ipl.setWarehouseName(warehouseTmp.getName());
			ipl.setLine(sequence + 10);
			sequence = ipl.getLine();
			Boolean exist = true;

			if (exist) {
				BigDecimal discount = (ipl.issmj_percentage()) ? ipl.getPriceEntered().multiply(ipl.getsmj_discount().divide(Env.ONEHUNDRED)) : ipl.getsmj_discount();			
				BigDecimal price = ipl.getPriceEntered().subtract(discount);
				
				String descnote="";
				if (ipl.getsmj_notes()!=null)	descnote=ipl.getsmj_notes().trim(); //iMarch
				
				Integer c_order_line_id = Documents.createOrderLine(order, ipl.getM_Product_ID(), ipl.getQtyEntered(), ipl.getC_UOM_ID(), price
						, descnote, 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(), dateInvoice, ipl.getsmj_plate(), null, ipl.getLine());

				if (c_order_line_id > 0) {
					ipl.setC_OrderLine_ID(c_order_line_id);	
					ipl.saveEx();									
					list.add(ipl);					
					loadTable();
					dataTable.setSelectedIndex(dataTable.getItemCount() - 1);
					productSearch.setValue("");
				}
			}// if (exist)
			// }//if (doit)
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".showDialogInfoProduct - ERROR: " + e.getMessage(), e);
		}
	}// showInfoProduct




	/**
	 * Show the a small dialog with the descripcion and qty of the BOM lines for this product
	 * Muestra un peque�o dialogo con la cantidad y descripcion de cada linea de la lista de materiales del producto
	 * @param productId
	 * @return
	 */
	private void  showBOMProduct (MProduct productId,BigDecimal qty) {
		int ret = 0;
		String texto = "";
		try {
			MProductBOM [] bomLines = MProductBOM.getBOMLines(productId);
			for (int j =0; j< bomLines.length;j++   ){
				MProduct p = new MProduct(Env.getCtx(), bomLines [j].getM_ProductBOM_ID(), null);
				texto += bomLines[j].getBOMQty().multiply(qty)+"->" + p.getName() +"<BR>";
			}
			ret = bomLines.length;
			if ( ret > 0) {    // validates that BOM size > 0
				Messagebox.showDialog(texto, Msg.translate(Env.getCtx(), "SMJMSGBOMProdIncluded").trim() , Messagebox.OK,Messagebox.INFORMATION);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".showDialogInfoProduct - ERROR: " + e.getMessage(), e);
		}
		return ;

	}// showBOMProduct


	/**
	 * valida existencias ingresadas no superen las existencias en almacen -
	 * validate qty entered no exced qty in warehouse
	 * 
	 * @return
	 */
	private Boolean validateQty() {

		if (useTransport) {
			return validateQtyTrans();
		}

		Boolean flag = true;
		try {
			Iterator<MSMJTmpWebSalesLine> itl = list.iterator();
			while (itl.hasNext()) {
				MSMJTmpWebSalesLine xp = itl.next();
				boolean qtyValid = validateQtyProduct(xp.getM_Product_ID(), 0);
				if (!qtyValid ) {
					flag = false;
					Message.showWarningPlusInfo("SMJMSGQtyEntered",xp.getProductName(), true);
					break;
				}
			}// while
		} catch (Exception e) {
			flag = false;
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".validateQty ERROR: " + e.getMessage(), e);
		}
		return flag;
	}// validateQty

	/**
	 * muestra la ventana de pagos - show payment window
	 */
	private void showPayment(Date dateInvoice) {

		if (!validatePrice()) {
			return;
		}

		int rdm = Util.getRdmInteger();
		EventListener<Event> event = new EventListener<Event>() {			
			@Override
			public void onEvent(Event event) throws Exception {		
				clean();
				initOpenTickets();
			}
		};

		boolean onlyShipment = false;

		if (useTransport) {
			onlyShipment = generateOnlyShipment.isChecked();
		}

		SMJPaymentWindow win = new SMJPaymentWindow(rdm, "M_Product","M_Product_ID", false, "", bpartnerId, bpartnerName, salesListVersionId,
				locationID, m_warehouse_id, order.getGrandTotal().subtract(m_covered), taxTotal, subTotal,tmp.getsmj_tmpwebsales_ID(), paymentTerm, selectedPlate,
				vehicleID, woTextBox.getValue(), dateInvoice, onlyShipment, event);
		
		win.setVisible(true);
		win.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelPay"));
		win.setStyle("border: 2px");
		win.setClosable(true);
		win.setParent(form);
		win.addValueChangeListener(this);
		win.setCoverage(m_covered);
		win.setPlan(m_insuranceP);
		win.setIdInsurance(insuranceId);		
		AEnv.showCenterScreen(win);

	}//

	protected void cleanCoverageLines() {
		try {
			Iterator<MSMJTmpWebSalesLine> it = list.iterator();
			while (it.hasNext()) {
				MSMJTmpWebSalesLine ipx = it.next();
				if (ipx.getProductName().equalsIgnoreCase(
						insuranceDiscountLabel)) // is the coverage linea
				{
					list.remove(ipx);
				}
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".cleanCoverageLines(): ", e);
		}
	}

	/***
	 * limpia los datos de la ventana - Clean data window
	 */
	public void clean() {
		list = new LinkedList<MSMJTmpWebSalesLine>();
		dataTable.clear();
		dataTable.setData(new ListModelTable(), getTableColumnNames());
		loadTable();

		bpartnerId = 0;		
		bpartnerSearch.setValue(defaultCustomerId);
		//bpartnerSearch.getComponent().setWidth("100px");
		ZKUpdateUtil.setWidth(bpartnerSearch.getComponent(), "240px");
		currentSelectedTicket = -1;
		productSearch.setValue("");
		bpartnerName = "";
		ticketLabel.setValue("");
		txtCredit.setValue("");
		setPartner(false);

		productSearch.getComponent().setFocus(true);
		listTmpSales();
		selectedPlate = "";
		woTextBox.setValue("");
		paymentTerm = 0;
		grandTotal = Env.ZERO;
		taxTotal = Env.ZERO;
		subTotal = Env.ZERO;
		sequence = 0;
		vehicleID = 0;

		order = null;
		tmp = null;

		if (useTransport) {
			txtPlate.setText("");
			searchWorkOrder.getComponent().setText("");
			
			if (MSysConfig.getIntValue("SMJ_POSORGANIZATION", -1, Env.getAD_Client_ID(Env.getCtx())) != Env.getAD_Org_ID(Env.getCtx())) {
				generateOnlyShipment.setChecked(false);
			}
		}
	}// clean

	/***
	 * limpia los datos de producto - Clean product data
	 */
	public void cleanProductSearch() {
		productSearch.setValue(null);
		//setPartner();
		productSearch.getComponent().setFocus(true);
	}// clean

	/***
	 * limpia los datos del cliente - Clean customer data
	 */
	public void cleanCustomerSearch() {
		bpartnerId = 0;
		bpartnerSearch.setValue("");
		bpartnerSearch.getComponent().setText("");
		bpartnerSearch.getComponent().setFocus(true);
	}// clean

	/**
	 * Llena el combo de tickets fill tickets lists
	 */
	private void listTmpSales() {
		try {
			currentSelectedTicket = tmpSalesListbox.getSelectedIndex(); // store
			// previous
			// seleced
			//ListItem it = tmpSalesListbox.getSelectedItem();															

			tmpSalesListbox.removeAllItems();
			ArrayList<ArrayList<Object>> data = getTmpWebSales(
					Env.getAD_Client_ID(Env.getCtx()),
					Env.getAD_Org_ID(Env.getCtx()));
			tmpSalesListbox.appendItem("", "-1");
			for (int i = 0; i < data.size(); i++) {
				ArrayList<Object> line = data.get(i);
				tmpSalesListbox.appendItem((String) line.get(1), line.get(0));
			}// for

			tmpSalesListbox.setMold("select");
			tmpSalesListbox.addActionListener(this);
			//tmpSalesListbox.setSelectedIndex(currentSelectedTicket);

		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".listTmpSales - ERROR: " + e.getMessage(), e);
		}
	}// listTmpSales

	/**
	 * carga la informacion de la venta temporal seleccionada - load temporary
	 * sales selected
	 * 
	 * @param tmpCode
	 */
	private void loadTmpSales(Integer tmpCode) {
		try {
			if (tmpCode == -1) {
				tmp = new MSMJTmpWebSales(Env.getCtx(), 0, null);				
				bpartnerSearch.setValue(defaultCustomerId);
				bpartnerId = defaultCustomerId;				
				setPartner(true);

				if (useTransport) {
					List<KeyNamePair> plates = DataQueries.getPlatesByBPartner(defaultCustomerId, tmp.getWorkOrderAsString());

					if (plates.size() > 1) {						
						MBPartner partner = new MBPartner(Env.getCtx(), defaultCustomerId, null);
						showInfoPlate(partner.getName());
					} else {
						Env.getCtx().setProperty("#SMJ_Plate", "");
					}
				}

				loadTable();
			} else {
				ticketLabel.setValue(tmpCode.toString());
				tmp = new MSMJTmpWebSales(Env.getCtx(), tmpCode, null);
				order = new MOrder(Env.getCtx(), tmp.getC_Order_ID(), null);
				bpartnerSearch.setValue(tmp.getC_BPartner_ID());				
				bpartnerId = tmp.getC_BPartner_ID();
				setPartner(false);
				bpartnerName = DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), bpartnerId);
				locationID = tmp.getC_BPartner_Location_ID();
				list = new LinkedList<MSMJTmpWebSalesLine>(Arrays.asList(tmp.getLines()));
				//iMarch  verifica tercero de solo OV sin  factura
				if(BPartnerOnlyOV != "" && !BPartnerOnlyOV.isEmpty()){
					String[] bpartners = BPartnerOnlyOV.trim().split(";");
					Boolean t_generateOnlyShipment = false;
					for(int x=0; x<bpartners.length;x++){
						if(tmp.getC_BPartner_ID() == Integer.parseInt(bpartners[x].trim()))
							t_generateOnlyShipment=true;		
					}
					if(t_generateOnlyShipment) {
						generateOnlyShipment.setChecked(true);
						generateOnlyShipment.setEnabled(false);
					}
					else {
						generateOnlyShipment.setChecked(false);
						generateOnlyShipment.setEnabled(true);
					}
				}
				//fin adicion iMarch------
				setPlate();
				setWorkOrder();
				loadTable();
			}					
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".loadTmpSales ERROR: ", e);
		}
	}// loadTmpSales

	/**
	 * valida que no exista el elemento en la lista, si no existe regresa true -
	 * validate no exist element in list, if no exist return true
	 * 
	 * @param ipl
	 * @return
	 */
	@SuppressWarnings("unused")
	private Boolean validateElementExist(MSMJTmpWebSalesLine ipl) {
		try {
			Iterator<MSMJTmpWebSalesLine> it = list.iterator();
			while (it.hasNext()) {
				MSMJTmpWebSalesLine ipx = it.next();
				if ((ipl.getM_Product_ID() == ipx.getM_Product_ID())
						&& (ipl.getM_Locator_ID() == ipx.getM_Locator_ID())
						&& (ipl.getC_BPartner_ID() == ipx.getC_BPartner_ID())) {
					Message.showWarning("SMJMSGItemListExist");
					return false;
				}
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".validateElementExist ERROR: ", e);
		}
		return true;
	}// validateElementExist

	/**
	 * valida la informacion de tercero. - validate bpartner information
	 */
	private void setPartner(boolean createOrder) {
		try {			
			MBPartner bp = MBPartner.get(Env.getCtx(), bpartnerId);

			if (bp == null) {
				return;
			}

			//Validates Credit Status. Must be No Credit Check, otherwise show error
			if (bp.getSOCreditStatus() == null || !bp.getSOCreditStatus().equals(MBPartner.SOCREDITSTATUS_NoCreditCheck)) {
				String msg = Msg.parseTranslation(Env.getCtx(), "@C_BPartner_ID@: @SMJ_MsgBPartnerNoCreditCheck@");
				Message.showError(msg, false);
				cleanCustomerSearch();
				return;
			}

			txtCredit.setValue("" + bp.getSO_CreditLimit().subtract(bp.getSO_CreditUsed()));
			bPartnerDiscount = bp.getFlatDiscount();
			setBPartnerDiscount();
			insuranceId = bp.get_ValueAsInt("smj_insurancecompany_id");
			int bPlan = bp.get_ValueAsInt("smj_insuranceplan_id");

			if (activeLoyalty) {
				chbBeneficiary.setChecked(bp.get_ValueAsBoolean("isbeneficiary"));
				txtPoints.setText(DataQueries.getPointsByBPartnerID(bpartnerId).toString());
			}

			if (insuranceId > 0) {
				MBPartner insu1 = new MBPartner(Env.getCtx(), insuranceId,
						null);
				m_insuranceC = insu1.getName();
				if (bPlan > 0) {
					MSmjInsurancePlan planT = new MSmjInsurancePlan(
							Env.getCtx(), bPlan, null);
					bpartnerTextBox.setValue(planT.getName());
					m_insuranceP = planT.getName();
					m_coverageM = new BigDecimal(planT.getcopay_value());
					m_coverageP = planT.getcopay_percentage();
				} else {
					bpartnerTextBox.setValue("");
					m_insuranceP = "";
				}
			} else { // (insuranceId > 0)
				m_insuranceC = "";
				m_insuranceP = "";
				m_coverageM = Env.ZERO;
				m_coverageP = Env.ZERO;
				m_covered = Env.ZERO;
				m_insuranceP = "";
				bpartnerTextBox.setValue("");
			}
			bpartnerName = bp.getName();
			if (!bp.isActive()) {
				bpartnerId = 0;
				bpartnerName = "";
				locationID = 0;
				// cargar aqui el plan del tercero
				// bpartnerTextBox.setValue("");
				try {
					Message.showWarning("SMJMSGInactivePartner");
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()
							+ ".setPartner ERROR: ", e);
				}
				return;
			}

			locationID = Env.getContextAsInt(Env.getCtx(), form.getWindowNo(), Env.TAB_INFO, "C_BPartner_Location_ID");

			//Validate first if was taken from InfoWindow, otherwise get default
			if (locationID < 1) {
				locationID = DataQueries.getLocationPartner(bpartnerId);
			}

			if (bpartnerId <= 0) {
				bpartnerName = "";
				locationID = 0;
				bpartnerTextBox.setValue("");
			}
			if (locationID <= 0) {
				Message.showWarning("SMJMSGPartnerValidLocation");
				bpartnerId = (tmp.getC_BPartner_ID() > 0) ? tmp.getC_BPartner_ID() : 0;
				bpartnerName = "";
				bpartnerSearch.setValue((tmp.getC_BPartner_ID() > 0) ? tmp.getC_BPartner_ID() : "");
				bpartnerTextBox.setValue("");

			}// if location

			if (bpartnerId > 0 && locationID > 0) {
				tmp.setC_BPartner_ID(bpartnerId);
				tmp.setC_BPartner_Location_ID(locationID);
				orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), lorgId).trim();
				if (useTransport && tmp.getSMJ_Vehicle_ID() > 0 && !orgInfotrans.equals(lorgId) 
						&& orgfreighttrans.equals("false")) {	// Modificacion iMarch
					MSMJVehicle vehicle = new MSMJVehicle(Env.getCtx(), tmp.getSMJ_Vehicle_ID(), null);
					paymentTerm = vehicle.getC_PaymentTerm_ID();
					log.info("C_PaymentTerm_ID is been taken from Vehicle");
				} 

				if (paymentTerm <= 0) {
					paymentTerm = bp.getC_PaymentTerm_ID();
					log.info("C_PaymentTerm_ID is been taken from BParnter");
				}

				if (paymentTerm <= 0) {
					paymentTerm = SMJConfig.getIntValue("C_PaymentTerm_ID", 0, lorgId);
					log.info("C_PaymentTerm_ID is been taken from Organization");
				}

				log.info("C_PaymentTerm_ID -> " + paymentTerm);
				
				tmp.setC_PaymentTerm_ID(paymentTerm);
				tmp.saveEx();

				loadPriceList();

				if (order != null) {
					order.setC_BPartner_ID(bpartnerId);
					order.setC_BPartner_Location_ID(locationID);
					order.setDateOrdered((Timestamp) entryDate.getValue());
					order.set_ValueNoCheck(MOrder.COLUMNNAME_DateAcct, (Timestamp) entryDate.getValue());
					
					dateScheduled.setValue(order.getDatePromised());

					if (paymentTerm > 0) {
						order.setC_PaymentTerm_ID(paymentTerm);
						order.setPaymentRule(MOrder.PAYMENTRULE_OnCredit);
					}

					order.saveEx(); 					
				} 

				if (order == null && createOrder) {
					generateOrder();
				}

				ticketLabel.setValue(String.valueOf(tmp.get_ID()));
				payButton.setEnabled(true);
				productSearch.getComponent().setEnabled(true);
			} else {
				productSearch.getComponent().setEnabled(false);
				payButton.setEnabled(false);
			}

			listTmpSales();
			Env.getCtx().setProperty("#C_BPartnerPOS_ID",bpartnerId.toString());	//Adicion iMarch
			loadTable();
			productSearch.getComponent().setFocus(true);
			//adicion iMarch tersero sin factura
			if(BPartnerOnlyOV != "" && !BPartnerOnlyOV.isEmpty()){
				String[] bpartners = BPartnerOnlyOV.trim().split(";");
				Boolean t_generateOnlyShipment = false;
				for(int x=0; x<bpartners.length;x++){
					if(tmp.getC_BPartner_ID() == Integer.parseInt(bpartners[x].trim()))
						t_generateOnlyShipment=true;		
				}
				if(t_generateOnlyShipment) {
					generateOnlyShipment.setChecked(true);
					generateOnlyShipment.setEnabled(false);
				}
				else {
					generateOnlyShipment.setChecked(false);
					generateOnlyShipment.setEnabled(true);
				}
			}
			//fin adicion iMarch
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".setPartner ERROR: ", e);
			Message.showError(e.getMessage());
			bpartnerId = 0;
			bpartnerSearch.getComponent().setText("");
			productSearch.getComponent().setFocus(true);
		}
	}// setPartner

	/**
	 * borra todos los registros de venta temporal - delete all sales temp
	 * records
	 */
	private boolean deleteAll() {
		Trx trx = Trx.get(Trx.createTrxName("POSDelete"), false);
		try {

			String whereClause = "SMJ_TmpWebSales.IsActive = 'Y' AND SMJ_TmpWebSales.AD_Client_ID = ? "
					+ "AND SMJ_TmpWebSales.AD_Org_ID = ? AND o.M_Warehouse_ID = ? AND SMJ_TmpWebSales.SMJ_TmpWebSales_ID NOT IN "
					+ "(SELECT DISTINCT SMJ_TmpWebSales_ID FROM SMJ_TmpWebSalesLine WHERE SMJ_IsWorkOrder = 'Y')"; 
			Query query = new Query(Env.getCtx(), MSMJTmpWebSales.Table_Name, whereClause, trx.getTrxName());
			query.addJoinClause("JOIN C_Order o ON (o.C_Order_ID = SMJ_TmpWebSales.C_Order_ID)");

			query.setParameters(lclientId, lorgId, m_warehouse_id);

			List<MSMJTmpWebSales> webSalesList = query.list();
			int rows = webSalesList.size();

			for (MSMJTmpWebSales webSales : webSalesList) {
				MOrder order2Delete = new MOrder(Env.getCtx(), webSales.getC_Order_ID(), trx.getTrxName());
				order2Delete.deleteEx(true);
				webSales.deleteEx(true);
			}

			ArrayList<Integer> listWebSalesId = DataQueries.getSendedWebSalesId(lclientId, lorgId);

			if (listWebSalesId != null && !listWebSalesId.isEmpty()) {

				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGTicketWasAlreadySent", new Object[] {listWebSalesId.toString()}),
						labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}

			order = null;
			clean();
			Messagebox.showDialog(Msg.parseTranslation(Env.getCtx(), "@SMJ-MSGPROCESSSUCCESFULL@ - @NoOfLines@: ") + rows, "SmartPOS", Messagebox.OK, Messagebox.INFORMATION);
			trx.commit(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".deleteAll ERROR: ", e);
			Message.showError(e.getMessage());
			trx.rollback();
			return false;
		} finally {
			trx.close();
		}

		return true;
	}// deleteAll

	/**
	 * borra el registro actual de la venta temporal - delete current sales temp
	 * records
	 */
	private boolean deleteCurrentTicket() {
		if (ticketLabel.getValue().isEmpty())
			return true;

		Trx trx = Trx.get(Trx.createTrxName("POSDelete"), false);		
		Integer tmpCode = Integer.parseInt(ticketLabel.getValue());
		
		try {
			MSMJTmpWebSales tmpDel = new MSMJTmpWebSales(Env.getCtx(), tmpCode, trx.getTrxName());

			for (MOrderLine orderLine: order.getLines()) {
				orderLine.setQtyReserved(Env.ZERO);
				orderLine.deleteEx(false, trx.getTrxName());
			}
			
			for (MSMJTmpWebSalesLine tmpLineDel: tmp.getLines()) {
				tmpLineDel.deleteEx(false, trx.getTrxName());
			}

			order.deleteEx(false, trx.getTrxName());
			tmpDel.deleteEx(false);
			trx.commit(true);
			clean();
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".deleteAll ERROR: ", e);
			trx.rollback();
			return false;
		} finally {
			trx.close();
		}

		return true;
	}// deleteCuretnTicket



	/**
	 * se ejecuta cuando cambia el valor de bpartnerSearch - it's excecuted when
	 * bpartnerSearch changes
	 */
	@Override
	public void valueChange(ValueChangeEvent evt) {
		final int invoiceId = validateCloseTicket();
		if (invoiceId > 0) { // ticket closed by other user - there is an
			// invoice
			SessionManager.getAppDesktop().closeActiveWindow();
			Messagebox.showDialog(Msg.translate(Env.getCtx(),
					"SMJMSGTicketClosedWithInvoice"), labelInfo, Messagebox.OK
					| Messagebox.CANCEL, Messagebox.QUESTION,
					new Callback<Integer>() {
				@Override
				public void onCallback(Integer result) {
					if (result.equals(Messagebox.OK)) {
						ShowWindow.openInvoiceCustomer(invoiceId);
					}
				}
			});

			// cerrar pos pendiente
			return;
		} else if (invoiceId == 0) { // ticket closed by other user - no invoice
			SessionManager.getAppDesktop().closeActiveWindow();
			Message.showWarning("SMJMSGTicketClosedNoInvoice");
			return;
		}

		try {
			String name = evt.getPropertyName();
			Object value = evt.getNewValue();
			// BPartner
			if (name.equals("C_BPartner_ID")) {
				if (value != null) {
					bpartnerId = ((Integer) value).intValue();
					setPartner(true);
					if (useTransport) {
						List<KeyNamePair> plates = DataQueries.getPlatesByBPartner(bpartnerId, "");
						if (plates.size() > 1) {
							showInfoPlate(tmp.getC_BPartner().getName());
						} else {
							updatePlateAndVehicle("", null);
							loadTable(true);
						}
					}
				} else {
					bpartnerId = 0;
				}
				productSearch.getComponent().setFocus(true);
			}// C_BPartner_ID
			else if (name.equals("M_Product_ID")) {				

				if (bpartnerId > 0) {
					// here validate stock before creating a new ticket line
					if (value != null) {
						MProduct ps = new MProduct(Env.getCtx(),
								(Integer) value, null);
						boolean qtyValid1 = validateQtyProduct(ps.getM_Product_ID(), 1);
						if (!qtyValid1) {							
							String whereClause = "M_Product_ID = " + ps.getM_Product_ID();
							List<X_M_Substitute> substitutesTmp = new Query(Env.getCtx(), X_M_Substitute.Table_Name, whereClause, null)
									.setOnlyActiveRecords(true)
									.list();

							//Validate if the substitutes have quantity on hand, if is true, create a new list with them
							List<X_M_Substitute> substitutes = new ArrayList<X_M_Substitute>();
							for (int i = 0; i < substitutesTmp.size(); i++) {
								if (validateQtyProduct(substitutesTmp.get(i).getSubstitute_ID(), 1)) 
									substitutes.add(substitutesTmp.get(i));
							}

							//Validate if has substitutes
							if (!substitutes.isEmpty()) {
								substitute = new SMJSubstituteProduct(substitutes, new EventListener<Event>() {

									@Override
									public void onEvent(Event event) throws Exception {
										int M_Product_ID = substitute.getM_Product_ID();
										showInfoProduct(M_Product_ID);
									}
								});
								substitute.setParent(form);
								substitute.doModal();
							} else {
								Message.showWarning("SMJMSGQtyEntered");
							}
						} else {
							showInfoProduct((Integer) value);
							//cleanProductSearch();
						}
					}
					cleanProductSearch();
				} else {
					Message.showWarning("SMJMSGSelectBpartner");
				}

				productSearch.getComponent().setFocus(true);
			}
			productTextBox.setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()
					+ ".valueChange - ERROR: " + e.getMessage(), e);
		}
	}// valueChange

	private void loadTable() {
		loadTable(false);
	}
	
	/**
	 * Carga la informacion en la tabla - load table with info generate
	 */
	private void loadTable(boolean updatePlate) {
		if (order == null || order.getC_Order_ID() < 1) 
			return;

		Iterator<MSMJTmpWebSalesLine> it = list.iterator();

		List<List<Object>> data = new ArrayList<List<Object>>();
		order.load(null);
		grandTotal = order.getGrandTotal();		
		String whereClause = MOrderTax.COLUMNNAME_TaxAmt + " > 0 AND " + MOrderTax.COLUMNNAME_C_Order_ID + " = " + order.getC_Order_ID();
		taxTotal = new Query(Env.getCtx(), MOrderTax.Table_Name, whereClause, null).sum(MOrderTax.COLUMNNAME_TaxAmt);		
		whereClause = MOrderLine.COLUMNNAME_PriceActual + " < 0 AND " + MOrderLine.COLUMNNAME_C_Order_ID + " = " + order.getC_Order_ID();
		BigDecimal promotion = new Query(Env.getCtx(), MOrderLine.Table_Name, whereClause, null).sum(MOrderLine.COLUMNNAME_PriceActual);
		BigDecimal discountTotal = promotion.abs();
		subTotal = promotion.abs().multiply(new BigDecimal(-1));
		selectedPlate = tmp.getsmj_plate();
		
		while (it.hasNext()) {
			MSMJTmpWebSalesLine ipx = it.next();
			if (!(ipx.getProductName().equalsIgnoreCase(insuranceDiscountLabel))) {
				List<Object> v = new ArrayList<Object>();

				Integer M_Product_ID = DataQueries.getProductByName(lclientId, ipx.getProductName());
				whereClause = X_AD_Attachment.COLUMNNAME_AD_Table_ID + " = 208 AND " + X_AD_Attachment.COLUMNNAME_Record_ID + " = " + M_Product_ID;
				MAttachment attachment = new Query(Env.getCtx(), X_AD_Attachment.Table_Name, whereClause, null).firstOnly();

				MAttachmentEntry entry = (attachment != null) ? attachment.getEntry(0) : null;	

				v.add(ipx.getProductValue());				
				if (useTransport) {
					v.add(ipx.getM_Product().getSKU());
					v.add(ipx.getWarehouseName());
				}
				v.add(ipx.getProductName());
				v.add(ipx.getUOMName());

				if (useTransport) {
					v.add(ipx.getPriceEntered());
				} else {
					v.add(nf.format(ipx.getPriceEntered()));
				}

				v.add(ipx.getQtyEntered());
				String discount = (ipx.issmj_percentage()) ? ipx.getsmj_discount() + "%" : nf.format(ipx.getsmj_discount().multiply(ipx.getQtyEntered()));
				v.add(discount);
				v.add(nf.format(ipx.gettotal()));
				v.add(ipx.gettax());
				v.add(ipx.getsmj_destination());
				v.add((ipx.getsmj_notes() == null) ? "" : ipx.getsmj_notes());
				v.add(ipx.getsmj_isworkorder().equals("Y"));
				if (useTransport) {					
					Object value = 0;
					
					if (ipx.getR_Request_ID() < 1 && updatePlate) {	
						boolean nullValues = (tmp.getsmj_plate() == null) ? ipx.getsmj_plate() == null :  (ipx.getsmj_plate() != null) ? tmp.getsmj_plate().equals(ipx.getsmj_plate()) : false;
						boolean differentVehicle = tmp.get_ValueAsInt("SMJ_Vehicle_ID") > 0 && ipx.get_ValueAsInt("SMJ_Vehicle_ID") != tmp.get_ValueAsInt("SMJ_Vehicle_ID");
						boolean differentPlate = tmp.get_ValueAsInt("SMJ_Vehicle_ID") <= 0 && (!nullValues);
						
						if (differentVehicle || differentPlate) {
							ipx.setsmj_plate(selectedPlate);
							ipx.set_ValueNoCheck("SMJ_Vehicle_ID", tmp.get_ValueAsInt("SMJ_Vehicle_ID"));
							ipx.saveEx();
						}
					}
					
					if (ipx.get_ValueAsInt("SMJ_Vehicle_ID") > 0) {
						value = ipx.get_ValueAsInt("SMJ_Vehicle_ID");
					} else if (ipx.getsmj_isworkorder().equals("Y") && ipx.getsmj_plate() != null && !ipx.getsmj_plate().isEmpty()) {
						value = ipx.getsmj_plate();
					} else if (ipx.getsmj_plate() != null && !ipx.getsmj_plate().isEmpty()) {			
						value = ipx.getsmj_plate();
					}

					v.add(value);
				}
				v.add((entry != null && entry.isGraphic()) ? new BufferedInputStream(entry.getInputStream()) : null);				

				data.add(v);
				BigDecimal btax = Env.ONE.add((ipx.gettax().divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP)));
				BigDecimal base = ipx.gettotal().divide(btax, 2,RoundingMode.HALF_UP);
				BigDecimal totalNet = ipx.getQtyEntered().multiply(ipx.getPriceEntered());

				if (ipx.issmj_percentage()) {
					discountTotal = discountTotal.add(totalNet.multiply(ipx.getsmj_discount().divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP)));
				} else {
					discountTotal = discountTotal.add(ipx.getsmj_discount().multiply(ipx.getQtyEntered()));
				}

				subTotal = subTotal.add(base);
				//taxTotal = taxTotal.add(tax);
				if (ipx.getLine() > sequence) {
					sequence = ipx.getLine();
				}
			} // if ! ipx.getProductName().equalsIgnoreCase("")

		}// while
		if (!m_insuranceC.equalsIgnoreCase("")
				&& !m_insuranceP.equalsIgnoreCase("")) // parner with
			// insurance/club & plan
		{
			m_totalB = subTotal.add(taxTotal); // subtotal + taxes
			if (m_coverageP.intValue() > 0) // coverage by %
			{
				m_copay = m_totalB.multiply(m_coverageP).divide(
						new BigDecimal(100), 2, RoundingMode.HALF_UP);
				m_covered = m_totalB.subtract(m_copay);
			}// coverage by value
			else {
				if (m_totalB.compareTo(m_coverageM) < 0) { // subtotal is less
					// than copayment
					// m_covered = m_totalB;
					m_covered = Env.ZERO;
					m_copay = Env.ZERO;

				} // subtotal > copayment
				else {
					m_copay = m_coverageM;
					m_covered = m_totalB.subtract(m_copay);
				}

			}
			// total calculation when the partner has insuracnce/club coverage
			grandTotal = grandTotal.subtract(m_covered);
			m_totalA = grandTotal;
			if (m_covered.intValue() > 0) {
				dctDecimalbox.setValue(nf.format(m_covered.add(discountTotal)));
			} else {
				dctDecimalbox.setText(nf.format(discountTotal));
			}
			subTotalDecimalbox.setValue(nf.format(subTotal));
			taxDecimalbox.setValue(nf.format(taxTotal));
			totalDecimalbox.setValue(nf.format(grandTotal));
		} else {
			dctDecimalbox.setText(nf.format(discountTotal));
			subTotalDecimalbox.setValue(nf.format(subTotal));
			taxDecimalbox.setValue(nf.format(taxTotal));
			totalDecimalbox.setValue(nf.format(grandTotal));
		}
		ArrayList<String> columnNames = getTableColumnNames();
		dataTable.clear();
		ListHead listHead = new ListHead();
		ListHeader header = new ListHeader("Hola");
		//header.setWidth("50%");
		ZKUpdateUtil.setWidth(header, "50%");
		listHead.appendChild(header);
		dataTable.getModel().removeTableModelListener(this);
		// Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(this);		
		dataTable.setData(model, columnNames);		
		setWidths(dataTable.getListHead());
		SMJListItemRenderer render = new SMJListItemRenderer(columnNames);
		render.addTableValueChangeListener(dataTable);

		render.setColumnValidation(indexSent);

		if (useTransport) {
			render.addColumnData(indexPlate, DataQueries.getPlatesByBPartner(bpartnerId, tmp.getWorkOrderAsString()));
			searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
		}

		dataTable.setItemRenderer(render);	
		dataTable.removeActionListener(this);
		dataTable.addActionListener(this);
		setTableColumnClass(dataTable);
		productSearch.getComponent().setFocus(true);
	}// loadTable

	/**
	 * se ejecuta cuando cambia algun valor en la tabla - It's executed when
	 * something value in table changes
	 */
	@SuppressWarnings("unchecked")
	public void tableChanged(WTableModelEvent evx) {
		
		int indexQty = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelQty"));
		int indexNotes = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelNotes"));
		int indexImage = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelImage"));
		int indexPrice = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelPrice"));
		int indexCode = ((SMJListItemRenderer) dataTable.getItemRenderer()).getIndexColumn(Msg.translate(Env.getCtx(), "SMJLabelCode"));

		final int invoiceId = validateCloseTicket();
		if (invoiceId > 0) { // ticket closed by other user - there is an
			// invoice
			Messagebox.showDialog(Msg.translate(Env.getCtx(),
					"SMJMSGTicketClosedWithInvoice"), labelInfo, Messagebox.OK
					| Messagebox.CANCEL, Messagebox.QUESTION,
					new Callback<Integer>() {
				@Override
				public void onCallback(Integer result) {
					if (result.equals(Messagebox.OK)) {
						ShowWindow.openInvoiceCustomer(invoiceId);
					}
				}
			});
			// cerrar pos pendiente
			return;
		} else if (invoiceId == 0) { // ticket closed by other user - no invoice
			Message.showWarning("SMJMSGTicketClosedNoInvoice");
			dispose();
			return;
		}

		try {
			if (evx != null && evx.getIndex0() >= 0) {
				int index = evx.getIndex0();
				ArrayList<Object> v = (ArrayList<Object>) dataTable.getModel().get(index);
				MSMJTmpWebSalesLine ipl = list.get(index);
				Integer column = evx.getColumn();				
				BigDecimal qty = (BigDecimal) v.get(indexQty);

				if (qty.compareTo(Env.ZERO) < 1) {
					v.set(4, new BigDecimal(1));
					dataTable.getModel().set(index, v);
					return;
				}

				if (column.equals(indexQty)) { // cantidad modificada

					Boolean wasSent = ipl.getsmj_isworkorder().equalsIgnoreCase("Y");

					if (wasSent) {
						loadTable();
						Message.showWarning("SMJMSGItemSent");
						return;
					}

					MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
					qty = qty.setScale(p.getUOMPrecision(), BigDecimal.ROUND_HALF_UP);				
					Boolean isService = p.getProductType().equals("S") ? true : false;
					BigDecimal rate = DataQueries.getUOMDivideRate(ipl.getM_Product_ID(), ipl.getC_UOM_ID());
					if (!isService) {						
						BigDecimal oldQty = ipl.getQtyEntered();
						ipl.setQtyEntered(qty);
						ipl.setQtyOrdered(qty.multiply(rate));
						ipl.saveEx();
						list.remove(index);
						list.add(index, ipl);

						boolean qtyValid = false;

						if (useTransport) {
							qtyValid = validateQtyTrans(ipl.getM_Product_ID(), ipl.getM_Locator_ID(), false, false);
						} else {
							qtyValid = validateQtyProduct(ipl.getM_Product_ID(), 0);
						}

						if (!qtyValid) {
							ipl.setQtyEntered(oldQty);
							ipl.setQtyOrdered(oldQty.multiply(rate));
							ipl.saveEx();
							list.remove(index);
							list.add(index, ipl);
							Message.showWarning("SMJMSGQtyEntered");
						}

					} else {
						ipl.setQtyEntered(qty);
						ipl.setQtyOrdered(qty.multiply(rate));
						ipl.saveEx();
						list.remove(index);
						list.add(index, ipl);
					}

					if (useTransport) {
						productTextBox.setFocus(true);
					}
					
				} else if (column.equals(indexNotes)) {
					Boolean wasSent = ipl.getsmj_isworkorder().equalsIgnoreCase("Y");

					if (wasSent) {
						loadTable();
						Message.showWarning("SMJMSGItemSent");
						return;
					}

					String note = v.get(indexNotes).toString();	
					ipl.setsmj_notes(note);

					ipl.saveEx();
					list.remove(index);
					list.add(index, ipl);
				} else if (column.equals(indexImage)) {
					Integer M_Product_ID = DataQueries.getProductByName(lclientId, ipl.getProductName());
					String whereClause = X_AD_Attachment.COLUMNNAME_AD_Table_ID + " = 208 AND " + X_AD_Attachment.COLUMNNAME_Record_ID + " = " + M_Product_ID;
					MAttachment attachment = new Query(Env.getCtx(), X_AD_Attachment.Table_Name, whereClause, null).firstOnly();
					MAttachmentEntry entry = attachment.getEntry(0);					
					final SMJZoomImage zoomImage = new SMJZoomImage(new BufferedInputStream(entry.getInputStream()), ipl.getProductName());
					zoomImage.setParent(form);
					zoomImage.doModal();
				} else if (useTransport && column.equals(indexPlate)) {				

					Integer SMJ_Vehicle_ID = (Integer) v.get(indexPlate);

					if (SMJ_Vehicle_ID != null && SMJ_Vehicle_ID > 0) {
						ipl.set_ValueNoCheck("SMJ_Vehicle_ID", SMJ_Vehicle_ID);						
					} else {
						ipl.set_ValueNoCheck("SMJ_Vehicle_ID", 0);						
					}

					ipl.saveEx();
					list.remove(index);
					list.add(index, ipl);
				} else if (useTransport && column.equals(indexPrice)) {
					BigDecimal price = (BigDecimal) v.get(indexPrice);
					MProduct p = MProduct.get(Env.getCtx(), ipl.getM_Product_ID());
					MWarehouse warehouse = MWarehouse.get(Env.getCtx(), ipl.getM_Warehouse_ID());
					Integer warehouseOwnerId = 0;
					Boolean isConsigment = warehouse.get_ValueAsBoolean("smj_isconsignment");
					if (isConsigment) {
						warehouseOwnerId = warehouse.get_ValueAsInt("C_BPartner_ID");
					}
					//para validar que el servicio generico temporal, utilice la lista de precios adecuada.
					String tempario = temparioName;
					if (ipl.getM_Product_ID()== codeServiceMtto){
						tempario = "Mostrador";
					}
					HashMap<String, Integer> codes = DataQueries.getPListVersion(null, p, ipl.getC_BPartner_ID(), tiresCategory, ipl.getM_Locator_ID(), warehouseOwnerId, tempario);
					MProductPrice sp = null;
					Boolean allowChanges = false;
					if (codes != null) {
						sp = MProductPrice.get(Env.getCtx(), codes.get("SALES"),ipl.getM_Product_ID(), null);
						try {
							if(sp != null){
								allowChanges = (Boolean) sp.get_Value("smj_isallowchanges");
							}else{
								Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGCantFindPriceList"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
								loadTable();
								return;
							}
						} catch (Exception e) {
							log.log(Level.SEVERE, this.getClass().getName()
									+ ".tableChanged - ERROR: " + e.getMessage(), e);
						}
					}else{
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGCantFindPriceList"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}

					if (allowChanges){
						if(price.compareTo(sp.getPriceLimit()) < 0){
							Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGBelowLimitPrice"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
							loadTable();
							return;
						}else{
							ipl.setPriceEntered(price);
							ipl.setPriceActual(price);
							ipl.setQtyEntered(qty);
							ipl.setQtyOrdered(qty);
							ipl.saveEx();
							list.remove(index);
							list.add(index, ipl);
						}
					}else{
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGNoPriceChange"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						loadTable();
						return;
					}
				}

				loadTable();
			}
			productSearch.getComponent().setFocus(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".tableChanged ERROR: ", e);
		}
	}// tableChanged

	/**
	 * valida que el usuario logueado es representante de ventas - validate
	 * loggued is sales rep
	 * 
	 * @return
	 */
	private Boolean validateSalesRep() {
		Integer salesRepId = Env.getAD_User_ID(Env.getCtx());
		Boolean salesRep = DataQueries.isSalesRep(salesRepId);
		if (!salesRep) {

			try {
				Message.showWarning("SMJMSGNoSalesRep");
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName() + " - ERROR: "
						+ e.getMessage(), e);
			}
		}
		return salesRep;
	}//

	/**
	 * valida que se ingresa con una organizacion - validate log in with
	 * organization
	 */
	private boolean validateLoginOrg() {
		boolean closew = false;
		try {
			Integer orgId = Env.getAD_Org_ID(Env.getCtx());
			if (orgId <= 0) {
				Message.showWarning("SMJMSGEnterWithOrganization");
				closew = true;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()
					+ ".validateLoginOrg - ERROR: " + e.getMessage(), e);
		}
		return closew;
	}// validateLoginOrg

	private boolean validateLoginWarehouse() {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		if (M_Warehouse_ID < 1) {
			Message.showWarning("SMJMSGEnterWithWarehouse");
			return false;
		}

		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		boolean ispos = warehouse.get_ValueAsBoolean("ispos");

		if (!ispos) {
			Message.showWarning("SMJ_MsgWarehouseIsNotPOS");
			return false;
		}

		return true;
	}

	/**
	 * Regresa el listado de nombres de las columnas de la tabla return column
	 * list titles
	 * 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getTableColumnNames() {
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelCode"));

		if (useTransport) {
			columnNames.add(Msg.translate(Env.getCtx(), MProduct.COLUMNNAME_SKU));
			columnNames.add(Msg.translate(Env.getCtx(), MWarehouse.COLUMNNAME_M_Warehouse_ID));
		}

		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProd"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelUom")); //
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelPrice"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelQty"));
		columnNames.add(Msg.translate(Env.getCtx(), "discount.amt"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTotal"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTax"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelDestination"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelNotes"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelIsSent"));
		if (useTransport) columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelImage"));



		return columnNames;
	}// getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * 
	 * @param calendarTable
	 */
	public void setTableColumnClass(IMiniTable table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true); // code

		if (useTransport) {
			table.setColumnClass(i++, String.class, true); //SKU
			table.setColumnClass(i++, String.class, true); //Warehouse
		}

		table.setColumnClass(i++, String.class, true); // prod
		table.setColumnClass(i++, String.class, true); // uom

		if (useTransport) {
			table.setColumnClass(i++, BigDecimal.class, false); // price
		} else {
			table.setColumnClass(i++, String.class, true); // price
		}

		table.setColumnClass(i++, BigDecimal.class, false); // qty
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true); // total
		table.setColumnClass(i++, BigDecimal.class, true); // tax
		table.setColumnClass(i++, String.class, true); // destiantion
		table.setColumnClass(i++, String.class, false); // notes

		indexSent = i;
		table.setColumnClass(i++, Boolean.class, true); // issent		

		if (useTransport){
			indexPlate = i;
			table.setColumnClass(i++, Listbox.class, false); // Plate			
		}

		table.setColumnClass(i++, MImage.class, false); // Image		

		// Table UI
		table.autoSize();
	}// setTableColumnClass

	/**
	 * regresa la lista de temporary Sales Web returns Tmp Web Sales list
	 * 
	 * @param clientId
	 * @return ArrayList<ArrayList<Object>>
	 */
	protected ArrayList<ArrayList<Object>> getTmpWebSales(Integer clientId, Integer orgId) {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT t.smj_tmpWebSales_ID, b.name, t.created FROM smj_tmpWebSales t ");
		sql.append("JOIN C_Order o ON (o.C_Order_ID = t.C_Order_ID) ");
		sql.append("JOIN C_BPartner b ON (b.C_BPartner_ID = t.C_BPartner_ID) ");
		sql.append("WHERE t.isActive = 'Y' AND t.AD_Client_ID = " + clientId);
		sql.append(" AND t.AD_Org_ID = " + orgId + " AND o.M_Warehouse_ID = " + M_Warehouse_ID);
		sql.append(" ORDER BY name ASC ");
		ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			ArrayList<Object> line = new ArrayList<Object>();
			while (rs.next()) {
				line = new ArrayList<Object>();
				line.add(rs.getInt("smj_tmpWebSales_ID"));
				String dato = rs.getInt("smj_tmpWebSales_ID") + "-> " + rs.getString("name");  //sdf.format(cDate);
				line.add(dato);
				data.add(line);
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;
	}// getTmpWebSales

	/**
	 * Get invoiceId by ticketId
	 * 
	 * @param clientId
	 * @param orgId
	 * @param ticketId
	 * @return
	 */
	protected Integer getInvoiceByTicketId(Integer clientId, Integer orgId,
			int ticketId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT i.c_invoice_id FROM c_order o,c_invoice i ");
		sql.append(" WHERE o.isActive = 'Y' AND o.AD_Client_ID = " + clientId);
		sql.append(" AND o.AD_Org_ID = " + orgId);
		sql.append(" AND i.poreference = 'SmartPOS'");
		sql.append(" AND o.smj_tmpwebsales_id = '" + ticketId
				+ "' AND i.C_order_ID = o.C_order_ID");
		int invoiceId = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				invoiceId = rs.getInt("c_invoice_id");
				break;
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return invoiceId;
	}// getInvoiceByTicketId

	/**
	 * validate if the ticket exist in the database
	 * 
	 * @param clientId
	 * @param orgId
	 * @param ticketId
	 * @return
	 */
	protected boolean validateTicketById(Integer clientId, Integer orgId,
			int ticketId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT i.smj_tmpwebsales_id FROM smj_tmpwebsales i ");
		sql.append(" WHERE i.isActive = 'Y' AND i.AD_Client_ID = " + clientId);
		sql.append(" AND i.AD_Org_ID = " + orgId);
		sql.append(" AND i.smj_tmpwebsales_id = " + ticketId);
		int returnedId = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				returnedId = rs.getInt("smj_tmpwebsales_id");
				break;
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		if (returnedId == ticketId) {
			return true; // ticket exist - valid
		} else {
			return false; // ticket doesnot exist - invalid
		}
	}// validateTicketById

	public void dispose() {
		tmp.delete(true);
		this.detach();
	} // dispose

	/**
	 * Metodo para determinar si hay productos enviados, de ser asi
	 * realizar el proceso adecuado para estos.
	 * 
	 * @param all variable para saber si el proceso aplica para todos los productos
	 * 			  o solo para el producto actualmente seleccionado
	 */
	public void cancelTicketComandas(Boolean all) throws Exception {
		ArrayList<MSMJTmpWebSalesLine> lines = null;
		MSMJTmpWebSalesLine line = null;

		if (all) { //Se valida, si se desean obtener todas las lineas de la tabla que ya han sido enviadas, o solo la actual
			Integer currentRole = Env.getAD_Role_ID(Env.getCtx());
			int[] adminList = SMJConfig.getArrayIntValue("AdminRoles", ";", lorgId);
			boolean isAdmin = false;

			for (int i = 0; i < adminList.length; i++) {
				isAdmin = currentRole == adminList[i];
				if (isAdmin) 
					break;				
			}

			if (!isAdmin) { //Validacion si el usuario es un administrador, de lo contrario no se permite el proceso
				Message.showWarning("SMJ-MSGIsNotAdmin");
				return;
			}

			lines = new ArrayList<MSMJTmpWebSalesLine>();

			for (int i = 0; i < list.size(); i++) {
				MSMJTmpWebSalesLine tmpLine = list.get(i);

				if (tmpLine.getsmj_isworkorder().equals("Y")) {
					lines.add(tmpLine);
				}
			}

			if (lines.isEmpty()) { //validar que tenga un item al menos en comanda 
				if (deleteCurrentTicket()) {
					initOpenTickets();
				}

				productSearch.getComponent().setFocus(true);
				return;
			}
		} else {
			Integer index = dataTable.getSelectedIndex();

			if (index == null || index < 0) {
				Message.showWarning("SMJMSGSelectItemList");
				return;
			}

			line = list.get(index);

			if (useTransport && line.getR_Request_ID() > 0) {
				deleteWorkOrderLines(null);
				return;
			} else if (line.getsmj_isworkorder().equals("F")) { //validar que el item seleccionado ya ha sido enviado a comanda
				deleteLine(null);
				return;				
			}
		}

		sentCancel = new SMJSentCancel(ticketLabel.getValue(), order.getM_Warehouse_ID());
		sentCancel.setParent(form);
		sentCancel.setBtnOkOnClick(new EventListener<Event>() {			
			@Override
			public void onEvent(Event event) throws Exception {
				SentCancelBtnOkOnClick();
			}
		});

		if (all) {
			sentCancel.setLines(lines);
		} else {
			sentCancel.setLine(line);
		}

		sentCancel.doModal();
	}

	private void loadPriceList() {
		MBPartner bPartner = new MBPartner(Env.getCtx(), bpartnerId, null);
		MPriceList priceList = null;		

		if (bPartner.getM_PriceList_ID() > 0) {
			priceList = new MPriceList(Env.getCtx(), bPartner.getM_PriceList_ID(), null);
		}

		if (priceList != null && priceList.isSOPriceList()) {			
			StringBuffer whereClause = new StringBuffer(MPriceListVersion.COLUMNNAME_M_PriceList_ID + " = " + bPartner.getM_PriceList_ID())
					.append(" AND " + MPriceListVersion.COLUMNNAME_IsActive + " = 'Y'");
			Query query = new Query(Env.getCtx(), MPriceListVersion.Table_Name, whereClause.toString(), null);
			query.setOrderBy(MPriceListVersion.COLUMNNAME_M_PriceList_Version_ID + " DESC");
			salesListVersionId = query.firstId();
		} else {
			Integer M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
			MWarehouse mWarehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
			salesListVersionId = mWarehouse.get_ValueAsInt(MPriceListVersion.COLUMNNAME_M_PriceList_Version_ID);
		}
		Env.getCtx().setProperty("#SMJ_M_PriceList_Version_ID", salesListVersionId.toString());
		int salesListId = DataQueries.getPriceListByVersion(null, lclientId, salesListVersionId);

		if (order != null) {
			order.setM_PriceList_ID(salesListId);
			order.saveEx();
		}
	}

	/**
	 * Valida si el usuario esta dentro de la lista de los roles
	 * a los cuales se les permite agregar descuentos
	 * @return
	 */
	private boolean isAdminRole() {
		boolean isEnabled = false;
		int[] rolesEnabled = SMJConfig.getArrayIntValue("AdminRoles", ";", lorgId);
		int currentRole = Env.getAD_Role_ID(Env.getCtx());

		for (int i = 0; i < rolesEnabled.length; i++) {
			isEnabled = currentRole == rolesEnabled[i];
			if (isEnabled)
				break;
		}

		return isEnabled;
	}

	/**
	 * Valida si el usuario se encuentra con el rol especificado en
	 * el almacen.
	 * @return
	 */
	private boolean isAdminRoleWarehouse() {
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), m_warehouse_id);

		int currentRole = Env.getAD_Role_ID(Env.getCtx());
		int roleWarehouse = warehouse.get_ValueAsInt("AD_Role_ID");

		return currentRole == roleWarehouse;
	}

	/**
	 * Set BPartner Discount to all smj_tmpsalesline
	 */
	private void setBPartnerDiscount() {
		boolean isOk = true;
		for (MSMJTmpWebSalesLine line : list) {
			MProduct product = new MProduct(Env.getCtx(), line.getM_Product_ID(), null);
			boolean discountAllowed = product.get_ValueAsBoolean("smjdiscount_allowed");
			if (!discountAllowed) 
				continue;

			line.setsmj_percentage(true);
			line.setsmj_discount(bPartnerDiscount);
			if (!line.save()) {
				break;
			}
		}

		if (isOk) {
			loadTable();
		}
	}

	private void SentCancelBtnOkOnClick() throws Exception {
		if (sentCancel.isCancel()) {

			if (deleteCurrentTicket()) {
				initOpenTickets();
			}

			productSearch.getComponent().setFocus(true);
		} else {
			deleteLine(null);
		}
	}

	private void loyaltyLayout(Row row) {

		if (!activeLoyalty)
			return;

		String labelStyle = "font-size: 9.7px;color: #0B610B;background:#FFFFFF;";

		Label lblPoints = new Label(Msg.translate(Env.getCtx(), "Points"));
		Label lblBeneficiary = new Label(Msg.translate(Env.getCtx(), "IsBeneficiary"));
		lblPoints.setStyle(labelStyle);
		lblBeneficiary.setStyle(labelStyle);
		txtPoints.setReadonly(true);
		//txtPoints.setWidth("92%");
		ZKUpdateUtil.setWidth(txtPoints, "92%");
		chbBeneficiary.setEnabled(false);
		chbBeneficiary.setStyle(labelStyle);

		row.appendChild(new Space());
		row.appendChild(new Space());
		row.appendChild(new Space());
		row.appendCellChild(lblBeneficiary.rightAlign(), 2);
		row.appendCellChild(chbBeneficiary);
		row.getLastCell().setStyle("text-align: left");
		row.appendChild(lblPoints.rightAlign());
		row.appendChild(txtPoints);
	}	

	//Seveter Functionality
	private void transLoyaout(Row row) throws Exception {
		if (!useTransport) {
			return;
		}

		int tPrincipalWarehouse_ID = MSysConfig.getIntValue("SMJ-PRINCIPALWAREHOUSE", -1, Env.getAD_Client_ID(Env.getCtx()));
		
		showService = MSysConfig.getValue("SMJ-SHOWSERVICESSWEB",Env.getAD_Client_ID(Env.getCtx())).trim();
		codeServiceMtto = Integer.parseInt(MSysConfig.getValue("SMJ-SERVICEMTTO",Env.getAD_Client_ID(Env.getCtx())).trim());
		tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY", Env.getAD_Client_ID(Env.getCtx())).trim());
		rmnFacCategory = Integer.parseInt(MSysConfig.getValue("SMJ-REMANUFACTUREDCAT", Env.getAD_Client_ID(Env.getCtx())).trim());
		serviceRateGroupDefault = Integer.parseInt(MSysConfig.getValue("SMJ-SRATEGROUPDEF",Env.getAD_Client_ID(Env.getCtx())).trim());
		orgInfotrans = Integer.parseInt(MSysConfig.getValue("SMJ_ORGINFOTRANS",Env.getAD_Client_ID(Env.getCtx())).trim());
		//Adicion iMarch / Organizacion Transportadora
		orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), lorgId).trim();
		//--------------
		String labelStyle = "font-size: 9.7px;color: #0B610B;background:#FFFFFF;";
		final int v_ColumnID_SMJ_Vehicle = DataQueries.getColumnID("SMJ_Vehicle_ID", "SMJ_Vehicle");
		final int v_ColumnID_R_Request_ID = DataQueries.getColumnID("R_Request_ID", "R_Request");

		Label lblPlate = new Label(Msg.translate(Env.getCtx(), "smj_plate"));
		Label lblWorkOrder = new Label(Msg.translate(Env.getCtx(), "smj_workorder"));
		lblPlate.setStyle(labelStyle);
		lblWorkOrder.setStyle(labelStyle);
		generateOnlyShipment = new Checkbox();
		generateOnlyShipment.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelGenerateOnlyShipment"));

		if (MSysConfig.getIntValue("SMJ_POSORGANIZATION", -1, Env.getAD_Client_ID(Env.getCtx())) == Env.getAD_Org_ID(Env.getCtx())) { //Seveter
			generateOnlyShipment.setChecked(m_warehouse_id != tPrincipalWarehouse_ID);
		} else {
			generateOnlyShipment.setChecked(false);
		}
		
		//Search Work Order
		String whereClause = I_AD_Reference.COLUMNNAME_Name + " LIKE 'Search Work Order (SmartPOS)'";			
		Integer AD_Reference_ID = new Query(Env.getCtx(), I_AD_Reference.Table_Name, whereClause, null).firstIdOnly();
		lookupWorkOrder = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), v_ColumnID_R_Request_ID, DisplayType.Search, Env.getLanguage(Env.getCtx()), "R_Request_ID", AD_Reference_ID, true, null);
		searchWorkOrder = new WSearchEditor("R_Request_ID", true, false, true, lookupWorkOrder);

		searchWorkOrder.getComponent().getButton().addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				if (tmp.getsmj_workorder() != null && !tmp.getsmj_workorder().isEmpty()) {
					searchWorkOrder.setValue(null);
					searchWorkOrder.onEvent(event);
					searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
					searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
				}
				else{//Adicion iMArch
					searchWorkOrder.setValue(null);
					searchWorkOrder.getComponent().setText("");
				}
			}
		});

		searchWorkOrder.addValueChangeListener(new ValueChangeListener() {			
			@Override
			public void valueChange(ValueChangeEvent evt) {
				searchWorkOrderValueChange(evt);
			}
		});

		//Search Plate
		whereClause = I_AD_Reference.COLUMNNAME_Name + " LIKE 'Search Vehicle (SmartPOS)'";			
		AD_Reference_ID = new Query(Env.getCtx(), I_AD_Reference.Table_Name, whereClause, null).firstIdOnly();
		lookupPlate = MLookupFactory.get(Env.getCtx(), form.getWindowNo(), v_ColumnID_SMJ_Vehicle, DisplayType.Search, Env.getLanguage(Env.getCtx()), "SMJ_Vehicle_ID", AD_Reference_ID, true, null);
		searchPlate = new WSearchEditor("SMJ_Vehicle_ID", true, false, true, lookupPlate);
		searchPlate.addValueChangeListener(new ValueChangeListener() {			
			@Override
			public void valueChange(ValueChangeEvent evt) {
				searchPlateValueChange(evt);
			}
		});
		
		txtPlate = new Textbox();
		//txtPlate.setWidth("100%");
		//txtPlate.setHflex("1");
		ZKUpdateUtil.setWidth(txtPlate, "100%");
		ZKUpdateUtil.setHflex(txtPlate, "1");
		txtPlate.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {

			@Override
			public void onEvent(Event evt) throws Exception {
				if (evt instanceof InputEvent) {
					InputEvent iev = (InputEvent) evt;
					
					if (iev.getValue() == null || iev.getValue().isEmpty()) {
						txtPlate.setText(iev.getPreviousValue().toString());
						return;						
					}
					
					selectedPlate = iev.getValue().toUpperCase();
					searchPlate.setValue(null);
					showInfoPlate(iev.getValue());
				}
			}
		});
		
		row.appendChild(lblWorkOrder.rightAlign());
		row.appendChild(searchWorkOrder.getComponent());
		row.appendChild(lblPlate.rightAlign());
		row.appendCellChild(txtPlate, 3);
		row.appendCellChild(generateOnlyShipment,2 );
	}

	private void searchWorkOrderValueChange(ValueChangeEvent evt) {
		if (evt.getNewValue() == null ) {
			if (tmp.getC_BPartner_ID() > 0) {
				if (!tmp.getsmj_workorder().isEmpty()) {
					//Timer para asignar las ordenes de trabajo, esto se realiza por timer
					//ya que el ERP al cancelar la ventana emergente, enviara por defecto el valor 
					//NULL.
					final Timer timerOrder = new Timer(50);
					timerOrder.setRepeats(false);
					timerOrder.addEventListener(Events.ON_TIMER, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
							searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
							mainLayout.removeChild(timerOrder);
						}
					});

					mainLayout.appendChild(timerOrder);
					timerOrder.start();
				}
			}		

			return;
		}

		final int r_request_id = (Integer) evt.getNewValue();

		if (r_request_id < 1) {
			return;
		}
		Boolean doit = true;
		if (tmp.getC_BPartner_ID() > 0) {
			
			MRequest request = new MRequest(Env.getCtx(), r_request_id, null);
			if(request.getC_BPartner_ID()!=tmp.getC_BPartner_ID()) {
				//searchWorkOrder.getComponent().focus();
				Messagebox.showDialog("La solicitud pertenece a otro cliente", labelInfo, Messagebox.OK, Messagebox.QUESTION,
						new Callback<Integer>() {
					@Override
					public void onCallback(Integer result) {
						if(tmp.getsmj_workorder().isEmpty()){
							searchWorkOrder.setValue(null);
							searchWorkOrder.getComponent().setText("");
						}
						else{
							searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
							searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
						}
						searchWorkOrder.getComponent().focus();
					}
				});
				//Messagebox.showDialog("La solicitud pertenece a otro cliente", "Error", Messagebox.OK, Messagebox.QUESTION);
				doit=false;
			}			
		}
		if(doit) {
		Messagebox.showDialog(Msg.translate(Env.getCtx(),
				"SMJMSGSureAddWorkOrder"), labelInfo, Messagebox.OK
				| Messagebox.CANCEL, Messagebox.QUESTION,
				new Callback<Integer>() {
			@Override
			public void onCallback(Integer result) {
				if (result.equals(Messagebox.OK)) {
					
					isLoadingServiceOrder = orgInfotrans.equals(lorgId);
					//Adicion iMarch-------
					orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), lorgId).trim();
					if (orgfreighttrans.equals("true")){
						isLoadingServiceOrder=true;
					}
					//---------------------
					Trx trx = Trx.get(Trx.createTrxName(), false);
					
					try {						
						MRequest request = new MRequest(Env.getCtx(), r_request_id, trx.getTrxName());		

						if (tmp.getsmj_workorder().contains(request.get_ID())) {
							Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWOAlreadyAdded"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);

							if (!tmp.getsmj_workorder().isEmpty()) {
								searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
								searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
							}

							return;
						}
						
						if (tmp.getsmj_workorder().isEmpty()) {
							
							//It should be the first (Infotrans)
							if (tmp.getC_BPartner_ID() < 1) {
								bpartnerId = request.getC_BPartner_ID();
								bpartnerSearch.setValue(bpartnerId);
								setPartner(order == null);		
							}
							
							if (request.get_ValueAsInt("smj_vehicle_id") > 0 && (selectedPlate == null || selectedPlate.isEmpty())) {
															
								//Validate if the vehicle has a owner, if is true will show the popup
								String sql = "SELECT 1 FROM SMJ_VehicleOwner WHERE SMJ_Vehicle_ID = ? AND RelationShip = '1'";
								int value = DB.getSQLValueEx(null, sql, request.get_ValueAsInt("smj_vehicle_id"));
								
								if (value == 1) {
									showInfoPlate(request.get_ValueAsString("smj_plate"));
								}							
							}
						}

						if (tmp.getsmj_plate() == null || tmp.getsmj_plate().isEmpty()) {
							tmp.setsmj_plate(request.get_ValueAsString("SMJ_Plate"));		
							order.set_ValueNoCheck("smj_plate", tmp.getsmj_plate());
						}

						if (lorgId.equals(orgInfotrans) || orgfreighttrans.equals("true")) { //Modificacion iMarch, "|| orgfreighttrans.equals("true")"
							loadSalesInfotrans(request);
						} else {
							loadSalesSeveter(request, trx.getTrxName());
						}
						
						tmp.addWorkOrder(request.get_ID());
						order.set_ValueNoCheck("smj_workorder", tmp.getWorkOrderDocumentNo());
						order.saveEx(trx.getTrxName());
						tmp.saveEx(trx.getTrxName());
						
						trx.commit(true);
						
						loadTable();
						PO.set_TrxName(list.toArray(new MSMJTmpWebSalesLine[list.size()]), null); //Bug Fixed: trx closed or never opened
					} catch (Exception ex) {
						Message.showError(ex.getMessage());
						trx.rollback();
						
						if (!tmp.getsmj_workorder().isEmpty()) {
							searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
						}
						
						searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
					} finally {
						trx.close();
						if (order != null)
							order.set_TrxName(null);
						
						if (tmp != null)
							tmp.set_TrxName(null);
					}
					
					isLoadingServiceOrder = false;
				} else {
					//Adicion March if-else -> fix cuando esta vacio
					if(tmp.getsmj_workorder().isEmpty()){
						searchWorkOrder.setValue(null);
						searchWorkOrder.getComponent().setText("");
					}
					else{
						searchWorkOrder.setValue(tmp.getsmj_workorder().get(0));
						searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
					}
				}
			}
		});		
		}
	}

	private void searchPlateValueChange(ValueChangeEvent evt) {
		if (evt.getNewValue() == null) {
			
			if (order == null || tmp == null) {
				txtPlate.setText("");
				return;
			}
			
			if (bpartnerId > 0 && selectedPlate != null && !selectedPlate.isEmpty()) {		
				
				String whereClause = "UPPER(SMJ_Plate) = UPPER(?)";
				MSMJVehicle vehicle = new Query(Env.getCtx(), MSMJVehicle.Table_Name, whereClause, null)
						.setParameters(selectedPlate)
						.first();
				
				if (vehicle != null) {
					temparioName = vehicle.getsmj_servicerategroup().getName();
				}
				
				updatePlateAndVehicle(selectedPlate, null);
			} else if (bpartnerId < 1) {					
				updatePlateAndVehicle("", null);
			}

			loadTable(true);
			
			return;
		}

		int smj_vehicle_id = (Integer) evt.getNewValue();

		if (smj_vehicle_id < 1) {
			return;
		}

		MSMJVehicle vehicle = new MSMJVehicle(Env.getCtx(), smj_vehicle_id, null);
		selectedPlate = vehicle.getsmj_plate();
		txtPlate.setText(selectedPlate);

		if (!isLoadingServiceOrder) {
			bpartnerId = DataQueries.getVehicleTender(vehicle.getsmj_plate(), "1");
			bpartnerSearch.setValue(bpartnerId);
		}
		
		
		tmp.setsmj_plate(vehicle.getsmj_plate());
		tmp.setSMJ_Vehicle_ID(vehicle.getSMJ_Vehicle_ID());
		setPartner(order == null);
		order.set_ValueNoCheck("smj_plate", vehicle.getsmj_plate());
		order.set_ValueNoCheck("smj_vehicle_id", vehicle.getSMJ_Vehicle_ID());
		temparioName = vehicle.getsmj_servicerategroup().getName();

		if (temparioName == null || temparioName.isEmpty()) {
			MSMJServiceRateGroup srg =  new MSMJServiceRateGroup(Env.getCtx(), serviceRateGroupDefault, null);
			if (srg != null){
				temparioName = srg.getName();
			}
		}

		/*if (!tmp.getsmj_workorder().isEmpty()) {
			MRequest request = new MRequest(Env.getCtx(), tmp.getsmj_workorder().get(0), null);
			String plate = request.get_ValueAsString("smj_plate");
			if (!plate.equals(vehicle.getsmj_plate())) {
				tmp.setsmj_workorder(null);
				order.set_ValueNoCheck("smj_workorder", tmp.getWorkOrderDocumentNo());
				searchWorkOrder.getComponent().setText("");
			}
		}*/

		tmp.saveEx();
		order.saveEx();
		Env.getCtx().setProperty("#SMJ_Plate", vehicle.getsmj_plate());
		loadTable(true);
	}

	/**
	 * muestra la ventana de informacion de placas por tercero y trae la placa seleccionada - 
	 * show info plate window and returns selected plate data 
	 */
	private void showInfoPlate(String value){	
		if (!useTransport) {
			return;
		}

		searchPlate.getComponent().setText(value);		
		searchPlate.onEvent(new Event(Events.ON_CHANGE));
	}//showInfoPlate

	private void setPlate() {
		if (!useTransport || tmp.getsmj_plate() == null || tmp.getsmj_plate().isEmpty()) {
			return;
		}

		selectedPlate = tmp.getsmj_plate();
		txtPlate.setText(tmp.getsmj_plate());
		Env.getCtx().setProperty("#SMJ_Plate", tmp.getsmj_plate());
	}

	private void setWorkOrder() {
		if (!useTransport || tmp.getsmj_workorder().isEmpty()) {
			return;
		}

		searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
	}

	/**
	 * muestra la ventana de informacion de productos y trae el producto seleccionado - 
	 * para Transportes
	 * show info product window and returns selected product data  
	 * @param value
	 */
	private void showInfoProductTrans(String value) {
		InfoProductList dato = null;

		try {
			SMJSearchProducts search = new SMJSearchProducts();
			Boolean shServices = false;
			if (showService.equals("true")){
				shServices = true;
			}
			
			String locatorBranch = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"))).getName();
			LinkedList<InfoProductList> listProd =  search.searchProducts_Locator(value, locatorBranch, shServices, null);
			
			if (listProd.size() == 1 ) {
				Iterator<InfoProductList> itl = listProd.iterator();
				dato = itl.next();

				MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), 0, null);
				ipl.setM_Product_ID(dato.getProductID());
				ipl.setPriceActual(dato.getPrice());
				ipl.setPriceEntered(dato.getPrice());
				ipl.setM_Locator_ID(dato.getLocatorId());
				ipl.setC_BPartner_ID(dato.getPartnerId());
				ipl.setpurchaseprice(dato.getPurchasePrice());

				/*if (!validateQtyTrans(ipl.getM_Product_ID(), ipl.getM_Locator_ID(), true, true)) {
					return;
				}*/

				MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
				ipl.setQtyEntered(Env.ONE);
				ipl.setQtyOrdered(Env.ONE);
				ipl.setProductValue(p.getValue());
				ipl.setProductName(p.getName());
				ipl.setOldCode(p.getSKU());
				ipl.setUOMName(DataQueries.getUomName(p.getC_UOM_ID()));
				ipl.setC_UOM_ID(p.getC_UOM_ID());
				ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));
				ipl.setsmj_plate(selectedPlate);
				ipl.set_ValueNoCheck("SMJ_Vehicle_ID", tmp.getSMJ_Vehicle_ID());
				Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
				ipl.setM_AttributeSetInstance_ID(attInstance);
				ipl.settotal(ipl.getQtyEntered().multiply(ipl.getPriceEntered()));

				if (tmp == null) {
					tmp = new MSMJTmpWebSales(Env.getCtx(), 0, null);
					tmp.setC_BPartner_ID(bpartnerId);
					tmp.setC_BPartner_Location_ID(locationID);
					tmp.addWorkOrder((Integer) searchWorkOrder.getValue());
				}

				tmp.saveEx();
				listTmpSales();
				ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());
				MLocator loc = MLocator.get(Env.getCtx(), ipl.getM_Locator_ID());
				MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
				ipl.setM_Warehouse_ID(wh.getM_Warehouse_ID());
				ipl.setWarehouseName(wh.getName());
				ipl.setsmj_isworkorder("F");
				ipl.setLine(sequence+10);
				sequence = ipl.getLine();

				BigDecimal discount = (ipl.issmj_percentage()) ? ipl.getPriceEntered().multiply(ipl.getsmj_discount().divide(Env.ONEHUNDRED)) : ipl.getsmj_discount();			
				BigDecimal price2 = ipl.getPriceEntered().subtract(discount);
				String descnote="";
				if (ipl.getsmj_notes()!=null)	descnote=ipl.getsmj_notes().trim(); //iMarch
				
				Integer c_order_line_id = Documents.createOrderLine(order, ipl.getM_Product_ID(), ipl.getQtyEntered(), ipl.getC_UOM_ID(), price2
						, descnote, 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(), dateInvoice, ipl.getsmj_plate(), null, ipl.getLine());

				if (c_order_line_id > 0) {
					ipl.setC_OrderLine_ID(c_order_line_id);	
					ipl.saveEx();
					valideProductPVW(ipl.getM_Product_ID(),ipl.getC_OrderLine_ID());
					list.add(ipl);					
					loadTable();
					dataTable.setSelectedIndex(dataTable.getItemCount() - 1);
				}

				productTextBox.setFocus(true);
			} else {				
				EventListener<Event> event = new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), 0, null);

						String product = Env.getContext(Env.getCtx(),Env.TAB_INFO, "PRODUCT");
						ipl.setM_Product_ID(Integer.parseInt(product.trim()));
						String price = Env.getContext(Env.getCtx(),Env.TAB_INFO, "PRICE");
						ipl.setPriceActual(new BigDecimal(price));
						ipl.setPriceEntered(new BigDecimal(price));
						String locator = Env.getContext(Env.getCtx(),Env.TAB_INFO, "LOCATOR");
						ipl.setM_Locator_ID(Integer.parseInt(locator.trim()));
						String partner = Env.getContext(Env.getCtx(),Env.TAB_INFO, "PARTNER");
						ipl.setC_BPartner_ID(Integer.parseInt(partner.trim()));						
						try {
							String pprice = Env.getContext(Env.getCtx(),Env.TAB_INFO, "PURCHASE");
							ipl.setpurchaseprice(new BigDecimal(pprice));
						} catch (Exception e) {
						}

						/*if (!validateQtyTrans(ipl.getM_Product_ID(), ipl.getM_Locator_ID(), true, true)) {
							return;
						}*/
						
						MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
						ipl.setQtyEntered(Env.ONE);
						ipl.setQtyOrdered(Env.ONE);
						ipl.setProductValue(p.getValue());
						ipl.setProductName(p.getName());
						ipl.setOldCode(p.getSKU());
						try {
							BigDecimal tax = DataQueries.getTaxValue(p.getC_TaxCategory_ID());
							if (tax!= null){
								ipl.settax(tax);
							}
						} catch (Exception e) {
						}
						ipl.setUOMName(DataQueries.getUomName(p.getC_UOM_ID()));
						ipl.setC_UOM_ID(p.getC_UOM_ID());
						ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));
						ipl.setsmj_plate(selectedPlate);
						ipl.set_ValueNoCheck("SMJ_Vehicle_ID", tmp.getSMJ_Vehicle_ID());
						Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
						ipl.setM_AttributeSetInstance_ID(attInstance);
						ipl.settotal(ipl.getQtyEntered().multiply(ipl.getPriceEntered()));
						if (tmp == null) {
							tmp = new MSMJTmpWebSales(Env.getCtx(), 0, null);
							tmp.setC_BPartner_ID(bpartnerId);
							tmp.setC_BPartner_Location_ID(locationID);
							tmp.addWorkOrder((Integer) searchWorkOrder.getValue());
						}
						tmp.saveEx();
						listTmpSales();
						ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());
						MLocator loc = MLocator.get(Env.getCtx(), ipl.getM_Locator_ID());
						MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
						ipl.setM_Warehouse_ID(wh.getM_Warehouse_ID());
						ipl.setWarehouseName(wh.getName());
						ipl.setsmj_isworkorder("F");
						ipl.setLine(sequence+10);
						sequence = ipl.getLine();

						BigDecimal discount = (ipl.issmj_percentage()) ? ipl.getPriceEntered().multiply(ipl.getsmj_discount().divide(Env.ONEHUNDRED)) : ipl.getsmj_discount();			
						BigDecimal price2 = ipl.getPriceEntered().subtract(discount);
						
						String descnote="";
						if (ipl.getsmj_notes()!=null)	descnote=ipl.getsmj_notes().trim(); //iMarch

						Integer c_order_line_id = Documents.createOrderLine(order, ipl.getM_Product_ID(), ipl.getQtyEntered(), ipl.getC_UOM_ID(), price2
								, descnote, 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(), dateInvoice, ipl.getsmj_plate(), null, ipl.getLine());

						if (c_order_line_id > 0) {
							ipl.setC_OrderLine_ID(c_order_line_id);	
							ipl.saveEx();	
							valideProductPVW(ipl.getM_Product_ID(),ipl.getC_OrderLine_ID());
							list.add(ipl);					
							loadTable();
							dataTable.setSelectedIndex(dataTable.getItemCount() - 1);
						} 

						productTextBox.setFocus(true);
					}
				};

				SMJInfoProduct ip = new SMJInfoProduct(Env.TAB_INFO, "M_Product", "M_Product_ID", false, "", listProd, shServices, null); 
				ip.setVisible(true);
				ip.setTitle(Msg.getMsg(Env.getCtx(), "SMJLabelSearchProducts"));
				ip.setStyle("border: 2px");
				ip.setClosable(true);
				ip.setParent(form);
				ip.addEventListener(event);
				AEnv.showCenterScreen(ip);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".showInfoProduct - ERROR: " + e.getMessage(), e);
		}
	}//showInfoProduct

	/**
	 * Muestra una Alerta cuando el producto ingresado se encuentra en una OV sin Factura del mismo Vehiculo
	 * show message when the same product are in Sales Order that not invoiced  
	 * @param productid
	 */
	private void valideProductPVW(int productid, int orderlineid){
		
		if(!orgValidateProductPVW) return;
		int vehiculeID = tmp.getSMJ_Vehicle_ID();
		if (useTransport && vehiculeID>0) {
			
			StringBuilder sql = new StringBuilder("SELECT COUNT(DISTINCT C_Order_ID) ");
			sql.append(" FROM C_OrderLine ol WHERE M_Product_ID=").append(productid);
			sql.append(" AND C_Order_ID IN ");
			sql.append("(");
			sql.append(
		            "SELECT C_Order_ID "
		            + "FROM C_Invoice_Candidate_v ic, AD_Org o, C_BPartner bp, C_DocType dt "
		            + "WHERE ic.AD_Org_ID=o.AD_Org_ID"
		            + " AND ic.C_BPartner_ID=bp.C_BPartner_ID"
		            + " AND ic.C_DocType_ID=dt.C_DocType_ID"
		            + " AND ic.AD_Client_ID=").append(lclientId);
	        if (lorgId != null)
	            sql.append(" AND ic.AD_Org_ID=").append(lorgId);
	        if (tmp.getC_BPartner_ID() > 0)
	            sql.append(" AND ic.C_BPartner_ID=").append(tmp.getC_BPartner_ID());
	        sql.append(" AND ic.C_Order_ID IN "
	        		+ "( SELECT C_Order_ID FROM C_Order ov WHERE ov.C_Order_ID=ic.C_Order_ID "
	        		+ " AND ov.SMJ_Vehicle_ID=").append(vehiculeID).append(" )");
	        sql.append(" UNION ALL ");
	        sql.append(" SELECT C_Order_ID ");
	        sql.append(" FROM smj_tmpwebsales ts WHERE AD_Client_ID=").append(lclientId);
	        if (lorgId != null)
	            sql.append(" AND AD_Org_ID=").append(lorgId);
	        if (tmp.getC_BPartner_ID() > 0)
	        	sql.append(" AND C_BPartner_ID=").append(tmp.getC_BPartner_ID());
	        sql.append(" AND smj_tmpwebsales_ID IN ( "
	        		+ " SELECT smj_tmpwebsales_ID "
	        		+ " FROM smj_tmpwebsalesline tsl "
	        		+ " WHERE tsl.smj_tmpwebsales_ID=ts.smj_tmpwebsales_ID"
	        		+ " AND SMJ_Vehicle_ID=").append(vehiculeID);
	        	sql.append(" AND tsl.c_orderline_id!=").append(orderlineid);
	        	sql.append(" AND tsl.m_product_id=").append(productid);
	        	sql.append(")");
	        sql.append(" )");
			
	        int ovs=0;
	        PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				pstmt = DB.prepareStatement(sql.toString(), null);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					ovs = rs.getInt(1);
					break;
				}// while
			} catch (Exception e) {
				log.log(Level.SEVERE, sql.toString(), e);
			} finally {
				DB.close(rs, pstmt);
				rs = null;
				pstmt = null;
			}
	        
			if(ovs > 0)
				Message.showWarning("El Vehiculo "+tmp.getsmj_plate()+" tiene ("+ovs+")OVs/Tickets abiertos con el mismo Producto");
		}
		
	}
	
	private Boolean validateQtyTrans(){
		Boolean flag = true;
		try {
			Iterator<MSMJTmpWebSalesLine> itl = list.iterator();
			while (itl.hasNext()){
				MSMJTmpWebSalesLine xp = itl.next();
				MProduct p = MProduct.get(Env.getCtx(), xp.getM_Product_ID());
				BigDecimal totRepet = xp.getQtyEntered();
				if (!p.getProductType().equals("S")){
					Iterator<MSMJTmpWebSalesLine> itz = list.iterator();
					while (itz.hasNext()){
						MSMJTmpWebSalesLine zp = itz.next();
						if(xp.getM_Product_ID()== zp.getM_Product_ID() && xp.getM_Locator_ID()==zp.getM_Locator_ID()
								&& xp.getLine()!=zp.getLine()){
							totRepet = totRepet.add(zp.getQtyEntered());
						}
					}//while (itz.hasNext())
					BigDecimal qty = DataQueries.getTotalProductByLocator(xp.getM_Product_ID(), xp.getM_Locator_ID(), null);
					if (totRepet.compareTo(qty)>0){
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGQtyEntered")+" - "+xp.getProductName(), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return false;
					}//if (xp.getM_Locator_ID().compareTo(qty)>0)	
					if(p.getM_Product_Category_ID()==rmnFacCategory){
						if(xp.getPriceEntered().compareTo(Env.ZERO)<=0){
							Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGValueGreaterZero")+" - "+xp.getProductName(), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
							return false;
						}
					}
				}//if (!p.getProductType().equals("S"))
			}//while
		} catch (Exception e) {
			flag = false;
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".validateQty ERROR: "+e.getMessage(), e);
		}
		return flag;
	}//validateQtyTrans

	private boolean validateQtyTrans(int M_Product_ID, int M_Locator_ID, boolean newLine, boolean showMsg) {
		boolean valid = true;
		BigDecimal totq = Env.ZERO;

		Iterator<MSMJTmpWebSalesLine> it = list.iterator();
		MProduct product = new MProduct(Env.getCtx(), M_Product_ID, null);

		//When the product is a service, not validate qty on hand
		if (product.getProductType().equals(MProduct.PRODUCTTYPE_Service)) 
			return valid;	

		while (it.hasNext()) { // check all he items of the list
			MSMJTmpWebSalesLine ipl = it.next();

			if (M_Product_ID == ipl.getM_Product_ID() && M_Locator_ID == ipl.getM_Locator_ID()) { // mira el total para el producto todas las lineas

				BigDecimal qtyEntered;

				if (product.getC_UOM_ID() == ipl.getC_UOM_ID()) {
					qtyEntered = ipl.getQtyEntered();
				} else {
					BigDecimal divideRate = DataQueries.getUOMDivideRate(M_Product_ID, ipl.getC_UOM_ID());
					qtyEntered = ipl.getQtyEntered().multiply(divideRate);
				}

				totq = totq.add(qtyEntered);
			}
		}

		BigDecimal qtyWh = DataQueries.getTotalProductByLocator(M_Product_ID, M_Locator_ID, null);
		BigDecimal currentUsedStock = DataQueries.getTotalTempProductByLocator(M_Product_ID, M_Locator_ID, null).subtract(totq);
		
		//Validate when currentStock is negative (Bug)
		if (currentUsedStock.signum() == -1)  {
			currentUsedStock = Env.ZERO;
		}
		
		qtyWh = qtyWh.subtract(currentUsedStock);
		
		// When is a new line, it is necessary add 1 unit
		if (newLine) {			
			totq = totq.add(Env.ONE);		
		}

		if (qtyWh.signum() == 0 || totq.compareTo(qtyWh) > 0) {
			if (showMsg) {
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGQtyEntered"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
			return false;
		} else {
			return true;
		}
	}

	/**
	 * carga una orden de trabajo de seveter - 
	 * load work order seveter
	 * @param tmpRequest
	 * @param wot
	 */
	private void loadSalesSeveter(MRequest tmpRequest, String trxName) throws Exception {		
		LinkedList<MSMJWorkOrderLine> woList = DataQueries.getWOLines(tmpRequest.get_ID());
		Iterator<MSMJWorkOrderLine> itWo = woList.iterator();
		List<MSMJTmpWebSalesLine> trxLines = new ArrayList<MSMJTmpWebSalesLine>();
		
		while(itWo.hasNext()) {
			MSMJWorkOrderLine wl = itWo.next();
			MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), 0, trxName);
			
			if (wl!=null){
				ipl.setR_Request_ID(tmpRequest.get_ID());
				ipl.set_ValueNoCheck("smj_workOrderLine_ID", wl.getsmj_workOrderLine_ID());
				ipl.setM_Product_ID(wl.getM_Product_ID());
				ipl.setPriceEntered(wl.getPrice());
				ipl.setPriceActual(wl.getPrice());
				ipl.setM_Locator_ID(wl.getlocator_ID());
				ipl.setC_BPartner_ID(wl.getprovider_ID());
				ipl.setQtyOrdered(wl.getQty());
				ipl.setQtyEntered(wl.getQty());   // fix   tqy from worload order
				ipl.setpurchaseprice(wl.getPrice());
				MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
				ipl.setProductValue(p.getValue());
				ipl.setProductName(p.getName());
				ipl.setR_Request_ID(tmpRequest.getR_Request_ID());   // fix solicitud 1000384 y 1000385
				BigDecimal tax = DataQueries.getTaxValue(p.getC_TaxCategory_ID());
				if (tax!= null){
					ipl.settax(tax);
				}
				ipl.setUOMName(DataQueries.getUomName(p.getC_UOM_ID()));
				ipl.setC_UOM_ID(p.getC_UOM_ID());
				ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));				
				boolean hasOwner = DataQueries.getVehicleTender(tmpRequest.get_ValueAsString("SMJ_Plate"), "1") > 0;				
				
				if (hasOwner) {
					ipl.set_ValueNoCheck("SMJ_Vehicle_ID", tmpRequest.get_ValueAsInt("SMJ_Vehicle_ID"));
				}
				
				ipl.setsmj_plate(tmpRequest.get_ValueAsString("SMJ_Plate"));
				Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
				ipl.setM_AttributeSetInstance_ID(attInstance);
				ipl.settotal(ipl.getQtyEntered().multiply(ipl.getPriceEntered()));
				listTmpSales();
				ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());
				MLocator loc = MLocator.get(Env.getCtx(), ipl.getM_Locator_ID());
				MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
				ipl.setM_Warehouse_ID(wh.getM_Warehouse_ID());
				ipl.setWarehouseName(wh.getName());
				ipl.setsmj_isworkorder("Y");
				ipl.setLine(sequence+10);
				if(wl.getobservations()!=null)	ipl.setsmj_notes(wl.getobservations()); //iMarch  detalle de Orden mantenimiento

				BigDecimal discount = (ipl.issmj_percentage()) ? ipl.getPriceEntered().multiply(ipl.getsmj_discount().divide(Env.ONEHUNDRED)) : ipl.getsmj_discount();			
				BigDecimal price = ipl.getPriceEntered().subtract(discount);
				Integer c_order_line_id = Documents.createOrderLineTrans(order, ipl.getM_Product_ID(), ipl.getQtyEntered(), ipl.getC_UOM_ID(), price
						, (wl.getobservations()!=null ? wl.getobservations() : "" ), 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(), dateInvoice, ipl.getsmj_plate(), null, ipl.getLine(), trxName);

				ipl.setC_OrderLine_ID(c_order_line_id);
				sequence = ipl.getLine();
				///validacion que no exista elemento repetido en la lista, deshabilitada.
				//validateElementExist(ipl);
				
				ipl.saveEx();
				trxLines.add(ipl);					
			}//if (wl!=null){
		}//while(itWo.hasNext())
		
		list.addAll(trxLines);

		if (txtPlate.getText() == null || txtPlate.getText().isEmpty()) {
			txtPlate.setText(tmp.getsmj_plate());
		}
	}//completeWeb

	/**
	 * carga las ventas de infotrans - 
	 * load infotrans sales.
	 * @param tmpRequest
	 * @param code
	 */
	private void loadSalesInfotrans(MRequest tmpRequest) throws Exception {
		MSMJTmpWebSalesLine ipl = new MSMJTmpWebSalesLine(Env.getCtx(), 0, tmpRequest.get_TrxName());
		Integer productID = tmpRequest.getM_Product_ID();
		BigDecimal totFac = Env.ONE;

		try {
			totFac = new BigDecimal(tmpRequest.get_ValueAsString("smj_invoicevalue"));
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName() + ".calculateBrokeragePercent - ERROR: " + e.getMessage(), e);
		}

		ipl.setM_Product_ID(productID);
		ipl.setPriceEntered(totFac);
		ipl.setPriceActual(totFac);
		ipl.setsmj_isworkorder("F");

		ipl.setM_Locator_ID(0);
		ipl.setC_BPartner_ID(0);
		ipl.setQtyEntered(Env.ONE);
		ipl.setQtyOrdered(Env.ONE);

		//ipl.setsmj_plate(selectedPlate);
		//String whereClause = "SMJ_Plate LIKE '" + tmpRequest.get_Value("SMJ_Plate") + "'";
		//MSMJVehicle vehicle = new Query(Env.getCtx(), MSMJVehicle.Table_Name, whereClause, null).first();
		ipl.set_ValueNoCheck("SMJ_Vehicle_ID", tmpRequest.get_ValueAsInt("SMJ_Vehicle_ID"));
		ipl.setsmj_plate(tmpRequest.get_ValueAsString("SMJ_Plate"));

		ipl.setpurchaseprice(Env.ZERO);
		ipl.setR_Request_ID(tmpRequest.getR_Request_ID()); // fix solicitud
		// 1000384 y 1000385

		MProduct p = new MProduct(Env.getCtx(), ipl.getM_Product_ID(), null);
		ipl.setProductValue(p.getValue());
		ipl.setProductName(p.getName());
		ipl.setOldCode(p.getSKU());

		try {
			BigDecimal tax = DataQueries.getTaxValue(p.getC_TaxCategory_ID());
			if (tax != null) {
				ipl.settax(tax);
			}
		} catch (Exception e) {
		}

		ipl.setUOMName(DataQueries.getUomName(p.getC_UOM_ID()));
		ipl.setC_UOM_ID(p.getC_UOM_ID());
		ipl.setpartnername(DataQueries.getBpartnerName(Env.getAD_Client_ID(Env.getCtx()), ipl.getC_BPartner_ID()));
		ipl.setQtyEntered(Env.ONE);

		Integer attInstance = DataQueries.getAttInstanceId(ipl.getM_Locator_ID(), ipl.getM_Product_ID());
		ipl.setM_AttributeSetInstance_ID(attInstance);
		ipl.settotal(ipl.getQtyEntered().multiply(ipl.getPriceEntered()));
		listTmpSales();
		ipl.setsmj_tmpwebsales_ID(tmp.getsmj_tmpwebsales_ID());
		MLocator loc = MLocator.get(Env.getCtx(), ipl.getM_Locator_ID());
		MWarehouse wh = MWarehouse.get(Env.getCtx(), loc.getM_Warehouse_ID());
		ipl.setM_Warehouse_ID(wh.getM_Warehouse_ID());
		ipl.setWarehouseName(wh.getName());
		ipl.setLine(sequence + 10);
		sequence = ipl.getLine();

		BigDecimal discount = (ipl.issmj_percentage()) ? ipl.getPriceEntered().multiply(ipl.getsmj_discount().divide(Env.ONEHUNDRED)) : ipl.getsmj_discount();
		BigDecimal price = ipl.getPriceEntered().subtract(discount);
		Integer c_order_line_id = Documents.createOrderLineTrans(order, ipl.getM_Product_ID(), ipl.getQtyEntered(),
				ipl.getC_UOM_ID(), price, "", 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(),
				dateInvoice, ipl.getsmj_plate(), null, ipl.getLine(), tmpRequest.get_TrxName());

		ipl.setC_OrderLine_ID(c_order_line_id);

		ipl.saveEx();
		list.add(ipl);

		if (txtPlate.getText() != null && !txtPlate.getText().isEmpty()) {
			txtPlate.setText(tmp.getsmj_plate());
		}
	}// loadSalesInfotrans

	public void updatePlateAndVehicle(String plate, Integer vehicleId) {
		tmp.setsmj_plate(plate);
		tmp.set_ValueNoCheck("SMJ_Vehicle_ID", vehicleId);
		order.set_ValueNoCheck("SMJ_Plate", plate);
		order.set_ValueNoCheck("SMJ_Vehicle_ID", vehicleId);
		tmp.saveEx();
		order.saveEx();

		for (MSMJTmpWebSalesLine line : list) {
			if (line.getR_Request_ID() < 0) {
				line.set_ValueNoCheck("SMJ_Vehicle_ID", vehicleId);
				line.saveEx();
			}
		}
		
		searchPlate.setValue(vehicleId);
		txtPlate.setText(plate);
		Env.getCtx().setProperty("#SMJ_Plate", plate);
	}

	private void deleteWorkOrderLines(String trxName) {
		int index = dataTable.getSelectedIndex();

		if (index >= 0) {
			int R_Request_ID = list.get(index).getR_Request_ID();
			List<MSMJTmpWebSalesLine>  listTemp = new ArrayList<MSMJTmpWebSalesLine>();

			for (int i = 0; i < list.size(); i++) {
				MSMJTmpWebSalesLine ipl = list.get(i);
				ipl.set_TrxName(trxName);
				if (ipl.getR_Request_ID() == R_Request_ID) {
					listTemp.add(ipl); // remove from the ArrayList list
					MOrderLine orderLine = new MOrderLine(Env.getCtx(), ipl.getC_OrderLine_ID(), trxName);

					ipl.setQtyOrdered(Env.ZERO);
					ipl.saveEx();
					ipl.delete(true); // remove the physical row
					orderLine.setQtyReserved(Env.ZERO);
					orderLine.delete(true);							
				}
			}	

			list.removeAll(listTemp);

			tmp.removeWO(R_Request_ID);
			tmp.saveEx();
		} else {
			Message.showWarning("SMJMSGSelectItemList");
		}
		loadTable();
		searchWorkOrder.getComponent().setText(tmp.getWorkOrderDocumentNo());
	}

	private boolean validatePrice() {

		Boolean status = true;

		for (MSMJTmpWebSalesLine salesLine : list) {
			if (salesLine.getPriceEntered().signum() < 1) {
				Message.showError(Msg.parseTranslation(Env.getCtx(), "@M_Product_ID@: " + salesLine.getM_Product().getName() + " -> @SMJMSGValueGreaterZero@"), true);
				status = false;
				break;
			}
		}

		return status;
	}
	
	public void onPrint() 
	{
		
		String info = "";
		int smj_tmpWebSales_ID = tmp.get_ID(); 
		int AD_Process_ID = m_AD_Process_ID;
		if(AD_Process_ID==0) {
			Message.showWarning("No hay reporte configurado", false);
			log.log(Level.WARNING, "Falta configurar variable de configuracion iM_PrintTicket");
			return;
		}
		PO proceso = new MProcess(Env.getCtx(), AD_Process_ID, null);
		ProcessInfo pi = new ProcessInfo("Nota de Remision", proceso.get_ID());
		MPInstance instance = new MPInstance(Env.getCtx(), proceso.get_ID(), 0);
		if (!instance.save())
		{
			info = Msg.getMsg(Env.getCtx(), "ProcessNoInstance");
			log.log(Level.WARNING, info);
			return ;
		}
		pi.setRecord_ID(0);
		pi.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		pi.setClassName(JASPER_STARTER_CLASS);
		
		//----Parameter Locator
		MPInstancePara parametro = new MPInstancePara(instance, 0);
		parametro.setParameterName("smj_tmpWebSales_ID");	//Name of Column DB in  Process
		parametro.setP_Number(smj_tmpWebSales_ID);	//ID or Value of parameter
		parametro.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		if (!parametro.save())
		{
			log.log(Level.SEVERE, "No se pudo Guardar datos del Parametro smj_tmpWebSales_ID. (AD_PInstance_Para)");
			return ;
		}
				
		if(!ProcessUtil.startJavaProcess(Env.getCtx(), pi, null))
			log.log(Level.SEVERE, "Error al generar PDF Jasper");
		
	}
	
	
	// Loading Process
	private boolean isLoadingServiceOrder = false;
	
	// Loyalty Fields
	private Checkbox chbBeneficiary = new Checkbox();
	private Textbox txtPoints = new Textbox();

	// Trans Fields
	private MLookup lookupWorkOrder;
	private MLookup lookupPlate;
	private WSearchEditor searchWorkOrder;
	private WSearchEditor searchPlate;
	private Textbox txtPlate;
	private Textbox productTextBox = new Textbox();
	private Checkbox generateOnlyShipment;
	private int indexPlate;

	private static String showService;
	private static Integer codeServiceMtto;
	private static Integer tiresCategory;
	private static Integer rmnFacCategory;
	private static Integer serviceRateGroupDefault;
	private Integer orgInfotrans;
	private String temparioName = "";
	private String orgfreighttrans = "false";	//Adicion iMarch
}// WSimplifiedSales

