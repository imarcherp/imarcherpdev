package com.smj.webui.renders;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.ListCell;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.WTableColumn;
import org.adempiere.webui.component.ZkCssHelper;
import org.adempiere.webui.event.TableValueChangeEvent;
import org.adempiere.webui.event.TableValueChangeListener;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MImage;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Language;
import org.compiere.util.MSort;
import org.compiere.util.Util;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import com.smj.util.SMJConfig;


public class SMJListItemRenderer extends WListItemRenderer {

	/** Array of listeners for changes in the table components. */
	protected ArrayList<TableValueChangeListener> m_listeners =
            new ArrayList<TableValueChangeListener>();

	/** A list containing the indices of the currently selected ListItems. */
	private Set<ListItem> m_selectedItems = new HashSet<ListItem>();
	/**	Array of table details. */
	private ArrayList<WTableColumn> m_tableColumns = new ArrayList<WTableColumn>();

    private Listbox listBox;

	private EventListener<Event> cellListener;
	
	private Integer columnValidation;
	
	/** Array of data for a column */
	private Map<Integer, List<KeyNamePair>> data2Fill;

	/**
	 * Default constructor.
	 *
	 */
	public SMJListItemRenderer()
	{
		super();
	}

	/**
	 * Constructor specifying the column headers.
	 *
	 * @param columnNames	vector of column titles.
	 */
	public SMJListItemRenderer(List< ? extends String> columnNames)
	{
		super(columnNames);
		WTableColumn tableColumn;

		for (String columnName : columnNames)
		{
			tableColumn = new WTableColumn();
			tableColumn.setHeaderValue(Util.cleanAmp(columnName));
			m_tableColumns.add(tableColumn);
		}
	}

	/* (non-Javadoc)
	 * @see org.zkoss.zul.ListitemRenderer#render(org.zkoss.zul.Listitem, java.lang.Object)
	 */
	@Override
	public void render(Listitem item, Object data, int index) throws Exception
	{
		render((ListItem)item, data, index);
	}

	/**
	 * Renders the <code>data</code> to the specified <code>Listitem</code>.
	 *
	 * @param item 	the listitem to render the result.
	 * 				Note: when this method is called, the listitem has no child
	 * 				at all.
	 * @param data 	that is returned from {@link ListModel#getElementAt}
	 * @throws Exception
	 * @see {@link #render(Listitem, Object)}
	 */
	private void render(ListItem item, Object data, int index)
	{
		Listcell listcell = null;
		int colIndex = 0;
		int rowIndex = item.getIndex();
		WListbox table = null;

		if (item.getListbox() instanceof WListbox)
		{
			table = (WListbox)item.getListbox();
		}

		if (!(data instanceof List))
		{
			throw new IllegalArgumentException("A model element was not a list");
		}

		if (listBox == null || listBox != item.getListbox())
		{
			listBox = item.getListbox();
		}
		if (cellListener == null)
		{
			cellListener = new CellListener();
		}

		for (Object field : (List<?>)data)
		{
			boolean isEditable = true;
			if (columnValidation != null && columnValidation >= 0) 
				isEditable = !((Boolean) ((List<?>) data).get(columnValidation));
			
			listcell = getCellComponent(table, field, rowIndex, colIndex, isEditable);
			listcell.setParent(item);
			listcell.addEventListener(Events.ON_DOUBLE_CLICK, cellListener);
			colIndex++;
		}

		return;
	}

	/**
	 * Generate the cell for the given <code>field</code>.
	 *
	 * @param table 	The table into which the cell will be placed.
	 * @param field		The data field for which the cell is to be created.
	 * @param rowIndex	The row in which the cell is to be placed.
	 * @param columnIndex	The column in which the cell is to be placed.
	 * @return	The list cell component.
	 */
	private Listcell getCellComponent(WListbox table, Object field, int rowIndex, int columnIndex, boolean isEditable)
	{
		ListCell listcell = new ListCell();
		if (m_tableColumns.size() > columnIndex) {
			WTableColumn column = getColumn(columnIndex);
			if (column != null && column.getHeaderValue() != null) {
				listcell.setWidgetAttribute(AdempiereWebUI.WIDGET_INSTANCE_NAME, column.getHeaderValue().toString());
			}
		}
		boolean isCellEditable = (table != null ? table.isCellEditable(rowIndex, columnIndex) : false) && isEditable;
		
        // TODO put this in factory method for generating cell renderers, which
        // are assigned to Table Columns
		if (field != null)
		{
			if (field instanceof Boolean)
			{
				listcell.setValue(Boolean.valueOf(field.toString()));

				if (table != null && columnIndex == 0)
					table.setCheckmark(false);
				Checkbox checkbox = new Checkbox();
				checkbox.setChecked(Boolean.valueOf(field.toString()));

				if (isCellEditable)
				{
					checkbox.setEnabled(true);
					checkbox.addEventListener(Events.ON_CHECK, this);
				}
				else
				{
					checkbox.setEnabled(false);
				}

				listcell.appendChild(checkbox);
				ZkCssHelper.appendStyle(listcell, "text-align:center");
			}
			else if (field instanceof Number)
			{
				if (m_tableColumns != null && columnIndex < m_tableColumns.size()
						&& m_tableColumns.get(columnIndex).getColumnClass() != null
						&& m_tableColumns.get(columnIndex).getColumnClass().getName().equals(MImage.class.getName()) 
						&& field instanceof Integer)
				{
					MImage mImage = MImage.get(Env.getCtx(), (Integer) field);
					AImage img = null;
					byte[] data = mImage.getData();
					if (data != null && data.length > 0) {
						try {
							img = new AImage(null, data);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}
					Image image = new Image();
					image.setContent(img);
					image.setStyle("width: 48px; height: 48px;");
					listcell.appendChild(image);
					listcell.setStyle("text-align: center;");
				}
				//If Listbox get Data for the column
				else if (m_tableColumns != null && columnIndex < m_tableColumns.size()
						&& m_tableColumns.get(columnIndex).getColumnClass() != null
						&& m_tableColumns.get(columnIndex).getColumnClass().getName().equals(org.adempiere.webui.component.Listbox.class.getName()) 
						&& field instanceof Integer) {
					listcell.setValue(field.toString());
					
					org.adempiere.webui.component.Listbox listBox;
					
					if (data2Fill != null && data2Fill.containsKey(columnIndex)) {
						listBox = new org.adempiere.webui.component.Listbox((KeyNamePair[]) data2Fill.get(columnIndex).toArray());
						if (field != null) {
							for (Listitem item: listBox.getItems()) {
								if (item.getValue().equals(field)) {
									listBox.setSelectedItem(item);
								}
							}
						}
					} else {
						listBox = new org.adempiere.webui.component.Listbox();
					}
					
					listBox.setMold("select");
					//listBox.setWidth("100 %");
					//listBox.setHflex("1");
					ZKUpdateUtil.setWidth(listBox, "100%");
					ZKUpdateUtil.setHflex(listBox, "1");

					if (isCellEditable)
					{
						listBox.setEnabled(true);
						listBox.addEventListener(Events.ON_SELECT, this);
					}
					else
					{
						listBox.setEnabled(false);
					}

					listcell.appendChild(listBox);
					ZkCssHelper.appendStyle(listcell, "text-align:center");
				}
				else
				{
					Language lang = AEnv.getLanguage(Env.getCtx());
					int displayType = (field instanceof BigDecimal || field instanceof Double || field instanceof Float)
							? DisplayType.Amount
						    : DisplayType.Integer;
					DecimalFormat format = DisplayType.getNumberFormat(displayType, lang);
					format.setMaximumFractionDigits(2);
					format.setMinimumFractionDigits(2);

					// set cell value to allow sorting
					listcell.setValue(field.toString());

					if (isCellEditable)
					{
						NumberBox numberbox = new NumberBox(false);
						numberbox.getDecimalbox().setFormat(format.toPattern());
						numberbox.getDecimalbox().setLocale(lang.getLocale());
						numberbox.setFormat(format);
						numberbox.setValue(field);
//						numberbox.setWidth("100px");
						numberbox.setEnabled(true);
						numberbox.setStyle("text-align:right; width: 96%;"
										+ listcell.getStyle());
						numberbox.addEventListener(Events.ON_CHANGE, this);
						listcell.appendChild(numberbox);
					}
					else
					{
						listcell.setLabel(format.format(((Number)field).doubleValue()));
						ZkCssHelper.appendStyle(listcell, "width: 96%; text-align: right");
					}
				}
			}
			else if (field instanceof Timestamp)
			{

				SimpleDateFormat dateFormat = DisplayType.getDateFormat(DisplayType.Date, AEnv.getLanguage(Env.getCtx()));
				listcell.setValue(dateFormat.format((Timestamp)field));
				if (isCellEditable)
				{
					Datebox datebox = new Datebox();
					datebox.setValue(new Date(((Timestamp)field).getTime()));
					datebox.addEventListener(Events.ON_CHANGE, this);
					listcell.appendChild(datebox);
				}
				else
				{
					listcell.setLabel(dateFormat.format((Timestamp)field));
					ZkCssHelper.appendStyle(listcell, "width: 96%; margin: auto");
				}
			}
			else if (field instanceof InputStream) {
				try {
					if (field != null) {	
						Integer AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
						Integer size = SMJConfig.getIntValue("SizeImage", 24, AD_Org_ID);
						BufferedImage bImage = com.smj.util.Util.scaleImage(ImageIO.read((BufferedInputStream) field), size, size);
						Button button = new Button();
						button.setImageContent(bImage);
						button.setStyle("width: " + size + "px; height: " + size + "px; padding: 0px;");
						button.addEventListener(Events.ON_CLICK, this);
						listcell.appendChild(button);
						listcell.setStyle("text-align: center;");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}		
			else if (field instanceof String)
			{
				if (m_tableColumns != null && columnIndex < m_tableColumns.size()
						&& m_tableColumns.get(columnIndex).getColumnClass() != null
						&& m_tableColumns.get(columnIndex).getColumnClass().getName().equals(Boolean.class.getName()))
				{
					boolean isTrue = field.toString().equals("Y");
					listcell.setValue(isTrue);
					Checkbox checkBox = new Checkbox();
					checkBox.setSelected(isTrue);
					checkBox.setEnabled(isCellEditable);
					listcell.appendChild(checkBox);
				} else {
					listcell.setValue(field.toString());
					if (isCellEditable)
					{
						Textbox textbox = new Textbox();
						textbox.setValue(field.toString());
						textbox.addEventListener(Events.ON_CHANGE, this);
						ZkCssHelper.appendStyle(textbox, "width: 96%;");
						listcell.appendChild(textbox);
					}
					else
					{
						listcell.setLabel(field.toString());
						ZkCssHelper.appendStyle(listcell, "width: 96%;");
					}
				}
			}
			// if ID column make it invisible
			else if (field instanceof IDColumn)
			{
				listcell.setValue(((IDColumn) field).getRecord_ID());
				if (!table.isCheckmark()) {
					table.setCheckmark(true);
					table.removeEventListener(Events.ON_SELECT, this);
					table.addEventListener(Events.ON_SELECT, this);
				}
			} 			
			else
			{
				listcell.setLabel(field.toString());
				listcell.setValue(field.toString());
				ZkCssHelper.appendStyle(listcell, "width: 96%;");
			}
			
		}
		else
		{
			listcell.setLabel("");
			listcell.setValue("");
		}

		return listcell;
	}

	/**
	 * @param columnValidation
	 */
	public void setColumnValidation(Integer columnValidation) {
		this.columnValidation = columnValidation;
	}
	
	/**
	 * @param header
	 */
	public void addColumn(String header)
	{
		addColumn(header, null);
	}
	
	/**
	 *  Add Table Column.
	 *  after adding a column, you need to set the column classes again
	 *  (DefaultTableModel fires TableStructureChanged, which calls
	 *  JTable.tableChanged .. createDefaultColumnsFromModel
	 *  @param header The header text for the column
	 *  @param description
	 */
	public void addColumn(String header, String description)
	{
		WTableColumn tableColumn;

		tableColumn = new WTableColumn();
		tableColumn.setHeaderValue(Util.cleanAmp(header));
		tableColumn.setTooltipText(description);
		m_tableColumns.add(tableColumn);

		return;
	}   //  addColumn

	public static class ColumnComparator implements Comparator<Object>
    {

    	private int columnIndex;
		private MSort sort;

		public ColumnComparator(boolean ascending, int columnIndex)
    	{
    		this.columnIndex = columnIndex;
    		sort = new MSort(0, null);
        	sort.setSortAsc(ascending);
    	}

        public int compare(Object o1, Object o2)
        {
                Object item1 = ((List<?>)o1).get(columnIndex);
                Object item2 = ((List<?>)o2).get(columnIndex);
                return sort.compare(item1, item2);
        }

		public int getColumnIndex()
		{
			return columnIndex;
		}
    }

	/**
	 * Reset the renderer.
	 * This should be called if the table using this renderer is cleared.
	 */
	public void clearColumns()
	{
		m_tableColumns.clear();
	}

	/**
	 * Clear the renderer.
	 * This should be called if the table using this renderer is cleared.
	 */
	public void clearSelection()
	{
		m_selectedItems.clear();
	}

	/**
	 * Add a listener for changes in the table's component values.
	 *
	 * @param listener	The listener to add.
	 */
	public void addTableValueChangeListener(TableValueChangeListener listener)
	{
	    if (listener == null)
	    {
	    	return;
	    }

	    m_listeners.add(listener);
	}
	
	class CellListener implements EventListener<Event> {

		public CellListener() {
		}

		public void onEvent(Event event) throws Exception {
			if (listBox != null && Events.ON_DOUBLE_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_DOUBLE_CLICK, listBox);
				Events.sendEvent(listBox, evt);
			}
		}

	}

	/**
	 * Get the column details of the specified <code>column</code>.
	 *
	 * @param columnIndex	The index of the column for which details are to be retrieved.
	 * @return	The details of the column at the specified index.
	 */
	private WTableColumn getColumn(int columnIndex)
	{
		try
		{
			return m_tableColumns.get(columnIndex);
		}
		catch (IndexOutOfBoundsException exception)
		{
			throw new IllegalArgumentException("There is no WTableColumn at column "
                    + columnIndex);
		}
	}
	
	/**
	 * Get index of the specifed <code>column name</code>
	 * @param columnName
	 * @return
	 */
	public int getIndexColumn(String columnName) {
		int index = -1;
		
		for (WTableColumn m_tableColumn: m_tableColumns) {
			if (m_tableColumn.getHeaderValue().equals(columnName)) {
				index = m_tableColumns.indexOf(m_tableColumn);
				break;
			}
		}
		
		return index;
	}
	
	private boolean isWithinListCell(Component source) {
		if (source instanceof Listcell)
			return true;
		Component c = source.getParent();
		while(c != null) {
			if (c instanceof Listcell)
				return true;
			c = c.getParent();
		}
		return false;
	}
	
	/**
	 * Fire the given table value change <code>event</code>.
	 *
	 * @param event	The event to pass to the listeners
	 */
	private void fireTableValueChange(TableValueChangeEvent event)
	{
	    for (TableValueChangeListener listener : m_listeners)
	    {
	       listener.tableValueChange(event);
	    }
	}
	
	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.event.EventListener#onEvent(org.zkoss.zk.ui.event.Event)
	 */
	public void onEvent(Event event) throws Exception
	{
		int col = -1;
		int row = -1;
		Object value = null;
		TableValueChangeEvent vcEvent = null;
		WTableColumn tableColumn;

		Component source = event.getTarget();

		if (isWithinListCell(source))
		{
			row = getRowPosition(source);
			col = getColumnPosition(source);

			tableColumn = m_tableColumns.get(col);

			if (source instanceof Checkbox)
			{
				value = Boolean.valueOf(((Checkbox)source).isChecked());
			}
			else if (source instanceof Button) 
			{
				value = "";
			}
			else if (source instanceof Decimalbox)
			{
				value = ((Decimalbox)source).getValue();
			}
			else if (source instanceof Datebox)
			{
				if (((Datebox)source).getValue() != null)
					value = new Timestamp(((Datebox)source).getValue().getTime());
			}
			else if (source instanceof Textbox)
			{
				value = ((Textbox)source).getValue();
			}
			else if (source instanceof org.adempiere.webui.component.Listbox) {
				value = ((org.adempiere.webui.component.Listbox)source).getSelectedItem().getValue();
			}

			if(value != null)
			{
				vcEvent = new TableValueChangeEvent(source,
						tableColumn.getHeaderValue().toString(),
						row, col,
						value, value);

				fireTableValueChange(vcEvent);
			}
		}
		else if (event.getTarget() instanceof WListbox && Events.ON_SELECT.equals(event.getName()))
		{
			WListbox table = (WListbox) event.getTarget();
			if (table.isCheckmark()) {
				int cnt = table.getRowCount();
				if (cnt == 0 || !(table.getValueAt(0, 0) instanceof IDColumn))
					return;

				//update IDColumn
				tableColumn = m_tableColumns.get(0);
				for (int i = 0; i < cnt; i++) {
					IDColumn idcolumn = (IDColumn) table.getValueAt(i, 0);
					Listitem item = table.getItemAtIndex(i);

					value = item.isSelected();
					Boolean old = idcolumn.isSelected();

					if (!old.equals(value)) {
						vcEvent = new TableValueChangeEvent(source,
								tableColumn.getHeaderValue().toString(),
								i, 0,
								old, value);

						fireTableValueChange(vcEvent);
					}
				}
			}
		}

		return;
	}
	
	@Override
	public void setColumnClass(int index, Class<?> classType) {
		if (index >= 0 && index < m_tableColumns.size())
		{
			m_tableColumns.get(index).setColumnClass(classType);
		}
	}
	
	/**
	 * Add data to fill a column, that is a List
	 * @param col Column that it will be affected
	 * @param data Data that be shown in the Column
	 */
	public void addColumnData(Integer col, List<KeyNamePair> pairs) {
		if (data2Fill == null) {
			data2Fill = new HashMap<Integer, List<KeyNamePair>>();
		}
		
		if (data2Fill.containsKey(col)) {
			data2Fill.remove(col);			
		}
		
		data2Fill.put(col, pairs);
	}
}
