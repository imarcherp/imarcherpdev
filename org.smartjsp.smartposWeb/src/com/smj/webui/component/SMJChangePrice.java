package com.smj.webui.component;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.factory.ButtonFactory;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Vbox;

import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.util.Message;

public class SMJChangePrice extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8851505685874562326L;
	private EventListener<Event> eventOk;
	private MSMJTmpWebSalesLine tmpWebSalesLine;

	public SMJChangePrice(int recordId, EventListener<Event> eventOk) {
		super();
		tmpWebSalesLine = new MSMJTmpWebSalesLine(Env.getCtx(), recordId, null);
		this.eventOk = eventOk;
		initComponents();
	}

	private void initComponents() {
		//CSS
		String labelStyle = "font-size: 24px;color: #0B610B;background:#FFFFFF;";
		String borderStyle = "border: 1px solid #C0C0C0; border-radius:5px;";

		//Window
		this.setWidth("250px");
		this.setHeight("200px");
		this.setStyle(borderStyle);

		mainLayout = new Vbox();
		mainLayout.setParent(this);
		mainLayout.setWidth("99%");
		mainLayout.setVflex("1");
		mainLayout.setHflex("1");	

		//North Layout			
		lblTitle.setStyle(labelStyle);
		hLayout = new Hbox();
		hLayout.setAlign("center");
		hLayout.setPack("center");
		hLayout.setHflex("1");
		hLayout.setStyle("padding:3px;");
		hLayout.appendChild(lblTitle);

		northLayout = new Window();		
		northLayout.appendChild(hLayout);	
		northLayout.setStyle("margin:2px;" + borderStyle);
		mainLayout.appendChild(northLayout);

		//Center Layout
		hLayout = new Hbox();
		hLayout.setHflex("1");
		hLayout.setVflex("1");
		hLayout.setAlign("center");
		hLayout.setPack("center");		
		hLayout.appendChild(nbxPrice);

		centerLayout = new Window();
		centerLayout.setVflex("1");
		centerLayout.appendChild(hLayout);		
		centerLayout.setStyle(borderStyle + "margin:0px 2px;");
		mainLayout.appendChild(centerLayout);

		//South Layout
		Panel pnlButtonRight = new Panel();
		pnlButtonRight.appendChild(btnOk);
		pnlButtonRight.appendChild(btnCancel);
		pnlButtonRight.setStyle("text-align:right; padding: 4px;");
		pnlButtonRight.setWidth("100%");
		pnlButtonRight.setHflex("1");

		southLayout = new Window();
		southLayout.appendChild(pnlButtonRight);
		southLayout.setStyle(borderStyle + "margin:2px;");
		mainLayout.appendChild(southLayout);

		//Add Data
		nbxPrice.setValue(tmpWebSalesLine.getsmj_discount());

		//Add Events
		btnOk.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				btnOkOnClick();
				eventOk.onEvent(event);
			}
		});

		btnCancel.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				btnCancelOnClick();			
			}
		});
	}
	
	private void btnOkOnClick() {
		if (nbxPrice.getValue() == null || nbxPrice.getValue().compareTo(Env.ZERO) < 1) {
			Message.showWarning("SMJMSGValueGreaterZero");
			return;
		}
		
		tmpWebSalesLine.setPriceEntered(nbxPrice.getValue());
		
		if (tmpWebSalesLine.save())
			this.dispose();		
	}
	
	private void btnCancelOnClick() {
		this.dispose();
	}

	private Vbox mainLayout = new Vbox();
	private Window northLayout;
	private Window centerLayout;
	private Window southLayout;

	private Label lblTitle = new Label(Msg.translate(Env.getCtx(), "SMJLabelPrice"));
	private Hbox hLayout;	
	private NumberBox nbxPrice = new NumberBox(false);

	private Button btnOk = ButtonFactory.createNamedButton(ConfirmPanel.A_OK);
	private Button btnCancel = ButtonFactory.createNamedButton(ConfirmPanel.A_CANCEL);
}
