package com.smj.webui.component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.Window;
import org.compiere.model.MOrder;
import org.compiere.process.DocAction;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Row;

import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.util.DataQueries;
import com.smj.util.Documents;
import com.smj.util.Message;
import com.smj.util.SMJConfig;

/**
 * Ventana para cancelar el envio de productos por la ventana SmartPos
 * Window to cancel the delivery of products by the smartPOS window
 * @author Dany
 *
 */
public class SMJSentCancel extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer orgId = Env.getAD_Org_ID(Env.getCtx());
	private Integer clientId = Env.getAD_Client_ID(Env.getCtx());
	private Trx trx = Trx.get(Trx.createTrxName("SMJSC" + Calendar.getInstance().getTimeInMillis()), true);

	private ArrayList<MSMJTmpWebSalesLine> lines;
	private MSMJTmpWebSalesLine line;
	private EventListener<Event> eventBtnOk;
	private Boolean cancel;
	private String ticket;
	private int m_Warehouse_ID;

	public SMJSentCancel(String ticket, int m_Warehouse_ID) {
		this.ticket = ticket;
		this.m_Warehouse_ID = m_Warehouse_ID;
		initComponents();
	}

	/**
	 * Definir los componentes de la interfaz con sus respectivos eventos
	 * Define the interface components with their respective events
	 */
	private void initComponents() {
		this.setWidth("400px");
		grid.setParent(this);
		grid.appendChild(rows);

		chkAffectInventory.setChecked(false);
		txtDescription.setEnabled(false);
		txtDescription.setRows(5);

		row = rows.newRow();
		Cell cellLbl = new Cell();
		cellLbl.setAlign("center");
		cellLbl.setColspan(2);
		cellLbl.appendChild(lblMessage);
		row.appendChild(cellLbl);

		row = rows.newRow();
		row.appendChild(lblAffectInventory.rightAlign());
		row.appendChild(chkAffectInventory);

		row = rows.newRow();
		row.appendChild(lblDescription.rightAlign());
		row.appendChild(txtDescription);

		row = rows.newRow();
		Cell cell = new Cell();
		cell.setAlign("right");
		cell.appendChild(btnOk);
		row.appendChild(cell);
		row.appendChild(btnCancel);

		//Add Events
		btnCancel.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnCancelarOnClick();
			}
		});

		chkAffectInventory.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (chkAffectInventory.isChecked()) {
					txtDescription.setEnabled(true);
				} else {
					txtDescription.setEnabled(false);
					txtDescription.setValue("");
				}
			}
		});

		btnOk.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnOkOnClick();
				eventBtnOk.onEvent(event);	
			}
		});
	}

	private void btnOkOnClick() throws Exception {		

		try {

			if (chkAffectInventory.isChecked()) {

				MOrder order = new MOrder(Env.getCtx(), 0, trx.getTrxName());
				order.setIsActive(true);
				order.setIsSOTrx(true);
				order.setClientOrg(clientId, orgId);
				order.setAD_Org_ID(orgId);
				order.setDocStatus(DocAction.STATUS_Drafted);
				order.setDocAction(DocAction.ACTION_Complete);
				int wareHouseOrderType = DataQueries.getDocumentTypeId("Warehouse Order", clientId);		
				order.setC_DocType_ID(wareHouseOrderType);
				order.setC_DocTypeTarget_ID(wareHouseOrderType);
				order.setM_Warehouse_ID(m_Warehouse_ID);
				order.setProcessing(false);
				order.setProcessed(true);
				order.setIsApproved(true);
				order.setIsCreditApproved(false);
				order.setIsDelivered(false);
				order.setIsInvoiced(false);
				order.setIsPrinted(false);
				order.setIsTransferred(false);
				order.setIsSelected(false);
				order.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
				Timestamp date = Env.getContextAsDate(Env.getCtx(), "#Date");
				order.setDateOrdered(date);
				order.setDatePromised(date);
				order.setDatePrinted(date);
				order.setDateAcct(date);
				order.setC_BPartner_ID(SMJConfig.getIntValue("C_BPartner_ID", 0, orgId));
				order.setC_BPartner_Location_ID(SMJConfig.getIntValue("C_BPartner_Location_ID", 0, orgId));
				String description = "Created By SmartPOS " + txtDescription.getValue() + " " + ticket;
				order.setDescription(description);
				order.setIsDiscountPrinted(false);
				order.setPaymentRule("P"); ///B
				order.setC_PaymentTerm_ID(SMJConfig.getIntValue("C_PaymentTerm_ID", 0, orgId));
				order.setC_ConversionType_ID(114);
				order.setC_Currency_ID(SMJConfig.getIntValue("C_Currency_ID", 0, orgId));
				order.setInvoiceRule("I"); //D
				order.setPriorityRule("5");
				order.setFreightAmt (Env.ZERO);
				order.setChargeAmt (Env.ZERO);
				order.setTotalLines (Env.ZERO);
				order.setGrandTotal (Env.ZERO);
				order.setPosted(false);
				order.setDeliveryRule("A");
				order.setFreightCostRule("I");
				order.setDeliveryViaRule("P");
				order.setIsTaxIncluded (false);
				order.setIsDropShip(false);
				order.setSendEMail (false);
				order.setIsSelfService(false);			

				if (order.save()) {
					if (cancel) {
						for (int i = 0; i < lines.size(); i++) {
							MSMJTmpWebSalesLine tmpLine = lines.get(i);	

							if (Documents.createOrderLine(order, tmpLine.getM_Product_ID(), tmpLine.getQtyOrdered(), tmpLine.getC_UOM_ID(), tmpLine.getPriceEntered(), "", 0, 0, order.getC_BPartner_ID(), 
									false, 0, date, null, null, null) < 1) {
								trx.rollback();
								return;
							}
						}
					} else {

						if (Documents.createOrderLine(order, line.getM_Product_ID(), line.getQtyOrdered(), line.getC_UOM_ID(), line.getPriceEntered(), "", 0, 0, order.getC_BPartner_ID(), 
								false, 0, date, null, null, null) < 1) {
							trx.rollback();
							return;
						}
					}

					order.completeIt();

					order.setDocAction(DocAction.ACTION_Close);
					order.setDocStatus(DocAction.STATUS_Completed);

					if (!order.save()) {
						trx.rollback();
						return;
					}
				} else {
					trx.rollback();
					return;
				}

				trx.commit();
				trx.close();
			}

			this.detach();
		} catch (Exception ex) {
			Message.showError(ex.getMessage(), false);
			trx.rollback();
		} finally {
			trx.close();
		}
	}

	private void btnCancelarOnClick() {
		trx.close();
		this.detach();
	}

	public void setBtnOkOnClick(EventListener<Event> event) {
		eventBtnOk = event;
	}

	public void setLines(ArrayList<MSMJTmpWebSalesLine> lines) {
		cancel = true;
		this.lines = lines;
	}

	public void setLine(MSMJTmpWebSalesLine line) {
		cancel = false;
		this.line = line;
	}

	public Boolean isCancel() {
		return cancel;
	}

	//Defining Components
	private final Grid grid = GridFactory.newGridLayout();
	private final Rows rows = new Rows();
	private Row row;

	private final Label lblAffectInventory = new Label("Affect Inventory");
	private final Label lblDescription = new Label("Description");
	private final Label lblMessage = new Label (Msg.translate(Env.getCtx(), "SMJ-LabelSentCancel"));

	private final Checkbox chkAffectInventory = new Checkbox();
	private final Textbox txtDescription = new Textbox();

	private Button btnOk = new Button("OK");
	private Button btnCancel = new Button("Cancel");
}
