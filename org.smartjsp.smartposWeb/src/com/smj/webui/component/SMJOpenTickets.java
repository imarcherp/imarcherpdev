package com.smj.webui.component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Vbox;

import com.smj.util.SMJConfig;

/**
 * Ventana para poder ver los tickets abiertos y poder seleccionarlo o puede crear
 * uno nuevo.
 * Window to see open tickets and to select or create a new one
 * @author Dany
 *
 */
public class SMJOpenTickets extends Window {

	private static final long serialVersionUID = 3168651304490908146L;
	public static CLogger log = CLogger.getCLogger(SMJOpenTickets.class);
	private Integer currentTicket = -1;
	private EventListener<Event> searchTicketsOnEvent;
	private Timer timer;

	public SMJOpenTickets(EventListener<Event> searchTicketsOnEvent) {
		super();
		this.searchTicketsOnEvent = searchTicketsOnEvent;
		initComponents();
		loadTable();
		runTimer();
	}
	
	/**
	 * Definir los componentes de la interfaz con sus respectivos eventos
	 * Define the interface components with their respective events
	 */
	private void initComponents() {
		String greenButtonStyle = "border-color: #15cb3a #00991f #00841c;background: linear-gradient(#00c328,#00ab23);background: -webkit-linear-gradient(#00c328,#00ab23);background-color: #00bb23;color: #fff;text-shadow: 0 -1px rgba(0,0,0,.15);";
		String blueButtonStyle = "border-color: #1534cb #001599 #001184;background: linear-gradient(#003cc3,#003cab);background: -webkit-linear-gradient(#003cc3,#003cab);background-color: #001dbb;color: #fff;text-shadow: 0 -1px rgba(0,0,0,.15);";
		String labelStyle = "font-size: 24px;color: #0B610B;background:#FFFFFF;";
		
		//this.setHflex("true");
		//this.setVflex("true");		
		//this.setHeight("100%");
		//this.setWidth("100%");
		ZKUpdateUtil.setVflex(this, "true");
		ZKUpdateUtil.setHflex(this, "true");
		ZKUpdateUtil.setWidth(this, "100%");
		ZKUpdateUtil.setHeight(this, "100%");
		
		
		//mainLayout.setHflex("true");
		//mainLayout.setVflex("true");
		ZKUpdateUtil.setVflex(mainLayout, "true");
		ZKUpdateUtil.setHflex(mainLayout, "true");
		mainLayout.setParent(this);
		
		//contentLayout.setHeight("100%");
		//contentLayout.setWidth("100%");
		//contentLayout.setHflex("true");
		//contentLayout.setVflex("true");
		ZKUpdateUtil.setWidth(contentLayout, "100%");
		ZKUpdateUtil.setHeight(contentLayout, "100%");
		ZKUpdateUtil.setVflex(contentLayout, "true");
		ZKUpdateUtil.setHflex(contentLayout, "true");
		mainLayout.appendChild(contentLayout);		
		
		lblTitle.setText(Msg.translate(Env.getCtx(), "SMJ_LabelOpenTickets") + " - " + getWarehouseName());
		lblTitle.setStyle(labelStyle);
		Cell cell = new Cell();
		cell.appendChild(lblTitle);
		cell.setAlign("center");
		cell.setValign("middle");
		cell.setStyle("padding:2px;");				
		contentLayout.appendChild(cell);
		
		//tblTickets.setHflex("true");
		//tblTickets.setVflex("true");
		//tblTickets.setHeight("100%");
		ZKUpdateUtil.setVflex(tblTickets, "true");
		ZKUpdateUtil.setHflex(tblTickets, "true");
		ZKUpdateUtil.setWidth(tblTickets, "100%");
		
		//middleLayout.setHeight("76%");
		//middleLayout.setWidth("100%");
		//middleLayout.setHflex("true");
		//middleLayout.setVflex("true");
		ZKUpdateUtil.setWidth(middleLayout, "100%");
		ZKUpdateUtil.setHeight(middleLayout, "76%");
		ZKUpdateUtil.setVflex(middleLayout, "true");
		ZKUpdateUtil.setHflex(middleLayout, "true");
		middleLayout.appendChild(tblTickets);	
		middleLayout.setStyle("padding:2px;");
		contentLayout.appendChild(middleLayout);
		
		//buttonsLayout.setHeight("100%");
		//buttonsLayout.setWidth("100%");
		//buttonsLayout.setHflex("true");
		//buttonsLayout.setVflex("true");
		ZKUpdateUtil.setWidth(buttonsLayout, "100%");
		ZKUpdateUtil.setHeight(buttonsLayout, "100%");
		ZKUpdateUtil.setVflex(buttonsLayout, "true");
		ZKUpdateUtil.setHflex(buttonsLayout, "true");
		bottomLayout.appendChild(buttonsLayout);
		//bottomLayout.setHeight("12%");	
		ZKUpdateUtil.setHeight(bottomLayout, "12%");
		contentLayout.appendChild(bottomLayout);
		
		btnSelect.setEnabled(false);
		//btnSelect.setWidth("100px");
		ZKUpdateUtil.setWidth(btnSelect, "100px");
		btnSelect.setStyle(blueButtonStyle);
		cell = new Cell();
		cell.appendChild(btnSelect);
		cell.setAlign("right");
		cell.setValign("middle");
		cell.setWidth("50%");
		cell.setStyle("padding:3px;");
		buttonsLayout.appendChild(cell);		
		
		//btnNew.setWidth("100px");
		ZKUpdateUtil.setWidth(btnNew, "100px");
		btnNew.setStyle(greenButtonStyle);
		cell = new Cell();
		cell.appendChild(btnNew);
		cell.setAlign("left");
		cell.setValign("middle");
		cell.setWidth("50%");
		cell.setStyle("padding:3px;");
		buttonsLayout.appendChild(cell);

		tblTickets.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				tblTicketsOnEvent();
			}
		});
		
		btnNew.addActionListener(new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				currentTicket = -1;
				searchTicketsOnEvent.onEvent(event);
			}
		});
		
		EventListener<Event> openTicketsOnEvent = new EventListener<Event>() {			
			@Override
			public void onEvent(Event event) throws Exception {
				if (timer != null && timer.isRunning()) 
					timer.stop();
				
				searchTicketsOnEvent.onEvent(event);;
			}
		};
		
		tblTickets.addEventListener(Events.ON_DOUBLE_CLICK, openTicketsOnEvent);
		btnSelect.addEventListener(Events.ON_CLICK, openTicketsOnEvent);
	}
	
	private void runTimer() {
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		Integer delay = SMJConfig.getIntValue("POS_CheckOpenOrdersTime", 3000, AD_Org_ID);

		if (timer != null && timer.isRunning())
			timer.stop();

		timer = new Timer(delay);
		timer.setRepeats(true);
		timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				loadTable();
			}
		});
		this.appendChild(timer);
		timer.start();
	}

	/**
	 * Evento que se ejecuta cuando se selecciona un ticket en la tabla
	 * Event that runs when a ticket is selected in the table
	 */
	private void tblTicketsOnEvent() {
		if (!btnSelect.isEnabled())
			btnSelect.setEnabled(true);
		
		if (tblTickets.getSelectedIndex() < 0) {
			btnSelect.setEnabled(false);
			currentTicket = -1;
			return;
		}
		
		currentTicket = Integer.parseInt(tblTickets.getValueAt(tblTickets.getSelectedIndex(), 0).toString());
	}
	
	private String getWarehouseName() {
		int M_Warehouse_ID = Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"));
		MWarehouse warehouse = new MWarehouse(Env.getCtx(), M_Warehouse_ID, null);
		return warehouse.getName();
	}
	
	/**
	 * Carga los datos en la tabla
	 * Loads the data into the table
	 */
	private void loadTable() {
		tblTickets.clear();
		ArrayList<ArrayList<Object>> data = getData();

		if (data.isEmpty()) 
			return;

		ListModelTable modelTable = new ListModelTable(data);
		tblTickets.setData(modelTable, getTableColumnNames());
				
		for (Component component : tblTickets.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}

	/**
	 * Obtiene la informaci�n de los tickets que se encuentran abiertos
	 * Gets the information of tickets that are open
	 * @return
	 */
	private ArrayList<ArrayList<Object>> getData() {
		Integer clientId = Env.getAD_Client_ID(Env.getCtx());
		Integer orgId = Env.getAD_Org_ID(Env.getCtx());
		String m_warehouse_id = Env.getCtx().getProperty("#M_Warehouse_ID");
		
		//Delete Ticket and its lines Where the Orden doesn't exists.
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM SMJ_TmpWebSalesLine WHERE SMJ_TmpWebSales_ID IN ");
		sql.append("(SELECT SMJ_TmpWebSales_ID FROM SMJ_TmpWebSales t WHERE NOT EXISTS ");
		sql.append("(SELECT C_Order_ID FROM C_Order WHERE C_Order_ID = t.C_Order_ID))");
		
		DB.executeUpdateEx(sql.toString(), null);
		
		sql = new StringBuffer();
		sql.append("DELETE FROM SMJ_TmpWebSales t WHERE NOT EXISTS ");
		sql.append("(SELECT C_Order_ID FROM C_Order WHERE C_Order_ID = t.C_Order_ID)");
		
		DB.executeUpdateEx(sql.toString(), null);
		
		//Delete Tickets and its lines where the order status is completed!
		sql = new StringBuffer();
		sql.append("DELETE FROM SMJ_TmpWebSalesLine ");
		sql.append("WHERE SMJ_TmpWebSales_ID IN ");
		sql.append("(SELECT s.SMJ_TmpWebSales_ID FROM SMJ_TmpWebSales s ");
		sql.append("JOIN C_Order o ON (o.C_Order_ID = s.C_Order_ID AND o.DocStatus IN ('CO', 'CL', 'VO')))");
		
		DB.executeUpdateEx(sql.toString(), null);
		
		sql = new StringBuffer();
		sql.append("DELETE FROM SMJ_TmpWebSales ");
		sql.append("WHERE SMJ_TmpWebSales_ID IN ");
		sql.append("(SELECT s.SMJ_TmpWebSales_ID FROM SMJ_TmpWebSales s ");
		sql.append("JOIN C_Order o ON (o.C_Order_ID = s.C_Order_ID AND o.DocStatus IN ('CO', 'CL', 'VO')))");
		
		DB.executeUpdateEx(sql.toString(), null);
		
		sql = new StringBuffer();
		sql.append("SELECT t.smj_tmpWebSales_ID, b.name, t.created, o.DatePromised, (SELECT CASE WHEN SUM(total) > 0 THEN SUM(total) ELSE 0 END FROM SMJ_TMPWebSalesLine ");
		sql.append("WHERE smj_tmpwebsales_id = t.smj_tmpwebsales_id) AS total "); 
		sql.append(", t.smj_plate ");
		sql.append("FROM smj_tmpWebSales t JOIN C_BPartner b ON (b.C_BPartner_ID = t.C_BPartner_ID) JOIN C_Order o ON (o.C_Order_ID = t.C_Order_ID) ");
		sql.append(" WHERE t.isActive = 'Y' AND t.AD_Client_ID = " + clientId);
		sql.append(" AND t.AD_Org_ID = " + orgId + " AND o.M_Warehouse_ID = " + m_warehouse_id);
		sql.append(" ORDER BY name ASC ");

		ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			Integer numDec = SMJConfig.getIntValue("POS_Decimals", 0, orgId);
			String[] localeVals = SMJConfig.getArrayStringValue("POS_Locale", ",", orgId);
			NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale(localeVals[0].trim(), localeVals[1].trim()));
			nf.setMaximumFractionDigits(numDec);
			nf.setMinimumFractionDigits(numDec);
			
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();			
			while (rs.next()) {
				ArrayList<Object> line = new ArrayList<Object>();

				line.add(rs.getString("smj_tmpWebSales_ID"));
				line.add(rs.getString("name"));
				line.add(rs.getTimestamp("DatePromised"));
				line.add(rs.getTimestamp("created"));				
				line.add(nf.format(rs.getBigDecimal("total")));
				line.add(rs.getString("smj_plate"));
				data.add(line);
			}// while
		} catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return data;

	}

	/**
	 * Asigna los nombres de las columnas
	 * Assigns the names of the columns
	 * @return
	 */
	private ArrayList<String> getTableColumnNames()	{
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "smj_tmpWebSales_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "customer"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJ_LabelScheduledDate"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelSalesDate"));  
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelTotal"));
		columnNames.add(Msg.translate(Env.getCtx(), "smj_plate"));
		return columnNames;
	}//getTableColumnNames
	
	public Integer getTicketSelected() {
		return currentTicket;
	}
	
	//Defining Components
	private Window mainLayout = new Window();
	private Window middleLayout = new Window();
	private Window bottomLayout = new Window();
	
	private Vbox contentLayout = new Vbox();
	private Hbox buttonsLayout = new Hbox();

	private Label lblTitle = new Label();
	private WListbox tblTickets = ListboxFactory.newDataTable();
	private Button btnSelect = new Button(Msg.translate(Env.getCtx(), "select"));
	private Button btnNew = new Button(Msg.translate(Env.getCtx(), "SMJ_LabelNew"));
}
