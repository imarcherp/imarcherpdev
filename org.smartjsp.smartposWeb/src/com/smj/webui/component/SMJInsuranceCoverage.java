package com.smj.webui.component;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Vbox;

import com.smj.util.SMJConfig;
import com.smj.util.Util;

/**
 * @version <li>SmartJSP: SMJInsuranceCoverage
 *          <ul TYPE ="circle">
 *          <li>Ventana modal para muestra de liquidacion de impuesto
 *          <li>Modal Window for payment sales
 *          </ul>
 * @author Pedro Rozo - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInsuranceCoverage extends Window implements EventListener<Event>{
	private static final long serialVersionUID = -82379818861797771L;
	
	private static Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private static Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());	
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	
	private Label   insuranceCLabel = new Label();
	private Label   planLabel = new Label();
	private Label   copaypLabel = new Label();
	private Label   copaymLabel = new Label();
	private Label   totalbLabel = new Label();
	private Label   coveredLabel = new Label();
	private Label   totalaLabel = new Label();
	
	private Label   insuranceCbox = new Label();
	private Label   insurancePlanbox = new Label();
	private Decimalbox copaypDecimalbox = new Decimalbox(Env.ZERO);
	private Decimalbox copaymDecimalbox = new Decimalbox(Env.ZERO);
	private Decimalbox totalbDecimalbox = new Decimalbox(Env.ZERO);
	private Decimalbox coveredDecimalbox = new Decimalbox(Env.ZERO);
	private Decimalbox totalaDecimalbox = new Decimalbox(Env.ZERO);
	private Grid grid = GridFactory.newGridLayout();
	private Rows rows = new Rows();
	
	private NumberFormat nf;
	
	private int retorno = 0;
	private Vbox vLayout = new Vbox();
	
	
	public static CLogger log = CLogger.getCLogger(SMJInsuranceCoverage.class);
	  private Callback<Integer> m_callback;
	/**
	 * SMJInsuranceCoverage
	 * @param WindowNo
	 * @param partnerId
	 * @param partnerName
	 * @param wParent
	 */
	
	public SMJInsuranceCoverage(Integer partnerId, Callback<Integer> callback) {
		super();
		this.m_callback = callback;
		//this.wParent = wParent; 
		
		init();
	}//constructor

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		Integer numDec = SMJConfig.getIntValue("POS_Decimals", 0, lorgId);
		String[] localeVals = SMJConfig.getArrayStringValue("POS_Locale", ",", lorgId);
		nf = NumberFormat.getCurrencyInstance(new Locale(localeVals[0].trim(), localeVals[1].trim()));
		nf.setMaximumFractionDigits(numDec);
		nf.setMinimumFractionDigits(numDec);
		String decimalFormat = Util.getNumberFormatAsString(numDec);
		
		this.setBorder("normal");
		this.setWidth("420px");
		this.setHeight("300px");
		this.appendChild(vLayout);

		//insuranceCLabel.setText("texto de prueba");
		insuranceCLabel.setWidth("100px");
		vLayout.appendChild(insuranceCLabel.rightAlign());
		confirmPanel.addActionListener(this);
		vLayout.appendChild(grid);
		vLayout.appendChild(confirmPanel);
		
		insuranceCLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelInsuranceC"));
		planLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelPlan"));;
		copaypLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelCopayp"));;
		copaymLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelCopaym"));
		totalbLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelTotalb"));
		coveredLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelCovered"));
		totalaLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelTotala"));

		grid.appendChild(rows);
		this.setTitle(Msg.translate(Env.getCtx(), "SMJLabelCovered"));
		
		Row row = rows.newRow();
		rows.appendChild(row);
		insuranceCLabel.setWidth("100px");
		row.appendChild(insuranceCLabel.rightAlign());
		insuranceCbox.setWidth("150px");
		insuranceCbox.setText(nf.format(0));
		row.appendChild(insuranceCbox);
		
		row = rows.newRow();
		planLabel.setWidth("100px");
		row.appendChild(planLabel.rightAlign());
		insurancePlanbox.setWidth("150px");
		insurancePlanbox.setText(nf.format(0));
		row.appendChild(insurancePlanbox);
		
		row = rows.newRow();
		copaypLabel.setWidth("100px");
		row.appendChild(copaypLabel.rightAlign());
		copaypDecimalbox.setWidth("150px");
		copaypDecimalbox.setReadonly(true);
		copaypDecimalbox.setValue(new BigDecimal(10));
		copaypDecimalbox.setFormat(decimalFormat);
		row.appendChild(copaypDecimalbox);
		
		row = rows.newRow();
		copaymLabel.setWidth("100px");
		row.appendChild(copaymLabel.rightAlign());
		copaymDecimalbox.setWidth("150px");
		copaymDecimalbox.setReadonly(true);
		copaypDecimalbox.setValue(new BigDecimal(10));
		copaymDecimalbox.setFormat(decimalFormat);
		row.appendChild(copaymDecimalbox);

		row = rows.newRow();
		totalbLabel.setWidth("100px");
		row.appendChild(totalbLabel.rightAlign());
		totalbDecimalbox.setWidth("150px");
		totalbDecimalbox.setReadonly(true);
		copaypDecimalbox.setValue(new BigDecimal(10));
		totalbDecimalbox.setFormat(decimalFormat);
		row.appendChild(totalbDecimalbox);
		
		row = rows.newRow();
		coveredLabel.setWidth("100px");
		row.appendChild(coveredLabel.rightAlign());
		coveredDecimalbox.setWidth("150px");
		coveredDecimalbox.setReadonly(true);
		coveredDecimalbox.setFormat(decimalFormat);
		copaypDecimalbox.setValue(new BigDecimal(10));
		row.appendChild(coveredDecimalbox);
		
		row = rows.newRow();
		totalaLabel.setWidth("100px");
		row.appendChild(totalaLabel.rightAlign());
		totalaDecimalbox.setWidth("150px");
		totalaDecimalbox.setReadonly(true);
		totalaDecimalbox.setFormat(decimalFormat);
		copaypDecimalbox.setValue(new BigDecimal(10));
		row.appendChild(totalaDecimalbox);
		
		
	}//init
	
	
	@Override
	public void onEvent(Event ev) {
		try {
			if (ev.getTarget().getId().equals(ConfirmPanel.A_CANCEL))
	        {
				retorno = 2;
                dispose();
	        }//if cancel
			else if(ev.getTarget().getId().equals(ConfirmPanel.A_OK)){
				retorno = 1;
				dispose();
			}//if OK
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".onEvent ERROR:: ", e);
		}
	}//onEvent
	
	public void postQueryEvent() 
    {
		Clients.showBusy(Msg.getMsg(Env.getCtx(), "Processing"));
    	Events.echoEvent("onExecuteQuery", this, null);
    }
	
	@Override
	public void onPageDetached(Page page) {
		super.onPageDetached(page);
		if (m_callback != null) {
			m_callback.onCallback(retorno);
		}
	}
	
	public void dispose()
	{
		this.detach();
	}	//	dispose  

	public void setInsuranceCbox(String insuranceCbox) {
		this.insuranceCbox.setText(insuranceCbox);
	}

	public void setInsurancePlanbox(String insurancePlanbox) {
		this.insurancePlanbox.setText(insurancePlanbox);
	}

	public void setCopaypDecimalbox(BigDecimal copaypDecimalbox) {
		this.copaypDecimalbox.setValue(copaypDecimalbox);
	}

	public void setCopaymDecimalbox(BigDecimal copaymDecimalbox) {
		this.copaymDecimalbox.setValue(copaymDecimalbox);
	}

	public void setTotalbDecimalbox(BigDecimal totalbDecimalbox) {
		this.totalbDecimalbox.setValue(totalbDecimalbox);
	}

	public void setCoveredDecimalbox(BigDecimal coveredDecimalbox) {
		this.coveredDecimalbox.setValue(coveredDecimalbox);
	}

	public void setTotalaDecimalbox(BigDecimal totalaDecimalbox) {
		this.totalaDecimalbox.setValue(totalaDecimalbox);
	}


	
}

