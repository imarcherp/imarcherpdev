package com.smj.webui.component;

import java.util.List;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.factory.ButtonFactory;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MProduct;
import org.compiere.model.Query;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Vbox;

import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.util.Message;

/**
 * 
 * @author Dany - SmartJSP
 *
 */
public class SMJDiscountWindow extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6371006067196508456L;
	private Trx trx = Trx.get(Trx.createTrxName(), true);
	private EventListener<Event> eventLoadTable;
	private MSMJTmpWebSalesLine tmpWebSalesLine;

	public SMJDiscountWindow(MSMJTmpWebSalesLine tmpWebSalesLine, EventListener<Event> eventLoadTable) {
		super();
		this.tmpWebSalesLine = tmpWebSalesLine;
		this.eventLoadTable = eventLoadTable;
		initComponents();
	}

	private void initComponents() {		
		LayoutUtils.addSclass("adtab-content", this);

		//CSS
		String labelStyle = "font-size: 24px;color: #0B610B;background:#FFFFFF;";
		String borderStyle = "border: 1px solid #C0C0C0; border-radius:5px;";

		//Window
		this.setWidth("250px");
		this.setHeight("200px");
		this.setStyle(borderStyle);

		//Main Layout
		mainLayout = new Vbox();
		mainLayout.setParent(this);
		mainLayout.setWidth("99%");
		//mainLayout.setVflex("1");
		//mainLayout.setHflex("1");
		ZKUpdateUtil.setWidth(mainLayout, "1");
		ZKUpdateUtil.setHflex(mainLayout, "min");

		//North Layout			
		lblTitle.setStyle(labelStyle);
		hLayout = new Hbox();
		hLayout.setAlign("center");
		hLayout.setPack("center");
		hLayout.setHflex("1");
		hLayout.setStyle("padding:3px;");		
		hLayout.appendChild(lblTitle);

		northLayout = new Window();		
		northLayout.appendChild(hLayout);	
		northLayout.setStyle("margin:2px;" + borderStyle);
		mainLayout.appendChild(northLayout);

		//Center Layout
		vLayout = new Vbox();
		vLayout.setHflex("1");
		vLayout.setVflex("1");
		vLayout.setAlign("center");
		vLayout.setPack("center");

		hLayout = new Hbox();
		hLayout.setHflex("1");
		hLayout.setAlign("center");
		hLayout.setPack("center");
		vLayout.appendChild(hLayout);

		cell = new Cell();
		cell.setAlign("right");
		cell.setHflex("1");
		cell.appendChild(lblValue);
		hLayout.appendChild(cell);	

		cell = new Cell();
		cell.setAlign("left");
		cell.setHflex("1");
		cell.appendChild(txtValue);
		hLayout.appendChild(cell);

		hLayout = new Hbox();
		hLayout.setHflex("1");
		hLayout.setAlign("center");
		hLayout.setPack("center");		
		vLayout.appendChild(hLayout);

		cell = new Cell();
		cell.setAlign("right");
		cell.setHflex("1");
		cell.appendChild(lblPorcentage);
		hLayout.appendChild(cell);	

		cell = new Cell();
		cell.setAlign("left");
		cell.setHflex("1");
		cell.appendChild(chbPorcentage);
		hLayout.appendChild(cell);

		hLayout = new Hbox();
		hLayout.setHflex("1");
		hLayout.setAlign("center");
		hLayout.setPack("center");		
		vLayout.appendChild(hLayout);

		cell = new Cell();
		cell.setAlign("right");
		cell.setHflex("1");
		cell.appendChild(lblAllLines);
		hLayout.appendChild(cell);

		cell = new Cell();
		cell.setAlign("left");
		cell.setHflex("1");
		cell.appendChild(chbAllLines);
		hLayout.appendChild(cell);

		centerLayout = new Window();
		centerLayout.setVflex("1");
		centerLayout.appendChild(vLayout);		
		centerLayout.setStyle(borderStyle + "margin:0px 2px;");
		mainLayout.appendChild(centerLayout);

		//South Layout
		Panel pnlButtonRight = new Panel();
		pnlButtonRight.appendChild(btnOk);
		pnlButtonRight.appendChild(btnCancel);
		pnlButtonRight.setStyle("text-align:right; padding: 4px;");
		//pnlButtonRight.setWidth("100%");
		//pnlButtonRight.setHflex("1");
		ZKUpdateUtil.setWidth(pnlButtonRight, "10%");
		ZKUpdateUtil.setHflex(pnlButtonRight, "1");

		southLayout = new Window();
		southLayout.appendChild(pnlButtonRight);
		southLayout.setStyle(borderStyle + "margin:2px;");
		mainLayout.appendChild(southLayout);

		//Add Data
		txtValue.setValue(tmpWebSalesLine.getsmj_discount());
		chbPorcentage.setChecked(tmpWebSalesLine.issmj_percentage());

		//Add Events
		btnOk.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				btnOkOnClick();
				eventLoadTable.onEvent(event);
			}
		});

		btnCancel.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				btnCancelOnClick();			
			}
		});
	}

	private void btnOkOnClick() {
		if (txtValue.getValue() == null || txtValue.getValue().compareTo(Env.ZERO) < 0) {
			Message.showWarning("SMJ_MSGEnterValidValue");
			return;
		} else if ((chbPorcentage.isChecked() && txtValue.getValue().compareTo(Env.ONEHUNDRED) == 1)) {
			Message.showWarning("SMJ_MSGPercentageInvalid");
			return;
		}

		if (chbAllLines.isChecked()) {
			addDiscountAllLines();
		} else {
			addDiscountLine();
		}
	}

	private void btnCancelOnClick() {
		this.dispose();
	}

	/**
	 * Adds discount to the line selected
	 */
	private void addDiscountLine() {
		MSMJTmpWebSalesLine line = new MSMJTmpWebSalesLine(Env.getCtx(), tmpWebSalesLine.getsmj_tmpwebsalesline_ID(), trx.getTrxName());
		MProduct product = new MProduct(Env.getCtx(), line.getM_Product_ID(), null);

		try {
			if (product.get_ValueAsBoolean("smjdiscount_allowed")) {
				if (!chbPorcentage.isChecked() && line.getPriceEntered().compareTo(txtValue.getValue()) == -1) {
					Message.showWarning("<h3>" + Msg.translate(Env.getCtx(), "SMJ_InvalidDiscountAmt") + "</h3>- " + line.getProductName(), false);
					return;
				}
				line.setsmj_percentage(chbPorcentage.isChecked());
				line.setsmj_discount(txtValue.getValue());		
				
				if (line.save()) {
					trx.commit();				
					this.dispose();
				} else {
					trx.rollback();
				}

			} else {
				Message.showWarning("<h3>" + Msg.translate(Env.getCtx(), "SMJ_MSGDisabledDiscount") + "</h3>- " + line.getProductName(), false);
				this.dispose();
			}
		} finally {			
			trx.close();
		}
	}

	/**
	 * Adds discount to all lines
	 */
	private void addDiscountAllLines() {
		String whereClause = MSMJTmpWebSalesLine.COLUMNNAME_smj_tmpwebsales_ID + " = " + tmpWebSalesLine.getsmj_tmpwebsales_ID();
		List<MSMJTmpWebSalesLine> lines = new Query(Env.getCtx(), MSMJTmpWebSalesLine.Table_Name, whereClause, trx.getTrxName()).list();
		String msg = "<h3>" + Msg.translate(Env.getCtx(), "SMJ_InvalidDiscountAmt") + "</h3>";
		String msgDiscount = "<h3>" + Msg.translate(Env.getCtx(), "SMJ_MSGDisabledDiscount") + "</h3>";

		try {
			boolean showMsg = false;
			boolean showMsgDiscount = false;
			boolean isOk = true;			
			for (MSMJTmpWebSalesLine line: lines) {
				MProduct product = new MProduct(Env.getCtx(), line.getM_Product_ID(), null);

				if (!product.get_ValueAsBoolean("smjdiscount_allowed")) {
					if (showMsgDiscount)
						msgDiscount += "<br>";

					msgDiscount += "- " + line.getProductName();
					showMsgDiscount = true;
					continue;
				}

				if (!chbPorcentage.isChecked() && line.getPriceEntered().compareTo(txtValue.getValue()) == -1) {
					if (showMsg)
						msg += "<br>";

					msg += "- " + line.getProductName();
					showMsg = true;
					continue;
				}

				line.setsmj_percentage(chbPorcentage.isChecked());
				line.setsmj_discount(txtValue.getValue());					

				if (!line.save()) {
					isOk = false;
					trx.rollback();
					break;
				}
			}

			if (isOk) {
				if (showMsg)
					Message.showWarning(msg, false);

				if (showMsgDiscount)
					Message.showWarning(msgDiscount, false);

				trx.commit();
				this.dispose();
			} 
		} finally {
			trx.close();
		}
	}

	private Vbox mainLayout;	
	private Window northLayout;
	private Window centerLayout;
	private Window southLayout;

	private Hbox hLayout;
	private Vbox vLayout;

	private Cell cell;

	private Label lblTitle = new Label(Msg.translate(Env.getCtx(), "SMJLabelDiscounts"));	
	private Label lblPorcentage = new Label(Msg.translate(Env.getCtx(), "SMJLabelPercentage"));
	private Label lblAllLines = new Label(Msg.translate(Env.getCtx(), "SMJLabelGlobal"));
	private Label lblValue = new Label(Msg.translate(Env.getCtx(), "SMJLabelValue"));
	private Checkbox chbPorcentage = new Checkbox();	
	private Checkbox chbAllLines = new Checkbox();
	private NumberBox txtValue = new NumberBox(false);

	private Button btnOk = ButtonFactory.createNamedButton(ConfirmPanel.A_OK);
	private Button btnCancel = ButtonFactory.createNamedButton(ConfirmPanel.A_CANCEL);
}
