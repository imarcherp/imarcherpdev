package com.smj.webui.component;

import java.util.ArrayList;
import java.util.HashMap;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Row;

public class SMJChangeUnit extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<HashMap<String, Object>> uomList;
	private HashMap<String, Object> currentUOM;
	private EventListener<Event> eventBtnOk;

	public SMJChangeUnit(ArrayList<HashMap<String, Object>> uomList, EventListener<Event> eventBtnOk) {
		super();
		this.uomList = uomList;
		this.eventBtnOk = eventBtnOk;
		initComponents();
	}
	
	private void initComponents() {
		this.setWidth("300px");
		grid.setParent(this);
		
		cmbUnit.setMold("select");
		row = rows.newRow();
		row.appendChild(newCell("center", 2, lblMessage));
		
		row = rows.newRow();
		row.appendChild(newCell("center", 2, cmbUnit));
		
		row = rows.newRow();
		row.appendChild(newCell("center", 1, btnOk));
		row.appendChild(newCell("center", 1, btnCancel));
		
		fillUOMList();
		
		//Add Events
		cmbUnit.addActionListener(new EventListener<Event>() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				cmbUnitOnCange();
			}
		});
		
		btnOk.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				close();
				eventBtnOk.onEvent(event);
			}
		});
		
		btnCancel.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				close();
			}
		});		
	}
	
	private void fillUOMList() {
		
		cmbUnit.removeAllItems();	
			
		for (int i = 0; i < uomList.size(); i++) {
			HashMap<String, Object> hashMap = uomList.get(i);
			cmbUnit.appendItem((String) hashMap.get("name"), hashMap);
		}
		
		cmbUnit.setSelectedIndex(0);
		currentUOM = cmbUnit.getSelectedItem().getValue();
	}
	
	private void cmbUnitOnCange() {
		currentUOM = cmbUnit.getSelectedItem().getValue();
	}
	
	/**
	 * Cerrar Ventana
	 */
	private void close() {
		this.detach();
	}
	
	/**
	 * Traducir un Mensaje (Message)...
	 * @param message
	 * @return
	 */
	private String translate(String message) {
		return Msg.translate(Env.getCtx(), message);
	}
	
	/**
	 * Crear un cell con las propiedades (align, colspan) y se le agregara un
	 * Component (Component)...
	 * @param align
	 * @param colspan
	 * @param component
	 * @return
	 */
	private Cell newCell(String align, int colspan, Component component) {
		Cell cell = new Cell();
		cell.setAlign(align);
		cell.setColspan(colspan);
		cell.appendChild(component);
		
		return cell;
	}
	
	public HashMap<String, Object> getCurrentUOM() {
		return currentUOM;
	}

	private final Grid grid = GridFactory.newGridLayout();
	private final Rows rows = grid.newRows();
	private Row row;
	
	private	Label lblMessage = new Label(translate("SMJ_MSGChangeUnit"));
	private Listbox cmbUnit = new Listbox();
	
	private final Button btnOk = new Button("OK");
	private final Button btnCancel = new Button("Cancel");
}
