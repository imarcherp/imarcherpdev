package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.form.Allocation;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.smj.model.InfoProductList;
import com.smj.util.DataQueries;

public class SMJSearchProducts {

	/** Logger */
	public static CLogger	log					= CLogger.getCLogger(Allocation.class);
	private Integer			tiresCategory		= Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer			principalWarehouse	= Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE", Env.getAD_Client_ID(Env.getCtx())).trim());
	private String			noPos				= MSysConfig.getValue("SMJ-PCATPROD_NOPOS", Env.getAD_Client_ID(Env.getCtx())).trim();
	private String			admService			= MSysConfig.getValue("SMJ_PCATPROD_ADMSERVICE", "", Env.getAD_Client_ID(Env.getCtx())).trim();

	/**
	 * llena la lista de productos - fill product list
	 * 
	 * @param val
	 * @param service
	 * @return LinkedList<InfoProductList>
	 */
	/**
	 * public LinkedList<InfoProductList> searchProducts(String val, Boolean service, String temparioName){
	 * LinkedList<InfoProductList> list = new LinkedList<InfoProductList>(); Integer principalWarehouse =
	 * Integer.parseInt(MSysConfig.getValue("SMJ-PRINCIPALWAREHOUSE",Env.getAD_Client_ID(Env.getCtx())).trim()); //por
	 * codigo nuevo list = getProductList(val, true, false, false, false,principalWarehouse, service); //por codigo
	 * antiguo if(list == null || list.size() <= 0){ list = getProductList(val,false, true, false,
	 * false,principalWarehouse, service); } //por nombre if(list == null || list.size() <= 0){ list =
	 * getProductList(val, false, false, true, false,principalWarehouse, service); } //por codigo de proveedor if(list
	 * == null || list.size() <= 0){ list = getProductList(val, false, false, false, true,principalWarehouse, service);
	 * } list = fillPrice(list, temparioName); if (list == null){ list = new LinkedList<InfoProductList>(); } return
	 * list; }//searchProducts
	 */

	public LinkedList<InfoProductList> searchProducts_Locator(String val, String loc, Boolean service, String temparioName) {
		LinkedList<InfoProductList> list = new LinkedList<InfoProductList>();

		// por codigo nuevo
		list = getProductList_Locator(val, loc, true, false, false, false, principalWarehouse, service);
		// por codigo antiguo
		if (list == null || list.size() <= 0) {
			list = getProductList_Locator(val, loc, false, true, false, false, principalWarehouse, service);
		}
		// por nombre
		if (list == null || list.size() <= 0) {
			list = getProductList_Locator(val, loc, false, false, true, false, principalWarehouse, service);
		}
		// por codigo de proveedor
		if (list == null || list.size() <= 0) {
			list = getProductList_Locator(val, loc, false, false, false, true, principalWarehouse, service);
		}
		list = fillPrice(list, temparioName);
		if (list == null) {
			list = new LinkedList<InfoProductList>();
		}
		return list;
	}// searchProducts

	/**
	 * llena el precio del producto - fill product price
	 */
	private LinkedList<InfoProductList> fillPrice(LinkedList<InfoProductList> list, String temparioName) {
		LinkedList<InfoProductList> listx = new LinkedList<InfoProductList>();

		if (list == null || list.size() <= 0)
			return null;

		Iterator<InfoProductList> it = list.iterator();
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();

		while (it.hasNext()) {
			try {
				InfoProductList ipl = it.next();
				Boolean service = false;
				Vector<Object> rc = new Vector<Object>();
				rc.add(ipl.getProductValue());
				rc.add(ipl.getProductName());
				rc.add(ipl.getWarehouseName());
				rc.add(ipl.getPartnerName());

				if (ipl.getWarehouseId() == 0) {
					service = true;
					rc.add("--");
				} else {
					rc.add(ipl.getQty());
				}

				MProduct p = new MProduct(Env.getCtx(), ipl.getProductID(), null);
				Integer owner = DataQueries.getwarehouseOwner(ipl.getWarehouseId());
				Integer partnerId = ipl.getPartnerId();
				MLocator locator = MLocator.get(Env.getCtx(), ipl.getLocatorId());
				MWarehouse warehouse = MWarehouse.get(Env.getCtx(), locator.getM_Warehouse_ID());
				if (partnerId == null || partnerId <= 0 && !warehouse.get_ValueAsBoolean("IsPOS")) {
					partnerId = owner;
				}

				HashMap<String, Integer> codes = DataQueries.getPListVersion(null, p, partnerId, tiresCategory, ipl.getLocatorId(), owner, temparioName);
				MProductPrice sp = null;
				MProductPrice pp = null;

				if (codes != null) {
					sp = MProductPrice.get(Env.getCtx(), codes.get("SALES"), ipl.getProductID(), null);
					pp = MProductPrice.get(Env.getCtx(), codes.get("PURCHASE"), ipl.getProductID(), null);
				}

				if (sp != null && sp.getPriceStd() != null) {
					ipl.setListVersionId(codes.get("SALES"));
					ipl.setPrice(sp.getPriceStd());
					rc.add(sp.getPriceStd());

					try {
						ipl.setEstimedTime((BigDecimal) sp.get_Value("smj_estimatedtime"));
						ipl.setIsAllowChange((Boolean) sp.get_Value("smj_isallowchanges"));
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName() + ".fillPrice ERROR::", e);
					}

					data.add(rc);

					if (pp != null && pp.getPriceStd() != null) {
						ipl.setPurchasePrice(pp.getPriceStd());
					}

					listx.add(ipl);
				} else if (service) {
					ipl.setPrice(Env.ZERO);
					rc.add("-");
					data.add(rc);
					listx.add(ipl);
				}

			} catch (Exception e) {
				log.log(Level.SEVERE, SMJInfoProduct.class.getName() + ".fillPrice :: ", e);
			}
		} // while

		return listx;
	}// fillPrice

	private LinkedList<InfoProductList> getProductList_Locator(String value, String LocSuc, Boolean newCode, Boolean oldCode, Boolean desc, Boolean vendorCode,
			Integer mainLocator, Boolean service) {
		LinkedList<InfoProductList> data = new LinkedList<InfoProductList>();
		StringBuffer sql = new StringBuffer();

		String column = "";

		// p (M_Product Table in the SQL sentence)
		if (newCode) {
			column = "p.Value";
		} else if (oldCode) {
			column = "p.SKU";
		} else if (desc) {
			column = "p.Name";
		} else {
			column = "bp.VendorProductNo";
		}

		
		sql.append("SELECT M_Product_ID, Value, ProductName, C_BPartner_ID, PartnerName, M_Locator_ID, M_Warehouse_ID, WarehouseName, SUM(Qty) AS Qty ")
		.append("FROM (SELECT ip.*, p.Name AS ProductName, p.Value FROM SMJ_InfoProductStorage('" + LocSuc + "', '%" + value + "%', '" + column + "', '"
				+ noPos + "') ip ")
		.append("JOIN M_Product p ON (p.M_Product_ID = ip.M_Product_ID) ").append("WHERE ip.AD_Client_ID = " + Env.getAD_Client_ID(Env.getCtx()) + " ")
		.append("AND ip.AD_Org_ID = " + Env.getAD_Org_ID(Env.getCtx()) + " ");
		
		/*// Get Principal Warehouse and its branches
		sql.append("SELECT M_Product_ID, Value, ProductName, C_BPartner_ID, PartnerName, M_Locator_ID, M_Warehouse_ID, WarehouseName, SUM(Qty) AS Qty ")
				.append("FROM (SELECT ip.*, p.Name AS ProductName, p.Value FROM SMJ_InfoProduct('" + LocSuc + "', '%" + value + "%', '" + column + "', '"
						+ noPos + "') ip ")
				.append("JOIN M_Product p ON (p.M_Product_ID = ip.M_Product_ID) ").append("WHERE ip.AD_Client_ID = " + Env.getAD_Client_ID(Env.getCtx()) + " ")
				.append("AND ip.AD_Org_ID = " + Env.getAD_Org_ID(Env.getCtx()) + " ");

		if (!vendorCode) {
			// Get Return Material
			sql.append("UNION ALL ")
					.append("SELECT ip.*, p.Name AS ProductName, p.Value FROM SMJ_InfoProductReturnMaterial('" + LocSuc + "', '%" + value + "%', '" + column
							+ "', '" + noPos + "') ip ")
					.append("JOIN M_Product p ON (p.M_Product_ID = ip.M_Product_ID) ")
					.append("WHERE ip.AD_Client_ID = " + Env.getAD_Client_ID(Env.getCtx()) + " ")
					.append("AND ip.AD_Org_ID = " + Env.getAD_Org_ID(Env.getCtx()) + " ");

			// Get Physical Inventory
			sql.append("UNION ALL ")
					.append("SELECT ip.*, p.Name AS ProductName, p.Value FROM SMJ_InfoProductPhysicalInv('" + LocSuc + "', '%" + value + "%', '" + column
							+ "', '" + noPos + "') ip ")
					.append("JOIN M_Product p ON (p.M_Product_ID = ip.M_Product_ID) ")
					.append("WHERE ip.AD_Client_ID = " + Env.getAD_Client_ID(Env.getCtx()) + " ")
					.append("AND ip.AD_Org_ID = " + Env.getAD_Org_ID(Env.getCtx()) + " ");
		}*/

		// Get Services
		if (!vendorCode && service) {
			sql.append("UNION ALL ").append("SELECT ip.*, p.Name AS ProductName, p.Value FROM SMJ_InfoProductService('%" + value + "%',")
					.append("'" + column + "', '" + noPos + "', '" + admService + "') ip ").append("JOIN M_Product p ON (p.M_Product_ID = ip.M_Product_ID) ")
					.append("WHERE ip.AD_Client_ID = " + Env.getAD_Client_ID(Env.getCtx()) + " ")
					.append("AND ip.AD_Org_ID = " + Env.getAD_Org_ID(Env.getCtx()) + " ");
		}

		sql.append(") x GROUP BY M_Product_ID, Value, ProductName, C_BPartner_ID, PartnerName, M_Locator_ID, M_Warehouse_ID, WarehouseName ")
				.append("ORDER BY ProductName, Qty ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				InfoProductList ipl = new InfoProductList();
				ipl.setProductID(rs.getInt("M_Product_ID"));
				ipl.setProductValue(rs.getString("Value"));
				ipl.setProductName(rs.getString("ProductName"));
				ipl.setPartnerId(rs.getInt("C_BPartner_ID"));
				ipl.setPartnerName(rs.getString("PartnerName"));
				ipl.setLocatorId(rs.getInt("M_Locator_ID"));
				ipl.setWarehouseId(rs.getInt("M_Warehouse_ID"));
				ipl.setWarehouseName(rs.getString("WarehouseName"));
				ipl.setQty(rs.getBigDecimal("Qty"));
				data.add(ipl);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, SMJInfoProduct.class.getName() + ".getProductList :: ", e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return data;

	}// getProductList

}// SMJSearchProducts
