package com.smj.webui.component;

import java.util.List;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.compiere.model.X_M_Substitute;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Row;

public class SMJSubstituteProduct extends Window {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8329699781853494548L;
	
	private EventListener<Event> eventBtnOk;
	private int M_Product_ID;;
	private List<X_M_Substitute> substitutes;
	
	public SMJSubstituteProduct(List<X_M_Substitute> substitutes, EventListener<Event> eventBtnOk) {
		super();
		this.eventBtnOk = eventBtnOk;
		this.substitutes = substitutes;
		initComponents();
	}
	
	private void initComponents() {
		String labelStyle = "font-size: 20px;color: #0B610B;background:#FFFFFF;";
		this.setWidth("300px");
		grid.setParent(this);
		
		cmbSubstitute.setMold("select");
		lblMessage.setStyle(labelStyle);
		row = rows.newRow();
		row.appendChild(newCell("center", 2, lblMessage));
		
		row = rows.newRow();
		row.appendChild(newCell("center", 2, cmbSubstitute));
		
		row = rows.newRow();
		row.appendChild(newCell("center", 1, btnOk));
		row.appendChild(newCell("center", 1, btnCancel));
		
		fillSustitutes();
		
		//Add Events
		cmbSubstitute.addActionListener(new EventListener<Event>() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				cmbSustituteOnChange();
			}
		});
		
		btnOk.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				close();
				eventBtnOk.onEvent(event);
			}
		});
		
		btnCancel.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				close();
			}
		});		
	}
	
	private void cmbSustituteOnChange() {
		M_Product_ID = cmbSubstitute.getSelectedItem().getValue(); 
	}
	
	private void fillSustitutes() {
		cmbSubstitute.removeAllItems();	
		
		for (X_M_Substitute mSubstitute : substitutes) {
			cmbSubstitute.appendItem(mSubstitute.getName(), mSubstitute.getSubstitute_ID());
		}
		
		cmbSubstitute.setSelectedIndex(0);
		M_Product_ID = cmbSubstitute.getSelectedItem().getValue();
	}
	
	private void close() {
		this.detach();
	}
	
	public int getM_Product_ID() {
		return M_Product_ID;
	}
	
	/**
	 * Crear un cell con las propiedades (align, colspan) y se le agregara un
	 * Component (Component)...
	 * @param align
	 * @param colspan
	 * @param component
	 * @return
	 */
	private Cell newCell(String align, int colspan, Component component) {
		Cell cell = new Cell();
		cell.setAlign(align);
		cell.setColspan(colspan);
		cell.appendChild(component);
		
		return cell;
	}

	private final Grid grid = GridFactory.newGridLayout();
	private final Rows rows = grid.newRows();
	private Row row;
	
	private	Label lblMessage = new Label(Msg.getElement(Env.getCtx(), "Substitute_ID"));
	private Listbox cmbSubstitute = new Listbox();
	
	private final Button btnOk = new Button("OK");
	private final Button btnCancel = new Button("Cancel");
}
