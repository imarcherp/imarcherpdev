package com.smj.webui.component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.model.InfoProductList;

/**
 * @version <li>SmartJSP: SMJInfoProduct, 2013/03/06
 *          <ul TYPE ="circle">
 *          <li>Ventana modal de informacion de productos
 *          <li>Modal Window for info product
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJInfoProduct extends InfoPanel implements EventListener<Event>, WTableModelListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3729208262465567749L;
	private Borderlayout layout;
	private Vbox southBody;
	private Textbox fieldValue = new Textbox();
	private Label fieldLabel = new Label();
	private LinkedList<InfoProductList> list = new LinkedList<InfoProductList>();
//	private Integer tiresCategory = Integer.parseInt(MSysConfig.getValue("SMJ-TIRESCATEGORY",Env.getAD_Client_ID(Env.getCtx())).trim());
//	private String noPos = MSysConfig.getValue("SMJ-PCATPROD_NOPOS",Env.getAD_Client_ID(Env.getCtx())).trim();
	private SMJSearchProducts search = null;
	private Boolean service = false;
	private String tempario = "";
	private List<EventListener<Event>> listeners = new ArrayList<EventListener<Event>>();
	
	/**
	 * SMJInfoProduct constructor
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param listVal
	 * @param searchService
	 */
	public SMJInfoProduct(int WindowNo, String tableName, String keyColumn, Boolean multipleSelection, 
			String whereClause, LinkedList<InfoProductList> listVal, Boolean searchService, String temparioName) {
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);
		init();
		search = new SMJSearchProducts();
		service = searchService;
		tempario = temparioName;
		list = listVal;
		loadTable();
//		searchProducts(filterValue);
	}//constructor

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		fieldValue.setWidth("50%");
		fieldValue.removeEventListener(Events.ON_BLUR, this);
		fieldValue.addEventListener(Events.ON_BLUR, this);
		fieldLabel.setText("" + Msg.translate(Env.getCtx(), "M_Product_ID"));
	
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(fieldLabel);
		row.appendChild(fieldValue);
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());

        
		layout = new Borderlayout();
        layout.setWidth("100%");
        layout.setHeight("100%");
        if (!isLookup())
        {
        	layout.setStyle("position: absolute");
        }
        this.appendChild(layout);

        North north = new North();
        layout.appendChild(north);
		north.appendChild(grid);

        Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		if (isLookup())
			contentPanel.setWidth("99%");
        else
        	contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setVflex(true);
        contentPanel.setMold("paging");
        contentPanel.setPageSize(MSysConfig.getIntValue("ZK_PAGING_SIZE", 100));
        contentPanel.setPagingPosition("bottom");
        contentPanel.setAutopaging(true);
        
		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        		
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
//		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
		Env.setContext(Env.getCtx(), p_WindowNo, "PRODUCT", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "PRICE", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "LOCATOR", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "PARTNER", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "QTY", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "PURCHASE", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "HOUR", "");
		Env.setContext(Env.getCtx(), p_WindowNo, "ALLOWCH", "");
	}//init
	
	@Override
	protected String getSQLWhere() {
		return null;
	}//getSQLWhere

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
	}//setParameters

	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		if (list == null || list.size()<=0){
			return;
		}
		Iterator<InfoProductList> it = list.iterator();
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (it.hasNext()){
			
			try {
				InfoProductList ipl = it.next();
				Vector<Object> rc = new Vector<Object>();
				rc.add(ipl.getProductValue());
				rc.add(ipl.getProductName());
				rc.add(ipl.getWarehouseName());
				rc.add(ipl.getPartnerName());
				rc.add(ipl.getQty());
				rc.add(ipl.getPrice());
				data.add(rc);
			} catch (Exception e) {
				log.log(Level.SEVERE, this.getClass().getName()+".loadTable :: "+e.getMessage(), e);
			}
		}//while
		
		Vector<String> columnNames = getTableColumnNames();
		contentPanel.getModel().clear();
		contentPanel.getModel().removeTableModelListener(this);
		//  Set Model
		ListModelTable modelTable = new ListModelTable(data);
		modelTable.removeTableModelListener(this);
//		model.addTableModelListener(this);
		contentPanel.setData(modelTable, columnNames);
//		contentPanel.addActionListener(this);
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		setTableColumnClass(contentPanel);
		contentPanel.setFocus(true);
		
		for (Component component : contentPanel.getListHead().getChildren()) {
			ListHeader header = (ListHeader) component;
			header.setHflex("max");
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "value"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Product_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
//		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelProvider"));
		columnNames.add(Msg.translate(Env.getCtx(), "qty"));	
		columnNames.add(Msg.translate(Env.getCtx(), "price"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, String.class, true); 
		table.setColumnClass(i++, BigDecimal.class, true);
		table.setColumnClass(i++, BigDecimal.class, true);   	  
		//  Table UI
		table.autoSize();
	}//setTableColumnClass

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event ev) {
		try {
			if (ev.getTarget().equals(fieldValue)){
				String field = fieldValue.getValue();
				if (field.length()<= 0 || field.equals("") || field.equals(" "))
					return;
				String locatorBranch = MWarehouse.get(Env.getCtx(), Integer.parseInt(Env.getCtx().getProperty("#M_Warehouse_ID"))).getName();
				list = search.searchProducts_Locator(fieldValue.getValue(), locatorBranch, service, tempario);
				loadTable();
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Integer index = contentPanel.getSelectedIndex();
				Set<Object> setProduct = contentPanel.getModel().getSelection();
				Iterator<Object> iteratorProduct = setProduct.iterator();							
				Vector<Object> vector = (Vector<Object>) iteratorProduct.next();
				
				if (index == null || index < 0){
					Messagebox.show(Msg.getMsg(Env.getCtx(), "SMJMSGSelectItemList"), "Info", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				
				InfoProductList ipl = getInfoProductSelected(vector);
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "PRODUCT", ipl.getProductID());
				Env.setContext(Env.getCtx(), p_WindowNo, "PRICE", ipl.getPrice().toString());
				Env.setContext(Env.getCtx(), p_WindowNo, "LOCATOR", ipl.getLocatorId());
				Env.setContext(Env.getCtx(), p_WindowNo, "PARTNER", ipl.getPartnerId());
				Env.setContext(Env.getCtx(), p_WindowNo, "QTY", ipl.getQty().toString());
								
				String purchase = "";
				try {
					purchase = ipl.getPurchasePrice().toString();
				} catch (Exception e) {
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "PURCHASE", purchase);
				String hour = "";
				try {
					hour = ipl.getEstimedTime().toString();
				} catch (Exception e) {
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "HOUR", hour);
				String allowCH = "";
				try {
					allowCH = ipl.getIsAllowChange().toString();
				} catch (Exception e) {
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "ALLOWCH", allowCH);
				fireEventListener(new Event(Events.ON_SELECT));
				dispose(false);
			}
			else if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
	        {
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
				Env.setContext(Env.getCtx(), p_WindowNo, "PRODUCT", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "PRICE", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "LOCATOR", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "PARTNER", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "QTY", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "PURCHASE", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "HOUR", "");
				Env.setContext(Env.getCtx(), p_WindowNo, "ALLOWCH", "");
	            dispose(false);
	        }
			else if (ev.getTarget().equals(contentPanel) && ev.getName().equals(Events.ON_DOUBLE_CLICK)) {
				Integer index = contentPanel.getSelectedIndex();
				
				if (index < 0 || list.isEmpty()) {
					return;
				}
				
				InfoProductList ipl = list.get(index);
				Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");
				Env.setContext(Env.getCtx(), p_WindowNo, "PRODUCT", ipl.getProductID());
				Env.setContext(Env.getCtx(), p_WindowNo, "PRICE", ipl.getPrice().toString());
				Env.setContext(Env.getCtx(), p_WindowNo, "LOCATOR", ipl.getLocatorId());
				Env.setContext(Env.getCtx(), p_WindowNo, "PARTNER", ipl.getPartnerId());
				Env.setContext(Env.getCtx(), p_WindowNo, "QTY", ipl.getQty().toString());
				String purchase = "";
				if(ipl.getPurchasePrice()!=null){
					purchase = ipl.getPurchasePrice().toString();
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "PURCHASE", purchase);
				BigDecimal estimedTime = Env.ZERO;
				if(ipl.getEstimedTime() != null){
						estimedTime = ipl.getEstimedTime();
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "HOUR", estimedTime.toString());
				Boolean allowChange = false;
				if (ipl.getIsAllowChange() != null){
					allowChange = ipl.getIsAllowChange();
				}
				Env.setContext(Env.getCtx(), p_WindowNo, "ALLOWCH", allowChange.toString());
				fireEventListener(new Event(Events.ON_SELECT));
				dispose(false);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getName()+".onEvent :: "	, e);
		}
	}//onEvent

	private InfoProductList getInfoProductSelected(Vector<Object> vector) {
		InfoProductList infoProduct = new InfoProductList();
		
		String productName = vector.get(1).toString();
		String warehouseName = (vector.get(2) != null) ? vector.get(2).toString() : "";
		
		for (int i = 0; i < list.size(); i++) {
			InfoProductList infoProductList = list.get(i);
			
			String infoProductName = infoProductList.getProductName();
			String infoWarehouseName = (infoProductList.getWarehouseName() != null) ? infoProductList.getWarehouseName() : "";
			
			if (productName.equals(infoProductName) && warehouseName.equals(infoWarehouseName)) {
				infoProduct = infoProductList;
			}
		}
		
		return infoProduct;
	}
	
	public void addEventListener(EventListener<Event> eventListener) {
		
		if (eventListener == null) {
			return;
		}
		
		listeners.add(eventListener);
	}
	
	public void fireEventListener(Event event) throws Exception {
		for (EventListener<Event> listener : listeners)
        {
           listener.onEvent(event);
        }
	}

}//SMJInfoProduct
