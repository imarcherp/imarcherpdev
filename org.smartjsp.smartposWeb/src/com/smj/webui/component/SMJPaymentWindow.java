package com.smj.webui.component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.Callback;
import org.adempiere.util.ProcessUtil;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.InfoPanel;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPaymentTerm;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MRefList;
import org.compiere.model.MStorageReservation;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.model.X_C_Invoice;
import org.compiere.model.X_C_Payment;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.compiere.wf.MWFProcess;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

import com.smj.model.InvoiceInsurance;
import com.smj.model.MSMJTmpPayment;
import com.smj.model.MSMJTmpWebSales;
import com.smj.model.MSMJTmpWebSalesLine;
import com.smj.model.PaymentList;
import com.smj.model.ProductionOrder;
import com.smj.util.CompleteSaleWeb;
import com.smj.util.DataQueries;
import com.smj.util.Documents;
import com.smj.util.Message;
import com.smj.util.SMJConfig;
import com.smj.util.ShowWindow;
import com.smj.util.Util;

/**
 * @version <li>SmartJSP: SMJInfoProduct, 2013/03/10
 *          <ul TYPE ="circle">
 *          <li>Ventana modal para el pago de la venta
 *          <li>Modal Window for payment sales
 *          </ul>
 * @author Freddy Rodriguez - "SmartJSP" - http://www.smartjsp.com/
 */
public class SMJPaymentWindow extends InfoPanel implements  WTableModelListener{
	private static final long serialVersionUID = -82379818861797771L;
	private Integer lclientId = Env.getAD_Client_ID( Env.getCtx());
	private Integer lorgId =  Env.getAD_Org_ID( Env.getCtx());
	private Boolean activeLoyalty = MSysConfig.getBooleanValue("SMJ_USE_LOYALTY", false, lclientId, lorgId);
	private int catCombosPOS = SMJConfig.getIntValue("M_Product_Category_ID", 0, lorgId);
	private final static String labelInfo = Msg.translate(Env.getCtx(), "SMJLabelInfo").trim();

	private NumberFormat nf;
	private Integer paytermDef = SMJConfig.getIntValue("C_PaymentTerm_ID", 0, lorgId);
	private Integer payInternal = MSysConfig.getIntValue("SMJ-PAYMENTTERMINTERNAL", -1, Env.getAD_Client_ID(Env.getCtx()));
	private Integer internalUser = Integer.parseInt(MSysConfig.getValue("SMJ-USERINTERNALPAY","0",Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx())).trim());
	private Integer payTermPoints = MSysConfig.getIntValue("SMJ_Lty_PaymentTerm_Points", 0, lclientId, lorgId);
	private String payForms = MSysConfig.getValue("SMJ-PAYFORMS",lclientId,lorgId).trim();
	private String tenderCash = MSysConfig.getValue("SMJ-TENDERTYPECASH",lclientId,lorgId).trim();
	private String tenderCheck = MSysConfig.getValue("SMJ-TENDERTYPECHECK",lclientId,lorgId).trim();
	private String tenderCreditCard = MSysConfig.getValue("SMJ-TENDERTYPECREDITCARD",lclientId,lorgId).trim();
	private Integer taxDiscounts = SMJConfig.getIntValue("C_Tax_ID", 0, lorgId);
	private final static String insuranceDiscountLabel = Msg.translate(Env.getCtx(), "SMJLabelInsuranceDiscount").trim();
	private Integer coverageChargeId = SMJConfig.getIntValue("C_Charge_ID", 0, lorgId);
	private BigDecimal maxCredit;

	private int retorno = 0;
	private Borderlayout layout;
	private Vbox southBody;
	private Label paytermLabel = new Label();
	private Listbox paytermListbox = new Listbox();
	private Label payTypeLabel = new Label();
	private Listbox payTypeListbox = new Listbox();
	private Label moneyLabel = new Label();

	private NumberBox moneyDecimalbox = new NumberBox(false, true) ;

	//Loyalty Module
	private BigDecimal salesPointsFactor = new BigDecimal(MSysConfig.getValue("SMJ_Lty_SalesPointsFactor", "0", lclientId, lorgId));

	private Label changeLabel = new Label();
	private Label changeDecimalBox = new Label();

	private Label treceivedLabel = new Label();
	private Label treceivedDecimalBox = new Label();

	private Grid infoLayout = GridFactory.newGridLayout();

	private Boolean isAproval = false;
	private Integer bpartnerId = 0;
	private String bpartnerName = "";
	private Label totalLabel = new Label();
	private Label totalDecimalbox = new Label();
	private Label residueLabel = new Label();
	private Label residueDecimalbox = new Label();
	private Label accLabel = new Label();
	private Label accDecimalbox = new Label();
	private Label moChangeLabel = new Label();
	private Label moChangeDecimalbox = new Label();
	private BigDecimal grandTotal = Env.ZERO;
	private BigDecimal accTotal = Env.ZERO;
	private BigDecimal residue = Env.ZERO;
	private BigDecimal moChange = Env.ZERO;
	private Integer invoiceId = 0;
	private LinkedList<Integer> pxList = new LinkedList<Integer>();
	private Integer payTermId = 0;
	private Integer tmpCode = 0;
	private String TenderType = tenderCash;
	//////credit card fields
	private Label cCardTypeLabel = new Label();
	private Listbox cCardTypeListbox = new Listbox();
	private Label numberLabel = new Label();
	private Textbox numberTextbox = new Textbox();
	private Label verficationLabel = new Label();
	private Textbox verficationTextbox = new Textbox();
	private Label monthLabel = new Label();
	private Intbox monthIntbox = new Intbox();
	private Label yearLabel = new Label();
	private Intbox yearIntbox = new Intbox();
	private Label nameLabel = new Label();
	private Textbox nameTextbox = new Textbox();
	//cheque fields
	private Label routeLabel = new Label();
	private Textbox routeTextbox = new Textbox();
	private Label accountLabel = new Label();
	private Decimalbox accountTextbox = new Decimalbox();
	private Label checkLabel = new Label();
	private Decimalbox checkTextbox = new Decimalbox();
	private Label placeLabel = new Label();
	private Decimalbox placeTextbox = new Decimalbox();
	private Button addButton = new Button ();
	private Button deleteButton = new Button ();
	private LinkedList<PaymentList> payList = new LinkedList<PaymentList>();
	private Date orderDate = new Date();
	private boolean generatedOnlyShipment = false;
	public boolean paymentProcessed = false;

	//Loyalty Fields
	private Label lblAvailablePoints;
	private Label txtAvailablePoints = new Label();
	private Label lblTotal2Points;
	private Label txtTotal2Points = new Label();
	private Label lblPointsReceived;
	private Label txtPointsReceived = new Label();

	private Callback<Integer> m_callback;
	private Integer idInsurance;
	private String plan;
	private BigDecimal coverage;
	private EventListener<Event> event;

	//Transport
	private Boolean useTransport = MSysConfig.getBooleanValue("SMJ_USE_TRANSPORT", false, lclientId, lorgId);
	private Integer orgInfotrans = MSysConfig.getIntValue("SMJ_ORGINFOTRANS", -1, Env.getAD_Client_ID(Env.getCtx()));
	private Integer idTransportRequestType = MSysConfig.getIntValue("SMJ-DOCUMENTTRANSPORTREQUEST", -1,Env.getAD_Client_ID(Env.getCtx()));
	private North north;
	private String orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), lorgId).trim();
	/**
	 * SMJInfoProduct constructor 
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param filterValue
	 */
	public SMJPaymentWindow(int WindowNo, String tableName, String keyColumn, boolean multipleSelection, String whereClause, 
			Integer partnerId, String partnerName, Integer M_PriceList_Version, Integer location, Integer warehouseId,
			BigDecimal grandT, BigDecimal taxT, BigDecimal subT, Integer tmpId, Integer paymentCode,String plate, Integer vehicleID, 
			String work_Order, Date dateInvoice, boolean gOnlyShipment, EventListener<Event> event) {

		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause);

		Integer numDec = SMJConfig.getIntValue("POS_Decimals", 0, lorgId);
		String[] localeVals = SMJConfig.getArrayStringValue("POS_Locale", ",", lorgId);
		nf = NumberFormat.getCurrencyInstance(new Locale(localeVals[0].trim(), localeVals[1].trim()));
		nf.setMaximumFractionDigits(numDec);
		nf.setMinimumFractionDigits(numDec);

		bpartnerId = partnerId;
		bpartnerName = partnerName;
		orderDate = dateInvoice;
		grandTotal = grandT.setScale(numDec, RoundingMode.UP);
		tmpCode = tmpId;
		generatedOnlyShipment = gOnlyShipment;

		if (paymentCode!= null) {
			payTermId = paymentCode; 
		} else {
			payTermId = paytermDef;
		}

		init();
		totalDecimalbox.setValue(nf.format(grandTotal));

		if (salesPointsFactor.signum() != 0)
			txtTotal2Points.setValue(grandTotal.divide(salesPointsFactor, 0, RoundingMode.UP).toString());

		if (activeLoyalty) {
			BigDecimal availablePoints = DataQueries.getPointsByBPartnerID(bpartnerId);
			txtAvailablePoints.setValue(availablePoints.toBigInteger() + " = " + nf.format(availablePoints.multiply(salesPointsFactor)));
		}
		residue = grandTotal;
		residueDecimalbox.setValue(nf.format(grandTotal));
		this.event = event;
	}//constructor

	/**
	 * ubica elementos en la ventana - ubicate elements in info window
	 */
	private void init()
	{
		paytermLabel.setText("" + Msg.translate(Env.getCtx(), "C_PaymentTerm_ID"));
		payTypeLabel.setText("" + Msg.translate(Env.getCtx(), "TenderType"));
		totalLabel.setText("" + Msg.translate(Env.getCtx(), "Total"));
		residueLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelResidue"));
		cCardTypeLabel.setText("" + Msg.translate(Env.getCtx(), "CreditCardType"));
		numberLabel.setText("" + Msg.translate(Env.getCtx(), "CreditCardNumber"));
		numberTextbox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTCreditcCard"));
		verficationLabel.setText("" + Msg.translate(Env.getCtx(), "CreditCardVV"));
		verficationTextbox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTVerfication"));
		monthLabel.setText("" + Msg.translate(Env.getCtx(), "CreditCardExpMM"));
		monthIntbox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTMonth"));
		yearLabel.setText("" + Msg.translate(Env.getCtx(), "CreditCardExpYY"));
		yearIntbox.setTooltiptext("" + Msg.translate(Env.getCtx(), "SMJTTYear"));
		nameLabel.setText("" + Msg.translate(Env.getCtx(), "A_Name"));
		routeLabel.setText("" + Msg.translate(Env.getCtx(), "RoutingNo"));
		accountLabel.setText("" + Msg.translate(Env.getCtx(), "AccountNo"));
		checkLabel.setText("" + Msg.translate(Env.getCtx(), "CheckNo"));
		placeLabel.setText("" + Msg.translate(Env.getCtx(), "Micr"));
		addButton.setLabel("" + Msg.translate(Env.getCtx(), "add"));
		addButton.addActionListener(this);
		deleteButton.setLabel("" + Msg.translate(Env.getCtx(), "SMJLabelDelete"));
		deleteButton.addActionListener(this);
		moneyLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelMoney"));
		changeLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelChange"));
		treceivedLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelMoney"));
		lblAvailablePoints = new Label();
		lblTotal2Points = new Label();
		lblPointsReceived = new Label();

		if (activeLoyalty) {
			lblAvailablePoints.setText(Msg.translate(Env.getCtx(), "SMJ_LabelAvailablePoints"));
			lblTotal2Points.setText(Msg.translate(Env.getCtx(), "SMJ_LabelTotalPoints"));
			lblPointsReceived.setText(Msg.translate(Env.getCtx(), "SMJ_LabelPointsReceived"));
		}

		accLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelAccumulated"));
		moChangeLabel.setText("" + Msg.translate(Env.getCtx(), "SMJLabelChange"));

		Grid grid = GridFactory.newGridLayout();
		Columns columns = new Columns();
		grid.appendChild(columns);

		for (int i = 0; i < 2; i++) {
			Column column = new Column();
			column.setWidth("15%");
			columns.appendChild(column);

			column = new Column();
			column.setWidth("35%");
			columns.appendChild(column);
		}

		Rows rows = grid.newRows();

		Row row = rows.newRow();
		lblTotal2Points.setWidth("100px");
		txtTotal2Points.setWidth("150px");
		lblAvailablePoints.setWidth("100px");		
		txtAvailablePoints.setWidth("150px");		

		row.appendChild(lblTotal2Points.rightAlign());
		row.appendChild(txtTotal2Points.rightAlign());
		row.appendCellChild(lblAvailablePoints);
		row.appendCellChild(txtAvailablePoints);
		row.setStyle("text-align: right");

		row = rows.newRow();
		totalLabel.setWidth("100px");
		row.appendChild(totalLabel.rightAlign());
		totalDecimalbox.setWidth("150px");
		row.appendChild(totalDecimalbox.rightAlign());
		residueLabel.setWidth("100px");
		row.appendChild(residueLabel.rightAlign());
		residueDecimalbox.setWidth("150px");
		changeDecimalBox.setText(nf.format(0));
		treceivedDecimalBox.setText(nf.format(0));
		row.appendChild(residueDecimalbox.rightAlign());

		//row
		row = rows.newRow();
		row.setAlign("right");
		paytermLabel.setWidth("100px");
		row.appendChild(paytermLabel.rightAlign());
		paytermListbox.setWidth("150px");
		row.appendChild(paytermListbox);

		row.setAlign("right");
		payTypeLabel.setWidth("100px");
		row.appendChild(payTypeLabel.rightAlign());
		payTypeListbox.setWidth("150px");
		row.appendChild(payTypeListbox);
		//		Tendertype = T -> cuenta
		if (TenderType.equals(tenderCash)){
			payTypeLabel.setVisible(false);
			payTypeListbox.setVisible(false);
		}

		//dinero
		row = rows.newRow();
		row.setAlign("right");
		moneyLabel.setWidth("100px");
		row.appendChild(moneyLabel.rightAlign());
		moneyDecimalbox.setWidth("150px");
		moneyDecimalbox.addEventListener(Events.ON_BLUR, this);		
		moneyDecimalbox.getDecimalbox().setFormat(Util.getNumberFormatAsString(nf.getMaximumFractionDigits()));
		if (useTransport) moneyDecimalbox.setValue(grandTotal);
		row.appendChild(moneyDecimalbox);
		changeLabel.setWidth("100px");
		row.appendChild(changeLabel.rightAlign());
		changeDecimalBox.setText(nf.format(0));
		changeDecimalBox.setWidth("150px");
		row.appendChild(changeDecimalBox.rightAlign());

		//Loyalty Fields
		row = rows.newRow();
		lblPointsReceived.setWidth("100px");
		txtPointsReceived.setWidth("100px");
		row.appendChild(lblPointsReceived.rightAlign());
		row.appendChild(txtPointsReceived.rightAlign());

		row = rows.newRow();
		row.setAlign("right");
		cCardTypeLabel.setWidth("100px");
		row.appendChild(cCardTypeLabel.rightAlign());
		cCardTypeListbox.setWidth("150px");
		row.appendChild(cCardTypeListbox);

		row = rows.newRow();
		row.setAlign("right");
		numberLabel.setWidth("100px");
		row.appendChild(numberLabel.rightAlign());
		numberTextbox.setWidth("150px");
		numberTextbox.setMaxlength(4);
		row.appendChild(numberTextbox);
		verficationLabel.setWidth("100px");
		row.appendChild(verficationLabel.rightAlign());
		verficationTextbox.setWidth("150px");
		verficationTextbox.setMaxlength(4);
		row.appendChild(verficationTextbox);

		row = rows.newRow();
		row.setAlign("right");
		monthLabel.setWidth("100px");
		row.appendChild(monthLabel.rightAlign());
		monthIntbox.setWidth("150px");
		monthIntbox.setMaxlength(2);
		row.appendChild(monthIntbox);
		yearLabel.setWidth("100px");
		row.appendChild(yearLabel.rightAlign());
		yearIntbox.setWidth("150px");
		yearIntbox.setMaxlength(4);
		row.appendChild(yearIntbox);

		row = rows.newRow();
		row.setAlign("right");
		routeLabel.setWidth("100px");
		row.appendChild(routeLabel.rightAlign());
		routeTextbox.setWidth("150px");
		row.appendChild(routeTextbox);
		accountLabel.setWidth("100px");
		row.appendChild(accountLabel.rightAlign());
		accountTextbox.setWidth("150px");
		row.appendChild(accountTextbox);

		row = rows.newRow();
		row.setAlign("right");
		checkLabel.setWidth("100px");
		row.appendChild(checkLabel.rightAlign());
		checkTextbox.setWidth("150px");
		row.appendChild(checkTextbox);
		placeLabel.setWidth("100px");
		row.appendChild(placeLabel.rightAlign());
		placeTextbox.setWidth("150px");
		row.appendChild(placeTextbox);

		row = rows.newRow();
		row.setAlign("right");
		nameLabel.setWidth("100px");
		row.appendChild(nameLabel.rightAlign());
		nameTextbox.setWidth("150px");
		row.appendChild(nameTextbox);
		row = rows.newRow();
		row.setAlign("right");
		row.appendChild(new Label());
		row.appendChild(new Label());
		row.appendChild(new Label());
		Div divb = new Div();
		divb.appendChild(addButton);
		divb.appendChild(deleteButton);
		row.appendChild(divb);
		/////////TOTALES .....
		Rows rowsb = infoLayout.newRows();
		Row rowb = rowsb.newRow();
		//row.......
		rowb = rowsb.newRow();
		rowb.setAlign("right");
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());
		accLabel.setWidth("100px");
		accLabel.rightAlign();
		rowb.appendChild(accLabel);
		accDecimalbox.setWidth("50px");
		accDecimalbox.rightAlign();
		accDecimalbox.setText(nf.format(0));
		rowb.appendChild(accDecimalbox);
		//row.......
		rowb = rowsb.newRow();
		rowb.setAlign("right");
		rowb.appendChild(new Label());
		rowb.appendChild(new Label());

		// total received
		treceivedLabel.setWidth("100px");
		treceivedLabel.rightAlign();
		rowb.appendChild(treceivedLabel);
		treceivedDecimalBox.setWidth("50px");
		treceivedDecimalBox.rightAlign();
		treceivedDecimalBox.setText(nf.format(0));
		rowb.appendChild(treceivedDecimalBox);

		moChangeLabel.setWidth("100px");
		moChangeLabel.rightAlign();
		rowb.appendChild(moChangeLabel);
		moChangeDecimalbox.setWidth("50px");
		moChangeDecimalbox.rightAlign();
		moChangeDecimalbox.setText(nf.format(0));
		rowb.appendChild(moChangeDecimalbox);

		layout = new Borderlayout();
		layout.setWidth("100%");
		layout.setHeight("100%");
		if (!isLookup())
		{
			layout.setStyle("position: absolute");
		}
		this.appendChild(layout);

		north = new North();
		north.setMaxsize(300);
		layout.appendChild(north);
		north.appendChild(grid);

		Center center = new Center();
		layout.appendChild(center);
		center.setVflex("1");
		Div div = new Div();
		div.appendChild(contentPanel);
		contentPanel.setWidth("100%");
		contentPanel.setVflex(true);
		//		contentPanel.setHeight("100%");

		div.setStyle("width :100%; height: 100%");
		center.appendChild(div);

		infoLayout.setWidth("100%");
		infoLayout.setHeight("100%");
		infoLayout.setStyle("border: none");

		South south = new South();
		layout.appendChild(south);
		southBody = new Vbox();
		southBody.setWidth("100%");
		south.appendChild(southBody);
		southBody.appendChild(infoLayout);
		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);	
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(false);
		confirmPanel.getButton(ConfirmPanel.A_REFRESH).setVisible(false);
		listPayterm();
		listPayType();	
		listCreditCard();
		if(internalUser.compareTo(bpartnerId)==0  && payInternal!=0)
			paytermListbox.setValue(payInternal);
		else
			paytermListbox.setValue(payTermId);
		Integer pay = 0;
		try {
			pay = (Integer) paytermListbox.getSelectedItem().getValue();
		} catch (Exception e) {
		}
		if(payTermId.equals(paytermDef) || pay.equals(paytermDef)){
			payTypeLabel.setVisible(true);
			payTypeListbox.setVisible(true);
			visibleFields(tenderCash);
		}else{
			payTypeLabel.setVisible(false);
			payTypeListbox.setVisible(false);
			visibleFields(TenderType);
		}
		confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);	

	}//init

	/**
	 * muestro u oculta los campos segun tipo de pago -
	 *  show or hide fields by tender type
	 * @param tType
	 */
	private void visibleFields(String tType){
		cCardTypeLabel.setVisible(false);
		cCardTypeListbox.setVisible(false);
		numberLabel.setVisible(false);
		numberTextbox.setVisible(false);
		verficationLabel.setVisible(false);
		verficationTextbox.setVisible(false);
		monthLabel.setVisible(false);
		monthIntbox.setVisible(false);
		yearLabel.setVisible(false);
		yearIntbox.setVisible(false);
		nameLabel.setVisible(false);
		nameTextbox.setVisible(false);
		routeLabel.setVisible(false);
		routeTextbox.setVisible(false);
		accountLabel.setVisible(false);
		accountTextbox.setVisible(false);
		checkLabel.setVisible(false);
		checkTextbox.setVisible(false);
		placeLabel.setVisible(false);
		placeTextbox.setVisible(false);
		nameLabel.setVisible(false);
		nameTextbox.setVisible(false);

		boolean isPayTermPoints = tType.equals(payTermPoints.toString());
		lblAvailablePoints.setVisible(isPayTermPoints);
		txtAvailablePoints.setVisible(isPayTermPoints);
		lblTotal2Points.setVisible(isPayTermPoints);
		txtTotal2Points.setVisible(isPayTermPoints);
		lblPointsReceived.setVisible(isPayTermPoints);
		txtPointsReceived.setVisible(isPayTermPoints);
		
		north.setHeight("200px");

		if (tType.equals(tenderCreditCard)){ //tarjeta credito
			cCardTypeLabel.setVisible(true);
			cCardTypeListbox.setVisible(true);
			numberLabel.setVisible(true);
			numberTextbox.setVisible(true);
			verficationLabel.setVisible(true);
			verficationTextbox.setVisible(true);
			monthLabel.setVisible(true);
			monthIntbox.setVisible(true);
			yearLabel.setVisible(true);
			yearIntbox.setVisible(true);
			nameLabel.setVisible(true);
			nameTextbox.setVisible(true);
			north.setHeight("300px");
		}else if (tType.equals(tenderCheck)){ //cheque
			routeLabel.setVisible(true);
			routeTextbox.setVisible(true);
			accountLabel.setVisible(true);
			accountTextbox.setVisible(true);
			checkLabel.setVisible(true);
			checkTextbox.setVisible(true);
			placeLabel.setVisible(true);
			placeTextbox.setVisible(true);
			nameLabel.setVisible(true);
			nameTextbox.setVisible(true);
			north.setHeight("275px");
		}		
	}//visibleFields

	@Override
	protected String getSQLWhere() {
		return null;
	}

	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount)
			throws SQLException {
	}

	@Override
	public void onEvent(Event ev) {

		try {
			if (ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
			{
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGWantCancel"), labelInfo,Messagebox.OK|Messagebox.CANCEL , Messagebox.QUESTION, new Callback<Integer>() {
					@Override
					public void onCallback(Integer result) {

						if (result.equals( Messagebox.OK)) {
							retorno = 1;
							dispose(false);
							MSMJTmpWebSales tmp = new MSMJTmpWebSales(Env.getCtx(), tmpCode, null);
							MOrder order = new MOrder(Env.getCtx(), tmp.getC_Order_ID(), null);
							if (order.isComplete()) {
								order.reActivateIt();
								order.setDocStatus(DocAction.STATUS_Drafted);
								order.saveEx();
							}
						}
					}
				});				

				return;
			}//if cancel
			else if(ev.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))){
				Boolean ok = salesProcess();
				if(ok){
					Env.setContext(Env.getCtx(), p_WindowNo, "OK", "true");					
					paymentProcessed = true;
					dispose(false);	
					event.onEvent(ev);
				}else{
					Env.setContext(Env.getCtx(), p_WindowNo, "OK", "false");
					paymentProcessed = false;
				}
				return;
			}//if OK
			else if (ev.getTarget().equals(moneyDecimalbox.getDecimalbox())){

				Integer payTerm = paytermListbox.getSelectedItem().getValue();

				if (moneyDecimalbox.getValue() == null) {
					moneyDecimalbox.setValue(Env.ZERO);
				}

				if (payTerm.equals(paytermDef)) {

					BigDecimal change = moneyDecimalbox.getValue().subtract(residue);
					if(change.compareTo(Env.ZERO)<=0){
						change = Env.ZERO;
					}
					changeDecimalBox.setValue(nf.format(change));
					String tType = (String) payTypeListbox.getSelectedItem().getValue();
					if (tType != null && tType.equals(tenderCash)){
						moChange = change;
					}
				} else {						
					residue = grandTotal.subtract(accTotal);

					//No se permite agregar un valor mayor al valor faltante ya que no puede haber
					//devolucion de dinero en efectivo cuando es credito 
					if (moneyDecimalbox.getValue().compareTo(residue) == 1) {
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MSGExceedRemaining"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
						moneyDecimalbox.setValue(residue);
						return;
					}
				}

				if (salesPointsFactor.signum() != 0)
					txtPointsReceived.setValue(moneyDecimalbox.getValue().divide(salesPointsFactor, RoundingMode.UP).toString());

				return;
			}//moneyDecimalbox
			else if(ev.getTarget().equals(payTypeListbox)){
				String tType = (String) payTypeListbox.getSelectedItem().getValue();
				visibleFields(tType);
				return;
			}//payTypeListbox
			else if(ev.getTarget().equals(paytermListbox)){
				Integer pay = (Integer) paytermListbox.getSelectedItem().getValue();

				if (pay <= 0){
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectPayterm"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}

				if(pay.equals(paytermDef)) { //Cash
					payTypeLabel.setVisible(true);
					payTypeListbox.setVisible(true);
					changeLabel.setVisible(true);
					changeDecimalBox.setVisible(true);
					visibleFields(tenderCash);
				} else if (pay.equals(payTermPoints)) {
					payTypeLabel.setVisible(false);
					payTypeListbox.setVisible(false);
					changeLabel.setVisible(false);
					changeDecimalBox.setVisible(false);

					visibleFields(payTermPoints.toString());

					BigDecimal points = DataQueries.getPointsByBPartnerID(bpartnerId);

					if (salesPointsFactor.signum() < 1) {
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MsgSalesPointsFactorNotFound"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
						paytermListbox.setValue(-1);
						return;
					}

					if (points.signum() < 1) {
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MsgInsufficientPoints"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
						paytermListbox.setValue(-1);
						return;
					}
				} else { //Credit
					payTypeLabel.setVisible(false);
					payTypeListbox.setVisible(false);
					changeLabel.setVisible(false);
					changeDecimalBox.setVisible(false);

					visibleFields("Z");

					//Verificar que el usuario tenga credito habilitado
					MBPartner bPartner = new MBPartner(Env.getCtx(), bpartnerId, null);

					if (bPartner.getSO_CreditLimit() == null || bPartner.getSO_CreditLimit().compareTo(Env.ZERO) <= 0) {
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MSGNoCredit"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
						paytermListbox.setValue(-1);
						return;
					}

					maxCredit = bPartner.getSO_CreditLimit().subtract(bPartner.getSO_CreditUsed());
				}
				return;
			}//paytermListbox
			else if (ev.getTarget().equals(addButton)){
				if (useTransport && ((String) payTypeListbox.getSelectedItem().getValue()).equals(tenderCheck)) {
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGPayCheckNeedAp"), labelInfo,Messagebox.OK|Messagebox.CANCEL , Messagebox.QUESTION, new Callback<Integer>() {
						@Override
						public void onCallback(Integer result) {

							if (result.equals( Messagebox.OK)) {
								addPay();
							}
						}
					});
				} else {
					addPay();
				}

				return;
			}
			else if (ev.getTarget().equals(deleteButton)){
				try {
					Integer index = contentPanel.getSelectedIndex();
					if (index != null && index >= 0){
						PaymentList ipl = payList.get(index);
						payList.remove(ipl);
						loadTable();
					}else{
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectItemList"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					}
				} catch (Exception e) {
					log.log(Level.SEVERE, this.getClass().getCanonicalName()+".onEvent ERROR:: ", e);
				}
				return;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".onEvent ERROR:: ", e);
		}
	}//onEvent

	/**
	 * Llena el combo de terminos de pago
	 * fill payterm list
	 */
	private void listPayterm(){

		if (useTransport) {
			listPaytermTrans();
			return;
		}

		paytermListbox.removeAllItems();
		int[] listPayTerm = SMJConfig.getArrayIntValue("ListPayTerm", ",", lorgId);
		paytermListbox.appendItem("", -1);
		for (int i = 0; i < listPayTerm.length; i++) {
			Integer idPayTermTmp = listPayTerm[i];

			if (idPayTermTmp.equals(payTermPoints) && !activeLoyalty)
				continue;

			MPaymentTerm paymentTerm = new MPaymentTerm(Env.getCtx(), idPayTermTmp, null);
			paytermListbox.appendItem(paymentTerm.getName(), paymentTerm.get_ID());
		}
		paytermListbox.setMold("select");
		paytermListbox.addActionListener(this);
		paytermListbox.setValue(-1);
	}//listPayterm	

	private void listPaytermTrans(){
		paytermListbox.removeAllItems();
		Vector<Vector<Object>> data = DataQueries.getPayTermListTrans(Env.getAD_Client_ID(Env.getCtx()), payTermId, paytermDef, bpartnerId);
		paytermListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			paytermListbox.appendItem((String) line.get(1), line.get(0));
		}//for

		int[] listPayTerm = SMJConfig.getArrayIntValue("ListPayTerm", ",", lorgId);

		if (listPayTerm != null) {
			
			for (int i = 0; i < listPayTerm.length; i++) {

				boolean exists = false;

				for (int j = 0; j < data.size(); j++) {
					if (listPayTerm[i] == ((Integer) data.get(j).get(0))) {
						exists = true;
						break;
					}
				}

				if (exists) {
					continue;
				}

				Integer idPayTermTmp = listPayTerm[i];

				if (idPayTermTmp.equals(payTermPoints) && !activeLoyalty)
					continue;

				MPaymentTerm paymentTerm = new MPaymentTerm(Env.getCtx(), idPayTermTmp, null);
				paytermListbox.appendItem(paymentTerm.getName(), paymentTerm.get_ID());
			}

		}

		paytermListbox.setMold("select");
		paytermListbox.addActionListener(this);
		paytermListbox.setValue(paytermDef);
	}//listPayterm	

	/**
	 * Llena el combo de tipos de pago
	 * fill pay type list
	 */
	private void listPayType(){
		payTypeListbox.removeAllItems();
		//Vector<Vector<Object>> data = DataQueries.getTenderTypeList(payForms);
		ValueNamePair[] refList = MRefList.getList(Env.getCtx(), 214, false);
		for (int i=0; i < refList.length;i++){
			if (payForms.contains(refList[i].getValue()))
				payTypeListbox.appendItem(refList[i].getName(), refList[i].getValue());		
		}//for
		payTypeListbox.setMold("select");
		payTypeListbox.addActionListener(this);
		payTypeListbox.setValue(tenderCash);
	}//listPayterm

	/**
	 * Llena el combo de tipos de tarjeta de credito
	 * fill credit card type list
	 */
	private void listCreditCard(){
		cCardTypeListbox.removeAllItems();
		Vector<Vector<Object>> data = DataQueries.getCredictCardTypeList();
		cCardTypeListbox.appendItem(" ",-1);
		for (int i=0; i<data.size();i++){
			Vector<Object> line = data.get(i);
			cCardTypeListbox.appendItem((String) line.get(1), line.get(0));
		}//for
		cCardTypeListbox.setMold("select");
		cCardTypeListbox.addActionListener(this);
		cCardTypeListbox.setValue(tenderCash);
	}//listPayterm

	/**
	 * proceso de venta - 
	 * sales process
	 * @return
	 */
	private Boolean salesProcess() {
		Trx trx = Trx.get(Trx.createTrxName("SMJPW"), false);
		Boolean flag = true;
		Boolean hasTransportRequest = false;
		HashMap<Integer, MProduct> kits = new HashMap <Integer,MProduct> (); 


		HashMap<Integer,BigDecimal> qtyKits = new HashMap<Integer,BigDecimal> ();	
		HashMap<Integer,Integer> warehouseIds = new HashMap<Integer,Integer> ();
		try {
			if(payTypeListbox.getSelectedItem() == null || payTypeListbox.getSelectedItem().getValue() == null
					|| payTypeListbox.getSelectedItem().getValue().equals("")){
				trx.rollback();
				trx.close();
				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGSelectPayType"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			String desc = bpartnerName + " - " + sdf.format(orderDate);
			String descCustomer = bpartnerName + " - " + sdf.format(orderDate);

			Integer pay = payTermId;
			try {
				pay = getPayTerm();
				if (pay < 0){
					trx.rollback();
					trx.close();
					Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGSelectPayterm"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}
			} catch (Exception e) {
				trx.rollback();
				trx.close();
				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGSelectPayterm"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return false;
			}

			String payRule = "";

			if (payList.size() == 1 && (pay.equals(paytermDef)) && !useTransport) {
				String tType = payList.get(0).getTenderType();

				if (tType.equalsIgnoreCase(X_C_Payment.TENDERTYPE_Cash)) {
					payRule = X_C_Invoice.PAYMENTRULE_Cash;
				} else if (tType.equalsIgnoreCase(X_C_Payment.TENDERTYPE_CreditCard)) {
					payRule = X_C_Invoice.PAYMENTRULE_CreditCard;
				} else if (tType.equalsIgnoreCase(X_C_Payment.TENDERTYPE_Check)) {
					payRule = X_C_Invoice.PAYMENTRULE_Check;
				}
			} else {
				payRule = X_C_Invoice.PAYMENTRULE_OnCredit;
			}

			MSMJTmpWebSales tmp = new MSMJTmpWebSales(Env.getCtx(), tmpCode, trx.getTrxName());
			MOrder order = new MOrder(Env.getCtx(), tmp.getC_Order_ID(), trx.getTrxName());

			if (useTransport && order.get_Value("SMJ_Plate") != null && !order.get_ValueAsString("SMJ_Plate").isEmpty()) {
				descCustomer += "\n" + Msg.getElement(Env.getCtx(), "SMJ_Plate") + ": " + order.get_Value("smj_plate");
			}

			order.setDescription(descCustomer);
			order.setC_PaymentTerm_ID(pay);
			order.setPaymentRule(payRule);
			order.saveEx();

			ArrayList<Integer> prodIdToCheck = new ArrayList<Integer> ();
			ArrayList<Integer> prodASIToCheck = new ArrayList<Integer> ();
			ArrayList<Integer> prodWarehouseIdToCheck = new ArrayList<Integer> ();
			ArrayList<BigDecimal> qtyprodToCheck = new ArrayList<BigDecimal> ();

			List<MSMJTmpWebSalesLine> list = Arrays.asList(tmp.getLines());

			Iterator<MSMJTmpWebSalesLine> it = list.iterator();
			list = null;
			int lastLineNumber = 0;
			while (it.hasNext()){
				MSMJTmpWebSalesLine ipl = it.next();
				
				//Adicion iMarch / Organizacion Trasnportadora de Carga------
				orgfreighttrans = MSysConfig.getValue("iMarch_ORGFREIGHTTRANS", "false", Env.getAD_Client_ID(Env.getCtx()), lorgId).trim();
				if(orgfreighttrans.equals("true"))
					orgInfotrans=lorgId;
				//-----------------------------------------------------------
				if (useTransport) {
					if (lorgId.equals(orgInfotrans) && lastLineNumber == 0) //analizando primera linea de temporales del pvw
					{ 
						// obtiene el id de la orden de trabajo correspondiente a esta linea
						int rId = ipl.getR_Request_ID();
						// obtiene a que tipo de orden de trabajo/servicio pertenece				  
						int requestTypeId = DataQueries.getRequestTypeByRequestId(trx.getTrxName(), rId);
						if (requestTypeId == idTransportRequestType) 
						{
							hasTransportRequest = true; // la primera linea corresponde a una orden de transnporte se deja marcada
						}

					}
				}

				//              SMJ Fix reservas
				prodIdToCheck.add(ipl.getM_Product_ID());
				prodASIToCheck.add(ipl.getM_AttributeSetInstance_ID());
				prodWarehouseIdToCheck.add(ipl.getM_Warehouse_ID());
				qtyprodToCheck.add(ipl.getQtyOrdered());
				//				
				String descLine = desc;
				Boolean ok = true;

				if (ipl.getM_Product_ID() <= 0) {
					int C_OrderLine_ID = Documents.createOrderLine(order, ipl.getM_Product_ID(), ipl.getQtyEntered(), ipl.getC_UOM_ID(), ipl.getPriceEntered()
							, "", 0, 0, order.getC_BPartner_ID(), false, ipl.getC_BPartner_ID(), order.getDateOrdered(), ipl.getsmj_plate(), null, ipl.getLine());
					ipl.setC_OrderLine_ID(C_OrderLine_ID);

				}

				if ( ipl.getM_Product().getM_Product_Category_ID() == catCombosPOS ){
					if (ipl.getM_Product_ID() > 0 && ipl.getM_Product().isBOM()     ) {
						// aqui valida cantidades en existencia y temas de produccion
						// primero mira las exustencias del producto kit, para solo validar las BOM, de lo que se vaya a crear en la o. de produccion

						BigDecimal currentStock = DataQueries.getTotalProductByWarehouse(ipl.getM_Product().getM_Product_ID(), ipl.getM_Warehouse_ID(), trx.getTrxName());
						BigDecimal currentUsedStock = DataQueries.getTotalTempProductByWarehouse(trx.getTrxName(), ipl.getM_Product().getM_Product_ID(), ipl.getM_Warehouse_ID());

						BigDecimal qtyRequired = BigDecimal.ZERO;
						if (currentStock.compareTo(currentUsedStock) < 0) {   // if current stock of KIT is not enough we create a prod order 
							qtyRequired=  currentUsedStock.subtract(currentStock);
							String validBOMStock = DataQueries.validStockBOMProducts((MProduct) ipl.getM_Product(),trx.getTrxName(),ipl.getM_Warehouse_ID() , qtyRequired );
							if (!validBOMStock.equalsIgnoreCase("")) {
								// mostrar nuevo mensage // Neither we have stock of this product nor we have ingredients to build it.
								// mosrar error con el nombre del producto que no tiene existencias.
								Message.showWarningPlusInfo("SMJMSGQtyEntered",validBOMStock, true);
								return false;
							}
						}
						else {
							qtyRequired=  currentUsedStock;
						} //  if (currentStock.

						// aqui va acumulando los productos si son KITS y sus cantidades para luego expandir sus ingredientes al final
						// si se repite el KIT, se acumula la cantidad para no repetirlos ...
						if(kits.containsKey((Integer)ipl.getM_Product().getM_Product_ID())  ) {   // ya existe el kit en el ticket ?
							BigDecimal qtyPrev = (BigDecimal)qtyKits.get((Integer) (ipl.getM_Product()).getM_Product_ID() ); // obtiene cantidad previa 
							qtyKits.put((Integer)ipl.getM_Product().getM_Product_ID(),ipl.getQtyOrdered().add(qtyPrev)); //acumula nueva cantidad
						}
						else {
							kits.put((Integer)ipl.getM_Product().getM_Product_ID(), ( MProduct) ipl.getM_Product());
							qtyKits.put((Integer)ipl.getM_Product().getM_Product_ID(),ipl.getQtyOrdered());
							warehouseIds.put((Integer)ipl.getM_Product().getM_Product_ID(),ipl.getM_Warehouse_ID());
						} // if(kits.containsKey

					}  //ipl.getM_Product_ID() > 0
				}
				lastLineNumber = ipl.getLine();   // keep trak of last line sequence (order) to add new order lines for BOM details later 

				MOrderLine line = new MOrderLine(Env.getCtx(), ipl.getC_OrderLine_ID(), trx.getTrxName());
				
				if (ipl.getsmj_notes()!=null)	
					descLine = ipl.getsmj_notes().trim(); //iMarch
				else
					descLine = "";
				
				line.setDescription(descLine);

				// aqui define que es un cargo a la orden

				if(ipl.getProductName().equalsIgnoreCase(insuranceDiscountLabel) )   // is the coverage line
				{
					line.setC_Tax_ID(taxDiscounts);                 //default tax for discounts
					line.setC_Charge_ID(coverageChargeId);          //charge type for coverage discounts
					line.setPrice(ipl.getPriceEntered().multiply(new BigDecimal(-1)));
				}
				
				line.saveEx();

				Boolean rok = true;
				try {
				} catch (Exception e) {
					rok = false;
					log.log(Level.SEVERE, this.getClass().getCanonicalName()+ ".salesProcess - ERROR: " + e.getMessage(), e);
				}
				if (!ok || !rok){
					trx.rollback();
					trx.close();
					Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGErrorCreateSalesOrder"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				}else{
					ipl.set_TrxName(trx.getTrxName());
					ipl.deleteEx(true);
				}
			}//while



			//SMJ BOM Changes
			Iterator<Integer> itk = kits.keySet().iterator();
			while(itk.hasNext()) {
				Integer mKey = itk.next();
				// here it will create the production order if we did not have stock for the KIT
				BigDecimal currentStock = DataQueries.getTotalProductByWarehouse(kits.get(mKey).get_ID(), warehouseIds.get(mKey), trx.getTrxName());

				if (currentStock.compareTo(qtyKits.get(mKey)) < 0) {   // if current stock of KIT is not enough we create a prod order 
					ProductionOrder po = new ProductionOrder(kits.get(mKey).getM_Product_ID(),trx.getTrxName(),Env.getCtx(),(qtyKits.get(mKey).subtract(currentStock)),DataQueries.getLocatorId(warehouseIds.get(mKey))); // crea orden de produccion solo por la diferencia  
					trx.commit();
					if (po.getProduction().isComplete()) {
						log.log(Level.INFO,"Produccion creada:" + po.getProduction().get_ID());	
						//int sizeBOM  = showBOMProduct(p,webSalesLine.getQty());	
					}
					else
					{
						log.log(Level.SEVERE,"Produccion tiene errores:" + po.getProduction().get_ID());
						// mensaje de que no tiene inventario para este kit o hubo error en la orden de produccion anterior, revisar.
						//Message.showWarning("SMJMSGProdOrderInvalid");
					}
				}

				// here it will expand the BOM product descripcion and qty of previous KITS if they were included before
				// they are check to be in the correct category 

				if (kits.get(mKey).getM_Product_Category_ID() == catCombosPOS ) { 
					if (kits.get(mKey).isBOM()  ) {

						Documents.createOrderLine(order, 0, BigDecimal.ZERO,0, BigDecimal.ZERO, "--> "+kits.get(mKey).getName()+ " <--" , 
								0, 0, order.getC_BPartner_ID(), false, 0, new Date(), "", null, lastLineNumber+=10);
						trx.commit();
						//BOM lines		
						MProductBOM [] bomLines = MProductBOM.getBOMLines( kits.get(mKey) );
						for (int j =0; j< bomLines.length;j++   ){
							Documents.createOrderLine(order, 0, bomLines[j].getBOMQty().multiply(qtyKits.get(mKey) ), bomLines[j].getM_ProductBOM().getC_UOM_ID(), BigDecimal.ZERO
									, bomLines [j].getM_ProductBOM().getName() , 0, 0, order.getC_BPartner_ID(), false, 0, new Date(), 
									"", null, lastLineNumber+=10);
							trx.commit();
						} // for

					}//  if isBOM	
				} // Pos category
			} // while
			// SMJ BOM changes						



			Boolean pok = true;
			Boolean ok = true;			

			if (isAproval && useTransport) {
				Iterator<PaymentList> itpl = payList.iterator();
				String descCheck = "";
				while(itpl.hasNext()){
					PaymentList pm = itpl.next();

					MSMJTmpPayment px = new MSMJTmpPayment(Env.getCtx(), 0, trx.getTrxName());
					px.setC_PaymentTerm_ID(pm.getTypeTerm());
					px.setTenderType(pm.getTenderType()); 
					px.setcctype(pm.getCreditCardType());
					px.setccnumber(pm.getCreditCardNumber());
					px.setccverfication(pm.getNewCreditCardVV());
					Integer mm = pm.getCreditCardExpMM();
					if (mm != null && mm > 0){
						px.setccexpmm(mm);
					}
					Integer yy = pm.getNewCreditCardExpYY();
					if (yy != null && yy > 0){
						px.setccexpyy(yy);
					}
					px.setName(pm.getName());
					px.setRoutingNo(pm.getRoutingNo());
					px.setAccountNo(pm.getAccountNo());
					px.setCheckNo(pm.getCheckNo());
					px.setMicr(pm.getMicr());
					px.settotal(pm.getValue());
					px.setC_Order_ID(order.getC_Order_ID());
					Boolean bpx = px.save();

					if (pm.getTenderType().equals(tenderCheck)){
						descCheck = descCheck+"CHEQUE"+": "+pm.getCheckNo()+" - "+ Msg.getMsg(Env.getCtx(), "SMJLabelValue")+": "+pm.getValue()+" ; ";
					}

					if (!bpx){
						ok = false;
					}
				}//while

				order.setIsApproved(false);
				order.setDescription(order.getDescription()+" **** "+descCheck);
				order.set_ValueNoCheck("OrderType", "CHEQUE");
				order.setDocAction(DocAction.ACTION_Complete);
				order.saveEx();

				ProcessInfo wfProcess = new ProcessInfo(trx.getTrxName(), 116, 259, order.getC_Order_ID());
				wfProcess.setAD_User_ID (Env.getAD_User_ID(Env.getCtx()));
				wfProcess.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
				wfProcess.setTransactionName(trx.getTrxName());
				MWFProcess wdPro = ProcessUtil.startWorkFlow(Env.getCtx(), wfProcess, 116);
				//Boolean started = wdPro != null;

				if(wdPro == null) {
					log.log(Level.SEVERE, "Document action could not be proccesed");
				} else if (wfProcess.isError()) {
					log.log(Level.SEVERE, "Document action error: "+wfProcess.getSummary());
					trx.rollback();
					trx.close();
					Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGErrorCreateSalesOrder"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
					return false;
				} else {  	
					log.log(Level.INFO, "Document action processed ["+wfProcess.getSummary()+"]");
				}

				tmp.deleteEx(true);
				trx.commit(true);				
				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGTrxApprobalProcess"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				ShowWindow.openSalesOrder(order.get_ID());
				return true;
			}

			order.setDocAction(DocAction.ACTION_Complete);
			order.saveEx();			

			if (!order.processIt(DocAction.ACTION_Complete)) {
				Message.showError(Msg.parseTranslation(Env.getCtx(), order.getProcessMsg()), false);
				throw new AdempiereException(order.getProcessMsg());
			}
			
			//iMarch actualiza hora DataOrdered
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(order.getDateOrdered().getTime());
			c.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		    c.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));
		    c.set(Calendar.SECOND, Calendar.getInstance().get(Calendar.SECOND));
		    c.set(Calendar.MILLISECOND, 0);
		    order.setDateOrdered(new Timestamp(c.getTimeInMillis()));
			//fin iMarch
			
			order.saveEx();

			String sql = "DELETE FROM M_StorageReservation";
			DB.executeUpdateEx(sql, trx.getTrxName());
			
			// creates the insurance invoice just if the customer has insurance

			MBPartner bp1 = MBPartner.get(Env.getCtx(), bpartnerId);

			int insuranceId1 = bp1.get_ValueAsInt("smj_insurancecompany_id");
			int bPlan1 = bp1.get_ValueAsInt("smj_insuranceplan_id");

			if (insuranceId1 > 0 && bPlan1 > 0){   // just if the bpartner has insurance & plan

				InvoiceInsurance invInsu = new InvoiceInsurance();
				invInsu.setCoverage(coverage);
				invInsu.setIdInsurance(idInsurance);
				invInsu.setPlan(plan);
				invInsu.generate(trx, order, desc);

			}

			MInvoice inv = CompleteSaleWeb.completeDocumentsSales(trx.getTrxName(), true, order, generatedOnlyShipment, hasTransportRequest);

			if (inv == null && !generatedOnlyShipment) {
				ok = false;
			} else {

				if ((generatedOnlyShipment || !DataQueries.existsPayment(inv.getC_Invoice_ID(),trx.getTrxName())) && ok) {

					Boolean activeLoyalty = MSysConfig.getBooleanValue("SMJ_USE_LOYALTY", false, lclientId, lorgId);

					if (!generatedOnlyShipment && activeLoyalty && !inv.getPaymentRule().equalsIgnoreCase(MInvoice.PAYMENTRULE_Cash)) {
						List<PaymentList> payPoints = new ArrayList<PaymentList>();

						for (PaymentList payp: payList) {
							if (payp.getTypeTerm().equals(payTermPoints))
								payPoints.add(payp);
						}

						if (!payPoints.isEmpty()) {
							ok = Documents.createPaymentForPoints(payPoints, inv, bpartnerId, trx.getTrxName());
						}
					}

					if (!generatedOnlyShipment && ok)
					{
						invoiceId = inv.getC_Invoice_ID();
						Iterator<PaymentList> itp = payList.iterator();

						while(itp.hasNext()) {
							PaymentList pm = itp.next();

							if (pm.getTypeTerm().equals(paytermDef) || pm.getTypeTerm().equals(payInternal)) {
								BigDecimal writeOffAmt = Env.ZERO;
								BigDecimal value =  pm.getValue();
								String tender = pm.getTenderType();

								Integer payId;

								if (pm.getTypeTerm().equals(payInternal)) {
									payId = CompleteSaleWeb.createPaymentInternal(trx.getTrxName(), inv, value, tender,
											pm.getCreditCardType(),pm.getCreditCardNumber(), pm.getNewCreditCardVV(),
											pm.getCreditCardExpMM(), pm.getNewCreditCardExpYY(), pm.getName(), pm.getRoutingNo(), 
											pm.getAccountNo(), pm.getCheckNo(), pm.getMicr(), pm.getTypeTerm(), writeOffAmt, Env.ZERO);
								} else {
									payId = CompleteSaleWeb.createPayment(trx.getTrxName(), inv, order.getM_Warehouse_ID(), value, tender,
											pm.getCreditCardType(),pm.getCreditCardNumber(), pm.getNewCreditCardVV(),
											pm.getCreditCardExpMM(), pm.getNewCreditCardExpYY(), pm.getName(), pm.getRoutingNo(), 
											pm.getAccountNo(), pm.getCheckNo(), pm.getMicr(), pm.getTypeTerm(), writeOffAmt, Env.ZERO);
								}



								if (payId > 0){
									pxList.add(payId);
								}else{
									ok = false;
								}
							} else if (pm.getTypeTerm().equals(payTermPoints)) {
								continue;
							}
						}//while
					} //  if   !generatedOnlyShipment

				}
			}

			//trx.commit();
			// SMJFIX - fix reservations made by shipment with attribute set = 0
			// 
			for (int z = 0;z < prodIdToCheck.size();z++ )
			{	

				log.log(Level.INFO, prodIdToCheck.get(z)+"-"+prodWarehouseIdToCheck.get(z)+"-"+prodASIToCheck.get(z));
				MStorageReservation storageReservation = MStorageReservation.get(Env.getCtx(), prodWarehouseIdToCheck.get(z), prodIdToCheck.get(z), 
						0, order.isSOTrx(), trx.getTrxName());
				if (storageReservation != null) {
					BigDecimal prevReserv = storageReservation.getQty();
					BigDecimal qtyRecover = qtyprodToCheck.get(z);
					if (prevReserv.add(qtyRecover).intValue() == 0) {  // after compesate reserves if result = 0 delete the entry
						storageReservation.setQty(prevReserv.add(qtyRecover ) );
						storageReservation.delete(true, trx.getTrxName());   
					}
					else
					{
						storageReservation.setQty(prevReserv.add(qtyRecover ) );
						storageReservation.saveEx();
					}
				}		
				//trx.commit();

			}
			// end fix reservations 
			prodASIToCheck.clear(); 
			prodIdToCheck.clear();
			prodWarehouseIdToCheck.clear();

			tmp.deleteEx(true);
			if (flag && pok && ok){
				trx.commit(); //Final


				if (!generatedOnlyShipment){
					//ShowWindow.openSalesOrder(order.getC_Order_ID());

					Iterator<Integer> itp = pxList.iterator();

					while (itp.hasNext()){
						itp.next();
					}

					if (SMJConfig.getBooleanValue("isPosClientPrinting", true, lorgId)) {
						if (inv != null && inv.get_ID() > 0) {
							invoiceId = inv.get_ID();
							ShowWindow.openInvoiceCustomer(invoiceId);
						}
					} else {
						MUser user = new MUser(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()), null);	
						String printer = user.get_ValueAsString("smj_posdefaultprinter");

						if (printer == null || printer.isEmpty()) {
							printer = SMJConfig.getValue("DefaultPrinter", "PDFCreator", lorgId);
						} 

						ReportCtl.startDocumentPrint(ReportEngine.INVOICE, null, invoiceId, null, -1, true, printer);
					}
				} else {
					ShowWindow.openSalesOrder(order.get_ID());
				}
			}else{
				trx.rollback();
				flag = false;
				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGErrorCreateSalesOrder"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			}
		} catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".salesProcess ERROR:: ", e);
			Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGErrorCreateSalesOrder"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
			flag = false;
		}finally{
			trx.close();
			if (useTransport) {
				cleanReservation();
			}
		}

		return flag;
	}//salesProcess



	/**
	 * adiciona un elemento a lista de pagos previa validacion - 
	 * add element to pay list after validation
	 */
	private void addPay(){
		try {
			if (payList == null){
				payList = new LinkedList<PaymentList>();
			}
			PaymentList pay = new PaymentList();
			String termName = paytermListbox.getSelectedItem().getLabel();
			pay.setTermName(termName);
			Integer payTerm = (Integer) paytermListbox.getSelectedItem().getValue();

			if (payTerm <= 0){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectPayterm"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}

			pay.setTypeTerm(payTerm);
			BigDecimal money = Env.ZERO;

			if (moneyDecimalbox != null && moneyDecimalbox.getText() != null) 
				money = moneyDecimalbox.getDecimalbox().getValue();

			if (grandTotal.compareTo(Env.ZERO) == 0) {
				money = Env.ZERO;
			} else if (money.compareTo(Env.ZERO) <= 0){
				Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGValueGreaterZero"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}

			String tType = (String) payTypeListbox.getSelectedItem().getValue();
			if(tType == null ||tType.equals("") ||tType.equals("-1")){
				Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJMSGSelectTenderType"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if (payTerm.equals(paytermDef) || (payInternal != -1 && payTerm.equals(payInternal))){

				String tenderName = (String) payTypeListbox.getSelectedItem().getLabel();
				pay.setTenderType(tType);
				pay.setTenderName(tenderName);
				if (tType.equals(tenderCreditCard)){
					String CreditCardType = "";
					try {
						if (cCardTypeListbox.getSelectedItem() == null || cCardTypeListbox.getSelectedItem().getValue().equals("-1")){
							Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGSelectCreditCard"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
							cCardTypeListbox.setFocus(true);
							return;
						}else{
							CreditCardType = (String) cCardTypeListbox.getSelectedItem().getValue();
						}
					} catch (Exception e) {
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGSelectCreditCard"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						return;
					}
					pay.setCreditCardType(CreditCardType);
					String ccn = numberTextbox.getValue().trim().trim();
					if(ccn == null ||ccn.equals("") ||ccn.equals(" ") || ccn.length()!=4){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCreditCardValue"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						numberTextbox.setFocus(true);
						return;
					}
					pay.setCreditCardNumber(ccn);
					String vv = verficationTextbox.getValue().trim();
					if(useTransport && (vv == null || vv.equals("") || vv.equals(" "))){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCreditCardVerify"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						verficationTextbox.setFocus(true);
						return;
					}

					pay.setNewCreditCardVV(vv);
					Integer month = monthIntbox.getValue();
					if(useTransport && (month == null ||month <=0 || month >12)){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCreditCardMonth"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						monthIntbox.setFocus(true);
						return;
					}

					pay.setCreditCardExpMM(month);
					Integer year = yearIntbox.getValue();
					Calendar cal = Calendar.getInstance();
					Integer cy = cal.get(Calendar.YEAR);
					if(useTransport && (year == null ||year < cy)){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCreditCardYear"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						yearIntbox.setFocus(true);
						return;
					}

					Integer cm = cal.get(Calendar.MONTH)+1;
					if(useTransport && (year.equals(cy) && month < cm)){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCreditCardMonth"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						monthIntbox.setFocus(true);
						return;
					}

					pay.setNewCreditCardExpYY(year); 

					if (nameTextbox.getValue() == null || nameTextbox.getValue().trim().isEmpty()) {
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "NeedsName"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						nameTextbox.setFocus(true);
						return;
					}

					pay.setName(nameTextbox.getValue().trim());
				}//tenderCreditCard

				if (tType.equals(tenderCheck)) {

					try {
						pay.setRoutingNo(routeTextbox.getValue().trim());
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()+".addPay ERROR: pay.setRoutingNo ", e);
					}
					try {
						pay.setAccountNo(accountTextbox.getValue().toString());
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()+".addPay ERROR: pay.setAccountNo ", e);
					}

					BigDecimal ck = checkTextbox.getValue();
					if(ck == null ||ck.equals("") ||ck.equals(" ")){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGCheckValid"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						checkTextbox.setFocus(true);
						return;
					}
					pay.setCheckNo(ck.toString());
					BigDecimal micr = placeTextbox.getValue();
					if(micr == null ||micr.equals("") ||micr.equals(" ")){
						Messagebox.showDialog(Msg.getMsg(Env.getCtx(), "SMJMSGMicrValid"), labelInfo, Messagebox.OK, Messagebox.EXCLAMATION);
						placeTextbox.setFocus(true);
						return;
					}
					pay.setMicr(micr.toString());
					try {
						pay.setName(nameTextbox.getValue().trim());
					} catch (Exception e) {
						log.log(Level.SEVERE, this.getClass().getCanonicalName()+".addPay ERROR: pay.setName ", e);
					}
				}//tenderCheck	
			} else if (payTerm.equals(payTermPoints)) {
				BigDecimal points = DataQueries.getPointsByBPartnerID(bpartnerId);
				BigDecimal maxMoney = points.multiply(salesPointsFactor);
				if (money.compareTo(maxMoney.subtract(getPointsUsed())) == 1) {
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MsgInsufficientPoints"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
					moneyDecimalbox.setValue(maxMoney.subtract(getPointsUsed()));
					return;
				}
			} else { //Credit
				if (maxCredit == null) {
					//Verificar que el usuario tenga credito habilitado
					MBPartner bPartner = new MBPartner(Env.getCtx(), bpartnerId, null);

					if (bPartner.getSO_CreditLimit() == null || bPartner.getSO_CreditLimit().compareTo(Env.ZERO) <= 0) {
						Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MSGNoCredit"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
						paytermListbox.setValue(-1);
						return;
					}

					maxCredit = bPartner.getSO_CreditLimit().subtract(bPartner.getSO_CreditUsed());
				}

				if (money.compareTo(maxCredit.subtract(getCreditUsed())) == 1) {
					Messagebox.showDialog(Msg.translate(Env.getCtx(), "SMJ_MSGExceedCredit"), labelInfo,Messagebox.OK, Messagebox.EXCLAMATION);
					moneyDecimalbox.setValue(maxCredit.subtract(getCreditUsed()));
					return;
				}
			} 

			BigDecimal valuePay = money;
			BigDecimal sumData = accTotal.add(valuePay);
			if (sumData.compareTo(grandTotal)>0) {
				valuePay = grandTotal.subtract(accTotal);
			}

			pay.setValue(valuePay);
			pay.setValueReceived(money);
			payList.add(pay);
			clearPay();
			loadTable();	
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".addPay ERROR: ", e);
		}
	}//addPay

	/**
	 * Carga la informacion en la tabla 
	 * load table with info generate 
	 */
	private void loadTable(){
		try {
			Iterator<PaymentList> it = payList.iterator();
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			accTotal = Env.ZERO;
			BigDecimal recTotal = Env.ZERO;
			isAproval =  false;

			while (it.hasNext()){
				PaymentList ipx = it.next();
				Vector<Object> v = new Vector<Object>();
				v.add(ipx.getTermName());
				if (ipx.getTypeTerm().equals(paytermDef)){
					v.add(ipx.getTenderName());
				}else{
					v.add("");
				}
				// value received
				v.add(ipx.getValueReceived());
				// value charged
				v.add(ipx.getValue());

				data.add(v);
				accTotal = accTotal.add(ipx.getValue());
				recTotal = recTotal.add(ipx.getValueReceived());

				if (ipx.getTenderType()!=null && ipx.getTenderType().equals(tenderCheck) && useTransport){
					isAproval = true;
				}
			}
			residue = grandTotal.subtract(accTotal);
			if(residue.compareTo(Env.ZERO)<=0){
				confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(true);
				addButton.setEnabled(false);
			}else{
				confirmPanel.getButton(ConfirmPanel.A_OK).setEnabled(false);
				addButton.setEnabled(true);
			}

			moChange = recTotal.subtract(grandTotal);

			if (moChange.compareTo(Env.ZERO) == -1) {
				moChange = Env.ZERO;
			}

			residueDecimalbox.setValue(nf.format(residue));
			//smj1
			moneyDecimalbox.setValue(residue);
			paytermListbox.setValue(payTermId);
			if(payTermId.equals(paytermDef)){
				payTypeLabel.setVisible(true);
				payTypeListbox.setVisible(true);
				visibleFields(tenderCash);
			}else{
				payTypeLabel.setVisible(false);
				payTypeListbox.setVisible(false);
				visibleFields(TenderType);
			}
			accDecimalbox.setValue(nf.format(accTotal));
			moChangeDecimalbox.setValue(nf.format(moChange));
			treceivedDecimalBox.setValue(nf.format(recTotal));
			Vector<String> columnNames = getTableColumnNames();
			contentPanel.clear();
			contentPanel.getModel().removeTableModelListener(this);
			//  Set Model
			ListModelTable model = new ListModelTable(data);
			//			model.removeTableModelListener(this);
			model.addTableModelListener(this);
			contentPanel.setData(model, columnNames);
			contentPanel.addActionListener(this);
			setTableColumnClass(contentPanel);
			
			for (Component component : contentPanel.getListHead().getChildren()) {
				ListHeader header = (ListHeader) component;
				header.setHflex("max");
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, this.getClass().getCanonicalName()+".loadTable ERROR: ", e);
		}
	}//loadTable

	/**
	 * Regresa el listado de nombres de las columnas de la tabla
	 * return column list titles
	 * @return Vector<String>
	 */
	private Vector<String> getTableColumnNames()	{	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_PaymentTerm_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "TenderType"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelMoney"));
		columnNames.add(Msg.translate(Env.getCtx(), "SMJLabelPay"));
		return columnNames;
	}//getTableColumnNames

	/**
	 * Define si las columnas son editables para la tabla y el tipo de dato
	 * define if column is edited into the table and data type
	 * @param calendarTable
	 */
	private void setTableColumnClass(IMiniTable table)	{
		int i = 0; 
		table.setColumnClass(i++, String.class, true);   
		table.setColumnClass(i++, String.class, true);
		table.setColumnClass(i++, BigDecimal.class, true);   	  
		//  Table UI
		table.autoSize();
	}//setTableColumnClass

	/**
	 * limpia los campos de pago - clear payment fields
	 */
	private void clearPay(){
		changeDecimalBox.setValue(nf.format(Env.ZERO));
		moneyDecimalbox.setValue(Env.ZERO);
		numberTextbox.setValue("");
		verficationTextbox.setValue("");
		monthIntbox.setValue(null);
		yearIntbox.setValue(null);
		nameTextbox.setValue("");
		routeTextbox.setValue("");
		accountTextbox.setValue(new BigDecimal(0));
		placeTextbox.setValue(new BigDecimal(0));
		checkTextbox.setValue(new BigDecimal(0));
		cCardTypeListbox.setValue(-1);
		payTypeListbox.setValue(tenderCash);
	}//clearPay

	/***
	 * regresa el termino de pago segun los escenarios posibles - 
	 * returns payterm depending on the possible scenarios 
	 * @return Integer
	 * @throws InterruptedException
	 */
	private Integer getPayTerm() throws InterruptedException{
		Integer pay = payTermId;
		if (!payList.isEmpty()){
			Iterator<PaymentList> itp = payList.iterator();
			while(itp.hasNext()){
				PaymentList pm = itp.next();
				pay = pm.getTypeTerm();
			}
		}
		return pay;
	}//getPayTerm

	private BigDecimal getCreditUsed() {
		BigDecimal creditUsed = Env.ZERO;
		for (PaymentList pay : payList) {
			if (!pay.getTypeTerm().equals(paytermDef)) 
				creditUsed = creditUsed.add(pay.getValueReceived());			
		}
		return creditUsed;
	}

	/**
	 * Calculate the number of points used in the current payment
	 * @return
	 */
	private BigDecimal getPointsUsed() {
		BigDecimal cash = Env.ZERO;		
		for (PaymentList pay : payList) {
			if (!pay.getTypeTerm().equals(payTermPoints)) 
				cash = cash.add(pay.getValueReceived());			
		}		
		return cash;
	}

	@Override
	public void onPageDetached(Page page) {
		super.onPageDetached(page);
		if (m_callback != null) {
			m_callback.onCallback(retorno);
		}
	}


	public void setIdInsurance(Integer idInsurance) {
		this.idInsurance = idInsurance;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public void setCoverage(BigDecimal coverage) {
		this.coverage = coverage;
	}

	private void cleanReservation() {
		String sql = "DELETE FROM M_StorageReservation WHERE Qty <= 0";
		DB.executeUpdate(sql, null);
	}
}//SMJPaymentWindow