package com.smj.webui.component;

import java.io.BufferedInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Vbox;

/**
 * Ventana emergente que muestra la imagen del Producto
 * Popup window showing the image of Product
 * @author Dany
 *
 */
public class SMJZoomImage extends Window {

	private static final long serialVersionUID = 1L;
	private BufferedInputStream bis;
	private String productName;
	
	public SMJZoomImage(BufferedInputStream bis, String productName) {
		super();
		this.bis = bis;
		this.productName = productName;
		initComponents();
	}
	
	/**
	 * Definir los componentes de la interfaz con sus respectivos eventos
	 * Define the interface components with their respective events
	 */
	private void initComponents() {
		//Styles (CSS)
		String borderStyle = "border: 1px solid #C0C0C0; border-radius:5px;";
		String labelStyle = "font-size: 24px;color: #0B610B;background:#FFFFFF;";
		String redButtonStyle = "border-color: #cb1b15 #990100 #840300;background: linear-gradient(#c30100,#ab0100);background: -webkit-linear-gradient(#c30100,#ab0100);background-color: #bb0600;color: #fff;text-shadow: 0 -1px rgba(0,0,0,.15);";
		
		//this.setHeight("70%");
		//this.setWidth("60%");
		this.setStyle(borderStyle);
		ZKUpdateUtil.setWidth(this, "60%");
		ZKUpdateUtil.setHeight(this, "70%");
		
		
		//mainLayout.setHeight("100%");
		//mainLayout.setVflex("true");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		ZKUpdateUtil.setVflex(mainLayout, "true");
		this.appendChild(mainLayout);
		
		//contentLayout.setHeight("100%");
		//contentLayout.setWidth("100%");
		//contentLayout.setVflex("true");
		ZKUpdateUtil.setHeight(contentLayout, "100%");
		ZKUpdateUtil.setWidth(contentLayout, "60%");
		ZKUpdateUtil.setVflex(contentLayout, "true");
		mainLayout.appendChild(contentLayout);
			
		//Top of the interface 
		
		//topLayout.setHeight("12%");
		//topLayout.setWidth("100%");
		//topLayout.setHflex("true");
		ZKUpdateUtil.setHeight(topLayout, "100%");
		ZKUpdateUtil.setWidth(topLayout, "60%");
		ZKUpdateUtil.setHflex(topLayout, "true");
		topLayout.setStyle(borderStyle + "margin: 2px;");
		contentLayout.appendChild(topLayout);
		
		//titleLayout.setWidth("100%");	
		//titleLayout.setHeight("100%");
		ZKUpdateUtil.setHeight(titleLayout, "100%");
		ZKUpdateUtil.setWidth(titleLayout, "60%");
		topLayout.appendChild(titleLayout);
		
		lblTitle.setStyle(labelStyle);
		lblTitle.setText(productName);
		Cell cell = new Cell();
		cell.appendChild(lblTitle);
		cell.setAlign("right");
		cell.setValign("top");
		cell.setWidth("50%");
		cell.setStyle("padding: 5px;");
		titleLayout.appendChild(cell);
		
		btnClose.setStyle(redButtonStyle);
		cell = new Cell();
		cell.appendChild(btnClose);
		cell.setAlign("left");
		cell.setValign("top");	
		cell.setWidth("50%");
		cell.setStyle("padding: 5px;");
		titleLayout.appendChild(cell);
		
		//Bottom of the interface
		
		//bottomLayout.setHeight("78%");
		//bottomLayout.setWidth("100%");
		//bottomLayout.setVflex("true");
		//bottomLayout.setHflex("true");
		ZKUpdateUtil.setHeight(bottomLayout, "78%");
		ZKUpdateUtil.setWidth(bottomLayout, "100%");
		ZKUpdateUtil.setVflex(bottomLayout, "true");
		ZKUpdateUtil.setHflex(bottomLayout, "true");
		bottomLayout.setStyle(borderStyle + "margin: 2px");
		contentLayout.appendChild(bottomLayout);
		
		
		try {
			image.setContent(ImageIO.read(bis));	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		image.setStyle("padding: 0px;");
		cell = new Cell();
		cell.setAlign("center");
		cell.setValign("middle");
		cell.setVflex("true");
		cell.setHflex("true");
		cell.appendChild(image);	
		bottomLayout.appendChild(cell);
		
		//Events
		
		btnClose.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				btnCloseOnClick();
			}
		});
	}
	
	private void btnCloseOnClick() {
		this.dispose();
	}
	
	//Defining Components
	private Window mainLayout = new Window();
	private Window topLayout = new Window();
	private Hbox bottomLayout = new Hbox();
	
	private Vbox contentLayout = new Vbox();	
	private Hbox titleLayout = new Hbox();
	
	private Image image = new Image();
	private Label lblTitle = new Label();
	private Button btnClose = new Button(Msg.translate(Env.getCtx(), "close"));
}
