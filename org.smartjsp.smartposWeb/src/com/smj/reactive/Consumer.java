package com.smj.reactive;

public interface Consumer<T> {

	public void accept(T value) throws Exception;

}
