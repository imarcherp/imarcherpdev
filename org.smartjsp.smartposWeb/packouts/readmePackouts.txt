Pack Outs SmartPOS

1. Execution Order

Log In as System
Go to the menu Pack Out
Run the files in the following order

2Pack_smartpos3-001_1.0.0.zip
2Pack_smartpos3-002_1.0.0.zip
2Pack_smartpos3-003_1.0.0.zip
2Pack_smartpos3-004_1.0.0.zip
2Pack_smartpos3-005_1.0.0.zip
2Pack_smartpos3-006_1.0.0.zip   -> changes in close cash
2Pack_smartpos3-007_1.0.0.zip   -> changes in close cash parameter (date)
2Pack_smartpos3-008_1.0.0.zip	-> Add new messages (SMJ_MsgBankAccountUsed, SMJ_MsgMustBeLessOrEqual)
2Pack_smartpos3-009_1.0.0.zip	-> Fix problems, with UOM conversion and Discount (Change Columns in SMJ_TmpWebSalesLine)
2Pack_smartpos3-010_1.0.0.zip	-> Add new messages (SMJ_MsgBPartnerNoCreditCheck, SMJ_MsgDuplicateProduct) (2016-02-05)

2. Role Access Update

Log in as UserAdmin (GardenAdmin)
Go to menu Role Access Update
Fill out the required fields
Press OK button