ALTER TABLE smj_tmpwebsales ADD COLUMN smj_ticket character varying(30);
ALTER TABLE smj_tmpwebsales ADD COLUMN c_order_id numeric(10,0);
ALTER TABLE smj_tmpwebsales ADD COLUMN smj_tmpwebsales_uu character varying(36);
