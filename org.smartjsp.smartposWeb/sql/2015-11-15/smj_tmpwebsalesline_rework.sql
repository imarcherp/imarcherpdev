ALTER TABLE smj_tmpwebsalesline RENAME qty TO qtyordered;
ALTER TABLE smj_tmpwebsalesline RENAME price TO priceentered;
ALTER TABLE smj_tmpwebsalesline ALTER COLUMN priceentered SET DEFAULT 0;
ALTER TABLE smj_tmpwebsalesline ADD COLUMN m_pricelist_version_id numeric(10,0);
ALTER TABLE smj_tmpwebsalesline ADD COLUMN priceactual numeric DEFAULT 0;

UPDATE SMJ_TmpWebSalesLine SET M_PriceList_Version_ID = 1000011 WHERE M_PriceList_Version_ID IS NULL;