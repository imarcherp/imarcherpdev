-- Nuevo campo para Warehouse (Nueva Funcionalidad)
-- Si se registra algun valor este sera tomado para generar la factura de lo
-- contrario se tomara el que tenga por defecto
ALTER TABLE m_warehouse ADD COLUMN c_doctypetarget_id numeric(10,0);

-- Modificacion para cambiar el nombre, para que asi se registre la informacion
-- del almacen en el cierre de caja, el campo anterior no estaba siendo usado.
ALTER TABLE smj_closecash RENAME COLUMN smj_posid TO m_warehouse_id;