ALTER TABLE m_warehouse ADD COLUMN ispos character(1) NOT NULL DEFAULT 'N'::bpchar;
ALTER TABLE m_warehouse ADD COLUMN ad_role_id numeric(10,0);
ALTER TABLE m_warehouse ADD COLUMN c_bank_id numeric(10,0);
ALTER TABLE m_warehouse ADD COLUMN c_bankaccount_id numeric(10,0);