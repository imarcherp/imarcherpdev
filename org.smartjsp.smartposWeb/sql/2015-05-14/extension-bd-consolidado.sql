CREATE TABLE smj_insurancecompanytype
(
  smj_insurancecompanytype_id numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL, -- Insurance Company Type name
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_insurancecompanytype_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT pk_smj_insurancecompanytype PRIMARY KEY (smj_insurancecompanytype_id),
  CONSTRAINT adclient_smjinsurancecompanyty FOREIGN KEY (ad_client_id)
      REFERENCES ad_client (ad_client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE smj_insurancecompanytype
  OWNER TO adempiere;
COMMENT ON COLUMN smj_insurancecompanytype.name IS 'Insurance Company Type name';
COMMENT ON TABLE adempiere.smj_insuranceCompanyType IS 'Insurance Company Type List'; 

CREATE TABLE smj_insuranceplan
(
  smj_insuranceplan_id numeric(10,0) NOT NULL default 0, -- smj_insurancePlan table primary key
  name character varying(60) NOT NULL, -- Insurance Plan name
  c_bpartner_id numeric(10,0) NOT NULL,
  copay_percentage numeric, -- coPay percentage
  copay_value numeric(10,2), -- coPay value
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_insuranceplan_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT pk_smj_insuranceplan PRIMARY KEY (smj_insuranceplan_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_insuranceplan
  OWNER TO adempiere;
COMMENT ON TABLE smj_insuranceplan
  IS 'Insurance Plan';
COMMENT ON COLUMN smj_insuranceplan.smj_insuranceplan_id IS 'smj_insurancePlan table primary key';
COMMENT ON COLUMN smj_insuranceplan.name IS 'Insurance Plan name';
COMMENT ON COLUMN smj_insuranceplan.copay_percentage IS 'coPay percentage';
COMMENT ON COLUMN smj_insuranceplan.copay_value IS 'coPay value';


CREATE TABLE smj_tmppayment
(
  smj_tmppayment_id numeric(10,0) NOT NULL DEFAULT 0,
  c_order_id numeric(10,0) NOT NULL,
  total numeric NOT NULL,
  c_paymentterm_id numeric(10,0),
  tendertype character(1) NOT NULL,
  cctype character varying(10),
  ccnumber character varying(50),
  ccverfication character varying(10),
  ccexpmm numeric(10,0),
  ccexpyy numeric(10,0),
  name character varying(60),
  checkno character varying(20),
  routingno character varying(20),
  accountno character varying(20),
  micr character varying(20),
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_tmppayment_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT smj_tmppayment_pk PRIMARY KEY (smj_tmppayment_id),
  CONSTRAINT smj_tmppayment_uu_idx UNIQUE (smj_tmppayment_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_tmppayment
  OWNER TO adempiere;
COMMENT ON TABLE smj_tmppayment
  IS 'temporal table for order payments';


-- DROP TABLE smj_tmpwebsales;

CREATE TABLE smj_tmpwebsales
(
  smj_tmpwebsales_id numeric(10,0) NOT NULL, -- Id
  c_bpartner_id numeric(10,0) NOT NULL,
  c_bpartner_location_id numeric(10,0) NOT NULL,
  smj_plate character varying(10),
  c_paymentterm_id numeric(10,0),
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_workorder character varying(60),
  smj_ticket character varying(30),
  c_order_id numeric(10,0),
  smj_tmpwebsales_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT smj_tmpwebsales_pk PRIMARY KEY (smj_tmpwebsales_id),
  CONSTRAINT smj_tmpwebsales_uu_idx UNIQUE (smj_tmpwebsales_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_tmpwebsales
  OWNER TO adempiere;
COMMENT ON TABLE smj_tmpwebsales
  IS 'Temporary ticket for POS WEB';
COMMENT ON COLUMN smj_tmpwebsales.smj_tmpwebsales_id IS 'Id';


-- DROP TABLE smj_tmpwebsalesline;

CREATE TABLE smj_tmpwebsalesline
(
  smj_tmpwebsalesline_id numeric(10,0) NOT NULL,
  smj_tmpwebsales_id numeric(10,0) NOT NULL,
  m_product_id numeric(10,0),
  productname character varying(60),
  productvalue character varying(60),
  c_bpartner_id numeric(10,0),
  partnername character varying(60),
  price numeric,
  qty numeric,
  m_warehouse_id numeric(10,0),
  warehousename character varying(60),
  m_locator_id numeric(10,0),
  qtyentered numeric,
  total numeric,
  c_uom_id numeric(10,0),
  uomname character varying(60),
  tax numeric,
  m_attributesetinstance_id numeric(10,0),
  smj_plate character varying(10),
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  purchaseprice numeric,
  r_request_id numeric(10,0),
  smj_iswarranty character varying(1) NOT NULL DEFAULT 'N'::character varying, -- is a warranty
  smj_iswarrantyapplied character varying(1) NOT NULL DEFAULT 'N'::character varying,
  smj_isworkorder character varying(1) NOT NULL DEFAULT 'N'::character varying, -- belongs to a work order
  line numeric(10,0) DEFAULT NULL::numeric,
  smj_destination character varying(60),
  smj_notes character varying(100),
  smj_discount numeric DEFAULT 0,
  smj_percentage character(1) DEFAULT 'N'::bpchar,
  c_orderline_id numeric(10,0),
  smj_tmpwebsalesline_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT smj_tmpwebsalesline_pk PRIMARY KEY (smj_tmpwebsalesline_id),
  CONSTRAINT smj_tmpwebsales_line_fk FOREIGN KEY (smj_tmpwebsales_id)
      REFERENCES smj_tmpwebsales (smj_tmpwebsales_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT smj_tmpwebsalesline_uu_idx UNIQUE (smj_tmpwebsalesline_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_tmpwebsalesline
  OWNER TO adempiere;
COMMENT ON TABLE smj_tmpwebsalesline
  IS 'Lines of temporary ticket for POS  WEB';
COMMENT ON COLUMN smj_tmpwebsalesline.smj_iswarranty IS 'is a warranty';
COMMENT ON COLUMN smj_tmpwebsalesline.smj_isworkorder IS 'belongs to a work order';

-- soporte a descuentos

ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_discount numeric;
ALTER TABLE smj_tmpwebsalesline ALTER COLUMN smj_discount SET DEFAULT 0;

ALTER TABLE smj_tmpwebsalesline ADD COLUMN smj_percentage character(1);
ALTER TABLE smj_tmpwebsalesline ALTER COLUMN smj_percentage SET DEFAULT 'N'::bpchar;


-- DROP TABLE t_smj_storage;

CREATE TABLE t_smj_storage
(
  t_smj_storage_id serial NOT NULL,
  m_warehouse_id numeric(10,0) NOT NULL,
  m_product_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT now(),
  updated timestamp without time zone NOT NULL DEFAULT now(),
  qtyonhand numeric NOT NULL DEFAULT 0,
  warehousename text,
  product text,
  qty numeric NOT NULL,
  t_smj_storage_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT t_smj_storage_pkey PRIMARY KEY (t_smj_storage_id),
  CONSTRAINT t_smj_storage_uu_idx UNIQUE (t_smj_storage_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE t_smj_storage
  OWNER TO adempiere;


  
-- campos de tabla de terceros

ALTER TABLE adempiere.C_BPartner ADD smj_isinsuranceCompany character(1) DEFAULT 'N'::bpchar;
COMMENT ON COLUMN adempiere.C_BPartner.smj_isinsuranceCompany IS 'is insurance company';
ALTER TABLE adempiere.C_BPartner ADD smj_amountDue_debt NUMERIC(10,2) ;
COMMENT ON COLUMN adempiere.C_BPartner.smj_amountDue_debt IS 'amount due from the insurance company (debt)';
ALTER TABLE adempiere.C_BPartner ADD smj_nationalIDNumber  VARCHAR(60);
COMMENT ON COLUMN adempiere.C_BPartner.smj_nationalIDNumber IS 'Customer national ID number';
ALTER TABLE adempiere.C_BPartner ADD smj_insuranceCompanyType_ID NUMERIC(10) NOT NULL DEFAULT 0;
COMMENT ON COLUMN adempiere.C_BPartner.smj_insuranceCompanyType_ID IS 'insurance company Type';
ALTER TABLE adempiere.C_BPartner ADD smj_insurancePlan_ID NUMERIC(10) NOT NULL  default 0;
COMMENT ON COLUMN adempiere.C_BPartner.smj_insurancePlan_ID IS 'insurance company Plan customer';
ALTER TABLE adempiere.C_BPartner ADD smj_insuranceCompany_ID NUMERIC(10) NOT NULL default 0;
COMMENT ON COLUMN adempiere.C_BPartner.smj_insuranceCompany_ID IS 'insurance company';


--ALTER TABLE adempiere.C_BPartner ADD CONSTRAINT C_BPartner_insuranceCompanyType_fk
--FOREIGN KEY (smj_insuranceCompanyType_ID) REFERENCES adempiere.smj_insuranceCompanyType (smj_insuranceCompanyType_ID);

--ALTER TABLE adempiere.C_BPartner ADD CONSTRAINT C_BPartner_smj_insurancePlan_fk
--FOREIGN KEY (smj_insurancePlan_ID) REFERENCES adempiere.smj_insurancePlan (smj_insurancePlan_ID);

CREATE TABLE i_smj_medicine
(
  i_smj_medicine_id numeric(10,0) NOT NULL,
  code_cip character varying(12),
  code_pharma_ml character varying(25),
  name character varying(60),
  manufacturer character varying(60),
  category character varying(60),
  purchaseprice numeric(10,0),
  sellingprice numeric(10,0),
  salestax character varying(60),
  schedule character varying(15),
  i_isimported character(1),
  i_errormsg character varying(2000),
  processed character(1) DEFAULT 'N'::bpchar,
  processing character(1),
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  qty numeric(10,0),
  m_locator_value character varying(40) DEFAULT NULL::character varying,
  uom_name text,
  level_min numeric NOT NULL DEFAULT 0,
  smj_active character(1) NOT NULL DEFAULT 'Y'::bpchar,
  i_smj_medicine_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT pk_i_smj_medicine PRIMARY KEY (i_smj_medicine_id),
  CONSTRAINT i_smj_medicine_uu_idx UNIQUE (i_smj_medicine_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE i_smj_medicine
  OWNER TO adempiere;
COMMENT ON TABLE i_smj_medicine
  IS 'carga masiva de productos medicamentos';
  
  -- Agrega campos a tabla warehouse

ALTER TABLE m_warehouse ADD COLUMN m_pricelist_id numeric(10,0) NOT NULL DEFAULT 0;
ALTER TABLE m_warehouse ADD COLUMN m_pricelist_version_id numeric(10,0) NOT NULL DEFAULT 0;

 -- agrega campos a tabla de ordenes y lineas de ordenes
 
ALTER TABLE c_orderline ADD COLUMN smj_locator_id numeric(10,0) NOT NULL DEFAULT 0;
ALTER TABLE C_Order ADD smj_tmpwebsales_id character varying(10) NOT NULL DEFAULT 0;
COMMENT ON COLUMN adempiere.C_Order.smj_tmpwebsales_id IS 'POS temp ticket  id';


-- agregar campos a tabla de productos
ALTER TABLE m_product ADD COLUMN smjcovered character(1) NOT NULL DEFAULT 'N'::bpchar;
ALTER TABLE m_product ADD COLUMN smjdiscount_allowed character(1) NOT NULL DEFAULT 'N'::bpchar;;


CREATE TABLE smj_closecash
(
  smj_sequence numeric(8,0) NOT NULL,
  smj_closecash_id numeric(10,0) NOT NULL,
  smj_posid numeric(10,0) NOT NULL,
  smj_totaltickets numeric(10,0) NOT NULL,
  smj_startdate timestamp without time zone NOT NULL DEFAULT now(),
  smj_enddate timestamp without time zone NOT NULL DEFAULT now(),
  smj_total numeric(16,2) NOT NULL,
  smj_totalreturns numeric(16,2) NOT NULL,
  smj_initial numeric(16,2) NOT NULL,
  smj_final numeric(16,2) NOT NULL,
  smj_totalin numeric(16,2) NOT NULL,
  smj_totalout numeric(16,2) NOT NULL,
  smj_totaldiscounts numeric(16,2) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_closecash_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT smj_closecash_pk PRIMARY KEY (smj_closecash_id),
  CONSTRAINT smj_closecash_uu_idx UNIQUE (smj_closecash_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_closecash
  OWNER TO adempiere;
COMMENT ON TABLE smj_closecash
  IS 'temporal table for close cash';
  
CREATE TABLE smj_closecashline
(
  smj_sequence numeric(8,0) NOT NULL,
  smj_concept character varying(60) NOT NULL,
  smj_unit character varying(20),
  smj_units numeric(6,0),
  smj_totalline numeric(16,2),
  smj_closecash_id numeric(10,0) NOT NULL,
  smj_closecashline_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  smj_closecashline_uu character varying(36) DEFAULT NULL::character varying,
  CONSTRAINT smj_closecash_line_pk PRIMARY KEY (smj_closecashline_id),
  CONSTRAINT smj_closecash_line_fk FOREIGN KEY (smj_closecash_id)
      REFERENCES smj_closecash (smj_closecash_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT smj_closecashline_uu_idx UNIQUE (smj_closecashline_uu)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE smj_closecashline
  OWNER TO adempiere;
COMMENT ON TABLE smj_closecashline
  IS 'temporal table for close cash lines';
  
