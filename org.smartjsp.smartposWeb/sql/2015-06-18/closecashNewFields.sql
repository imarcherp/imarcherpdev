-- Additional fields to track documentno for invoices (inicial and final included within the close cash)
ALTER TABLE smj_closecash ADD COLUMN docno_initial character varying(30);
ALTER TABLE smj_closecash ADD COLUMN docno_final character varying(30);