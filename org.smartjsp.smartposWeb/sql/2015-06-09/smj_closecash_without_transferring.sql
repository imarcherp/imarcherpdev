CREATE OR REPLACE VIEW smj_closecash_without_transferring_v AS 
SELECT 
AD_Client_ID,
AD_Org_ID,
SMJ_CloseCash_ID,
SMJ_Sequence, 
M_Warehouse_ID, 
SMJ_StartDate, 
SMJ_EndDate, 
SMJ_Total,
(SMJ_Total - (SELECT COALESCE(SUM(p.payamt),0) FROM C_Payment p WHERE p.SMJ_CloseCash_ID = SMJ_CloseCash.SMJ_CloseCash_ID AND p.IsDeposit = 'Y')) AS Diference,
NULL AS DocumentNo,
(SELECT COALESCE(SUM(p.payamt),0) FROM C_Payment p WHERE p.SMJ_CloseCash_ID = SMJ_CloseCash.SMJ_CloseCash_ID AND p.IsDeposit = 'Y') AS PayAmt
FROM SMJ_CloseCash
WHERE (SELECT COALESCE(SUM(p.payamt),0) FROM C_Payment p WHERE p.SMJ_CloseCash_ID = SMJ_CloseCash.SMJ_CloseCash_ID AND p.IsDeposit = 'Y') < SMJ_Total AND SMJ_Sequence > 0

UNION

SELECT 
AD_Client_ID,
AD_Org_ID,
SMJ_CloseCash_ID,
NULL AS SMJ_Sequence, 
NULL AS M_Warehouse_ID, 
NULL AS SMJ_StartDate, 
NULL AS SMJ_EndDate, 
NULL AS SMJ_Total, 
NULL AS Diference,
DocumentNo, 
PayAmt
FROM C_Payment 
WHERE IsDeposit = 'Y' AND SMJ_CloseCash_ID IN 
(SELECT SMJ_CloseCash_ID FROM SMJ_CloseCash WHERE (SELECT COALESCE(SUM(p.payamt),0) FROM C_Payment p WHERE p.SMJ_CloseCash_ID = SMJ_CloseCash.SMJ_CloseCash_ID AND p.IsDeposit = 'Y') < SMJ_Total);

ALTER TABLE smj_closecash_without_transferring_v
  OWNER TO adempiere;