-- C_Payment Fields
ALTER TABLE c_payment ADD COLUMN isdeposit character(1) NOT NULL DEFAULT 'N'::bpchar;
ALTER TABLE c_payment ADD COLUMN smj_closecash_id numeric(10,0);