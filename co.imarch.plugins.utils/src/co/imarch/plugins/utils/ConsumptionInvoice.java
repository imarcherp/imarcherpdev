package co.imarch.plugins.utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.globalqss.model.LCO_MInvoice;
import org.globalqss.model.MLCOInvoiceWithholding;

public class ConsumptionInvoice extends SvrProcess{
	
	private int t_C_Invoice_ID=0;
	private Integer withholdingCalc = Integer.parseInt(MSysConfig.getValue("iMarch_WHCalcConsumptionInvoice","0",Env.getAD_Client_ID(Env.getCtx())).trim());
	
	
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] workorderid = getParameter(); 
		String nameColum = "";
		if(withholdingCalc==0)
			log.log(Level.SEVERE, "Variable - iMarch_WHRuleConsumptionInvoice: ", "NOT Config");
		else {
			for (int i=0; i < workorderid.length; i++)
			{
				nameColum = workorderid[i].getParameterName();
				if (workorderid[i].getParameter() == null)
					;
				else if (nameColum.equals("C_Invoice_ID")) 
					t_C_Invoice_ID= workorderid[i].getParameterAsInt();
				else
					log.log(Level.SEVERE, "prepare - Unknown Parameter: ", nameColum);
			}
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		
		MInvoice tInvioce = new MInvoice(getCtx(), t_C_Invoice_ID, get_TrxName());
		
		//Obtine los montos de los impuestos
		List<Object> taxInvoice = getTaxInvoice(t_C_Invoice_ID);
		BigDecimal taxamt = (BigDecimal)taxInvoice.get(0);
		BigDecimal base = (BigDecimal)taxInvoice.get(1);
		
		List<List<Object>> invoicelineID = getInvoiceLineID(t_C_Invoice_ID);
		
		//Elimina cantidades de las lineas
		for(int i=0;i<invoicelineID.size();i++) {
			int invoiceline_ID = Integer.parseInt(invoicelineID.get(i).get(0).toString());
			MInvoiceLine mInvoiceLineID = new MInvoiceLine(getCtx(), invoiceline_ID ,get_TrxName());
			mInvoiceLineID.setPriceEntered(BigDecimal.ZERO);
			mInvoiceLineID.setPriceActual(BigDecimal.ZERO);
			mInvoiceLineID.setLineNetAmt();
			mInvoiceLineID.saveEx();
		}
		tInvioce.calculateTaxTotal();
		tInvioce.saveEx();
		
		int cnt = 0;

		LCO_MInvoice inv = new LCO_MInvoice(getCtx(), t_C_Invoice_ID, get_TrxName());
		if (inv.getC_Invoice_ID() == 0)
			throw new AdempiereUserError("@No@ @Invoice@");

		cnt = inv.recalcWithholdings();

		if (cnt == -1)
			throw new AdempiereUserError("Error calculating withholding, please check log");
		
		if(taxamt.compareTo(BigDecimal.ZERO)==0) 
			;
		else
		{
		
			MLCOInvoiceWithholding iwh = new MLCOInvoiceWithholding(getCtx(), 0, get_TrxName());
			iwh.setAD_Org_ID(tInvioce.getAD_Org_ID());
			iwh.setC_Invoice_ID(tInvioce.getC_Invoice_ID());
			iwh.setDateAcct(tInvioce.getDateAcct());
			iwh.setDateTrx(tInvioce.getDateInvoiced());
			iwh.setIsCalcOnPayment( false );
			iwh.setIsTaxIncluded(false);
			
			List<Object> withholdingData = getWithholdingData(withholdingCalc);
			int withholdingType_ID = Integer.parseInt(withholdingData.get(0).toString().trim());
			int tax_ID = Integer.parseInt(withholdingData.get(1).toString().trim());
			BigDecimal rate = (BigDecimal)withholdingData.get(2);
			
			iwh.setLCO_WithholdingType_ID(withholdingType_ID);
			iwh.setC_Tax_ID(tax_ID);
			iwh.setPercent(rate);
			iwh.setProcessed(false);
			
			
			taxamt = taxamt.multiply(new BigDecimal("-1"));
			iwh.setTaxAmt(taxamt);
			iwh.setTaxBaseAmt(base);
			iwh.setIsActive(true);
			iwh.saveEx();
			
			log.info("LCO_InvoiceWithholding saved:"+iwh.getTaxAmt());
		}
		
		return "@OK@";
	}
	
	public List<List<Object>> getInvoiceLineID( int recordID ){
		List<List<Object>>  result = new Vector<List<Object>>();
		String sql = "";
		
		sql = "SELECT C_InvoiceLine_ID FROM C_InvoiceLine  WHERE qtyinvoiced!=0 AND C_Invoice_ID=?";
		result = DB.getSQLArrayObjectsEx(null, sql, recordID);
		
		return result;
	}
	
	public List<Object> getTaxInvoice(int recordID){
		List<Object> result = new Vector<Object>();
		String sql = "";
		
		sql = "SELECT COALESCE(SUM(taxamt),0), COALESCE(SUM(taxbaseamt),0) FROM c_invoicetax WHERE taxamt!=0 AND  c_invoice_id=?";
		result = DB.getSQLValueObjectsEx(null, sql, recordID);
		
		return result;
	}
	
	public List<Object> getWithholdingData(int recordID){
		List<Object> result = new Vector<Object>();
		String sql = "";
		
		sql = "SELECT LCO_WithholdingType_ID, w.C_Tax_ID, rate FROM LCO_WithholdingCalc w LEFT JOIN C_Tax t ON t.C_Tax_ID=w.C_Tax_ID WHERE LCO_WithholdingCalc_ID=?";
		result = DB.getSQLValueObjectsEx(null, sql, recordID);
		
		return result;
	}

}
