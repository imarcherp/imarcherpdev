package co.imarch.plugins.utils;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.compiere.model.MBPartner;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.smj.model.MSMJVehicle;
import com.smj.model.MSMJWoMechanicAssigned;
import com.smj.model.MSMJWorkOrderLine;





public class LoadWorkOrderBPartner extends SvrProcess {
	

	private int t_AD_Org_ID = 0;
	private int t_C_BPartner_ID = 0;
	private int t_AD_User_ID = 0;
	private String file = "";
	private StringBuffer msjErrorValid = new StringBuffer("");
	private Integer requestType = Integer.parseInt(MSysConfig.getValue("SMJ-REQUESTTYPE", Env.getAD_Client_ID(Env.getCtx())).trim());
	private Integer defaultStateWO = 1000014; //Estado ReAbierta
	private Integer R_Status_ID = 1000003;	//Estado Cerrada

	@Override
	protected void prepare() {


		t_AD_User_ID = Env.getAD_User_ID(Env.getCtx());

		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("file")){
				file = (String)para[i].getParameter();
			}
			else if (name.equals("C_BPartner_ID")){
				t_C_BPartner_ID = para[i].getParameterAsInt();
			}
			else if (name.equals("AD_Org_ID")){
				t_AD_Org_ID = para[i].getParameterAsInt();
			}
		}
			
	}

	@Override
	protected String doIt() throws Exception {
		
		Trx trx = Trx.get(get_TrxName(), true);
		int numberWO = 0;
		CsvListReader cReader = null;
		cReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);   // delimitador es ; y " para textos
		
		String newWorkOrder = "";
		MRequest workOrder = null;
		MSMJVehicle vh = null;
		String trxName = trx.getTrxName();
		
		final CellProcessor[] processors = getProcesador();
		List<Object> camposBase;
		
		Boolean noValid = validateFieldID(processors, cReader);
		
		if(noValid) {
			cReader.close();
			return msjErrorValid.toString();
		}
		
		cReader.close();
		cReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);   // delimitador es ; y " para textos
		
		while( (camposBase = cReader.read(processors)) != null ) {
			Object[] campos = camposBase.toArray();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			String documenNo = campos[0].toString().trim();
			Object plate = campos[1].toString().trim();
			int vehicleId = getVehicle(plate);
			int bpartnerOwnId = getVehicleOwnerId(plate);
			
			String descriptionOrder = campos[2].toString().trim();
			
			String  t_startDate= campos[3].toString().trim();
			Date td_startDate = dateFormat.parse(t_startDate);   
			Timestamp startDate = new Timestamp(td_startDate.getTime());
			
			String  t_closeDate = campos[4].toString().trim();
			Date td_closeDate = dateFormat.parse(t_closeDate);   
			Timestamp closeDate = new Timestamp(td_closeDate.getTime());
			
			Object codeActivity= campos[5].toString().trim();
			int activityID = getActivity(codeActivity);
			
			String  qtyActivity= campos[6].toString().trim();
			String  priceActivity= campos[7].toString().trim();
			String  workDescription= campos[8].toString().trim();
			
			Object  idMechanic= campos[9].toString().trim();
			int mechanicID = 0;
			if(idMechanic.toString().length()<=0);
			else mechanicID = getMechanic(idMechanic);			
			
			//Crea la Orden
			if(!newWorkOrder.equalsIgnoreCase(documenNo)) 
			{
				workOrder = new MRequest(getCtx(), 0, trxName);
				workOrder.setR_RequestType_ID(requestType);
				workOrder.setAD_User_ID(t_AD_User_ID);
				workOrder.setAD_Org_ID(t_AD_Org_ID);
				workOrder.setC_BPartner_ID(t_C_BPartner_ID);
				workOrder.setDocumentNo("MC"+documenNo);
				workOrder.set_ValueOfColumn("smj_sequence", "MC"+documenNo);
				vh = new MSMJVehicle(Env.getCtx(), vehicleId, trxName);
				workOrder.set_ValueOfColumn("smj_plate", vh.getsmj_plate() );
				workOrder.set_ValueOfColumn("smj_internalnumber", vh.getsmj_internalnumber());
				workOrder.set_ValueOfColumn("smj_vehicle_ID", vh.getSMJ_Vehicle_ID());
				workOrder.set_ValueOfColumn("c_bpartnercustomer_ID", bpartnerOwnId);
				workOrder.setSummary(descriptionOrder);
				//workOrder.set_ValueOfColumn("created", startDate);
				workOrder.setStartDate(startDate);
				workOrder.setCloseDate(closeDate);
				workOrder.setR_Status_ID(R_Status_ID);
				workOrder.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
				workOrder.setConfidentialTypeEntry("I");
				workOrder.setConfidentialType("I");
				
				workOrder.saveEx();
				numberWO++;
				newWorkOrder=documenNo;
			}
			
			int line = createWorkOrderLine(trxName, workOrder,activityID,qtyActivity,priceActivity,workDescription,mechanicID);
		
			if(line <= 0) {
				trx.rollback();
			}
			
		}//end while
		cReader.close();
		
		return "Ordenes creadas:"+numberWO;
	}
	
	private int createWorkOrderLine(String trxName, MRequest workOrder, int activityID, String qtyActivity, 
			String priceActivity, String workDescription, int mechanicID){
		
		int woLineID = 0;
		BigDecimal qty = new BigDecimal(qtyActivity);
		BigDecimal price = new BigDecimal(priceActivity);
		
		MSMJWorkOrderLine ipl = new MSMJWorkOrderLine(Env.getCtx(), 0, trxName); 
		ipl.setM_Product_ID(activityID);
		ipl.setPrice(price);
		ipl.setQty(qty);
		ipl.setsmj_isallowchanges(false);
		ipl.settaxvalue(new BigDecimal("19"));
		ipl.settotal(qty.multiply(price));
		ipl.setwolstate("C");
		ipl.setdateopen(workOrder.getStartDate());
		ipl.setR_Request_ID(workOrder.getR_Request_ID());
		ipl.setsmj_isservice(true);
		ipl.setobservations(workDescription);
		ipl.saveEx();
		
		if(mechanicID==0) {
			workOrder.setCloseDate(null);
			workOrder.setR_Status_ID(defaultStateWO);
			workOrder.saveEx();
			ipl.setwolstate("S");	
			ipl.saveEx();
		}
		else
		{
			MBPartner mechanic = new MBPartner(Env.getCtx(), mechanicID, trxName);
			MSMJWoMechanicAssigned mechanicAsig = new MSMJWoMechanicAssigned(Env.getCtx(), 0, trxName);
			mechanicAsig.setsmj_workOrderLine_ID(ipl.get_ID());
			mechanicAsig.setC_BPartner_ID(mechanic.get_ID());
			mechanicAsig.setsmj_iscommission(mechanic.get_ValueAsBoolean("smj_iscommission"));
			mechanicAsig.setAD_Org_ID(ipl.getAD_Org_ID());
			mechanicAsig.setappliedfee(Env.ZERO);
			
			mechanicAsig.saveEx();
		}
		
		woLineID = ipl.getsmj_workOrderLine_ID();
		
		return woLineID;
	}
	
	private Boolean validateFieldID(CellProcessor[] processors, CsvListReader cReader) throws IOException 
	{
		List<Object> camposBase;
		Boolean result = false;
		
		while( (camposBase = cReader.read(processors)) != null ) 
		{
			Object[] campos = camposBase.toArray();
			
			//Verifica si el vehiculo existe
			if(getVehicle(campos[1].toString().trim())<=0 ) { 	
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" Placa="+campos[1].toString().trim());
				result = true;
			}
			//Verifica si propietario del vehiculo es del tercero a facturar
			int vehicleOwner = getVehicleOwnerId(campos[1].toString().trim());
			if(vehicleOwner <= 0 || vehicleOwner != t_C_BPartner_ID) { 	
				if(msjErrorValid.length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" Propietario Placa="+campos[1].toString().trim());
				result = true;
			}
			//Verifica si el servicio existe
			if(getActivity(campos[5].toString().trim())<=0 ) {
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" Actividad="+campos[5].toString().trim());
				result = true;
			}
			//Verifica si el mecanico existe
			if(getMechanic(campos[9].toString().trim())<0 ) {
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" MecanicoNo="+campos[9].toString().trim());
				result = true;
			}
			//Verifica fecha Apertura
			if(!validateDate(campos[3].toString().trim(), false))
			{
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" FechaApertura="+campos[3].toString().trim());
				result = true;
			}
			//Verifica fecha Cierre
			if(!validateDate(campos[4].toString().trim(), true))
			{
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Valores con inconsistencia: ");
				msjErrorValid.append(" FechaCierre="+campos[4].toString().trim());
				result = true;
			}
		}
		
		return result;
	}
	
	private int getVehicle(Object plate) {
		int result = DB.getSQLValue(null, "SELECT COALESCE(smj_vehicle_id,0) FROM smj_vehicle WHERE smj_plate=?", plate);
		return result;
	}
	
	private int getActivity(Object codeActivity) {
		int result = DB.getSQLValue(null, "SELECT COALESCE(m_product_id,0) FROM m_product WHERE value=?", codeActivity);
		return result;
	}
	
	private int getMechanic(Object idMechanic) {
		int result = 0;

		if(idMechanic.toString().length()<=0 || idMechanic=="\"\"")
			result=0;
		else 
		 result = DB.getSQLValue(null, "SELECT COALESCE(c_bpartner_id,0) FROM c_bpartner WHERE taxid=? AND smj_ismechanic='Y'", idMechanic);
		return result;
	}
	
	private int getVehicleOwnerId(Object plate) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT COALESCE(C_BPartner_ID,0) FROM smj_vehicleOwner o , smj_vehicle v WHERE o.isactive = 'Y' ");
		sql.append(" AND v.smj_vehicle_id = o.smj_vehicle_id AND  o.enddate IS  NULL AND smj_plate =? ");
		sql.append(" AND relationship = '1' ");
		
		int result = DB.getSQLValue(null, sql.toString(), plate);
		return result;
	}
	
	private static CellProcessor[] getProcesador() {
		// 10 COLUMNAS
		final CellProcessor[] processors = new CellProcessor[] {  
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""), 
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""), 
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\"")
		};		


		return processors;
	}

	
	private Boolean validateDate(String date, Boolean isfchClose) {
		

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date datenow = new Date();
		Date inputDate = new Date();
		Calendar fechaMinOpen = Calendar.getInstance();
		Calendar fechaMinClose = Calendar.getInstance();
		Calendar fechaMax = Calendar.getInstance();
		Calendar fecha = Calendar.getInstance();
		
		try {
			inputDate = dateFormat.parse(date);
			datenow = dateFormat.parse(dateFormat.format(datenow));
			fechaMinOpen.setTime(datenow);
			fechaMinOpen.add(Calendar.MONTH, -4);
			fechaMinClose.setTime(datenow);
			fechaMinClose.add(Calendar.DAY_OF_MONTH, -5);
			fechaMax.setTime(datenow);
			fecha.setTime(inputDate);	
			
			if(isfchClose) 
			{
				if(fecha.compareTo(fechaMax)==1 || fecha.compareTo(fechaMinClose)==-1)
					return false;
			}
			else
			{
				if(fecha.compareTo(fechaMax)==1 || fecha.compareTo(fechaMinOpen)==-1)
					return false;
			}
					
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
		
		
		return true;
	}
	
}
