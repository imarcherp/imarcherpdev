package co.imarch.plugins.utils;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MClient;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.EMail;

public class ExpiredSoatNotice extends SvrProcess{
	

	private int qty_vehicles = 0;
	private int qty_gruas = 0;
	private int qty_buses = 0;
	private int qty_fidelity = 0;
	
	private StringBuilder listBuses = new StringBuilder();
	private StringBuilder listGruas = new StringBuilder();
	private StringBuilder listOtros = new StringBuilder();
	private StringBuilder listFidelity = new StringBuilder();
	
	private MClient			m_client = null;
	private MUser			m_fromGruas = null;
	private MUser			m_fromBuses = null;
	private MUser			m_fromOtros = null;
	private MUser			m_fromFidelity = null;
	
	private int userGruas = MSysConfig.getIntValue("iMarch_UserSoatGruas", 0);
	private int userBuses = MSysConfig.getIntValue("iMarch_UserSoatBuses", 0);
	private int userOtros = MSysConfig.getIntValue("iMarch_UserSoatOtros", 0);
	private int userFidelity = MSysConfig.getIntValue("iMarch_UserSoatFidelity", 0);
	

	@Override
	protected void prepare() {
		
		userGruas = MSysConfig.getIntValue("iMarch_UserSoatGruas", 0);
		userBuses = MSysConfig.getIntValue("iMarch_UserSoatBuses", 0);
		userOtros = MSysConfig.getIntValue("iMarch_UserSoatOtros", 0);
		userFidelity = MSysConfig.getIntValue("iMarch_UserSoatFidelity", 0);
		
		m_client = MClient.get (getCtx());
		m_fromGruas = new MUser (getCtx(), userGruas, null);
		m_fromBuses = new MUser (getCtx(), userBuses, null);
		m_fromOtros = new MUser (getCtx(), userOtros, null);
		m_fromFidelity = new MUser (getCtx(), userFidelity, null);
		
	}

	@Override
	protected String doIt() throws Exception {
		
		String doit="";
		listGruas = new StringBuilder();
		listBuses = new StringBuilder();
		listOtros = new StringBuilder();
		listFidelity = new StringBuilder();
		String listas = expirateSoatSQL();

		
		if (listas == null || listas.length() == 0)
			doit = "SOAT al dia";
		else {
			
			if (m_client.getSMTPHost() == null || m_client.getSMTPHost().length() == 0)
				return "No SMTP Host found";
			//Envia informacion a Usuario de Fidelizacion
			if (listFidelity == null || listFidelity.length() == 0)
				;
			else
				doit ="Total Vehiculos Fidelizados:" + qty_fidelity + "  " 
						+ enviarEmail(m_client, m_fromFidelity, listFidelity.toString(), " Fidelizados" ) + " - ";
			
			//Envia informacion a Usuario de Gruas
			if (listGruas == null || listGruas.length() == 0)
				;
			else
				doit ="Total Gruas:" + qty_gruas + "  " 
						+ enviarEmail(m_client, m_fromGruas, listGruas.toString(), " Gruas") + " - ";
			
			//Envia informacion a Usuario de Buses
			if (listBuses == null || listBuses.length() == 0)
				;
			else
				doit = doit + "Total Buses:" + qty_buses + "  " 
						+ enviarEmail(m_client, m_fromBuses, listBuses.toString(), " Buses Afiliados") + " - ";
			
			//Envia informacion a Usuario Otros Vehiculos
			if (listOtros == null || listOtros.length() == 0)
				;
			else
				doit = doit + "Total Vehiculos:" + qty_vehicles + "  " 
						+ enviarEmail(m_client, m_fromOtros, listOtros.toString(), " Trac/Otros" ) + " - ";
			
		}
		if (log.isLoggable(Level.FINE)) log.fine(doit);
		return doit;
	}

	private String enviarEmail(MClient m_client, MUser to, String message, String Asunto) {
		
		if(to.get_ID() == 0)
			return " - No hay Usuarios configurados para enviar informacion";
		EMail email = m_client.createEMail(null, to, "Vencimiento SOATs"+Asunto, message);
		if (!email.isValid() && !email.isValid(true))
			return "Invalid EMail";
		boolean OK = EMail.SENT_OK.equals(email.send());
		if (OK) {
			if (log.isLoggable(Level.FINE)) log.fine(to.getEMail());
			return "=> Informacion enviada a " + to.getName() + ".  "; 
		} else {
			log.warning("FAILURE - " + to.getEMail());
			return "Error al enviar informacion a " + to.getEMail();
		}
	
	}
	
	public String expirateSoatSQL() {
		BigDecimal empAfiliadora = new BigDecimal("1019280");
		qty_vehicles=0;
		qty_gruas=0;
		qty_buses=0;
		qty_fidelity=0;
		StringBuilder result = new StringBuilder();
		StringBuilder resultgruas = new StringBuilder();
		StringBuilder resultbuses = new StringBuilder();
		StringBuilder resultfidelity = new StringBuilder();
		String sql = "SELECT COALESCE(vehicletype,''), smj_plate, soatexpiration, "	// 1, 2, 3
				+ "(CASE WHEN soatexpiration<current_date THEN 'VENCIDO' ELSE "
				+ "soatexpiration-current_date || ' DIAS PARA VENCER' END) AS estado, "	// 4
				+ "edsfidelity, COALESCE(affiliatecompany_id,0), datedisaffiliation "		// 5, 6, 7
				+ "FROM smj_vehicle "
				+ "WHERE soatexpiration<=current_date+10  "
				+ "ORDER BY soatexpiration ";
		
		if (log.isLoggable(Level.INFO)) log.info("Class " + ExpiredSoatNotice.class + ": sql="+sql);
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql, get_TrxName());
			rs = st.executeQuery();

			while (rs.next()) {
				if(rs.getString(5).equals("Y"))  // Y- Fidelizado 
				{
					resultfidelity.append("Placa:" + rs.getString(2)+ "  " );
					resultfidelity.append("Fecha de Vencimiento:" + rs.getDate(3) + "  ");
					resultfidelity.append("Observacion:" + rs.getString(4) + ".  ");
					resultfidelity.append("\n");
					qty_fidelity++;
				}
				else if(rs.getString(1).equals("G"))  // G- Grua 
				{
					resultgruas.append("Placa:" + rs.getString(2)+ "  " );
					resultgruas.append("Fecha de Vencimiento:" + rs.getDate(3) + "  ");
					resultgruas.append("Observacion:" + rs.getString(4) + ".  ");
					resultgruas.append("\n");
					qty_gruas++;
				}
				else if(rs.getString(1).equals("B") && empAfiliadora.compareTo(rs.getBigDecimal(6))==0
						&& rs.getString(7)==null)  // B - Bus 
				{
					resultbuses.append("Placa:" + rs.getString(2)+ "  " );
					resultbuses.append("Fecha de Vencimiento:" + rs.getDate(3) + "  ");
					resultbuses.append("Observacion:" + rs.getString(4) + ".  ");
					resultbuses.append("\n");
					qty_buses++;
				}
				else if(rs.getString(1).equals("X") || rs.getString(1).equals("C"))    // X-Tractocamion C-Camion
				{
					result.append("Placa:" + rs.getString(2)+ "  " );
					result.append("Fecha de Vencimiento:" + rs.getDate(3) + "  ");
					result.append("Observacion:" + rs.getString(4) + ".  ");
					result.append("\n");
					qty_vehicles++;
				}
			}

		}
		catch ( SQLException ex ) {
			log.log(Level.SEVERE, sql, ex);
		}
		finally {
			DB.close(rs, st);
			rs = null;
			st = null;
		}
		listGruas=resultgruas;
		listBuses=resultbuses;
		listOtros=result;
		listFidelity=resultfidelity;
		return result.toString() + resultgruas.toString() + resultbuses.toString() + resultfidelity.toString();
	}
}
