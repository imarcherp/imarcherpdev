package co.imarch.plugins.utils;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.process.*;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;

import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;

import org.compiere.model.MWarehouse;


public class CreateLineShipmentFromOrder extends SvrProcess
{

	private int defaultLocator_ID=0;
	
	private int t_C_Order_ID=0;
	private int t_M_InOut_ID=0;
	private int t_M_WareHouse_ID=0;
	private int t_M_Locator_ID=0;
	
	protected MOrder p_order = null;
	
	protected void prepare() 
	{
		ProcessInfoParameter[] ShipmentDetails = getParameter();
		String ColumnName ="";
		for( int i=0; i < ShipmentDetails.length ; i++)
		{
			ColumnName= ShipmentDetails[i].getParameterName();
			if(ShipmentDetails[i].getParameterName() == null)
				;
			else if(ColumnName.equals("C_Order_ID"))
				t_C_Order_ID=ShipmentDetails[i].getParameterAsInt();
			else if(ColumnName.equals("M_InOut_ID"))
				t_M_InOut_ID=ShipmentDetails[i].getParameterAsInt();
			else if(ColumnName.equals("M_Warehouse_ID"))
				t_M_WareHouse_ID=ShipmentDetails[i].getParameterAsInt();
			else
				log.log(Level.INFO, "prepare - Unknown Parameter: ", ColumnName);	
		}
		
		MWarehouse wh = MWarehouse.get(Env.getCtx(), t_M_WareHouse_ID);
		if (wh != null)
		{
			MLocator locatorid = wh.getDefaultLocator();
			t_M_Locator_ID = locatorid.get_ID();
		}
	}// end prepare
	
	
	protected String doIt() throws Exception 
	{
		Vector<Vector<Object>> OLines = new Vector<Vector<Object>>();
		
		if (t_C_Order_ID == 0)
			return "Lines = 0";
		String trxname = get_TrxName();
		
		OLines=getOrderData(t_C_Order_ID, false, t_M_Locator_ID); //Obtiene las lineas de la Orden de Compra
		
		boolean issave = save_inoutlines(OLines, trxname);// Crea las Lineas y Guarda
		
		if(!issave)
			return "Lines=0"; // Error al crear las lineas
		
		return "@OK@";
	}
	

	protected Vector<Vector<Object>> GetLinesOrder (int C_Order_ID, boolean forInvoice)
	{
		/**
		 *  Selected        - 0
		 *  Qty             - 1
		 *  C_UOM_ID        - 2
		 *  M_Locator_ID    - 3
		 *  M_Product_ID    - 4
		 *  VendorProductNo - 5
		 *  OrderLine       - 6
		 *  ShipmentLine    - 7
		 *  InvoiceLine     - 8
		 */
		if (log.isLoggable(Level.CONFIG)) log.config("C_Order_ID=" + C_Order_ID);
		p_order = new MOrder (Env.getCtx(), C_Order_ID, null);      //  save

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT "
				+ "l.QtyOrdered-SUM(COALESCE(m.Qty,0))"
				// subtract drafted lines from this or other orders IDEMPIERE-2889
				+ "-COALESCE((SELECT SUM(MovementQty) FROM M_InOutLine iol JOIN M_InOut io ON iol.M_InOut_ID=io.M_InOut_ID WHERE l.C_OrderLine_ID=iol.C_OrderLine_ID AND io.Processed='N'),0),"	//	1
				+ "CASE WHEN l.QtyOrdered=0 THEN 0 ELSE l.QtyEntered/l.QtyOrdered END,"	//	2
				+ " l.C_UOM_ID,COALESCE(uom.UOMSymbol,uom.Name),"			//	3..4
				+ " p.M_Locator_ID, loc.Value, " // 5..6
				+ " COALESCE(l.M_Product_ID,0),COALESCE(p.Name,c.Name), " //	7..8
				+ " po.VendorProductNo, " // 9
				+ " l.C_OrderLine_ID,l.Line "	//	10..11
				+ "FROM C_OrderLine l"
				+ " LEFT OUTER JOIN M_Product_PO po ON (l.M_Product_ID = po.M_Product_ID AND l.C_BPartner_ID = po.C_BPartner_ID) "
				+ " LEFT OUTER JOIN M_MatchPO m ON (l.C_OrderLine_ID=m.C_OrderLine_ID AND ");
		sql.append(forInvoice ? "m.C_InvoiceLine_ID" : "m.M_InOutLine_ID");
		sql.append(" IS NOT NULL)")
		.append(" LEFT OUTER JOIN M_Product p ON (l.M_Product_ID=p.M_Product_ID)"
				+ " LEFT OUTER JOIN M_Locator loc on (p.M_Locator_ID=loc.M_Locator_ID)"
				+ " LEFT OUTER JOIN C_Charge c ON (l.C_Charge_ID=c.C_Charge_ID)");
		if (Env.isBaseLanguage(Env.getCtx(), "C_UOM"))
			sql.append(" LEFT OUTER JOIN C_UOM uom ON (l.C_UOM_ID=uom.C_UOM_ID)");
		else
			sql.append(" LEFT OUTER JOIN C_UOM_Trl uom ON (l.C_UOM_ID=uom.C_UOM_ID AND uom.AD_Language='")
			.append(Env.getAD_Language(Env.getCtx())).append("')");
		//
		sql.append(" WHERE l.C_Order_ID=? "			//	#1
				+ "GROUP BY l.QtyOrdered,CASE WHEN l.QtyOrdered=0 THEN 0 ELSE l.QtyEntered/l.QtyOrdered END, "
				+ "l.C_UOM_ID,COALESCE(uom.UOMSymbol,uom.Name), p.M_Locator_ID, loc.Value, po.VendorProductNo, "
				+ "l.M_Product_ID,COALESCE(p.Name,c.Name), l.Line,l.C_OrderLine_ID "
				+ "ORDER BY l.Line");
		//
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           //  0-Selection
				BigDecimal qtyOrdered = rs.getBigDecimal(1);
				BigDecimal multiplier = rs.getBigDecimal(2);
				BigDecimal qtyEntered = qtyOrdered.multiply(multiplier);
				line.add(qtyEntered);  //  1-Qty
				KeyNamePair pp = new KeyNamePair(rs.getInt(3), rs.getString(4).trim());
				line.add(pp);                           //  2-UOM
				// Add locator
				line.add(getLocatorKeyNamePair(rs.getInt(5)));// 3-Locator
				// Add product
				pp = new KeyNamePair(rs.getInt(7), rs.getString(8));
				line.add(pp);                           //  4-Product
				line.add(rs.getString(9));				// 5-VendorProductNo
				pp = new KeyNamePair(rs.getInt(10), rs.getString(11));
				line.add(pp);                           //  6-OrderLine
				line.add(null);                         //  7-Ship
				line.add(null);                         //  8-Invoice
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}   //  GetLinesOrder
	
	protected KeyNamePair getLocatorKeyNamePair(int M_Locator_ID)
	{
		MLocator locator = null;
		
		// Load desired Locator
		if (M_Locator_ID > 0)
		{
			locator = MLocator.get(Env.getCtx(), M_Locator_ID);
			// Validate warehouse
			if (locator != null && locator.getM_Warehouse_ID() != getM_Warehouse_ID_select())
			{
				locator = null;
			}
		}
		
		// Try to use default locator from Order Warehouse
		if (locator == null && p_order != null && p_order.getM_Warehouse_ID() == getM_Warehouse_ID_select())
		{
			MWarehouse wh = MWarehouse.get(Env.getCtx(), p_order.getM_Warehouse_ID());
			if (wh != null)
			{
				locator = wh.getDefaultLocator();
				defaultLocator_ID = locator.get_ID();
			}
		}
		// Try to get from locator field
		if (locator == null)
		{
			if (defaultLocator_ID > 0)
			{
				locator = MLocator.get(Env.getCtx(), defaultLocator_ID);
			}
		}
		// Validate Warehouse
		if (locator == null || locator.getM_Warehouse_ID() != getM_Warehouse_ID_select())
		{
			locator = MWarehouse.get(Env.getCtx(), getM_Warehouse_ID_select()).getDefaultLocator();
		}
		
		KeyNamePair pp = null ;
		if (locator != null)
		{
			pp = new KeyNamePair(locator.get_ID(), locator.getValue());
		}
		return pp;
	}//getLocatorKeyNamePair
	
	
	public int getM_Warehouse_ID_select()
	{
		return t_M_WareHouse_ID;
	}
	
	
	public boolean save_inoutlines(Vector<Vector<Object>> OrderLines, String trxName)
	{
		
		int M_Locator_ID = defaultLocator_ID;
		if (M_Locator_ID == 0) 
			return false;

		
		MInOut inout = new MInOut(Env.getCtx(), t_M_InOut_ID, trxName);
		if (log.isLoggable(Level.CONFIG)) 
			log.config(inout + ", C_Locator_ID=" + M_Locator_ID);
		
		//Lineas
		
		for(int i=0; i < OrderLines.size(); i++)
		{
			Vector<Object> t_line = new Vector<Object>();
			t_line =   OrderLines.get(i); // miniTable.getValueAt(i, 1); // Qty
			BigDecimal QtyEntered = (BigDecimal) t_line.get(1);// Qty
			//KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 2); // UOM
			KeyNamePair pp = (KeyNamePair) t_line.get(2);// UOM
			int C_UOM_ID = pp.getKey();
			pp = (KeyNamePair) t_line.get(3); // Locator
			// If a locator is specified on the product, choose that otherwise default locator
			M_Locator_ID = pp!=null && pp.getKey()!=0 ? pp.getKey() : defaultLocator_ID;
			
			pp = (KeyNamePair) t_line.get(4); // Product
			int M_Product_ID = pp.getKey();
			int C_OrderLine_ID = 0;
			pp = (KeyNamePair) t_line.get(6); // OrderLine
			if (pp != null)
				C_OrderLine_ID = pp.getKey();
			
			int precision = 2;
			if (M_Product_ID != 0)
			{
				MProduct product = MProduct.get(Env.getCtx(), M_Product_ID);
				precision = product.getUOMPrecision();
			}
			QtyEntered = QtyEntered.setScale(precision, BigDecimal.ROUND_HALF_DOWN);
			//
			if (log.isLoggable(Level.FINE)) log.fine("Line QtyEntered=" + QtyEntered
					+ ", Product=" + M_Product_ID 
					+ ", OrderLine=" + C_OrderLine_ID);

			//	Create new InOut Line
			MInOutLine iol = new MInOutLine (inout);
			iol.setM_Product_ID(M_Product_ID, C_UOM_ID);	//	Line UOM
			iol.setQty(QtyEntered);							//	Movement/Entered
			//
			MOrderLine ol = null;
			//MRMALine rmal = null;
			if (C_OrderLine_ID != 0)
			{
				iol.setC_OrderLine_ID(C_OrderLine_ID);
				ol = new MOrderLine (Env.getCtx(), C_OrderLine_ID, trxName);
				if (ol.getQtyEntered().compareTo(ol.getQtyOrdered()) != 0)
				{
					iol.setMovementQty(QtyEntered
							.multiply(ol.getQtyOrdered())
							.divide(ol.getQtyEntered(), 12, BigDecimal.ROUND_HALF_UP));
					iol.setC_UOM_ID(ol.getC_UOM_ID());
				}
				iol.setM_AttributeSetInstance_ID(ol.getM_AttributeSetInstance_ID());
				iol.setDescription(ol.getDescription());
				//
				iol.setC_Project_ID(ol.getC_Project_ID());
				iol.setC_ProjectPhase_ID(ol.getC_ProjectPhase_ID());
				iol.setC_ProjectTask_ID(ol.getC_ProjectTask_ID());
				iol.setC_Activity_ID(ol.getC_Activity_ID());
				iol.setC_Campaign_ID(ol.getC_Campaign_ID());
				iol.setAD_OrgTrx_ID(ol.getAD_OrgTrx_ID());
				iol.setUser1_ID(ol.getUser1_ID());
				iol.setUser2_ID(ol.getUser2_ID());
			}
			iol.setM_Locator_ID(M_Locator_ID);
			iol.saveEx();

			
		}
		/**
		 *  Update Header
		 *  - if linked to another order/invoice/rma - remove link
		 *  - if no link set it
		 */
		if (p_order != null && p_order.getC_Order_ID() != 0)
		{
			inout.setC_Order_ID (p_order.getC_Order_ID());
			inout.setAD_OrgTrx_ID(p_order.getAD_OrgTrx_ID());
			inout.setC_Project_ID(p_order.getC_Project_ID());
			inout.setC_Campaign_ID(p_order.getC_Campaign_ID());
			inout.setC_Activity_ID(p_order.getC_Activity_ID());
			inout.setUser1_ID(p_order.getUser1_ID());
			inout.setUser2_ID(p_order.getUser2_ID());

			if ( p_order.isDropShip() )
			{
				inout.setM_Warehouse_ID( p_order.getM_Warehouse_ID() );
				inout.setIsDropShip(p_order.isDropShip());
				inout.setDropShip_BPartner_ID(p_order.getDropShip_BPartner_ID());
				inout.setDropShip_Location_ID(p_order.getDropShip_Location_ID());
				inout.setDropShip_User_ID(p_order.getDropShip_User_ID());
			}
		}
		inout.saveEx();
		return true;
	}//save_inoutlines
	
	protected Vector<Vector<Object>> getOrderData (int C_Order_ID, boolean forInvoice, int M_Locator_ID)
	{
		defaultLocator_ID = M_Locator_ID;
		return GetLinesOrder (C_Order_ID, forInvoice);
	}
	
}
