package co.imarch.plugins.utils;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.process.*;
import org.compiere.util.AdempiereUserError;
import org.compiere.model.MRequest;
import org.compiere.model.MSysConfig;

public class WorkOrderReOpen extends SvrProcess {
	
	//private static final CLogger logger = CLogger.getCLogger(WorkOrderReOpen.class);
	
	private int t_R_Request_ID=0;
	//private int t_Status_ID = MSysConfig.getIntValue("iMarch_ReOpenWorkOrder", Env.getAD_Client_ID(Env.getCtx()));
	private int t_Status_ID = MSysConfig.getIntValue("iMarch_ReOpenWorkOrder", 0, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
	private Timestamp t_CloseDate=null;
/*
 * Prepare
 */
	protected void prepare() 
	{
		ProcessInfoParameter[] workorderid = getParameter(); 
		
		String nameColum = "";
		for (int i=0; i < workorderid.length; i++)
		{
			nameColum = workorderid[i].getParameterName();
			if (workorderid[i].getParameter() == null)
				;
			else if (nameColum.equals("R_Request_ID")) 
				t_R_Request_ID= workorderid[i].getParameterAsInt();
			else if (nameColum.equals("CloseDate")) 
				t_CloseDate= workorderid[i].getParameterAsTimestamp();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: ", nameColum);
		}
		
	}//prepare
	
	
	
	protected String doIt() throws Exception 
	{
		MRequest WorkOrder = new MRequest(getCtx(), t_R_Request_ID, get_TrxName());
		if (log.isLoggable(Level.INFO)) log.info(WorkOrder.toString());
		if (WorkOrder.get_ID()==0)
			throw new AdempiereUserError("@NotFound@ @R_Request_ID@ = " + t_R_Request_ID);
		
		Boolean inuse = getWorkOrderUsed(t_R_Request_ID); 
		if (inuse)
			return "@Error@" + " " + "@WORKORDER@" + ":" + WorkOrder.getDocumentNo() + " - " + "@DEPENDENCE@" ;
		
		
		Calendar actualDate = Calendar.getInstance();
		Calendar closeDate = Calendar.getInstance();
		
		actualDate.setTime(Env.getContextAsDate(getCtx(), "#Date"));
		closeDate.setTime(t_CloseDate);
			
		if(closeDate.get(Calendar.MONTH)!=actualDate.get(Calendar.MONTH))
			return "@Error@" + " " + "@OMOUTOFPERIOD@" ;
		
		
		WorkOrder.setR_Status_ID(t_Status_ID);
		WorkOrder.setCloseDate(null);
		WorkOrder.setC_Order_ID(0);
		if (WorkOrder.save() && !WorkOrder.isProcessed())
		{//actualiza lineas setStatusLine()
			
			return "@OK@" + " " + "@WORKORDER@" + ":" +
					WorkOrder.getDocumentNo() + " - " +  setStatusLine(t_R_Request_ID) +
					" - " + removeCommisionTable(t_R_Request_ID);
			
		}
			
		return "@Error@";
		
	}
	
	/*
	 * Querry verifica WorkOrder no este cargada en POS
	 * @parameter  R_Request_ID
	 * @result true/false
	 */
	public Boolean getWorkOrderUsed (int R_Request_ID)  
	{
		Boolean result=false;
		StringBuilder sql = new StringBuilder(""
				+ "SELECT smj_tmpwebsales_id FROM smj_tmpwebsales WHERE "
				+ "smj_workorder='"
				+ R_Request_ID
				+"' ");
		
		if (log.isLoggable(Level.INFO)) log.fine(sql.toString());
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			//pstmt.setString(1, DocumentNo);
			rs = pstmt.executeQuery();	
			if(rs.next())
			{
				log.log(Level.SEVERE, "@DEPENDENCE@" +": " + rs.getString(1));
				result=true;
			}
			else
			{
				result=false;
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			result=true;
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		return result;
	}//getWorkOrderUsed
	
	/*
	 * UpdateQuerry cambia estado de linea en WorkOrder
	 * @parameter  R_Request_ID
	 * @result Actualizacion Exitosa
	 */	
	public String setStatusLine (int R_Request_ID ){
		
		String result = "@UPDATELINE@" + " " + "@OK@";
		StringBuilder sql = new StringBuilder( ""
				+ "UPDATE smj_workorderline SET "
				+ "wolstate='A', "
			    + "updatedby= '" + Env.getAD_User_ID(Env.getCtx()) + "', "
			    + "updated= SysDate "
				+ "WHERE wolstate='C' AND R_Request_ID=?");
		
		if (log.isLoggable(Level.INFO)) log.fine(sql.toString());
			
		PreparedStatement pstmt = null;
		//ResultSet rs = null;
		
		try 
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1,R_Request_ID);
			pstmt.executeUpdate();
			
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			result="@Error@" + " " + "@UPDATELINE@";
		}
		finally
		{
			DB.close(pstmt);
		}
		
		return result;
	}//setStatusLine
	
	
	public String removeCommisionTable (int R_Request_ID ){
		
		int nodel = DB.executeUpdateEx(
				"DELETE FROM smj_MechanicCommission WHERE R_Request_ID = ?",
				new Object[] { R_Request_ID },
				null);
		
		String result = "("+nodel+") " + "@Deleted@" + " Registros Tabla smj_MechanicCommission ";
		
		if(nodel > 0) 
			result= result  + "@OK@"; 

		return result;
		
		
	}//removeCommisionTable
	
}
