package co.imarch.plugins.utils.model;

import org.compiere.report.core.RModel;

import co.imarch.plugins.utils.AbstractExcelExporteriMarch;



public class RModelExcelExporteriMarch extends AbstractExcelExporteriMarch{

	private RModel m_model = null;
	private int m_currentRow = 0;

	public RModelExcelExporteriMarch(RModel model) {
		super();
		m_model = model;
	}

	@Override
	public int getColumnCount() {
		return m_model.getColumnCount();
	}

	@Override
	public int getDisplayType(int row, int col) {
		return m_model.getRColumn(col).getDisplayType();
	}

	@Override
	public String getHeaderName(int col) {
		return m_model.getRColumn(col).getColHeader();
	}

	@Override
	public int getRowCount() {
		return m_model.getRowCount();
	}

	@Override
	public Object getValueAt(int row, int col) {
		return m_model.getValueAt(row, col);
	}

	@Override
	public boolean isColumnPrinted(int col) {
		return true;
	}

	@Override
	public boolean isFunctionRow() {
		return m_model.isGroupRow(m_currentRow);
	}

	@Override
	public boolean isPageBreak(int row, int col) {
		return false;
	}

	@Override
	protected void setCurrentRow(int row) {
		m_currentRow = row;
	}

	protected int getCurrentRow() {
		return m_currentRow;
	}

}
