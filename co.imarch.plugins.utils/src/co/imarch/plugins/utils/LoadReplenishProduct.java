package co.imarch.plugins.utils;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;

import org.compiere.model.MLocator;
import org.compiere.model.MReplenish;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Trx;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

public class LoadReplenishProduct extends SvrProcess{
	
	private int t_AD_Org_ID = 0;
	private int t_M_Locator_ID = 0;
	private String file = "";
	
	private StringBuffer msjErrorValid = new StringBuffer("");

	@Override
	protected void prepare() {


		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("file")){
				file = (String)para[i].getParameter();
			}
			else if (name.equals("M_Locator_ID")){
				t_M_Locator_ID = para[i].getParameterAsInt();
			}
			else if (name.equals("AD_Org_ID")){
				t_AD_Org_ID = para[i].getParameterAsInt();
			}
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		if(!file.endsWith("csv"))
			if(!file.endsWith("CSV"))
				return "@Error@" + " Tipo de Archivo no valido";
		
			
				
		Trx trx = Trx.get(get_TrxName(), true);
		String trxName = trx.getTrxName();
		
		int actualizadas = 0;
		int creadas = 0;
		
		CsvListReader cReader = null;
		cReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);   // delimitador es ; y " para textos
		
		final CellProcessor[] processors = getProcesador();
		List<Object> camposBase;
		
		Boolean noValid = validateFieldID( processors, cReader);
		
		if(noValid) {
			cReader.close();
			return "@Error@" + msjErrorValid.toString();
		}
		
		cReader.close();
		cReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);   // delimitador es ; y " para textos
		while( (camposBase = cReader.read(processors)) != null ) {
			Object[] campos = camposBase.toArray();
			
			int productid = getProduct(campos[0].toString().trim());			
			BigDecimal minQty = new BigDecimal((String)campos[1].toString().trim());
			BigDecimal maxQty = new BigDecimal((String)campos[2].toString().trim());
			
			Boolean existReplenish = false;
			for (MReplenish rep : MReplenish.getForProduct(getCtx(), productid, trxName))
				{
					if ( rep.getM_Locator_ID() == t_M_Locator_ID)
					{
						existReplenish = true;
						rep.setLevel_Min(minQty);
						rep.setLevel_Max(maxQty);
						rep.saveEx();
						actualizadas++;
					}
				}
			if(!existReplenish) 
			{
				MReplenish newreplenish = new MReplenish(getCtx(), 0, trxName);
				MLocator m_locator = new MLocator(getCtx(), t_M_Locator_ID, trxName);
				newreplenish.setM_Product_ID(productid);
				newreplenish.setM_Warehouse_ID(m_locator.getM_Warehouse_ID());
				newreplenish.setAD_Org_ID(t_AD_Org_ID);
				newreplenish.setReplenishType("1");
				newreplenish.setLevel_Min(minQty);
				newreplenish.setLevel_Max(maxQty);
				newreplenish.setM_Locator_ID(t_M_Locator_ID);
				newreplenish.saveEx();	
				creadas++;
			}
			
			
		}
		
		cReader.close();
		
		return "Proceso Terminado. Actualizados="+actualizadas+ " / Creados="+creadas;
	}

	private static CellProcessor[] getProcesador() {
		// 10 COLUMNAS
		final CellProcessor[] processors = new CellProcessor[] {  
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\"")
		};		


		return processors;
	}
	
	private Boolean validateFieldID(CellProcessor[] processors, CsvListReader cReader) throws IOException 
	{
		
		List<Object> camposBase;
		Vector<Object> code = new Vector<Object>();
		Boolean result = false;
		
		while( (camposBase = cReader.read(processors)) != null ) 
		{
			Object[] campos = camposBase.toArray();
			
			code.add(campos[0]);
			
			if(getProduct(campos[0].toString().trim())<=0 ) { 
				if(msjErrorValid.toString().length()==0 )
					msjErrorValid =	new StringBuffer("Codigo ingresado no existe: ");
				else 
					msjErrorValid.append(", ");
				msjErrorValid.append(campos[0].toString().trim());
				result = true;
			}
			else {
				for(int i=1 ; i<code.size(); i++) 
				{
					if(campos[0].toString().trim().equals(code.get(i-1).toString().trim()))
					{
						msjErrorValid =	new StringBuffer("El Archivo presenta codigos repetidos");
						return true;
						
					}
				}
			}
			
		}
		
		return result;
	}
	
	private int getProduct(Object product) {
		int result = DB.getSQLValue(null, "SELECT COALESCE(m_product_id,0) FROM m_product WHERE value=?", product);
		return result;
	}
		

	
}
