package co.imarch.plugins.utils;

import java.io.FileReader;
import java.util.List;

import org.compiere.model.MInvoice;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

public class LoadCUFE extends SvrProcess{
	
	private int t_AD_Org_ID = 0;
	private int t_AD_User_ID = 0;
	private String file = "";
	private String t_IDDocumentTypeFE="";
	
	private StringBuffer msjErrorValid = new StringBuffer("");

	@Override
	protected void prepare() {

		t_AD_User_ID = Env.getAD_User_ID(Env.getCtx());

		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("file")){
				file = (String)para[i].getParameter();
			}
			else if (name.equals("AD_Org_ID")){
				t_AD_Org_ID = para[i].getParameterAsInt();
			}
			else if (name.equals("IDDocumentTypeFE")){
					t_IDDocumentTypeFE = para[i].getParameterAsString();
			}
		}
		
	}

	@Override
	protected String doIt() throws Exception {

		msjErrorValid= new StringBuffer(" ");
		Trx trx = Trx.get(get_TrxName(), true);
		int totalUpDate = 0;
		int totalError = 0;
		CsvListReader cReader = null;
		cReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_PREFERENCE);   // delimitador es , y " para textos
		
		final CellProcessor[] processors = getProcesador();
		List<Object> camposBase;
		
		while( (camposBase = cReader.read(processors)) != null ) {
			
			Object[] campos = camposBase.toArray();
			String documenNo = campos[0].toString().trim();
			String cufe = campos[1].toString().trim();
			
			int invoiceID = getInvoiceId(documenNo);
			if(invoiceID<0) {
				msjErrorValid.append(documenNo);
				msjErrorValid.append(", ");
				totalError++;
			}
			else {
				MInvoice invoice = new MInvoice(Env.getCtx(), invoiceID, trx.getTrxName());
				invoice.set_ValueOfColumn("cufe", cufe);
				
				if(!invoice.save()) {
					msjErrorValid = new StringBuffer("Error al gurdar cambios. Inconsictencia con ");
					msjErrorValid.append("Documento No: "+documenNo);
					trx.rollback();
					cReader.close();
					return msjErrorValid.toString();
				}
				totalUpDate++;
			}
							
		}
		
		cReader.close();
		return "Facturas Actualizadas: "+totalUpDate +"\n"+" / Inconsistencias encontradas: "
				+totalError+" => "+msjErrorValid;
	}

	private int getInvoiceId(Object documentNo) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT C_Invoice_ID FROM C_Invoice i WHERE i.IsSoTrx = 'Y' ");
		sql.append(" AND  i.DocumentNo=? ");
		if(t_IDDocumentTypeFE.length()>0)
			sql.append(" AND  i.C_DocType_ID IN ( ").append(t_IDDocumentTypeFE).append(") ");
		if(t_AD_Org_ID>0)
			sql.append(" AND  i.AD_Org_ID= '").append(t_AD_Org_ID).append("' ");
		
		int result = DB.getSQLValue(null, sql.toString(), documentNo);
		return result;
	}
	
	private static CellProcessor[] getProcesador() {
		// 2 COLUMNAS
		final CellProcessor[] processors = new CellProcessor[] {  
				new ConvertNullTo("\"\""),
				new ConvertNullTo("\"\"")
		};		


		return processors;
	}
}
