package org.idempiere.extend.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;


import org.compiere.util.CLogger;
import org.compiere.util.DB;


public class DataQuerysSpecialEditor {

	public static CLogger log = CLogger.getCLogger(DataQuerysSpecialEditor.class);
	/**
	 * Busca si la Factura tiene Pagos
	 * @param InvoiceID
	 * @return
	 */
	public static int getAllocateInvoice (Integer InvoiceID){
		
		int result = 0 ;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(al.C_AllocationLine_id) FROM C_AllocationLine AS al ");
		sql.append("WHERE al.C_Invoice_ID=");
		sql.append("'"+InvoiceID+"' ");
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		
		return result;
	}// getAllocateInvoice
	
	public static int getRMAInvoice (Integer InvoiceID){
		
		int result = 0 ;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(m_rma_id) FROM C_Invoice WHERE C_Invoice_ID=");
		sql.append("'"+InvoiceID+"'");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}
		
		return result;
	}// getRMAInvoice
	
	public static Vector<Object> getPaySelectionID(int C_Payment_ID){
		
		Vector<Object> result = new Vector<Object>();
		
		StringBuffer sql = new StringBuffer("SELECT C_PaySelection_ID, C_PaySelectionCheck_ID "
				+"FROM adempiere.C_PaySelectionCheck WHERE C_Payment_ID=?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try{
			st = DB.prepareStatement(sql.toString(), null);
			st.setInt(1, C_Payment_ID);
			rs = st.executeQuery();
			
			while(rs.next()){
				result.add(rs.getInt(1));
				result.add(rs.getInt(2));
			}
			
		}
		catch(Exception e){
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally{
			DB.close(rs, st);
		}
		return result;
	}//getPaySelectionID
	
	public static String getPlate(int smj_vehicle_id) {
		
		String result = "";
		
		StringBuffer sql = new StringBuffer("SELECT smj_plate " 
				+ "FROM smj_vehicle WHERE smj_vehicle_id=? ");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			st.setInt(1, smj_vehicle_id);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getString(1);
			}
			
		}
		catch (Exception e){
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally {
			DB.close(rs, st);
			rs = null;
			st = null;
		}
		
		return result;
	}
	
	public static Boolean updatePlateCraneControl(String Plate, int imarch_CraneControl_ID) {
		
		StringBuffer sql = new StringBuffer();
		
		sql.append(" UPDATE imarch_CraneControl SET imarch_crane_plate = '"+Plate+"' ");
		sql.append(" WHERE imarch_CraneControl_ID = '"+imarch_CraneControl_ID+"' ");
		PreparedStatement pstmt = null;
		Integer rs = -1;
		Boolean flag = false;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), null);
			rs = pstmt.executeUpdate();
			flag = rs>0?true:false;
		}catch (Exception e)	{
			flag = false;
			log.log (Level.SEVERE, DataQuerysSpecialEditor.class.getName()+".updatePlateCraneControl - ERROR::"+e.getMessage() , e);
		}
		finally	{
			DB.close(pstmt);
			rs = null; pstmt = null;
		}
		
		return flag;
	}
	
	public static String getApproveds(int R_Request_ID) {
		
		String result="";
		
		StringBuffer sql = new StringBuffer("SELECT smj_isinvoicecharge || COALESCE(isapproved,'N') " 
				+ "FROM R_Request r "
				+ "LEFT JOIN smj_controlAdvances a ON r.R_Request_ID=a.R_Request_ID "
				+ "WHERE r.R_Request_ID=? ");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), null);
			st.setInt(1, R_Request_ID);
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getString(1);
			}
			
		}
		catch (Exception e){
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally {
			DB.close(rs, st);
			rs = null;
			st = null;
		}
		
		return result;
	}
}
