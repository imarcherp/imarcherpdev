package org.idempiere.extend;


import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInOut;
import org.compiere.model.PO;
import org.compiere.model.X_M_InOut;
import org.compiere.util.Env;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;

public class SpecialEditM_InOut implements ISpecialEditCallout{
	
	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("cantEdit "+mTab+" - "+mField+" - "+po);
		
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("validateEdit"+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("preEdit " + mTab + " - " + mField + " - "+ po);
		
		//PO popreedit= new MPayment(Env.getCtx(), (Integer) mTab.getValue("C_Payment_ID"), null);
		PO popreedit= new MInOut(Env.getCtx(), (Integer) mTab.getValue("M_InOut_ID"), null);
		SpecialEditorUtils.deletePosting(popreedit);
		
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("updateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		X_M_InOut inout = new X_M_InOut(Env.getCtx(),mTab.getRecord_ID(), null);
		if(mField.getColumnName().equals("Description"))
			inout.setDescription((String)newValue);
		else
			return false;
		
		inout.saveEx();
		
		return true;
	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("postEdit " + mTab + " - " + mField + " - "+ po);
		
		PO popostedit = new MInOut(Env.getCtx(), (Integer) mTab.getValue("M_InOut_ID"), null);
		SpecialEditorUtils.post(mTab, popostedit);
		
		SpecialEditorUtils.refresh(mTab);
		return true;
	}

}
