package org.idempiere.extend;

import org.adempiere.webui.component.Messagebox;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MRequest;
import org.compiere.model.PO;
import org.compiere.model.X_R_Request;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;
import org.idempiere.extend.utils.DataQuerysSpecialEditor;

public class SpecialEditR_Request implements ISpecialEditCallout {

	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		
		//Permitir editar Campos, si no tiene Factura smj_vehicle_ID smj_plate
		if(mTab.getValue("C_Invoice_ID")!=null)
			return false;
		//deshabilita opcion de editar campo Tercero
		if(mField.getColumnName().equals("C_BPartner_ID"))
			return false;
		
		System.out.println("canEdit  "+mTab+" - "+mField+" - "+po);
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		
		System.out.println("Metod validateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		if(newValue == null)
			return Msg.getMsg(Env.getCtx(),"iMarch_MsgValidateSpecialEdit");
		
		if(mField.getColumnName().equals("smj_vehicle_ID")) {
			String approved = DataQuerysSpecialEditor.getApproveds(mTab.getRecord_ID());
			if(!approved.equals("NN"))
				return Msg.getMsg(Env.getCtx(),"iMarch_MsgValidateSpecialEditRequest");
		}
		
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		
		System.out.println("updateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		

		MRequest m_request = new MRequest(Env.getCtx(), mTab.getRecord_ID(), null); 
		
		if(mField.getColumnName().equals("smj_vehicle_ID")) {
			m_request.set_ValueOfColumn("smj_vehicle_ID", newValue);
			m_request.set_ValueOfColumn("c_bpartnerdriver_ID", null);
			m_request.set_ValueOfColumn("c_bpartnerowner_ID", null);
			String plate = DataQuerysSpecialEditor.getPlate((Integer)newValue);
			m_request.set_ValueOfColumn("smj_plate", plate);
			if(mTab.getValue("imarch_cranecontrol_ID")!=null) {
				Boolean ok = DataQuerysSpecialEditor.updatePlateCraneControl(plate, (Integer)mTab.getValue("imarch_cranecontrol_ID"));
				if(!ok)
					return false;
				else
					Messagebox.showDialog("Control Guras Actualizado", "Aviso", 0, null);
					
			}
		}
		else if(mField.getColumnName().equals("C_BPartner_ID")) {
			m_request.setC_BPartner_ID((Integer)newValue);
		}
		else
			return false;
		m_request.saveEx();
		
		return true;

	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		
		System.out.println("postEdit "+mTab+" - "+mField+" - "+po);
		SpecialEditorUtils.refresh(mTab);
		
		return true;
	}

}
