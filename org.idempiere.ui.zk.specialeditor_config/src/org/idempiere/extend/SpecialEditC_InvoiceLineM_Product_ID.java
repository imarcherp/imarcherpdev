package org.idempiere.extend;

import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInvoice;
import org.compiere.model.PO;
import org.compiere.model.X_C_InvoiceLine;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;

public class SpecialEditC_InvoiceLineM_Product_ID implements ISpecialEditCallout{

	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("cantEdit "+mTab+" - "+mField+" - "+po);
		
		if(mTab.getValue("C_OrderLine_ID") != null || mTab.getValue("C_Charge_ID") != null)
			return false;
		
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("validateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		if(newValue == null)
			return Msg.getMsg(Env.getCtx(),"iMarch_MsgValidateSpecialEdit");
		
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("preEdit "+mTab+" - "+mField+" - "+po);
		
		PO poInvoice = new MInvoice(Env.getCtx(), (Integer)mTab.getValue("C_Invoice_ID"), null);
		SpecialEditorUtils.deletePosting(poInvoice);
		
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("updateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		X_C_InvoiceLine involine = new X_C_InvoiceLine(Env.getCtx(), mTab.getRecord_ID(), null);
		involine.setM_Product_ID((Integer)newValue);
		involine.saveEx();
		
		return true;
	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("postEdit "+mTab+" - "+mField+" - "+po);
		
		PO poInvoice = new MInvoice(Env.getCtx(), (Integer)mTab.getValue("C_Invoice_ID"), null);
		SpecialEditorUtils.post(mTab, poInvoice);
		
		SpecialEditorUtils.refresh(mTab);
		
		return true;
	}

}
