package org.idempiere.extend;

import java.sql.Timestamp;

import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInvoice;
import org.compiere.model.PO;
import org.compiere.model.X_C_Invoice;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;

import org.idempiere.extend.utils.DataQuerysSpecialEditor;

public class SpecialEditC_Invoice implements ISpecialEditCallout{

	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		int allocate = DataQuerysSpecialEditor.getAllocateInvoice((Integer)mTab.getValue("C_Invoice_ID"));
		int rma = DataQuerysSpecialEditor.getRMAInvoice((Integer)mTab.getValue("C_Invoice_ID"));
		if(allocate!=0 || rma!=0 || mTab.getValue("C_Order_ID")!=null)
			if(mField.getColumnName().equals("C_BPartner_ID"))
				return false;
		
		System.out.println("canEdit "+mTab+" - "+mField+" - "+po);
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("validateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		if(newValue == null)
			return Msg.getMsg(Env.getCtx(),"iMarch_MsgValidateSpecialEdit");
		
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("preEdit "+mTab+" - "+mField+" - "+po);
		
		PO poInvoicepreEdit = new MInvoice(Env.getCtx(), (Integer)mTab.getValue("C_Invoice_ID"), null);
		SpecialEditorUtils.deletePosting(poInvoicepreEdit);
		
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("updateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		X_C_Invoice xinvoice = new X_C_Invoice(Env.getCtx(),mTab.getRecord_ID(),null);
		if(mField.getColumnName().equals("DateInvoiced"))
			xinvoice.setDateInvoiced((Timestamp) newValue);
		else if(mField.getColumnName().equals("DateAcct"))
			xinvoice.setDateAcct((Timestamp) newValue);
		else if(mField.getColumnName().equals("C_BPartner_ID"))
			xinvoice.setC_BPartner_ID((Integer)newValue);
		else if(mField.getColumnName().equals("IsPaid"))
			xinvoice.setIsPaid((Boolean)newValue);
		else
			return false;
		
		xinvoice.saveEx();
		
		return true;
	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("postEdit "+mTab+" - "+mField+" - "+po);
		
		PO poInvoicepostEdit = new MInvoice(Env.getCtx(), (Integer)mTab.getValue("C_Invoice_ID"), null);
		SpecialEditorUtils.post(mTab, poInvoicepostEdit);
		SpecialEditorUtils.refresh(mTab);
		
		return true;
	}

}
