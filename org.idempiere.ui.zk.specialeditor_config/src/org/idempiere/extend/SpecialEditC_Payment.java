package org.idempiere.extend;


import java.sql.Timestamp;
import java.util.Vector;

import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MPayment;
import org.compiere.model.PO;
import org.compiere.model.X_C_PaySelection;
import org.compiere.model.X_C_PaySelectionCheck;
import org.compiere.model.X_C_Payment;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;
import org.idempiere.extend.utils.DataQuerysSpecialEditor;

public class SpecialEditC_Payment implements ISpecialEditCallout{
	
	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("cantEdit "+mTab+" - "+mField+" - "+po);
		
		//Permitir editar Cargo y Tercero, si no tiene Factura
		if(mTab.getValue("C_Invoice_ID")!=null)
			if(mField.getColumnName().equals("C_Charge_ID") || mField.getColumnName().equals("C_BPartner_ID"))
				return false;
		
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("validateEdit"+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		if(newValue==null){
			if(mField.getColumnName().equals("C_Charge_ID"))
				return null;
			else
				return Msg.getMsg(Env.getCtx(),"iMarch_MsgValidateSpecialEdit");
		}
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("preEdit " + mTab + " - " + mField + " - "+ po);
		
		PO popreedit= new MPayment(Env.getCtx(), (Integer) mTab.getValue("C_Payment_ID"), null);
		SpecialEditorUtils.deletePosting(popreedit);
		
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		System.out.println("updateEdit "+mTab+" - "+mField+" - "+po+" - "+newValue);
		
		X_C_Payment pay = new X_C_Payment(Env.getCtx(),mTab.getRecord_ID(), null);
		Vector<Object> payselections = DataQuerysSpecialEditor.getPaySelectionID(mTab.getRecord_ID());
		int C_PaySelection_ID = 0;
		int C_PaySelectionCheck_ID =0;
		X_C_PaySelection payselec = null;
		X_C_PaySelectionCheck payselecchk = null;
		if (!payselections.isEmpty()){
			C_PaySelection_ID=Integer.parseInt(payselections.elementAt(0).toString());
			C_PaySelectionCheck_ID = Integer.parseInt(payselections.elementAt(1).toString());
			payselec = new X_C_PaySelection(Env.getCtx(), C_PaySelection_ID, null);
			payselecchk = new X_C_PaySelectionCheck(Env.getCtx(), C_PaySelectionCheck_ID, null);
		}
		
		if(mField.getColumnName().equals("C_Charge_ID")){
			if (newValue == null){
				pay.setC_Charge_ID(0); //validacion en null=0
				pay.setIsAllocated(false);
			}
			else{
				pay.setC_Charge_ID((Integer)newValue); 
				pay.setIsAllocated(true);
			}
			//pay.setC_Charge_ID(newValue == null ? 0:(Integer)newValue); //validacion en null=0
		}
		else if(mField.getColumnName().equals("C_BPartner_ID")){
			pay.setC_BPartner_ID((Integer)newValue);
			if (!payselections.isEmpty()) payselecchk.setC_BPartner_ID((Integer)newValue);
		}
		else if(mField.getColumnName().equals("C_BankAccount_ID")){
			pay.setC_BankAccount_ID((Integer)newValue);
			if (!payselections.isEmpty()) payselec.setC_BankAccount_ID((Integer)newValue);
		}
		else if(mField.getColumnName().equals("Description")){
			pay.setDescription((String)newValue);
			if (!payselections.isEmpty()) payselec.setDescription((String)newValue);
		}
		else if(mField.getColumnName().equals("DateAcct"))
			pay.setDateAcct((Timestamp)newValue);
		else if(mField.getColumnName().equals("DateTrx")){
			pay.setDateTrx((Timestamp)newValue);
			if (!payselections.isEmpty()) payselec.setPayDate((Timestamp)newValue);
		}
		else
			return false;
		
		pay.saveEx();
		if (!payselections.isEmpty()){
			payselecchk.saveEx();	//linea seleccion de pago
			payselec.saveEx();		//Seleccion de Pago
		}
		return true;
	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("postEdit " + mTab + " - " + mField + " - "+ po);
		
		PO popostedit = new MPayment(Env.getCtx(), (Integer)mTab.getValue("C_Payment_ID"), null);
		SpecialEditorUtils.post(mTab, popostedit);
		
		SpecialEditorUtils.refresh(mTab);
		return true;
	}

}
